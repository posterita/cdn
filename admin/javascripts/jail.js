// Create the main Angular module
var app = angular.module('myApp', ['ui.router']);

// Configure the routes
app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('jails', {
      url: '/jails',      
      controller: 'JailsController',
      template: `
    	<h2>Jails</h2>
    	<table class="table">
    	  <tr ng-repeat="jail in jails">
    	  	<td>
		    	<a ui-sref="jail({ name: jail.name })">{{ jail.name }} <span class="badge badge-info">{{jail.count}}</span></a>
		    </td>
		  </tr>
    	</table>		
      `
    })
    .state('jail', {
      url: '/jail/:name',      
      controller: 'JailController',
      template: `
    	<a ui-sref="jails">Back to jails</a>
      
	    <h2>Jail - {{jail.name}} <button class="btn btn-danger" type="button" ng-click="releaseAll()">Release All</button></h2>					
		
		<br><br>
		
		<h4>Detainees List</h4>
		
		<br>
		
		<table class="table">
    	  <tr ng-repeat="detainee in jail.detainees">
    	  	<td>
    	  		<a href="javascript:void(0);" ng-click="showDetails(detainee.ip)">{{detainee.ip}}</a>
    	  	</td>
    	  	<td>
    	  		{{detainee.city}}, {{detainee.region}}, {{detainee.country_name}}
    	  	</td>
    	  	<td>
    	  		{{ formatDate(detainee.releaseTime) }}
    	  	</td>
    	  	<td>
    	  		<button class="btn btn-danger" type="button" ng-click="release(detainee.ip)">Release</button>
    	  	</td>
    	  </tr>
		</table>
		
		<div id="details-container">		
    	  <div id="ip-details" class="container"></div>
    	  <div id="map" class="container"></div>
    	</div>
      `
    });

  // Redirect to the jails state if no other URL matches
  $urlRouterProvider.otherwise('/jails');
});

app.service('JailService', function($http){
	
	let service = this;
	
	service.jails = null;
	
	service.getJails = function(){		
		
		if(this.jails){
			return Promise.resolve(this.jails);
		}
		else
		{
			return $http.get('AdminAction.do?action=getJails').then(function(response){
				service.jails = response.data;
				return service.jails;
			});
		}
		
		return this.jails;
	};
	
	service.releaseFromJail = function(jail, detainee){
		return $http.get(`AdminAction.do?action=releaseFromJail&jail=${jail}&detainee=${detainee}`);
	}
	
	service.releaseAllFromJail = function(jail){
		return $http.get(`AdminAction.do?action=releaseAllFromJail&jail=${jail}`);
	}
});

// Define the jails controller
app.controller('JailsController', function($scope, JailService, $timeout) {
	
	$scope.jails = [];
	
	// Sample data for the jails view  
	JailService.getJails().then(function(data){
		$timeout(function(){
			$scope.jails = data;
		});
	});
});

// Define the jail controller
app.controller('JailController', function($scope, $stateParams, $timeout, $http, JailService) {
  // Get the ID parameter from the URL
  var jailName = $stateParams.name;

  // Sample data for the jail view
  $scope.jail = null;
  
  JailService.getJails().then(function(data){
	  
	  let jails = data;
	  
	  let jail = jails.find(x => x.name == jailName);
	  
		$timeout(function(){
			$scope.jail = jail;
		});
	});
  
  $scope.release = function(detainee){
	  JailService.releaseFromJail($scope.jail.name, detainee).then(function(response){
		  alert("Released");
	  });
  };
  
  $scope.releaseAll = function(){
	  JailService.releaseAllFromJail($scope.jail.name).then(function(response){
		  alert("Released All");
	  });
  };
  
  $scope.showDetails = function(ipAddress){
	 $http.get(`https://ipapi.co/${ipAddress}/json/`).then(function(response){
		 let json = response.data;
		 console.log(json);
		 
		 if(json.latitude && json.longitude){
			 showMap(json.latitude , json.longitude);
		 }
		 else
		 {
			 document.getElementById('map').innerHTML = "";
		 }
		 
		 let div = document.getElementById('ip-details');
		 
		 div.innerHTML = `<pre>${JSON.stringify(json,null,4)}</pre>`;
	 });
  }
  
  function showMap(latitude, longitude) {
	  
	  if($scope.map){
		  $scope.map.remove();
	  }
	  
	  var map = L.map('map').setView([latitude, longitude], 10);
	  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
	    maxZoom: 18,
	  }).addTo(map);
	  L.marker([latitude, longitude]).addTo(map);
	  
	  $scope.map = map;
  }
  
  $scope.formatDate = function(d){
	  
	  return moment().add(Math.abs(d), 'seconds').fromNow();
  }
  
});
