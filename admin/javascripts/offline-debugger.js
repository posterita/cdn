// Refactored JS file

$(document).ready(function () {
  const modal = new BootstrapDialog({
    title: 'Please wait',
    message: 'Executing query ...',
    closable: false
  });

  // Custom alert function
  const showAlert = function (msg, type = BootstrapDialog.TYPE_DANGER, title = 'Alert') {
    BootstrapDialog.show({
      type: type,
      title: title,
      message: msg,
      buttons: [{
        label: 'OK',
        action: function (dialog) {
          dialog.close();
        }
      }]
    });
  };

  const loadChannels = function () {
    jQuery.getJSON("AdminAction.do?action=getOfflineDebuggerChannels")
      .done(function (json) {
        if (json.error) {
          showAlert(json.error);
          return;
        }
        renderChannels(json.channels);
      })
      .fail(function (jqxhr, textStatus, error) {
        const err = textStatus + ", " + error;
        showAlert("Request Failed: " + err);
      });
  };

  const renderChannels = function (channels) {
    const html = channels.map(x => `<option value='${x}'>${x}</option>`).join('');
    jQuery("#channels").html(html);
    jQuery("#channels, #connect-btn, #refresh-btn").prop("disabled", false);
  };

  let socket;

  const connect = function () {
    const channel = jQuery("#channels").val();
    if (!channel) {
      showAlert("Invalid channel!");
      return;
    }

    console.log("Opening connection ....");

    let url = window.location.href.replace("http","ws").replace("administrator/offline-debugger.do","websocket/offline-debugger");
    
    socket = new WebSocket(`${url}?channel=${channel}`);

    socket.onopen = function () {
      console.log('Websocket connected');
      onConnect();
    };

    socket.onerror = function (event) {
      showAlert("WebSocket error observed:", event);
    };
    
    socket.onclose = function (event) {
    	onDisconnect();
    };

    socket.onmessage = function (message) {
      modal.close();

      const data = message.data;
      if (!data.startsWith("{")) {
        showAlert(data);
        return;
      }

      const payload = JSON.parse(data);

      if (payload.error) {
        showAlert(payload.error);
        return;
      }

      if (payload.event === "action") {
        showAlert(payload.result || payload.action, BootstrapDialog.TYPE_SUCCESS, payload.action);
      } else if (payload.event === "sql") {
    	  
    	  let data = payload.data;
    	  
    	  if(data.error){
    		  showAlert(data.error);
    	      return;
    	  }
    	  
    	  renderDataTable(payload.data);
      }
      else if(payload.event == "email-logs" || payload.event == "email-db"){
    	  
    	  let data = payload.data;
    	  
    	  if(data.error){
    		  showAlert(data.error);
    	      return;
    	  }
    	  
    	  showAlert(JSON.stringify(data), BootstrapDialog.TYPE_SUCCESS, payload.event);
      }
      else
      {
    	  console.log(message);
      }
    };
  };

  const disconnect = function () {
    socket.close();
    console.log("Closing connection ....");
  };

  const onConnect = function () {
    jQuery("#channels, #refresh-btn").prop("disabled", true);
    jQuery("#connect-btn").hide();
    jQuery("#disconnect-btn, #actions-panel").show();
  };

  const onDisconnect = function () {
    jQuery("#channels, #refresh-btn").prop("disabled", false);
    jQuery("#connect-btn").show();
    jQuery("#disconnect-btn, #actions-panel").hide();
    loadChannels();
  };

  jQuery('#connect-btn').on('click', connect);

  jQuery('#disconnect-btn').on('click', disconnect);

  jQuery('#refresh-btn').on('click', loadChannels);

  /* action buttons */
  const sendAction = function (action) {
    socket.send(JSON.stringify({
      event: 'action',
      action: action
    }));
  };

  jQuery('#email-logs-btn').on('click', () => {
    sendAction('email-logs');
  });

  jQuery('#email-db-btn').on('click', () => {
    sendAction('email-db');
  });

  jQuery('#disconnect-client-btn').on('click', () => {
    sendAction('disconnect-client');
  });

  /* load channels */
  loadChannels();

  var queryLogs = [];
  const debuggerHistory = localStorage.debuggerHistory || '[]';
  queryLogs = JSON.parse(debuggerHistory);

  window.renderSqlHistory = function () {
    $('#sql-history-dropdown').html('<a class="dropdown-item" href="javascript:clearHistory()">Clear History</a>');
    for (let i = 0; i < queryLogs.length && i < 20; i++) {
      const index = queryLogs.length - (i + 1);
      const query = queryLogs[index];
      $('#sql-history-dropdown').append(`<a class="dropdown-item" href="javascript:loadHistory(${index})">${query.substr(0, 150)}</a>`);
    }
  };

  window.renderSqlHistory();

  window.loadHistory = function (index) {
    const query = queryLogs[index];
    $('#sqlTextField').val(query);
  };

  window.clearHistory = function () {
    BootstrapDialog.show({
      type: BootstrapDialog.TYPE_WARNING,
      title: 'Confirmation',
      message: "Do you want to clear sql history?",
      buttons: [{
        label: 'Cancel',
        action: function (dialog) {
          dialog.close();
        }
      },
      {
        label: 'Clear',
        action: function (dialog) {
          dialog.close();
          localStorage.debuggerHistory = [];
          $('#sql-history-dropdown').html("");
          window.renderSqlHistory();
        }
      }]
    });
  };

  $('#send_query_btn').on('click', function () {
    const query = $('#sqlTextField').val();
    const terminal_id = $('#terminal_id').val();

    if (query === "") {
      showAlert('Please enter a query!');
      return false;
    }

    queryLogs.push(query);
    $('#sql-history-dropdown').prepend(`<a class="dropdown-item" href="javascript:loadHistory(${queryLogs.length - 1})">${query.substr(0, 150)}</a>`);
    localStorage.debuggerHistory = JSON.stringify(queryLogs);

    socket.send(JSON.stringify({
      event: 'sql',
      query: query
    }));
  });

  $("#export-csv-button").on("click", function (e) {
    if (results !== null) {
      exportCSV();
    }
  });

  let results = null;
  let DT = null;

  function renderDataTable(json) {
    renderResultTab();
    results = json;
    const columns = json.columns;
    const names = columns.map(column => column.sTitle);

    const id = `#tab-table${count}`;
    table = $(id).DataTable({
      "data": json.data,
      "columns": columns,
      "scrollX": true,
      "drawCallback": function (settings) {
        // Handle draw callback if needed
      }
    });
    
    
    setTimeout(() => {
    	
    	console.log("Applying action ..");
    	
    	// Modify the cells that match the condition and make them clickable
    	table.cells().every(function() {
          var cell = this;

          // Check if the cell's content starts with "{" and ends with "}"
          var cellText = $(cell.node()).text().trim();
          if (cellText.startsWith("{") && cellText.endsWith("}")) {
            // Add a click event to the cell
            $(cell.node()).css('cursor', 'pointer'); // Optional: Change cursor to pointer
            $(cell.node()).click(function() {
              // Handle click event here
            	try {
	        		
	        		var json = JSON.parse(cellText);
		        	
		        	var $text = $('<div></div>');
			      	$text.height(600);
			          
			      	const editor = new JSONEditor($text.get(0), {});            
			        editor.set(json)
			          
			        BootstrapDialog.show({
			         	cssClass: 'view-json-dialog',
			            title: 'JSON Viewer',
			            message: $text
			        });
					
				} catch (e) {
					console.error(e);
				}
            });
          }
        });
		
	}, 1000);
	
  }

  let count = 0;

  function renderResultTab() {
    const nextTab = ++count;

    // create the tab
    $(`<li class="nav-item tab${nextTab}"><a class="nav-link" href="#tab${nextTab}" data-toggle="tab">Tab ${nextTab}
        <button type="button" class=" nav-link close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></a>
      </li>`).appendTo('#myTab');

    // create the tab content
    $(`<div class="tab-pane fade tab${nextTab}" id="tab${nextTab}"><table id="tab-table${nextTab}" class="display compact nowrap" style="width:100%"></table></div>`).appendTo('#myTabContent');

    // make the new tab active
    const tab = $('#myTab a:last');

    tab.on('shown.bs.tab', function (event) {
      if ($('#myTab a').length > 1) {
        $(`#tab-table${nextTab}`).DataTable().columns.adjust();
      }
    });

    tab.tab('show');    
  }

  $('#myTab').on('click', '.close', function () {
    const tabID = $(this).parents('a').attr('href').substr(1);
    $('.' + tabID).remove();
    $('#myTab a:last').tab('show');
  });

  function exportCSV() {
    const filename = "export.csv";
    const rows = [];
    rows.push(results.columns.map(column => column.title));
    rows.push(...results.data);

    const processRow = function (row) {
      let finalVal = '';
      for (let j = 0; j < row.length; j++) {
        let innerValue = row[j] === null ? '' : row[j].toString();
        if (row[j] instanceof Date) {
          innerValue = row[j].toLocaleString();
        }
        const result = innerValue.replace(/"/g, '""');
        if (result.search(/("|,|\n)/g) >= 0) {
          finalVal += '"' + result + '"';
        } else {
          finalVal += result;
        }
        if (j < row.length - 1) {
          finalVal += ',';
        }
      }
      return finalVal + '\n';
    };

    let csvFile = '';
    for (let i = 0; i < rows.length; i++) {
      csvFile += processRow(rows[i]);
    }

    const blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
    } else {
      const link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        const url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        link.style = "visibility:hidden";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
});
