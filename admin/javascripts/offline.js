jQuery(function(){
	
	var modal = new BootstrapDialog({
		title: 'Please wait',
	    message: 'Executing request ...',
	    closable: false
	});
			
	window.alert = function(msg){
		
		BootstrapDialog.show({
			type: BootstrapDialog.TYPE_DANGER,
	        title: 'Alert',
	        message: msg,
	        buttons: [{
	            label: 'OK',
	            action: function(dialog) {
	                dialog.close();
	            }
	        }]
	    });
	};
	
	function renderInfo(info){
		
		/* display info */
		
		var html = `Status:${info.status}, 
	   				Found:${info.found}, 
	   				Processed:${info.processed}, 
	   				Queued:${info.queued}, 
	   				Created:${info.created},  
	   				Failed:${info.failed},  
	   				Active:${info.active}, 
	   				Max-Queue:${info.maxQueue}, 
	   				Max-Thread:${info.maxThread},  
	   				Sleep:${ info.sleep }`;
		
		jQuery("#status_info").html(html);
	}
	
	function renderFailedOrders(json){
		
		window.failedOrders = json;
		
		var columns = [
			{"sTitle" : "Domain", "data" : 'domain'},
			{"sTitle" : "Created", "data" : "created"},
			{"sTitle" : "C_Order_ID", "data" : "c_order_id"},
			{"sTitle" : "Document No", "data" : "documentno"},
			{"sTitle" : "Error Msg", "data" : "error_msg"},
			{"sTitle" : "UUID", "data" : "uuid"},
			{"sTitle" : "", "data" : "json"}
		];
		
		var columnDefs = [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    return "<button class='btn btn-secondary' onclick='viewOrder(" + data + ")'>View Order</button>";
                },
                "targets": 6
            }
        ];
		
		if(window.DT){
			window.DT.destroy();
			$('#failed_order_table').empty();
		}
		
		window.DT = $('#failed_order_table').DataTable( {
	        "data": json.data,
	        "columnDefs": columnDefs,
	        "columns": columns,
	        "scrollX": true
	    } );  
	}
	
	function requestUrl(url, callback){
		
		modal.open();
		
		jQuery.get(url).always(function() {
			
			setInterval(function(){
				modal.close();
			}, 250)
			
			
		}).done(function( json ) {
			
			if(json.error){
				alert(json.reason);
				return;
			}
			
			if(callback){
				callback(json);
				return;
			}
			
			renderInfo(json);
		    
		}).fail(function( jqxhr, textStatus, error ) {
			
		    var err = textStatus + ", " + error;
		    alert( "Request Failed: " + err );
		    
		});
	}
	
	jQuery("#btn_status").on("click", function(){
		
		requestUrl("OfflineProcessingAction.do?action=status");
		
	});
	
	jQuery("#btn_start").on("click", function(){
		
		requestUrl("OfflineProcessingAction.do?action=start");
		
	});
	
	jQuery("#btn_stop").on("click", function(){
		
		requestUrl("OfflineProcessingAction.do?action=stop");
		
	});
	
	jQuery("#btn_wakeup").on("click", function(){
		
		requestUrl("OfflineProcessingAction.do?action=wakeup");
		
	});
	
	jQuery("#btn_set").on("click", function(){
		
		var maxQueue = jQuery.val("#maxQueue");
		var sleep = jQuery.val("#sleep");
		var maxThread = jQuery.val("#maxThread");
		
		requestUrl("OfflineProcessingAction.do?action=set&maxQueue=" + maxQueue + "&sleep=" + sleep + "&maxThread=" + maxThread);
		
	});
	
	jQuery("#btn_reprocess").on("click", function(){
		
		var date_from = jQuery("#date_from").val();
		var date_to = jQuery("#date_to").val();
		
		if(date_from == "" || date_to == ""){
			alert("Invalid date!");
			return;
		}
		
		requestUrl("OfflineProcessingAction.do?action=reprocessFailedOrders&date_from=" + date_from + "&date_to=" + date_to, function(json){
			
			if(json.count == 0){
				alert("No failed order found!");
				return;
			}
			
			alert(`Found ${json.count} orders. Stop and restart offline order processor to reprocess these updated orders. `);
			
		});
		
	});
	
	jQuery("#btn_load_failed_orders").on("click", function(){
		
		var date_from = jQuery("#date_from").val();
		var date_to = jQuery("#date_to").val();
		
		if(date_from == "" || date_to == ""){
			alert("Invalid date!");
			return;
		}
		
		requestUrl("OfflineProcessingAction.do?action=loadFailedOrders&date_from=" + date_from + "&date_to=" + date_to, function(results){
			
			renderFailedOrders(results);			
			
		});
		
	});
	
	jQuery("#btn_status").trigger( "click" );
	
});

function viewOrder(json){
	
	console.log(json);
	
	var $text = $('<div></div>');
	$text.height(600);
    
	const editor = new JSONEditor($text.get(0), {});            
    editor.set(json)
    
    BootstrapDialog.show({
    	cssClass: 'view-json-dialog',
        title: 'Order #' + json.uuid,
        message: $text
    });
}

