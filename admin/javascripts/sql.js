$(document).ready(function(){
	
	var modal = new BootstrapDialog({
		title: 'Please wait',
	    message: 'Executing query ...',
	    closable: false
	});
			
	window.alert = function(msg){
		
		BootstrapDialog.show({
			type: BootstrapDialog.TYPE_DANGER,
	        title: 'Alert',
	        message: msg,
	        buttons: [{
	            label: 'OK',
	            action: function(dialog) {
	                dialog.close();
	            }
	        }]
	    });
	};
	
	var queryLogs;
	
	window.renderSqlHistory = function(){		
		/* build query history dropdown */
		queryLogs = [];

		var queryHistory = localStorage.queryHistory || '[]';
		queryLogs = JSON.parse(queryHistory);
		
		$('#sql-history-dropdown').append('<a class="dropdown-item" href="javascript:clearHistory()">Clear History</a>');
			
		for(var i=0; i<queryLogs.length; i++){
			
			if(i == 20) break;
			
			var index = queryLogs.length - (i+1);
			
			var query = queryLogs[index];
			
			$('#sql-history-dropdown').append('<a class="dropdown-item" href="javascript:loadHistory(' + index + ')">' + query.substr(0,150) + '</a>');
		}
	};
	
	window.renderSqlHistory();
	
	window.loadHistory = function(index){
		
		var query = queryLogs[ index ];		
		window.editor.setValue(query);
	};
	
	window.clearHistory = function(){
		
		BootstrapDialog.show({
			type: BootstrapDialog.TYPE_WARNING,
	        title: 'Confirmation',
	        message: "Do you want to clear sql history?",
	        buttons: [{
	            label: 'Cancel',
	            action: function(dialog) {
	                dialog.close();
	            }
	        },
	        {
	            label: 'Clear',
	            action: function(dialog) {
	                dialog.close();
	                
	                localStorage.queryHistory = [];
	        		$('#sql-history-dropdown').html("");		
	        		window.renderSqlHistory();
	            }
	        }]
	    });		
		
	};
					
	$('#sqlForm').submit(function(){				
		
		var query = window.editor.getSelection() || window.editor.getValue();
		
		$('#sqlTextField').val(query);
		
		if(query == "")
		{
			alert('Please enter a query!');
			return false;
		}
		
		modal.open();
		
		//save query
		queryLogs.push(query);
		
		$('#sql-history-dropdown').prepend('<a class="dropdown-item" href="javascript:loadHistory(' + (queryLogs.length - 1) + ')">' + query.substr(0,150) + '</a>');
		
		localStorage.queryHistory = JSON.stringify(queryLogs);	
		
		jQuery.post("AdminAction.do?action=executeSql", { sql: query },null,"json").always(function() {
			
			setInterval(function(){
				modal.close();
			}, 500)
			
			
		}).done(function( json ) {
			
			if(json.error){
				alert(json.reason);
				return;
			}
			
			renderDataTable(json);
		    
		}).fail(function( jqxhr, textStatus, error ) {
			
		    var err = textStatus + ", " + error;
		    alert( "Request Failed: " + err );
		    
		});
		
		return false;
		
		});
	
	$("#export-csv-button").on("click", function(e){
		
		if(results != null){
			exportCSV();
		}
		
	});
	
	var results, DT = null;
	
	function renderDataTable( json ){
		
		renderResultTab();
		
		results = json;
		
		var columns = new Array();
		for(var i=0; i<results.columns.length; i++){
			columns.push({ "sTitle": results.columns[i] });
		}
		
		var names = [];
		columns.forEach( x => names.push(x.sTitle));
		
		var aSelected = [];
		
		var id = `#tab-table${count}`
		
		table = $(id).DataTable( {
	        "data": results.data,
	        "columns": columns,
	        "scrollX": true,
	        "drawCallback": function( settings ) {
	            
	        }
	    } ); 
		
		// Modify the cells that match the condition and make them clickable
		table.cells().every(function() {
	      var cell = this;

	      // Check if the cell's content starts with "{" and ends with "}"
	      var cellText = $(cell.node()).text().trim();
	      if (cellText.startsWith("{") && cellText.endsWith("}")) {
	        // Add a click event to the cell
	        $(cell.node()).css('cursor', 'pointer'); // Optional: Change cursor to pointer
	        $(cell.node()).click(function() {
	          // Handle click event here
	          
	        	try {
	        		
	        		var json = JSON.parse(cellText);
		        	
		        	var $text = $('<div></div>');
			      	$text.height(600);
			          
			      	const editor = new JSONEditor($text.get(0), {});            
			        editor.set(json)
			          
			        BootstrapDialog.show({
			         	cssClass: 'view-json-dialog',
			            title: 'JSON Viewer',
			            message: $text
			        });
					
				} catch (e) {
					console.error(e);
				}
	        });
	      }
	    });
		
	}
	
	var count = 0;
	
	function renderResultTab(){
		
		var nextTab = ++count;
		
	  	// create the tab
	  	$(`<li class="nav-item tab${nextTab}"><a class="nav-link" href="#tab${nextTab}" data-toggle="tab">Tab ${nextTab}
	  		<button type="button" class=" nav-link close" data-dismiss="alert" aria-label="Close">
	  			<span aria-hidden="true">&times;</span>
	  		</button></a>
	  	</li>`).appendTo('#myTab');
	  	
	  	// create the tab content
	  	$(`<div class="tab-pane fade tab${nextTab}" id="tab${nextTab}"><table id="tab-table${nextTab}" class="display compact nowrap" style="width:100%"></table></div>`).appendTo('#myTabContent');
	  	
	  	// make the new tab active
	  	var tab = $('#myTab a:last');
	  	
	  	tab.on('shown.bs.tab', function (event) {
	  	 
	  		if($('#myTab a').length > 1 ){
	  		  $(`#tab-table${nextTab}`).DataTable().columns.adjust();
	  	  }
	  	  
	  	});
	  	
	  	tab.tab('show');
	  	
	}
	
	$('#myTab').on('click', '.close', function() {
        var tabID = $(this).parents('a').attr('href').substr(1);
        $('.' + tabID).remove();

        $('#myTab a:last').tab('show');
    });
	
	function exportCSV(){
		
		var filename = "export.csv";
		var rows = [];
		// add header
		rows.push(results.columns);
		
		for(var i=0; i<results.data.length; i++){
			rows.push(results.data[i]);
		}						
		
		var processRow = function (row) {
	        var finalVal = '';
	        for (var j = 0; j < row.length; j++) {
	            var innerValue = row[j] === null ? '' : row[j].toString();
	            if (row[j] instanceof Date) {
	                innerValue = row[j].toLocaleString();
	            };
	            var result = innerValue.replace(/"/g, '""');
	            if (result.search(/("|,|\n)/g) >= 0)
	                result = '"' + result + '"';
	            if (j > 0)
	                finalVal += ',';
	            finalVal += result;
	        }
	        return finalVal + '\n';
	    };

	    var csvFile = '';
	    for (var i = 0; i < rows.length; i++) {
	        csvFile += processRow(rows[i]);
	    }

	    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
	    if (navigator.msSaveBlob) { // IE 10+
	        navigator.msSaveBlob(blob, filename);
	    } else {
	        var link = document.createElement("a");
	        if (link.download !== undefined) { // feature detection
	            // Browsers that support HTML5 download attribute
	            var url = URL.createObjectURL(blob);
	            link.setAttribute("href", url);
	            link.setAttribute("download", filename);
	            link.style = "visibility:hidden";
	            document.body.appendChild(link);
	            link.click();
	            document.body.removeChild(link);
	        }
	    }
	
	}
	
	/* code mirror */
	window.editor = CodeMirror.fromTextArea(document.getElementById('sqlTextField'), {
		mode: 'text/x-pgsql',
	    indentWithTabs: true,
	    smartIndent: true,
	    lineNumbers: true,
	    matchBrackets : true,
	    autofocus: true,
	    extraKeys: {"Ctrl-Space": "autocomplete"},
	    hintOptions: {
	    	tables: {
	    		ad_client : ["ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "value", "name", "description", "smtphost", "requestemail", "requestuser", "requestuserpw", "requestfolder", "ad_language", "ismultilingualdocument", "issmtpauthorization", "isusebetafunctions", "ldapquery", "modelvalidationclasses", "autoarchive", "mmpolicy", "emailtest", "isserveremail", "documentdir", "ispostimmediate", "iscostimmediate", "storeattachmentsonfilesystem", "windowsattachmentpath", "unixattachmentpath", "storearchiveonfilesystem", "windowsarchivepath", "unixarchivepath", "isuseasp", "domain", "u_pos_type_id", "autodocumentno", "istranslationenabled"],
	      		ad_org : ["ad_org_id", "ad_client_id", "isactive", "created", "createdby", "updated", "updatedby", "value", "name", "description", "issummary"],
	      		ad_user : ["ad_user_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "name", "description", "password", "email", "supervisor_id", "c_bpartner_id", "processing", "emailuser", "emailuserpw", "c_bpartner_location_id", "c_greeting_id", "title", "comments", "phone", "phone2", "fax", "lastcontact", "lastresult", "birthday", "ad_orgtrx_id", "emailverify", "emailverifydate", "notificationtype", "isfullbpaccess", "c_job_id", "ldapuser", "connectionprofile", "value", "userpin", "useruuid", "isfirsttimelogin", "averagetargetperday", "averagetargetperweek", "averagetargetpermonth", "emailgroups"],
	      		ad_role : ["ad_role_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "name", "updatedby", "description", "userlevel", "c_currency_id", "amtapproval", "ad_tree_menu_id", "ismanual", "isshowacct", "ispersonallock", "ispersonalaccess", "iscanexport", "iscanreport", "supervisor_id", "iscanapproveowndoc", "isaccessallorgs", "ischangelog", "preferencetype", "overwritepricelimit", "isuseuserorgaccess", "ad_tree_org_id", "confirmqueryrecords", "maxqueryrecords", "connectionprofile", "allow_info_account", "allow_info_asset", "allow_info_bpartner", "allow_info_cashjournal", "allow_info_inout", "allow_info_invoice", "allow_info_order", "allow_info_payment", "allow_info_product", "allow_info_resource", "allow_info_schedule", "userdiscount", "isdiscountuptolimitprice", "isdiscountallowedontotal", "allow_order_backdate", "allow_change_terminal", "allow_order_refund", "allow_order_exchange", "allow_order_void", "allow_complete_inventorycount", "allow_upsell", "allow_order_split", "allow_payment_void", "allow_stocktransfer_void", "enablesignupverification", "allow_inventory_void", "editattendance", "saveorders", "discountoncurrentprice", "viewpurchaseprice", "savestocktransfer", "allow_pick_item", "allow_place_item", "allow_move_item", "allow_cycle_count", "allow_view_stock", "allow_reorder_level", "allow_complete_send_stock", "allow_reset_mappings", "allow_update_price"],
	      		ad_user_roles : ["ad_user_id", "ad_role_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby"],
	      		u_posterminal : ["ad_client_id", "ad_org_id", "autolock", "card_bankaccount_id", "cardtransferbankaccount_id", "cardtransfercashbook_id", "cardtransfertype", "cashbooktransfertype", "cashtransferbankaccount_id", "cashtransfercashbook_id", "c_cashbook_id", "c_cashbpartner_id", "check_bankaccount_id", "checktransferbankaccount_id", "checktransfercashbook_id", "checktransfertype", "created", "createdby", "c_templatebpartner_id", "description", "help", "isactive", "lastlocktime", "locked", "locktime", "m_warehouse_id", "name", "po_pricelist_id", "printername", "salesrep_id", "so_pricelist_id", "unlockingtime", "updated", "updatedby", "u_posterminal_id", "timezone", "c_tax_id", "voucher_bankaccount_id", "po_tax_id", "islive", "sequence_no", "sequence_prefix", "isdefault", "offline", "pos_version"],
	      		c_tax : ["c_tax_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "name", "updatedby", "description", "taxindicator", "isdocumentlevel", "validfrom", "issummary", "requirestaxcertificate", "rate", "parent_tax_id", "c_country_id", "c_region_id", "to_country_id", "to_region_id", "c_taxcategory_id", "isdefault", "istaxexempt", "sopotype", "issalestax"],
	      		c_taxcategory : ["c_taxcategory_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "name", "description", "commoditycode", "isdefault"],
	      		c_uom : ["c_uom_id", "ad_client_id", "ad_org_id", "isactive", "created", "updated", "createdby", "updatedby", "x12de355", "uomsymbol", "name", "description", "stdprecision", "costingprecision", "isdefault"],
	      		m_productprice : ["m_pricelist_version_id", "m_product_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "pricelist", "pricestd", "pricelimit"],
	      		m_pricelist_version : ["m_pricelist_version_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "name", "description", "m_pricelist_id", "m_discountschema_id", "validfrom", "proccreate", "m_pricelist_version_base_id"],
	      		m_pricelist : ["m_pricelist_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "name", "description", "basepricelist_id", "istaxincluded", "issopricelist", "isdefault", "c_currency_id", "enforcepricelimit", "priceprecision", "ismandatory", "ispresentforproduct"],
	      		m_product : ["m_product_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "value", "name", "description", "documentnote", "help", "upc", "sku", "c_uom_id", "salesrep_id", "issummary", "isstocked", "ispurchased", "issold", "isbom", "isinvoiceprintdetails", "ispicklistprintdetails", "isverified", "c_revenuerecognition_id", "m_product_category_id", "classification", "volume", "weight", "shelfwidth", "shelfheight", "shelfdepth", "unitsperpallet", "c_taxcategory_id", "s_resource_id", "discontinued", "discontinuedby", "processing", "s_expensetype_id", "producttype", "imageurl", "descriptionurl", "guaranteedays", "r_mailtext_id", "versionno", "m_attributeset_id", "m_attributesetinstance_id", "downloadurl", "m_freightcategory_id", "m_locator_id", "guaranteedaysmin", "iswebstorefeatured", "isselfservice", "c_subscriptiontype_id", "isdropship", "isexcludeautodelivery", "group1", "group2", "unitsperpack", "primarygroup", "group3", "group4", "group5", "group6", "group7", "group8", "m_producttemplate_id", "editdesconfly", "editpriceonfly", "istemplate", "extendeddescription", "productsummary", "variant1", "variant2", "variant3", "variant1name", "variant2name", "variant3name", "isserialno", "m_product_parent_id", "serialno", "ismodifier", "ismodifiable", "isgift", "loyaltypoints", "isparent", "isageverified", "productimageurl", "iscoupon", "ispromotion", "u_pos_discountcode_id", "discountcodes", "po_description", "cost", "isdeposit"],
	      		c_order : ["c_order_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "issotrx", "documentno", "docstatus", "docaction", "processing", "processed", "c_doctype_id", "c_doctypetarget_id", "description", "isapproved", "iscreditapproved", "isdelivered", "isinvoiced", "isprinted", "istransferred", "isselected", "salesrep_id", "dateordered", "datepromised", "dateprinted", "dateacct", "c_bpartner_id", "c_bpartner_location_id", "poreference", "isdiscountprinted", "c_currency_id", "paymentrule", "c_paymentterm_id", "invoicerule", "deliveryrule", "freightcostrule", "freightamt", "deliveryviarule", "m_shipper_id", "c_charge_id", "chargeamt", "priorityrule", "totallines", "grandtotal", "m_warehouse_id", "m_pricelist_id", "istaxincluded", "c_campaign_id", "c_project_id", "c_activity_id", "posted", "c_payment_id", "c_cashline_id", "sendemail", "ad_user_id", "copyfrom", "isselfservice", "ad_orgtrx_id", "user1_id", "user2_id", "c_conversiontype_id", "bill_bpartner_id", "bill_location_id", "bill_user_id", "pay_bpartner_id", "pay_location_id", "ref_order_id", "isdropship", "volume", "weight", "ordertype", "c_pos_id", "amounttendered", "amountrefunded", "discountamt", "referenceno", "scheduleddeliverydate", "voucheramtconsumed", "uuid", "offlinedocumentno", "costtotal", "taxtotal", "discounttotal", "nooflines", "loyaltypoints", "userdiscounttotal", "referencename", "agent"],
	      		c_orderline : ["c_orderline_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "c_order_id", "line", "c_bpartner_id", "c_bpartner_location_id", "dateordered", "datepromised", "datedelivered", "dateinvoiced", "description", "m_product_id", "m_warehouse_id", "c_uom_id", "qtyordered", "qtyreserved", "qtydelivered", "qtyinvoiced", "m_shipper_id", "c_currency_id", "pricelist", "priceactual", "pricelimit", "linenetamt", "discount", "freightamt", "c_charge_id", "c_tax_id", "s_resourceassignment_id", "ref_orderline_id", "m_attributesetinstance_id", "isdescription", "processed", "qtyentered", "priceentered", "c_project_id", "pricecost", "qtylostsales", "c_projectphase_id", "c_projecttask_id", "rrstartdate", "rramt", "c_campaign_id", "c_activity_id", "user1_id", "user2_id", "ad_orgtrx_id", "salesstaffcode", "qtyreturned", "bom_ref_orderline_id", "modifier_ref_orderline_id", "u_modifier_id", "discountamt", "linenettaxamt", "pricestd", "u_pos_discountcode_id", "box"],
	      		c_invoice : ["c_invoice_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "issotrx", "documentno", "docstatus", "docaction", "processing", "processed", "posted", "c_doctype_id", "c_doctypetarget_id", "c_order_id", "description", "isapproved", "istransferred", "isprinted", "salesrep_id", "dateinvoiced", "dateprinted", "dateacct", "c_bpartner_id", "c_bpartner_location_id", "poreference", "isdiscountprinted", "dateordered", "c_currency_id", "paymentrule", "c_paymentterm_id", "c_charge_id", "chargeamt", "totallines", "grandtotal", "m_pricelist_id", "istaxincluded", "c_campaign_id", "c_project_id", "c_activity_id", "ispaid", "c_payment_id", "c_cashline_id", "createfrom", "generateto", "sendemail", "ad_user_id", "copyfrom", "isselfservice", "ad_orgtrx_id", "user1_id", "user2_id", "c_conversiontype_id", "ispayschedulevalid", "ref_invoice_id", "isindispute", "invoicecollectiontype", "m_rma_id", "dunninggrace", "c_dunninglevel_id", "discountamt"],
	      		c_invoiceline : ["c_invoiceline_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "c_invoice_id", "c_orderline_id", "m_inoutline_id", "line", "description", "m_product_id", "qtyinvoiced", "pricelist", "priceactual", "pricelimit", "linenetamt", "c_charge_id", "c_uom_id", "c_tax_id", "s_resourceassignment_id", "a_asset_id", "taxamt", "m_attributesetinstance_id", "isdescription", "isprinted", "linetotalamt", "ref_invoiceline_id", "processed", "qtyentered", "priceentered", "c_project_id", "c_projectphase_id", "c_projecttask_id", "rrstartdate", "rramt", "c_campaign_id", "c_activity_id", "user1_id", "user2_id", "ad_orgtrx_id", "m_rmaline_id"],
	      		c_payment :  ["c_payment_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "documentno", "datetrx", "isreceipt", "c_doctype_id", "trxtype", "c_bankaccount_id", "c_bpartner_id", "c_invoice_id", "c_bp_bankaccount_id", "c_paymentbatch_id", "tendertype", "creditcardtype", "creditcardnumber", "creditcardvv", "creditcardexpmm", "creditcardexpyy", "micr", "routingno", "accountno", "checkno", "a_name", "a_street", "a_city", "a_state", "a_zip", "a_ident_dl", "a_ident_ssn", "a_email", "voiceauthcode", "orig_trxid", "ponum", "c_currency_id", "payamt", "discountamt", "writeoffamt", "taxamt", "isapproved", "r_pnref", "r_result", "r_respmsg", "r_authcode", "r_avsaddr", "r_avszip", "r_info", "processing", "oprocessing", "docstatus", "docaction", "isreconciled", "isallocated", "isonline", "processed", "posted", "isoverunderpayment", "overunderamt", "a_country", "c_project_id", "isselfservice", "chargeamt", "c_charge_id", "isdelayedcapture", "r_authcode_dc", "r_cvv2match", "r_pnref_dc", "ad_orgtrx_id", "c_campaign_id", "c_activity_id", "user1_id", "user2_id", "c_conversiontype_id", "description", "dateacct", "c_order_id", "isprepayment", "ref_payment_id", "c_cashbook_id", "c_pos_id", "swipe", "trxid", "r_creditcardbalance", "r_processdata", "r_acqrefdata", "r_approvalamt", "giftcardno", "giftcardswipe", "giftcardcvv", "ise2e", "signature", "u_coupon_id", "voucher_id", "tipamt"],
	      		c_allocationhdr : ["c_allocationhdr_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "documentno", "description", "datetrx", "dateacct", "c_currency_id", "approvalamt", "ismanual", "docstatus", "docaction", "isapproved", "processing", "processed", "posted"],
	      		c_allocationline : ["c_allocationline_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "allocationno", "datetrx", "ismanual", "c_invoice_id", "c_bpartner_id", "c_order_id", "c_payment_id", "c_cashline_id", "amount", "discountamt", "writeoffamt", "posted", "overunderamt", "c_allocationhdr_id"],
	      		m_warehouse : ["m_warehouse_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "value", "name", "description", "c_location_id", "separator", "m_warehousesource_id", "replenishmentclass"],
	      		m_storage : ["m_product_id", "m_locator_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "qtyonhand", "qtyreserved", "qtyordered", "datelastinventory", "m_attributesetinstance_id", "m_movementline_id", "c_orderline_id"],
	      		m_transaction : ["m_transaction_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "movementtype", "m_locator_id", "m_product_id", "movementdate", "movementqty", "m_inventoryline_id", "m_movementline_id", "m_inoutline_id", "m_productionline_id", "c_projectissue_id", "m_attributesetinstance_id", "u_productconversionline_id"],
	      		o_order : ["id", "uuid", "json", "status", "error_msg", "ad_client_id", "c_order_id", "created", "updated", "documentno"],
	      		m_inout : ["m_inout_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "issotrx", "documentno", "docaction", "docstatus", "posted", "processing", "processed", "c_doctype_id", "description", "c_order_id", "dateordered", "isprinted", "movementtype", "movementdate", "dateacct", "c_bpartner_id", "c_bpartner_location_id", "m_warehouse_id", "poreference", "deliveryrule", "freightcostrule", "freightamt", "deliveryviarule", "m_shipper_id", "c_charge_id", "chargeamt", "priorityrule", "dateprinted", "c_invoice_id", "createfrom", "generateto", "sendemail", "ad_user_id", "salesrep_id", "nopackages", "pickdate", "shipdate", "trackingno", "ad_orgtrx_id", "c_project_id", "c_campaign_id", "c_activity_id", "user1_id", "user2_id", "datereceived", "isintransit", "ref_inout_id", "createconfirm", "createpackage", "isapproved", "isindispute", "volume", "weight", "m_rma_id"],
	      		m_inoutline : ["m_inoutline_id", "ad_client_id", "ad_org_id", "isactive", "created", "createdby", "updated", "updatedby", "line", "description", "m_inout_id", "c_orderline_id", "m_locator_id", "m_product_id", "c_uom_id", "movementqty", "isinvoiced", "m_attributesetinstance_id", "isdescription", "confirmedqty", "pickedqty", "scrappedqty", "targetqty", "ref_inoutline_id", "processed", "qtyentered", "c_charge_id", "c_project_id", "c_projectphase_id", "c_projecttask_id", "c_campaign_id", "c_activity_id", "user1_id", "user2_id", "ad_orgtrx_id", "m_rmaline_id", "box", "reservestock"],
	      		c_cash : ["c_cash_id","ad_client_id","ad_org_id","isactive","created","createdby","updated","updatedby","c_cashbook_id","name","description","statementdate","dateacct","beginningbalance","endingbalance","statementdifference","processing","processed","posted","ad_orgtrx_id","c_project_id","c_campaign_id","c_activity_id","user1_id","user2_id","isapproved","docstatus","docaction","totalcashamount","cardamount","chequeamount","transferamount","cashdifference","closetill","floatamount","cardamountentered","chequeamountentered","cashsales","changepaymenttype","iscardamountdifferent","ischequeamountdifferent","u_posterminal_id","opendate","closedate","voucheramt","externalcreditcardamt","externalcreditcardamtentered","voucheramtentered","voucherdifference","externalcreditcarddifferent","giftcardamt","skwalletamt","zapperamt","uuid","loyaltyamt","json","couponamt","posteritagiftcardamt","externalcardamountentered","externalcarddifference","mcbjuiceamt","mytmoneyamt","emtelmoneyamt","depositamt"],
	      		c_bpartner : ["c_bpartner_id","ad_client_id","ad_org_id","isactive","created","createdby","updated","updatedby","value","name","name2","description","issummary","c_bp_group_id","isonetime","isprospect","isvendor","iscustomer","isemployee","issalesrep","referenceno","duns","url","ad_language","taxid","istaxexempt","c_invoiceschedule_id","rating","salesvolume","numberemployees","naics","firstsale","acqusitioncost","potentiallifetimevalue","actuallifetimevalue","shareofcustomer","paymentrule","so_creditlimit","so_creditused","c_paymentterm_id","m_pricelist_id","m_discountschema_id","c_dunning_id","isdiscountprinted","so_description","poreference","paymentrulepo","po_pricelist_id","po_discountschema_id","po_paymentterm_id","documentcopies","c_greeting_id","invoicerule","deliveryrule","freightcostrule","deliveryviarule","salesrep_id","sendemail","bpartner_parent_id","invoice_printformat_id","socreditstatus","shelflifeminpct","ad_orgbp_id","flatdiscount","totalopenbalance","dunninggrace","company","identifier","emailpromotion","emailreceipt","custom1","custom2","custom3","custom4","gender","loyaltypoints","enableloyalty","loyaltypointsearned","loyaltypointsspent","completedloyaltyform","customerimageurl","loyaltystartingpoints","title","notes","u_cms_customer_id","u_pos_discountcode_id","discountcode_expiry"],
	      		m_locator : ["m_locator_id","ad_client_id","ad_org_id","isactive","created","createdby","updated","updatedby","value","m_warehouse_id","priorityno","isdefault","x","y","z"]		      		
	    	}
	    }
	  });
	
});
