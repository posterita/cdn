BackOffice.ID_COLUMN_NAME = "m_product_id";
BackOffice.defaults = {
		
			};

/* override default export function */
BackOffice.exportToCsv = function(f){
	window.location = "DataTableAction.do?action=exportArchivedItems";
};

BackOffice.requestDataTableData = function(){	
	jQuery.post("DataTableAction.do", { action: "getArchivedItems"},
			function(json, textStatus, jqXHR){	
		
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var archivedItems = json["archivedItems"];
				
				BackOffice["archivedItems"] = archivedItems;
				
				//set data source		
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = archivedItems;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.create();
					}
					else
					{
						BackOffice.datasource = json.data;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
					}
				}
				else
				{
					BackOffice.setDataSource(archivedItems);
					BackOffice.renderDataTable();
					jQuery('.backoffice-create-btn').hide();
				}				
				
			},"json").fail(function(){
				alert("Failed to request data!");
			});
};

BackOffice.afterSave = function(model){
	
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	if(datasource.length == 0)
	{
		jQuery('.master').hide();
		jQuery('.no-data').show();
		//jQuery('#main-body-container #detail').show();
		jQuery('.backoffice-create-btn').hide();
 	}
	else
	{
		jQuery('#example').show();
		jQuery('.no-data').hide();
		jQuery('.master').show();
		//jQuery('#main-body-container #detail').show();
		jQuery('.backoffice-create-btn').hide();
	}
	
	if (datasource.length > 0)
	{
		BackOffice.dt = jQuery('#example').dataTable({
		    "data": datasource,
		    "columns": [
		  	      { "data": "name" },
			      { "data": "description"}, 
			      { "data": "discontinued"},
			      { "data": "upc"},
			      { "data": "isactive"},
			      { "data": "m_product_id"}
	  	    ],
		    "columnDefs": [
		      {
		        "render": function (data, type, row) {
		          return '<button type="button" class="btn btn-default" title="Remove from archive" onclick="unarchiveItem(' + data + ','+ row['m_product_id'] + ')"><span class="glyphicon glyphicon-circle-arrow-up"></span></button>';
		        },
		        "targets": 5
		      }	,
		      {
		    	  "render": function (data, type, row) {
			        	 var value = row["isactive"];
			          
						 if (value == "Y")
						 {
							  value = "Yes";
						 }
						 else
						 {
							 value = "No";
						 }
						 
						 return value;
			        },
			     "targets": 4
		      },
		      {
		    	  "data" : "upc",
		    		  "mRender" : function(data, type, row){
		    			  if(row[3] !=null){
							return row[3];
							}
							else
							{
								return '';
							}
		    		  },
		      },
		    ],
		    /*"scrollX" : true*/	
		
			/*,
		    "language" : {
		    	//"url" : "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
		    	paginate: {
		    		first: "Premier",
		    		next : "Suivant"
		    	},
		    }*/
		});
	}
}

function unarchiveItem(id){
	
	url = "DataTableAction.do?action=unarchiveItem";
	
	var postData = {json : Object.toJSON({"m_product_id" : id})};		
	
	jQuery.post(url,
			postData,
    		function(json, textStatus, jqXHR){
		
				BackOffice.displayMessage(json.success);
				BackOffice.renderDataTable();
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('autentication failed');
    				BackOffice.displayError('Failed!');
    				return;
    			}
    			
    			if(json.error){
    				console.error(json.error);
    				BackOffice.displayError("Failed! " + json.error);
    				return;
    			}
    			
    			if(json.success)
    			{
    				BackOffice.renderDataTable();
    				BackOffice.requestDataTableData();
    			}
    		},
			"json").fail(function(){
				console.error('request failed');
				BackOffice.displayError(json.error);
			});
}
