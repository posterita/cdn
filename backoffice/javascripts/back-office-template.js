var BackOfficeMenu = {};

jQuery(document).ready(function() {
	resizeContainerWidth();
	resizeContainerHeight();
	resetStarPosOfMainBody(10);
	
	jQuery(window).resize(function(){
		resizeContainerWidth();/*
		resizeContainerHeight();*/
		resetStarPosOfMainBody(10);
	});
	
	jQuery("#toggle-sidebar-visibilty-btn").on("click", function(){
		toggleSidebarVisibility();
	});
});

function resizeContainerWidth()
{
	var width = jQuery("#backoffice-template-main-content").width();
	
	jQuery("#backoffice-template-message-container").width(width);
	jQuery("#backoffice-template-breadcrumb-container").width(width);
}

function resizeContainerHeight()
{
	jQuery("#backoffice-template-left-sidebar").height("auto");
	
	var height = jQuery(document).height();
	jQuery("#backoffice-template-left-sidebar").height(height);
	
	/*var height = jQuery("#backoffice-template-container").height();
	var windowHeight= jQuery("html").height();
	
	if(windowHeight > height)
	{
		jQuery("#backoffice-template-left-sidebar").height(windowHeight);
	}
	else
	{
		jQuery("#backoffice-template-left-sidebar").height(height);
	}*/
}

function toggleSidebarVisibility()
{
	var sidebar = jQuery("#backoffice-template-left-sidebar");
	var mainContent = jQuery("#backoffice-template-main-content");
	/*var mainBodyContent = jQuery("#main-body-container");*/
	
	var toggleBtn = jQuery("#toggle-sidebar-visibilty-btn");
	var isVisible = toggleBtn.hasClass("glyphicons-circle-arrow-left");
	
	var menus = jQuery("#backofice-template-menu-container a");
	
	if (isVisible)
	{
		jQuery("#backofice-template-company-details").width("50px");
		
		sidebar.width("50px");
		mainContent.css("margin-left", "50px");
		/*mainBodyContent.css("left", "50px");*/
		
		toggleBtn.removeClass("glyphicons-circle-arrow-left").addClass("glyphicons-circle-arrow-right");
		
		jQuery("#company-details").hide();
		/*jQuery("#company-details").addClass("tooltip-company-details");*/
		menus.addClass("hover-menu");
	}
	else
	{
		jQuery("#backofice-template-company-details").width("200px");
		
		sidebar.width("200px");
		mainContent.css("margin-left", "200px");
		/*mainBodyContent.css("left", "200px");*/
		
		toggleBtn.removeClass("glyphicons-circle-arrow-right").addClass("glyphicons-circle-arrow-left");
		
		jQuery("#company-details").show();
		/*jQuery("#company-details").removeClass("tooltip-company-details");*/
		menus.removeClass("hover-menu");
	}
	
	resizeContainerWidth();
}

function closeAlert(box)
{
	jQuery(box).hide();
	
	resetStarPosOfMainBody(10);
}

function resetStarPosOfMainBody(offset)
{
	var messageHeight = jQuery("#backoffice-template-message-container").height();
	var mainBody = jQuery("#main-body-container");
	
	if (messageHeight > 0)
	{
		mainBody.css("padding-top", (messageHeight + offset));
	}
	else
	{
		mainBody.css("padding-top", offset);
	}
}