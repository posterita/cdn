var BackOffice = {};
BackOffice.ID_COLUMN_NAME = null;
BackOffice.AD_TABLE_ID = 0;

BackOffice.create = function(){	
	BackOffice.model = defaults;
	
	var form = jQuery("#form");
	/*reset form*/
	form.get(0).reset();
	
	var defaults = this.defaults;
	DataTable.utils.mapModelToForm(defaults, form);	
	this.showDetail();
	BackOffice.showDefaultCountry();
	BackOffice.defaultTaxCategory();
	
	if(BackOffice.model == undefined)
	{
		BackOffice.model = defaults;
	}
	
	BackOffice.onCreate();
	
};	
	
BackOffice.cancel = function(){
	
	if( this.model == undefined )
	{
		this.showMaster();
		
		this.initViewActionButtons();
		
		return;
	}
	
	if ((jQuery('#view-details').length) && (jQuery('#detail').is(':visible') && BackOffice.model[BackOffice.ID_COLUMN_NAME] > 0)) {
		this.showView();
	} else {
		this.showMaster();
	}
	
	this.initViewActionButtons();
};

BackOffice.afterCancel = function(){
	
};

BackOffice.showDefaultCountry = function(){
	
};

BackOffice.defaultTaxCategory = function(){
	
};

BackOffice.edit = function(id)
{
	/*show main tab*/
	
	if (jQuery('#tabs a[href="#tab1"]'))
	{
		jQuery('#tabs a[href="#tab1"]').click();
	}
	
	/* reset form validation */
	jQuery("#form").get(0).reset();
	
	var db = this.db;
	var query = {};
	query[this.ID_COLUMN_NAME] = {'==':id};
	
	var model = db(query).first(); /* return first result */	
	this.model = model;
	
	this.initializeForm();
	
	DataTable.utils.mapModelToForm(model, jQuery("#form"));	
	this.showDetail();
	this.showActionButtons();
	
	var isActive = this.model.isactive == 'Y' ? true : false;
	
	if (isActive)
	{
		BackOffice.renderAccordions();
		enableFormElements();
	}
	else
	{
		BackOffice.disableAccordions();
		disableFormElements();
	}
	
	
	
};

BackOffice.view = function(id){
	var model = BackOffice["model"];
	
	if (model == undefined || model[BackOffice.ID_COLUMN_NAME] != id)
	{
		var db = this.db;
		var query = {};
		query[this.ID_COLUMN_NAME] = {'==':id};
		
		var model = db(query).first(); /* return first result */	
		this.model = model;
	}
	
	this.showView();
};

BackOffice.initializeForm = function(){
	
};

BackOffice.showActionButtons = function(){
	
	var model = BackOffice["model"];
	var isActive = model.isactive == 'Y' ? true : false;
	
	if(isActive)
	{
		jQuery(".backoffice-btn-activate").each(function(index, button){
			
			button.hide();
		});
		
		jQuery(".backoffice-btn-deactivate").each(function(index, button){
			
			button.show();
		});
	}
	else
	{
		jQuery(".backoffice-btn-activate").each(function(index, button){
			
			button.show();
		});
		
		jQuery(".backoffice-btn-deactivate").each(function(index, button){
			
			button.hide();
		});
	}
	
	
};

BackOffice.showMaster = function(){
	this.clearMessage();
	
	/* reset model */
	this.model = null;
	
	/* reset accordions */
	jQuery("div.accordion div.section-inner-left, div.accordion div.section-inner-right").hide();
	
	jQuery('.no-data').hide();
	jQuery('.detail').hide();
	jQuery('.master').show();
	jQuery('.view').hide();
	
	BackOffice.renderDataTable();
	
	var height = jQuery("html").height();
	jQuery("#left-panel").height(height);
	
	resizeContainerHeight()
	
	window.scrollTo(0,0);
};

/* call when detail pane is shown */
BackOffice.onShowDetail = function(){
	
		jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});	
		
	
};

BackOffice.afterShowDetail = function(){};

BackOffice.showDetail = function(){
	
	this.onShowDetail();
	this.clearMessage();
	this.initActionButtons();
	enableFormElements();
	
	jQuery('.no-data').hide();
	jQuery('.master').hide();
	jQuery('.detail').show();
	jQuery('.view').hide();
	
	var height = jQuery("html").height();
	jQuery("#left-panel").height(height);
	
	resizeContainerHeight();
	
	jQuery(".section.accordion").on("click",function(){
		var height = jQuery("html").height();
		jQuery("#left-panel").height(height);
		
	});
	

	window.scrollTo(0,0);
	
	this.afterShowDetail();
	
};

BackOffice.onShowView = function() {
	this.renderView();
	
	this.afterShowView();
}

BackOffice.renderView = function() {
	
	var source   = jQuery("#view-template").html();
	var template = Handlebars.compile(source);
	
	jQuery("#view-details").html(template(this.model));
	
	BackOffice.afterRenderView(this.model[BackOffice.ID_COLUMN_NAME]);
};

BackOffice.afterRenderView = function(id) {};

BackOffice.renderOtherViews = function(templateId, viewId, data)
{
	var source   = jQuery(templateId).html();
	var template = Handlebars.compile(source);
	
	jQuery(viewId).html(template(data));
};

BackOffice.afterShowView = function() {
	resizeContainerHeight();
	
	window.scrollTo(0,0);
};

BackOffice.showView = function(){
	this.clearMessage();
	this.initActionButtons();
	
	jQuery('.no-data').hide();
	jQuery('.master').hide();
	jQuery('.detail').hide();
	jQuery('.view').show();
	
	jQuery(".backoffice-btn-edit").on("click", function(){
		var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
		BackOffice.edit(id);
	});
	
	this.onShowView();
};

BackOffice.displayError = function (error){
	var container = jQuery(".backoffice-message-container");	
	container.html('<div class="backoffice-alert backoffice-alert-danger"><i class="fa fa-times"></i>&nbsp;'+ error +'<button type="button" class="close" onclick="">&times;</button></div>');
	
	resetStarPosOfMainBody(10);
	
	BackOffice.closeAlert();
	
	jQuery("body").get(0).scrollTop = 0;
	
	BackOffice.preloader.hide();
	
	/* show error tab */
	var ele = jQuery(".help-block.form-error").first();
	if(ele){
		var id = ele.parents('.ui-tabs-panel').attr("id");
		jQuery('a[href="#' + id + '"]').trigger("click");
	}
	
};

BackOffice.displayMessage = function(success){
	var container = jQuery(".backoffice-message-container");
	container.html('<div class="backoffice-alert backoffice-alert-success"><i class="fa fa-check"></i>&nbsp;'+ success +'<button type="button" class="close" onclick="">&times;</button></div>');
	
	resetStarPosOfMainBody(10);
	
	BackOffice.closeAlert();
	
	jQuery("body").get(0).scrollTop = 0;
	
	BackOffice.preloader.hide();
};

BackOffice.clearMessage = function(){
	jQuery(".backoffice-message-container").empty();
	
	resetStarPosOfMainBody(10);
};

BackOffice.closeAlert = function(){
	jQuery(".backoffice-message-container").on("click", function(e){
		BackOffice.clearMessage();
	});
};

BackOffice.afterCloseAlert = function() { };

BackOffice.initViewActionButtons = function(){ 
	jQuery("#view-action-buttons").hide();
};

/**
 * This function is called on page load.
 */
BackOffice.requestDataTableData = function(){
	/* to be overrided */
	alert("requestDataTableData function not implemented");
};

BackOffice.renderDataTable = function(){
	/* to be overrided */
	alert("renderDataTable function not implemented");
};

/**
 * Save function is called when element with class .
 */
BackOffice.beforeSave = function(model){};
BackOffice.afterSave = function(model){};

BackOffice.validateBeforeSave = function(model){
	return true;
};

BackOffice.save = function(form)
{
	/* add custom validation */
	if( BackOffice.validateBeforeSave( BackOffice["model"] ) == false ){
		
		return;		
	}
	
	
	var isActive = 'Y';
	
	if (BackOffice["model"] != undefined)
	{
		isActive = BackOffice["model"]["isactive"];
	}
	
	var defaults = BackOffice.defaults;
	var url = BackOffice.SAVE_URL;
	
	var s = jQuery(form).serialize();
	var model = jQuery.extend({}, defaults);
	model["modelName"] = BackOffice.MODEL_NAME;
	model["columnIdName"] = BackOffice.ID_COLUMN_NAME;
	model["isactive"] = isActive;
	
	DataTable.utils.mapFormToModel(form, model);
	
	/* add any dynamically generate data to model */
	try
	{
		this.beforeSave(model);
	}
	catch(e){
		BackOffice.displayError('Oops .. js error! ' + e.message);
		console.error(e);
		return;
	}
	
	
	var postData = {json : Object.toJSON(model)};
	
	var db = BackOffice.db;
	var datasource = BackOffice.datasource;
	
	BackOffice.preloader.show();
	
	jQuery.post(url,
			postData,
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('autentication failed');
    				BackOffice.displayError('Failed to save: session timed out!');
    				return;
    			}
    			
    			if(json.error){
    				console.error(json.error);
    				BackOffice.displayError(json.error);
    				return;
    			}
    			else
    			{
    				var id = json[BackOffice.ID_COLUMN_NAME];	
        			
        			var query = {};
        			query[BackOffice.ID_COLUMN_NAME] = {'==':id};
        			
        			var records = db(query).get();
        			
        			if(records.length == 0){
        				db.insert(json)
        			}
        			else
        			{
        				db(query).update(json);
        			}
        			
        			datasource = db().get();    			
        			BackOffice.setDataSource(datasource);
        			
        			//BackOffice.datasource = json;	
        			BackOffice.db = TAFFY(BackOffice.datasource);  	
        			
        			BackOffice["model"] = json;
        			/*DataTable.utils.mapModelToForm(BackOffice["model"], form);*/
        			
        			/*update model id in form in case user modifies and save*/
        			if (jQuery('input[name="' + BackOffice.ID_COLUMN_NAME +'"]') && BackOffice["model"] != undefined)
        			{
        				jQuery('input[name="' + BackOffice.ID_COLUMN_NAME +'"]').val(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
        			}
        			
        			BackOffice.afterSave(json);
        			
        			
        			
        			
        			
        			BackOffice.initActionButtons();
        			
        			BackOffice.preloader.hide();
        			
        			BackOffice.displayMessage(Translation.translate("saved"));
        			
        			setTimeout(function(){
    					BackOffice.afterCloseAlert();
    				},2000);
    			}
    			
    			BackOffice.initActionButtons();
    			
    			BackOffice.displayMessage(Translation.translate("saved"));
    			
    		},
			"json").fail(function(json, textStatus, jqXHR){
				
				if (json.status == 401)
				{
					BackOffice.preloader.hide();
					jQuery.globalEval(json.responseText);
				}
				else
				{
					console.error('request failed');
					BackOffice.displayError("Failed to process request");
				}
				
			}).done(function() {
				
				/*setTimeout(function(){
					BackOffice.afterCloseAlert();
				},2000);*/
    			
    			
			}).always(function() {
				
				/*BackOffice.preloader.hide();*/
				
			});
};

BackOffice.setDataSource = function(json){
	this.datasource = json;	
	this.db = TAFFY(this.datasource);
	
	/*if (window.location.hash)
	{
		if (window.location.hash.substring(1) != '0')
		{
			this.renderDataTable();
		}
	}
	else
	{
		this.renderDataTable();
	}*/
	
	
};

BackOffice.activate = function(isPasswordValid){
	
	
	if (BackOffice.MODEL_NAME == 'MOrg' || BackOffice.MODEL_NAME == 'MPOSTerminal')
	{
		if (isPasswordValid == null || !isPasswordValid)
		{
			var createStorePanel = new CreateStorePanel();
	    	createStorePanel.operation = 'activate';
	    	createStorePanel.show();
	    	
	    	return;
		}
    	
	}
	
	BackOffice.preloader.show();
	
	var model = BackOffice["model"];
	var params = {
					action: "activate",
					modelName: BackOffice.MODEL_NAME,
					id: model[BackOffice.ID_COLUMN_NAME]
				};
	
	jQuery.ajax({
		type: "POST",
		url: "DataTableAction.do",
		data: params
	}).done(function(data, textStatus, jqXHR){
		
		BackOffice.preloader.hide();
		
		if(data == null || jqXHR.status != 200){
    		alert('Failed to activate!');
    	}
    	
    	var json = JSON.parse(data);
    	
    	if(json.success)
    	{
    		BackOffice.displayMessage(json.message);
    		BackOffice.afterActivate();
    	}
    	else
    	{
    		BackOffice.displayError(json.message);
    	}
	});
};


BackOffice.afterActivate = function(){
	
	BackOffice["model"].isactive = 'Y';
	BackOffice.renderAccordions();
	enableFormElements();
	/*jQuery(".backoffice-btn-deactivate").show();
	jQuery(".backoffice-btn-activate").hide();*/
	
	this.initActionButtons();
	this.initViewActionButtons();
	
	this.renderView();
}

BackOffice.deActivate = function(){
	
	BackOffice.preloader.show();
	
	var model = BackOffice["model"];
	var params = {
					action: "deActivate",
					modelName: BackOffice.MODEL_NAME,
					id: model[BackOffice.ID_COLUMN_NAME]
				};
	
	jQuery.ajax({
		type: "POST",
		url: "DataTableAction.do",
		data: params
	}).done(function(data, textStatus, jqXHR){
		
		BackOffice.preloader.hide();
		
		if(data == null || jqXHR.status != 200){
    		alert('Failed to deactivate!');
    	}
    	
    	var json = JSON.parse(data);
    	
    	if(json.success)
    	{
    		BackOffice.displayMessage(json.message);
    		BackOffice.afterDeActivate();
    	}
    	else
    	{
    		BackOffice.displayError(json.message);
    	}
	});
};

BackOffice.afterDeActivate = function(){
	
	BackOffice["model"].isactive = 'N';
	BackOffice.disableAccordions();
	disableFormElements();
	/*jQuery(".backoffice-btn-deactivate").hide();
	jQuery(".backoffice-btn-activate").show();*/
	
	this.initActionButtons();
	this.initViewActionButtons();
	
	this.renderView();
};


BackOffice.deleteObject = function(){
	
	var r = confirm("Are you sure you want to delete?");
	
	if (r == false)
	{
	    return;
	}
	
	
	jQuery(document).ajaxSend(function() {
		/*jQuery("#indicator").show();*/
		BackOffice.preloader.show();
	});

	jQuery(document).ajaxStop(function() {
		/*jQuery("#indicator").hide();*/
		BackOffice.preloader.hide();
	});
	
	var model = BackOffice["model"];
	var params = {
					action: "delete",
					modelName: BackOffice.MODEL_NAME,
					id: model[BackOffice.ID_COLUMN_NAME]
				};
	
	jQuery.ajax({
		type: "POST",
		url: "DataTableAction.do",
		data: params
	}).done(function(data, textStatus, jqXHR){
		
		if(data == null || jqXHR.status != 200){
    		alert('Failed to delete!');
    	}
    	
    	var json = JSON.parse(data);
    	
    	if(json.success)
    	{
    		BackOffice.displayMessage(json.message);
    		BackOffice.afterDelete();
    	}
    	else
    	{
    		BackOffice.displayError(json.message);
    	}
	});
};


BackOffice.afterDelete = function(){
	/*remove deleted object*/
	var objects = BackOffice.datasource;
	var deletedObjectId;
	var model = BackOffice["model"];
	
	jQuery.each(objects, function(index, object){
		
		if(object[BackOffice.ID_COLUMN_NAME] == model[BackOffice.ID_COLUMN_NAME])
		{
			deletedObjectId = index;
		}
	});
	
	objects.splice(deletedObjectId, 1);
	
	BackOffice.setDataSource(objects);
	BackOffice.renderDataTable();
	BackOffice.showMaster();
};

BackOffice.initActionButtons = function(){
	
	var model = BackOffice["model"];
	
	if (model == undefined || model[BackOffice.ID_COLUMN_NAME] == 0)
	{
		jQuery(".backoffice-btn-deactivate").hide();
		jQuery(".backoffice-btn-activate").hide();
		jQuery(".backoffice-btn-delete").hide();
		jQuery(".backoffice-btn-upload-image").hide();
	}
	else
	{
		if (model["isactive"] == 'Y')
		{
				jQuery(".backoffice-btn-deactivate").show();
				jQuery(".backoffice-btn-activate").hide();
				jQuery(".backoffice-btn-delete").show();
				jQuery(".backoffice-btn-upload-image").show();
		}
		else
		{
			jQuery(".backoffice-btn-deactivate").hide();
			jQuery(".backoffice-btn-activate").show();
			jQuery(".backoffice-btn-delete").hide();
			jQuery(".backoffice-btn-upload-image").hide();
		}
	}
};

BackOffice.uploadImage = function(){
	
	var imageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + BackOffice["model"][BackOffice.ID_COLUMN_NAME];
	
	jQuery("#upload-image-pop-up").dialog({
		modal: true,
		height: '400',
		width: '600',
		title: Translation.translate('upload.image.title'),
		autoOpen: false,
		open: function(event, ui){
			setTimeout(function(){jQuery('#upload-image-frame').contents().find('#image-panel-frame').attr('src', imageUrl);},1000);
		},
		buttons: {
			Capture : {
				'id' : 'capture-img-btn',
				'text' : 'Capture Image',
				'class': 'btn btn-default',
				'click' : function(){
					jQuery('#upload-image-pop-up').dialog('close');
					BackOffice.captureImage();
				}
			},
			
			Save : {
				'id' : 'save-img-btn',
				'text' : 'Save',
				'class' : 'btn btn-primary',
				'click' : function(){
					/*jQuery('[name=tableId]').val(BackOffice.AD_TABLE_ID);
					jQuery('[name=recordId]').val(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
					jQuery('[name=modelName]').val(BackOffice.MODEL_NAME);
					jQuery('[name=controller]').val(BackOffice.MODEL_NAME);
					jQuery('#ImageForm').submit();*/
					jQuery('#upload-image-frame').contents().find('[name=tableId]').val(BackOffice.AD_TABLE_ID);
					jQuery('#upload-image-frame').contents().find('[name=recordId]').val(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
					jQuery('#upload-image-frame').contents().find('[name=modelName]').val(BackOffice.MODEL_NAME);
					jQuery('#upload-image-frame').contents().find('[name=controller]').val(BackOffice.MODEL_NAME);
					jQuery('#upload-image-frame').contents().find('#ImageForm').submit();
					
					setTimeout(function(){
						jQuery('#upload-image-pop-up').dialog('close');
						
						
						BackOffice.view(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
						
						jQuery('.view-image').attr('src', imageUrl + "&12345");
						
						},2000);
					
				}
				
			}
		}
		
	});
	
	jQuery('#upload-image-pop-up').dialog('open');
	
	jQuery("#save-img-btn").html(Translation.translate("save"));
	jQuery("#capture-img-btn").html(Translation.translate("capture.image"));
};

BackOffice.captureImage = function(){
	jQuery('#photoBooth-popup-panel').dialog({
		modal: true,
		height: '550',
		width: '575',
		title: Translation.translate('capture.image')
		
	});
};

/* on document ready */

jQuery(document).ready(function(){
	//request or initialize data
	
	jQuery('#backoffice-import-btn').hide();
	
	BackOffice.preloader = function(){
		var dlg = new Dialog(Translation.translate("loading") + " ...");
		
		this.show = function(){
			dlg.show();
		};
		
		this.hide = function(){
			dlg.hide();
		};
		
		return this;
	}();
	
	BackOffice.requestDataTableData();
	
	jQuery(".backoffice-btn-cancel").on("click", function(e){
		BackOffice.clearMessage();
		
		BackOffice.cancel();
		
		BackOffice.afterCancel();
	});
	
	/*jQuery("#backoffice-create-btn").on("click", function(e){
		BackOffice.clearMessage();
		
		BackOffice.create();
	});*/
	
	jQuery(".backoffice-create-btn").on("click", function(e){
		BackOffice.clearMessage();
		
		BackOffice.create();
	});
	
	jQuery(".backoffice-btn-upload-image").on('click', function(){
		BackOffice.uploadImage();
	});
	
	jQuery("#backoffice-export-btn-csv").on('click', function(){
		var filename = jQuery(this).attr('filename').trim();
		BackOffice.exportToCsv(filename);
	});
	
	jQuery("#backoffice-export-btn-excel").on('click', function(){
		var filename = jQuery(this).attr('filename').trim();
		BackOffice.exportToExcel(filename);
	});
	
	/* validate form */
	jQuery.validate({
		  form : '#form',
		  modules : 'security',
		  onModulesLoaded : function() {
			    var optionalConfig = {
			      fontSize: '12px',
			      padding: '4px 0 0',
			      bad : 'Very bad',
			      weak : 'Weak',
			      good : 'Good',
			      strong : 'Strong'
			    };
			    
			    jQuery('input[name="pass_confirmation"]').displayPasswordStrength(optionalConfig);
		  },
		  onError : function() {
			  BackOffice.displayError(Translation.translate("error.correct.highligthed.fields"));
		  },
		  onSuccess : function() {
		      console.info("sbumitting form");
		      var form = jQuery("#form").get(0);
		      
		    if (BackOffice.MODEL_NAME == 'MOrg' || BackOffice.MODEL_NAME == 'MPOSTerminal')
	  		{
		    	
		    	if (BackOffice["model"][BackOffice.ID_COLUMN_NAME] == 0)
			    {
		    		var createStorePanel = new CreateStorePanel();
			    	createStorePanel.operation = 'create';
			    	createStorePanel.show();
			    	
			    	return false;
			    }
		    	else
		    	{
		    		BackOffice.save(form);
		    	}
		    	
	  		}
		    else
		    {
		    	BackOffice.save(form);
		    }
		      
		      
		      return false;
		  }
	});
	
	/* disable autocomplete */
	jQuery("#form").attr("autocomplete","off");
	
	BackOffice.renderAccordions();
	
	jQuery("#backoffice-btn-view-actions").on("click", function(){
		jQuery("#view-action-buttons").toggle();
	});
	
	/*var hash = window.location.hash;
	if(hash.length > 1){
		hash = hash.substring(1);
		
		if (hash == "0")
		{
			BackOffice.showDetail();
		}
	}*/
	
	
	jQuery('#categories li a').on('click',function(){
		var id = jQuery(this).attr('href').split('#')[1];
		
		jQuery('#categories').height('');
		jQuery('#'+id).height('');
		
		setTimeout(
				function(){
					
					var leftSideHeight = jQuery('#categories').height();
					var rightSideHeight = jQuery('#'+id).height();	
				
					if(rightSideHeight < leftSideHeight)
					{
						jQuery('#'+id).height(leftSideHeight);
					}
					else
					{
						jQuery('#'+id).height(rightSideHeight);
					}
					
				},		
		100);
				
	});	
	
	
});

BackOffice.renderAccordions = function(){
	var accordions = jQuery(".accordion");	
	jQuery.each(accordions, function(index, accordion){
		accordion = jQuery(accordion);
		var header = accordion.find("h1").detach();
		header.addClass("accordion-header");
		
		var divs = accordion.children("div");
		divs.hide();
		
		/* event handler */
		var callback = accordion.attr("data-onshow");	
		
		header.off('click');
		
		header.on("click", function(e){
			divs.toggle();
			
			if(callback){
				if(divs.css("display") == "block"){
					jQuery.globalEval(callback);
				}
			}
			
			//resizeContainerHeight();
		});
		
		accordion.prepend(header);		
	});
};

BackOffice.disableAccordions = function(){
	
	var accordions = jQuery(".accordion");	
	jQuery.each(accordions, function(index, accordion){
		accordion = jQuery(accordion);
		
		var header = accordion.find("h1");
		
		var divs = accordion.children("div");
		divs.hide();
		
		jQuery(header).off('click');
		
	});
};

function disableFormElements(){
	var form = jQuery("#form").first();
	
	jQuery(form).find('input,select,textarea').each(function(index, input){
		jQuery(input).prop("disabled", true);
	});
}

function enableFormElements(){
	var form = jQuery("#form").first();
	
	jQuery(form).find('input,select,textarea').each(function(index, input){
		jQuery(input).prop("disabled", false);
	});
}

/* exporting data */
function getTableData(){
	
	var tbl = BackOffice.dt;
	
	var st = tbl.fnSettings();
	var rows = [], data = st.aoData,	displayed = st.aiDisplay, i, iLen;
	
	var columns = st.aoColumns;
	
	var visible = [];
	
	/* add headers */
	var ths = jQuery("#example thead th");
	
	for(var j=0; j<ths.length; j++){
		var th = jQuery(ths[j]);
		
		var column = columns[j];		
		if(column.bVisible == true){
			var x = th.text();
			visible.push(x);
		}
	}
	
	rows.push(visible);	
	
	/* add rows */
	for (i = 0; i < displayed.length; i++) {
	  var record = data[displayed[i]]._aFilterData;
	  
	  var visible = [];
	  
	  for(var j=0; j<columns.length; j++){
		  var column = columns[j];
		  
		  if(column.bVisible == true){
			  var x = record[j];
			  visible.push(x);			  
		  }
	  }
	  
	  rows.push(visible);
	}
	
	return rows;
}

function saveAsExcel(filename, rows) {
	var wb = new ExcelJS.Workbook();
	var ws = wb.addWorksheet(filename);
	
	for (var j = 0; j < rows.length; j++) {
		ws.addRow(rows[j]);
		
		ws.getRow(j+1).font = j == 0 ? { name: 'Arial', size: 11, bold: true } : { name: 'Arial', size: 10, bold: false };
	}
	
	
	
	wb.xlsx.writeBuffer({
		base64: true
	}).then(function(xls64) {
		// build anchor tag and attach file (works in chrome)
		var a = document.createElement("a");
		var data = new Blob([xls64], {
			type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
		});
		var url = URL.createObjectURL(data);
		a.href = url;
		a.download = filename + ".xlsx";
		document.body.appendChild(a);
		a.click();
		setTimeout(function() {
			document.body.removeChild(a);
			window.URL.revokeObjectURL(url);
		}, 0);
	}).catch(function(error) {
		console.log(error.message);
	});
}

function saveAsCsv(filename, rows) {
	
	/* construct csv file */
	var processRow = function (row) {
        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            var result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';
    };
    
	var csvFile = '';
	
	for (var i = 0; i < rows.length; i++) {
		csvFile += processRow(rows[i]);
	}
	var blob = new Blob([csvFile], {
		type: 'text/csv;charset=utf-8;'
	});
	if (navigator.msSaveBlob) { // IE 10+
		navigator.msSaveBlob(blob, filename + ".csv");
	}
	else {
		var link = document.createElement("a");
		if (link.download !== undefined) {
			/* feature detection
			Browsers that support HTML5 download attribute */
			var url = URL.createObjectURL(blob);
			link.setAttribute("href", url);
			link.setAttribute("download", filename + ".csv");
			link.style = "visibility:hidden";
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	}
}


BackOffice.exportToCsv = function(f){
	
	var rows = getTableData();
	saveAsCsv(f, rows);
    
};

BackOffice.exportToExcel = function(f){
	
	var rows = getTableData(); 
	saveAsExcel(f, rows);
    
};


BackOffice.onCreate = function(){
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
};

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

Handlebars.registerHelper("formatDate", function(date, dateFormat) {

	var result = moment(date.replace(/-/g,"/")).format(dateFormat);

	return result;
	
});

Handlebars.registerHelper('formatDateHour', function(date, dateFormat) {
	
	return moment(date).format(dateFormat);
	
});

Handlebars.registerHelper("getSelectName", function(select_id, id) {

	var result = jQuery("#" + select_id + " option[value='" + id + "']").text();
	
	return result;
	
});

Handlebars.registerHelper("getRegionName", function(c_country_id, c_region_id) {
	
	renderStatesDropdown(c_country_id);
	
	var result = jQuery("#c_region_id option[value='" + c_region_id + "']").text();
	
	return result;
	
});

BackOffice.renderInactive = function(){
	//jQuery(".section-inner.view-header h1").css("text-decoration","line-through");
}

Handlebars.registerHelper('isActive', function(isactive) {
	
	if (isactive == "N") {
		
		return new Handlebars.SafeString(
				
				"<div class='inactive-span'><p class='center-block'></p></div>"
			);		
	}
	
	return new Handlebars.SafeString(
			"<div class=''></div>"
		);
});

Handlebars.registerHelper('getImage', function(id) {
	var imageUrl = "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + id;
	
	return imageUrl;
});

Handlebars.registerHelper("isOptionActive", function(isactive, message) {
	
	if (isactive == 'Y')
	{
		return new Handlebars.SafeString(
				"<div><label class='glyphicons glyphicons-check checkedOption'>" + message + "</label></div>"
			);
	}
	
	return new Handlebars.SafeString(
		"<div><label class='glyphicons glyphicons-unchecked checkedOption' style='text-decoration: line-through'>" + message + "</label></div>"
	);
	
});

Handlebars.registerHelper('equal', function(val1, val2, options) {
	if(arguments.length < 3)
		throw new Error("errors");
	if(val1 != val2)
	{
		return options.inverse(this);
	}
	else
	{
		return options.fn(this);
	}
});

Handlebars.registerHelper('notequal', function(val1, val2, options){
	if(val1 != val2)
	{
		return options.fn(this);
	}
	return options.inverse(this);
	
})

/* backoffice filters */
BackOffice.FILTER = {
		
		elements : [],
		
		init : function () {
			
			var i = 0;
			
			// 1.column, 2.Option Label key, 3.Option value key, 4.Placeholder
			var groupings = [
			                 ["primarygroup", "primarygroup", "primarygroup", "Primary Group"]			                 
			                 ];
			
			for(i=1; i<9; i++) {
				
				groupings.push(["group" + i, "group" + i, "group" + i, "Group " + i]);
			}
			
			for( i=0; i<groupings.length; i++ ) {
				
				var grouping = groupings[i];
				
				var filter = jQuery("[data-filter-field='product-" + grouping[0] + "']");
				
				if(filter.length == 0) continue;
				
				this.DROPDOWN (
						filter , 
						'DataTableAction.do?action=getFilter&table=Product&column=' + grouping[0] , 
						grouping[1], 
						grouping[2], 
						grouping[3] 
				);
				
				this.elements.push(filter);
			}
			
			jQuery('#data-filter-button').on('click', function(){
				
				var oTable = jQuery('#example').dataTable( );
				// to reload
				oTable.api().ajax.reload();
				
			});
				
			
		},
		
		setFilters : function (postData) {		
			
			if ( BackOffice.FILTER.elements.length == 0 ) return;
			
			var s = jQuery("#data-filter").serialize();
			var model = {};
						
			var params = s.split("&");
			
			for(var i=0; i<params.length; i++){
				
				var keyvalue = params[i].split("=");
				var key = decodeURIComponent(keyvalue[0]);
				var value = decodeURIComponent(keyvalue[1]);
				value = value.replace(/\+/g, ' ');	
				
				if(!model[key]){
					model[key] = value;
				}
				else
				{
					model[key] = model[key] + ',' + value;
				}			
			}
			
			postData.push( { "name": "filter", 	"value": Object.toJSON(model) } );
			
			
		},
		
		DROPDOWN : function( dropdown, url, labelKey, valueKey, placeholder ){
			
			var dfd = jQuery.Deferred();
		    
		    dfd.notify('requesting ' + dropdown + ' data ..');
		    
		    jQuery.post( url, 
		    		{},
					function(jsonArray, textStatus, jqXHR){	
						
		    			if (jsonArray == null || jqXHR.status != 200) {
		                    dfd.reject("Failed to request data!");
		                    return;
		                }

		    			dropdown.html("");
						
						if (jsonArray != null) {
							
							for(var i=0;i<jsonArray.length;i++)
							{
								var json = jsonArray[i];
								dropdown.append(jQuery("<option/>",{value:json[valueKey], html:json[labelKey]}));
							}
							
						}	
						
						if(dropdown.prop('multiple') == true){
							dropdown.SumoSelect({selectAll: false, 'placeholder': placeholder});
						}
						else
						{
							dropdown.prepend(jQuery("<option/>",{value:"", html:placeholder}));
						}		
						
						
		                
		                dfd.resolve("Successfully rendered " + dropdown + " dropdown");
						
					},"json").fail(function(){
						alert("Failed to request " + dropdown + " data!");
					});
		    
		    return dfd.promise();
		}
		
};


/* bug fix bootstrap dropdown */
(function() {
    var isBootstrapEvent = false;
    if (window.jQuery) {
        var all = jQuery('*');
        jQuery.each(['hide.bs.dropdown', 
            'hide.bs.collapse', 
            'hide.bs.modal', 
            'hide.bs.tooltip',
            'hide.bs.popover'], function(index, eventName) {
            all.on(eventName, function( event ) {
                isBootstrapEvent = true;
            });
        });
    }
    var originalHide = Element.hide;
    Element.addMethods({
        hide: function(element) {
            if(isBootstrapEvent) {
                isBootstrapEvent = false;
                return element;
            }
            return originalHide(element);
        }
    });
})();


BackOffice.delaySearch = function(){
	
	jQuery.fn.dataTable.Debounce = function ( table, options ) {
	    var tableId = table.settings()[0].sTableId;
	    jQuery('.dataTables_filter input[aria-controls="' + tableId + '"]') // select the correct input field
	        .unbind() // Unbind previous default bindings
	        .bind('input', (delay(function (e) { // Bind our desired behavior
	            table.search(jQuery(this).val()).draw();
	            return;
	        }, 1500))); // Set delay in milliseconds
	}
	 
	function delay(callback, ms) {
	    var timer = 0;
	    return function () {
	        var context = this, args = arguments;
	        clearTimeout(timer);
	        timer = setTimeout(function () {
	            callback.apply(context, args);
	        }, ms || 0);
	    };
	}

	var table = jQuery("#example").DataTable();
	var debounce = new jQuery.fn.dataTable.Debounce(table);
}

/* error handling */
jQuery( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
	var json = JSON.parse(jqxhr.responseText);
  	if(json.reason == 'session.timeout'){
		
		BootstrapDialog.show({
			closable: false,
			type: BootstrapDialog.TYPE_DANGER,
	      title: 'Session Expired',
	      message: 'Your session has expired. Please login again.',
	      buttons: [{
	          label: 'OK',
	          action: function(dialog) {
	          	window.location.href = "select-user.do";
	          }
	      }]
	  });      
	}
  
  	console.log(thrownError);
});
