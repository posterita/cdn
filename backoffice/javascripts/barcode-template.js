BackOffice.ID_COLUMN_NAME = "u_barcodetemplate_id";
BackOffice.MODEL_NAME = "MBarcodeTemplate";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveBarcodeTemplate";

BackOffice.defaults = {
		"printertype":"zebra", 
		"template": null,
		"u_barcodetemplate_id" : 0
		};

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getBarcodeTemplates"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var templates = json["templates"];
				
				//set data source
				//BackOffice.setDataSource(templates);
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = templates;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(templates);
					BackOffice.renderDataTable();
				}
				
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};


BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	  	      { "data": "name" },
		      { "data": "printertype"}, 
		      { "data": "printermodel"},
		      { "data": "template"}
  	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      }	      
	    ]
	});
};

BackOffice.onShowDetail = function()
{
	var model = this.model;
	
	if (model==null)
	{
		return;
	}
	
};

BackOffice.beforeSave = function(model) 
{
	/**
	 * Server expects numeric value. Sending empty string. 
	 * 
	 */
	
};

jQuery(document).ready(function()
{	
			
});

