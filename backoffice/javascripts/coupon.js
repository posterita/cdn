BackOffice.ID_COLUMN_NAME = "u_coupon_id";
BackOffice.MODEL_NAME = "X_U_Coupon";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveCoupon";

BackOffice.defaults = {
		"u_coupon_id":0,
		"isactive": "Y",
		"isgift": "Y",
		"amount":0,
		"dateissued2":"",
		"ismultipleuse" : 'N',
		"ispercentage" : 'N'
		};

/* override default export function */

BackOffice.exportToCsv = function(f){
	window.location = "DataTableAction.do?action=exportCoupons&format=csv";
};

BackOffice.exportToExcel = function(f){
	window.location = "DataTableAction.do?action=exportCoupons&format=xls";
};

BackOffice.requestDataTableData = function(){
	BackOffice.renderDataTable();
};

BackOffice.afterSave = function(model){
	
	toggleIssueCouponBtn(model);
	
	BackOffice.renderDataTable();
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	BackOffice.dt = jQuery('#example').on('xhr.dt', function ( e, settings, json ) {
		
		var coupons = json.data;
		
		var count = json["count"];		
				
		BackOffice.datasource = coupons;
		BackOffice.count = count;
		
		BackOffice.db = TAFFY(BackOffice.datasource);
		
		/*
		if(BackOffice.datasource.length == 0 && count <= 0)
		{
			jQuery('.master').hide();
			jQuery('.no-data').show();	
	 	}
		else
		{
			jQuery('#example').show();
			jQuery('.no-data').hide();
			jQuery('.master').show();
		}
		*/
		
    }).dataTable({
		"bServerSide": true,
	    "sAjaxSource": "DataTableAction.do?action=getCoupons2",
		  "fnPreDrawCallback": function() {
			  /*
	    	  if (BackOffice.datasource.length == 0 && BackOffice.count <= 0)
	    	  {
	    		  jQuery('#master').hide();
	    	  }
	    	  */
	      },
	      "columns": [
	  	      { "data": "couponno" },
		      { "data": "amount" },
		      { "data": "ispercentage" },
		      { "data": "dateissued2" },
		      { "data": "expirydate2"},
		      { "data": "dateissued2"},
		      { "data": "ismultipleuse"},
		      { "data": "balance"},
		      { "data": "datereactivated"},
		      
  	    ],
  	  "columnDefs": [
	      {
		        "render": function (data, type, row) {
		          var id = row[BackOffice.ID_COLUMN_NAME];
		          return "<a href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
		        },
		        "targets": 0
		      },
		      
		      {
		        "render": function (data, type, row) {
		        	 
		        	var status = 'Issued';
		        	
		        	var balance = row['balance'];
		        	var isMultipleUse = row['ismultipleuse'];
		        	var amount = row['amount'];
		        	var orderId = row['c_order_id']
		        	
		        	if (isMultipleUse == 'Y')
		        	{
		        		if (data == '')
		        		{
		        			status = 'Not Issued';
		        		}
		        		else
			        		if (balance < amount && balance != 0)
			        		{
			        			status = 'Partially Redeemed';
			        		}
		        		else 
		        			if (balance == amount)
			        		{
			        			status = 'Issued';
			        		}
		        		else
		        			if (orderId > 0)
			        		{
			        			status = 'Redeemed';
			        		}
		        	}
		        	else
		        	{
		        		if(data == '')
		        		{
			        		status = 'Not Issued';
			        	}
		        		else
		        			if(orderId > 0)
		        			{
		        				status = 'Redeemed';
		        			}
		        	}
					 
					 return status;
		        },
		        "targets": 5
		      }
		    ]
	});
};

BackOffice.validateBeforeSave = function(model) {
		
	if(jQuery("#is-percentage").is(":checked")){
		
		var amount = jQuery("#amount").val();
		
		if( amount == "" || isNaN(amount) || amount > 100 ){
			
			alert("Invalid coupon percentage");
			
			return false;
		}
		
		/*
		if(jQuery("#is-multiple-use-checkbox").is(":checked")){
			
			alert("Percentage coupons cannot be multiple use");
			
			return false;
			
		}
		*/
		
	}
	
	return true;
	
}

BackOffice.afterShowDetail = function(){
	
	var model = this.model || this.defaults;
	
	if(model['ispercentage' == 'Y']){
		jQuery('#coupon-value-label').html("Coupon Percentage");
	}
	else
	{
		jQuery('#coupon-value-label').html("Coupon Amount");
	}
	
	if(model['isgift'] == 'Y'){
		
		jQuery("#gift-container").show();
		jQuery("#compensation-container").hide();
		
	}
	else
	{
		jQuery("#gift-container").hide();
		jQuery("#compensation-container").show();
	}
	
	jQuery( "#datepicker" ).datepicker( "setDate", model['expirydate2'] );
	
	toggleIssueCouponBtn(model);
	
	if(model['dateissued2'] != '')
	{
		jQuery('.backoffice-btn-save').attr('style','background-color: #08a600;');
	}
	
	if((model['ismultipleuse'] == 'Y' && model['balance'] == 0) || (model['ismultipleuse'] == 'Y' && moment().diff(moment(model['expirydate'])) > 0))
	{
		jQuery('#couponno').attr('readonly', 'readonly');
		jQuery('#is-multiple-use-checkbox').attr('readonly', 'readonly');
		jQuery('.backoffice-btn-save').text('Reactivate Coupon');
	}
	else
	{
		jQuery('#couponno').removeAttr('readonly');
		jQuery('#is-multiple-use-checkbox').removeAttr('readonly');
		jQuery('.backoffice-btn-save').text('Save');
	}
	
}

BackOffice.afterCancel = function(){
	jQuery('.backoffice-btn-save').attr('style','background-color: #fff !important; color:#1d1d1d; border-color:#bdbec2 !important;');
	
	jQuery('#dateissued2').val('');
};

function toggleIssueCouponBtn(model){
	
	if(model['dateissued2'] != ""){
		
		jQuery('.backoffice-btn-save-issue-coupon').hide();
		
	}
	else
	{
		jQuery('.backoffice-btn-save-issue-coupon').show();
	}
}

BackOffice.afterSave = function(model){

	BackOffice.requestDataTableData();	
};

jQuery(document).ready(function(){
	jQuery('.backoffice-btn-save-issue-coupon').on('click',function(){
		
		/* set date issued and save */
		var dateissued = moment().format('DD-MM-YYYY HH:mm:ss');
		jQuery("#dateissued2").val(dateissued);
		
		var form = jQuery("#form").get(0);
	    BackOffice.save(form);
		
	});
	
	jQuery( "#datepicker" ).datepicker({
		format: "dd-mm-yyyy",
		orientation: "top left",
		minDate:'0'
	}).on('hide', function (e) { e.preventDefault(); });
	
	jQuery("#is-gift").on('click',function(){
		jQuery("#gift-container").show();
		jQuery("#compensation-container").hide();
	});
	
	jQuery("#is-compensation").on('click',function(){
		
		jQuery("#gift-container").hide();
		jQuery("#compensation-container").show();
	});	
	
	jQuery("#is-amount").on('click',function(){		
		jQuery('#coupon-value-label').html("Coupon Amount");
	});
	
	jQuery("#is-percentage").on('click',function(){		
		jQuery('#coupon-value-label').html("Coupon Percentage");
	});
	
	
	/*if (BackOffice["model"].iscompensation == 'Y') {

		jQuery("[name=iscompensation]").attr("checked",true);
		jQuery("[name=isgift]").attr("checked",false);
		jQuery("#gift-container").hide();
		jQuery("#compensation-container").show();
	}
	
	if (BackOffice["model"].iscompensation != 'Y') {

		jQuery("[name=iscompensation]").attr("checked",false);
		jQuery("[name=isgift]").attr("checked",true);
		jQuery("#gift-container").show();
		jQuery("#compensation-container").hide();
	}*/
	
	/*if(jQuery("#is-gift").is(":checked") == true)
	{
		jQuery("#is-compensation").attr('disabled', true);
	}
	else
		if(jQuery("#is-compensation").is(":checked") == true)
		{
			jQuery("#is-gift").attr('disabled', true);
		}*/
	
	/*
	var products;
	
	jQuery.post("DataTableAction.do?action=getProductsAutocomplete", function(data){
		products = data.names;
		
		jQuery("#product-name").autocomplete({
		    source: products,
		    minlength: 2,
		    focus: function(event, ui) {
		        jQuery("#product-name").val(ui.item.value);
		        return false;
		    },
		    select: function(event, ui) {
		        jQuery("#product-name").val(ui.item.value);
		        jQuery("#m_product_id").val(ui.item.id);
		        return false;
		    }
		}).data("autocomplete")._renderItem = function(ul, item) {
		    return jQuery("<li></li>")
		        .append("<a><div>" + item.value + "</div><div>" + item.description + "</div></a>")
		        .data("item.autocomplete", item)
		        .appendTo(ul);
		};			
	});	
	*/
	jQuery("#product-name").autocomplete({
	    source: function( request, response ) {
	    	
	    	var searchTerm = request.term;
	    	
	    	var url = "DataTableAction.do?action=autocomplete&table=m_product&columns=name,description,upc&term=" + searchTerm;
	    			    	
	    	jQuery.getJSON( url, request, function( data, status, xhr ) {
	            response( data );
	          });
	    	
	    },
	    minlength: 2,
	    focus: function(event, ui) {
	        jQuery("#product-name").val(ui.item.name);
	        return false;
	    },
	    select: function(event, ui) {
	    	
	    	if(ui.item.m_product_id == -1){
	    		
	    		alert("Item not found!");
	    		
	    		document.getElementById("product-name").select();
	    		
	    		return false;
	    	}
	    	
	        jQuery("#product-name").val(ui.item.name);
	        jQuery("#m_product_id").val(ui.item.m_product_id);
	        return false;
	    }
	}).data("autocomplete")._renderItem = function(ul, item) {
	    return jQuery("<li style='border-bottom:solid 1px #CCC;'></li>")
	        .append("<a><div><strong>" + item.name + "</strong></div><div>" + item.description + "</div>" + "</div><div>" + item.upc + "</div></a>")
	        .data("item.autocomplete", item)
	        .appendTo(ul);
	};
	
	jQuery(".backoffice-btn-save").prop('style','background-color: #fff !important;color: #1d1d1d;border: 1px solid #bdbec2 !important;');
	
});

/* launch coupon importer */
BackOffice.importCoupon = function(){	
	window.location = 'importer.do?importer=CouponImporter';
};