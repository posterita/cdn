BackOffice.ID_COLUMN_NAME = "c_bpartner_id";
BackOffice.MODEL_NAME = "MBPartner";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveCustomer";
BackOffice.AD_TABLE_ID = 291;

BackOffice.defaults = {		
		"issopricelist":"N", 
		"istaxincluded":"N",
		"emailreceipt":"N",
		"istaxexempt":"N",
		"emailpromotion":"N",
		"isvendor":"N",
		"isemployee":"N",
		"credit_limit":0,
		/*"credit_watch":0,*/
		"c_bpartner_id":0,
		"isactive": "Y",
		"enableloyalty" : "N",
		"c_country_id": 245,
		"c_region_id": 0,
		"so_creditused":0
		};

/* override default export function */

BackOffice.exportToCsv = function(f){
	window.location = "DataTableAction.do?action=exportCustomers&format=csv";
};

BackOffice.exportToExcel = function(f){
	window.location = "DataTableAction.do?action=exportCustomers&format=xls";
};


BackOffice.requestDataTableData = function(){
	
	BackOffice.preloader.show();
	
	jQuery.post("DataTableAction.do", { action: "getCustomers"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					
					alert("Failed to request data!"); 
					
					BackOffice.preloader.hide();
					
					return;
				}
				
				var customers = json["customers"];
				var countries = json["countries"];
				var pricelists = json["pricelists"];
				var paymentterms = json["paymentterms"];
				var purchases = json["purchases"];
				var cursymbol = json["cursymbol"];
				var discountcodes = json["discountcodes"];
				
				//set data source
				//BackOffice.setDataSource(customers);
				BackOffice.countryDB = TAFFY(countries);
				BackOffice.purchasesDB = TAFFY(purchases);
				
				BackOffice["discountcodes"] = discountcodes;
				
				BackOffice.curSymbol = cursymbol;
				
				//render country list
				DataTable.utils.populateSelect(jQuery("#c_country_id"), countries, "c_country_name", "c_country_id");
				
				//render price list
				DataTable.utils.populateSelect(jQuery("#sales-price-list"), pricelists, "name", "m_pricelist_id");
				
				//render payment term
				DataTable.utils.populateSelect(jQuery("#c_paymentterm_id"), paymentterms, "name", "c_paymentterm_id");
				
				//render discount codes
				DataTable.utils.populateSelect(jQuery("#u_pos_discountcode_id"), discountcodes, "name", "u_pos_discountcode_id");
				
				BackOffice.preloader.hide();
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = customers;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(customers);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
				BackOffice.preloader.hide();
			}); 
		
};

BackOffice.renderDataTable = function(){
	
	BackOffice.preloader.show();
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	BackOffice.dt = jQuery('#example').on('xhr.dt', function ( e, settings, json ) {
		
		var customers = json.data;
		
		var customerCount = json["customerCount"];
		BackOffice["customerCount"] = customerCount;
		
				
		BackOffice.datasource = customers;
		
		BackOffice.db = TAFFY(BackOffice.datasource);
		
		
		if(BackOffice.datasource.length == 0 && customerCount <= 0)
		{
			jQuery('.master').hide();
			jQuery('.no-data').show();	
	 	}
		else
		{
			jQuery('#example').show();
			jQuery('.no-data').hide();
			jQuery('.master').show();
		}
		
    }).dataTable({
    	"searchDelay" : 2000,
		"bServerSide": true,
		"initComplete": function(settings, json) {
			BackOffice.preloader.hide();
		},
	    "sAjaxSource": "DataTableAction.do?action=getCustomers2",
		  "fnPreDrawCallback": function() {
			  
			  BackOffice.preloader.hide();
			  
	    	  if (BackOffice.datasource.length == 0 && BackOffice.customerCount <= 0)
	    	  {
	    		  jQuery('#master').hide();
	    	  }
	      },
	      "columns": [
	  	      { "data": "name" },
		      { "data": "email"}, 
		      { "data": "phone"},
		      { "data": "address1"},
		      { "data": "address2"},
		      { "data": "city"},
		      { "data": "isactive"},
		      { "data": "identifier"},
		      { "data": "created"},
		      { "data": "createdby"},
		      { "data": "updated"},
		      { "data": "updatedby"}
		      /*{ "data": "custom1"},
		      { "data": "custom2"},
		      { "data": "custom3"},
		      { "data": "custom4"}*/
  	    ],
  	  "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      }	,
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 6
	      },
	      { 	"visible": false, "targets": [7] }
	    ],
	    
	    "fnServerParams": function(aoData) {
	    	
	    	BackOffice.preloader.show();
	    }
	});
	
	BackOffice.delaySearch();
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

/*BackOffice.beforeShowView = function(id) {
	BackOffice.preloader.show();
	
	jQuery.post("DataTableAction.do", { action: "getCustomerSalesHistory", c_bpartner_id: id},
				function(json, textStatus, jqXHR){	
		
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}		
					BackOffice.salesHistory = json["salesHistory"];
					BackOffice.salesHistoryDB = TAFFY(BackOffice.salesHistory);
					
				},"json").done(function() {
					
					BackOffice.KPI = new customerStatistics(BackOffice.salesHistoryDB);
					
					showKPI(BackOffice.KPI);
					
					if ( jQuery.fn.dataTable.isDataTable('#sales-history') ) {
						jQuery('#sales-history').dataTable().fnDestroy();
					}
					
					var datasource = BackOffice.salesHistory;
					
					jQuery('#sales-history').dataTable({
						"bAutoWidth": false,
						"data": datasource,
					    "columns": [
					  	      { "data": "dateordered" },
						      { "data": "organization"}, 
						      { "data": "docstatus"}, 
						      { "data": "documentno"},
						      { "data": "grandtotal"},
						      { "data": "salesrep"}
				  	    ],
					    "columnDefs": [
					          {
							     "render": function (data, type, row) {
							       return moment(data).format("MMMM DD, YYYY HH:MM A");
							     },
							     "targets": 0
							  },
			         	      {
			         	        "render": function (data, type, row) {
			         	          var id = row["c_order_id"];
			         	          return "<a href='javascript:void(0);' onclick='ReportItemDetails.displayOrderInfo(" + id + ")'>" + data + "</a>";
			         	        },
			         	        "targets": 3
			         	      },
			                  {
			                      "targets": [ 2 ],
			                      "visible": false
			                  }     
			         	],
				  	    "order": [[ 0, "desc" ]],
				  	    "deferRender": true
					});
					
					jQuery('#sales-history').on( 'draw.dt', function () {
						var height = jQuery("html").height();
						jQuery("#left-panel").height(height);
					});
					
					jQuery.fn.dataTable.ext.search.push(
						    function( settings, data, dataIndex ) {
						    	if (settings.nTable.attributes['id'].nodeValue == "sales-history")
					    		{
							    	var date_from = jQuery('#date-from').val();
							    	var date_to = jQuery('#date-to').val();
							    	var status = jQuery('#status option:selected').val();
							        
							    	var date = new Date(moment(data[0]).format("MM/DD/YYYY"));
							        var docstatus = data[2];
							        
							        if ( (!date_from.trim() && !date_to.trim() && !status.trim()) ||
							        	 (!date_from.trim() && !date_to.trim() && status.trim() && status == docstatus) ||
							        	 (!date_from.trim() && date_to.trim() && !status.trim() && date <= new Date(date_to)) ||
							        	 (date_from.trim() && !date_to.trim() && !status.trim() && date >= new Date(date_from)) ||
							        	 (date_from.trim() && date_to.trim() && status.trim() && date >= new Date(date_from) && date <= new Date(date_to) && status == docstatus) ||
							        	 (date_from.trim() && date_to.trim() && !status.trim() && date >= new Date(date_from) && date <= new Date(date_to)) ||
							        	 (date_from.trim() && !date_to.trim() && status.trim() && date >= new Date(date_from) && status == docstatus) ||
							        	 (!date_from.trim() && date_to.trim() && status.trim() && date <= new Date(date_to) && status == docstatus) )
							        {
							        	return true;
							        }
							        
							        return false;
					    		}
						    	
						    	return true;
						    });
					
				}).fail(function(){
					alert("Failed to request data!");
				}).always(function(){
					BackOffice.preloader.hide();
					
					var height = jQuery("html").height();
					jQuery("#left-panel").height(height);
				});
};*/

BackOffice.onShowDetail = function(){
	var model = this.model;
	
	if (model==null){
		return;
	}
	
	var c_country_id = model["c_country_id"];
	renderStatesDropdown(c_country_id);
	
	var c_region_id = model["c_region_id"];	
	jQuery("#c_region_id").val(c_region_id);
		
	if(model["credit_status"]=="O")
	{
		jQuery("#credit-status-list-checkbox").attr('checked', 'checked');	
		jQuery("#credit-status-list-container").show();
	}
	
	if((model["custom1"] || model["custom2"] || model["custom3"] || model["custom4"] || model["notes"])=="")
	{
		jQuery("#additional-information-container").hide();
	}
	else
	{
		jQuery("#additional-information-container").show();
		jQuery("#additional-information-checkbox").attr('checked', 'checked');
	}	
	
	jQuery("#categories li a").on("click",function(){
		if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			jQuery(".ui-tabs-selected")
			.children("i")
			.removeClass("fa arrow-icons fa-chevron-down")
			.addClass("fa arrow-icons fa-chevron-right");
		}
		if((jQuery("#categories li").not(".ui-tabs-selected")))
		{
			jQuery("#categories li").not(".ui-tabs-selected")
			.children("i")
			.removeClass("fa arrow-icons fa-chevron-right")
			.addClass("fa arrow-icons fa-chevron-down");
		}
	});
	
	if(model['birthday'] && model['birthday'].length > 0){
		var dob = new Date( moment( model['birthday'], 'MM/DD/YYYY' ).valueOf() );	
		jQuery( "#datepicker" ).datepicker( "setDate", model['birthday'] );
	}
	
	if(model['discount-code-expiry'] && model['discount-code-expiry'].length > 0){
		var dob = new Date( moment( model['discount-code-expiry'], 'MM/DD/YYYY' ).valueOf() );	
		jQuery( "#datepicker-discount-code-expiry" ).datepicker( "setDate", model['discount-code-expiry'] );
	}
	
};

BackOffice.beforeSave = function(model) {
	/**
	 * Server expects numeric value. Sending empty string. 
	 */
	var m_pricelist_id = model["m_pricelist_id"];
	
	if (m_pricelist_id == ""){
		model["m_pricelist_id"] = 0;
	};
	
	var c_paymentterm_id = model["c_paymentterm_id"];
	
	if (c_paymentterm_id == ""){
		model["c_paymentterm_id"] = 0;
	};
		
	var c_country_id = model["c_country_id"];
	
	if (c_country_id == ""){
		model["c_country_id"] = 0;
	};
	
	var c_region_id = model["c_region_id"];
	
	if (c_region_id == ""){
		model["c_region_id"] = 0;
	};
	
	var credit_checkbox = jQuery("#credit-status-list-checkbox");
	if(credit_checkbox.is(':checked'))
	{
		model["credit_status"]="O";
	}
	else
	{
		model["credit_status"]="X";
	}
	
	/*bug fix*/
	var discount_code_expiry = model["discount_code_expiry"];
	if(discount_code_expiry == ""){
		delete model["discount_code_expiry"];
	}
};

BackOffice.onCreate = function(){
	var model = BackOffice["model"];
	
	if(model["emailreceipt"]=="N")
	{
		jQuery("#email-receipt").attr('checked', 'checked');		
	}
	
	if(model["istaxexempt"]=="N")
	{
		/*jQuery("#is-tax-exempt").attr('checked', 'checked');*/		
	}
	
	if(model["enableloyalty"]=="N")
	{
		jQuery("#loyalty-program-checkbox").attr('checked', 'checked');		
	}
		
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	jQuery("#credit-status-list-container").hide();
	jQuery("#additional-information-container").hide();
	
	renderStatesDropdown(245);
	jQuery('#c_region_id').val('0');
};

BackOffice.showDefaultCountry = function(){
	
	var countryValue = jQuery("#c_country_id:eq(0)").val();
	if(countryValue == "")
	{
		jQuery("#c_country_id").val("100");
		
		var c_country_id = jQuery("#c_country_id").val();
		
		var countryDB = BackOffice.countryDB;
		
		var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
		var regions = country.regions;
		
		DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
	}
	
	var regionValue = jQuery("#c_region_id:eq(0)").val();
	if(regionValue == "")
	{
		jQuery("#c_region_id").val("0");
	}
};

BackOffice.afterRenderView = function(id) {
	BackOffice.preloader.show();
	
	var query = {};
	query["c_bpartner_id"] = {"==":id};
	
	var purchases = BackOffice.purchasesDB(query).get();
	
	BackOffice.renderOtherViews("#view-purchases", "#show-purchases", purchases[0]);
	
	BackOffice.preloader.hide();
};

jQuery(document).ready(function(){
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	var pricelist_checkbox = jQuery("#assignPriceList-checkbox");
	pricelist_checkbox.on("change", function(e){
		var container = jQuery("#price-list-container");	
		
		if(pricelist_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var credit_management_checkbox = jQuery("#enable-credit-management-checkbox");
	credit_management_checkbox.on("change", function(e){
		var container = jQuery("#enable-credit-management-container");	
		
		if(credit_management_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
		
	var notifications_checkbox = jQuery("#enable-notifications-checkbox");
	notifications_checkbox.on("change", function(e){
		var container = jQuery("#enable-notifications-container");
		
		if(notifications_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var additionalInformationCheckbox = jQuery("#additional-information-checkbox");
	additionalInformationCheckbox.on("change", function(){
		var container = jQuery("#additional-information-container");
		if(additionalInformationCheckbox.is(':checked')){
			container.show()
		}
		else{
			container.hide();
		}
	});
	
	var loyaltyProgramCheckbox = jQuery("#loyalty-program-checkbox");
	loyaltyProgramCheckbox.on("change", function(){
		var container = jQuery("#loyalty-program-container");
		if(loyaltyProgramCheckbox.is(':checked')){
			container.show()
		}
		else{
			container.hide();
		}
	});
	
	var creditStatusListCheckbox = jQuery("#credit-status-list-checkbox");
	creditStatusListCheckbox.on("change", function(){
		var container = jQuery("#credit-status-list-container");
		if(creditStatusListCheckbox.is(':checked')){
			container.show()
		}
		else{
			container.hide();
		}
	});
		
	jQuery(function() {
		jQuery( "#datepicker" ).datepicker({
			format: "mm-dd-yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
		
		jQuery( "#datepicker-discount-code-expiry" ).datepicker({
			format: "mm-dd-yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
		
		jQuery( "#date-from" ).datepicker({
			format: "mm/dd/yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
		
		jQuery( "#date-to" ).datepicker({
			format: "mm/dd/yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
	});
	
	var select  = jQuery("#c_country_id");
	select.on("change", function(e){
		var c_country_id = jQuery("#c_country_id").val();
		renderStatesDropdown(c_country_id);
	});
	
	jQuery('.backoffice-btn-view-statment-of-account').on('click', function(){
		
		var url = 'BPartnerAction.do?action=getStatementOfAccount&customerId=';
		var customerId = BackOffice.model[BackOffice.ID_COLUMN_NAME];
		
		jQuery("#date-picker-pop-up").dialog({
			modal: true,
			title: Translation.translate("select.date.range"),
			open: function(){
				jQuery('#dateFrom, #dateTo').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true
                });
				
				jQuery('#dateFrom').datepicker("show");
				
				jQuery.each(jQuery('#dateFrom, #dateTo'), function(i, val) {
                    jQuery(val).on('hide', function(e) {
                        e.preventDefault();
                    });
                });
			},
			
			buttons:[{
					text: 'Ok',
					"class" : 'btn btn-primary btn-ok',
					click: function(){
						jQuery('#date-picker-pop-up').dialog("close");
						
						var statementOfAccount = new StatementOfAccount();
						statementOfAccount.init(customerId, jQuery('#dateFrom').val(), jQuery('#dateTo').val());
						
						showStatementOfAccountPopUp(statementOfAccount);
					}
				},
				
				{
					text: 'Cancel',
					"class" : 'btn btn-default btn-cancel',
					click: function(){
						jQuery('#date-picker-pop-up').dialog("close");
					}
				}
			]
		});
		
		jQuery(".btn-ok").html(Translation.translate("ok"));
		jQuery(".btn-cancel").html(Translation.translate("cancel"));
		
	});
	
	jQuery(".backoffice-btn-disclaimer-form").on('click', function(){
		
		if(BackOffice.model['custom1'] == ''){
			
			alert('No disclaimer form!!!');
			return;
		}
		
		jQuery(".view-action-buttons").hide();
		
		var bpartnerId = BackOffice.model[BackOffice.ID_COLUMN_NAME];
		
		 try
			{	
			 	window.location = 'BPartnerAction.do?action=generatePdf&bpartnerId='+ bpartnerId;
			}
			catch(e)
			{}		
		
	});
	
	jQuery("#view-select-actions").on('change', function() {
		var selectedOption = jQuery(this).val();
		
		jQuery(selectedOption).click();
	});
	
	jQuery("#refresh-sales-history").on('click' , function() {
		BackOffice.preloader.show();

		jQuery("#sales-history").DataTable().draw();
		
		BackOffice.preloader.hide();
	});
	
	jQuery("#sales-history-sort-by").on('change' , function() {
		jQuery("#sales-history").DataTable().column(jQuery(this).val()).order('asc').draw();
	});
	
	jQuery("#email-groups-plus").on('click', function(){
		jQuery("#additional-emails-container").show();
		jQuery("#additional-emails-minus").show();
		jQuery("#additional-emails-plus").hide();
	});
	
	jQuery("#email-groups-minus").on('click', function(){
		jQuery("#additional-emails-container").hide();
		jQuery("#additional-emails-plus").show();
		jQuery("#additional-emails-minus").hide();	
	});
	
	var fields = {			
			company_name : {
				tooltip: Translation.translate("tooltip.cus.enter.companuname")				
				},
			taxid : {
				tooltip: Translation.translate("tooltip.cus.enter.taxid"),
				},
			name : {
				tooltip: Translation.translate("tooltip.cus.enter.name")
				},
			email :{
				tooltip: Translation.translate("tooltip.enter.email.address")
				},
			identifier :{
				tooltip: Translation.translate("tooltip.uniquely.identify.cus")
				},
			phone :{
				tooltip: Translation.translate("tooltip.office.home.number")
				},
			mobile :{
				tooltip: Translation.translate("tooltip.mobile.num")
				},
			gender :{
				tooltip: Translation.translate("tooltip.select.gender")
				},
			title :{
				tooltip: Translation.translate("tooltip.select.title")
				},
			emailreceipt :{
				tooltip: Translation.translate("tooltip.selecting.option"),
				position : 'top'
				},
			isvendor :{
				tooltip: Translation.translate("tooltip.select.cus.sup"),
				position : 'top'
				},
			isemployee :{
				tooltip: Translation.translate("tooltip.cus.employee.company"),
				position : 'top'
				},
			notes :{
				tooltip: Translation.translate("tooltip.additionalinfo")
				},
			address1 :{
				tooltip: Translation.translate("tooltip.enter.first.line.cus.add")
				},
			address2 :{
				tooltip: Translation.translate("tooltip.enter.secondline.cus.add")
				},
			city :{
				tooltip: Translation.translate("tooltip.enter.city.of.cus")
				},
			postal :{
				tooltip: Translation.translate("tooltip.enter.zipcode.of.cus")
				},
			c_region_id :{
				tooltip: Translation.translate("tooltip.select.state.cus")
				},
			c_country_id :{
				tooltip: Translation.translate("tooltip.select.country.cus")
				},
			assignPriceList :{
				tooltip: Translation.translate("tooltip.set.pricelist.diff"),
				position : 'top'
			},
			m_pricelist_id :{
				tooltip: Translation.translate("tooltip.select.pricelist.drop")
				},
			credit_status :{
				tooltip: Translation.translate("tooltip.this.must.be.enabled.cus")
				},
			credit_limit :{
				tooltip: Translation.translate("tooltip.please.enter.limit.cus.credit")
				},
			c_paymentterm_id :{
				tooltip: Translation.translate("tooltip.select.days.before.payment")
				},
			enableloyalty:{
				tooltip: Translation.translate("tooltip.enable.customer.loyalty"),
				position : 'top'
				},
			emailgroups :{
				tooltip: Translation.translate("tooltip.enter.additional.emails")
			}
			};
	
	//Include Global Color 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
});

function renderStatesDropdown(c_country_id){
	
	if (c_country_id == undefined)
	{
		return;
	}
	
	var countryDB = BackOffice.countryDB;
	
	var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
	var regions = country.regions;
	
	DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
}

function showStatementOfAccountPopUp(statementOfAccount)
{
	jQuery('#statement-of-account-pop-up').dialog({
		modal: true,
		title: Translation.translate("statement.of.account"),
		height: '550',
		width: '775',
		buttons:[{
			text: 'Send Via Mail',
			"class" : 'btn btn-default1',
			click: function(){
				statementOfAccount.sendMail();
			}
		},
		{
			text: 'Export',
			id: 'export-button-pdf',
			"class" : 'btn btn-default',
			click : function(){
				statementOfAccount.exportPDF();
			}
		},
		{
			text : 'Ok',
			"class" : 'btn btn-primary',
			click : function(){
				jQuery('#statement-of-account-pop-up').dialog("close");
			}
		}
		
		]
	});
	
	jQuery(".btn-primary").html(Translation.translate("ok"));
	jQuery("#export-button-pdf").html(Translation.translate("export"));
	jQuery(".btn-default1").html(Translation.translate("send.via.mail"));
}

Handlebars.registerHelper("getAvailableCredit", function(credit_limit, so_creditused) {

	var result = credit_limit - so_creditused;
	
	return result;
	
});

Handlebars.registerHelper("getAvailableLoyaltyPoints", function(loyalty_points_earned, loyalty_points_spent) {

	var result = new Number(loyalty_points_earned - loyalty_points_spent).toFixed(2);
	
	/*if (isNaN(result)) 
	    {
			result = 0;
		
	    }*/
	
	return result;
	
});

Handlebars.registerHelper('displayGreyOutBox', function(credit_status) {
	
	if (credit_status == "X") {
		return new Handlebars.SafeString(
				"<div class='inactive-span' style='border-bottom: none'><p class='center-block'>No Credit Check</p></div>"
			);
	}
	
	return;
});

Handlebars.registerHelper("getSelectName", function(select_id, id) {

	var result = jQuery("#" + select_id + " option[value='" + id + "']").text();
	
	return result;
	
});

BackOffice.uploadImage = function(){
	
	var customer_image_url = jQuery("#customer-image-url").val();	
	
	
	var customerImageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + BackOffice["model"][BackOffice.ID_COLUMN_NAME]  + "&12345";
	
	if (customer_image_url != "")
	{
		customerImageUrl = customer_image_url;
	}
	
	jQuery("#upload-image-pop-up").dialog({
		modal: true,
		height: '400',
		width: '600',
		title: Translation.translate('upload.image.title'),
		autoOpen: false,
		open: function(event, ui){
			setTimeout(function(){jQuery('#upload-image-frame').contents().find('#image-panel-frame').attr('src', customerImageUrl);},1000);
		},
		buttons: {
			Capture : {
				'id' : 'capture-img-btn',
				'text' : 'Capture Image',
				'class': 'btn btn-default',
				'click' : function(){
					jQuery('#upload-image-pop-up').dialog('close');
					BackOffice.captureImage();
				}
			},
			
			Save : {
				'id' : 'save-img-btn',
				'text' : 'Save',
				'class' : 'btn btn-primary',
				'click' : function(){
					/*jQuery('[name=tableId]').val(BackOffice.AD_TABLE_ID);
					jQuery('[name=recordId]').val(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
					jQuery('[name=modelName]').val(BackOffice.MODEL_NAME);
					jQuery('[name=controller]').val(BackOffice.MODEL_NAME);
					jQuery('#ImageForm').submit();*/
					jQuery("#image-url").val('');
					//BackOffice.model.imageurl = '';
					BackOffice.model.customerimageurl = '';
					jQuery('#upload-image-frame').contents().find('[name=tableId]').val(BackOffice.AD_TABLE_ID);
					jQuery('#upload-image-frame').contents().find('[name=recordId]').val(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
					jQuery('#upload-image-frame').contents().find('[name=modelName]').val(BackOffice.MODEL_NAME);
					jQuery('#upload-image-frame').contents().find('[name=controller]').val(BackOffice.MODEL_NAME);
					jQuery('#upload-image-frame').contents().find('#ImageForm').submit();
					
					setTimeout(function(){
						jQuery('#upload-image-pop-up').dialog('close');						
						
						BackOffice.view(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
						
						var image_url_view = BackOffice.model.customerimageurl;
						
						if (image_url_view != "")
						{
							customerImageUrl = image_url_view;
						}						
						else
						{
							customerImageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + BackOffice["model"][BackOffice.ID_COLUMN_NAME]  + "&12345";
						}
						
						jQuery('.view-image').attr('src', customerImageUrl);
						jQuery("#customer-image-url").val('');
						
						},2000);					
				}
				
			}
		}
		
	});
	
	jQuery('#upload-image-pop-up').dialog('open');
	
	jQuery("#save-img-btn").html(Translation.translate("save"));
	jQuery("#capture-img-btn").html(Translation.translate("capture.image"));
};


function showDiscountPercentage()
{
	var discountCodes = BackOffice["discountcodes"];
	var selectedVal = jQuery("#u_pos_discountcode_id").val();
	
	for(var i=0; i<discountCodes.length; i++)
	{
		var discountCode = discountCodes[i];
		
		if (discountCode["u_pos_discountcode_id"] == selectedVal)
		{
			jQuery("#discount-code-percentage").html(discountCode["percentage"]);
			jQuery("#discount-code-percentage-container").show();
			break;
		}
		
		if (selectedVal == "")
		{
			jQuery("#discount-code-percentage").html("");
			jQuery("#discount-code-percentage-container").hide();
		}
	}
}

Handlebars.registerHelper('getImage', function(id) {
	var imageUrl = '';	
	
	var customer_image_url = BackOffice.model.customerimageurl;
	
	if (customer_image_url != "")
	{
		imageUrl = customer_image_url;
	}
	else
	{
		imageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + id;
	}
		
	return imageUrl;
});
