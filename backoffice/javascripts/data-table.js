var DataTable = {};

DataTable.utils = {};

DataTable.utils.mapModelToForm = function(model, form){
	/* get inputs */
	var inputs = jQuery(form).find("input");
	for(var i=0; i<inputs.length; i++){
		var input = inputs[i];
		var name = input.name;
		var type = input.type;
		
		if(type == "radio" || type == "checkbox"){
			var value = input.value;
			if(value == model[name]){
				input.checked = true;
			}
			else{
				input.checked = false;
			}
			
			continue;
		}
		
		if(model[name] === undefined || (jQuery.trim(model[name]+'').length == 0)){
			/*jQuery(input).val("");*/
		}
		else{			
			jQuery(input).val(model[name]);
		}			
	}
	
	/* get selects */
	var selects = jQuery(form).find("select");
	for(var i=0; i<selects.length; i++){
		var select = selects[i];
		var name = select.name;
		
		if(model[name] === undefined){
			jQuery(select).val("");
		}
		else{			
			jQuery(select).val(model[name]);
		}
	}
	
	/* get textareas */
	var textareas = jQuery(form).find("textarea");
	for(var i=0; i<textareas.length; i++){
		var textarea = textareas[i];
		var name = textarea.name;
		
		if(model[name]){
			jQuery(textarea).val(model[name]);
		}
		else{
			jQuery(textarea).val("");
		}
	}
};

DataTable.utils.mapFormToModel = function(form, model){
	
	/*form = jQuery(form)[0];
	var inputs = form.getInputs();
	
	for(var i=0;i<inputs.length; i++){
		var input = inputs[i];
		var key = input.name || input.id;
		var value = input.value;
		
		if("number" == jQuery(input).attr("data-validation")){
			value = parseFloat(value);
		}
		
		var index = key.indexOf("[]");
		if(index > 0){
			// remove square bracket from key
			var key = key.substring(0, index);
			
			if(model[key]){
				model[key].push(value);
			}
			else
			{
				model[key] = [value];
			}
		}
		else
		{			
			model[key] = value;
		}		
		
	}*/
	/*
	var s = jQuery(form).serialize();
	form = jQuery(form)[0];
	
	var params = s.split("&");
	for(var i=0; i<params.length; i++){
		var keyvalue = params[i].split("=");
		var key = decodeURIComponent(keyvalue[0]);
		var value = decodeURIComponent(keyvalue[1]);
		value = value.replace(/\+/g, ' ');
		
		var input = form[key];
		if(input){
			if("number" == jQuery(input).attr("data-validation")){
				value = parseFloat(value);
			}
		}
		
		// parse arrays
		var index = key.indexOf("[]");
		if(index > 0){
			// remove square bracket from key
			var key = key.substring(0, index);
			
			if(model[key]){
				model[key].push(value);
			}
			else
			{
				model[key] = [value];
			}
		}
		else
		{			
			model[key] = value;
		}
		
	}
	*/
	
	var elements = jQuery("#form input, #form textarea, #form select");
    for(var i=0; i<elements.length; i++){
        var input = jQuery(elements[i]);
        var name = input.attr("name");
        var type = input.attr("type");
   
        if(!name) continue;
       
        if(type == "radio" || type == "checkbox"){
            if(!input.prop("checked")) continue;
        }
       
        var value = input.val() || '';
        /* trim white spaces */
        value = value.trim();
       
        if("number" == jQuery(input).attr("data-validation")){
            value = parseFloat(value);
        }
   
        var key = name;
       
        /* parse arrays */
        var index = key.indexOf("[]");
        if(index > 0){
            /* remove square bracket from key */
            var key = key.substring(0, index);
           
            if(model[key]){
                model[key].push(value);
            }
            else
            {
                model[key] = [value];
            }
        }
        else
        {           
            model[key] = value;
        }
    }
	
};

DataTable.utils.populateSelect = function(select, jsonArray, label, value){

	var select = jQuery(select);
	select.html("<option value=''></option>");
	
	if (jsonArray != null)
	for(var i=0;i<jsonArray.length;i++)
	{
		var json = jsonArray[i];
		select.append(jQuery("<option/>",{value:json[value], html:json[label]}));
	}
};

/**
 * Takes a select and returns the text of the option selected
 */
DataTable.utils.getSelectedOptionText = function(select){
	
	var index = select.selectedIndex;	
	var option = select.options[index];
	var name = jQuery(option).text();
	
	return name;
};
