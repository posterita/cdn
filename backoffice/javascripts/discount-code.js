BackOffice.ID_COLUMN_NAME = "u_pos_discountcode_id";
BackOffice.MODEL_NAME = "X_U_POS_DiscountCode";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveDiscountCode";

BackOffice.defaults = {
		"u_pos_discountcode_id":0,
		"isactive": "Y",
		"name": "",
		"percentage":0,
		"criteria":"",
		"discountcap":0
		};

jQuery(function(){
	jQuery('#filter_button').on("click", function(){
		BackOffice.requestDataTableData();
	});
});

BackOffice.requestDataTableData = function(){
	
	jQuery.post("DataTableAction.do", { action: "getDiscountCodes", isactive: jQuery('#filter_isactive').val()},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var discountCodes = json["discount_codes"];
								
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = discountCodes;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(discountCodes);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.afterSave = function(model){
	
	BackOffice.renderDataTable();
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	  	      { "data": "name" },
	  	      { "data": "percentage"},
			  { "data": "discountcap"},
	  	      { "data": "isactive"},
	  	      { "data": "updated"},
	  	      { "data": "updatedby"}
		      
  	    ],
  	    "rowCallback" : function(row, data){
  	    	
  	    	var isactive = data.isactive; 
        	
        	if(isactive == 'N')
        	{
        		jQuery(row).css('text-decoration','line-through');
        		jQuery(row).css('font-style','italic');
        	}
  	    } ,
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      }	      
	    ],
	    
	    "order": [[ 2, "desc" ], [ 0, "asc" ]]
	});
};

BackOffice.beforeSave = function(model) {
	
	/*var date = moment();
	var today = date.format("MM-DD-YYYY");
	
	var expiryDate = jQuery("#datepicker").val();
	
	if(today > expiryDate)
	{
		alert("Invalid expiry date");
	}*/
	
	
}

BackOffice.afterShowDetail = function(){
	
	var model = this.model || this.defaults;
		
	
	
		
}

BackOffice.afterCancel = function(){
	
	jQuery('.backoffice-btn-save').attr('style','background-color: #fff !important; color:#1d1d1d; border-color:#bdbec2 !important;');
};

BackOffice.afterSave = function(model){

	BackOffice.requestDataTableData();	
};

jQuery(document).ready(function(){
	/*jQuery('.backoffice-btn-save').on('click',function(){
				
		var form = jQuery("#form").get(0);
	    BackOffice.save(form);
		
	});*/
		
});
