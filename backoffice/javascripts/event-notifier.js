BackOffice.ID_COLUMN_NAME = "u_pos_eventnotifier_id";
BackOffice.MODEL_NAME = "MPOSEventNotifier";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveEventNotifier";

BackOffice.curSymbol = "$";

BackOffice.defaults = { "phone": "",
        "sendemail": "N",
        "refundamt": 0,
        "closetill": "N",
        "void": "N",
        "exchangeamt": 0,
        "sendsms": "N",
        "inventorycount": "N",
        "salesamt": 0,
        "u_pos_eventnotifier_id": 0,
        "email": "",
        "description": "",
        "sales": "N",
        "refund": "N",
        "name": "",
        "currentmoneyinterminal_frequency": 0,
        "currentmoneyinterminal": "N",
        "exchange": "N",
        "redeemcoupon": "N",
        "customerdiscountcode": "N",
        "requestitems": "N",
        "lowstock":"N",
        "isactive": "Y"
};


BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getEventNotifiers"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}  
				
				var eventnotifiers = json["eventnotifiers"];
				var orgs = json["orgs"];
				
				BackOffice.curSymbol =  json["cursymbol"];
				BackOffice["orgs"] = orgs;
				
				for(i=0;i<eventnotifiers.length;i++){
					var orgId = eventnotifiers[i].ad_org_id;
					
					if(orgId == 0)
					{
						eventnotifiers[i].ad_org_name = "All Stores";
					}
				}
				
				//BackOffice.setDataSource(eventnotifiers);
				DataTable.utils.populateSelect(jQuery("#ad_org_id"), orgs, "ad_org_name", "ad_org_id");
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = eventnotifiers;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(eventnotifiers);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	      { "data": "ad_org_name" },
	      { "data": "name" },
	      { "data": "description"},
	      { "data": "isactive"}
	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	        	var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 1
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 3
	      }
	    ]
	});
};



BackOffice.afterCloseAlert = function() {
	
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	var orgs = BackOffice["orgs"];
	
	for(i=0;i<orgs.length;i++){
		var orgId = orgs[i].ad_org_id;
		
		if(orgId == 0)
		{
			orgs[i].ad_org_name = "All Stores";
		}
	}
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

Handlebars.registerHelper('isOptionWithAmountActive', function(isactive, message, amt) {
	
	if (isactive == "Y") {
		return new Handlebars.SafeString(
				"<p>" + message + " " + BackOffice.curSymbol + amt + "</p>"
			);
	}
	
	return;
});

Handlebars.registerHelper('isOptionActive', function(isactive, message) {
	
	if (isactive == "Y") {
		return new Handlebars.SafeString(
				"<p>" + message + "</p>"
			);
	}
	
	return;
});

jQuery(document).ready(function(){
	
	var fields = {
			 
				name : {
	    			tooltip : Translation.translate("tooltip.name.of.notifier")
				},
				ad_org_id : {	
					tooltip : Translation.translate("tooltip.select.store.notifier") 
				},
				email : {	
					tooltip : Translation.translate("tooltip.enter.email.add.toreceive") 
				},
				sales : {	
					tooltip : Translation.translate("tooltip.select.if.wish.to.receive.notif"),
					position: 'top'
				},
				salesamt :{
					tooltip: Translation.translate("tooltip.sales.notif.will.occur"),
						position: 'bottom'
				},
				refund:{
					tooltip: Translation.translate("tooltip.select.if.wish.notif.for.home"),
					position: 'top'
				},
				refundamt: {
					tooltip: Translation.translate("tooltip.refund.notif.after.no.entered"),
					position: 'bottom'
				},
				exchange: {
					tooltip: Translation.translate("tooltip.select.if.wish.notif.exchanges"),
					position: 'top'
				},
				exchangeamt: {
					tooltip: Translation.translate("tooltip.exchange.notif.occur.after.exceeded"),
					position: 'bottom'
				},
				void: {
					tooltip: Translation.translate("tooltip.select.if.you.wish.notif.transactions"),
					position: 'bottom'
				},
				closetill: {
					tooltip: Translation.translate("tooltip.select.wish.notif.end.of.the.day")
				}
				
	    };
	 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
	
	
	var salesAmt_checkbox = jQuery("#sales");
	salesAmt_checkbox.on("change", function(e){
		var container = jQuery("#sales-amt-container");	
		
		if(salesAmt_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var refundAmt_checkbox = jQuery("#refund");
	refundAmt_checkbox.on("change", function(e){
		var container = jQuery("#refund-amt-container");	
		
		if(refundAmt_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var exchangeAmt_checkbox = jQuery("#exchange");
	exchangeAmt_checkbox.on("change", function(e){
		var container = jQuery("#exchange-amt-container");	
		
		if(exchangeAmt_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
});

BackOffice.onShowDetail = function(){
	var model = this.model;
	
	if (model==null){
		return;
	}
	
	if(model["sales"] == "Y")
	{
		var container = jQuery("#sales-amt-container");	
		container.show();
	}
	else
	{
		var container = jQuery("#sales-amt-container");	
		container.hide();
	}
	
	if(model["refund"] == "Y")
	{
		var container = jQuery("#refund-amt-container");	
		container.show();
	}
	else
	{
		var container = jQuery("#refund-amt-container");
		container.hide();
	}
	
	if(model["exchange"] == "Y")
	{
		var container = jQuery("#exchange-amt-container");	
		container.show();
	}
	else
	{
		var container = jQuery("#exchange-amt-container");	
		container.hide();
	}	
	
	jQuery("#categories li a").on("click",function(){
		 
	 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
	 	{
		 jQuery(".ui-tabs-selected")
		 .children("i")
		 .removeClass("fa arrow-icons fa-chevron-down")
		 .addClass("fa arrow-icons fa-chevron-right");
	 	}		  
	  if((jQuery("#categories li").not(".ui-tabs-selected")))
	  	{
		 jQuery("#categories li").not(".ui-tabs-selected")
		 .children("i")
		 .removeClass("fa arrow-icons fa-chevron-right")
		 .addClass("fa arrow-icons fa-chevron-down");
	  	}
	});
	
	jQuery("option[value='0']","#ad_org_id").text('All Stores');
}

BackOffice.onCreate = function(){
	
	jQuery("#sales-amt-container").hide();
	jQuery("#refund-amt-container").hide();
	jQuery("#exchange-amt-container").hide();
	
	jQuery("#categories li a").on("click",function(){
		 
	 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
	 	{
		 jQuery(".ui-tabs-selected")
		 .children("i")
		 .removeClass("fa arrow-icons fa-chevron-down")
		 .addClass("fa arrow-icons fa-chevron-right");
	 	}		  
	  if((jQuery("#categories li").not(".ui-tabs-selected")))
	  	{
		 jQuery("#categories li").not(".ui-tabs-selected")
		 .children("i")
		 .removeClass("fa arrow-icons fa-chevron-right")
		 .addClass("fa arrow-icons fa-chevron-down");
	  	}
	});
	
	jQuery("option[value='0']","#ad_org_id").text('All Stores');	
	
}

Handlebars.registerHelper("getSelectName", function(select_id, id) {

	var result = jQuery("#" + select_id + " option[value='" + id + "']").text();
	
	if (result == '*')
	{
		result = 'All Stores';
	}
	
	return result;
	
});


/*Handlebars.registerHelper('allStores', function(ad_org_id) {
	
	if (ad_org_id == 0) {
		return new Handlebars.SafeString(
				"<div class=''><p class='center-block'>All Stores</p></div>"
			);
	}
	
	return;
});*/