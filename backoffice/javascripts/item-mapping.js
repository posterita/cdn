BackOffice.ID_COLUMN_NAME = "u_pos_itemmapping_id";
BackOffice.MODEL_NAME = "MPOSItemMappping";
//BackOffice.SAVE_URL = "DataTableAction.do?action=saveItemMapping";

BackOffice.defaults = {
    "u_pos_itemmapping_id": 0,
    "location": ""
};

BackOffice.requestDataTableData = function() {
};

BackOffice.renderMappingTable = function(m_warehouse_id){
	
	//delete previous table
    if ( jQuery.fn.dataTable.isDataTable('#item-mapping-table') ) {
		jQuery('#item-mapping-table').dataTable().fnDestroy();
	}
    
    BackOffice.dt = jQuery('#item-mapping-table').dataTable({
		"bServerSide": true,
	    "sAjaxSource": "DataTableAction.do?action=getItemMappingWarehouse2&m_warehouse_id=" + m_warehouse_id,
		  "fnPreDrawCallback": function() {
	      },
	      "columns": [
	        	{
	                "data": "name"
	            },
	            {
	                "data": "description"
	            },
	            {
	                "data": "barcode"
	            },
	            {
	                "data": "location"
	            },
	            //{"data":"qty"},
	            {
	                "data": "u_pos_itemmapping_id"
	            }
	        ],
  	  "columnDefs": [{
          "render": function(data, type, row) {

              return '<button type="button" class="btn btn-default" onclick="BackOffice.deleteItemMapping(' + data + ',' + row['u_pos_itemmapping_id'] + ')"><span class="glyphicon glyphicon-trash"></span></button>';
          },
          "targets": 4
      },
      {
          className: "center",
          "targets": 4
      }
  ]
	});
    
    
    /*
    jQuery('#item-mapping-table').dataTable({
        //"bServerSide": true,
        "data": mappings,
        "columns": [
        	{
                "data": "name"
            },
            {
                "data": "description"
            },
            {
                "data": "barcode"
            },
            {
                "data": "location"
            },
            //{"data":"qty"},
            {
                "data": "u_pos_itemmapping_id"
            }
        ],
        "columnDefs": [{
                "render": function(data, type, row) {

                    return '<button type="button" class="btn btn-default" onclick="BackOffice.deleteItemMapping(' + data + ',' + row['u_pos_itemmapping_id'] + ')"><span class="glyphicon glyphicon-trash"></span></button>';
                },
                "targets": 4
            },
            {
                className: "center",
                "targets": 4
            }
        ]
    });
    */
    /* data table*/
	
};

jQuery(document).ready(function() {

    jQuery("#m_warehouse_id").on('change', function() {

        var m_warehouse_id = jQuery(this).val();

        if (m_warehouse_id > 0) {
        	BackOffice.renderMappingTable(m_warehouse_id);
        }
        else
        {
        	BackOffice.renderMappingTable([]);
        }

    });

    //render warehouse list
    jQuery.post("DataTableAction.do", {
            action: "getWarehousesList"
        },
        function(json, textStatus, jqXHR) {

            if (json == null || jqXHR.status != 200) {
                alert("Failed to request warehouses!");
                return;
            }

            //render warehouse list
            DataTable.utils.populateSelect(jQuery("#m_warehouse_id"), json, "name", "value");

            //check for hash returned by importer
            var hash = window.location.hash;
            if (hash.length > 1) {
                hash = hash.substring(1);

                //set warehouse id
                jQuery("#m_warehouse_id").val(hash);
                jQuery("#m_warehouse_id").trigger('change');
            }

        }, "json").fail(function() {
        alert("Failed to request warehouses!");
    });
    
    /* view page */
    var products;		
	
    /*
	jQuery.post("DataTableAction.do?action=getProductsAutocomplete", function(data){
		products = TAFFY(data.names);
		
		BackOffice.products = products;
					
		jQuery("#product-name").autocomplete({
		    source: function( request, response ) {
		    	
		    	var searchTerm = request.term;
		    			    	
		    	var results = products({"upc":searchTerm}).get();
		    	if(results.length == 0){
		    		
		    		results = products({"value":{leftnocase:searchTerm}}).limit(5).get();
		    	}
		    	
		    	if (results.length === 0) {
		    		results = [{
		    			"value":"No Results",
		    			"id":-1,
		    			"description":""
		    		}];
		        }
		    	
		    	response( results );
		    	
		    },
		    minlength: 2,
		    focus: function(event, ui) {
		        jQuery("#product-name").val(ui.item.value);
		        return false;
		    },
		    select: function(event, ui) {
		    	
		    	if(ui.item.id == -1){
		    		
		    		alert("Item not found!");
		    		
		    		document.getElementById("product-name").select();
		    		
		    		return false;
		    	}
		    	
		        jQuery("#product-name").val(ui.item.value);
		        jQuery("#product-id").val(ui.item.id);
		        return false;
		    }
		}).data("autocomplete")._renderItem = function(ul, item) {
		    return jQuery("<li></li>")
		        .append("<a><div>" + item.value + "</div><div>" + item.description + "</div></a>")
		        .data("item.autocomplete", item)
		        .appendTo(ul);
		};			
	});	
	*/
    
    jQuery("#product-name").autocomplete({
	    source: function( request, response ) {
	    	
	    	var searchTerm = request.term;
	    	
	    	var url = "DataTableAction.do?action=autocomplete&table=m_product&columns=name,description,upc&term=" + searchTerm;
	    			    	
	    	jQuery.getJSON( url, request, function( data, status, xhr ) {
	            response( data );
	          });
	    	
	    },
	    minlength: 2,
	    focus: function(event, ui) {
	        jQuery("#product-name").val(ui.item.name);
	        return false;
	    },
	    select: function(event, ui) {
	    	
	    	if(ui.item.m_product_id == -1){
	    		
	    		alert("Item not found!");
	    		
	    		document.getElementById("product-name").select();
	    		
	    		return false;
	    	}
	    	
	        jQuery("#product-name").val(ui.item.name);
	        jQuery("#product-id").val(ui.item.m_product_id);
	        return false;
	    }
	}).data("autocomplete")._renderItem = function(ul, item) {
	    return jQuery("<li style='border-bottom:solid 1px #CCC;'></li>")
	        .append("<a><div><strong>" + item.name + "</strong></div><div>" + item.description + "</div>" + "</div><div>" + item.upc + "</div></a>")
	        .data("item.autocomplete", item)
	        .appendTo(ul);
	};
	

	jQuery('#save-button').on('click', function(){
		
		BackOffice.saveMapping();
		
	});
	
	jQuery('#cancel-button').on('click', function(){
		
		jQuery("#mapping-master-container").show();
		jQuery("#mapping-detail-container").hide();
		
	});

});

//add actions
BackOffice.addMapping = function(){
	
	BackOffice.clearMessage();
	
	var warehouse_id = jQuery("#m_warehouse_id").val();
	
	if( warehouse_id > 0){
			
	}
	else
	{
		alert("Please select a warehouse");
		return;
	}
	
	document.getElementById('add-mapping-form').reset();
	
	var warehouseName = DataTable.utils.getSelectedOptionText(jQuery("#m_warehouse_id").get(0));
	jQuery('#warehouse-name').html(warehouseName);
	
	jQuery("#mapping-master-container").hide();
	jQuery("#mapping-detail-container").show();
	
	document.getElementById('product-name').focus();
}

BackOffice.saveMapping = function() {
	
	var post = {};
	  
	  var location = jQuery("input[name='location']").val();
	  post["location"] = location;
	  
	  /*
	  var qty = jQuery("input[name='qty']").val();
	  post["qty"] = qty;
	  */
	  
	  var m_warehouse_id = jQuery("#m_warehouse_id").val();
	  var m_product_id = jQuery("#product-id").val();
	  
	  post["m_product_id"] = m_product_id;
	  post["m_warehouse_id"] = m_warehouse_id;
	  
	  if(m_product_id == null || m_product_id.length == 0){
		  
		  alert('Item is required');
		  document.getElementById('product-name').select();
		  return;
		  
	  }
	  
	  if(location == null || location.length == 0){
		  
		  alert('Location is required');
		  document.getElementById('location').select();
		  return;
		  
	  }
	  
	  /*
	  //add validation
	  if(isNaN(parseInt(qty))){
		  
		  alert('Invalid Quantity');
		  document.getElementById('qty').select();
		  return;		  
	  }	  
	  */
	  
	  
	  if (post["m_product_id"] == 0)
	  {
		  BackOffice.displayError(Translation.translate("product.not.found.on.system") + "!");
	  }
	  else
	  {
		  jQuery.post("DataTableAction.do?action=saveItemMapping", {json : Object.toJSON(post)},
					function(json, textStatus, jqXHR){	
						
						if(json == null || jqXHR.status != 200){
							alert("Failed to save mapping!"); 
							return;
						}
						
						BackOffice.displayMessage("Item mapping saved");
						
						jQuery("#mapping-master-container").show();
						jQuery("#mapping-detail-container").hide();
						
						//reload mappings
						jQuery("#m_warehouse_id").trigger('change');
						
						
					},"json").fail(function(){
						BackOffice.displayError(json.error);
					});
	  };
	
}

BackOffice.deleteItemMapping = function(id) {	
	
	var _delete = function(){
		
		var m_warehouse_id = jQuery("#m_warehouse_id").val();
		
		var params = {
					action: "deleteItemMapping",
					u_pos_itemmapping_id: id
				};

		jQuery.ajax({
			type: "POST",
			url: "DataTableAction.do",
			data: params
		}).done(function(data, textStatus, jqXHR){
		
			if(data == null || jqXHR.status != 200){
				alert('Failed to delete!');
			}
			
			var json = JSON.parse(data);
			
			if(json.success)
			{
				BackOffice.displayMessage(json.message);
				
				//reload mappings
				jQuery("#m_warehouse_id").trigger('change');
			}
			else
			{
				BackOffice.displayError(json.message);
			}
		});
		
	};
	
	var choice = window.confirm("Do you want to delete mapping?");
	
	if(choice == true){
		_delete();
	}
	
};

BackOffice.importItemMapping = function(){	
	var warehouse_id = jQuery("#m_warehouse_id").val();
	
	if( warehouse_id > 0){
		
		window.location = 'importer.do?importer=ItemMappingImporter&warehouseId='+warehouse_id;
		
	}
	else
	{
		alert("Please select a warehouse");
	}	
	
};

BackOffice.exportToCsv = function(f){	
	var warehouse_id = jQuery("#m_warehouse_id").val();
	
	if( warehouse_id > 0){
		
		window.location = "DataTableAction.do?action=exportItemMapping&warehouse_id="+warehouse_id;
		
	}
	else
	{
		alert("Please select a warehouse");
	}
	
};

/* reset mappings */
BackOffice.resetItemMappings = function() {	

	var m_warehouse_id = jQuery("#m_warehouse_id").val();
	
	if(m_warehouse_id == "") return;
	
	var _reset = function(){		
		
		var params = {
					"action" : "resetItemMappings",
					"m_warehouse_id" : m_warehouse_id
				};

		jQuery.ajax({
			type: "POST",
			url: "DataTableAction.do",
			data: params
		}).done(function(data, textStatus, jqXHR){
		
			if(data == null || jqXHR.status != 200){
				alert('Failed to delete!');
			}
			
			var json = JSON.parse(data);
			
			if(json.success)
			{
				BackOffice.displayMessage(json.message);
				
				//reload mappings
				jQuery("#m_warehouse_id").trigger('change');
			}
			else
			{
				BackOffice.displayError(json.message);
			}
		});
		
	};
	
	var choice = window.confirm("Do you want to reset all mappings?");
	
	if(choice == true){
		_reset();
	}
	
};


