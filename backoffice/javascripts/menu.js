/* menu builder */
var MenuManager = {
		callbacks : jQuery.Callbacks(),
		json : null,
		initDB : function(){
			this.db = TAFFY(this.json);
			this.loadBreadcrumbs();
		},
		
		getParentMenu : function(parent_id){
			return this.db({'parent_id':parent_id}).get();
		},
		
		loadBreadcrumbs : function(){
			
			var breadcrumbs = [];
			
			var pmenu, categories, category, menus, menu, menuLink, index;
			
			for(var i=0; i<this.json.length; i++){
				
				pmenu = this.json[i];
				
				categories = pmenu.categories;
				
				for(var j=0; j<categories.length; j++){
					
					category = categories[j];
					
					menus = category.menus;
					
					for(var k=0; k<menus.length; k++){
						
						menu = menus[k];
						
						menuLink = menu.menuLink;
						index = menuLink.indexOf('pmenuId');
						if(index > 0){
							menuLink = menuLink.substr(0, index - 1);
						}
						
						breadcrumbs.push({
							'parent' : pmenu.parent,
							'parent_id' : pmenu.parent_id,
							'id' : menu.id,
							'name' : menu.name,
							'menuLink' : menuLink
						})
					}
				}
			}
			
			this.breadcrumbs = TAFFY(breadcrumbs);
			
		},
		
		getBreadcrumbs : function(){
			
			//get breadcrumbs from url
			//first look for menuId
			var reg = /[\?&]menuId=(\d*)/g;
			var matches = reg.exec(window.location.search + '');
			
			var menuId, menu;
			
			if(matches != null && matches.length > 2){				
				menuId = matches[1];				
				menu = this.breadcrumbs({'id':menuId}).first();	
			}
			else
			{
				var loc = window.location.href;
				
				var end = loc.lastIndexOf("?");
				var start = loc.lastIndexOf("/") +1;
				
				var link = end > 0 ? loc.substring(start, end) : loc.substring(start);
				menu = this.breadcrumbs({'menuLink':link}).first();	
			}
			
			if(!menu){
				return [];
			}
			
			
			
			return [
				{
					'id' : menu.parent_id,
					'name' : Translation.translate(menu.parent), 
					'link' : "backoffice-menus.do#pmenuId=" + menu.parent_id 
				}, 
				{
					'id' : menu.id,
					'name' : Translation.translate(menu.name), 
					'link' : menu.menuLink
				}
			];
			
		},
				
		init : function()
		{
			if(this.json != null){
				this.initDB();
				MenuManager.callbacks.fire('menus loaded');
			}
			
			var json = this.json;
			var roleId = sessionStorage.getItem('MENU_ROLE_ID');
			
						
			if(json == null){
				
				if(TerminalManager.terminal.userRole.roleId != roleId){
					sessionStorage.removeItem('MENU_JSON'); /* invalidate cache */
				}
				
				/* load menu from sessionstorage */
				/* TerminalManager.terminal.userRole.roleId */
				json = sessionStorage.getItem('MENU_JSON');
				
				if(json == null){
					/* load menu from server */
					jQuery.post('MenuAction.do?action=getMenuJSON',
							{},
				    		function(json, textStatus, jqXHR){
				    			
				    			if(json == null || jqXHR.status != 200){
				    				alert('Failed to load menu');
				    				return;
				    			}
				    			
				    			if(json.error != null)
				    			{
				    				alert(json.error);
				    				return;
				    			}
				    			
				    			/* set menu to sessionstorage */
				    			sessionStorage.setItem('MENU_JSON', jqXHR.responseText);
				    			sessionStorage.setItem('MENU_ROLE_ID', TerminalManager.terminal.userRole.roleId);
				    			MenuManager.json = JSON.parse(jqXHR.responseText);
				    			MenuManager.initDB();
				    			MenuManager.callbacks.fire('menus loaded');
							});
					
				}
				else
				{
					this.json = JSON.parse(json);
					MenuManager.initDB();
					MenuManager.callbacks.fire('menus loaded');
				}
			}			
			
		},
		onReady : function(fn){
			this.callbacks.add(fn);
		}
};

jQuery(document).ready(function() {
	MenuManager.init();
});

MenuManager.logout = function(url){
	if(window.confirm("Do you want to logout?")) {
		getURL('LoginAction.do?action=logout');
	}	
};

function createPOFromIsync()
{
	var preloader = new Dialog(Translation.translate("processing") + " ...");
	preloader.show();
	
	jQuery.ajax({
		type: "POST",
		url: "IsyncAction.do",
		data: "action=createPurchaseOrder"
	}).done(function(data, textStatus, jqXHR){
		
		preloader.hide();
		
		if(data == null || jqXHR.status != 200){
    		alert('Failed to create purchase order from Isync!');
    	}
    	
    	var json = JSON.parse(data);
    	
    	alert(json.message);
    	
    	var url = 'report-view-purchase-order-history.do?date-from=' + moment(new Date()).format("DD MM YYYY") + '&date-to=' +  moment(new Date()).format("DD MM YYYY");
    	
    	getURL(url);
	});
}