BackOffice.ID_COLUMN_NAME = "u_modifiergroup_id";
BackOffice.MODEL_NAME = "MModifierGroup";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveModifierGroup";

BackOffice.defaults = {
		"u_modifiergroup_id" : 0,
		"u_modifiergroup_isexclusive" : "N",
		"u_modifiergroup_ismandatory" : "N",
		"isactive": "Y",
		"modifiers" : []
	};


BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getModifierGroups"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}  
				
				var modifiergroups = json["modifiergroups"];
				//BackOffice.setDataSource(modifiergroups);		
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = modifiergroups;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(modifiergroups);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};


BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	      { "data": "u_modifiergroup_name" },
	      { "data": "u_modifiergroup_isexclusive"}, 
	      { "data": "u_modifiergroup_ismandatory"},
	      { "data": "isactive"}
	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 3
	      }
	    ]
	});
};

BackOffice.beforeSave = function(model){
	/* add changes made to modifiers */
	var modifiers = [];
	
	var rows = jQuery("#modifiers-table-tbody tr");
	for(var i=0; i<rows.length; i++){
		var row = rows[i];
		
		var inputs = jQuery(row).find("input");
		
		var modifier = {};
		
		for(var j=0; j<inputs.length; j++){
			
			var input = jQuery(inputs[j]);
			var name = input.attr("name");
			var value = input.attr("value");
			
			modifier[name] = value;
		}
		
		modifiers.push(modifier);
	}
	
	model.modifiers = modifiers;
};

BackOffice.afterSave = function(model){
	/* clear modifiers to delete */
	jQuery("#u_modifier_id_to_delete").val("");
	
	/*update modifiers in case user save again*/
	
	var modifiers = model["modifiers"];
	
	for(var i=0; i<modifiers.length; i++)
	{
		jQuery('input[name=u_modifier_id]')[i].value = modifiers[i]["u_modifier_id"];
		jQuery('input[name=u_modifier_m_product_id]')[i].value = modifiers[i]["u_modifier_m_product_id"];
		jQuery('input[name=u_modifier_name]')[i].value = modifiers[i]["u_modifier_name"];
		jQuery('input[name=u_modifier_position]')[i].value = modifiers[i]["u_modifier_position"];
		jQuery('input[name=u_modifier_price]')[i].value = modifiers[i]["u_modifier_price"];
	}
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

BackOffice.onShowDetail = function(){
	/* draw modifier table */
	var modifiers = [];	
	var model = this.model;	
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	if(model != null){
		modifiers = model["modifiers"];
	}
	
	renderModifiers(modifiers);
};

BackOffice.onCreate = function(){
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
}

function renderModifiers(modifiers){
	/* clear modifiers to delete */
	jQuery("#u_modifier_id_to_delete").val("");
	
	/* draw modifier table rows */
	jQuery("#modifiers-table-tbody tr").remove();
	
	for(var i=0; i<modifiers.length; i++){
		var modifier = modifiers[i];
		addModifier(modifier);
	}
}

function addModifier(modifier){
	/* draw new modifier row*/
	if(modifier == null){
		
		//var position = BackOffice.model["modifiers"].length + 1;
		
		var position = jQuery("#modifiers-table-tbody")[0].rows.length + 1;
		
		modifier = {
			"u_modifier_id" : 0,
			"u_modifier_m_product_id" : 0,
			"u_modifier_name" : "",
			"u_modifier_position" : position,
			"u_modifier_price" : "0.00"
		};
	}
	
	var tbody = jQuery("#modifiers-table-tbody");
	var row = '<tr>' +
	'<td>' +
		'<input type="hidden" id="u_modifier_id" name="u_modifier_id" value="' + modifier.u_modifier_id + '">' +
		'<input type="hidden" id="u_modifier_m_product_id" name="u_modifier_m_product_id" value="' + modifier.u_modifier_m_product_id + '">' +
		'<input type="text" id="u_modifier_name" name="u_modifier_name" value="' + modifier.u_modifier_name + '" data-validation="required" data-validation-error-msg="'+ Translation.translate("name.is.required") +'" class="required">' +
	'</td>' +
	'<td><input type="text" id="u_modifier_position" name="u_modifier_position" value="' + modifier.u_modifier_position + '" data-validation="number" data-validation-error-msg="'+ Translation.translate("position.must.be.number") +'" data-validation-optional="true"></td>' +
	'<td><input type="text" id="u_modifier_price" name="u_modifier_price" value="' + modifier.u_modifier_price + '" data-validation="number" data-validation-error-msg="'+ Translation.translate("price.must.be.number") +'" data-validation-allowing="float"></td>' +
	'<td><button type="button" class="btn btn-default" onclick="deleteModifier(this);"><span class="glyphicon glyphicon-trash"></span></button></td>' +
	'</tr>';
	
	tbody.append(row);
}

function deleteModifier(button){
	/* delete modifier row and add modifier id to deleted modifiers list */
	var row = jQuery(button).parentsUntil("tbody");
	var input = jQuery(row).find("#u_modifier_id");
	
	var u_modifier_id = input.val();
	
	if(u_modifier_id == 0){
		row.remove();
		return;
	}
	
	var u_modifier_id_to_delete = jQuery("#u_modifier_id_to_delete").val();
	
	if(u_modifier_id_to_delete.length > 0)
	{
		u_modifier_id_to_delete = u_modifier_id_to_delete + ",";
	}
	
	u_modifier_id_to_delete = u_modifier_id_to_delete + u_modifier_id;
	jQuery("#u_modifier_id_to_delete").val(u_modifier_id_to_delete);
	
	row.remove();
}

jQuery(document).ready(function(e){
	jQuery("#add-modifier-button").on("click", function(e){
		BackOffice.clearMessage();
		
		addModifier(null);
	});
	
	var fields = {			
			u_modifiergroup_name : {
				tooltip: Translation.translate("create.name.modifier.group.tooltip")	
				},
			u_modifiergroup_isexclusive : {
				tooltip: "<strong>"+ Translation.translate("exclusive")+"</strong> -" + Translation.translate("only.one.item.selected.modifier.group.tooltip")+"</br><strong>" + Translation.translate("mandatory")+"</strong> -" +Translation.translate("at.least.one.item.selected.modifier.group.tooltip") + "</br><strong>" + Translation.translate("mandatory.exclusive") +"</strong> -"+Translation.translate("maximum.one.item.must.be.selected.modifier.group.tooltip")+"</br>"+ Translation.translate("if.these.not.checked.modifier.group.optional.tooltip"),
				position : 'top'
				},
			u_modifiergroup_ismandatory : {
				tooltip:"<strong>"+ Translation.translate("exclusive")+"</strong> -" + Translation.translate("only.one.item.selected.modifier.group.tooltip")+"</br><strong>" + Translation.translate("mandatory")+"</strong> -" +Translation.translate("at.least.one.item.selected.modifier.group.tooltip") + "</br><strong>" + Translation.translate("mandatory.exclusive") +"</strong> -"+Translation.translate("maximum.one.item.must.be.selected.modifier.group.tooltip")+"</br>"+ Translation.translate("if.these.not.checked.modifier.group.optional.tooltip"),
				position : 'top'
				}
			};
	
	//Include Global Color 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
});