BackOffice.ID_COLUMN_NAME = "c_paymentprocessor_id";
BackOffice.MODEL_NAME = "MPaymentProcessor";
BackOffice.SAVE_URL = "DataTableAction.do?action=savePaymentProcessor";

BackOffice.defaults = {
			"c_paymentprocessor_id":0,
			"hostaddress":"",
			"userid":"",
			"password":"",
			"partnerid":"",
			"vendorid":"",
			"isactive": "Y"
		};


BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getPaymentProcessors"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}  
				
				var orgs = json["orgs"];				
				var paymentprocessors = json["paymentprocessors"];
				
				//BackOffice.setDataSource(paymentprocessors);	
				
				/* render select */
				DataTable.utils.populateSelect(jQuery("#ad_org_id"), orgs, "ad_org_name", "ad_org_id");
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = paymentprocessors;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(paymentprocessors);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};


BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	      { "data": "ad_org_name" },
	      { "data": "c_paymentprocessor_name"},
	      { "data": "isactive"}
	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 1
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 2
	      }
	    ]
	});
};

BackOffice.onShowDetail = function(){
	if(this.model == null) return;
	
	var model = this.model;	
	var payprocessorclass = model["payprocessorclass"];
	renderPaymentProcessorFields(payprocessorclass);
	
	DataTable.utils.mapModelToForm(model, jQuery("#form"));	
	
	jQuery("option[value='0']","#ad_org_id").text('All Stores');
};

jQuery(document).ready(function(e){
	jQuery("#payprocessorclass").on("change",function(e){
		var payprocessorclass = jQuery(this).val();
		renderPaymentProcessorFields(payprocessorclass);		
	})
	
	
	var fields = {
		
		ad_org_id:{
			tooltip:Translation.translate("select.this.payment.processor.apply.stores.tooltip")
		},
		payprocessorclass :{
			tooltip: Translation.translate("select.processor.from.the.list.tooltip")
		}
		
};

jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});

});

/* some functions */
function renderPaymentProcessorFields(payprocessorclass){
	
	/* org.compiere.model.PP_Authorize ==> PP_Authorize */
	var index = payprocessorclass.lastIndexOf(".");
	if(index >= 0){
		payprocessorclass = payprocessorclass.substring(index + 1);
	}
	
	var id = "#" + payprocessorclass;
	
	var div = jQuery(id);
	var html = "";
	
	if(div.length > 0){
		html = div.html();
	}
	
	jQuery("#paymentprocessor-fields-container").html(html);
}

BackOffice.beforeSave = function(model) {
	var select = jQuery("#payprocessorclass").get(0);
	model["c_paymentprocessor_name"] = DataTable.utils.getSelectedOptionText(select);
};

BackOffice.onCreate = function(){
	jQuery("option[value='0']","#ad_org_id").text('All Stores');	
};



BackOffice.afterRenderView = function(id) {
	BackOffice.preloader.show();
	
	var model = this.model;
	var payprocessorclass = model["payprocessorclass"];
	
	showPaymentProcessorsDetails(payprocessorclass);
	
	BackOffice.preloader.hide();
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

function showPaymentProcessorsDetails(payprocessorclass) {
	
	var index = payprocessorclass.lastIndexOf(".");
	if(index >= 0){
		payprocessorclass = payprocessorclass.substring(index + 1);
	}
	
	var id = "#view-" + payprocessorclass;

	var source   = jQuery(id).html();
	var template = Handlebars.compile(source);
	
	var password = BackOffice.model.password;
	
	if (password != undefined && password != null)
	{
		for(var i=0; i<password.length; i++)
		{
			var char = password.charAt(i);
			password = password.replace(char, '*');
		}
		
		BackOffice.model.hiddenpassword = password;
	}
	
	jQuery("#show-payment-processors-details").html(template(BackOffice.model));
}