BackOffice.ID_COLUMN_NAME = "c_paymentterm_id";
BackOffice.MODEL_NAME = "MPaymentTerm";
BackOffice.SAVE_URL = "DataTableAction.do?action=savePaymentTerm";

BackOffice.defaults = {
		"c_paymentterm_id":0,
		"isactive": "Y"
		};


BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getPaymentTerms"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}  
				
				var paymenterms = json["paymenterms"];				
				//BackOffice.setDataSource(paymenterms);	
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = paymenterms;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(paymenterms);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};


BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	      { "data": "name" },
	      { "data": "netdays"},
	      { "data": "isactive"}
	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 2
	      }
	    ]
	});
};

jQuery(document).ready(function(){
	var fields = {			
			name : {
				tooltip: Translation.translate("debtor.name.show.on.debtors.report.tooltip")
				},
			netdays : {
				tooltip: Translation.translate("days.sale.credit.balance.fully.due.tooltip")
				}
			};
	
	//Include Global Color 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
	
});