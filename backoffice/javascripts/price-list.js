BackOffice.ID_COLUMN_NAME = "m_pricelist_id";
BackOffice.MODEL_NAME = "MPriceList";
BackOffice.SAVE_URL = "DataTableAction.do?action=savePriceList";
var pricelistId;

BackOffice.defaults = {
		"m_pricelist_id":"0",
		issopricelist:"N", 
		istaxincluded:"N",
		"isactive": "Y",
		enforcepricelimit:"N",
		priceprecision: 2
			};

BackOffice.requestDataTableData = function(){	
	jQuery.post("DataTableAction.do", { action: "getPriceList"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}  
				
				BackOffice.JSON = json.data;
				//BackOffice.setDataSource(json.data);	
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = json.data;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
					else
					{
						BackOffice.datasource = json.data;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						BackOffice.view(hash);
					}
				}
				else
				{
					BackOffice.setDataSource(json.data);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			});
	
	//BackOffice.preloader.hide();	
};


BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	      { "data": "name" },
	      { "data": "issopricelist"}, 
	      { "data": "istaxincluded"},
	      { "data": "isactive"}
	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 3
	      }
	    ]
	});
};

BackOffice.deleteItemFromPricelist = function(id, version_id) {
	var model = BackOffice.model;
	var m_pricelist_id = model[BackOffice.ID_COLUMN_NAME];
	
	var params = {
				action: "deleteProductPrice",
				productId: id,
				pricelistId: m_pricelist_id,
				pricelistVersionId: version_id
			};

	jQuery.ajax({
		type: "POST",
		url: "DataTableAction.do",
		data: params
	}).done(function(data, textStatus, jqXHR){
	
		if(data == null || jqXHR.status != 200){
			alert('Failed to delete!');
		}
		
		var json = JSON.parse(data);
		
		if(json.success)
		{
			BackOffice.displayMessage(json.message);
			BackOffice.renderView();
		}
		else
		{
			BackOffice.displayError(json.message);
		}
	});
};

BackOffice.afterRenderView = function(id) {
	
	BackOffice.preloader.show();
	
	pricelistId = id;
	
	if ( jQuery.fn.dataTable.isDataTable('#products') ) {
		jQuery('#products').dataTable().fnDestroy();
	}
	
	dt = jQuery('#products').dataTable({
		"searching":true,
		"bServerSide": true,
	    "sAjaxSource": "DataTableAction.do?action=getProductsOnPriceList&m_pricelist_id=" + id,
	    "columns": [
	  	      { "data": "name" },
	  	      { "data": "description" },
	  	      { "data": "upc" },
		      { "data": "pricestd"}, 
		      { "data": "pricelist"}, 
		      { "data": "pricelimit"},
		      { "data": "m_product_id"}
  	    ],
	    "columnDefs": [
     	      {
     	        "render": function (data, type, row) {
     	        	/*return "<button onclick='BackOffice.deleteItemFromPricelist(" + data + ", " + row['m_pricelist_version_id'] + ")' class='btn btn-default btn-table-delete'><span class='glyphicons glyphicons-bin'></span></button>";*/

return '<button type="button" class="btn btn-default" onclick="BackOffice.deleteItemFromPricelist(' + data + ', ' + row['m_pricelist_version_id'] + ')"><span class="glyphicon glyphicon-trash"></span></button>';
     	        },
     	        "targets": 6
     	      },
     	      {
     	    	  className: "center", "targets" : 6
     	      },
     	      { 'bSortable': false, 'aTargets': [ 6 ] }
     	]
	});
	
	jQuery('#products').on( 'draw.dt', function () {
	    resizeContainerHeight();
	});
	
	BackOffice.preloader.hide();
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

jQuery(document).ready(function(){
	jQuery(function() {
		jQuery( ".datepicker" ).datepicker({
			format: "yyyy-mm-dd",
			orientation: "top left"/*,
			defaultDate : new Date()*/
		/*showOn: "button",
		buttonImage: "./images3/org.png",
		buttonImageOnly: true,
		buttonText: "Select date"*/
		});
		
		jQuery(".datepicker").on('hide', function (e) { e.preventDefault(); });
	});
	
});

BackOffice.onShowDetail = function(){
	if (jQuery( ".datepicker" ).val()==""){
		jQuery( ".datepicker" ).datepicker({
			format: "yyyy-mm-dd",
			orientation: "top left"
		}).datepicker("setDate","0");
	}
};

BackOffice.addProduct = function() {
	var model = BackOffice.model;
	
	if (model["issopricelist"] == 'Y')
	{
		jQuery('#add-product-to-sales-pricelist-form-popup').dialog({
			modal: true,
			position: { my: "center", at: "center", of: document },
			open: function(event, ui) {
				var form = jQuery('#add-product-to-sales-pricelist-form-popup form');
				form.get(0).reset();
				
				jQuery("#sales-product-id").val(0);
			},
			title: Translation.translate("add.product.to.pricelist"),
			buttons: [
			          {
			        	  text: "Add Product",
			        	  "class" : 'btn btn-primary add-product',
			        	  click: function() {
			        		  
			        		  var post = {};
			        		  
			        		  post["pricestd"] = jQuery("input[name='sales-price-form-popup-pricestd']").val();
			        		  post["pricelimit"] = jQuery("input[name='sales-price-form-popup-pricelimit']").val();
			        		  post["pricelist"] = jQuery("input[name='sales-price-form-popup-pricelist']").val();
			        		  
			        		  post["m_pricelist_version_id"] = model["m_pricelist_version_id"];
			        		  post["m_product_id"] = jQuery("#sales-product-id").val();
			        		  
			        		  if (post["m_product_id"] == 0)
			        		  {
			        			  BackOffice.displayError(Translation.translate("no.product.selected.pricelist") +"!");
			        		  }
			        		  else
		        			  {
				        		  jQuery.post("DataTableAction.do?action=saveProductOnPricelist", {json : Object.toJSON(post)},
				        					function(json, textStatus, jqXHR){	
				        						
				        						if(json == null || jqXHR.status != 200){
				        							alert("Failed to request data!"); 
				        							return;
				        						}
				        						
				        						BackOffice.displayMessage(json.success);
				        						
				        						BackOffice.renderView();
				        						
				        					},"json").fail(function(){
				        						BackOffice.displayError(json.error);
				        					});
		        			  };

			        		  jQuery( this ).dialog( "close" );
			        	  }
			           },
			           {
			        	  text: "Cancel",
			        	  "class" : 'btn cancel-product',
			        	  click: function() {
			        		  jQuery( this ).dialog( "close" );
			        	  }
				       }
			          ]
		});
		
		jQuery(".add-product").html(Translation.translate("ok"));
		jQuery(".cancel-product").html(Translation.translate("cancel"));
		
		jQuery("#sales-product-name").autocomplete({
			source: function( request, response ) {
						jQuery.ajax({
							url: "DataTableAction.do?action=getProductsNotOnPricelist",
							dataType: "json",
							data: {
								q: request.term,
								m_pricelist_id: model[BackOffice.ID_COLUMN_NAME]
							}
						}).done(function(data, textStatus, jqXHR){
							
							if(data == null || jqXHR.status != 200){
					    		alert('Failed to query!');
					    	}
							else
							{
								response(data);
							}
						})
					},
			minLength: 2,
			appendTo: "#add-product-to-sales-pricelist-form-popup",
			select: function( event, ui ) {
						jQuery("#sales-product-id").val(ui.item.id);
					}
		});
	}
	else
	{
		jQuery('#add-product-to-purchases-pricelist-form-popup').dialog({
			modal: true,
			title: Translation.translate("add.product.to.pricelist"),
			position: { my: "center", at: "center", of: document },
			open: function(event, ui) {
				var form = jQuery('#add-product-to-purchases-pricelist-form-popup form');
				form.get(0).reset();
				
				jQuery("#purchases-product-id").val(0);
			},
			buttons: [
			          {
			        	  text: "Add Product",
			        	  "class" : 'btn btn-primary add-product',
			        	  click: function() {
			        		  
			        		  var post = {};
			        		  
			        		  var pricestd = jQuery("input[name='purchase-price-form-popup-pricestd']").val();
			        		  
			        		  post["pricestd"] = pricestd;
			        		  post["pricelimit"] = pricestd;
			        		  post["pricelist"] = pricestd;
			        		  
			        		  post["m_pricelist_version_id"] = model["m_pricelist_version_id"];
			        		  post["m_product_id"] = jQuery("#purchases-product-id").val();
			        		  
			        		  if (post["m_product_id"] == 0)
			        		  {
			        			  BackOffice.displayError(Translation.translate("product.not.found.on.system") + "!");
			        		  }
			        		  else
		        			  {
				        		  jQuery.post("DataTableAction.do?action=saveProductOnPricelist", {json : Object.toJSON(post)},
				        					function(json, textStatus, jqXHR){	
				        						
				        						if(json == null || jqXHR.status != 200){
				        							alert("Failed to request data!"); 
				        							return;
				        						}
				        						
				        						BackOffice.displayMessage(json.success);
				        						
				        						BackOffice.renderView();
				        						
				        					},"json").fail(function(){
				        						BackOffice.displayError(json.error);
				        					});
		        			  };
			        		  
			        		  jQuery( this ).dialog( "close" );
			        	  }
			           },
			           {
			        	  text: "Cancel",
			        	  "class" : 'btn cancel-product',
			        	  click: function() {
			        		  jQuery( this ).dialog( "close" );
			        	  }
				       }
			          ]
		});
		
		jQuery(".add-product").html(Translation.translate("ok"));
		jQuery(".cancel-product").html(Translation.translate("cancel"));
		
		jQuery("#purchases-product-name").autocomplete({
			source: function( request, response ) {
						jQuery.ajax({
							url: "DataTableAction.do?action=getProductsNotOnPricelist",
							dataType: "json",
							data: {
								q: request.term,
								m_pricelist_id: model[BackOffice.ID_COLUMN_NAME]
							}
						}).done(function(data, textStatus, jqXHR){
							
							if(data == null || jqXHR.status != 200){
					    		alert('Failed to query!');
					    	}
							else
							{
								response(data);
							}
						})
					},
			minLength: 2,
			appendTo: "#add-product-to-purchases-pricelist-form-popup",
			select: function( event, ui ) {
						jQuery("#purchases-product-id").val(ui.item.id);
					}
		});
	};
	
};

Handlebars.registerHelper('isForSales', function(option) {
	
	if (option == "Y") {
		return "Sales only";
	}
	
	return new Handlebars.SafeString(
			"Purchases only"
		);
});

Handlebars.registerHelper('isInclusiveTax', function(option) {
	
	if (option == "Y") {
		return "Inclusive";
	}
	
	return "Not Inclusive";
});

/* launch product price importer */
BackOffice.importPrices = function(){
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	window.location = 'importer.do?importer=ProductPriceImporter&pricelistId=' + id;
};


function exportProductsOnPriceList()
{
	window.location = "DataTableAction.do?action=exportProductsOnPriceList&m_pricelist_id=" + pricelistId;
}