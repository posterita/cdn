/*Version 1.1*/
BackOffice.ID_COLUMN_NAME = "m_product_id";
BackOffice.MODEL_NAME = "MProduct";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveProduct";
BackOffice.AD_TABLE_ID = 208;

BackOffice.defaults = {
			"m_product_id":0,
			"c_uom_id":100,
			"sales_pricestd":"0.00",
			"sales_pricelist":"0.00",
			"sales_pricelimit":"0.00",
			"purchase_pricestd":"0.00",
			"loyaltypoints":"0.00",
			"isserialno":'N',
			"isageverified":'N',
			"isactive": 'Y',
			"editpriceonfly": 'N',
			"editdesconfly": 'N',
			"isageverified": 'N',
			"ismodifiable": 'N',
			"producttype" : 'N',
			"loyaltypoints":"0.00",
			"issold":'Y',
			"isstocked":'Y',
			"discountcodes_ids":"{}",
			"hasbatchandexpiry" : 'N',
			"unitsperpack" : 1,
			"extendeddescription": "",
			"isshowonpos" : 'Y'
		};

/* override default export function */
BackOffice.exportToCsv = function(f){
	BackOffice._export("csv");
};

BackOffice.exportToExcel = function(f){
	BackOffice._export("xls");
};

BackOffice._export = function(format){
	
	var url = "DataTableAction.do?action=exportProducts&format=" + format;
	
	var primarygroup = jQuery("#filter_primarygroup").val();
	if(primarygroup.length > 0){
		
		url = url + "&primarygroup=" + encodeURIComponent(primarygroup);
	}
	
	var group1 = jQuery("#filter_group1").val();
	if(group1.length > 0){
		
		url = url + "&group1=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group2").val();
	if(group1.length > 0){
		
		url = url + "&group2=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group3").val();
	if(group1.length > 0){
		
		url = url + "&group3=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group4").val();
	if(group1.length > 0){
		
		url = url + "&group4=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group5").val();
	if(group1.length > 0){
		
		url = url + "&group5=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group6").val();
	if(group1.length > 0){
		
		url = url + "&group6=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group7").val();
	if(group1.length > 0){
		
		url = url + "&group7=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group8").val();
	if(group1.length > 0){
		
		url = url + "&group8=" + encodeURIComponent(group1);
	}
	
	var isactive = jQuery("#filter_isactive").val();
	if(isactive.length > 0){
		
		url = url + "&isactive=" + isactive;
	}
	
	
	var isarchive = jQuery("#filter_isarchive").val();
	if(isarchive.length > 0){
		
		url = url + "&isarchive=" + isarchive;
		
	}
	
	var issold = jQuery("#filter_issold").val();
	if(issold.length > 0){
		
		url = url + "&issold=" + issold;
		
	}
	
	var searchTerm = jQuery("#example_wrapper input[type=search]").val();
	if(searchTerm.length > 0){
		
		url = url + "&searchTerm=" + searchTerm;
		
	}
	
	window.location = url;
};


BackOffice.exportMinColumns = function(format){
	
	var url = "DataTableAction.do?action=exportProductsWithMinColumns&format=" + format;
	
	var primarygroup = jQuery("#filter_primarygroup").val();
	if(primarygroup.length > 0){
		
		url = url + "&primarygroup=" + encodeURIComponent(primarygroup);
	}
	
	var group1 = jQuery("#filter_group1").val();
	if(group1.length > 0){
		
		url = url + "&group1=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group2").val();
	if(group1.length > 0){
		
		url = url + "&group2=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group3").val();
	if(group1.length > 0){
		
		url = url + "&group3=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group4").val();
	if(group1.length > 0){
		
		url = url + "&group4=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group5").val();
	if(group1.length > 0){
		
		url = url + "&group5=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group6").val();
	if(group1.length > 0){
		
		url = url + "&group6=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group7").val();
	if(group1.length > 0){
		
		url = url + "&group7=" + encodeURIComponent(group1);
	}
	
	var group1 = jQuery("#filter_group8").val();
	if(group1.length > 0){
		
		url = url + "&group8=" + encodeURIComponent(group1);
	}
	
	var isactive = jQuery("#filter_isactive").val();
	if(isactive.length > 0){
		
		url = url + "&isactive=" + isactive;
	}
	
	
	var isarchive = jQuery("#filter_isarchive").val();
	if(isarchive.length > 0){
		
		url = url + "&isarchive=" + isarchive;
		
	}
	
	var issold = jQuery("#filter_issold").val();
	if(isarchive.length > 0){
		
		url = url + "&issold=" + issold;
		
	}
	
	var searchTerm = jQuery("#example_wrapper input[type=search]").val();
	if(searchTerm.length > 0){
		
		url = url + "&searchTerm=" + searchTerm;
		
	}
	
	window.location = url;

}

BackOffice.requestDataTableData = function(){
	
	BackOffice.preloader.show();
	
	jQuery.post("DataTableAction.do", { action: "getProducts"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					BackOffice.preloader.hide();
					return;
				}  
				
				if(json.error)
				{
					BackOffice.displayError(json.error);
					BackOffice.preloader.hide();
					return;
				}
				
				var products = json["products"];
				var variants = json["variants"];				
				
				//BackOffice.setDataSource(products);
				BackOffice.variants = variants;
				
				var taxcategories = json["taxcategories"];
				var uoms = json["uoms"];
				
				var modifiergroups = json["modifiergroups"];
				var warehouses = json["warehouses"];
				var producttaxes = json["producttaxes"];
				var pairings = json["pairings"];
				
				var discountcodes = json["discountcodes"];
				
				BackOffice["modifiergroups"] = modifiergroups;
				BackOffice["warehouses"] = warehouses;
				BackOffice["producttaxes"] = producttaxes;
				BackOffice["pairings"] = pairings;
				BackOffice["discountcodes"] = discountcodes;
				BackOffice.DISCOUNT_CODE_DB = TAFFY(discountcodes);
				
				BackOffice["taxcategories"] = taxcategories;
				
				var taxcategory;
				
				if( taxcategories ){
					for(var i=0; i<taxcategories.length; i++){
						taxcategory = taxcategories[i];
						if(taxcategory.isdefault == 'Y'){
							BackOffice.defaults['c_taxcategory_id'] = taxcategory['c_taxcategory_id'];
						}
					}					
				}
								
				//built tax category	
				DataTable.utils.populateSelect(jQuery("#c_taxcategory_id"), taxcategories, "c_taxcategory_name", "c_taxcategory_id");				
				//built unit of measure
				DataTable.utils.populateSelect(jQuery("#c_uom_id"), uoms, "c_uom_name", "c_uom_id");
				
				DataTable.utils.populateSelect(jQuery("#discount-code-list"), discountcodes, "name", "u_pos_discountcode_id");
				
				BackOffice.preloader.hide();
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = products;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(products);
					BackOffice.renderDataTable();
				}				
				
				
			},"json").fail(function(){
				alert("Failed to request data!");
				BackOffice.preloader.hide();
			}); 
		
};


BackOffice.onCreate = function(){
	var categories_height = jQuery("#categories").height();
	var height = jQuery("#details, #modifiers").height();
	
	if(height < categories_height){
		jQuery("#details, #modifiers").css('height',categories_height + 'px');
	}
	
	jQuery("#prices-container").hide();
	jQuery("#multiple-price-list-container").hide();
	jQuery("#department-categories-container").hide();
	jQuery("#tax-category-container").hide();
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	jQuery(".backoffice-btn-remove-item-from-archive").hide();
	jQuery(".backoffice-btn-move-item-to-archive").hide();
}

/* override function from backoffice.js */
BackOffice.defaultTaxCategory = function(){
	var c_taxcategory_id = jQuery("#c_taxcategory_id").val();
	if( c_taxcategory_id == ""){
		BackOffice.defaults['c_taxcategory_id']; 
	}
	
	productTaxes(c_taxcategory_id);
};

BackOffice.renderDataTable = function(){
	
	BackOffice.preloader.show();
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	BackOffice.dt = jQuery('#example').on('xhr.dt', function ( e, settings, json ) {
				
		var products = json.data;
		
		var productCount = json["productcount"];
		BackOffice["productcount"] = productCount;
		
		/*var variants = new Array();
		
		//Do not display variants. Display only parent product
		
		jQuery.each(jQuery(products), function(index, product){
			
			if (product.m_product_parent_id > 0)
			{
				var variantIndex = products.indexOf(product);
				
				if (variantIndex >= 0)
				{
					var array = products.splice(variantIndex, 1);
					
					variants.push(array.pop());
				}
			}
		});*/
		
		BackOffice.datasource = products;	
		//BackOffice.variants = variants;
		
		BackOffice.db = TAFFY(BackOffice.datasource);
		
		
		if(BackOffice.datasource.length == 0 && productCount <= 0)
		{
			jQuery('.master').hide();
			jQuery('.no-data').show();	
	 	}
		else
		{
			jQuery('#example').show();
			jQuery('.no-data').hide();
			jQuery('.master').show();
		}
		
    }).dataTable({
    	"searchDelay" : 2000,
		"bServerSide": true,
		"initComplete": function(settings, json) {
			BackOffice.preloader.hide();
		},
	    "sAjaxSource": "DataTableAction.do?action=getProducts3",
		  "fnPreDrawCallback": function() {
			  
			  BackOffice.preloader.hide();
			  
	    	  if (BackOffice.datasource.length == 0 && BackOffice.productCount <= 0)
	    	  {
	    		  jQuery('#master').hide();
	    	  }
	      },
	    "columns": [
	      { "data": "m_product_name" },
	      { "data": "description" },
	      { "data": "upc" },
	      { "data": "sales_pricestd" },
	      { "data": "primarygroup" },
	      { "data" : "created_date"},
	      { "data" : "created_by"},
	      { "data" : "updated_date"},
	      { "data" : "updated_by"},
	      { "data": "isactive"}
	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          var isParent = row["isparent"];
	          var style = isParent ? "color:#0400ff;" : "";
	          return "<a style='"+ style + "' href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	          
	        },
	        "targets": 0
	      },
	      {
	        "render": function (data, type, row) {
		          return moment(data).format('MMMM Do YYYY, h:mm a');
		     },
		     "targets": [5,7]
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 9
	      }
	    ],
	    
	    "fnServerParams": function(aoData) {
	    	
	    	BackOffice.preloader.show();
	    	
	    	if(jQuery("#filter_primarygroup").val().length > 0){
	    		aoData.push({
					"name": "primarygroup",
					"value": jQuery("#filter_primarygroup").val()
				}); //primarygroup
	    	}	
	    	
	    	if(jQuery("#filter_group1").val().length > 0){
	    		aoData.push({
					"name": "group1",
					"value": jQuery("#filter_group1").val()
				}); 
	    	}
	    	
	    	if(jQuery("#filter_group2").val().length > 0){
	    		aoData.push({
					"name": "group2",
					"value": jQuery("#filter_group2").val()
				}); 
	    	}
	    	
	    	if(jQuery("#filter_group3").val().length > 0){
	    		aoData.push({
					"name": "group3",
					"value": jQuery("#filter_group3").val()
				}); 
	    	}
	    	
	    	if(jQuery("#filter_group4").val().length > 0){
	    		aoData.push({
					"name": "group4",
					"value": jQuery("#filter_group4").val()
				}); 
	    	}
	    	
	    	if(jQuery("#filter_group5").val().length > 0){
	    		aoData.push({
					"name": "group5",
					"value": jQuery("#filter_group5").val()
				}); 
	    	}
	    	
	    	if(jQuery("#filter_group6").val().length > 0){
	    		aoData.push({
					"name": "group6",
					"value": jQuery("#filter_group6").val()
				}); 
	    	}
	    	
	    	if(jQuery("#filter_group7").val().length > 0){
	    		aoData.push({
					"name": "group7",
					"value": jQuery("#filter_group7").val()
				}); 
	    	}
	    	
	    	if(jQuery("#filter_group8").val().length > 0){
	    		aoData.push({
					"name": "group8",
					"value": jQuery("#filter_group8").val()
				}); 
	    	}
			
	    	var isactive = jQuery("#filter_isactive").val();
	    	if(isactive.length > 0){
	    		aoData.push({
					"name": "isactive",
					"value": isactive
				}); //isactive
	    	}
	    	
	    	
	    	var isarchive = jQuery("#filter_isarchive").val();
	    	if(isarchive.length > 0){
	    		aoData.push({
					"name": "isarchive",
					"value": isarchive
				}); //isarchive
	    	}
	    	
	    	var issold = jQuery("#filter_issold").val();
	    	if(issold.length > 0){
	    		aoData.push({
					"name": "issold",
					"value": issold
				}); //issold
	    	}
			
		}
		
	});
	
	BackOffice.delaySearch();
	
	
	/*
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	      { "data": "m_product_name" },
	      { "data": "description" },
	      { "data": "upc" },
	      { "data": "sku" },
	      { "data": "primarygroup" }	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          var isParent = row["isparent"];
	          var style = isParent ? "color:#0400ff;" : "";
	          return "<a style='"+ style + "' href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
	          
	        },
	        "targets": 0
	      }	      
	    ]
	});
	*/
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

/*BackOffice.beforeShowView = function(id) {
	
	BackOffice.beforeShowViewProduct(id);
	
};*/

BackOffice.afterRenderView = function(id) {
	
	if(id > 0){
		jQuery('#default-sales-pricestd, #purchase-pricestd, #sales-pricelist, #sales-pricestd, #sales-pricelimit').attr('readonly', !BackOffice.allowUpdatePrice);
	}	
	
	
	if (BackOffice.model["isactive"] == "N")
	{
		jQuery(".backoffice-btn-move-item-to-archive").hide();
	}
	else
	{
		jQuery(".backoffice-btn-move-item-to-archive").show();
	}
	
	if (BackOffice.model.discontinued == "Y")
	{
		jQuery(".backoffice-btn-activate").hide();
		jQuery(".backoffice-btn-remove-item-from-archive").show();
	}
	else
	{
		jQuery(".backoffice-btn-activate").show();
		jQuery(".backoffice-btn-remove-item-from-archive").hide();
	}
	
	if(BackOffice.model.discontinued == "N" && BackOffice.model["isactive"] == "Y")
	{
		jQuery(".backoffice-btn-activate").hide();
	}
	
	var url = "DataTableAction.do?action=getProductInventory";
	var postData = {json : Object.toJSON({"m_product_id" : id})};
	
	var inventory = {};
	var pairing = {};
	var modifier = {};
	var pricelist = {};
	
	BackOffice.preloader.show();

	jQuery.post(url,
			postData,
			function(json, textStatus, jqXHR){	
		
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				inventory.inventories = json["inventories"];
				
			},"json").done(function() {
				
				showInventories(inventory);
				
			}).fail(function(){
				alert("Failed to request data!");
			});
	
	url = "DataTableAction.do?action=getProductPairing";

	jQuery.post(url,
			postData,
			function(json, textStatus, jqXHR){	
		
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				pairing.pairings = json["pairings"];
				
			},"json").done(function() {
				
				BackOffice.renderOtherViews("#view-pairing-details", "#show-pairings-details", pairing);
				
			}).fail(function(){
				alert("Failed to request data!");
			});
	
	url = "DataTableAction.do?action=getProductModifierGroup";

	jQuery.post(url,
			postData,
			function(json, textStatus, jqXHR){	
		
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				modifier.modifiers = json["modifiergroups"];	
				
			},"json").done(function() {
				
				BackOffice.renderOtherViews("#view-modifier-details", "#show-modifiers-details", modifier);
				
			}).fail(function(){
				alert("Failed to request data!");
			});
	
	url = "DataTableAction.do?action=getProductPrices";
	var idToBePosted = "m_product_id=" + id;
	
	jQuery.post(url,
			idToBePosted,
			function(json, textStatus, jqXHR){	
		
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				pricelist.salespricelist = json["saleprices"];	
				pricelist.purchasepricelist = json["purchaseprices"];
				pricelist.isbatchandexpiry = BackOffice.model["isbatchandexpiry"] == 'Y';
				
			},"json").done(function() {
				
				
				BackOffice.renderOtherViews("#view-pricelist-details", "#show-pricelists-details", pricelist);
				
				var tax = {};
				var array = [];
				
				for(var i=0; i<BackOffice["producttaxes"].length; i++)
				{
					var currentTax = BackOffice["producttaxes"][i];
					
					var c_taxcategory_id = currentTax["c_taxcategory_id"];
					
					if (c_taxcategory_id == BackOffice.model["c_taxcategory_id"])
					{
						array.push(currentTax);
					}
				}
				
				if (array.length > 0)
				{
					tax.tax_category = array[0]["tax_category"];
					tax.taxes = array;
				}
				
				BackOffice.renderOtherViews("#view-tax-details", "#show-taxes-details", tax);
				
				array = [];
				
				for(var i=0; i<BackOffice["variants"].length; i++)
				{
					var currentVariant = BackOffice["variants"][i];
					
					var parent_id = currentVariant["m_product_parent_id"];
					
					if (parent_id == BackOffice.model["m_product_id"])
					{
						array.push(currentVariant);
					}
				}
				
				if (BackOffice.model["isserialno"] == "N")
				{
					var variant = {};
					
					variant.variants = array;
					
					BackOffice.renderOtherViews("#view-variant-details", "#show-variants-details", variant);
				} 
				else 
				{
					var serialno = {};
					
					serialno.serialnos = array;
					
					BackOffice.renderOtherViews("#view-serialno-details", "#show-variants-details", serialno);
				}
				
			}).fail(function(){
				alert("Failed to request data!");
			}).always(function(){
				BackOffice.preloader.hide();
				
				resizeContainerHeight();
				
				var height = jQuery("html").height();
				jQuery("#left-panel").height(height);
			});
};

BackOffice.onShowDetail = function(){
	jQuery("#modifier-group-container, #inventory-container, #pairing-tbody, #bom-tbody, #sales-price-grid, #purchase-price-grid").html("");
	
	var model = this.model;
	
	if (model==null){
		
		jQuery("#inventory-checkbox").attr('checked', 'checked');
		jQuery("#inventory-table-container").show();
		
		return;
	};
	
	if(model['isstocked'] == 'Y')
	{
		jQuery("#inventory-checkbox").attr('checked', 'checked');
	}
	else
	{
		jQuery("#inventory-checkbox").removeAttr('checked');
	}
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	jQuery('#department-categories-container input').each(function()
	{
	    if( !(jQuery(this).val().length === 0) ) {
	        jQuery('#department-categories-container').show();
	        jQuery("#department-categories-checkbox").attr('checked','checked');
	        
	        return false;	      
	    }
	  else
	  {
	    jQuery('#department-categories-container').hide();
	     jQuery("#department-categories-checkbox").attr('checked',false);
	      
	  }
	});
	
	/*var modifiers = BackOffice["modifiergroups"];
	
	if(modifiers.length == 0)
	{
		jQuery("#modifier-group-container").hide();
		jQuery('#modifier-group-main-container').css('display', 'none');
	}
	else
	{
		jQuery('#modifier-group-main-container').css('display', 'block');
		jQuery("#modifier-group-container").show();
		jQuery("#has-modifier-checkbox").attr('checked', 'checked');
	}*/
	
	/*var pairings = BackOffice["pairings"];
	
	if(pairings.length == 0)
	{
		jQuery("#pairing-container").hide();
	}
	else
	{
		jQuery("#pairing-container").show();
		jQuery("#item-combination-checkbox").attr('checked', 'checked');
	}*/
	
	if((model["sales_pricelist"] || model["sales_pricestd"] || model["sales_pricelimit"] || model["purchase_pricestd"])=="")
	{
		jQuery("#prices-container").hide();
	}
	else
	{
		jQuery("#prices-container").show();
		jQuery("#manage-prices-checkbox").attr('checked', 'checked');
	}	
	
	var producttype = model["producttype"];
	var container = jQuery("#accordion-inventory-container");
	
	if (producttype=='S'){
		container.css("display", "none");
	} else {
		container.css("display", "block");
	};
		
	if(model["producttype"]=="S")
	{
		jQuery("#item-service-checkbox").attr('checked', 'checked');		
	}
	
		
	var c_taxcategory_id = model.c_taxcategory_id;
	productTaxes(c_taxcategory_id);	

	
	jQuery('#variant-form-container').hide();
	jQuery('#serial-no-form-container').hide();
	jQuery('#serial-nos-table').hide();
	jQuery('#variants-table').hide();
	
	var ageVerificationContainer = jQuery("#age-verification");
	var ageVerification = TerminalManager.terminal.preference.age;
	jQuery(ageVerificationContainer).html(ageVerification);
	
	if (BackOffice.model.discontinued == "Y")
	{
		jQuery(".backoffice-btn-activate").hide();
		jQuery(".backoffice-btn-remove-item-from-archive").show();
	}
	/*else
	{
		jQuery(".backoffice-btn-activate").show();
		jQuery(".backoffice-btn-remove-item-to-archive").hide();
	}*/
	
	/* discount codes */
	displayDiscountCodes();
	
	jQuery("#discount-code-add-button").on("click", function(){
		var code = jQuery("#discount-code-list").val();
		if(code){
			
			var codes =  BackOffice.model["discountcodes_ids"];
			
			var codes = codes.substr(1, codes.length - 2);
			
			var array = [];
			
			if(codes.length > 0){
				array = codes.split(",");
			}			
			
			var index = array.indexOf( code );
			
			if(index < 0){
				array.push( code );
			}
			
			codes = "{" + array.join(",") + "}";
			
			BackOffice.model["discountcodes_ids"] = codes;
			
			displayDiscountCodes();
			
		}
	});

};

BackOffice.beforeSave = function(model){
	/* set inventory */
	var inventories = [];
	
	var movementDate = moment().format('YYYY-MM-DD HH:mm:ss');
	
	jQuery("#inventory-container tr").each(function(index, ele){
		
		var row = jQuery(ele);
		
		var qtyonhand = row.find('[name=qtyonhand]').val();
		var initial_qtyonhand = row.find('[name=initial_qtyonhand]').val();
		var m_warehouse_id = row.find('[name=m_warehouse_id]').val();
		var m_locator_id = row.find('[name=m_locator_id]').val();
		var m_attributesetinstance_id = row.find('[name=m_attributesetinstance_id]').val();
		
		if(qtyonhand != initial_qtyonhand){
			inventories.push({
				"m_warehouse_id" : m_warehouse_id,
				"m_attributesetinstance_id" : m_attributesetinstance_id,
				"m_locator_id" : m_locator_id,
				"qtyonhand" : qtyonhand,
				"movementDate" : movementDate
			});
		}
	});
		
	model["inventories"] = inventories;
	
	
	/*var inputs = jQuery("#inventory-container input[id=replenishMax_" + m_warehouse_id+"]");
	for(var i=0; i<inputs.length; i++){
		var input = jQuery(inputs[i]);
		
		var m_warehouse_id = input.attr("m_warehouse_id");
		
		replenishes.push({
			"m_warehouse_id" : m_warehouse_id,
			"m_product_id" : model.m_product_id,
			"level_max" : jQuery("#replenishMax_" + m_warehouse_id).val()
		});
	}
	
	
	model["replenishes"] = replenishes;*/
	
	var replenishes = {};
	
	jQuery("#inventory-container tr").each(function(index, ele){
		
		var row = jQuery(ele);
		
		var m_warehouse_id = row.find('[name=m_warehouse_id]').val();
		var level_max = row.find('[name=level_max]').val();
		var level_min = row.find('[name=level_min]').val();
		
		if(!replenishes[m_warehouse_id]){
			replenishes[m_warehouse_id] = {
				'level_max' : level_max,
				'level_min' : level_min
			};
		}
		
	});

	
	model["replenishes"] = replenishes;
	
	/* add modifiergroups-to-add and modifiergroups-to-remove */
	var modifiergroups_to_assign = [];
	var modifiergroups_to_remove = [];	
	
	/* get all modifiergroup checkboxes*/	
	var modifiergroup_checkboxes = jQuery("#modifier-group-container input[type=checkbox]");	
	
	for(var i=0; i<modifiergroup_checkboxes.length; i++){
		var checkbox = modifiergroup_checkboxes[i];
		checkbox = jQuery(checkbox);
		
		var hasClass = checkbox.hasClass("checked-on-load");
		var isChecked = checkbox.prop("checked");
		var u_modifiergroup_id = checkbox.val();
		
		if(isChecked && !hasClass){
			/* is checked and was not marked */
			modifiergroups_to_assign.push(u_modifiergroup_id);
		}
		else if(!isChecked && hasClass){
			/* is not checked and was marked */
			modifiergroups_to_remove.push(u_modifiergroup_id);
		}
		else {
			/* do nothing */
		}
	}/* for */
	
	/* link model */
	model["modifiergroups_to_assign"] = modifiergroups_to_assign;
	model["modifiergroups_to_remove"] = modifiergroups_to_remove;
	
	/* add pairing data */
	var pairings = [];
	/* traverse pairing table body */
	var rows = jQuery("#pairing-tbody tr");
	for(var i=0; i<rows.length; i++){
		var row = rows.get(i);
		
		row = jQuery(row);
		
		var m_product_bom_id = row.find(".m_product_bom_id").val();
		var m_productbom_id = row.find(".m_productbom_id").val();
		
		var input = row.find(".bomqty");
		var bomqty = input.val();
		
		var update = true; /* flag to decide whether we should update bom record. set to false if there is no change */
		if(m_product_bom_id != 0){
			var initialvalue = input.attr("initial-value");
			if(initialvalue == bomqty){ /* no change */
				update = false;
			}
		}		
		
		var pairing = {};
		pairing["m_product_bom_id"] = m_product_bom_id;
		pairing["m_productbom_id"] = m_productbom_id;
		pairing["bomqty"] = bomqty;
		pairing["update"] = update;
		
		pairings.push(pairing);
	}
			
	model["pairings"] = pairings;
	
	/** ingredients **/
	var ingredients = [];
	/* traverse ingredient table body */
	var rows = jQuery("#bom-tbody tr");
	for(var i=0; i<rows.length; i++){
		var row = rows.get(i);
		
		row = jQuery(row);
		
		var m_bomproduct_id = row.find(".m_bomproduct_id").val();
		var m_productbom_id = row.find(".m_productbom_id").val();
		
		var input = row.find(".bomqty");
		var bomqty = input.val();
		
		var update = true; /* flag to decide whether we should update bom record. set to false if there is no change */
		if(m_bomproduct_id != 0){
			var initialvalue = input.attr("initial-value");
			if(initialvalue == bomqty){ /* no change */
				update = false;
			}
		}		
		
		var ingredient = {};
		ingredient["m_bomproduct_id"] = m_bomproduct_id;
		ingredient["m_productbom_id"] = m_productbom_id;
		ingredient["bomqty"] = bomqty;
		ingredient["update"] = update;
		
		ingredients.push(ingredient);
	}
			
	model["ingredients"] = ingredients;
	
	/*add variants*/
	/*updated variants*/
	if (BackOffice.variantsGrid)
	{
		var data = BackOffice.variantsGrid.data;
		var variant;
		var columnValue;
		var columnLabel;
		
		/*Check if variants have been modified*/
		for(var i=0; i<data.length; i++)
		{
			variant = data[i].values
			
			for(var j=0; j<data[i].columns.length; j++)
			{
				columnValue = data[i].columns[j];
				columnLabel = BackOffice.variantsGrid.getColumnName(j);
				
				if (variant[columnLabel] != columnValue)
				{
					data[i].values[columnLabel] = columnValue;
				}
			}
			
		}
		
		/*construct variants json from grid*/
		var variants = new Array();
		
		for(var i=0; i<data.length; i++)
		{
			variants.push(data[i].values);
		}
		
		model["variants"] = variants;
		
		
		/* set variant inventory */
		model["variantsinventories"] = BackOffice.variantInventories;
	}
	
	
	/*Product Prices*/
	/*Sales Prices*/
	if (BackOffice.salesPriceGrid && jQuery('#manage-multiple-price-list-checkbox').is(':checked'))
	{
		var data = BackOffice.salesPriceGrid.data;
		var salesPrice;
		var columnValue;
		var columnLabel;
		
		var salesPrices = new Array();
		
		for(var i=0; i<data.length; i++)
		{
			salesPrice = data[i].values
			
			for(var j=0; j<data[i].columns.length; j++)
			{
				columnValue = data[i].columns[j];
				columnLabel = BackOffice.salesPriceGrid.getColumnName(j);
				
				if (salesPrice[columnLabel] != columnValue)
				{
					data[i].values[columnLabel] = columnValue;
				}
			}
			
			/*construct sales prices json from grid*/
			salesPrices.push(data[i].values);
		}
		
		model["salesprices"] = salesPrices;
	}
	
	
	/*Purchase Prices*/
	if (BackOffice.purchasePriceGrid && jQuery('#manage-multiple-price-list-checkbox').is(':checked'))
	{
		var data = BackOffice.purchasePriceGrid.data;
		var purchasePrice;
		var columnValue;
		var columnLabel;
		
		var purchasePrices = new Array();
		
		for(var i=0; i<data.length; i++)
		{
			purchasePrice = data[i].values
			
			for(var j=0; j<data[i].columns.length; j++)
			{
				columnValue = data[i].columns[j];
				columnLabel = BackOffice.purchasePriceGrid.getColumnName(j);
				
				if (purchasePrice[columnLabel] != columnValue)
				{
					data[i].values[columnLabel] = columnValue;
				}
			}
			
			/*construct purchase prices json from grid*/
			purchasePrices.push(data[i].values);
		}
		
		model["purchaseprices"] = purchasePrices;
	}
	
	if (BackOffice.model != undefined)
	{
		model["salespricelists"] = BackOffice.model["salespricelists"];
		model["purchasepricelists"] = BackOffice.model["purchasepricelists"];
	}
	
	var item_service_checkbox = jQuery("#item-service-checkbox");
	if(item_service_checkbox.is(':checked'))
	{
		model["producttype"]="S";
	}
	else
	{
		model["producttype"]="I";
	}
	
	var isSoldCheckbox = jQuery("#issold");
	if(isSoldCheckbox.is(':checked'))
	{
		model["issold"]="Y";
	}
	else
	{
		model["issold"]="N";
	}
	
	var isShowOnPOS = jQuery("#isshowonpos");
	if(isShowOnPOS.is(':checked'))
	{
		model["isshowonpos"]="Y";
	}
	else
	{
		model["isshowonpos"]="N";
	}
	
	var isStockedCheckbox = jQuery("#inventory-checkbox");
	if(isStockedCheckbox.is(':checked'))
	{
		model["isstocked"]="Y";
	}
	else
	{
		model["isstocked"]="N";
	}
	
	var hasBatchAndExpiry = jQuery("#isbatchandexpiry");
	if(hasBatchAndExpiry && hasBatchAndExpiry.is(':checked'))
	{
		model["isbatchandexpiry"]="Y";
	}
	else
	{
		model["isbatchandexpiry"]="N";
	}
		
	
	var defaultSalesPriceStdVal = jQuery("#default-sales-pricestd").val();
	
	var salesPriceListVal = jQuery("#sales-pricelist").val();
	var salesPriceStdVal = jQuery("#sales-pricestd").val();
	var salesPriceLimitVal = jQuery("#sales-pricelimit").val();
	
	if(salesPriceListVal =='0.00' || salesPriceStdVal =='0.00' || salesPriceLimitVal == '0.00')
	{
		salesPriceListVal = jQuery("#default-sales-pricestd").val(defaultSalesPriceStdVal);
		salesPriceStdVal = jQuery("#default-sales-pricestd").val(defaultSalesPriceStdVal);
		salesPriceLimitVal = jQuery("#default-sales-pricestd").val(defaultSalesPriceStdVal);
		
		model["sales_pricestd"] = defaultSalesPriceStdVal;
		model["sales_pricelist"] = defaultSalesPriceStdVal;
		model["sales_pricelimit"] = defaultSalesPriceStdVal;
	}	
	
	jQuery("#default-sales-pricestd").keyup(function(){
		
		var defaultSalesPriceStdVal = jQuery("#default-sales-pricestd").val();
		jQuery("#sales-pricestd").val(defaultSalesPriceStdVal);
		model["sales_pricestd"] = defaultSalesPriceStdVal;
		
		
	});
	
	jQuery("#sales-pricestd").keyup(function(){
		
		var salesPriceStdVal = jQuery("#sales-pricestd").val();
		jQuery("#default-sales-pricestd").val(salesPriceStdVal);
		model["sales_pricestd"] = salesPriceStdVal;
	});
	
};

jQuery(document).ready(function(e){
	jQuery("#c_taxcategory_id").on("change",function(e){
		var c_taxcategory_id = jQuery(this).val();
		productTaxes(c_taxcategory_id);		
	});
	
	var managePrices_checkbox = jQuery("#manage-prices-checkbox");
	managePrices_checkbox.on("change", function(e){
		var container = jQuery("#prices-container");	
		
		if(managePrices_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var manageMultiplePriceList_checkbox = jQuery("#manage-multiple-price-list-checkbox");
	manageMultiplePriceList_checkbox.on("change", function(e){
		var container = jQuery("#multiple-price-list-container");	
		
		if(manageMultiplePriceList_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var departmentCategories_checkbox = jQuery("#department-categories-checkbox");
	departmentCategories_checkbox.on("change", function(e){
		var container = jQuery("#department-categories-container");	
		
		if(departmentCategories_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var inventory_checkbox = jQuery("#inventory-checkbox");
	inventory_checkbox.on("change", function(e){
		var container = jQuery("#inventory-table-container");	
		
		if(inventory_checkbox.is(':checked')){
			container.show();
			
			BackOffice.model['isstocked'] = 'Y';
			
			displayInventories();
			
		}else{
			container.hide();
		}
	});
	
	var taxCategory_checkbox = jQuery("#tax-category-checkbox");
	taxCategory_checkbox.on("change", function(e){
		var container = jQuery("#tax-category-container");
		
		if(taxCategory_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var serialVariants_checkbox = jQuery("#serial-variants-checkbox");
	serialVariants_checkbox.on("change", function(e){
		var container = jQuery("#serial-variant-number-container");
		
		if(serialVariants_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	var pairing_checkbox = jQuery("#item-combination-checkbox");
	pairing_checkbox.on("change", function(e){
		var container = jQuery("#pairing-container");
		
		if(pairing_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});	
		
	jQuery('#scan_items').on('click', function(){
		jQuery('#scan-item-pop-up').dialog({
			modal: true,
			title: Translation.translate("scan.items"),
			height: '200',
			width: '300',
			buttons:[
			         {
					text : 'Cancel',
					"class" : 'btn btn-default',
					"id": 'button_cancel_scan',
					click : function(){
						jQuery('#scan-item-pop-up').dialog("close");
					}
				},
				{
					text: 'Go to POS',
					"class" : 'btn btn-primary',
					"id": 'button_ok_scan',
					click: function(){
						window.location.href = "OrderAction.do?action=loadOrderScreen";
					}
				}	
			]
		});
		
		jQuery("#button_ok_scan").html(Translation.translate("go.to.POS"));
		jQuery("#button_cancel_scan").html(Translation.translate("cancel"));
	});
	
	jQuery('.backoffice-btn-move-item-to-archive').on('click', function(){
		
		BackOffice.preloader.show();
		
		var model = BackOffice.model;
		
		if(model == null) return;
		
		var m_product_id = model["m_product_id"];
		
		url = "DataTableAction.do?action=addItemToArchive";
		
		var postData = {json : Object.toJSON({"m_product_id" : m_product_id})};		
		
		jQuery.post(url,
				postData,
	    		function(json, textStatus, jqXHR){
			
					BackOffice.preloader.hide();				
	    			
					if(json.success){
						BackOffice.displayMessage(json.success);						
						BackOffice.afterArchiveItem();
						BackOffice.model.discontinued = 'Y';
					}
					
	    			if(json == null || jqXHR.status != 200){
	    				console.error('autentication failed');
	    				BackOffice.displayError('Failed!');
	    				return;
	    			}
	    			
	    			if(json.error){
	    				console.error(json.error);
	    				BackOffice.displayError("Failed! " + json.error);
	    				return;
	    			}	    			
	    		},	    		
	    		"json").done(function() {			
	    			
	    			BackOffice.afterArchiveItem();
	    			
				}).fail(function(){
					console.error('request failed');
					BackOffice.displayError(json.error);
				});
			
	});
	
	jQuery('.backoffice-btn-remove-item-from-archive').on('click', function(){
		
		BackOffice.preloader.show();
		
		var model = BackOffice.model;
		
		if(model == null) return;
		
		var m_product_id = model["m_product_id"];
		
		url = "DataTableAction.do?action=unarchiveItem";
		
		var postData = {json : Object.toJSON({"m_product_id" : m_product_id})};		
		
		jQuery.post(url,
				postData,
	    		function(json, textStatus, jqXHR){
			
					BackOffice.preloader.hide();				
	    			
					if(json.success){
						BackOffice.displayMessage(json.success);						
						BackOffice.afterUnarchiveItem();
						BackOffice.model.discontinued = 'N';
					}
					
	    			if(json == null || jqXHR.status != 200){
	    				console.error('autentication failed');
	    				BackOffice.displayError('Failed!');
	    				return;
	    			}
	    			
	    			if(json.error){
	    				console.error(json.error);
	    				BackOffice.displayError("Failed! " + json.error);
	    				return;
	    			}	    			
	    		},	    		
	    		"json").done(function() {			
	    			
	    			BackOffice.afterUnarchiveItem();
	    			
				}).fail(function(){
					console.error('request failed');
					BackOffice.displayError(json.error);
				});
			
	});
	
	jQuery('#backoffice-import-btn').show();
	
	jQuery("#backoffice-import-btn").on("click", function(e){
		
		window.location = 'importer.do?importer=ProductImporter';
	});
	
});

BackOffice.afterArchiveItem = function(){
	
	BackOffice["model"].isactive = 'N';	
	
	jQuery(".backoffice-btn-deactivate").hide();
	
	//jQuery(".backoffice-btn-edit").hide();
	jQuery(".backoffice-btn-create").hide();
	
	disableFormElements();
	
	BackOffice.renderView();
};

BackOffice.afterUnarchiveItem = function(){
	
	BackOffice["model"].isactive = 'Y';
	BackOffice.renderAccordions();
	enableFormElements();
	/*jQuery(".backoffice-btn-deactivate").show();
	jQuery(".backoffice-btn-activate").hide();*/
	
	BackOffice.initActionButtons();
	BackOffice.initViewActionButtons();
	
	BackOffice.renderView();
	
	jQuery(".backoffice-btn-activate").hide();
};

function productTaxes(c_taxcategory_id){
	
	var producttaxes = BackOffice["producttaxes"];
	var container = jQuery("#product-tax-table-tbody");
	
	var html = '';
	
	for(i=0; i<producttaxes.length; i++){
		
		var producttax = producttaxes[i];
		
		var id = producttaxes[i].c_taxcategory_id;
		
		if (c_taxcategory_id == id) {
			
			if(producttax == null)
			{
				producttax = {
						"ad_org_id" : 0,
						"c_tax_id" : 0,
						"c_taxcategory_id" : 0,
						"org_name" : "",
						"tax_category" : "",
						"tax_rate" : ""
				};
			}
			
			html += '<tr>' +
			/*'<td>' +
				'<input type="hidden" id="ad_org_id" name="ad_org_id" value="'+ producttax.ad_org_id +'">' +
				'<input type="hidden" id="c_tax_id" name="c_tax_id" value="'+ producttax.c_tax_id +'">' +
				'<input type="hidden" id="c_taxcategory_id" name="c_taxcategory_id" value="'+ producttax.c_taxcategory_id +'">' +
			'</td>' +*/
			'<td>' +
				'<div id="org_name">' +producttax.org_name +'</div>' +
			'</td>' +
			'<td>' +
				'<div id="tax_rate">' + producttax.tax_rate +'</div>' +
			'</td>' +
			'</tr>';			
		}		
		
	}
	container.html(html);	
}

BackOffice.afterSave = function(model){
	
	/*var db = BackOffice.db;
	
	var id = model[BackOffice.ID_COLUMN_NAME];	
	
	var query = {};
	query[BackOffice.ID_COLUMN_NAME] = {'==':id};
	
	var records = db(query).get();
	
	if(records.length == 0){
		db.insert(model)
	}
	else
	{
		db(query).update(model);
	}
	
	datasource = db().get();    			
	
	BackOffice.datasource = datasource;	
	BackOffice.db = TAFFY(BackOffice.datasource);  */	
	
	/* modifier groups */
	/* reset checked-on-load marker class */
	var modifiergroup_checkboxes = jQuery("#modifier-group-container input[type=checkbox]");	
	
	for(var i=0; i<modifiergroup_checkboxes.length; i++){
		
		var checkbox = modifiergroup_checkboxes[i];
		checkbox = jQuery(checkbox);
		
		var isChecked = checkbox.prop("checked");
		
		if(isChecked){
			checkbox.addClass("checked-on-load");
		}
		else{
			checkbox.removeClass("checked-on-load");
		}
	}
	
	/* inventories */
	/* reset initial-values */
	var inputs = jQuery("#inventory-container input[type=text]");
	for(var i=0; i<inputs.length; i++){
		var input = jQuery(inputs[i]);
		var qtyonhand = input.val();
		input.attr("initial-value", qtyonhand);
	}
	
	/* pairings */
	if(model["pairings"] != undefined){
		var pairings = model["pairings"];
		addPairingAutoCompleter();
		initializePairingTextfields(pairings);
	}
	
	/*variants*/
	if(model["variants"] != undefined)
	{
		var modifiedVariants = model["variants"];
		
		var variantId;
		var variant1;
		var variant2;
		var variant3;
		var allVariants = BackOffice.variants;
		
		/*update modified variants*/
		for(var i=0; i<modifiedVariants.length; i++)
		{
			variantId = modifiedVariants[i]["m_product_id"];
			variant1 = modifiedVariants[i]["variant1"];
			variant2 = modifiedVariants[i]["variant2"];
			variant3 = modifiedVariants[i]["variant3"];
			
			for(var j=0; j<allVariants.length; j++)
			{
				if (allVariants[j]["m_product_id"] == 0 && allVariants[j]["variant1"] == variant1
						&& allVariants[j]["variant2"] == variant2 && allVariants[j]["variant3"] == variant3)
				{
					/*newly created variants*/
					allVariants[j] = modifiedVariants[i];
					break;
				}
				else
					if (allVariants[j]["m_product_id"] == variantId)
					{
						/*edited variants*/
						allVariants[j] = modifiedVariants[i];
						break;
					}
			}
		}
		
		BackOffice.variants = allVariants;
		
		displayVariants();
		
		BackOffice.variant = null;
		/*Remove validation on variants*/
		jQuery("[name=serialno]").removeAttr("data-validation");
		jQuery("[name=variant1]").removeAttr("data-validation");		
	}
};

/**
 * Draws the modifier group checkboxes
 */
function addModifierGroupCheckboxes(){
	var container = jQuery("#modifier-group-container");
	
	/* add modifiers checkboxes */
	var modifiergroups = BackOffice["modifiergroups"];
	
	var html = '';	
	for(var i=0; i<modifiergroups.length; i++){
		var modifiergroup = modifiergroups[i];
		html += '<div class="form-group left-input checkbox-container"><label class="checkcontainer" for="modifiergroup_' + modifiergroup.u_modifiergroup_id + '"><input type="checkbox" id="modifiergroup_' + modifiergroup.u_modifiergroup_id + '" value="' + modifiergroup.u_modifiergroup_id + '"> ' + modifiergroup.u_modifiergroup_name + '<span class="checkmark"></span></label></div>';
	}
	
	container.html(html);
}

/**
 * Initialises modifier group checkboxes
 * @param modifiergroups
 * @returns
 */
function initializeModifierGroupCheckboxes(modifiergroups){
	for(var i=0; i<modifiergroups.length; i++){
		
		var modifiergroup = modifiergroups[i];    				
		var u_modifiergroup_id = modifiergroup["u_modifiergroup_id"];
		
		/* set checkbox value */
		var checkbox = document.getElementById("modifiergroup_" + u_modifiergroup_id); /* for perfomance issue do not use jquery here */
		if(checkbox){
			jQuery(checkbox).attr('checked', 'checked').addClass("checked-on-load");			
		}
	}  
}

/* display modifiers */
function displayModifierGroups(){
	
	/* check if container is empty */
	var container = jQuery("#modifier-group-container");
	if(jQuery.trim(container.text()).length != 0){
		return;
	}
	
	addModifierGroupCheckboxes();
	
	/* query previous modifiers */
	var model = BackOffice.model;
	
	if(model == null) return;
	
	var m_product_id = model["m_product_id"];
	var url = "DataTableAction.do?action=getProductModifierGroup";
	
	var postData = {json : Object.toJSON({"m_product_id" : m_product_id})};
	
	jQuery.post(url,
			postData,
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('autentication failed');
    				BackOffice.displayError('Failed to query modifier groups: session timed out!');
    				return;
    			}
    			
    			if(json.error){
    				console.error(json.error);
    				BackOffice.displayError("Failed to query modifier groups! " + json.error);
    				return;
    			}
    			
    			var modifiergroups = json["modifiergroups"];
    			
    			if (modifiergroups.length > 0)
    			{
    				jQuery('#has-modifier-checkbox').attr('checked', true);
    				jQuery('#has-modifier-checkbox').attr('disabled', true);
        			initializeModifierGroupCheckboxes(modifiergroups);
        			jQuery('#modifier-group-main-container').css('display', 'block');
        			jQuery("#modifier-group-container").css('display', 'block');
    			}
    			else
    			{
    				jQuery('#has-modifier-checkbox').attr('checked', false);
    				jQuery('#has-modifier-checkbox').attr('disabled', false);
    				jQuery('#modifier-group-main-container').css('display', 'none');
    				jQuery("#modifier-group-container").css('display', 'none');
    			}
    		},
			"json").fail(function(){
				console.error('request failed');
				BackOffice.displayError("Failed to process request");
			});
}

/**
 * Add a textfield for each warehouse present
 */
function addInventoryTextfields(){
	
	var model = BackOffice.model;
	
	if(model == null) return;
	
	var m_product_id = model["m_product_id"];
	
	var container = jQuery("#inventory-container");
	
	var warehouses = BackOffice["warehouses"];
	//var model = BackOffice.model;
	
	var html = '';	
	
	for(var i=0; i<warehouses.length; i++){
		var warehouse = warehouses[i];
		/*html +='<tr>'+
		'<td>'+warehouse.m_warehouse_name+'</td>' +
		'<td><input type="text" value="0" class="form-control" m_locator_id="' + warehouse.m_locator_id + '" m_warehouse_id="' + warehouse.m_warehouse_id + '" data-validation="required" id="inventory_' + warehouse.m_warehouse_id + '"></td' +
		'<td><input type="text" value="0" class="form-control" m_locator_id="' + warehouse.m_locator_id + '" m_warehouse_id="' + warehouse.m_warehouse_id + '" data-validation="required" id="replenishMax_' + warehouse.m_warehouse_id + '" productId="'+BackOffice.model.m_product_id+'"></td>' +
		'<td><input type="text" value="0" class="form-control" m_locator_id="' + warehouse.m_locator_id + '" m_warehouse_id="' + warehouse.m_warehouse_id + '" data-validation="required" id="replenishMin_' + warehouse.m_warehouse_id + '" productId="'+BackOffice.model.m_product_id + '"></td>' +
		
		'</tr>';*/
		
		html += 
		'<tr>'+	
		'<td>'+
		'<div><label for="warehouse_' + warehouse.m_warehouse_id + '">' + warehouse.m_warehouse_name + '</label> '+
		'</td>'+
		'<td>'+
		'<div><input type="text" value="0" class="form-control" m_locator_id="' + warehouse.m_locator_id + '" m_warehouse_id="' + warehouse.m_warehouse_id + '" data-validation="required" id="inventory_' + warehouse.m_warehouse_id + '"> '+
		'</td>'+
		'<td>'+
		'<input type="text" value="0" m_locator_id="' + warehouse.m_locator_id + '" m_warehouse_id="' + warehouse.m_warehouse_id + '" data-validation="required" id="replenishMax_' + warehouse.m_warehouse_id + '" productId="'+BackOffice.model.m_product_id+'">'+
		'</td>'+
		'<td>'+
		'<input type="text" value="0"  m_locator_id="' + warehouse.m_locator_id + '" m_warehouse_id="' + warehouse.m_warehouse_id + '" data-validation="required" id="replenishMin_' + warehouse.m_warehouse_id + '" productId="'+BackOffice.model.m_product_id + '">'+
		'</td>'+
		'</div>'+
		'</td>'+
		'</tr>';
		
		
	}
	
	container.html(html);
}

/**
 * Initialises each inventory textfield 
 * @param inventories
 */
function initializeInventoryTextfields(inventories){
	
	for(var i=0; i<inventories.length; i++){		
		var inventory = inventories[i];    				
		var m_warehouse_id = inventory["m_warehouse_id"];
		var qtyonhand = inventory["qtyonhand"];
		
		jQuery("#inventory_" + m_warehouse_id).attr("initial-value", qtyonhand).val(qtyonhand);
	} 	
}

function initializeReplenishTextfields(replenishes){
	
	for(var i=0;i<replenishes.length; i++){
		var replenish = replenishes[i];
		var m_warehouse_id = replenish["m_warehouse_id"];
		var m_product_id = replenish["m_product_id"];
		var levelMax = replenish["level_max"]; 
		var levelMin = replenish["level_min"];
		
		jQuery("#replenishMax_" + m_warehouse_id).attr("level-max", levelMax).val(levelMax);
		jQuery("#replenishMin_" + m_warehouse_id).attr("level-min", levelMin).val(levelMin);
	}
}

/**
 * Initialises variant inventory textfield 
 * @param inventories
 */
function initializeVariantInventoryTextfields(inventories){
	
	for(var i=0; i<inventories.length; i++){		
		var inventory = inventories[i];    				
		var m_warehouse_id = inventory["m_warehouse_id"];
		var qtyonhand = inventory["qtyonhand"];
		
		jQuery("#variant_warehouse_" + m_warehouse_id).attr("initial-value", qtyonhand).val(qtyonhand);
	} 	
}

function renderInventoryTable(inventories){
	
	var isbatchandexpiry = BackOffice.model["isbatchandexpiry"] == 'Y';
	
	if(isbatchandexpiry){
		
		jQuery("#inventory-table-lot-header").show();
	}
	else
	{
		jQuery("#inventory-table-lot-header").hide();
	}
	
	var model = BackOffice.model;
	var m_product_id = model["m_product_id"];
	var warehouses = BackOffice["warehouses"];
	
	var container = jQuery("#inventory-container");	
	
	var html = '';	
	
	for(var i=0; i<warehouses.length; i++){
		var warehouse = warehouses[i];
		
		var found = false;
		
		for(var j=0; j<inventories.length; j++){
			
			var inventory = inventories[j];
			
			if(warehouse['m_warehouse_id'] != inventory['m_warehouse_id']) continue;
			
			found = true;
				
			html += 
				'<tr>'+	
					'<td>'+
						'<label>' + warehouse.m_warehouse_name + '</label> '+
						'<input type="hidden" name="m_warehouse_id" value="'+ warehouse['m_warehouse_id'] + '">' +
						'<input type="hidden" name="m_locator_id" value="'+ warehouse['m_locator_id'] + '">' + 
						'<input type="hidden" name="m_attributesetinstance_id" value="'+ inventory['m_attributesetinstance_id'] + '">' + 
						'<input type="hidden" name="initial_qtyonhand" value="'+ inventory['qtyonhand'] + '">' +
					'</td>';
			
			if(isbatchandexpiry){
				html += '<td class="link">' + inventory['lot'] + '</td>';
			}
					
			html += '<td>'+
						'<input type="text" name="qtyonhand" value="'+ inventory['qtyonhand'] +'" class="form-control" data-validation="required">'+
					'</td>'+
					'<td>'+
						'<input type="text" name="level_max" value="'+ (inventory['level_max'] | 0) +'" class="form-control">'+
					'</td>'+
					'<td>'+
						'<input type="text" name="level_min" value="'+ (inventory['level_min'] | 0) +'" class="form-control">'+
					'</td>'+
				'</tr>';
		}
		
		if(!found){
			
			html += 
				'<tr>'+	
					'<td>'+
						'<label>' + warehouse.m_warehouse_name + '</label> '+
						'<input type="hidden" name="m_warehouse_id" value="'+ warehouse['m_warehouse_id'] + '">' +
						'<input type="hidden" name="m_locator_id" value="'+ warehouse['m_locator_id'] + '">' + 
						'<input type="hidden" name="m_attributesetinstance_id" value="0">' + 
						'<input type="hidden" name="initial_qtyonhand" value="0">' +
					'</td>';
			
			if(isbatchandexpiry){
				html += '<td>---</td>';
			}
			
			html += '<td>'+
						'<input type="text" name="qtyonhand" value="0" class="form-control" data-validation="required"> '+
					'</td>'+
					'<td>'+
						'<input type="text" name="level_max" value="0" class="form-control">'+
					'</td>'+
					'<td>'+
						'<input type="text" name="level_min" value="0" class="form-control">'+
					'</td>'+
				'</tr>';
		}
	}
	
	container.html(html);
	
	/*make lot editable*/
	if(isbatchandexpiry){
	
		jQuery('#inventory-container tr').each(function() {
		    /* Find the hidden input field in the first <td> */
		    const firstTd = jQuery(this).find('td').eq(0);
		    const m_attributesetinstance_id = firstTd.find('input[name="m_attributesetinstance_id"]').val();
		    const m_warehouse_id = firstTd.find('input[name="m_warehouse_id"]').val();
		    
		    /* Attach the click event to the second <td> */
		    jQuery(this).find('td').eq(1).on('click', function() {
		    	const lotNo = jQuery(this).html();
		        showUpdateLotAndExpiryDialog(m_warehouse_id, m_attributesetinstance_id, lotNo);
		    });
		});
	
	}
	
	if(BackOffice.model.isactive == 'N'){
		jQuery("#inventory-table-container input[type=text]").attr('disabled', true);
	}
}

function showUpdateLotAndExpiryDialog(m_warehouse_id, m_attributesetinstance_id, lotNo){

	const m_product_id = BackOffice.model['m_product_id']; //test
	
	console.log('showUpdateLotAndExpiryDialog', lotNo, m_product_id, m_warehouse_id, m_attributesetinstance_id);
	
	//parse lotNo ex «zzzzzz»_08/24/2024
	var x  = lotNo.replaceAll('«','').replaceAll('»','').split('_');
	
	var lot = x[0];	
	var expiry = "";
	if(x.length > 1){
		expiry = moment(x[1],'MM/DD/YYYY').format('YYYY-MM-DD');
	} 

	BootstrapDialog.show({
        title: 'Update Lot & Expiry Date',
        message: jQuery(`<form>
            <div class="form-group">
                <label for="text" class="control-label">Lot No.</label>
                <input autocomplete="off" name="lotNo" type="text" class="form-control" value="" id="update-lot-expiry-popup-lot-no">
            </div>
            <div class="form-group">
                <label for="text1" class="control-label">Expiry Date</label>
                <div class="input-group">
                    <input autocomplete="off" class="form-control" id="update-lot-expiry-popup-expiry-date" name="expiry" type="text">                                         
                    <span class="input-group-btn">
                        <button style="font-size:20px;" class="btn btn-default" id="update-lot-expiry-popup-date-picker-btn"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                </div>
            </div>
        </form>`),
        buttons: [{
            label: 'Update',
            cssClass: 'btn-primary',
            action: function(dialogRef) {
                
				var expiry = jQuery('#update-lot-expiry-popup-expiry-date').val();
				var lot = jQuery('#update-lot-expiry-popup-lot-no').val();

				if(expiry == "" && lot == ""){
					alert("Both Lot No. and Expiry Date cannot be empty!");
					return;
				}

				//make request
				_updateLotAndExpiry({m_product_id, m_warehouse_id, m_attributesetinstance_id, expiry, lot});
				
				dialogRef.close();

            }
        }],
        onshown: function(dialogRef){
            // Initialize the date picker
            jQuery("#update-lot-expiry-popup-expiry-date").datepicker({
                dateFormat: "yy-mm-dd" // or any other format you prefer
            });

            // Show the date picker when the calendar icon is clicked
            jQuery("#update-lot-expiry-popup-date-picker-btn").on("click", function(e) {
                e.preventDefault(); // prevent the form from submitting
                jQuery("#update-lot-expiry-popup-expiry-date").datepicker("show");
            });
            
            jQuery('#update-lot-expiry-popup-lot-no').val(lot)
            jQuery("#update-lot-expiry-popup-expiry-date").val(expiry);
        }
    });
}

function _updateLotAndExpiry({m_product_id, m_warehouse_id, m_attributesetinstance_id, expiry, lot}) {

	var url = "DataTableAction.do?action=updateLotAndExpiry";

	var postData = jQuery.param({ 
        "m_product_id": m_product_id,
        "m_warehouse_id": m_warehouse_id,
        "m_attributesetinstance_id": m_attributesetinstance_id,
        "expiry": expiry,
        "lot": lot
    });

	jQuery.post(url,
		postData,
		function (json, textStatus, jqXHR) {

			if (json == null || jqXHR.status != 200) {
				console.error('autentication failed');
				BackOffice.displayError('Failed to update lot and expiry: session timed out!');
				return;
			}

			if (json.error) {
				console.error(json.error);
				BackOffice.displayError("Failed to update lot and expiry! " + json.error);
				return;
			}

			console.log(json);
			
			if(json.updated){
				jQuery("#inventory-container").html('');
				displayInventories();
			}
			else
			{
				alert("Unable to update lot and expiry!");
			}

		},
		"json").fail(function () {
			console.error('request failed');
			BackOffice.displayError("Failed to process request");
		});
}

/* display inventories */
function displayInventories(){
	
	/* show hide inventory based on isstocked flag */
	if(BackOffice.model['isstocked'] == 'Y'){
		jQuery("#inventory-table-container").show();
	}
	else
	{
		jQuery("#inventory-table-container").hide();
		return;
	}
	
	
	/* check if container is empty */
	var container = jQuery("#inventory-container");
	if(jQuery.trim(container.text()).length != 0){
		return;
	}
	
	/* add inventory textfields */
	//addInventoryTextfields();
	
	/* query inventory */
	var model = BackOffice.model;
	
	if(model == null) return;
		
	var m_product_id = model["m_product_id"];
	var url = "DataTableAction.do?action=getProductInventory";
	
	var postData = {json : Object.toJSON({"m_product_id" : m_product_id})};
	
	jQuery.post(url,
			postData,
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('autentication failed');
    				BackOffice.displayError('Failed to query inventory: session timed out!');
    				return;
    			}
    			
    			if(json.error){
    				console.error(json.error);
    				BackOffice.displayError("Failed to query inventory! " + json.error);
    				return;
    			}
    			    			
    			/*var replenishes = json["replenishes"];
    			initializeReplenishTextfields(replenishes);	*/
    			
    			var inventories = json["inventories"];
    			//initializeInventoryTextfields(inventories);
    			console.log('render inventory table', inventories);
    			renderInventoryTable(inventories);
    			
    			
    		},
			"json").fail(function(){
				console.error('request failed');
				BackOffice.displayError("Failed to process request");
			});
	
	//displayReplenish();
}

/*Display replenish*/
function displayReplenish(){
	
	/* query replenish */
	var model = BackOffice.model;
	
	if(model == null) return;
		
	var m_product_id = model["m_product_id"];
	var url = "DataTableAction.do?action=getProductReplenish";
	
	var postData = {json : Object.toJSON({"m_product_id" : m_product_id})};
	
	jQuery.post(url,
			postData,
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('autentication failed');
    				BackOffice.displayError('Failed to query inventory: session timed out!');
    				return;
    			}
    			
    			if(json.error){
    				console.error(json.error);
    				BackOffice.displayError("Failed to query inventory! " + json.error);
    				return;
    			}
    			
    			/*var inventories = json["inventories"];
    			initializeInventoryTextfields(inventories);*/
    			
    			var replenishes = json["replenishes"];
    			initializeReplenishTextfields(replenishes);	
    		},
			"json").fail(function(){
				console.error('request failed');
				BackOffice.displayError("Failed to process request");
			});
}


var template_pairing = '<tr>' +
	'<td>{m_product_name}</td>' +
	'<td>' +
		'<input type="text" class="bomqty" value="{qty}" initial-value={qty}>' +
		'<input type="hidden" class="m_productbom_id" value="{m_productbom_id}">' +
		'<input type="hidden" class="m_product_bom_id" value="{m_product_bom_id}">' +
	'</td>' +
	'<td>' +
		'<button type="button" class="btn btn-default" title="Delete" onclick="removePairing(this)"><span class="glyphicon glyphicon-trash"></span></button>' +
	'</td>' +
	'</tr>';

function addPairingAutoCompleter(){
	/* built autocompleter */
	/* TODO advance search Ex: http://jsfiddle.net/h5E6C/ */
	
	//var products = BackOffice.db().get();
	/*
	var products;
	
	jQuery.post("DataTableAction.do?action=getProductsAutocomplete", function(data){
		products = data.names;
		
		jQuery("#pairing-autocompleter").autocomplete({
		    source: products,
		    minlength: 3,
		    focus: function(event, ui) {
		        jQuery("#pairing-autocompleter").val(ui.item.value);
		        return false;
		    },
		    select: function(event, ui) {
		        jQuery("#pairing-autocompleter").val(ui.item.value);
		        jQuery("#pairing-product-id").val(ui.item.id);
		        return false;
		    }
		}).data("autocomplete")._renderItem = function(ul, item) {
		    return jQuery("<li></li>")
		        .append("<a><div>" + item.value + "</div><div>" + item.description + "</div></a>")
		        .data("item.autocomplete", item)
		        .appendTo(ul);
		};
		
	});
	*/
	
	jQuery("#pairing-autocompleter").autocomplete({
	    source: function( request, response ) {
	    	
	    	var searchTerm = request.term;
	    	
	    	var url = "DataTableAction.do?action=autocomplete&table=m_product&columns=name,description,upc&term=" + searchTerm;
	    			    	
	    	jQuery.getJSON( url, request, function( data, status, xhr ) {
	            response( data );
	          });
	    	
	    },
	    minlength: 3,
	    focus: function(event, ui) {
	        jQuery("#pairing-autocompleter").val(ui.item.name);
	        return false;
	    },
	    select: function(event, ui) {
	    	
	    	if(ui.item.m_product_id == -1){
	    		
	    		alert("Item not found!");
	    		
	    		document.getElementById("pairing-autocompleter").select();
	    		
	    		return false;
	    	}
	    	
	        jQuery("#pairing-autocompleter").val(ui.item.name);
	        jQuery("#pairing-product-id").val(ui.item.m_product_id);
	        return false;
	    }
	}).data("autocomplete")._renderItem = function(ul, item) {
	    return jQuery("<li style='border-bottom:solid 1px #CCC;'></li>")
	        .append("<a><div><strong>" + item.name + "</strong></div><div>" + item.description + "</div>" + "</div><div>" + item.upc + "</div></a>")
	        .data("item.autocomplete", item)
	        .appendTo(ul);
	};
	
	
	/*var custom_source = function(request, response) {
        var matcher = new RegExp(jQuery.ui.autocomplete.escapeRegex(request.term), "i");
        response(jQuery.grep(products, function(value) {
            return matcher.test(value.m_product_name)
                || matcher.test(value.description);
        }));
    };*/	
}

/**
* ####### Pairings #######
**/
function initializePairingTextfields(pairings){
	var container = jQuery("#pairing-tbody");
	container.html("");
	
	for(var i=0; i<pairings.length; i++){
		
		var pairing = pairings[i];    				
		var m_product_bom_id = pairing["m_product_bom_id"];
		var m_productbom_id = pairing["m_productbom_id"];
		var bomqty = pairing["bomqty"];
		var m_product_name = pairing["m_product_name"];
		
		var html = template_pairing
			.replace(/{m_product_name}/g, m_product_name)
			.replace(/{qty}/g, bomqty)
			.replace(/{m_productbom_id}/g, m_productbom_id)
			.replace(/{m_product_bom_id}/g, m_product_bom_id);
		
		container.append(html);
	}
}

/* display pairings */
function displayPairings(){
	/* check if container is empty */
	var container = jQuery("#pairing-tbody");
	if(jQuery.trim(container.text()).length != 0){
		return;
	}
	
	// add autocompleter
	addPairingAutoCompleter(); 	 
	 
	var model = BackOffice.model;		
	if(model == null) return;
	
	var m_product_id = model["m_product_id"];
	var url = "DataTableAction.do?action=getProductPairing";
	
	var postData = {json : Object.toJSON({"m_product_id" : m_product_id})};
	
	jQuery.post(url,
			postData,
			function(json, textStatus, jqXHR){
				
		if(json == null || jqXHR.status != 200){
			console.error('autentication failed');
			BackOffice.displayError('Failed to query pairings: session timed out!');
			return;
		}
		
		if(json.error){
			console.error(json.error);
			BackOffice.displayError("Failed to query pairings! " + json.error);
			return;
		}
		
		var pairings = json["pairings"];		
		initializePairingTextfields(pairings);	
		
		if(pairings.length == 0)
		{
			jQuery("#pairing-container").hide();
		}
		else
		{
			jQuery("#pairing-container").show();
			jQuery("#item-combination-checkbox").attr('checked', 'checked');
		}
		
	},
	"json").fail(function(){
		console.error('request failed');
		BackOffice.displayError("Failed to process request");
	});	
	
}

/* remove pairing when remove button is click */
function removePairing(button){
	var row = jQuery(button).parentsUntil("tbody");
	var m_product_bom_id = jQuery(row).find(".m_product_bom_id").val();
		
	if(m_product_bom_id != 0){
		/* a hidden field to store boms to be deleted */
		var input = jQuery("#m_product_bom_id_to_delete");
		var value = jQuery.trim(input.val());
		if(value.length > 0){
			value =  value + ",";
		}
		
		value =  value + m_product_bom_id;
		
		input.val(value);
	}
	
	row.remove();
}

/**
 * ######### Ingredients ###########
 */

var template_ingredient = '<tr>' +
	'<td>{m_product_name}</td>' +
	'<td>' +
		'<input type="text" class="bomqty" value="{qty}" initial-value={qty}>' +
		'<input type="hidden" class="m_bomproduct_id" value="{m_bomproduct_id}">' +
		'<input type="hidden" class="m_productbom_id" value="{m_productbom_id}">' +
	'</td>' +
	'<td>' +
		'<button type="button" class="btn btn-default" title="Delete" onclick="removeIngredient(this)"><span class="glyphicon glyphicon-trash"></span></button>' +
	'</td>' +
	'</tr>';


function addIngredientAutoCompleter(){	
	 
	jQuery("#ingredient-autocompleter").autocomplete({
	    source: function( request, response ) {
	    	
	    	var searchTerm = request.term;
	    	
	    	var url = "DataTableAction.do?action=autocomplete&table=m_product&columns=name,description,upc&term=" + searchTerm;
	    			    	
	    	jQuery.getJSON( url, request, function( data, status, xhr ) {
	            response( data );
	          });
	    	
	    },
	    minlength: 3,
	    focus: function(event, ui) {
	        jQuery("#ingredient-autocompleter").val(ui.item.name);
	        return false;
	    },
	    select: function(event, ui) {
	    	
	    	if(ui.item.m_product_id == -1){
	    		
	    		alert("Item not found!");
	    		
	    		document.getElementById("ingredient-autocompleter").select();
	    		
	    		return false;
	    	}
	    	
	        jQuery("#ingredient-autocompleter").val(ui.item.name);
	        jQuery("#ingredient-product-id").val(ui.item.m_product_id);
	        return false;
	    }
	}).data("autocomplete")._renderItem = function(ul, item) {
	    return jQuery("<li style='border-bottom:solid 1px #CCC;'></li>")
	        .append("<a><div><strong>" + item.name + "</strong></div><div>" + item.description + "</div>" + "</div><div>" + item.upc + "</div></a>")
	        .data("item.autocomplete", item)
	        .appendTo(ul);
	};
}

/* remove ingredient when remove button is click */
function removeIngredient(button){
	var row = jQuery(button).parentsUntil("tbody");
	var m_bomproduct_id = jQuery(row).find(".m_bomproduct_id").val();
		
	if(m_bomproduct_id != 0){
		/* a hidden field to store ingredients to be deleted */
		var input = jQuery("#m_bomproduct_id_to_delete");
		var value = jQuery.trim(input.val());
		if(value.length > 0){
			value =  value + ",";
		}
		
		value =  value + m_bomproduct_id;
		
		input.val(value);
	}
	
	row.remove();
}

function initializeIngredientsTextfields(ingredients){
	var container = jQuery("#bom-tbody");
	container.html("");
	
	for(var i=0; i<ingredients.length; i++){
		
		var ingredient = ingredients[i];    				
		var m_bomproduct_id = ingredient["m_bomproduct_id"];
		var m_productbom_id = ingredient["m_productbom_id"];
		var bomqty = ingredient["bomqty"];
		var m_product_name = ingredient["m_product_name"];
		
		var html = template_ingredient
			.replace(/{m_product_name}/g, m_product_name)
			.replace(/{qty}/g, bomqty)
			.replace(/{m_productbom_id}/g, m_productbom_id)
			.replace(/{m_bomproduct_id}/g, m_bomproduct_id);
		
		container.append(html);
	}
}

/* display ingredients */
function displayIngredients(){
	
	var container = jQuery("#bom-container");
	
	var bom_checkbox = jQuery("#is-bom-checkbox");
	bom_checkbox.on("change", function(e){		
		
		if(bom_checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});
	
	if(bom_checkbox.is(':checked')){
		container.show();
	}else{
		container.hide();
	}
	
	jQuery("#bom-add-button").on("click", function(e){
		var m_bomproduct_id = 0;
		var m_productbom_id = jQuery("#ingredient-product-id").val();
		var bomqty = jQuery("#ingredient-qty-textfield").val();
		var m_product_name = jQuery("#ingredient-autocompleter").val();
		
		if( m_productbom_id == 0 ){
			return;
		}
		
		var html = template_ingredient
			.replace(/{m_product_name}/g, m_product_name)
			.replace(/{qty}/g, bomqty)
			.replace(/{m_productbom_id}/g, m_productbom_id)
			.replace(/{m_bomproduct_id}/g, m_bomproduct_id);
		
		jQuery("#bom-tbody").append(html);
		
		jQuery("#ingredient-qty-textfield").val("1");
		jQuery("#ingredient-autocompleter").val("");
        jQuery("#ingredient-product-id").val("0");
	});
		
	// add autocompleter
	addIngredientAutoCompleter(); 	 
	 
	var model = BackOffice.model;		
	if(model == null) return;
	
	var m_product_id = model["m_product_id"];
	var url = "DataTableAction.do?action=getProductIngredients";
	
	var postData = {json : Object.toJSON({"m_product_id" : m_product_id})};
	
	jQuery.post(url,
			postData,
			function(json, textStatus, jqXHR){
				
		if(json == null || jqXHR.status != 200){
			console.error('autentication failed');
			BackOffice.displayError('Failed to query ingredients: session timed out!');
			return;
		}
		
		if(json.error){
			console.error(json.error);
			BackOffice.displayError("Failed to query ingredients! " + json.error);
			return;
		}
		
		var ingredients = json["ingredients"];		
		initializeIngredientsTextfields(ingredients);	
		
		if(ingredients.length == 0)
		{
			jQuery("#bom-container").hide();
		}
		else
		{
			jQuery("#bom-container").show();
			jQuery("#is-bom-checkbox").attr('checked', 'checked');
		}
		
	},
	"json").fail(function(){
		console.error('request failed');
		BackOffice.displayError("Failed to process request");
	});	
	
}

/**
 * ######### Discount Codes ###########
 */

function displayDiscountCodes()
{	
	if( BackOffice.model["discountcodes_ids"] != undefined )
	{
		var discountcodes = BackOffice.model["discountcodes_ids"];
		discountcodes = discountcodes.substr(1, discountcodes.length - 2);
		
		var container = jQuery("#discount-codes-container");
		var html = "";
		
		var ids = [];
		
		if( discountcodes.length > 2)
		{
			ids = discountcodes.split(",");
		}
		
		var code, id, results;		
		
		if( ids.length > 0)
		{
			html += "<table class='testgrid' style='width:100%;'>";
			html += "<tr><th>Name</th><th>Percentage</th><th style='width:50px;'>&nbsp;</th></tr>";
			
			for( var i=0; i<ids.length; i++ )
			{
				id = ids[i];
				results = BackOffice.DISCOUNT_CODE_DB({"u_pos_discountcode_id" : {'==':id}}).get();
				
				if(results && results.length > 0){
					
					code = results[0];
					
					html += ( "<tr><td>" + code.name + "</td><td>" + code.percentage + "</td><td><button type='button' class='btn btn-default' title='Delete' onclick='removeDiscountCode(" + i + ")'><span class='glyphicon glyphicon-trash'></span></button></td></tr>" );
					
				}
			}	
			
			html += "</table><br>";
		}		
		
		container.html( html );
		
		jQuery('#discount-codes-textfield').val(BackOffice.model["discountcodes_ids"]);
	}
}

function removeDiscountCode(id){
	
	//hidden textfield
	var codes =  BackOffice.model["discountcodes_ids"];
	
	var codes = codes.substr(1, codes.length - 2);
	
	var array = codes.split(",");
	
	array.splice(id, 1);
	
	codes = "{" + array.join(",") + "}";
	
	BackOffice.model["discountcodes_ids"] = codes;
	
	displayDiscountCodes();
}


/* display variants 
 * displays either variants or serial nos*/
function displayVariants(){
	var model = BackOffice.model;		
	//if(model == null) return;
	
	if (model == null)
	{
		m_product_id = 0;
	}
	else
	{
		var m_product_id = model["m_product_id"];
	}
	
	/*display variants*/
	resetVariants();
	var variants = new Array();
	
	if (jQuery("[name=isserialno]:checked").attr("id") == "is-variant-number")
	{
		var variant1Name;
		var variant2Name;
		var variant3Name;
		
		jQuery.each(jQuery(BackOffice.variants), function(index, element){
			
			if (element.m_product_parent_id == m_product_id || element.m_product_parent_id == undefined)
			{
				//element["inventory"] = '<i style="cursor:pointer" class="glyphicons glyphicons-pro bin" onclick="javascript:createVariantInventory('+element["m_product_id"]+');"/>';
				
				element["inventory"] = '<button class="btn btn-secondary" type="button" onclick="javascript:createVariantInventory('+index+','+element["m_product_id"]+');">Inv</button>';
				
				element["actions"] = '<i style="cursor:pointer" class="glyphicons glyphicons-pro glyphicons-bin" onclick="javascript:deleteVariant('+index+','+element["m_product_id"]+');"/>';
				
				element["lineid"] = index;
				
				variants.push({id:index, values:element});
				variant1Name = element.variant1name;
				variant2Name = element.variant2name;
				variant3Name = element.variant3name;
			}
		});
		
		buildVariantGrid(variants, variant1Name, variant2Name, variant3Name);
	}
	else
	{
		/*display serial nos*/
		jQuery.each(jQuery(BackOffice.variants), function(index, element){
			
			if (element.m_product_parent_id == m_product_id || element.m_product_parent_id == undefined)
			{
				element["actions"] = '<i style="cursor:pointer" class="glyphicons glyphicons-pro glyphicons-bin" onclick="javascript:deleteVariant('+index+','+element["m_product_id"]+');"/>';
				
				element["inventory"] = '<button class="btn btn-secondary" type="button" onclick="javascript:createVariantInventory('+index+','+element["m_product_id"]+');">Inv</button>';
				
				element["lineid"] = index;
				
				variants.push({id:index, values:element});
			}
		});
		
		buildSerialNoGrid(variants);
	}
	
	if (variants.length > 0)
	{
		jQuery('#choose-serial-variants').attr('checked', true);
		jQuery('#choose-serial-variants').attr('disabled', true);
		jQuery('#variants-main-container').css('display', 'block');
		
		jQuery('#isSerialNo').attr('disabled', true);
		jQuery('#is-variant-number').attr('disabled', true);
	}
	else
	{
		jQuery('#isSerialNo').attr('disabled', false);
		jQuery('#is-variant-number').attr('disabled', false);
		
		/*New Product*/
		if (BackOffice["model"]["m_product_id"] <= 0)
		{
			jQuery('#choose-serial-variants').attr('checked', false);
			jQuery('#variants-main-container').css('display', 'none');
		}
		else
		{
			/*Normal Product- No Variants*/
			if (!jQuery('#choose-serial-variants').is(':checked'))
			{
				jQuery('#variants-main-container').css('display', 'none');
			}
			else
			{
				jQuery('#variants-main-container').css('display', 'block');
			}
			
		}
	}
	
	BackOffice.variantsGrid = variantsGrid;
	
	/*Clear Inventories for variants*/
	BackOffice.variantInventories = [];
}

function buildVariantGrid(variants, variant1Name, variant2Name, variant3Name)
{
	var metadata = [];
	metadata.push({ name: "variant1", label: "Variant1" , datatype: "string", editable: true});
	metadata.push({ name: "variant2", label: "Variant2", datatype: "string", editable: true});
	metadata.push({ name: "variant3", label: "Variant3", datatype: "string", editable: true});
	metadata.push({ name: "upc", label: "UPC", datatype: "string", editable: true});
	metadata.push({ name: "sku", label: "SKU", datatype: "string", editable: true});
	metadata.push({ name: "sales_pricestd", label: "Current Price", datatype: "double", editable: true});
	metadata.push({ name: "sales_pricelist", label: "Original Price", datatype: "double", editable: true});
	metadata.push({ name: "sales_pricelimit", label: "Minimum Price", datatype: "double", editable: true});
	metadata.push({ name: "inventory", label: " ", datatype: "html", editable: false});
	metadata.push({ name: "actions", label: " ", datatype: "html", editable: false});
	
	if (variant1Name != null && variant1Name != '')
	{
		metadata[0].label = variant1Name;
	}
	
	if (variant2Name != null && variant2Name != '')
	{
		metadata[1].label = variant2Name;
	}
	
	if (variant3Name != null && variant3Name != '')
	{
		metadata[2].label = variant3Name;
	}
	
	showVariantGrid();
	
	variantsGrid = new EditableGrid("VariantsGrid", {enableSort:false});
	variantsGrid.load({"metadata": metadata, "data": variants});
	variantsGrid.renderGrid("variants-table", "testgrid");
}

function buildSerialNoGrid(variants)
{
	var metadata = [];
	metadata.push({ name: "serialno", label: "SerialNo" , datatype: "string", editable: true});
	metadata.push({ name: "upc", label: "UPC", datatype: "string", editable: true});
	metadata.push({ name: "sku", label: "SKU", datatype: "string", editable: true});
	metadata.push({ name: "sales_pricestd", label: "Current Price", datatype: "double", editable: true});
	metadata.push({ name: "sales_pricelist", label: "Original Price", datatype: "double", editable: true});
	metadata.push({ name: "sales_pricelimit", label: "Minimum Price", datatype: "double", editable: true});
	metadata.push({ name: "inventory", label: " ", datatype: "html", editable: false});
	metadata.push({ name: "actions", label: " ", datatype: "html", editable: false});
	
	
	showSerialNoGrid();
	
	variantsGrid = new EditableGrid("SerialNosGrid", {enableSort:false});
	variantsGrid.load({"metadata": metadata, "data": variants});
	variantsGrid.renderGrid("serial-nos-table", "testgrid");
}

function showVariantForm()
{
	if (jQuery('#variant-form-container').css('display') == 'none')
	{
		jQuery('#variants-table').hide();
		jQuery('#serial-no-form-container').hide();
		jQuery('#serial-nos-table').hide();
		jQuery('#variant-form-container').show();
	}
}

function showVariantGrid()
{
	jQuery('#variant-form-container').hide();
	jQuery('#serial-no-form-container').hide();
	jQuery('#serial-nos-table').hide();
	jQuery('#variants-table').show();
}

function showSerialNoForm()
{
	jQuery('#variant-form-container').hide();
	jQuery('#variants-table').hide();
	jQuery('#serial-nos-table').hide();
	jQuery('#serial-no-form-container').show();
}

function showSerialNoGrid()
{
	jQuery('#variant-form-container').hide();
	jQuery('#variants-table').hide();
	jQuery('#serial-no-form-container').hide();
	jQuery('#serial-nos-table').show();
}

function showPriceListGrid()
{
	if (jQuery('#pricelist-container').css('display') == 'none')
	{
		jQuery('#pricelist-container').show();
	}
}

BackOffice.afterCancel = function(){
	
	resetVariants();
}

function resetVariants()
{
	jQuery('#variant-form-container :input').each(function(){
		jQuery(this).val('');
	});
	
	jQuery('#serial-no-form-container :input').each(function(){
		jQuery(this).val('');
	});
	
	/*Remove validation on variants*/
	jQuery("[name=serialno]").removeAttr("data-validation");
	jQuery("[name=variant1]").removeAttr("data-validation");
	
	BackOffice.variantInventories = [];
}

function createVariantInventory(lineId, variantId)
{
	var container = jQuery('#variants-inventory-form');
	var warehouses = BackOffice["warehouses"];
	
	var html = '';	
	
	for(var i=0; i<warehouses.length; i++){
		var warehouse = warehouses[i];
		html += '<div class="form-group left-input"><label for="warehouse_' + warehouse.m_warehouse_id + '">' + warehouse.m_warehouse_name + '</label><input type="text" value="0" class="form-control" variantId="' + variantId + '" m_locator_id="' + warehouse.m_locator_id + '" m_warehouse_id="' + warehouse.m_warehouse_id + '" data-validation="required" id="variant_warehouse_' + warehouse.m_warehouse_id + '" lineId="'+ lineId +'" initial-value="0"></div>';
	}
	
	container.html(html);
	
	var url = "DataTableAction.do?action=getProductInventory";
	
	var postData = {json : Object.toJSON({"m_product_id" : variantId})};
	
	jQuery.post(url,
			postData,
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('autentication failed');
    				BackOffice.displayError('Failed to query inventory: session timed out!');
    				return;
    			}
    			
    			if(json.error){
    				console.error(json.error);
    				BackOffice.displayError("Failed to query inventory! " + json.error);
    				return;
    			}
    			
    			var inventories = json["inventories"];
    			initializeVariantInventoryTextfields(inventories);		
    			
    		},
			"json").fail(function(){
				console.error('request failed');
				BackOffice.displayError("Failed to process request");
			});
	
	jQuery('#variants-inventory-form').dialog({
		modal: true,
		title: 'Variant Inventory',
		height: 500,
		width: 400,
		position: { my: "top", at: "top", of: window },
		buttons: {Ok: {
			'text':'Ok',
			'class':'btn-primary',
			'id' : 'variant-inventory-ok-btn',
			'click' : function(){
				var inputs = jQuery("#variants-inventory-form input[type=text]");
				for(var i=0; i<inputs.length; i++){
					var input = jQuery(inputs[i]);
					
					var m_warehouse_id = input.attr("m_warehouse_id");
					var m_locator_id = input.attr("m_locator_id");
					var initialvalue = input.attr("initial-value");
					var variantId = input.attr("variantId");
					var lineId = input.attr("lineId");
					
					var qtyonhand = input.val();
					
					if(qtyonhand != initialvalue){
						BackOffice.variantInventories.push({
							"m_warehouse_id" : m_warehouse_id,
							"m_locator_id" : m_locator_id,
							"qtyonhand" : qtyonhand,
							"variantId" : variantId,
							"movementDate" : moment().format('YYYY-MM-DD HH:mm:ss'),
							"lineId" : lineId
						});
					}
				}
				
				jQuery('#variants-inventory-form').dialog('close');
			}
		}}
			
	});
	
	jQuery("#variant-inventory-ok-btn").html(Translation.translate("ok"));
	
	jQuery('#variants-inventory-form input').keyup(function(){
		
		var activeInput = jQuery(this);
		
		if (BackOffice.model["isserialno"] == 'Y')
		{
			jQuery('#variants-inventory-form input').each(function(index){
				var input = jQuery(this);
				if (!activeInput.is(input))
				{
					input.val('0');
				}
				else
				{
					if (activeInput.val() > 1 || activeInput.val() < 0)
					{
						alert("Inventory for serial no cannot be greater than 1.");
						activeInput.val('0');
						return;
					}
				}
			});
		}
	});
}

function deleteVariant(lineId, variantId)
{
	
	if (variantId == 0)
	{
		/*Not yet created, remove only from grid*/
		var variants = BackOffice.variantsGrid.data;
		
		for(var i=0; i<variants.length; i++)
		{
			if (variants[i]["originalIndex"] == lineId)
			{
				/*remove from grid*/
				BackOffice.variants.splice(variants[i]["id"], 1);
				BackOffice.variantsGrid.remove(lineId);
			}
		}
	}
	
	jQuery(document).ajaxSend(function() {
		jQuery("#indicator").show();
	});

	jQuery(document).ajaxStop(function() {
		jQuery("#indicator").hide();
	});
	
	var params = {
					action: "delete",
					modelName: BackOffice.MODEL_NAME,
					id: variantId
				};
	
	jQuery.ajax({
		type: "POST",
		url: "DataTableAction.do",
		data: params
	}).done(function(data, textStatus, jqXHR){
		
		if(data == null || jqXHR.status != 200){
    		alert('Failed to delete!');
    	}
    	
    	var json = JSON.parse(data);
    	
    	if(json.success)
    	{
    		jQuery('#backoffice-export-btn').hide();
    		jQuery('#backoffice-create-btn').hide();
    		
    		BackOffice.displayMessage(json.message);
    		
    		
    		var variants = BackOffice.variants;
    		var deletedObjectIndex;
    		var productParentId = 0;
    		
    		jQuery.each(variants, function(index, variant){
    			
    			if(variant[BackOffice.ID_COLUMN_NAME] == json.id)
    			{
    				deletedObjectIndex = index;
    				productParentId = variant["m_product_parent_id"];
    			}
    		});
    		
    		variants.splice(deletedObjectIndex, 1);
    		
    		BackOffice.variants = variants;
    		//BackOffice.renderDataTable();
    		
    		jQuery.each(BackOffice.variantsGrid["data"], function(index, variant){
    			
    			if (variant.id == deletedObjectIndex)
    			{
    				BackOffice.variantsGrid.remove(index);
    			}
    		});
    		
    		/*Check if variants still exists*/
    		var isChildPresent = true;
    		
			jQuery.each(variants, function(index, variant){
			    			
    			if(variant["m_product_parent_id"] == productParentId)
    			{
    				isChildPresent = false;
    			}
    		});
    		
    		if (!isChildPresent)
    		{
    			jQuery('#choose-serial-variants').attr('checked', true);
    			jQuery('#choose-serial-variants').attr('disabled', true);
    			jQuery('#variants-main-container').css('display', 'block');
    			
    			jQuery('#isSerialNo').attr('disabled', true);
    			jQuery('#is-variant-number').attr('disabled', true);
    		}
    		else
    		{
    			jQuery('#isSerialNo').attr('disabled', false);
    			jQuery('#is-variant-number').attr('disabled', false);
    			
    			/*New Product*/
    			if (BackOffice["model"]["m_product_id"] <= 0)
    			{
    				jQuery('#choose-serial-variants').attr('checked', false);
    				jQuery('#variants-main-container').css('display', 'none');
    			}
    			else
    			{
    				/*Normal Product- No Variants*/
    				if (!jQuery('#choose-serial-variants').is(':checked'))
    				{
    					jQuery('#variants-main-container').css('display', 'none');
    				}
    				else
    				{
    					jQuery('#variants-main-container').css('display', 'block');
    				}
    				
    			}
    		}
    	}
    	else
    	{
    		BackOffice.displayError(json.message);
    	}
	});
}

function displayPrices()
{	
	var container = jQuery("#sales-price-grid");
	if(jQuery.trim(container.text()).length != 0){
		return;
	}
	
	var model = BackOffice.model;
	
	if(model == null) return;
	
	var m_product_id = model["m_product_id"];
	var url = "DataTableAction.do?action=getProductPrices";
	
	var postData = "m_product_id=" + m_product_id;
	
	jQuery.post(url,
			postData,
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('autentication failed');
    				BackOffice.displayError('Failed to query product prices: session timed out!');
    				return;
    			}
    			
    			if(json.error){
    				console.error(json.error);
    				BackOffice.displayError("Failed to query product prices! " + json.error);
    				return;
    			}
    			
    			var salesPrices = json["saleprices"];
    			var purchasePrices = json["purchaseprices"];
    			var salesPriceLists = json["sopricelists"];
    			var purchasePriceLists = json["popricelists"];
    			var lots = json["lots"];
    			
    			BackOffice.model.salesprices = salesPrices;
    			BackOffice.model.purchaseprices = purchasePrices;
    			BackOffice.model.salespricelists = salesPriceLists;
    			BackOffice.model.purchasepricelists = purchasePriceLists;
    			BackOffice.model.lots = lots;    			
    			
    			/*Display Grid for sales prices*/
    			displaySalesPrices();
    			
    			/*Display grid for purchase prices*/
    			displayPurchasePrices();
    			
    			showPriceListGrid();
    			
    			displayMultiplePriceTable();
    			
    		},
			"json").fail(function(){
				console.error('request failed');
				BackOffice.displayError("Failed to process request");
			});
}


function displaySalesPrices()
{
	var salesPriceArray = new Array();
	
	jQuery.each(BackOffice.model.salesprices, function(index, element){
		
		element["actions"] = `<i style="cursor:pointer" class="glyphicons glyphicons-pro glyphicons-bin" onclick="javascript:deletePrice(true, ${index}, ${element["m_product_id"]}, ${element["m_pricelist_id"]}, ${element["m_pricelist_version_id"]}, ${element["m_attributesetinstance_id"]});"/>`;
		salesPriceArray.push({id:index, values:element});
	});
	
	var isbatchandexpiry = BackOffice.model["isbatchandexpiry"] == 'Y';
	
	var metadata = [];
	metadata.push({ name: "name", label: "Sales Pricelist" , datatype: "string", editable: false});
	metadata.push({ name: "istaxincluded", label: "Tax Incl", datatype: "string", editable: false});
	
	if(isbatchandexpiry){
		metadata.push({ name: "lot", label: "Lot/Expiry", datatype: "string", editable: false});
	}
	
	metadata.push({ name: "pricestd", label: " Current", datatype: "double", editable: BackOffice.allowUpdatePrice});
	metadata.push({ name: "pricelist", label: "Original", datatype: "double", editable: BackOffice.allowUpdatePrice});
	metadata.push({ name: "pricelimit", label: "Minimum", datatype: "double", editable: BackOffice.allowUpdatePrice});
	metadata.push({ name: "actions", label: " ", datatype: "html", editable: false});
	
	var modelChangedListener = function(rowIdx, colIdx, oldValue, newValue, row) {};
	var default_pricelist_name = TerminalManager.terminal.so_priceList;
	
	if(isbatchandexpiry){
		modelChangedListener = function(rowIdx, colIdx, oldValue, newValue, row) {	
			
			console.log(`rowIdx:${rowIdx}, colIdx:${colIdx}, oldValue:${oldValue}, newValue:${newValue}, row:${row.childNodes[2].innerText}`); 
			
			var batch = row.childNodes[2].innerText;
			if(batch != '---') return;
			
			if(row.childNodes[0].innerText != default_pricelist_name) return;
			
			var x = ["#sales-pricestd","#sales-pricelist","#sales-pricelimit"][colIdx -3];
		
			jQuery(x).val(newValue);
			
			jQuery("#default-sales-pricestd").val( jQuery("#sales-pricestd").val() );
		};
	}
	
	var salesPriceGrid = new EditableGrid("SalesPriceGrid", {
		enableSort:false, 
		modelChanged: modelChangedListener
	});
	salesPriceGrid.load({"metadata": metadata, "data": salesPriceArray});
	salesPriceGrid.renderGrid("sales-price-grid", "testgrid");
	
	BackOffice.salesPriceGrid = salesPriceGrid;
}

function displayPurchasePrices()
{
	var purchasePriceArray = new Array();
	
	jQuery.each(BackOffice.model.purchaseprices, function(index, element){
		
		element["actions"] = '<i style="cursor:pointer" class="glyphicons glyphicons-pro glyphicons-bin" onclick="javascript:deletePrice('+false+','+index+','+element["m_product_id"]+','+element["m_pricelist_id"]+','+element["m_pricelist_version_id"]+');"/>';
		purchasePriceArray.push({id:index, values:element});
	});
	
	var isbatchandexpiry = BackOffice.model["isbatchandexpiry"] == 'Y';
	
	var metadata = [];
	metadata.push({ name: "name", label: "Purchase Pricelist" , datatype: "string", editable: false});
	metadata.push({ name: "istaxincluded", label: "Tax Incl", datatype: "string", editable: false});
	
	if(isbatchandexpiry){
		metadata.push({ name: "lot", label: "Lot/Expiry", datatype: "string", editable: false});
	}
	
	metadata.push({ name: "pricestd", label: " Current", datatype: "double", editable: BackOffice.allowUpdatePrice});
	metadata.push({ name: "actions", label: " ", datatype: "html", editable: false});
	
	var modelChangedListener = function(rowIdx, colIdx, oldValue, newValue, row) {};	
	var default_pricelist_name = TerminalManager.terminal.po_priceList;
	
	if(isbatchandexpiry){
		modelChangedListener = function(rowIdx, colIdx, oldValue, newValue, row) {	
						
			var batch = row.childNodes[2].innerText;
			if(batch != '---') return;
			
			if(row.childNodes[0].innerText != default_pricelist_name) return;
			
			var x = ["#purchase-pricestd"][colIdx -3];
			jQuery(x).val(newValue);
		};
	}
	
	var purchasePriceGrid = new EditableGrid("PurchasePriceGrid", {
		modelChanged: modelChangedListener,
		enableSort:false
		});
	
	purchasePriceGrid.load({"metadata": metadata, "data": purchasePriceArray});
	purchasePriceGrid.renderGrid("purchase-price-grid", "testgrid");
	
	BackOffice.purchasePriceGrid = purchasePriceGrid;
}

function displayMultiplePriceTable()
{
	var salesPrices = BackOffice.model.salesprices;
	var purchasePrices = BackOffice.model.purchaseprices;
	
	var multiple_price_list_container = jQuery("#multiple-price-list-container");
		
	if(salesPrices.length == 0 || purchasePrices.length == 0)
	{
		multiple_price_list_container.hide();
	}
	else
	{
		multiple_price_list_container.show();
		jQuery("#manage-multiple-price-list-checkbox").attr('checked', 'checked');
	}
	
}

function deletePrice(isSOPrice, lineId, productId, pricelistId, pricelistVersionId, m_attributesetinstance_id)
{
	if( BackOffice.model.m_product_id > 0 && BackOffice.allowUpdatePrice == false )
	{
		return;
	}
	
	var pricesGrid = BackOffice.salesPriceGrid.data;
	var prices = BackOffice.model.salesprices;
	
	if (!isSOPrice)
	{
		pricesGrid = BackOffice.purchasePriceGrid.data;
		prices = BackOffice.model.purchaseprices;
	}
	
	if (pricelistVersionId == undefined)
	{
		/*Not yet created, remove only from grid*/
		for(var i=0; i<pricesGrid.length; i++)
		{
			if (pricesGrid[i]["originalIndex"] == lineId)
			{
				/*remove from grid*/
				if (isSOPrice)
				{
					BackOffice.model.salesprices.splice(pricesGrid[i]["id"], 1);
					BackOffice.salesPriceGrid.remove(lineId);
				}
				else
				{
					BackOffice.model.purchaseprices.splice(pricesGrid[i]["id"], 1);
					BackOffice.purchasePriceGrid.remove(lineId);
				}
			}
		}
	}
	
	jQuery(document).ajaxSend(function() {
		jQuery("#indicator").show();
	});

	jQuery(document).ajaxStop(function() {
		jQuery("#indicator").hide();
	});
	
	var params = {
					action: "deleteProductPrice",
					productId: productId,
					pricelistId: pricelistId,
					pricelistVersionId: pricelistVersionId,
					m_attributesetinstance_id: m_attributesetinstance_id
				};
	
	jQuery.ajax({
		type: "POST",
		url: "DataTableAction.do",
		data: params
	}).done(function(data, textStatus, jqXHR){
		
		if(data == null || jqXHR.status != 200){
    		alert('Failed to delete!');
    	}
    	
    	var json = JSON.parse(data);
    	
    	if(json.success)
    	{
    		BackOffice.displayMessage(json.message);
    		
    		
    		var deletedObjectIndex;
    		
    		jQuery.each(prices, function(index, price){
    			
    			if(price["m_product_id"] == json["m_product_id"] &&
    					price["m_pricelist_id"] == json["m_pricelist_id"] &&
    					price["m_pricelist_version_id"] == json["m_pricelist_version_id"] &&
    					price["m_attributesetinstance_id"] == json["m_attributesetinstance_id"]
    			)
    			{
    				deletedObjectIndex = index;
    			}
    		});
    		
    		prices.splice(deletedObjectIndex, 1);
    		
    		if (isSOPrice)
    		{
    			BackOffice.model.salesprices = prices;
    		}
    		else
    		{
    			BackOffice.model.purchaseprices = prices;
    		}
    		
    		BackOffice.renderDataTable();
    		
    		jQuery.each(pricesGrid, function(index, price){
    			
    			if (price.id == deletedObjectIndex)
    			{
    				if (isSOPrice)
    				{
    					BackOffice.salesPriceGrid.remove(index);
    				}
    				else
    				{
    					BackOffice.purchasePriceGrid.remove(index);
    				}
    				
    			}
    		});
    		
    	}
    	else
    	{
    		BackOffice.displayError(json.message);
    	}
	});
}

jQuery(document).ready(function(e){
	jQuery("#pairing-add-button").on("click", function(e){
		var m_product_bom_id = 0;
		var m_productbom_id = jQuery("#pairing-product-id").val();
		var bomqty = jQuery("#pairing-qty-textfield").val();
		var m_product_name = jQuery("#pairing-autocompleter").val();
		
		if( m_productbom_id == 0 ){
			return;
		}
		
		var html = template_pairing
			.replace(/{m_product_name}/g, m_product_name)
			.replace(/{qty}/g, bomqty)
			.replace(/{m_productbom_id}/g, m_productbom_id)
			.replace(/{m_product_bom_id}/g, m_product_bom_id);
		
		jQuery("#pairing-tbody").append(html);
		
		jQuery("#pairing-qty-textfield").val("1");
		jQuery("#pairing-autocompleter").val("");
        jQuery("#pairing-product-id").val("0");
	});
	
	jQuery('#add-variant-button').on('click', function(){
		
		resetVariants();
		
		var variant = jQuery.extend({}, BackOffice.defaults);
		
		if (jQuery("[name=isserialno]:checked").attr('id') == 'isSerialNo')
		{
			/*Serial No*/
			
			showSerialNoGrid();
			
			/*pre fill form from parent values*/
			jQuery("[name=serial-no-pricestd]").val(jQuery("[name=sales_pricestd]").val());
			jQuery("[name=serial-no-pricelist]").val(jQuery("[name=sales_pricelist]").val());
			jQuery("[name=serial-no-pricelimit]").val(jQuery("[name=sales_pricelimit]").val());
			
			/*Add Validation*/
			jQuery("[name=serial-no]").attr("data-validation", "required");
			
			//showSerialNoForm();
			
			variant["isserialno"] = "Y";
			
			jQuery('#serial-no-form-container').dialog({
				modal: true,
				title: 'Serial no',
				position: { my: "top", at: "top", of: window },
				width: 400,
				buttons: { Ok :{
					'text' : 'Ok',
					'class' : 'btn-primary',
					'id' : 'button-variant-serial',
					'title' : 'Serial No',
					'click' : function(){
						
						var serialNo = jQuery('[name=serial-no]').val();
						
						if ((serialNo == null || serialNo == ''))
						{
							alert(Translation.translate("please.fill.serial.no"));
							return;
						}
						
						/*add variant to editable grid*/
						
						variant["serialno"] = serialNo;
						variant["sales_pricestd"] = jQuery('[name=serial-no-pricestd]').val();
						variant["sales_pricelist"] = jQuery('[name=serial-no-pricelist]').val();
						variant["sales_pricelimit"] = jQuery('[name=serial-no-pricelimit]').val();
						variant["upc"] = jQuery('[name=serial-no-upc]').val();
						variant["sku"] = jQuery('[name=serial-no-sku]').val();
						
						BackOffice.variants.push(variant);
						displayVariants();
						
						jQuery(this).dialog('close');
					}
				}
					
				}
			});
			
			jQuery('#button-variant-serial').height = '20px';
			jQuery('#button-variant-serial').width = '35px';
			jQuery('#button-variant-serial').html(Translation.translate("ok"));
		}
		else
		{
			showVariantGrid();
			
			/*pre fill form from parent values*/
			jQuery("[name=variant-pricestd]").val(jQuery("[name=sales_pricestd]").val());
			jQuery("[name=variant-pricelist]").val(jQuery("[name=sales_pricelist]").val());
			jQuery("[name=variant-pricelimit]").val(jQuery("[name=sales_pricelimit]").val());
			
			/*Add Validation*/
			jQuery("[name=variant1]").attr("data-validation", "required");
			
			//showVariantForm();
			
			jQuery('#variant-form-container').dialog({
				modal:true,
				title : 'Variant',
				position: { my: "top", at: "top", of: window },
				width: 400,
				buttons: {"Ok": {
					
					click: function(){
					
					var variant1 = jQuery('[name=variant1]').val();
					var variant2 = jQuery('[name=variant2]').val();
					var variant3 = jQuery('[name=variant3]').val();
					
					if ((variant1 == null || variant1 == '') && 
							(variant2 == null || variant2 == '') &&
							(variant3 == null || variant3 == ''))
					{
						alert(Translation.translate("please.fill.least.one.variant"));
						return;
					}
					
					/*add variant to editable grid*/
					
					variant["variant1"] = variant1;
					variant["variant2"] = variant2;
					variant["variant3"] = variant3;
					variant["sales_pricestd"] = jQuery('[name=variant-pricestd]').val();
					variant["sales_pricelist"] = jQuery('[name=variant-pricelist]').val();
					variant["sales_pricelimit"] = jQuery('[name=variant-pricelimit]').val();
					variant["upc"] = jQuery('[name=variant-upc]').val();
					variant["sku"] = jQuery('[name=variant-sku]').val();
					
					BackOffice.variants.push(variant);
					displayVariants();
					
					
					jQuery(this).dialog('close');
				},
				"class" : "btn-primary",
				"text" : "Ok",
				'id' : 'button-variant'
			}}
			});
			
			jQuery('#button-variant').height = '20px';
			jQuery('#button-variant').width = '35px';
			jQuery('#button-variant').html(Translation.translate("ok"));
		}
		
		//BackOffice.variant = variant;
		jQuery("#button-variant-serial").html(Translation.translate("ok"));
	});
	
	var model = BackOffice.model;
	jQuery('#add-sales-price-button').on('click', function(){
		
		if(BackOffice.model.m_product_id > 0 && !BackOffice.allowUpdatePrice){
			return;
		}
		
		/*pre fill form*/
		jQuery("[name=sales-price-form-popup-pricestd]").val(jQuery("[name=sales_pricestd]").val());
		jQuery("[name=sales-price-form-popup-pricelist]").val(jQuery("[name=sales_pricelist]").val());
		jQuery("[name=sales-price-form-popup-pricelimit]").val(jQuery("[name=sales_pricelimit]").val());
		
		/*build pricelist dropdown*/
		jQuery("#sales-pricelist-select").html('');
		
		jQuery.each(BackOffice.model.salespricelists, function(index, salesPricelist){
			jQuery("#sales-pricelist-select").append('<option style="display:block;" value="'+salesPricelist.m_pricelist_id+'" istaxincluded="'+salesPricelist.istaxincluded+'">'+salesPricelist.name+'</option>');
		});
		
		/*display only pricelists which have not been used yet*/
		var prices = BackOffice.salesPriceGrid.data;
		
		var isbatchandexpiry = BackOffice.model["isbatchandexpiry"] == 'Y';
		
		if(!isbatchandexpiry){
			jQuery.each(prices, function(index, price){
				
				var usedPricelistId = price.values["m_pricelist_id"];
				
				jQuery("#sales-pricelist-select option").each(function(i, option){
					
					if (usedPricelistId == jQuery(this).val())
					{
						jQuery(this).hide();
					}
				});
				
			});
		}
		
		
		if (jQuery('#sales-pricelist-select option[style*="display:block"]').length == 0)
		{
			//alert('All pricelist already used! Edit price from grid');

			BootstrapDialog.show({
				message: jQuery('<div>'+Translation.translate("all.pricelist.have.already")+'&nbsp;<a href="backoffice-pricelist.do">' + Translation.translate("manage.price.list")+'</a></div>')
			});
			
			return;
		}
		
		jQuery('#sales-pricelist-select').val(jQuery('#sales-pricelist-select option[style*="display:block"]:first').val());		
		jQuery('#sales-price-form-popup-isTaxInclusive-span').text(jQuery('#sales-pricelist-select option:selected').attr('istaxincluded'));
		
		
		
		if(isbatchandexpiry){
			jQuery("#sales-pricelist-lot-select-container").show();
			
			//build select
			jQuery("#sales-pricelist-lot-select").html('');
			
			jQuery.each(BackOffice.model.lots, function(index, lot){
				jQuery("#sales-pricelist-lot-select").append('<option style="display:block;" value="'+lot.m_attributesetinstance_id+'">'+lot.description+'</option>');
			});
			
			jQuery("#sales-pricelist-lot-select").val(0);
		}
		else{
			jQuery("#sales-pricelist-lot-select-container").hide();
		}
		
		jQuery('#sales-price-form-popup').dialog({
			modal: true,
			title: 'Sales Price',
			position: { my: "top", at: "top", of: window },
			buttons: {Ok : {'text' : 'Ok',
						"class" : 'btn-primary btn-sales-price-ok',
						"click" : function(){
							
							var json = {};
							json["m_product_id"] = BackOffice.model["m_product_id"];
							json["pricestd"] = jQuery("[name=sales-price-form-popup-pricestd]").val();
							json["pricelist"] = jQuery("[name=sales-price-form-popup-pricelist]").val();
							json["pricelimit"] = jQuery("[name=sales-price-form-popup-pricelimit]").val();
							json["m_pricelist_id"] = jQuery("#sales-pricelist-select").val();
							json["istaxincluded"] = jQuery("#sales-price-form-popup-isTaxInclusive-span").text();
							json["name"] = jQuery("#sales-pricelist-select option:selected").text();
							
							if(isbatchandexpiry){
								json["lot"] = jQuery("#sales-pricelist-lot-select option:selected").text();
								json["m_attributesetinstance_id"] = jQuery("#sales-pricelist-lot-select").val();
							}
							
							BackOffice.model.salesprices.push(json);
							
							displaySalesPrices();
							
							jQuery("#sales-price-form-popup").dialog("close");
						}
			}
		}
		});
		
		/*jQuery('#sales-price-form-popup').dialog('option', 'position', 'center');*/
		jQuery(".btn-sales-price-ok").html(Translation.translate("ok"));
	});
	
	jQuery("#sales-pricelist-select").on('change', function(){
		
		jQuery('#sales-price-form-popup-isTaxInclusive-span').text(jQuery('#sales-pricelist-select option:selected').attr('istaxincluded'));
	});
	
	
	
	jQuery('#add-purchase-price-button').on('click', function(){
		
		/*pre fill form*/
		jQuery("[name=purchase-price-form-popup-pricestd]").val(jQuery("[name=purchase_pricestd]").val());
		
		/*build pricelist dropdown*/
		jQuery("#purchase-pricelist-select").html('');
		
		jQuery.each(BackOffice.model.purchasepricelists, function(index, purchasePricelist){
			jQuery("#purchase-pricelist-select").append('<option style="display:block;" value="'+purchasePricelist.m_pricelist_id+'" istaxincluded="'+purchasePricelist.istaxincluded+'">'+purchasePricelist.name+'</option>');
		});
		
		
		/*display only pricelists which have not been used yet*/
		var prices = BackOffice.purchasePriceGrid.data;
		
		var isbatchandexpiry = BackOffice.model["isbatchandexpiry"] == 'Y';
		
		if(!isbatchandexpiry){
			jQuery.each(prices, function(index, price){
				
				var usedPricelistId = price.values["m_pricelist_id"];
				
				jQuery("#purchase-pricelist-select option").each(function(i, option){
					
					if (usedPricelistId == jQuery(this).val())
					{
						jQuery(this).hide();
					}
				});
				
			});
		}
			
		
		if (jQuery('#purchase-pricelist-select option[style*="display:block"]').length == 0)
		{
			//alert('All pricelist already used! Edit price from grid');
			
			BootstrapDialog.show({
				message: jQuery('<div>'+Translation.translate("all.pricelist.have.already")+'&nbsp;<a href="backoffice-pricelist.do">' + Translation.translate("manage.price.list")+'</a></div>')
			});
			
			return;
		}
		
		
		jQuery('#purchase-pricelist-select').val(jQuery('#purchase-pricelist-select option[style*="display:block"]:first').val());
		
		
		jQuery('#purchase-price-form-popup-isTaxInclusive-span').text(jQuery('#purchase-pricelist-select option:selected').attr('istaxincluded'));
		
		
		if(isbatchandexpiry){
			jQuery("#purchase-pricelist-lot-select-container").show();
			
			//build select
			jQuery("#purchase-pricelist-lot-select").html('');
			
			jQuery.each(BackOffice.model.lots, function(index, lot){
				jQuery("#purchase-pricelist-lot-select").append('<option style="display:block;" value="'+lot.m_attributesetinstance_id+'">'+lot.description+'</option>');
			});
			
			jQuery("#purchase-pricelist-lot-select").val(0);
		}
		else{
			jQuery("#purchase-pricelist-lot-select-container").hide();
		}
		
		jQuery('#purchase-price-form-popup').dialog({
			modal: true,
			title: 'Purchase Price',
			position: { my: "top", at: "top", of: window },
			buttons: {Ok : {'text' : 'Ok',
						"class" : 'btn-primary btn-purchase-price-ok',
						"text" : 'Purchase Price',
						"click" : function(){
							
							var json = {};
							json["m_product_id"] = BackOffice.model["m_product_id"];
							json["pricestd"] = jQuery("[name=purchase-price-form-popup-pricestd]").val();
							json["pricelist"] = jQuery("[name=purchase-price-form-popup-pricestd]").val();
							json["pricelimit"] = jQuery("[name=purchase-price-form-popup-pricestd]").val();
							json["m_pricelist_id"] = jQuery("#purchase-pricelist-select").val();
							json["istaxincluded"] = jQuery("#purchase-price-form-popup-isTaxInclusive-span").text();
							json["name"] = jQuery("#purchase-pricelist-select option:selected").text();
							
							if(isbatchandexpiry){
								json["lot"] = jQuery("#purchase-pricelist-lot-select option:selected").text();
								json["m_attributesetinstance_id"] = jQuery("#purchase-pricelist-lot-select").val();
							}
							
							BackOffice.model.purchaseprices.push(json);
							
							displayPurchasePrices();
							
							jQuery("#purchase-price-form-popup").dialog("close");
						}
			}
		}
			
		});
		
		jQuery(".btn-purchase-price-ok").html(Translation.translate("ok"));
	});


	jQuery("#purchase-pricelist-select").on('change', function(){
		
		jQuery('#purchase-price-form-popup-isTaxInclusive-span').text(jQuery('#purchase-pricelist-select option:selected').attr('istaxincluded'));
	});		
	
	var fields = {
			m_product_name : {
				tooltip: Translation.translate("tooltip.name.wish.to.display")
			},
			description : {
				tooltip: Translation.translate("tooltip.short.des.easy.identification")
			},
			upc : {
				tooltip: Translation.translate("tooltip.enter.barcode.information")
			},
			c_uom_id : {
				tooltip: Translation.translate("tooltip.select.unit.measure.item")
			},
			sales_pricestd : {
				tooltip: Translation.translate("tooltip.default.listed.price.item")
			},
			sales_pricelist : {
				tooltip: Translation.translate("tooltip.original.price.msrp")
			},
			sales_pricelimit : {
				tooltip: Translation.translate("tooltip.minimum.price.dis")
			},
			purchase_pricestd : {
				tooltip: Translation.translate("tooltip.purchase.cost.order.product")
			},
			sku : {
				tooltip: Translation.translate("tooltip.sku.inventory.item")
			},
			hasModifiers : {
				tooltip: Translation.translate("tooltip.options.this.item"),
				position : 'top'
			},
			isserialno : {
				tooltip: Translation.translate("tooltip.select.this.product.serial.no")
			},
			producttype : {
				tooltip: Translation.translate("tooltip.check.box.item.being.service"),
				position : 'top'
			},
			itemCombination : {
				tooltip: Translation.translate("tooltip.does.this.item.combine.products"),
				position : 'top'
			}
		};
	//Include Global Color 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
	
	jQuery("[name=isserialno]").on('click', function(){
		
		if (jQuery("[name=isserialno]:checked").attr("id") == "is-variant-number")
		{
			//change the name of the button when variant is selected
			jQuery("#add-variant-button").text(Translation.translate("add.variant"));
			
			BackOffice.model["isserialno"] = 'N';
			buildVariantGrid(new Array(), "Variant1", "Variant2", "Variant3");
		}
		else
		{
			//change the name of the button when serial is selected
			jQuery("#add-variant-button").text(Translation.translate("add.serial"));
			
			BackOffice.model["isserialno"] = 'Y';
			buildSerialNoGrid(new Array());
		}
	});	
	
	jQuery('#choose-serial-variants').on('click', function(){
		
		if (jQuery('#choose-serial-variants').is(':checked'))
		{
			jQuery('#variants-main-container').css('display', 'block');
		}
		else
		{
			jQuery('#variants-main-container').css('display', 'none');
		}
	});
	
	jQuery('#has-modifier-checkbox').on('click', function(){
		
		if (jQuery('#has-modifier-checkbox').is(':checked'))
		{
			jQuery('#modifier-group-main-container').css('display', 'block');
			jQuery("#modifier-group-container").css('display', 'block');
		}
		else
		{
			jQuery('#modifier-group-main-container').css('display', 'none');
			jQuery("#modifier-group-container").css('display', 'block');
		}
	});
	
	jQuery("#backoffice-export-btn-csv-min-columns").on('click', function(){
		
		BackOffice.exportMinColumns("csv");
	});
	
	jQuery("#backoffice-export-btn-excel-min-columns").on('click', function(){
		
		BackOffice.exportMinColumns("xls");
	});
	
});

function showInventories(inventory) {
	
	var model = BackOffice["model"];
	
	inventory.isbatchandexpiry = model["isbatchandexpiry"] == 'Y';

	var source   = jQuery("#view-inventory-details").html();
	var template = Handlebars.compile(source);
	
	jQuery("#show-inventory-details").html(template(inventory));
	
	source   = jQuery("#view-groupings-details").html();
	template = Handlebars.compile(source);	
	
	var groupings = [];
	groupings["primarygroup"] = model["primarygroup"];
	groupings["group1"] = model["group1"];
	groupings["group2"] = model["group2"];
	groupings["group3"] = model["group3"];
	groupings["group4"] = model["group4"];
	groupings["group5"] = model["group5"];
	groupings["group6"] = model["group6"];
	groupings["group7"] = model["group7"];
	groupings["group8"] = model["group8"];
	
	jQuery("#show-groupings-details").html(template(groupings));
}

/*
function showDiscountPercentage()
{
	var discountCodes = BackOffice["discountcodes"];
	var selectedVal = jQuery("#u_pos_discountcode_id").val();
	
	for(var i=0; i<discountCodes.length; i++)
	{
		var discountCode = discountCodes[i];
		
		if (discountCode["u_pos_discountcode_id"] == selectedVal)
		{
			jQuery("#discount-code-percentage").html(discountCode["percentage"]);
			jQuery("#discount-code-percentage-container").show();
			break;
		}
		
		if (selectedVal == "")
		{
			jQuery("#discount-code-percentage").html("");
			jQuery("#discount-code-percentage-container").hide();
		}
	}
}
*/

Handlebars.registerHelper("getWarehouseName", function(m_warehouse_id) {
	
	var warehouses = BackOffice["warehouses"];
	
	for(var i=0; i < warehouses.length; i++){
		
		var warehouse = warehouses[i];
		
		if(warehouse.m_warehouse_id == m_warehouse_id) {
			return warehouse.m_warehouse_name;
		};
		
	};
	
});

Handlebars.registerHelper("getModifierName", function(id) {
	
	var modifiers = BackOffice["modifiergroups"];
	
	for (var i=0; i<modifiers.length; i++)
	{
		var modifier = modifiers[i];
		var modifier_id = modifier["u_modifiergroup_id"];
		
		if(modifier_id == id)
		{
			return modifier["u_modifiergroup_name"];
		}
	}	
	
});

BackOffice.showActionButtons = function(){
	
	var model = BackOffice["model"];
	var isActive = model.isactive == 'Y' ? true : false;
	
	if(isActive)
	{
		jQuery(".backoffice-btn-activate").each(function(index, button){
			
			button.hide();
		});
		
		jQuery(".backoffice-btn-deactivate").each(function(index, button){
			
			button.show();
		});
	}
	else
	{
		jQuery(".backoffice-btn-activate").each(function(index, button){
			
			button.show();
		});
		
		jQuery(".backoffice-btn-deactivate").each(function(index, button){
			
			button.hide();
		});
	}
	
		var isDiscontinued = BackOffice.model.discontinued == 'Y' ? true : false;
		
		if(isDiscontinued)
		{
			jQuery(".backoffice-btn-activate").hide();
		}
};

BackOffice.uploadImage = function(){
	
	var image_url = jQuery("#image-url").val();	
	
	//jQuery('.view-image').attr('src', '');
	
	var imageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + BackOffice["model"][BackOffice.ID_COLUMN_NAME]  + "&12345";
	
	if (image_url != "")
	{
		imageUrl = image_url;
	}
	
	/*var imageUrl = '';
	
	if(image_url == "")
	{
		imageUrl = image_url
	}
	
	else if(image_url.indexOf("AttachmentServlet")>-1)
	{
		var imageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + BackOffice["model"][BackOffice.ID_COLUMN_NAME];
	}
	
	else
	{
		imageUrl = image_url
	}*/
	
	jQuery("#upload-image-pop-up").dialog({
		modal: true,
		height: '400',
		width: '600',
		title: Translation.translate('upload.image.title'),
		autoOpen: false,
		open: function(event, ui){
			setTimeout(function(){jQuery('#upload-image-frame').contents().find('#image-panel-frame').attr('src', imageUrl);},1000);
		},
		buttons: {
			Capture : {
				'id' : 'capture-img-btn',
				'text' : 'Capture Image',
				'class': 'btn btn-default',
				'click' : function(){
					jQuery('#upload-image-pop-up').dialog('close');
					BackOffice.captureImage();
				}
			},
			
			Save : {
				'id' : 'save-img-btn',
				'text' : 'Save',
				'class' : 'btn btn-primary',
				'click' : function(){
					/*jQuery('[name=tableId]').val(BackOffice.AD_TABLE_ID);
					jQuery('[name=recordId]').val(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
					jQuery('[name=modelName]').val(BackOffice.MODEL_NAME);
					jQuery('[name=controller]').val(BackOffice.MODEL_NAME);
					jQuery('#ImageForm').submit();*/
					jQuery("#image-url").val('');
					//BackOffice.model.imageurl = '';
					BackOffice.model.productimageurl = '';
					jQuery('#upload-image-frame').contents().find('[name=tableId]').val(BackOffice.AD_TABLE_ID);
					jQuery('#upload-image-frame').contents().find('[name=recordId]').val(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
					jQuery('#upload-image-frame').contents().find('[name=modelName]').val(BackOffice.MODEL_NAME);
					jQuery('#upload-image-frame').contents().find('[name=controller]').val(BackOffice.MODEL_NAME);
					jQuery('#upload-image-frame').contents().find('#ImageForm').submit();
					
					setTimeout(function(){
						jQuery('#upload-image-pop-up').dialog('close');						
						
						BackOffice.view(BackOffice["model"][BackOffice.ID_COLUMN_NAME]);
						
						var image_url_view = BackOffice.model.productimageurl;
						
						if (image_url_view != "")
						{
							imageUrl = image_url_view;
						}						
						else
						{
							imageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + BackOffice["model"][BackOffice.ID_COLUMN_NAME]  + "&12345";
						}
						
						jQuery('.view-image').attr('src', imageUrl);
						
						jQuery("#image-url").val('');
						
						},2000);					
				}
				
			}
		}
		
	});
	
	jQuery('#upload-image-pop-up').dialog('open');
	
	jQuery("#save-img-btn").html(Translation.translate("save"));
	jQuery("#capture-img-btn").html(Translation.translate("capture.image"));
};

/*Handlebars.registerHelper('getImage', function(id) {
	var image_url = BackOffice.model.imageurl;
	
	var imageUrl = '';
	
	if(image_url == "")
	{
		imageUrl = image_url
	}
	
	else if(image_url.indexOf("AttachmentServlet")>-1)
	{
		var imageUrl = "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + id
	}
	
	else
	{
		imageUrl = image_url
	}	
	
	return imageUrl;
});*/

Handlebars.registerHelper('getImage', function(id) {
	var imageUrl = '';	
	
	//var image_url = BackOffice.model.imageurl;
	var image_url = BackOffice.model.productimageurl;
	
	if (image_url != "")
	{
		imageUrl = image_url;
	}
	else
	{
		imageUrl = jQuery('base').attr('href') + "AttachmentServlet?tableId="+ BackOffice.AD_TABLE_ID+ "&recordId=" + id;
	}
		
	return imageUrl;
});

/* filter */
jQuery(function(){
	
	jQuery("#filter_button").on("click", function(){
		
		BackOffice.renderDataTable();
		
	});
	
	jQuery( "#filter_primarygroup" ).autocomplete({
		
		source: "DataTableAction.do?action=search&field=product&column=primarygroup",
		
		create: function() {
			
			var label = "primarygroup";
			
			jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
				
				if(item == null) return jQuery("<li>");
				
			      return jQuery( "<li>" )
			        .data( "item.autocomplete", item )
			        .append( "<a>" + item[label] + "</a>" )
			        .appendTo( ul );
			};
	    },
	    
	    select: function(event, ui) {
	    	
            var field = jQuery(this);
            
            var label = "primarygroup";
            var value = "primarygroup";
            
            field.val(ui.item[label]);

            return false;
        },
        
        change: function(event, ui) {
            var field = jQuery(this);
           
            console.log(field.val());
            
            return false;
        }
		
	});
	
	
jQuery( "#filter_group1" ).autocomplete({
		
		source: "DataTableAction.do?action=search&field=product&column=group1",
		
		create: function() {
			
			var label = "group1";
			
			jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
				
				if(item == null) return jQuery("<li>");
				
			      return jQuery( "<li>" )
			        .data( "item.autocomplete", item )
			        .append( "<a>" + item[label] + "</a>" )
			        .appendTo( ul );
			};
	    },
	    
	    select: function(event, ui) {
	    	
            var field = jQuery(this);
            
            var label = "group1";
            var value = "group1";
            
            field.val(ui.item[label]);

            return false;
        },
        
        change: function(event, ui) {
            var field = jQuery(this);
           
            console.log(field.val());
            
            return false;
        }
		
	});

jQuery( "#filter_group2" ).autocomplete({
	
	source: "DataTableAction.do?action=search&field=product&column=group2",
	
	create: function() {
		
		var label = "group2";
		
		jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
			
			if(item == null) return jQuery("<li>");
			
		      return jQuery( "<li>" )
		        .data( "item.autocomplete", item )
		        .append( "<a>" + item[label] + "</a>" )
		        .appendTo( ul );
		};
    },
    
    select: function(event, ui) {
    	
        var field = jQuery(this);
        
        var label = "group2";
        var value = "group2";
        
        field.val(ui.item[label]);

        return false;
    },
    
    change: function(event, ui) {
        var field = jQuery(this);
       
        console.log(field.val());
        
        return false;
    }
	
});


jQuery( "#filter_group3" ).autocomplete({
	
	source: "DataTableAction.do?action=search&field=product&column=group3",
	
	create: function() {
		
		var label = "group3";
		
		jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
			
			if(item == null) return jQuery("<li>");
			
		      return jQuery( "<li>" )
		        .data( "item.autocomplete", item )
		        .append( "<a>" + item[label] + "</a>" )
		        .appendTo( ul );
		};
    },
    
    select: function(event, ui) {
    	
        var field = jQuery(this);
        
        var label = "group3";
        var value = "group3";
        
        field.val(ui.item[label]);

        return false;
    },
    
    change: function(event, ui) {
        var field = jQuery(this);
       
        console.log(field.val());
        
        return false;
    }
	
});


jQuery( "#filter_group4" ).autocomplete({
	
	source: "DataTableAction.do?action=search&field=product&column=group4",
	
	create: function() {
		
		var label = "group4";
		
		jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
			
			if(item == null) return jQuery("<li>");
			
		      return jQuery( "<li>" )
		        .data( "item.autocomplete", item )
		        .append( "<a>" + item[label] + "</a>" )
		        .appendTo( ul );
		};
    },
    
    select: function(event, ui) {
    	
        var field = jQuery(this);
        
        var label = "group4";
        var value = "group4";
        
        field.val(ui.item[label]);

        return false;
    },
    
    change: function(event, ui) {
        var field = jQuery(this);
       
        console.log(field.val());
        
        return false;
    }
	
});


jQuery( "#filter_group5" ).autocomplete({
	
	source: "DataTableAction.do?action=search&field=product&column=group5",
	
	create: function() {
		
		var label = "group5";
		
		jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
			
			if(item == null) return jQuery("<li>");
			
		      return jQuery( "<li>" )
		        .data( "item.autocomplete", item )
		        .append( "<a>" + item[label] + "</a>" )
		        .appendTo( ul );
		};
    },
    
    select: function(event, ui) {
    	
        var field = jQuery(this);
        
        var label = "group5";
        var value = "group5";
        
        field.val(ui.item[label]);

        return false;
    },
    
    change: function(event, ui) {
        var field = jQuery(this);
       
        console.log(field.val());
        
        return false;
    }
	
});


jQuery( "#filter_group6" ).autocomplete({
	
	source: "DataTableAction.do?action=search&field=product&column=group6",
	
	create: function() {
		
		var label = "group6";
		
		jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
			
			if(item == null) return jQuery("<li>");
			
		      return jQuery( "<li>" )
		        .data( "item.autocomplete", item )
		        .append( "<a>" + item[label] + "</a>" )
		        .appendTo( ul );
		};
    },
    
    select: function(event, ui) {
    	
        var field = jQuery(this);
        
        var label = "group6";
        var value = "group6";
        
        field.val(ui.item[label]);

        return false;
    },
    
    change: function(event, ui) {
        var field = jQuery(this);
       
        console.log(field.val());
        
        return false;
    }
	
});


jQuery( "#filter_group7" ).autocomplete({
	
	source: "DataTableAction.do?action=search&field=product&column=group7",
	
	create: function() {
		
		var label = "group7";
		
		jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
			
			if(item == null) return jQuery("<li>");
			
		      return jQuery( "<li>" )
		        .data( "item.autocomplete", item )
		        .append( "<a>" + item[label] + "</a>" )
		        .appendTo( ul );
		};
    },
    
    select: function(event, ui) {
    	
        var field = jQuery(this);
        
        var label = "group7";
        var value = "group7";
        
        field.val(ui.item[label]);

        return false;
    },
    
    change: function(event, ui) {
        var field = jQuery(this);
       
        console.log(field.val());
        
        return false;
    }
	
});


jQuery( "#filter_group8" ).autocomplete({
	
	source: "DataTableAction.do?action=search&field=product&column=group8",
	
	create: function() {
		
		var label = "group8";
		
		jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
			
			if(item == null) return jQuery("<li>");
			
		      return jQuery( "<li>" )
		        .data( "item.autocomplete", item )
		        .append( "<a>" + item[label] + "</a>" )
		        .appendTo( ul );
		};
    },
    
    select: function(event, ui) {
    	
        var field = jQuery(this);
        
        var label = "group8";
        var value = "group8";
        
        field.val(ui.item[label]);

        return false;
    },
    
    change: function(event, ui) {
        var field = jQuery(this);
       
        console.log(field.val());
        
        return false;
    }
	
});
	
});

/* groups autocompletes */

BackOffice.renderProductGroupsAutocomplete = function() {
	
	var dfd = jQuery.Deferred();	
	
	var array = [
			"primarygroup",
			"group1",
			"group2",
			"group3",
			"group4",
			"group5",
			"group6",
			"group7",
			"group8"
		];
	
	jQuery(array).each(function(index, value){
		
		var selector = "[data-field='product-" + value + "']";
		var source = "DataTableAction.do?action=search&field=product&column=" + value;
		var labelKey = value;
		var valueKey = value;
		
		dfd.notify('rendering product ' + value + ' autocomplete');
		
		BackOffice.AUTO_COMPLETE(selector, source, labelKey, valueKey);		
		
	});
	
	dfd.resolve("Successfully rendered product autocomplete");
	
	return dfd.promise();
}; 

BackOffice.AUTO_COMPLETE = function( selector, src, labelKey, valueKey ){
	
	var autocomplete = jQuery( selector ).autocomplete({
		
		source: src,
		
		create: function() {
			
			var label = labelKey;
			
			jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
				
				if(item == null) return jQuery("<li>");
				
			      return jQuery( "<li>" )
			        .data( "item.autocomplete", item )
			        .append( "<a>" + item[label] + "</a>" )
			        .appendTo( ul );
			};
	    },
	    
	    select: function(event, ui) {
	    	
            var field = jQuery(this);
            
            var label = labelKey;
            var value = valueKey;
            
            field.val(ui.item[label]);

            var hiddenTextfieldId = field.attr('data-report-field-autocomplete');
            
            if( hiddenTextfieldId != null && hiddenTextfieldId.length > 0 ) {
            	
            	jQuery('#' + hiddenTextfieldId).val(ui.item[value]);
            }            

            return false;
        },
        
        change: function(event, ui) {
            var field = jQuery(this);
            var hiddenTextfieldId = field.attr('data-report-field-autocomplete');

            /*
            if (ui.item == null) {
                jQuery('#' + hiddenTextfieldId).val('');
                jQuery(this).val('');
            }
            */

            return false;
        }
		
	});	
	
	return autocomplete;
	
};

jQuery(function(){
	
	BackOffice.renderProductGroupsAutocomplete();
	
	var fn = BackOffice.showView.bind(BackOffice);
	
	BackOffice.showView = function(){
		
		BackOffice.preloader.show();
		
		var m_product_id = BackOffice.model["m_product_id"];
		
		jQuery.get( "DataTableAction.do?action=getVariants&m_product_parent_id=" + m_product_id ).done(function( data ) {
			
			console.log('loaded variants');
			
			//load variants
			BackOffice.variants = JSON.parse(data);
			
			fn();
			
		  })
		  .fail(function( err ) {
		    alert( "Failed to load variants" );
		  })
		  .always(function() {
			  BackOffice.preloader.hide();
		  });		
		
	};
	
});
