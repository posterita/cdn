BackOffice.ID_COLUMN_NAME = "u_promotion_id";
BackOffice.MODEL_NAME = "X_U_Promotion";
BackOffice.SAVE_URL = "DataTableAction.do?action=savePromotion";

BackOffice.defaults = {
		"u_promotion_id":0,
		"isactive": "Y",
		"earnloyaltypoints" : "Y",
		"resetloyaltypoints" : "N",
		"description": "",
		"amount":0,
		"expirydate" : moment().add(1,'years').format('DD-MM-YYYY'),
		"amount" : 0
		};

BackOffice.requestDataTableData = function(){
	
	jQuery.post("DataTableAction.do", { action: "getPromotions"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var promotions = json["promotions"];
								
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = promotions;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(promotions);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.afterSave = function(model){
	
	BackOffice.renderDataTable();
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	  	      { "data": "description" },
	  	      { "data": "points"},
		      { "data": "amount" },
		      { "data": "expirydate"}		      
		      
  	    ],
  	    "rowCallback" : function(row, data){
  	    	
  	    	var isactive = data.isactive; 
        	
        	if(isactive == 'N')
        	{
        		jQuery(row).css('text-decoration','line-through');
        		jQuery(row).css('font-style','italic');
        	}
  	    } ,
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      }    
	      
	    ]
	});
};

BackOffice.beforeSave = function(model) {
	
	/*var date = moment();
	var today = date.format("MM-DD-YYYY");
	
	var expiryDate = jQuery("#datepicker").val();
	
	if(today > expiryDate)
	{
		alert("Invalid expiry date");
	}*/
	
	if(model['expirydate'] == ""){
		model['expirydate'] = moment().add(1,'years').format('DD-MM-YYYY');
	}
	
	var earnloyaltypoints = "N";
	
	if(jQuery("#earn-loyalty-points").prop("checked")){
		earnloyaltypoints = "Y";
	}
	
	model["earnloyaltypoints"] = earnloyaltypoints;
	
	var resetloyaltypoints = "N";
	
	if(jQuery("#reset-loyalty-points").prop("checked")){
		resetloyaltypoints = "Y";
	}
	
	model["resetloyaltypoints"] = resetloyaltypoints;

}

BackOffice.afterShowDetail = function(){
	
	var model = this.model || this.defaults;
		
	jQuery( "#datepicker" ).datepicker( "setDate", model['expirydate'] );
	
		
}

BackOffice.afterCancel = function(){
	
	jQuery('.backoffice-btn-save').attr('style','background-color: #fff !important; color:#1d1d1d; border-color:#bdbec2 !important;');
};

BackOffice.afterSave = function(model){

	BackOffice.requestDataTableData();	
};

jQuery(document).ready(function(){
	jQuery('.backoffice-btn-save-promotion').on('click',function(){
				
		var form = jQuery("#form").get(0);
	    BackOffice.save(form);
		
	});
	
	jQuery( "#datepicker" ).datepicker({
		format: "dd-mm-yyyy",
		orientation: "top left",
		minDate:0
	}).on('hide', function (e) { e.preventDefault(); });
		
});
