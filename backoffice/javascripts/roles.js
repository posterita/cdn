BackOffice.ID_COLUMN_NAME = "ad_role_id";
BackOffice.MODEL_NAME = "MRole";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveRole";

BackOffice.defaults = {
		ad_role_id: "0",
		userdiscount: "0",
		allow_change_terminal: "N",
		allow_complete_inventorycount: "N",
		allow_inventory_void: "N",
		allow_order_backdate: "N",
		allow_order_exchange: "N",
		allow_order_refund: "N",
		allow_order_split: "N",
		allow_order_void: "N",
		allow_payment_void: "N",
		allow_stocktransfer_void: "N",
		allow_upsell: "N",
		isdiscountallowedontotal: "N",
		overwritepricelimit: "N",
		editattendance: "N",
		saveorders: "N",
		discountoncurrentprice: "N",
		isactive: "Y",
		viewpurchaseprice: "N",
		savestocktransfer: "N",
		/* warehouse mgt app */
		allow_pick_item : "N",
		allow_place_item : "N",
		allow_move_item : "N",
		allow_cycle_count : "N",
		allow_view_stock : "N",
		allow_reorder_level : "N",
		allow_complete_send_stock : "N",
		allow_reset_mappings : "N",
		allow_update_price : "Y",
		allow_save_shipment: "N",
		allow_void_purchase: "N",
	};

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getRoles"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}  
				
				var roles = json["roles"];				
				//BackOffice.setDataSource(roles);
				
				var roleorgs = json["roleorgs"];
				var rolemenus = json["rolemenus"];
				var menus = json["menus"];
				var orgs = json["orgs"];
				var assignedEmployees = json["assignedEmployees"];
				
				renderOrgAccess(orgs);
				renderMenus(menus);
				
				/* create two dbs */
				/* clean DBs */
				
				if (BackOffice.ORG_ACCESS_DB != undefined)
				{
					ORG_ACCESS_DB(query).remove(true);
				}
				
				if (BackOffice.ROLE_MENU_DB != undefined)
				{
					ROLE_MENU_DB(query).remove(true);
				}
				
				BackOffice.ORG_ACCESS_DB = TAFFY(roleorgs);
				BackOffice.ROLE_MENU_DB = TAFFY(rolemenus);
				BackOffice.ASSIGNED_EMPLOYEE_DB = TAFFY(assignedEmployees);
				
				BackOffice["orgs"] = orgs;
				BackOffice["menus"] = menus;
				BackOffice["roleMenus"] = rolemenus;
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = roles;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(roles);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};


BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	      { "data": "ad_role_name" },
	      { "data": "isactive"}
	      
	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 1
	      }
	    ]
	});
};

BackOffice.onShowDetail = function(){
	
	/* uncheck all menus and remove marker */
	jQuery(".menu-checkbox").removeAttr('checked').removeClass('checked-on-load');
	/* uncheck all orgs and remove marker */
	jQuery(".org-checkbox").removeAttr('checked').removeClass('checked-on-load');
	
	if(this.model == null){		
		return;
	}
	
	var model = this.model;	
	var ad_role_id = model["ad_role_id"];
	
	checkAssignedOrgs(ad_role_id);
	checkAssignedMenus(ad_role_id);
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
};

BackOffice.beforeSave = function(model){
	/* add menus-to-add and menus-to-remove */
	var menus_to_assign = [];
	var menus_to_remove = [];	
	
	/* get all menu checkboxes*/	
	var menu_checkboxes = jQuery(".menu-checkbox");	
	
	for(var i=0; i<menu_checkboxes.length; i++){
		var checkbox = menu_checkboxes[i];
		checkbox = jQuery(checkbox);
		
		var hasClass = checkbox.hasClass("checked-on-load");
		var isChecked = checkbox.prop("checked");
		var u_menu_id = checkbox.val();
		
		if(isChecked && !hasClass){
			/* is checked and was not marked */
			menus_to_assign.push(u_menu_id);
		}
		else if(!isChecked && hasClass){
			/* is not checked and was marked */
			menus_to_remove.push(u_menu_id);
		}
		else {
			/* do nothing */
		}
	}/* for */
	
	/* link model */
	model.menus_to_assign = menus_to_assign;
	model.menus_to_remove = menus_to_remove;
	
	
	/* add orgs-to-add and orgs-to-remove */
	var orgs_to_assign = [];
	var orgs_to_remove = [];	
	
	/* get all org checkboxes*/	
	var org_checkboxes = jQuery(".org-checkbox");	
	
	for(var i=0; i<org_checkboxes.length; i++){
		var checkbox = org_checkboxes[i];
		checkbox = jQuery(checkbox);
		
		var hasClass = checkbox.hasClass("checked-on-load");
		var isChecked = checkbox.prop("checked");
		var ad_org_id = checkbox.val();
		
		if(isChecked && !hasClass){
			/* is checked and was not marked */
			orgs_to_assign.push(ad_org_id);
		}
		else if(!isChecked && hasClass){
			/* is not checked and was marked */
			orgs_to_remove.push(ad_org_id);
		}
		else {
			/* do nothing */
		}
	}/* for */
	
	/* link model */
	model.orgs_to_assign = orgs_to_assign;
	model.orgs_to_remove = orgs_to_remove;
	
	/* it may happen than no store or menu is selected */
	if(model.menus == undefined){
		model.menus = [];
	}
	
	if(model.orgs == undefined){
		model.orgs = [];
	}
	
	/* allow update price */
	var allow_update_price = jQuery('#allow_update_price').prop('checked') ? "Y" : "N";		
	model["allow_update_price"] = allow_update_price;
};

BackOffice.afterSave = function(model){
	/* update local DB */
	var ROLE_MENU_DB = BackOffice.ROLE_MENU_DB;
	var ORG_ACCESS_DB = BackOffice.ORG_ACCESS_DB;
	
	var ad_role_id = model.ad_role_id;
	
	var query = {};
	query["ad_role_id"] = {'==':ad_role_id};
	
	/* clean DBs */
	ROLE_MENU_DB(query).remove(true);
	ORG_ACCESS_DB(query).remove(true);
	
	/* insert records */
	var menus = model.menus;
	if(menus){		
		for(var i=0; i<menus.length; i++){
			var u_webmenu_id = menus[i];
			ROLE_MENU_DB.insert({
				"u_webmenu_id" : u_webmenu_id,
				"ad_role_id" : ad_role_id
					});
		}
		
		/* mark all checkboxes */
		checkAssignedMenus(ad_role_id);
	}
	
	
	var orgs = model.orgs;
	if(orgs){
		for(var i=0; i<orgs.length; i++){
			var ad_org_id = orgs[i];
			ORG_ACCESS_DB.insert({
				"ad_org_id" : ad_org_id,
				"ad_role_id" : ad_role_id
					});
		}
		
		/* mark all checkboxes */
		checkAssignedOrgs(ad_role_id);	
	}
	
};

/* draws the store access part of the detail form */
function renderOrgAccess(orgs){
	var container = jQuery("#org-access-container");
	var html = "";
	
	for(var i=0; i<orgs.length; i++){
		var org = orgs[i];
		
		if(org.ad_org_id == 0) continue; /* do not show org(*) */
		
		html += '<div class="form-group left-input checkbox-container">' +
				'<label for="org_' + org.ad_org_id + '" class="checkcontainer">' +
				'<input type="checkbox" id="org_' + org.ad_org_id + '" name="orgs[]" value="' + org.ad_org_id + '" class="org-checkbox">' +
		    	'<label for="org_' + org.ad_org_id + '">&nbsp;' + org.ad_org_name + '</label>' +
		    	'<span class="checkmark"></span>' +
		    	'</label>' +
		  		'</div>';
	}
	
	container.html(html);
}

/* draws the menu part of the detail form */
function renderMenus(menus){
	var container = jQuery("#menu-container");
	var html = "";
	
	for(var i=0; i<menus.length; i++){
		var menu = menus[i];
		var title = menu.name;
		var categories = menu["categories"];
		
		html += '<div>';
		html += '<p class="left-input view-parent-menu">';
		html += Translation.translate(title);
		html += '</p>';
		html += '</div>';
		
		for(var j=0; j<categories.length; j++){
			var category = categories[j];
			var categoryKey = category["categorykey"];
			
			html += '<div>';
			html += '<p class="left-input role-category-menu">';
			html += '<label for="'+categoryKey+'" class="checkcontainer">'
			html += '<input type="checkbox" id="'+categoryKey+'" class="category-checkbox" onclick="javascript:checkSubMenu(this);">&nbsp;';
			html += Translation.translate(category["category"]);
			html += '<span class="checkmark"></span>';
			html += '</label>';
			html += '</p>';
			
			var links = category["menus"];
			
			for(var k=0; k<links.length; k++){
				var link = links[k];
				
				html += '<div class="form-group left-input checkbox-container role-sub-menu">' +				
		    	'<label class="checkcontainer" for="menu_' + link.id + '" >&nbsp;' + Translation.translate(link.name) + 
		    	'<input type="checkbox" categorykey="' +categoryKey+ '" id="menu_' + link.id + '" name="menus[]" value="' + link.id + '" class="menu-checkbox '+ categoryKey +'" onclick="javascript:checkOrUncheckCategory(this);">' +
		    	'<span class="checkmark"></span>' +
		    	'</label>' +
		  		'</div>';
			}
			
			html += '</div>';
		}
	}
	
	container.html(html);
}

function checkOrUncheckCategory(element)
{
	var checkbox = jQuery(element);
	var isAllChecked = true;
	
	var categoryKey = checkbox.attr('categorykey');
	
	jQuery('input[categorykey="'+categoryKey+'"]').each(function(index, element){
		
		var isChecked = jQuery(element).prop('checked');
		
		isAllChecked = isAllChecked && isChecked;
		
	});
	
	if (isAllChecked)
	{
		jQuery('#' + categoryKey).attr('checked', 'checked');
	}
	else
	{
		jQuery('#' + categoryKey).attr('checked', false);
	}
}

/* check all menus assigned to role */
function checkAssignedMenus(ad_role_id){
	/* query menus*/
	var ROLE_MENU_DB = BackOffice.ROLE_MENU_DB;
	var query = {};
	query["ad_role_id"] = {'==':ad_role_id};
	
	var menus = ROLE_MENU_DB(query).get();
	
	//menus = BackOffice.roleMenus;
	
	for(var i=0; i<menus.length; i++){
		var menu = menus[i];
		var u_webmenu_id = menu["u_webmenu_id"];
		
		var checkbox = document.getElementById("menu_" + u_webmenu_id); /* for perfomance issue do not use jquery here */
		if(checkbox){
			jQuery(checkbox).attr('checked', 'checked').addClass("checked-on-load");			
		}
	}
	
	var allMenus = BackOffice.menus;
	
	for(var i=0; i<allMenus.length; i++){
		var menu = allMenus[i];		
		var categories = menu["categories"];
		
		for(var j=0; j<categories.length; j++){
			var category = categories[j];
			var categoryKey = category["categorykey"];
			
			var isAllChecked = true;
			
			jQuery('input[categorykey="'+categoryKey+'"]').each(function(index, element){
				var isChecked = jQuery(element).prop('checked');
				
				isAllChecked = isAllChecked && isChecked;
			});
		
			if (isAllChecked)
			{
				jQuery('#' + categoryKey).attr('checked', 'checked');
			}
			else
			{
				jQuery('#' + categoryKey).attr('checked', false);
			}
			
			/*var className = '.' + categoryKey;
			
			jQuery('#menu-container').find(className).each(function(index, element){
					var isChecked = jQuery(element).prop('checked');
					
					isAllChecked = isAllChecked && isChecked;
				});
			
				if (isAllChecked)
				{
					jQuery('#' + categoryKey).attr('checked', 'checked');
				}*/
			}
		}
	
}

/* check all org assigned to role */
function checkAssignedOrgs(ad_role_id){
	/* query menus*/
	var ORG_ACCESS_DB = BackOffice.ORG_ACCESS_DB;
	var query = {};
	query["ad_role_id"] = {'==':ad_role_id};
	
	var orgs = ORG_ACCESS_DB(query).get();
	for(var i=0; i<orgs.length; i++){
		var org = orgs[i];
		var ad_org_id = org["ad_org_id"];
		
		var checkbox = document.getElementById("org_" + ad_org_id); /* for perfomance issue do not use jquery here */
		if(checkbox){
			jQuery(checkbox).attr('checked', 'checked').addClass("checked-on-load");;
		}
	}
}

function checkSubMenu(checkbox)
{
	checkbox = jQuery(checkbox);
	var isChecked = checkbox.prop("checked");
	
	if (isChecked)
	{
		/*jQuery('#menu-container input[class*="'+checkbox.attr("id")+'"]').each(function(index, element){
			
			jQuery(element).attr('checked', 'checked');
		});*/
		
		jQuery('input[categorykey="'+checkbox.attr("id")+'"]').each(function(index, element){
			jQuery(element).attr('checked', 'checked');
		});
	}
	else
	{
		/*jQuery('#menu-container input[class*="'+checkbox.attr("id")+'"]').each(function(index, element){
			
			jQuery(element).attr('checked', false);
		});*/
		
		jQuery('input[categorykey="'+checkbox.attr("id")+'"]').each(function(index, element){
			jQuery(element).attr('checked', false);
		});
	}
}

BackOffice.onCreate = function(){
	var model = BackOffice["model"];
	
	/*select all menus by default for new role*/
	if(model["ad_role_id"] == 0)
	{
		var allMenus = BackOffice.menus;
		
		for(var i=0; i<allMenus.length; i++){
			var menu = allMenus[i];		
			var categories = menu["categories"];
			
			for(var j=0; j<categories.length; j++){
				var category = categories[j];
				var categoryKey = category["categorykey"];
				
				var isAllChecked = true;
				
				jQuery('#menu-container input[class*="'+categoryKey+'"]').each(function(index, element){
					
						jQuery(element).attr('checked', 'checked');
						
					});
				
				jQuery('#' + categoryKey).attr('checked', 'checked');
				}
			}
	}
	
	/*give access to all orgs by default*/
	
	jQuery(".org-checkbox").each(function(index, element){
		jQuery(element).attr('checked', 'checked');
	});
};

BackOffice.afterRenderView = function(id) {
	BackOffice.preloader.show();
	
	var model = BackOffice["model"];
	var id = model["ad_role_id"];
	
	var ORG_ACCESS_DB = BackOffice.ORG_ACCESS_DB;
	var query = {};
	query["ad_role_id"] = {'==':id};
	
	var allOrgs = [];
	var orgs = BackOffice["orgs"];
	
	for(var i=1; i < orgs.length; i++)
	{
		var json = {};
		json.name = orgs[i].ad_org_name;
		json.hasAccess = 'N';
		
		query["ad_org_id"] = {'==':orgs[i].ad_org_id};
		
		var currentOrg = ORG_ACCESS_DB(query).get();
		
		if (currentOrg.length > 0)
		{
			json.hasAccess = 'Y';
		}
		
		allOrgs.push(json);
	}
	
	var finalOrgList = {};
	finalOrgList.stores = allOrgs;
	
	BackOffice.renderOtherViews("#view-stores", "#show-stores", finalOrgList);
	
	var ROLE_MENU_DB = BackOffice.ROLE_MENU_DB;
	query = {};
	query["ad_role_id"] = {'==':id};
	
	var allMenus = BackOffice.menus;
	var menu_list = [];
	
	for (i=0; i < allMenus.length; i++)
	{
		var flag_parent = false;
		var json = {};
		
		var categories =  allMenus[i]["categories"];
		
		for(j=0; j < categories.length; j++)
		{
			var flag_category = false;
			
			var category = {};
			var menus = [];
			
			var submenu = categories[j]["menus"];
			
			for (k=0; k<submenu.length; k++)
			{
				var web_menu = submenu[k];
				query["u_webmenu_id"] = {'==':web_menu.id};
				
				var menu = ROLE_MENU_DB(query).get();
				
				if (menu.length > 0)
				{	
					if (!flag_parent)
					{
						json.parent = Translation.translate(allMenus[i]["name"]);
						
						json.categories = [];
						
						flag_parent = true;
					}
					
					if (!flag_category)
					{
						category.name = Translation.translate(categories[j]["category"]);
						
						flag_category = true;
					}
					
					var menu = {};
					menu.name = Translation.translate(web_menu.name);
					
					menus.push(menu);
				}
			}
			
			if (Object.keys(category).length > 0)
			{
				category.menus = menus;
				json.categories.push(category);
			};
		}
		
		if (Object.keys(json).length > 0)
		{
			menu_list.push(json);
		};
	}
	
	var menu = {};
	
	menu.menus = menu_list;

	BackOffice.renderOtherViews("#view-menus", "#show-menus", menu);
	
	//set height of the menu accessible
	var heightStoresAccessible = jQuery("#show-stores").height() - jQuery("#show-stores div").eq(0).height();
	
	if (heightStoresAccessible < 150)
	{
		heightStoresAccessible = 200;
	}
	
	//jQuery(".view-menus-body").height(heightStoresAccessible);
	
	var query = {};
	query["ad_role_id"] = {"==":id};
	
	var employees = BackOffice.ASSIGNED_EMPLOYEE_DB(query).get();
	
	jQuery("#show-employees").innerHTML = '';	
	
	for(var i=0;i<employees.length;i++)
	{
		var employee = employees[i];
		
		for(var j=0; j<employee.employees.length;j++)
		{
			var emp = employee.employees[j];			
			
			var div = document.createElement('div');
			div.innerHTML = emp.name;
			jQuery("#show-employees").append(div);
			//BackOffice.renderOtherViews("#view-employees", "#show-employees", emp);
		}
	}	
	
	BackOffice.preloader.hide();
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

BackOffice.beforeShowView = function(id)
{
	BackOffice.preloader.show();
	
	jQuery.post("DataTableAction.do", { action: "getEmployeesByRole", ad_role_id: id },
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}  
				
				showEmployees(json);
				
			},"json").fail(function(){
				alert("Failed to request data!");
			},"json").always(function(){
				
				BackOffice.preloader.hide();
				
			});
};

Handlebars.registerHelper("renderDiscountLimit", function(userdiscount) {
	
	if (userdiscount > 0)
	{
		return new Handlebars.SafeString(
				"<div><label class='glyphicons glyphicons-check checkedOption'>Allowed to give discounts</label><div style='padding: 2px 0px 10px;'>Discount limit (%): " + userdiscount + "</div></div>"
			);
	}
	
	return new Handlebars.SafeString(
		"<div><label class='glyphicons glyphicons-unchecked checkedOption' style='text-decoration: line-through'>Allowed to give discounts</label></div>"
	);
	
});

Handlebars.registerHelper("hasAccessToStoreName", function(hasAccess, name) {
	
	if (hasAccess == "Y")
	{
		return new Handlebars.SafeString(
				"<span>" + name + "</span>"
			);
	}
	
	return new Handlebars.SafeString(
			"<span style='text-decoration: line-through'>" + name + "</span>"
	);
	
});

Handlebars.registerHelper("hasAccessToStore", function(hasAccess) {
	
	if (hasAccess == "Y")
	{
		return new Handlebars.SafeString(
				"<span>Yes</span>"
			);
	}
	
	return new Handlebars.SafeString(
			"<span>No</span>"
	);
	
});
	
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	jQuery(document).ready(function(){
		var fields = {
				ad_role_name : {
					tooltip: Translation.translate("tooltip.create.new.role")
				},
				userdiscount : {	
					tooltip:  Translation.translate("tooltip.specify.certain.limit.on.percentage") 
				},
			/*	
				isdiscountallowedontotal : {	
					tooltip: "Select if the employees in this role can give discounts on products or services.",
					position : 'left'
				},
				
				
				isdiscountallowedontotal : {
					tooltip: "Select if employees in this role may discount the total amount.",
					position : 'left'
				},
				isdiscountuptolimitprice :{
					tooltip: "Select if employees in this role may do a price-override.",
					position : 'left'
				},
				allow_change_terminal: { 
					tooltip: "Select if this employee may change registers.",
					position : 'left'
				},
				allow_order_exchange: { 
					tooltip: "Select if this employee will have the ability to perform an exchange.",
					position : 'left'
				},
				allow_order_refund: { 
					tooltip: "Select if this employee will be allowed to perform a refund.",
					position : 'left'
				},
				allow_order_void: { 
					tooltip: "Select if an employee in this role will be allowed to void a sale.",
					position : 'left'
				},
				allow_order_backdate: { 
					tooltip: "Select if an employee in this role will be allowed to backdate a sale.",
					position : 'left'
				},
				allow_order_split: { 
					tooltip: "Select if an employee in this role will be allowed to split a sale after completion",
					position : 'left'
				},
				allow_payment_void : { 
					tooltip: "Select if an employee in this role will be allowed to void any payments",
					position : 'left'
				},
				allow_inventory_void :{ 
					tooltip: "Select if an employee with this role will be allowed to void an inventory adjustment",
					position : 'left'
				},
				allow_stocktransfer_void :{ 
					tooltip: "Select if an employee in this role will be allowed to void an inventory transfer.",
					position : 'left'
				}
				*/
		
			};
		//Include Global Color 
		jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
	});
	
	