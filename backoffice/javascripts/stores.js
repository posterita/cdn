BackOffice.ID_COLUMN_NAME = "ad_org_id";
BackOffice.MODEL_NAME = "MOrg";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveStore";
BackOffice.AD_TABLE_ID = 155;

BackOffice.defaults = {
		"ad_org_id":0,
		"c_country_id":0,
		"c_region_id":0,
		"isactive": "Y",
		"averageworkinghoursperweek" : 0,
		"numberopenweekdays" : 0,
		"salestargetperweek" : 0,
		"metersqval" : 0,
		"averagerentpermonth" : 0,
		"averageelectricitypermonth" : 0,
		"averageotherexpensespermonth" : 0,
		"averagetotalwagespermonth" : 0,
		"averagenumberofemployees" : 0,
		"averagehoursperemployeeperday" : 0,
		"c_country_id": 245,
		"c_region_id": 0,
		
		"receiptformat" : {
 		   
	 		   companyName: "",
	 		   showStoreName: true,
	 		   showStoreAddress: true,
	 		   showPhone: true,
	 		   showTaxId: true,
	 		   additionalFields: null,
	 		   salesReceiptHeader: null,
	 		   showTerminalName: true,
	 		   showBarcode: true,
	 		   footerMessage: "",
	 		   showCustomerFullInfo: false,
 		   
			}
		};

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getStores"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var stores = json["stores"];
				var countries = json["countries"];
				var eventnotifiers = json["eventnotifiers"];
				var paymentprocessors = json["paymentprocessors"];
				var linkedTerminals = json["linkedTerminals"];
				var linkedWarehouses = json["linkedWarehouses"];
				
				//set data source
				//BackOffice.setDataSource(stores);
				
				BackOffice.countryDB = TAFFY(countries);
				
				BackOffice.EVENT_NOTIFIERS_DB = TAFFY(eventnotifiers);
				BackOffice.PAYMENT_PROCESSORS_DB = TAFFY(paymentprocessors);
				BackOffice.TERMINALS_DB = TAFFY(linkedTerminals);
				BackOffice.WAREHOUSES_DB = TAFFY(linkedWarehouses);
				
				//render country list
				DataTable.utils.populateSelect(jQuery("#c_country_id"), countries, "c_country_name", "c_country_id");
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = stores;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(stores);
					BackOffice.renderDataTable();
				}
								
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	  	      { "data": "ad_org_name" },
		      /*{ "data": "phone"},
		      { "data": "fax"},*/
		      { "data": "address1"},
		      { "data": "address2"},
		      { "data": "city"},
		      { "data": "isactive"}
  	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 4
	      }
	    ]
	});
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

BackOffice.afterRenderView = function (id) {
	BackOffice.preloader.show();
	
	var query = {};
	query["ad_org_id"] = {"==":id}
	
	var data = BackOffice.EVENT_NOTIFIERS_DB(query).get();
	BackOffice.renderOtherViews("#view-eventnotifiers", "#show-eventnotifiers", data[0]);
	
	data = BackOffice.PAYMENT_PROCESSORS_DB({"ad_org_id":[0, id]}).get();
	BackOffice.renderOtherViews("#view-pps", "#show-pps", data[0]);
	
	data = BackOffice.TERMINALS_DB({"ad_org_id":[0, id]}).get();
	BackOffice.renderOtherViews("#view-terminals", "#show-terminals", data[0]);
	
	data = BackOffice.WAREHOUSES_DB(query).get();
	BackOffice.renderOtherViews("#view-warehouses", "#show-warehouses", data[0]);
	
	BackOffice.preloader.hide();
};

BackOffice.onShowDetail = function(){
	
	var ctrl = angular.element(document.getElementById("receiptController"));	
	var scope = ctrl.scope();
	
    scope.$apply(function () {
    	scope.config = {};
    });
	
	var availableTemplates = BackOffice.db(function () {
		return (this.receiptformat.length > 0 ) ? true : false;
	}).get();
	
	var select = jQuery("#copy-receipt-format");
	DataTable.utils.populateSelect( select , availableTemplates, "ad_org_name", "receiptformat");
	
	select.on("change", function(){
		
		var format = jQuery(this).val();
		
		if( format.length > 0 ){
			
			 scope.$apply(function () {
			    	scope.config = JSON.parse( format );
			 });
		}
		else
		{
			scope.$apply(function () {
				scope.config = angular.copy( CURRENT_RECEIPT_FORMAT );
			});			
		}
		
	});
	
	var ctrl = angular.element(document.getElementById("receiptController"));	
	var scope = ctrl.scope();
	
    scope.$apply(function () {
    	scope.config = {};
    	scope.model = {};
    });
    
	var model = this.model;
	
	if (model==null){
		return;
	}
	
	var c_country_id = model["c_country_id"];
	renderStatesDropdown(c_country_id);
	
	var c_region_id = model["c_region_id"];	
	jQuery("#c_region_id").val(c_region_id);
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
};

BackOffice.beforeSave = function(model) {
	/**
	 * Server expects numeric value. Sending empty string. 
	 */
	var c_region_id = model["c_region_id"];
	
	if (c_region_id == "" || c_region_id == null){
		model["c_region_id"] = 0;
	}
		
	var c_country_id = model["c_country_id"];
	
	if (c_country_id == ""){
		model["c_country_id"] = 0;
	}
	
	var ctrl = angular.element(document.getElementById("receiptController"));	
	var scope = ctrl.scope();
	
	var receiptformat = scope.config;
	
	model['receiptformat'] = receiptformat;
};

BackOffice.afterSave = function(){
	
	BackOffice.requestDataTableData();
};

BackOffice.showDefaultCountry = function(){
	
	var countryValue = jQuery("#c_country_id:eq(0)").val();
	if(countryValue == "")
	{
		jQuery("#c_country_id").val("100");
		
		var c_country_id = jQuery("#c_country_id").val();
		
		var countryDB = BackOffice.countryDB;
		
		var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
		var regions = country.regions;
		
		DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
	}
	
	var regionValue = jQuery("#c_region_id:eq(0)").val();
	if(regionValue == "")
	{
		jQuery("#c_region_id").val("0");
	}
};

jQuery(document).ready(function()
{	
	var select  = jQuery("#c_country_id");
	select.on("change", function(e){
		var c_country_id = jQuery("#c_country_id").val();
		renderStatesDropdown(c_country_id);
	});
	
	jQuery(function() {
		
		jQuery( "#date-from" ).datepicker({
			format: "mm/dd/yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
		
		jQuery( "#date-to" ).datepicker({
			format: "mm/dd/yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
	});
	
	jQuery("#refresh-sales-history").on('click' , function() {
		jQuery("#sales-history").DataTable().draw();
	});
	
	jQuery("#sales-history-sort-by").on('change' , function() {
		jQuery("#sales-history").DataTable().column(jQuery(this).val()).order('asc').draw();
	});
	
	jQuery("#meter-square-val").keyup(function(){
		var meterSqVal = jQuery("#meter-square-val").val();
		var sqFeetContainer = jQuery("#square-feet-val");
		var value = 0.0929;
		var temp = (meterSqVal/value);
		var val = temp.toFixed(2);
		
		sqFeetContainer.text(val+" sq ft");		
	});
	
	 var fields = {
	    		ad_org_name : {
	    			tooltip : Translation.translate("enter.company.name.wish.show.reports.tooltip")
				},
				taxid : {	
					tooltip : Translation.translate("enter.the.company.tax.id.tooltip" )
				},
				phone : {	
					tooltip : Translation.translate("enter.phone.number.location.if.applicable.tooltip" )
				},
				receiptfootermsg : {	
					tooltip : Translation.translate("enter.message.displayed.receipt.footer.tooltip" )
				},
				email : {
					tooltip : Translation.translate("enter.email.address.this.store")
				},
				address1 :{
					tooltip: Translation.translate("enter.the.address.display.printed.receipts")
				},
				address2 :{
					tooltip: Translation.translate("enter.secondary.address.information.such.tooltip")
				},
				city :{
					tooltip: Translation.translate("enter.city.where.store.located.in.tooltip")
				},
				postal :{
					tooltip: Translation.translate("enter.the.postal.zip.code.store.located.tooltip")
				},
				c_region_id :{
					tooltip: Translation.translate("select.state.if.applicable.tooltip")
				},
				c_country_id :{
					tooltip: Translation.translate("select.country.store.located.in.tooltip")
				}
	    };
	    jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
		    
});
	

function renderStatesDropdown(c_country_id){
	var countryDB = BackOffice.countryDB;
	
	var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
	var regions = country.regions;
	
	DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
}

function showAll(parameter) {
	jQuery("#show-" + parameter).children("div[style]").css("display", "block");
	
	jQuery("#show-more-" + parameter).css("display", "none");
	jQuery("#show-less-" + parameter).css("display", "block");
}

function showLess(parameter) {
	jQuery("#show-" + parameter).children("div[style]").css("display", "none");
	
	jQuery("#show-more-" + parameter).css("display", "block");
	jQuery("#show-less-" + parameter).css("display", "none");
}

Handlebars.registerHelper("addHiddenClass", function(index) {
	
	if (index < 3)
	{
		return;
	}
	
	return new Handlebars.SafeString("style='display: none'");
	
});

Handlebars.registerHelper("showMore", function(last, index, parameter) {
	
	if (last && index > 2)
	{
		return new Handlebars.SafeString("<p class='p-link' id='show-more-" + parameter + "' onclick='javascript:showAll(\"" + parameter + "\")'>Show More</p>" +
		"<p class='p-link' id='show-less-" + parameter + "' onclick='javascript:showLess(\"" + parameter + "\")' style='display: none'>Show Less</p>");
	}
	
	return
});

Handlebars.registerHelper("getNameIfActivated", function(isactive, name) {
	
	if (isactive)
	{
		return new Handlebars.SafeString("<span>" + name + "</span>");
	}
	
	return new Handlebars.SafeString("<span style='text-decoration: line-through'>" + name + "</span>");
});


Handlebars.registerHelper("isActivated", function(isactive) {
	
	if (isactive)
	{
		return new Handlebars.SafeString("<span>"+Translation.translate("active")+"</span>");
	}
	
	return new Handlebars.SafeString("<span>"+Translation.translate("not.active")+"</span>");
});


BackOffice.afterShowDetail = function(){
	
	var model = this.model;
	
	if( model && model['receiptformat'] != ''){
		
		CURRENT_RECEIPT_FORMAT = JSON.parse( model['receiptformat'] );
				
	}
	else
	{
		CURRENT_RECEIPT_FORMAT = DEFAULT_RECEIPT_FORMAT;
	}
		
	var ctrl = angular.element(document.getElementById("receiptController"));	
	var scope = ctrl.scope();
	
    scope.$apply(function () {
    	scope.config = angular.copy( CURRENT_RECEIPT_FORMAT );
    	scope.model = model;
    });
};

jQuery(function(){
	angular.bootstrap(document.getElementById("receiptController"), ['receipt']);
});