var SubMenu = {};

SubMenu.renderSubMenus = function(id) {
	
	if(MenuManager && MenuManager.json != null && SubMenu.MENU_DB_ACCESS == null){
		SubMenu.MENU_DB_ACCESS = TAFFY(MenuManager.json);
	}

	/* set menu active */
	jQuery('#template-menu-container li').removeClass('active');
	jQuery('#' + id).addClass('active');
	
	var query = {};
	query["parent_id"] = {'==':id};
	
	
	var parent = SubMenu.MENU_DB_ACCESS(query).get();
	
	var categories = parent[0]["categories"];
	
	jQuery("#sub-menu-parent-name").text(Translation.translate(parent[0]["parent"]));
	
	var parentContainer = jQuery("#parent-menu-container");
	
	jQuery("#parent-menu-container").empty();
	
	for(var i=0; i<categories.length; i++){
		var categoryMenu = categories[i];
		
		
		var category = jQuery('<div class="sub-menu-wrapper">'
			 + '<div class="sub-menu-category">'
			 + '<p class="">'
			 + Translation.translate(categoryMenu.category)
			 + '</p>'
			 + '</div>'
			 + '<div class="sub-menu-category-menus-wrapper"></div>'
			 + '</div>');
		
		parentContainer.append(category);
		
		/*
		var category = '<div class="sub-menu-wrapper">'
					 + '<div class="sub-menu-category">'
					 + '<p class="">'
					 + Translation.translate(categoryMenu.category)
					 + '</p>'
					 + '</div>'
					 + '<div class="sub-menu-category-menus-wrapper">'
					 */
		
		var menus = categoryMenu.menus;
		var opacity = ' style="opacity:0.6;cursor:not-allowed" ';
					 
		for (var j=0; j<menus.length; j++)
		{
			var menu = menus[j];
			var link = menu.menuLink;
			
			/*
			var div = jQuery('<div class="sub-menu-container" menulink="' + link + '">'
					 + '<i class="glyphicons glyphicons-' + menu.imageLink + '">&nbsp;</i>'
					 + '<div>' + Translation.translate(menu.name) + '</div>'
					 + '</div>');*/
			
			if(menu.hasAccess == false)
			{
				
			}
			
			var html = '<div class="sub-menu-container" menulink="' + link + '" ' + ((menu.hasAccess == false ) ? opacity : '') + '>';			
			
			if (link.indexOf('javascript:') != -1)
			{
				html = '<div class="sub-menu-container" menulink="' + link + '"' + 'onclick="' + link +'" ' + ((menu.hasAccess == false ) ? opacity : '') + '>';		
			}			
			
	        html = html + '<div style="height:60px;">';
	        
	        	        
	        html = html + '<span class="glyphicons2 glyphicons-' + menu.imageLink + '" style="font-size:40px;"></span>';
	        	        	        
	        html += '</div>';
	        
	        if(menu.hasAccess == false)
			{
				html += "<div class='glyphicons2 glyphicons-lock menu-lock'></div>";
			}
	        
	        
	        html += '<div>' + Translation.translate(menu.name) + '</div>';	        
	        
	        html += '</div>';
	          
			var div = jQuery(html);
			
			div.on('click', function(e){
				var link = jQuery(this).attr('menulink');
				
				if (link.indexOf('javascript:') == -1)
				{
					getURL(link);
					//alert(link);
				}
			});
			
			category.append(div);
			
			/*
			category = category
					 + '<div class="sub-menu-container">'
					 + '<i class="glyphicons glyphicons-' + menu.imageLink + '">&nbsp;</i>'
					 + '<a href="' + menu.menuLink  + '">' + Translation.translate(menu.name) + '</a>'
					 + '</div>'*/
		}	
		
		
		resizeContainerHeight();
	}
}