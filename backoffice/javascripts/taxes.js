BackOffice.ID_COLUMN_NAME = "c_tax_id";
BackOffice.MODEL_NAME = "MTax";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveTax";

BackOffice.defaults = {
		"ad_org_id":0,
		"c_tax_id":0,
		"c_tax_rate":0,
		"isactive": "Y",
		"c_taxcategory_id":0,
		"defaultsalestaxid":0,
		"defaultpurchasetaxid":0,
		"u_posterminal_id":0,
		"rate" : 0
		};

BackOffice.isRenderTaxCategory = true;

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getTaxes"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}				
				var taxes = json["data"];
				var orgs = json["orgs"];
				var taxcategories = json["taxcategorydata"];
				var multipletaxes = json["multipletaxes"];
				var storesterminals = json["storesterminals"];
				var multipletaxcategories = json["taxcategories"];
				var taxesratename = json["taxes"];
				
				BackOffice["data"] = taxes;
				
				BackOffice["multipletaxes"] = multipletaxes;
				BackOffice["orgs"] = orgs;
				BackOffice["multipletaxcategories"] = multipletaxcategories;
				BackOffice["taxesratename"] = taxesratename;
				BackOffice["storesterminals"] = storesterminals;			
				
				//render sales taxes list
				populateSalesTaxSelect(jQuery("#so-tax-id"), taxes , "c_tax_name", "c_tax_id");
				
				DataTable.utils.populateSelect(jQuery("#tax-sub-id"), taxes, "c_tax_name", "c_tax_id");
				
				//render purchase taxes list
				populatePurchaseTaxSelect(jQuery("#po-tax-id"), taxes, "c_tax_name", "c_tax_id");
				
				//render multiple tax rate org
				DataTable.utils.populateSelect(jQuery("#ad_org_id"), orgs, "ad_org_name", "ad_org_id");				
				
				//render multiple tax rate terminal
				DataTable.utils.populateSelect(jQuery("#u_posterminal_id"), multipletaxes, "terminal", "u_posterminal_id");
				
				//render multiple tax categories
				DataTable.utils.populateSelect(jQuery("#c_taxcategory_id"), multipletaxcategories, "c_taxcategory_name", "c_taxcategory_id");
				
				//render multiple taxes
				DataTable.utils.populateSelect(jQuery("#c_tax_id"), taxesratename, "c_tax_name", "c_tax_id");
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = taxes;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(taxes);
					BackOffice.renderDataTable();
				}
				
				//addMultipleTaxRate();
				
				if (BackOffice["model"] != undefined)
				{
					if (BackOffice.isRenderTaxCategory)
					{
						displayTaxCategory(BackOffice["model"]["c_taxcategory_id"]);
					}
					else
					{
						jQuery('[name=c_tax_id]').val(0);
					}
					
					displayMultipleTaxes();
					
					if (BackOffice["model"]["subtaxes"] != undefined)
					{
						renderSubtaxes(BackOffice["model"]["subtaxes"]);
					}
				}
				else
				{
					displayTaxCategory(0);
				}
				
				
				
				jQuery('[href="#tab2"]').on('click', function(){
					
					//render sales taxes list
					
					var taxes = BackOffice["data"];	
					
					populateSalesTaxSelect(jQuery("#so-tax-id"), taxes , "c_tax_name", "c_tax_id");
					
					//render purchase taxes list
					populatePurchaseTaxSelect(jQuery("#po-tax-id"), taxes, "c_tax_name", "c_tax_id");
					
					
					var isMultipleTax = false;
					
					var setSales = false;
					var setPurchase = false;
					
					var defaultSalesTaxId = 0;
					var defaultPurchaseTaxId = 0;
					
					for(var j=0; j<taxes.length; j++)
					{
						var c_tax_id = taxes[j].c_tax_id;
						
						if((taxes[j].isdefault == 'Y')&&(taxes[j].c_tax_sopotype == 'B')){
							
							if (!setSales)
							{
								defaultSalesTaxId = c_tax_id;
							}
							
							if (!setPurchase)
							{
								defaultPurchaseTaxId = c_tax_id;
							}
						}
						else
						if((taxes[j].isdefault == 'Y')&&(taxes[j].c_tax_sopotype == 'S')){
							setSales = true;
							defaultSalesTaxId = c_tax_id;
						}
						else
							if((taxes[j].isdefault == 'Y')&&(taxes[j].c_tax_sopotype == 'P')){
								setPurchase = true;
								defaultPurchaseTaxId = c_tax_id;
							}
					}
					
					for(var i=0; i<multipletaxes.length; i++)
					{
						var salesTaxId = multipletaxes[i].so_c_tax_id;
						var purchaseTaxId = multipletaxes[i].po_c_tax_id;
						
						if ((salesTaxId != defaultSalesTaxId) || (purchaseTaxId != defaultPurchaseTaxId))
						{
							isMultipleTax = true;
						}
						
					}
					
					if (isMultipleTax)
					{
						jQuery("#multiple-tax-rate-container").show();
						jQuery("#is-multiple-tax-rate").attr('checked', true);
						
						jQuery("#single-tax-rate-container").hide();
						jQuery("#is-single-tax-rate").attr('checked', false);
					}
					else
					{
						jQuery("#single-tax-rate-container").show();
						jQuery("#is-single-tax-rate").attr('checked', true);
						
						jQuery("#multiple-tax-rate-container").hide();
						jQuery("#is-multiple-tax-rate").attr('checked', false);
					}
					
					var sotaxid = jQuery("#so-tax-id").val();					
					salesTaxRate(sotaxid);
					
					var potaxid = jQuery("#po-tax-id").val();					
					purchasesTaxRate(potaxid);
				});
				
				jQuery('[href="#tab3"]').on('click', function(){
					displayTaxNameForSubtax();
				});
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}).done(function(){
				BackOffice.isRenderTaxCategory = true;
			}); 		
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
		  	  { "data": "org_name"},
		  	 /* { "data": "c_tax_id"},*/
		      { "data": "c_tax_name"}, 
		      { "data": "c_tax_rate"},
		      { "data": "c_taxcategory_name"},
		      { "data": "isactive"}
  	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.edit(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 3
	      },
	      {
	        "render": function (data, type, row) {
	        	 var value = row["org_name"];
	          
				 if (value == "*")
				 {
					  value = "All Stores";
				 }
				 
				 return value;
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 4
	      }
	      /*,	      
	     
 	      { "visible": false, "targets": 0 }*/
 	      /*{ "visible": false, "targets": 1 }*/
 	    ],
 	    
 	   "drawCallback": function ( settings ) {
           var api = this.api();
           var rows = api.rows( {page:'current'} ).nodes();
           var last=null;

           api.column(0, {page:'current'} ).data().each( function ( group, i ) {
        	   if (group == "*")
        	   {
        		   group = "All Stores";
        	   }
        	   if ( last !== group ) {
            	   jQuery(rows).eq( i ).before(
                       '<tr class="group-store"><td colspan="5">Store: '+group+'</td></tr>'
                   );

                   last = group;
               }
           } );           
       }	    
	});
};

BackOffice.beforeSave = function(model){	
	/* add changes made to subtaxes */
	var subtaxes = [];
	
	var rows = jQuery("#sub-taxes-table-tbody tr");
	for(var i=0; i<rows.length; i++){
		var row = rows[i];
		
		var inputs = jQuery(row).find("input");
		
		var subtax = {};
		
		for(var j=0; j<inputs.length; j++){
			
			var input = jQuery(inputs[j]);
			var name = input.attr("name");
			var value = input.attr("value");
			
			subtax[name] = value;
			subtax["c_tax_id"] = jQuery('#tax-rate-id').val();
		}		
		subtaxes.push(subtax);
	}	
	model.subtaxes = subtaxes;
	
	/*tax*/
	var taxes = [];
	var rows = jQuery("#tax-category-name-table-tbody tr");
	for(var i=0; i<rows.length; i++){
		var row = rows[i];
		
		var inputs = jQuery(row).find("input");
		
		var tax = {};
		
		for(var j=0; j<inputs.length; j++){
			
			var input = jQuery(inputs[j]);
			var name = input.attr("name");
			var value = input.attr("value");
			
			tax[name] = value;
		}
		taxes.push(tax);
	}
	model.taxes = taxes;	
	
	var defaultsalestaxid = model["defaultsalestaxid"];
		
	if (defaultsalestaxid == ""){
		model["defaultsalestaxid"] = 0;		
	}
		
	var defaultpurchasetaxid = model["defaultpurchasetaxid"];
			
	if (defaultpurchasetaxid == ""){
		model["defaultpurchasetaxid"] = 0;		
	}	
	
	var u_posterminal_id = model["u_posterminal_id"];
	
	if (u_posterminal_id == null){
		model["u_posterminal_id"] = 0;		
	}	
	
	var c_tax_id = model["c_tax_id"];
	
	if (c_tax_id == ""){
		model["c_tax_id"] = 0;		
	}	
	
	var c_taxcategory_id = model["c_taxcategory_id"];
	
	if (c_taxcategory_id == ""){
		model["c_taxcategory_id"] = 0;		
	}
	
	/*var rows = jQuery('.tax-details');
	var taxArray = new Array();
	
	for(var j=0; j<rows.length; j++)
	{
		var inputs = jQuery(rows[j]).find('input');
		var tax = {};
		for(var i=0; i<inputs.length; i++)
		{
			var input = jQuery(inputs[i]);
			tax[input.attr('name')] = input.val();
		}		
		taxArray.push(tax);
	}	
	model["taxes"] = taxArray;*/
	
	setMultipleTaxes(model);
	
	jQuery('.multiple-tax select').each(function(event){
	if(jQuery(this).val()=='')
		{
			alert("Please fill the fields");
			return;
		}
	});	
	
	/*Multiple taxes to delete*/
	
	model["deletedMultipleTaxes"] = BackOffice["deletedMultipleTaxes"];
	
	if (jQuery("#is-single-tax-rate").is(':checked'))
	{
		model["ismultipletax"]  = false;
	}
	else
	{
		model["ismultipletax"]  = true;
	}
};

function setMultipleTaxes(model)
{
	if (model==null){
		return;
	};
	
	var multipleTaxArray = new Array();
	
	var additionalRows = jQuery('.multiple-tax');
	
	if (additionalRows.length == 0)
	{
		return;
	}
	
	var rows = jQuery('.multiple-tax');	
	
	for(var j=0; j<rows.length; j++)
	{		
		var inputs = jQuery(rows[j]).find('select');
		var multipleTax = {};
		for(var i=0; i<inputs.length; i++)
		{
			var input = jQuery(inputs[i]);
			
			var value = input.val();
			
			if (value == '')
			{
				value = 0;
			}
			multipleTax[input.attr('name')] = value;			
		}				
		multipleTaxArray.push(multipleTax);		
	}		
	model["multipletaxes"] = multipleTaxArray;			
}

BackOffice.afterSave = function(model){
	/* clear subtaxes to delete */
	//jQuery("#deletedsubtaxes").val("");
	jQuery("#deletedTaxes").val("");
	
	jQuery("#deletedMultipleTaxes").val("");
	
	BackOffice["deletedMultipleTaxes"] = new Array();
	
	BackOffice.requestDataTableData();
	
};

jQuery(document).ready(function(){
	var fields = {
			defaultsalestaxid:{
				tooltip:Translation.translate("select.from.your.created.tax.rates.tooltip")
			},
			defaultpurchasetaxid :{
				tooltip: Translation.translate("select.from.your.created.tax.rates.tooltip")
			}			
	};
	
	var single_tax_rate_radio = jQuery("#is-single-tax-rate");
	single_tax_rate_radio.on("change", function(e){
		var container = jQuery("#single-tax-rate-container");	
		
		if(single_tax_rate_radio.is(':checked')){
			
			container.show();
			jQuery("#multiple-tax-rate-container").hide();
			jQuery("#is-multiple-tax-rate").attr('checked', false);	
		}
		else
		{
			container.hide();
		}
	});
	
	var multiple_tax_rate_radio = jQuery("#is-multiple-tax-rate");
	multiple_tax_rate_radio.on("change", function(e){
		var container = jQuery("#multiple-tax-rate-container");	
		
		if(multiple_tax_rate_radio.is(':checked')){
			container.show();
			jQuery("#single-tax-rate-container").hide();
			
			jQuery("#is-single-tax-rate").attr('checked', false);
		}
		else
		{
			container.hide();
		}
	});
	
	
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});

	function renderTerminal(){
		jQuery("select[name='ad_org_id']").on("change",function(e){
			var ad_org_id = jQuery("select[name='ad_org_id']").val();
			terminalsForOrg(ad_org_id);
		});
	}
	
	function renderTaxName(){
		jQuery("select[name='c_assignment_taxcategory_id']").on("change",function(e){
			var c_taxcategory_id = jQuery("select[name='c_taxcategory_id']").val();
			taxForTaxCategory(c_taxcategory_id);
		});
	}
	
	function renderTax(){
		jQuery("select[name='c_tax_id']").on("change",function(e){
			var c_tax_id = jQuery(this).val();
			displayTaxRate(c_tax_id);
		});
	}
	
	function validateRow(){
		var selects = jQuery(".multiple-tax select");
		if(selects.val()==''){
			alert("please fill choose");
			return false;
		}
	}	
		
	jQuery("select[name='c_taxcategory_id']").on("change",function(e){
		var c_taxcategory_id = jQuery(this).val();
		displayTax(c_taxcategory_id);
	});
	
	jQuery("#add-sub-tax-button").on("click", function(e){
		BackOffice.clearMessage();		
		addSubtax(null);
	});
	
	jQuery("#add-tax-rate-button").on("click", function(e){
		BackOffice.clearMessage();
		var c_taxcategory_id = jQuery("input[name='c_taxcategory_id']").val();
		jQuery("#c_taxcategory_id").val(c_taxcategory_id);		
		addTaxCategory(c_taxcategory_id);
	});
	
	jQuery("#add-tax-category-rate-button").on("click", function(e){
		BackOffice.clearMessage();
		addMultipleTaxRate();
		renderTax();
		renderTerminal();
		renderTaxName();
		//validateRow();
		//jQuery("#add-tax-category-rate-button").hide();
	});	
		
/*	jQuery("#add-tax-category-button").on("click", function(e){
		BackOffice.clearMessage();
		
		var c_taxcategory_id = 0;
		jQuery("#c_taxcategory_id").val(c_taxcategory_id);		
		addTaxCategory(c_taxcategory_id);
		
		jQuery("[name=c_tax_name]").val('');
		jQuery("[name=c_tax_rate]").val('');
		jQuery("[name=c_tax_id]").val(0);
		jQuery("[name=c_taxcategory_name]").val('');
		jQuery("#tax-category-name-table-tbody tr:gt(0)").remove();		
		
	});*/	
	
	jQuery('#example button').click(function(){
		var ids = BackOffice.db({isSelected:"Y"}).select("c_tax_id");		
		alert(ids);
	});	
	
	jQuery("#so-tax-id").on("change",function(e){
		var so_tax_id = jQuery(this).val();
		salesTaxRate(so_tax_id);		
	});
	
	jQuery("#po-tax-id").on("change",function(e){
		var po_tax_id = jQuery(this).val();
		purchasesTaxRate(po_tax_id);		
	});
	
	jQuery("#tax-rate-id").on("change",function(e){
		var tax_rate_id = jQuery(this).val();
		showTaxRateForSubtax(tax_rate_id);	
		
		var db = BackOffice.db;
		var query = {};
		query[BackOffice.ID_COLUMN_NAME] = {'==':tax_rate_id};
		
		var records = db(query).get();
		renderSubtaxes(records[0].subtaxes);
	});
});

/*function clickTaxCategoryButton(){
	BackOffice.clearMessage();
	
	if(confirm('If you have not saved your tax please click on save!')){
		
		
		BackOffice.save();
		
		var tbody = jQuery("#tax-category-name-table-tbody");
		
		jQuery(tbody).html('');
		
		var c_taxcategory_id = 0;
		jQuery("#c_taxcategory_id").val(c_taxcategory_id);				
		addTaxCategory(c_taxcategory_id);
		
		jQuery("[name=c_tax_name]").val('');
		jQuery("[name=c_tax_rate]").val('');
		jQuery("[name=c_tax_id]").val(0);
		jQuery("[name=c_taxcategory_name]").val('');
		jQuery("#tax-category-name-table-tbody tr:gt(0)").remove();			
		
	}
	else
	{
		var c_taxcategory_id = 0;
		jQuery("#c_taxcategory_id").val(c_taxcategory_id);		
		addTaxCategory(c_taxcategory_id);
		
		jQuery("[name=c_tax_name]").val('');
		jQuery("[name=c_tax_rate]").val('');
		jQuery("[name=c_tax_id]").val(0);
		jQuery("[name=c_taxcategory_name]").val('');
		jQuery("#tax-category-name-table-tbody tr:gt(0)").remove();	
	}
}*/
	
	
function clickTaxCategoryButton()
{
	jQuery("#dialog1").dialog({
		modal: true,
		title: Translation.translate("new.tax.category"),
		height: '100',
		width: '300',
		buttons:[{
			text: 'Yes',
			"class" : 'btn btn-primary btn-save',
			click: function(){
				BackOffice.isRenderTaxCategory = false;
				BackOffice.save();
	    		
	    		var tbody = jQuery("#tax-category-name-table-tbody");
	    		
	    		jQuery(tbody).html('');
	    		
	    		var c_taxcategory_id = 0;
	    		jQuery("#c_taxcategory_id").val(c_taxcategory_id);				
	    		addTaxCategory(c_taxcategory_id);
	    		
	    		jQuery("[name=c_tax_name]").val('');
	    		jQuery("[name=c_tax_rate]").val('');
	    		jQuery("[name=c_tax_id]").val(0);
	    		jQuery("[name=c_taxcategory_name]").val('');
	    		jQuery("#tax-category-name-table-tbody tr:gt(0)").remove(); 
	    		jQuery(this).dialog("close");
			}
		}/*,		
		{
			text : 'Proceed',
			"class" : 'btn btn-default btn-default1',
			click : function(){
				
				BackOffice.isRenderTaxCategory = true;
				var c_taxcategory_id = 0;
	    		jQuery("#c_taxcategory_id").val(c_taxcategory_id);		
	    		addTaxCategory(c_taxcategory_id);
	    		
	    		jQuery("[name=c_tax_name]").val('');
	    		jQuery("[name=c_tax_rate]").val('');
	    		jQuery("[name=c_tax_id]").val(0);
	    		jQuery("[name=c_taxcategory_name]").val('');
	    		jQuery("#tax-category-name-table-tbody tr:gt(0)").remove();	
	    		
	        	jQuery(this).dialog("close");
			}
		}*/
		
		]	
	});
	
	jQuery(".btn-save").html(Translation.translate("save"));
	jQuery(".btn-default1").html(Translation.translate("proceed"));
	jQuery("#dialog1").css("height","100px");
	jQuery("#dialog1").parent().position({my:'center', of:'center', collision:'fit'});
}
	

function taxName()
{
	jQuery("#display-tax-name").html("");
	
	var tax_name = BackOffice.model.c_tax_name;
	jQuery("#display-tax-name").html(tax_name);
}

function salesTaxRate(so_tax_id){
	
	var taxes = BackOffice["data"];	
	var container = jQuery("#so-tax-rate");
	
	for(i=0;i<taxes.length;i++){
		var tax = taxes[i];
		var tax_id = taxes[i].c_tax_id;
		if(tax_id == so_tax_id){
			var taxRate = tax.c_tax_rate
			container.html(taxRate);
		}
	}	
}

function purchasesTaxRate(po_tax_id){	
	var taxes = BackOffice["data"];	
	var container = jQuery("#po-tax-rate");
	
	for(i=0;i<taxes.length;i++){
		var tax = taxes[i];
		var tax_id = taxes[i].c_tax_id;
		
		if(tax_id == po_tax_id){
			var taxRate = tax.c_tax_rate
			container.html(taxRate);
		}
	}	
}

function showTaxRateForSubtax(tax_rate_id){	
	var taxes = BackOffice["data"];	
	var container = jQuery("#tax-rate-container");
	container.html('');	
	
	for(i=0;i<taxes.length;i++){
		var tax = taxes[i];
		var tax_id = taxes[i].c_tax_id;
		
		if(tax_id == tax_rate_id){
			var taxRate = tax.c_tax_rate
			container.html(taxRate);
		}
	}	
}

function displayTaxRate(c_tax_id){
	var taxes = BackOffice["data"];	
	var containerRate = jQuery("#rate");
	var containerSopotype = jQuery("#sopotype");
	var containerIsdefault= jQuery("#isdefault");	
	
	for(i=0;i<taxes.length;i++){
		var tax = taxes[i];
		var tax_id = taxes[i].c_tax_id;
		
		if(tax_id == c_tax_id){
			var taxRate = tax.c_tax_rate
			containerRate.html(taxRate);
			
			var sopotype = tax.c_tax_sopotype;
			containerSopotype.html(sopotype);
			
			var isdefault = tax.isdefault;
			containerIsdefault.html(isdefault);			
		}
	}	
}

/*function displayTax(c_taxcategory_id){
	var taxes = BackOffice["data"];	
	var containerTax = jQuery("#");
		
	for(i=0;i<taxes.length;i++){
		var tax = taxes[i];
		var tax_id = taxes[i].c_tax_id;
		
		if(tax_id == c_tax_id){
			var taxRate = tax.c_tax_rate
			containerRate.html(taxRate);			
		}
	}	
}*/



function renderSubtaxes(subtaxes){
	 /*clear subtaxes to delete */
	jQuery("#deletedsubtaxes").val("");
	
	 /*draw subtax table rows */
	jQuery("#sub-taxes-table-tbody tr").remove();
	
	for(var i=0; i<subtaxes.length; i++){
		var subtax = subtaxes[i];
		addSubtax(subtax);
	}
	
	//taxName();
}

function addSubtax(subtax){
	 /*draw new subtax row*/	
	if(subtax == null){
		subtax = {
			"c_subtax_id" : 0,
			"c_subtax_name" : "",
			"c_subtax_rate" : "0.00"
		};
	}	
	
	/* draw subtax table */
	var model = BackOffice.model;		
	//var c_tax_id = model["c_tax_id"];	
	var c_tax_id = jQuery("#tax-rate-id").val();
	
	var tbody = jQuery("#sub-taxes-table-tbody");
	
	var row = "<tr>" +
		"<td class='tax-name'>" +
			"<input type='hidden' id='c_subtax_id' name='c_subtax_id' value='" + subtax.c_subtax_id + "'/>" +
			"<input type='hidden' id='c_tax_id' name='c_tax_id' value='" + c_tax_id + "'/>" +
			"<input name='c_subtax_name'  type='text' value='" + subtax.c_subtax_name + "' data-validation='required' data-validation-error-msg='Name is required' class='required'/>" +
		"</td>" +
		"<td class='tax-rate'><input name='c_subtax_rate' type='text' value='" + subtax.c_subtax_rate + "'  data-validation='number' data-validation-error-msg='Rate must be a number' data-validation-allowing='float'></td>" +
		"<td><button type='button' class='btn btn-default' onclick='deleteSubtax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+
		"</tr>";
	
	tbody.append(row);
}

BackOffice.onShowDetail = function(){
	var model = this.model;
	
	if (model==null){
		return;
	};
		
	/*if(model["issalestax"]=="Y" || model["isdefault"]=="Y")
	{
		jQuery("#single-tax-rate-container").show();
		jQuery("#is-single-tax-rate").attr('checked', true);
	}*/
	
	jQuery("#categories li a").on("click",function(){
		if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			jQuery(".ui-tabs-selected")
			.children("i")
			.removeClass("fa arrow-icons fa-chevron-down")
			.addClass("fa arrow-icons fa-chevron-right");
		}
		if((jQuery("#categories li").not(".ui-tabs-selected")))
		{
			jQuery("#categories li").not(".ui-tabs-selected")
			.children("i")
			.removeClass("fa arrow-icons fa-chevron-right")
			.addClass("fa arrow-icons fa-chevron-down");
		}
	});	
	
	/* draw subtax table */
	var subtaxes = [];	
	var model = this.model;	
	
	if(model != null){
		subtaxes = model["subtaxes"];
	}	
	
	renderSubtaxes(subtaxes);
	
	jQuery("#multiple-tax-rate-tbody input").prop("disabled","disabled");
	
	var ad_org_id = model["ad_org_id"];
	terminalsForOrg(ad_org_id);
		
	var c_taxcategory_id = model.c_taxcategory_id;
	displayTaxCategory(c_taxcategory_id);	
	//taxForTaxCategory(c_taxcategory_id);
	
	displayTaxName(c_taxcategory_id);	
	displayTax(c_taxcategory_id);
	displayMultipleTaxes();
	displayTaxNameForSubtax();
	
	var taxes = BackOffice["data"];
	
	
	
	/*for(i=0;i<taxes.length;i++){
		
		if((taxes[i].isdefault == 'Y')&&(taxes[i].c_tax_sopotype == 'S')){
			var c_tax_id = taxes[i].c_tax_id;
			jQuery("#so-tax-id").val(c_tax_id);
		}
		
		if((taxes[i].isdefault == 'Y')&&(taxes[i].c_tax_sopotype == 'P')){
			var c_tax_id = taxes[i].c_tax_id;
			jQuery("#po-tax-id").val(c_tax_id);
		}
		
		if((taxes[i].isdefault == 'Y')&&(taxes[i].c_tax_sopotype == 'B')){
			var c_tax_id = taxes[i].c_tax_id;
			jQuery("#po-tax-id").val(c_tax_id);
			jQuery("#so-tax-id").val(c_tax_id);
		}
	}	*/
}

BackOffice.onCreate = function(){
	var model = this.model;
	
	if (model==null){
		return;
	};
	
	jQuery("#categories li a").on("click",function(){
		if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			jQuery(".ui-tabs-selected")
			.children("i")
			.removeClass("fa arrow-icons fa-chevron-down")
			.addClass("fa arrow-icons fa-chevron-right");
		}
		if((jQuery("#categories li").not(".ui-tabs-selected")))
		{
			jQuery("#categories li").not(".ui-tabs-selected")
			.children("i")
			.removeClass("fa arrow-icons fa-chevron-right")
			.addClass("fa arrow-icons fa-chevron-down");
		}
	});
	
	displayMultipleTaxes();
	
	/*var c_taxcategory_id = 0;
	addTaxCategory(c_taxcategory_id);*/
	
	displayTaxCategory(0);
	
	jQuery("select[name='ad_org_id']").on("change", function(e){
		populateOrgs(jQuery("select[name='ad_org_id']").val());
	});	
}

BackOffice.afterCancel = function(){	
	var row = jQuery("#tax-category-name-table-tbody tr");
	row.remove();
}

function resetTaxes()
{
	jQuery('#tax-category-name-container :input').each(function(){
		jQuery(this).val('');
	});
}

function displayTaxCategory(c_taxcategory_id){
	
	var taxes = BackOffice.datasource;
	var tbody = jQuery("#tax-category-name-table-tbody");
	var tax = null;	
	
		jQuery(tbody).html('');
		jQuery("[name='c_tax_id']").val(0);
		
		if (c_taxcategory_id == 0) {
			
			if(tax == null){
				tax = {
						"c_taxcategory_id" : 0,
						"c_taxcategory_name" : "",
						"c_tax_id" : 0,
						"c_tax_name" : "",
						"c_tax_rate" : 0,
						"ad_org_id" : 0,
						"c_tax_sopotype" : "B"
				};
			}
			
			var row = "<tr class='tax-details'>"+
			"<td style='padding:5px;'>"+
				"<input type='hidden' id='c_tax_sopotype' name='c_tax_sopotype' value='" + tax.c_tax_sopotype + "'/>" +
				"<input type='hidden' id='ad_org_id' name='ad_org_id' value='" + tax.ad_org_id + "'/>" +
				/*"<input type='hidden' id='c_taxcategory_id' name='c_taxcategory_id' value='" + tax.c_taxcategory_id + "'/>" +*/
				"<input type='hidden' id='c_tax_id' name='c_tax_id' value='" + tax.c_tax_id + "'/>" +
				"<input type='hidden' id='c_taxcategory_id' name='c_taxcategory_id' value='" + tax.c_taxcategory_id + "'/>" +
				"<input name='c_tax_name'  type='text' placeholder='Tax Name' value='" + tax.c_tax_name + "' data-validation='required' data-validation-error-msg='Name is required' class='required'/>" +
			"</td>"+
			"<td class='tax-rate' style='padding:5px;'><input name='c_tax_rate' type='text' placeholder='Tax Rate' value='" + tax.c_tax_rate + "'  data-validation='number' data-validation-error-msg='Rate must be a number' data-validation-allowing='float'></td>" +
			"<td style='padding:5px;'><button type='button' class='btn btn-default' onclick='deleteTax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+
			"</tr>";	
			tbody.append(row);			
		}		
		else{
			
			for(i=0;i<taxes.length;i++){
				var tax = taxes[i];
				var taxcategory_id = taxes[i].c_taxcategory_id;
				
				if(taxcategory_id == c_taxcategory_id)
				{					
					var row = "<tr class='tax-details'>"+
					"<td style='padding:5px;'>"+
						"<input type='hidden' id='c_tax_sopotype' name='c_tax_sopotype' value='" + tax.c_tax_sopotype + "'/>" +
						"<input type='hidden' id='ad_org_id' name='ad_org_id' value='" + tax.ad_org_id + "'/>" +
						"<input type='hidden' id='c_taxcategory_id' name='c_taxcategory_id' value='" + tax.c_taxcategory_id + "'/>" +
						"<input type='hidden' id='c_tax_id' name='c_tax_id' value='" + tax.c_tax_id + "'/>" +
						"<input name='c_tax_name'  type='text' placeholder='Tax Name' value='" + tax.c_tax_name + "' data-validation='required' data-validation-error-msg='Name is required' class='required'/>" +
					"</td>"+
					"<td style='padding:5px;'><input name='c_tax_rate' placeholder='Tax Rate' type='text' value='" + tax.c_tax_rate + "'  data-validation='number' data-validation-error-msg='Rate must be a number' data-validation-allowing='float'></td>" +
					"<td style='padding:5px;'><button type='button' class='btn btn-default' onclick='deleteTax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+
					"</tr>";
					
					tbody.append(row);					
				}
			}			
		}				
	}

function addTaxCategory(c_taxcategory_id){
	
	var taxes = BackOffice["data"];
	var tbody = jQuery("#tax-category-name-table-tbody");
	var tax = null;	
		
	if (c_taxcategory_id == 0) {
		//jQuery(tbody).html('');
		jQuery("[name='c_taxcategory_id']").val(0);
		
		
		if(tax == null){
			tax = {
					"c_taxcategory_id" : 0,
					"c_taxcategory_name" : "",
					"c_tax_id" : 0,
					"c_tax_name" : "",
					"c_tax_rate" : 0,
					"ad_org_id" : 0,
					"c_tax_sopotype" : "B"
			};
		}
	
	var row = "<tr class='tax-details'>"+
	"<td style='padding:5px;'>"+
		"<input type='hidden' id='c_tax_sopotype' name='c_tax_sopotype' value='" + tax.c_tax_sopotype + "'/>" +
		"<input type='hidden' id='ad_org_id' name='ad_org_id' value='"+ tax.ad_org_id +"'/>" +
		//"<input type='hidden' id='c_taxcategory_id' name='c_taxcategory_id' value='" + jQuery('#c_taxcategory_id').val() + "'/>" +
		"<input type='hidden' id='c_tax_id' name='c_tax_id' value='"+ tax.c_tax_id +"'/>" +
		"<input type='hidden' id='c_taxcategory_id' name='c_taxcategory_id' value='"+ tax.c_taxcategory_id +"'/>" +
		"<input name='c_tax_name'  type='text' placeholder='Tax Name' value='"+ tax.c_tax_name +"' data-validation='required' data-validation-error-msg='Name is required' class='required'/>" +
	"</td>"+
	"<td style='padding:5px;'><input name='c_tax_rate' placeholder='Tax Rate' type='text' value='"+ tax.c_tax_rate+ "'  data-validation='required' data-validation-error-msg='Rate must be a number' data-validation-allowing='float'></td>" +
	"<td style='padding:5px;'><button type='button' class='btn btn-default' onclick='deleteTax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+
	"</tr>";
	
	tbody.append(row);	
	}
	else
	{
		var row = "<tr class='tax-details'>"+
		"<td style='padding:5px;'>"+
			"<input type='hidden' id='c_tax_sopotype' name='c_tax_sopotype' value='B'/>" +
			"<input type='hidden' id='ad_org_id' name='ad_org_id' value='0'/>" +
			//"<input type='hidden' id='c_taxcategory_id' name='c_taxcategory_id' value='" + jQuery('#c_taxcategory_id').val() + "'/>" +
			"<input type='hidden' id='c_tax_id' name='c_tax_id' value='0'/>" +
			"<input type='hidden' id='c_taxcategory_id' name='c_taxcategory_id' value='"+c_taxcategory_id+"'/>" +
			"<input name='c_tax_name'  type='text' placeholder='Tax Name' value='' data-validation='required' data-validation-error-msg='Name is required' class='required'/>" +
		"</td>"+
		"<td style='padding:5px;'><input name='c_tax_rate' placeholder='Tax Rate' type='text' value=''  data-validation='required' data-validation-error-msg='Rate must be a number' data-validation-allowing='float'></td>" +
		"<td style='padding:5px;'><button type='button' class='btn btn-default' onclick='deleteTax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+
		"</tr>";	
		
		tbody.append(row);
	}	
	
	
}

function displayMultipleTaxes(){
	var multipleTaxes = BackOffice["multipletaxes"];
	
	var tbody = jQuery("#multiple-tax-rate-tbody");
	
	jQuery(tbody).html('');
	
	for(i=0; i< multipleTaxes.length; i++){
		var multipleTaxe = multipleTaxes[i];
		
		var sotype = multipleTaxe.so_taxtype;
		var potype = multipleTaxe.po_taxtype;
		
		if (sotype == 'B')
		{
			sotype = 'Both';
		}
		
		if (potype == 'B')
		{
			potype = 'Both';
		}
		
		if (sotype == 'S')
		{
			sotype = 'Sales';
		}
		
		if (potype == 'P')
		{
			potype = 'Purchase';
		}
		
		if(multipleTaxe.so_c_tax_id != 0)
		{
			var row = "<tr>"+
			"<td>"+				
				"<label name='' data-validation='required'>" + multipleTaxe.store +"</label>" +					
			"</td>"+
			"<td>"+		
			"<input type='hidden' id='u_posterminal_id' name='u_posterminal_id' value='"+multipleTaxe.u_posterminal_id+"'/>" +
				"<label name='' data-validation='required'/>" + multipleTaxe.terminal +"</label>" +					
			"</td>"+
			"<td>"+				
			"<label name='' data-validation='required' class='required'/>" + multipleTaxe.so_taxcategory_name +"</label>"+					
			"</td>"+
			"<td>"+	
			"<input type='hidden' id='so_c_tax_id' name='so_c_tax_id' value='" + multipleTaxe.so_c_tax_id + "'/>" +
			"<label name=''  data-validation='required' class='required'/>" + multipleTaxe.so_c_tax_name + "</label>"+						
			"</td>"+
			"<td>"+				
			"<label name=''  data-validation='required' class='required'/>" + multipleTaxe.so_taxrate + "</label>"+			
			"</td>"+
			"<td>"+				
			"<label name=''   data-validation='required' id='tax-type' class='required'/>" + sotype + "</label>"+				
			"</td>"+
			"<td>"+				
			"<label name=''  data-validation='required' class='required'/>" + multipleTaxe.so_default + "</label>"+						
			"</td>"+
			"<td style='padding:5px;'><button type='button' id='delete-multiple-tax-button' class='btn btn-default' onclick='deleteMultipleTax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+
			"</tr>";
			
			tbody.append(row);
		}
		
		if(multipleTaxe.po_c_tax_id != 0)
		{
			var row = "<tr>"+
			"<td>"+						
				"<label name=''  data-validation='required'>" + multipleTaxe.store +"</label>"+						
			"</td>"+
			"<td>"+				
			"<input type='hidden' id='u_posterminal_id' name='u_posterminal_id' value='"+multipleTaxe.u_posterminal_id+"'/>" +
			"<label name=''  data-validation='required'/>"+ multipleTaxe.terminal +"</label>"+			
			"</td>"+
			"<td>"+				
			"<label name='' data-validation='required' class='required'/>" + multipleTaxe.po_taxcategory_name +"</label>"+				
			"</td>"+
			"<td>"+			
			"<input type='hidden' id='po_c_tax_id' name='po_c_tax_id' value='" + multipleTaxe.po_c_tax_id + "'/>" +
			"<label name=''  data-validation='required' class='required'/>" + multipleTaxe.po_c_tax_name +"</label>"+					
			"</td>"+
			"<td>"+				
			"<label name=''  data-validation='required' class='required'/>" + multipleTaxe.po_taxrate +"</label>"+				
			"</td>"+
			"<td>"+				
			"<label name=''  data-validation='required' id='tax-type' class='required'/>" + potype +"</label>"+		
			"</td>"+
			"<td>"+				
			"<label name=''  data-validation='required' class='required'/>" + multipleTaxe.po_default +"</label>"+				
			"</td>"+
			"<td style='padding:5px;'><button type='button' id='delete-multiple-tax-button' class='btn btn-default' onclick='deleteMultipleTax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+						
		"</tr>";		
			tbody.append(row);	
		}
	}	
}

var counter = 0;

function addMultipleTaxRate(){	
	
	counter ++;
	
	var tbody = jQuery("#multiple-tax-rate-tbody");		
	
	var row = "<tr class='multiple-tax'>"+
	"<td>"+		
		"<select id='ad_org_id"+counter+"' name='ad_org_id' class='form-control'>" +					
	"</td>"+
	"<td>"+			
		"<input type='hidden' id='u_posterminal_id' name='u_posterminal_id' value='0'/>" +
		"<select id='u_posterminal_id"+counter+"' name='u_posterminal_id' class='form-control'><option></option></select>" +					
	"</td>"+
	"<td>"+				
		"<select id='c_taxcategory_id"+counter+"' name='c_assignment_taxcategory_id' class='form-control'>" +					
	"</td>"+
	"<td>"+				
		"<select id='c_tax_id"+counter+"' name='c_tax_id' class='form-control'><option></option></select>" +					
	"</td>"+
	"<td>"+				
		"<div name='rate' id='rate"+counter+"'></div>" +					
	"</td>"+
	"<td>"+				
		"<select id='sopotype"+counter+"' name='sopotype' style='width:50px;' class='form-control'><option value='B'>Both</option><option value='P'>Purchase</option><option value='S'>Sales</option></select>" +					
	"</td>"+
	"<td>"+				
		"<select id='isdefault"+counter+"' name='isdefault' style='width:50px;' class='form-control'><option>Y</option><option>N</option></select>" +					
	"</td>"+
	"<td style='padding:5px;'><button type='button' id='delete-multiple-tax-button' class='btn btn-default' onclick='deleteMultipleTax(this);'><span class='glyphicon glyphicon-trash'></span></button></td>"+
	"</tr>";
	
	tbody.append(row);	

	DataTable.utils.populateSelect(jQuery("#ad_org_id"+counter), BackOffice.orgs, "ad_org_name", "ad_org_id");
	
	DataTable.utils.populateSelect(jQuery("#u_posterminal_id"+counter), BackOffice.storesterminals, "name", "u_posterminal_id");
	
	DataTable.utils.populateSelect(jQuery("#c_taxcategory_id"+counter), BackOffice.multipletaxcategories, "c_taxcategory_name", "c_taxcategory_id");
	
	DataTable.utils.populateSelect(jQuery("#c_tax_id"+counter), BackOffice.taxesratename, "c_tax_name", "c_tax_id");
}
/*dropdown for terminal on multiple taxes*/
function terminalsForOrg(ad_org_id){
	var terminals = BackOffice["storesterminals"];
	
	var ad_org_id = jQuery("#ad_org_id"+counter).val();
	var select = jQuery("#u_posterminal_id"+counter);
	
	jQuery(select).html('');
	var option = '<option value=""></option>';
	
	for(i=0;i<terminals.length;i++){
		var terminal = terminals[i];
		orgId = terminal.ad_org_id;
		
		if(ad_org_id==orgId)
		{
			option = option + '<option value="'+ terminals[i]["u_posterminal_id"] +'" ad_org_id'+counter+'="'+ terminals[i]["ad_org_id"] +'">' + terminals[i]["name"]+ '</option>';
		}
	}
	select.append(option);	
}

/*dropdown for tax*/
function taxForTaxCategory(){
	var taxes = BackOffice["data"];
	
	var c_taxcategory_id = jQuery("#c_taxcategory_id"+counter).val();
	var select = jQuery("#c_tax_id"+counter);
	
	jQuery(select).html('');
	var option = '<option value=""></option>';
	
	for(i=0;i<taxes.length;i++){
		var tax = taxes[i];
		taxCategory_id = tax.c_taxcategory_id;
		
		if(c_taxcategory_id == taxCategory_id)
		{
			option = option + '<option value="'+ taxes[i]["c_tax_id"] +'" c_taxcategory_id'+counter+'="'+ taxes[i]["c_taxcategory_id"] +'">' + taxes[i]["c_tax_name"]+ '</option>';
		}
	}
	select.append(option);	
}

function displayTax(c_taxcategory_id)
{
	var taxes = BackOffice["data"];	
	
	var select = jQuery("select[name='c_tax_id']");
	var option = '<option value=""></option>';
		
	for(var i=0; i<taxes.length; i++)
	{
		option = option + '<option value="'+ taxes[i]["c_tax_id"] +'" c_taxcategory_id="'+ taxes[i]["c_taxcategory_id"] +'">' + taxes[i]["c_tax_name"]+ '</option>';
	}		
	select.append(option);
}

function displayTaxName()
{
	var taxes = BackOffice["data"];
	
	var c_taxcategory_id = jQuery("[name='c_assigment_taxcategory_id']").val();	
	var select = jQuery("select[name='c_tax_name']");
	
	jQuery(select).html('');
	var option = '<option value=""></option>';
		
	for(var i=0; i<taxes.length; i++)
	{
		var tax = taxes[i];
		taxCategoryId = tax.c_taxcategory_id;
		
		if(taxCategoryId == c_taxcategory_id)
		{
			option = option + '<option value="'+ taxes[i]["c_tax_id"] +'" c_assigment_taxcategory_id="'+ taxes[i]["c_taxcategory_id"] +'">' + taxes[i]["c_tax_name"]+ '</option>';
		}			
	}		
	select.append(option);
}


function displayTaxRate(c_tax_id){
	var taxes = BackOffice["data"];	
	var containerRate = jQuery("#rate"+counter);
	var containerSopotype = jQuery("#sopotype");
	var containerIsdefault= jQuery("#isdefault");	
	
	for(i=0;i<taxes.length;i++){
		var tax = taxes[i];
		var tax_id = taxes[i].c_tax_id;
		
		if(tax_id == c_tax_id){
			var taxRate = tax.c_tax_rate
			containerRate.html(taxRate);
			
			var sopotype = tax.c_tax_sopotype;
			containerSopotype.html(sopotype);
			
			var isdefault = tax.isdefault;
			containerIsdefault.html(isdefault);			
		}
	}	
}


function deleteTax(button){
	var row = jQuery(button).parentsUntil("tbody");
	var input = jQuery(row).find("#c_taxcategory_id");
	
	var c_taxcategory_id = input.val();
	
	if(c_taxcategory_id == 0){
		row.remove();
		return;
	}	
	
	var tax_id_to_delete = jQuery("#deletedTaxes").val();
	if(tax_id_to_delete.length > 0){
		tax_id_to_delete = tax_id_to_delete + ",";
	}
	
	var taxid_input = jQuery(row).find("#c_tax_id");
	
	var c_tax_id = taxid_input.val();
	
	tax_id_to_delete = tax_id_to_delete + c_tax_id;
	jQuery("#deletedTaxes").val(tax_id_to_delete);
	
	row.remove();
}

function deleteMultipleTax(button){
	/* delete multiple tax row*/
	var row = jQuery(button).parentsUntil("tbody");
	
	
	if (BackOffice["deletedMultipleTaxes"] == undefined)
	{
		BackOffice["deletedMultipleTaxes"] = new Array();
	}
	
	
	var obj = {};
	var terminalIdInput = jQuery(row).find("#u_posterminal_id");
	
	var taxid_input = jQuery(row).find("#so_c_tax_id");
	var isSoTax = true;
	
	if (taxid_input.length == 0)
	{
		taxid_input = jQuery(row).find("#po_c_tax_id");
		isSoTax = false;
	}
	var c_tax_id = taxid_input.val();
	var u_posterminal_id = terminalIdInput.val();
	
	obj["c_tax_id"] = c_tax_id;
	obj["u_posterminal_id"] = u_posterminal_id;
	obj["isSoTax"] = isSoTax;
	
	BackOffice["deletedMultipleTaxes"].push(obj);
	
	/*var multiple_tax_id_to_delete = jQuery("#deletedMultipleTaxes").val();
	if(multiple_tax_id_to_delete.length > 0){
		multiple_tax_id_to_delete = multiple_tax_id_to_delete + ",";
	}
	
	var taxid_input = jQuery(row).find("#so_c_tax_id");
	
	if (taxid_input.length == 0)
	{
		taxid_input = jQuery(row).find("#po_c_tax_id");
	}
	var c_tax_id = taxid_input.val();
	
	multiple_tax_id_to_delete = multiple_tax_id_to_delete + c_tax_id;
	jQuery("#deletedMultipleTaxes").val(multiple_tax_id_to_delete);*/
	
	row.remove();
	
	jQuery("#add-tax-category-rate-button").show();
}

function deleteSubtax(button){
	 /*delete subtax row and add subtax id to deleted subtaxes list */
	var row = jQuery(button).parentsUntil("tbody");
	var input = jQuery(row).find("#c_subtax_id");
	
	var c_subtax_id = input.val();
	
	if(c_subtax_id == 0){
		row.remove();
		return;
	}
	
	var c_subtax_id_to_delete = jQuery("#deletedsubtaxes").val();
	
	if(c_subtax_id_to_delete.length > 0){
		c_subtax_id_to_delete = c_subtax_id_to_delete + ",";
	}
	
	c_subtax_id_to_delete = c_subtax_id_to_delete + c_subtax_id;
	jQuery("#deletedsubtaxes").val(c_subtax_id_to_delete);
	
	row.remove();
}

function populateSalesTaxSelect(select, jsonArray, label, value){

	var select = jQuery(select);
	select.html("<option value=''></option>");
	
	/*for(var i=0;i<jsonArray.length;i++)
	{
		var json = jsonArray[i];
		
		if (json['isdefault'] == 'Y' && json['c_tax_sopotype'] == 'S')
		{
			select.append(jQuery("<option/>",{value:json[value], html:json[label],selected:'selected'}));
		}
		else
		{
			select.append(jQuery("<option/>",{value:json[value], html:json[label]}));
		}
	}*/
	
	
	/*Sales have higher priority than Both*/
	var setSales = false;
	
	for(i=0;i<jsonArray.length;i++){
		
		if((jsonArray[i].isdefault == 'Y')&&(jsonArray[i].c_tax_sopotype == 'B')){
			var c_tax_id = jsonArray[i].c_tax_id;
			
			if (!setSales)
			{
				select.append(jQuery("<option/>",{value:jsonArray[i][value], html:jsonArray[i][label],selected:'selected'}));
				jQuery(select).val(c_tax_id);
			}
		}
		else
		if((jsonArray[i].isdefault == 'Y')&&(jsonArray[i].c_tax_sopotype == 'S')){
			setSales = true;
			var c_tax_id = jsonArray[i].c_tax_id;
			select.append(jQuery("<option/>",{value:jsonArray[i][value], html:jsonArray[i][label],selected:'selected'}));
			jQuery(select).val(c_tax_id);
		}
		else
		{
			select.append(jQuery("<option/>",{value:jsonArray[i][value], html:jsonArray[i][label]}));
		}
	}
}

function populatePurchaseTaxSelect(select, jsonArray, label, value){

	var select = jQuery(select);
	select.html("<option value=''></option>");
	
	/*for(var i=0;i<jsonArray.length;i++)
	{
		var json = jsonArray[i];
		if (json['isdefault'] == 'Y' && json['c_tax_sopotype'] == 'P')
		{
			select.append(jQuery("<option/>",{value:json[value], html:json[label],selected:'selected'}));
		}
		else
		{
			select.append(jQuery("<option/>",{value:json[value], html:json[label]}));
		}
	}*/
	
	
	/*Purchase have higher priority than Both*/
	var setPurchase = false;
	
	for(i=0;i<jsonArray.length;i++){
		
		if((jsonArray[i].isdefault == 'Y')&&(jsonArray[i].c_tax_sopotype == 'B')){
			var c_tax_id = jsonArray[i].c_tax_id;
			
			if (!setPurchase)
			{
				//jQuery("#po-tax-id").val(c_tax_id);
				select.append(jQuery("<option/>",{value:jsonArray[i][value], html:jsonArray[i][label],selected:'selected'}));
				jQuery(select).val(c_tax_id);
			}
		}
		else
		if((jsonArray[i].isdefault == 'Y')&&(jsonArray[i].c_tax_sopotype == 'P')){
			setPurchase = true;
			var c_tax_id = jsonArray[i].c_tax_id;
			select.append(jQuery("<option/>",{value:jsonArray[i][value], html:jsonArray[i][label],selected:'selected'}));
			jQuery(select).val(c_tax_id);
		}
		else
		{
			select.append(jQuery("<option/>",{value:jsonArray[i][value], html:jsonArray[i][label]}));
		}
	}
}


function displayTaxNameForSubtax()
{
	//var taxes = BackOffice["data"];
	var taxes = BackOffice.datasource;
	
	var c_taxcategory_id = 	BackOffice.model.c_taxcategory_id;
	var select = jQuery("#tax-rate-id");
	
	jQuery(select).html('');
	var option = '<option value=""></option>';
		
	for(var i=0; i<taxes.length; i++)
	{
		var tax = taxes[i];
		taxCategoryId = tax.c_taxcategory_id;
		
		if(taxCategoryId == c_taxcategory_id)
		{
			option = option + '<option value="'+ taxes[i]["c_tax_id"] +'" c_taxcategory_id="'+ taxes[i]["c_taxcategory_id"] +'">' + taxes[i]["c_tax_name"]+ '</option>';
		}			
	}		
	select.append(option);
	
	jQuery('#tax-rate-id').val(BackOffice.model.c_tax_id);
}


BackOffice.cancel = function(){
	
	this.showMaster();
	
	this.initViewActionButtons();
	
	return;
};
