BackOffice.ID_COLUMN_NAME = "u_posterminal_id";
BackOffice.MODEL_NAME = "MPOSTerminal";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveTerminal";

BackOffice.defaults = {
		"u_posterminal_id":0,
		"c_bpartner_id":"",
		"floatamount":0,
		"isactive": "Y",
		"sequence_prefix" : ""
		};

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getTerminals"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var terminals = json["terminals"];
				var stores = json["stores"]
				var warehouses = json["warehouses"];
				var taxes = json["taxes"];
				//var customers = json["customers"]
				var sales_price_list = json["sales-price-list"];
				var purchase_price_list = json["purchase-price-list"];
				//var storesWarehouses = json["storesWarehouses"];
				
								
				//set data source
				//BackOffice.setDataSource(terminals);
				BackOffice.TAXES_DB = TAFFY(taxes);
				
				//BackOffice.storesWarehousesDB = TAFFY(storesWarehouses);
				
				//render org list (stores)
				DataTable.utils.populateSelect(jQuery("#ad_org_id"), stores, "ad_org_name", "ad_org_id");
				
				//render warehouse list
				//DataTable.utils.populateSelect(jQuery("#m_warehouse_id"), warehouses, "m_warehouse_name", "m_warehouse_id", "ad_org_id");
				populateWarehouse(warehouses);
				
				//render sales taxes list
				DataTable.utils.populateSelect(jQuery("#so_tax_id"), taxes, "c_tax_name", "c_tax_id");
				
				//render purchase taxes list
				DataTable.utils.populateSelect(jQuery("#po_tax_id"), taxes, "c_tax_name", "c_tax_id");
				
				//use auto complete
				//render customers list
				//DataTable.utils.populateSelect(jQuery("#c_bpartner_id"), customers, "c_bpartner_name", "c_bpartner_id");
				
				let AUTO_COMPLETE = function( selector, src, labelKey, valueKey ){
					
					var autocomplete = jQuery( selector ).autocomplete({
						
						source: src,
						
						create: function() {
							
							var label = labelKey;
							
							jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
								
								if(item == null) return jQuery("<li>");
								
							      return jQuery( "<li>" )
							        .data( "item.autocomplete", item )
							        .append( "<a>" + item[label] + "</a>" )
							        .appendTo( ul );
							};
					    },
					    
					    select: function(event, ui) {
					    	
					    	var field = jQuery(this);
				            
				            var label = labelKey;
				            var value = valueKey;
				            
				            field.val(ui.item[label]);
					    	
					    	jQuery('#c_bpartner_id').val(ui.item['c_bpartner_id']);

				            return false;
				        },
				        
				        change: function(event, ui) {
				           
				        	if (ui.item == null) {
				            	jQuery('#c_bpartner_id').val('');
				                jQuery(this).val('');
				            }

				            return false;
				        }
						
					});					
					
					return autocomplete;
					
				};
				
				let renderCustomerAutocomplete = function() {
					
					var selector = "[data-report-field='customer']";
					var source = "DataTableAction.do?action=search&field=customer";
					var labelKey = "name";
					var valueKey = "c_bpartner_id";
					    
					AUTO_COMPLETE(selector, source, labelKey, valueKey);	
				};
				
				renderCustomerAutocomplete();
				
				//render sales price list
				DataTable.utils.populateSelect(jQuery("#so_pricelist_id"), sales_price_list, "name", "m_pricelist_id");
				
				//render promotion price list
				DataTable.utils.populateSelect(jQuery("#promotion_pricelist_id"), sales_price_list, "name", "m_pricelist_id");
				
				//render purchases price list
				DataTable.utils.populateSelect(jQuery("#po_pricelist_id"), purchase_price_list, "name", "m_pricelist_id");
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = terminals;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(terminals);
					BackOffice.renderDataTable();
				}
								
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
			{ "data": "terminal_name" },
			{ "data": "org_name"},
			{ "data": "ispaying"},
			{ "data": "so_tax"},
			{ "data": "po_tax"},
			{ "data": "bp_name"},
			{ "data": "so_pricelist"},
			{ "data": "po_pricelist"},
			{ "data": "warehouse_name"},
			{ "data": "sequence_prefix"},
			{ "data": "isactive"}
  	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 10
	      }
	    ]
	});
};

BackOffice.onShowDetail = function(){
	var model = this.model;
	
	jQuery('#bp_name').val('');
	
	if (model==null){
		return;
	}
	
	jQuery('#bp_name').val(model["bp_name"]);
	
	/*var ad_org_id = model["ad_org_id"];
	renderStatesDropdown(ad_org_id);*/
	
	var ad_org_id = model["ad_org_id"];
	
	limitWarehouseToOrg(ad_org_id);
	
	jQuery('#ad_org_id').on('change', function(){
		
		var selectedStore = jQuery('#ad_org_id').val();
		limitWarehouseToOrg(selectedStore);
	});
	
	var m_warehouse_id = model["m_warehouse_id"];	
	
	if (m_warehouse_id != undefined && m_warehouse_id >= 0)
	{
		jQuery("#m_warehouse_id").val(m_warehouse_id);
		return;
	}
	
	var value = jQuery("#m_warehouse_id option:eq(0)").val();
	if(value == "")
	{
		jQuery("#m_warehouse_id").val(jQuery("#m_warehouse_id option:eq(1)").val());
	}	
};

/*jQuery(document).ready(function()
	{	
		var select  = jQuery("#ad_org_id");
		select.on("change", function(e){
			var ad_org_id = jQuery("#ad_org_id").val();
			renderStatesDropdown(ad_org_id);
		});
		
	});*/

/*function renderStatesDropdown(ad_org_id){
	var storesWarehousesDB = BackOffice.storesWarehousesDB;
	
	var org = storesWarehousesDB({"ad_org_id" :{'==':ad_org_id}}).first();	
	var warehouses = org.warehouses;
	
	DataTable.utils.populateSelect(jQuery("#m_warehouse_id"), warehouses, "warehousename", "m_warehouse_id");
	
	var value = jQuery("#m_warehouse_id option:eq(0)").val();
	if(value == "")
	{
		jQuery("#m_warehouse_id").val(jQuery("#m_warehouse_id option:eq(1)").val());
	}
}*/

BackOffice.beforeSave = function(model) {
	/**
	 * Server expects numeric value. Sending empty string. 
	 */
/*	var ad_org_id = model["ad_org_id"];
	
	if (ad_org_id == ""){
		model["ad_org_id"] = 0;
	}
		
	var so_pricelist_id = model["so_pricelist_id"];
	
	if (so_pricelist_id == ""){
		model["so_pricelist_id"] = 0;
	}
	
	var po_pricelist_id = model["po_pricelist_id"];
	
	if (po_pricelist_id == ""){
		model["po_pricelist_id"] = 0;
	} */
	
	var so_tax_id = model["so_tax_id"];
	
	if (so_tax_id == ""){
		model["so_tax_id"] = 0;		
	}
	
	var floatamount = model["floatamount"];
	
	if (floatamount == ""){
		model["floatamount"] = 0;		
	}
	
	var po_tax_id = model["po_tax_id"];
	
	if (po_tax_id == ""){
		model["po_tax_id"] = 0;
	}
	
	var c_bpartner_id = model["c_bpartner_id"];
	
	if (c_bpartner_id == ""){
		model["c_bpartner_id"] = 0;
	}
	
	var promotion_pricelist_id = model["promotion_pricelist_id"];
	if (promotion_pricelist_id == ""){
		model["promotion_pricelist_id"] = 0;		
	}
	
	var select = jQuery("#so_tax_id").get(0);
	model["so_tax"] = DataTable.utils.getSelectedOptionText(select);
	
	select = jQuery("#po_tax_id").get(0);
	model["po_tax"] =DataTable.utils.getSelectedOptionText(select);
	
	select = jQuery("#ad_org_id").get(0);
	model["org_name"] =DataTable.utils.getSelectedOptionText(select);
	
	/*select = jQuery("#c_bpartner_id").get(0);*/
	model["bp_name"] = jQuery("#bp_name").val();
	
	select = jQuery("#so_pricelist_id").get(0);
	model["so_pricelist"] =DataTable.utils.getSelectedOptionText(select);
	
	select = jQuery("#promotion_pricelist_id").get(0);
	model["promotion_pricelist"] =DataTable.utils.getSelectedOptionText(select);
	
	select = jQuery("#po_pricelist_id").get(0);
	model["po_pricelist"] =DataTable.utils.getSelectedOptionText(select);
	
	select = jQuery("#m_warehouse_id").get(0);
	model["warehouse_name"] =DataTable.utils.getSelectedOptionText(select);
};

BackOffice.onCreate = function(){
	
	jQuery('#ad_org_id').on('change', function(){
			
			var selectedStore = jQuery('#ad_org_id').val();
			limitWarehouseToOrg(selectedStore);
		});
};


BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};


function populateWarehouse(warehouses)
{
	if (warehouses != undefined)
	{
		var select = jQuery("#m_warehouse_id");
		var option = '<option value=""></option>';
			
		for(var i=0; i<warehouses.length; i++)
		{
			option = option + '<option value="'+ warehouses[i]["m_warehouse_id"] +'" ad_org_id="'+ warehouses[i]["ad_org_id"] +'">' + warehouses[i]["m_warehouse_name"]+ '</option>';
		}
		
		select.append(option);
	}
}

function limitWarehouseToOrg(ad_org_id)
{
	/*limit warehouse to org selected*/
	jQuery("#m_warehouse_id option").each(function(index, element){
		var option = jQuery(element);
		var orgId = option.attr('ad_org_id');
		jQuery("#m_warehouse_id").val(jQuery("#m_warehouse_id option:eq(0)").val());
		
		if ( orgId != ad_org_id)
		{
			jQuery(element).css('display', 'none');
		}
		else
		{
			jQuery(element).css('display', 'block');
		}
	});
}

Handlebars.registerHelper("getTaxRate", function(id) {
	
	var query = {};
	query["c_tax_id"] = {"==":id};
	
	var tax = BackOffice.TAXES_DB(query).get()[0];
	
	if (tax != undefined)
	{
		return tax["rate"];
	}
	
	return "-";
});


jQuery(document).ready(function(){	
	 var fields = {
			 
			 terminal_name : {
	    			tooltip : Translation.translate("enter.name.terminal.creating.tooltip")
				},
				ad_org_id : {	
					tooltip : Translation.translate("select.store.assign.this.terminal.tooltip")
				},
				m_warehouse_id : {	
					tooltip : Translation.translate("select.which.warehouse.terminal.linked.to.tooltip")
				},
				c_bpartner_id : {	
					tooltip : Translation.translate("choose.default.displayed.customer.tooltip")
				},
				floatamount :{
					tooltip: Translation.translate("enter.float.amount.if.applicable.tooltip")
				},
				so_tax_id :{
					tooltip: Translation.translate("select.an.applicable.sales.tax.here.tooltip")
				},
				po_tax_id :{
					tooltip: Translation.translate("select.applicable.purchase.tax.tooltip")
				},
				so_pricelist_id :{
					tooltip: Translation.translate("select.default.sales.pricelist.menu.tooltip")
				},
				po_pricelist_id :{
					tooltip: Translation.translate("select.default.purchase.pricelist.menu.tooltip")
				}
				
	    };
	 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
	
});
