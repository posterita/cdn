BackOffice.ID_COLUMN_NAME = "ad_user_id";
BackOffice.MODEL_NAME = "MUser";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveUser";
BackOffice.AD_TABLE_ID = 114;

BackOffice.defaults = {
		"ad_user_id":0,
		"issalesrep":"N",
		"isactive": "Y",
		"c_country_id": 245,
		"c_region_id": 0,
		"discountlimit": 0
		};

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getUsers"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var users = json["users"];
				var countries = json["countries"];
				var roles = json["roles"];
				
				//set data source
				//BackOffice.setDataSource(users);
				BackOffice.countryDB = TAFFY(countries);
				
				//render country list
				DataTable.utils.populateSelect(jQuery("#c_country_id"), countries, "c_country_name", "c_country_id");
				
				//render price list
				DataTable.utils.populateSelect(jQuery("#ad_role_id"), roles, "ad_role_name", "ad_role_id");
				
				BackOffice.setDataSource(users);
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
      	      { "data": "user_name" },
      	      { "data": "full_name" },
    	      { "data": "user_email"},
    	      { "data": "role_name"},
    	      { "data": "isactive"}
  	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 4
	      }
	    ]
	});
};

BackOffice.showDefaultCountry = function(){
	
	var countryValue = jQuery("#c_country_id:eq(0)").val();
	
	var c_country_id = countryValue;
	
	if(countryValue == "")
	{
		jQuery("#c_country_id").val("100");
		
		c_country_id = jQuery("#c_country_id").val();
	}
	
	
	var countryDB = BackOffice.countryDB;
	
	var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
	var regions = country.regions;
	
	DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
	
	
	var regionValue = jQuery("#c_region_id:eq(0)").val();
	if(regionValue == "")
	{
		jQuery("#c_region_id").val("0");
	}
};

jQuery(document).ready(function(){
	var select  = jQuery("#c_country_id");
	select.on("change", function(e){
		var c_country_id = jQuery("#c_country_id").val();
		renderStatesDropdown(c_country_id);
	});
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	var fields = {
			user_name : {
				tooltip: Translation.translate("employee.username.login.pos.tooltip")
			},
			full_name : {
				tooltip: Translation.translate("full.name.employee.record.only.tooltip")
			},
			ad_role_id : {
				tooltip: Translation.translate("assign.role.permission.employee.action.menus.access.tooltip")
			},
			user_password : {
				tooltip: Translation.translate("password.alphanumeric.characters.digits.employee.password.tooltip")
			},
			issalesrep : {
				tooltip : Translation.translate("option.employee.sales.assign.tooltip"),
				position : 'top'
			},
			user_email : {
				tooltip : Translation.translate("email.address.employee.if.applicable")
			},
			user_phone : {
				tooltip : Translation.translate("employee.phone.number.if.applicable.tooltip")
			},
			address1 : {
				tooltip : Translation.translate("enter.first.line.of.the.employee.address.tooltip")
			},
			address2 : {
				tooltip : Translation.translate("enter.employee.second.address.line.tooltip")
			},
			city : {
				tooltip : Translation.translate("enter.employee.city.residence.tooltip")
			},
			postal : {
				tooltip : Translation.translate("enter.postal.zip.code.employee.tooltip")
			},
			c_region_id : {
				tooltip : Translation.translate("select.employee.state.residence.tooltip")
			},
			c_country_id : {
				tooltip : Translation.translate("select.employee.country.residence.tooltip")
			},
			average_target_per_day : {
				tooltip : Translation.translate("you.may.enter.target.sales.amount.day.tooltip")
			},
			average_target_per_week : {
				tooltip : Translation.translate("enter.target.sales.amount.per.week.tooltip")
			},
			average_target_per_month : {
				tooltip : Translation.translate("enter.target.sales.amount.per.month.here.tooltip")
			}
		};
	//Include Global Color 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
	
});

BackOffice.beforeSave = function (model)
{
	var c_region_id = model["c_region_id"];
	
	if (c_region_id=="")
	{
		model["c_region_id"] = 0;
	};
	
	var ad_user_id = model["ad_user_id"];
	
	if (ad_user_id=="")
	{
		model["ad_user_id"] = 0;
	};
	
	var select = jQuery("#ad_role_id").get(0);
	model["role_name"] = DataTable.utils.getSelectedOptionText(select);
};

BackOffice.onShowDetail = function(){
	var model = this.model;
	
	if (model==null){
		return;
	};
	
	var password = model["user_password"];
	jQuery("#password").val(password);
	jQuery("#confirm-password").val(password);
	
	jQuery("#categories li a").on("click",function(){
		 
		 if(jQuery("#categories li").hasClass("ui-tabs-selected"))
		{
			 jQuery(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-down")
			 .addClass("fa arrow-icons fa-chevron-right");
		}		  
		  if((jQuery("#categories li").not(".ui-tabs-selected")))
		  {
			 jQuery("#categories li").not(".ui-tabs-selected")
			 .children("i")
			 .removeClass("fa arrow-icons fa-chevron-right")
			 .addClass("fa arrow-icons fa-chevron-down");
		 }
	});
	
	var c_country_id = model["c_country_id"];
	renderStatesDropdown(c_country_id);
	
	var c_region_id = model["c_region_id"];	
	jQuery("#c_region_id").val(c_region_id);
	
	
};

BackOffice.afterCloseAlert = function() {
	
	var model = this.model;
	
	if (model==null){
		return;
	}
	
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

/*BackOffice.beforeShowView = function(id) {
	BackOffice.preloader.show();
	
	jQuery.post("DataTableAction.do", { action: "getUserSalesHistory", ad_user_id: id},
				function(json, textStatus, jqXHR){	
		
					if(json == null || jqXHR.status != 200){
						alert("Failed to request data!"); 
						return;
					}	
					
					BackOffice.salesHistory = json["salesHistory"];
					BackOffice.salesHistoryDB = TAFFY(BackOffice.salesHistory);
					
				},"json").done(function() {
					
					if ( jQuery.fn.dataTable.isDataTable('#sales-history') ) {
						jQuery('#sales-history').dataTable().fnDestroy();
					}
					
					var datasource = BackOffice.salesHistory;
					
					jQuery('#sales-history').dataTable({
						"bAutoWidth": false,
						"data": datasource,
					    "columns": [
					  	      { "data": "dateordered" },
						      { "data": "organization"}, 
						      { "data": "docstatus"}, 
						      { "data": "documentno"},
						      { "data": "grandtotal"},
						      { "data": "customer"}
				  	    ],
					    "columnDefs": [
							 {
							    "render": function (data, type, row) {
							      return moment(data).format("MMMM DD, YYYY HH:MM A");
							    },
							    "targets": 0
							  },
			         	      {
			         	        "render": function (data, type, row) {
				         	          var id = row["c_order_id"];
				         	          return "<a href='javascript:void(0);' onclick='ReportItemDetails.displayOrderInfo(" + id + ")'>" + data + "</a>";
			         	        },
			         	        "targets": 3
			         	      },
			                  {
			                      "targets": [ 2 ],
			                      "visible": false
			                  }     
			         	],
				  	    "order": [[ 0, "desc" ]],
				  	    "deferRender": true
					});
					
					jQuery('#sales-history').on( 'draw.dt', function () {
						var height = jQuery("html").height();
						jQuery("#left-panel").height(height);
					});
					
					jQuery.fn.dataTable.ext.search.push(
						    function( settings, data, dataIndex ) {
						    	if (settings.nTable.attributes['id'].nodeValue == "sales-history")
					    		{
							    	var date_from = jQuery('#date-from').val();
							    	var date_to = jQuery('#date-to').val();
							    	var status = jQuery('#status option:selected').val();
							        
							    	var date = new Date(moment(data[0]).format("MM/DD/YYYY"));
							        var docstatus = data[2];
							        
							        if ( (!date_from.trim() && !date_to.trim() && !status.trim()) ||
							        	 (!date_from.trim() && !date_to.trim() && status.trim() && status == docstatus) ||
							        	 (!date_from.trim() && date_to.trim() && !status.trim() && date <= new Date(date_to)) ||
							        	 (date_from.trim() && !date_to.trim() && !status.trim() && date >= new Date(date_from)) ||
							        	 (date_from.trim() && date_to.trim() && status.trim() && date >= new Date(date_from) && date <= new Date(date_to) && status == docstatus) ||
							        	 (date_from.trim() && date_to.trim() && !status.trim() && date >= new Date(date_from) && date <= new Date(date_to)) ||
							        	 (date_from.trim() && !date_to.trim() && status.trim() && date >= new Date(date_from) && status == docstatus) ||
							        	 (!date_from.trim() && date_to.trim() && status.trim() && date <= new Date(date_to) && status == docstatus) )
							        {
							        	return true;
							        }
							        
							        return false;
					    		}
						    	
						    	return true;
						    });
					
				}).fail(function(){
					alert("Failed to request data!");
				}).always(function(){
					BackOffice.preloader.hide();
					
					var height = jQuery("html").height();
					jQuery("#left-panel").height(height);
				});
};*/

BackOffice.onCreate = function(){
	/*For new users, set isSalesRep by default*/
	if (this.model["ad_user_id"] == 0)
	{
		jQuery('#issalesrep').attr('checked', true);
	}
	
	renderStatesDropdown(245);
	jQuery('#c_region_id').val('0');
};

jQuery(document).ready(function() {
	
	jQuery(function() {
		
		jQuery( "#date-from" ).datepicker({
			format: "mm/dd/yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
		
		jQuery( "#date-to" ).datepicker({
			format: "mm/dd/yyyy",
			orientation: "top left"
		}).on('hide', function (e) { e.preventDefault(); });
	});
	
	jQuery("#refresh-sales-history").on('click' , function() {
		BackOffice.preloader.show();
		
		jQuery("#sales-history").DataTable().draw();
		
		BackOffice.preloader.hide();
	});
	
	jQuery("#sales-history-sort-by").on('change' , function() {
		jQuery("#sales-history").DataTable().column(jQuery(this).val()).order('asc').draw();
	});
});

function renderStatesDropdown(c_country_id){
	var countryDB = BackOffice.countryDB;
	
	var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
	var regions = country.regions;
	
	DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
};

Handlebars.registerHelper('canRingSales', function(issalesrep) {
	
	if (issalesrep == "N") {
		return "No";
	}
	
	return "Yes";
});