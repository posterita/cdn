BackOffice.ID_COLUMN_NAME = "c_bpartner_id";
BackOffice.MODEL_NAME = "MBPartner";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveVendor";

BackOffice.defaults = {
		"issopricelist":"N",
		"c_bpartner_id":0,
		"po_pricelist_id":0,
		"iscustomer":"N",
		"isemployee":"N",
		"isactive": "Y"
		};

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getVendors"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var vendors = json["vendors"];
				var countries = json["countries"];
				var pricelists = json["pricelists"];
				var purchases = json["purchases"];
				var discountcodes = json["discountcodes"];
				
				//set data source
				//BackOffice.setDataSource(vendors);
				
				BackOffice.countryDB = TAFFY(countries);
				BackOffice.purchasesDB = TAFFY(purchases);
				
				BackOffice["discountcodes"] = discountcodes;
				
				//render country list
				DataTable.utils.populateSelect(jQuery("#c_country_id"), countries, "c_country_name", "c_country_id");
				
				//render price list
				DataTable.utils.populateSelect(jQuery("#purchase-price-list"),pricelists, "name", "m_pricelist_id");
				
				//render discount codes
				DataTable.utils.populateSelect(jQuery("#u_pos_discountcode_id"), discountcodes, "name", "u_pos_discountcode_id");
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = vendors;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(vendors);
					BackOffice.renderDataTable();				
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.initializeForm = function()
{
	window.scrollTo(0,0);
	var c_country_id = BackOffice.model["c_country_id"];
	renderStatesDropdown(c_country_id);
	
	var c_region_id = BackOffice.model["c_region_id"];	
	jQuery("#c_region_id").val(c_region_id);
	
	var u_pos_discountcode_id = BackOffice.model["u_pos_discountcode_id"];	
	jQuery("#u_pos_discountcode_id").val(u_pos_discountcode_id);
}

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	if(datasource.length == 0)
	{
		jQuery('.master').hide();
		jQuery('.no-data').show();		
 	}
	else
	{

		jQuery('#example').show();
		jQuery('.no-data').hide();
		jQuery('.master').show();
	}
	
	if (datasource.length > 0)
	{
		BackOffice.dt = jQuery('#example').dataTable({
		    "data": datasource,
		    "columns": [
	  	      { "data": "name" },
	  	      { "data": "email"}, 
	  	      { "data": "phone"},
	  	      { "data": "address1"},
	  	      { "data": "address2"},
	  	      { "data": "city"},
	  	      { "data": "discountcode"},
	  	      { "data": "isactive"}
	  	      
	  	    ],
		    "columnDefs": [
		      {
		        "render": function (data, type, row) {
		          var id = row[BackOffice.ID_COLUMN_NAME];
		          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
		        },
		        "targets": 0
		      },
		      {
		    	  "render": function (data, type, row) {
			        	 var value = row["isactive"];
			          
						 if (value == "Y")
						 {
							  value = "Yes";
						 }
						 else
						 {
							 value = "No";
						 }
						 
						 return value;
			        },
			     "targets": 7
		      }
		    ]
		});
	}
};


/*BackOffice.onShowDetail = function(){
	var dropdown = jQuery("#purchase-price-list");
	var container = jQuery("#price-list-container");
	var checkbox = jQuery("#assignPriceList-checkbox");
	
	if(dropdown.val() != ""){
		container.show();
		checkbox.get(0).checked = true;
	}else{
		container.hide();
		checkbox.get(0).checked = false;
	};
	
	var model = this.model;
	
	if (model==null){
		return;
	};
	
	var c_country_id = model["c_country_id"];
	renderStatesDropdown(c_country_id);
	
	var c_region_id = model["c_region_id"];	
	jQuery("#c_region_id").val(c_region_id);
};*/

BackOffice.afterRenderView = function(id) {
	BackOffice.preloader.show();
	
	var query = {};
	query["c_bpartner_id"] = {"==":id};
	
	var purchases = BackOffice.purchasesDB(query).get();
	
	BackOffice.renderOtherViews("#view-purchases", "#show-purchases", purchases[0]);
	
	BackOffice.preloader.hide();
};

BackOffice.beforeSave = function(model) {
	/**
	 * Server expects numeric value. Sending empty string. 
	 */
	var po_pricelist_id = model["po_pricelist_id"];
	
	if (po_pricelist_id == ""){
		model["po_pricelist_id"] = 0;
	};
	
	var c_country_id = model["c_country_id"];
	
	if (c_country_id == ""){
		model["c_country_id"] = 0;
	};
	
	var c_region_id = model["c_region_id"];
	
	if (c_region_id == ""){
		model["c_region_id"] = 0;
	};
};

BackOffice.showDefaultCountry = function(){
	
	var countryValue = jQuery("#c_country_id:eq(0)").val();
	if(countryValue == "")
	{
		jQuery("#c_country_id").val("245");
		
		var c_country_id = jQuery("#c_country_id").val();
		
		var countryDB = BackOffice.countryDB;
		
		var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
		var regions = country.regions;
		
		DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
	}
	
	var regionValue = jQuery("#c_region_id:eq(0)").val();
	if(regionValue == "")
	{
		jQuery("#c_region_id").val("0");
	}
};

BackOffice.beforeShowView = function(id) {
	
};

jQuery(document).ready(function(){
	/*var container = jQuery("#price-list-container");
	
	var checkbox = jQuery("#assignPriceList-checkbox");
	checkbox.on("change", function(e){
		if(checkbox.is(':checked')){
			container.show();
		}else{
			container.hide();
		}
	});*/
	
	var additionalInformationCheckbox = jQuery("#additional-information-checkbox");
	additionalInformationCheckbox.on("change", function(){
		var container = jQuery("#additional-information-container");
		if(additionalInformationCheckbox.is(':checked')){
			container.show()
		}
		else{
			container.hide();
		}
	});
	
	var select  = jQuery("#c_country_id");
	select.on("change", function(e){
		var c_country_id = jQuery("#c_country_id").val();
		renderStatesDropdown(c_country_id);
	});
	
	var fields = {			
			company_name : {
				tooltip: Translation.translate("tooltip.enter.companyname.sup")				
				},
			taxid : {
				tooltip: Translation.translate("tooltip.enter.taxid.sup")
				},
			name : {
				tooltip: Translation.translate("tooltip.enter.company.contact.name")
				},
			email : {
				tooltip: Translation.translate("tooltip.enter.email.add.company")
				},
			phone : {
				tooltip: Translation.translate("tooltip.optional.enter.phone.of.contract")
				},
			mobile : {
				tooltip: Translation.translate("tooltip.optional.enter.mobile.phone.contract")
				},
			iscustomer : {
				tooltip: Translation.translate("tooltip.make.supplier.also.as.cus"),
				position : 'top'
				},
			isemployee : {
				tooltip: Translation.translate("tooltip.list.supplier.as.employee"),
				position : 'top'
					
				},
			additionalInfo : {
				tooltip: Translation.translate("tooltip.save.full.info.sup"),
				position : 'top'
				},
			address1 :{
				tooltip: Translation.translate("tooltip.enter.first.line.sup")
				},
			address2 :{
				tooltip: Translation.translate("tooltip.enter.second.line.sup")
				},
			city :{
				tooltip: Translation.translate("tooltip.enter.city.sup")
				},
			postal :{
				tooltip: Translation.translate("tooltip.enter.zip.code.sup")
				},
			c_region_id :{
				tooltip: Translation.translate("tooltip.select.state.sup")
				},
			c_country_id :{
				tooltip: Translation.translate("tooltip.select.country.sup")
				},
			po_pricelist_id :{
				tooltip: Translation.translate("tooltip.set.pricelist.sup")
				}
			};
	//Include Global Color 
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
	
});

function renderStatesDropdown(c_country_id){
	var countryDB = BackOffice.countryDB;
	
	var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
	var regions = country.regions;
	
	DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
}

function showDiscountPercentage()
{
	var discountCodes = BackOffice["discountcodes"];
	var selectedVal = jQuery("#u_pos_discountcode_id").val();
	
	for(var i=0; i<discountCodes.length; i++)
	{
		var discountCode = discountCodes[i];
		
		if (discountCode["u_pos_discountcode_id"] == selectedVal)
		{
			jQuery("#discount-code-percentage").html(discountCode["percentage"]);
			jQuery("#discount-code-percentage-container").show();
			break;
		}
		
		if (selectedVal == "")
		{
			jQuery("#discount-code-percentage").html("");
			jQuery("#discount-code-percentage-container").hide();
		}
	}
}