BackOffice.ID_COLUMN_NAME = "m_warehouse_id";
BackOffice.MODEL_NAME = "MWarehouse";
BackOffice.SAVE_URL = "DataTableAction.do?action=saveWarehouse";

BackOffice.defaults = {
		"m_warehouse_id":0,
		"isactive": "Y"
		};

BackOffice.requestDataTableData = function(){
	jQuery.post("DataTableAction.do", { action: "getWarehouses"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				var warehouses = json["warehouses"];
				var countries = json["countries"];
				var orgs = json["orgs"];
				var linkedTerminals = json["linkedTerminals"];
				
				//set data source
				//BackOffice.setDataSource(warehouses);	
				BackOffice.countryDB = TAFFY(countries);
				
				BackOffice.linkedTerminals = TAFFY(linkedTerminals);
				
				//render country list
				DataTable.utils.populateSelect(jQuery("#c_country_id"), countries, "c_country_name", "c_country_id");
				
				//render store list
				DataTable.utils.populateSelect(jQuery("#ad_org_id"), orgs, "ad_org_name", "ad_org_id");
				
				var hash = window.location.hash;
				if(hash.length > 1)
				{
					hash = hash.substring(1);
					
					if (hash == "0")
					{
						BackOffice.datasource = warehouses;	
						BackOffice.db = TAFFY(BackOffice.datasource);	
						//BackOffice.showDetail();
						BackOffice.create();
					}
				}
				else
				{
					BackOffice.setDataSource(warehouses);
					BackOffice.renderDataTable();
				}
				
			},"json").fail(function(){
				alert("Failed to request data!");
			}); 
		
};

BackOffice.renderDataTable = function(){
	
	if(BackOffice.dt){
		BackOffice.dt.fnDestroy();
	}
	
	var datasource = BackOffice.datasource;
	
	BackOffice.dt = jQuery('#example').dataTable({
	    "data": datasource,
	    "columns": [
	  	      { "data": "warehousename" },
		      { "data": "orgname" },
		      { "data": "address1"}, 
		      { "data": "address2"},
		      { "data": "city"},
		      { "data": "isactive"}
		      
  	    ],
	    "columnDefs": [
	      {
	        "render": function (data, type, row) {
	          var id = row[BackOffice.ID_COLUMN_NAME];
	          return "<a href='javascript:void(0);' onclick='BackOffice.view(" + id + ")'>" + data + "</a>";
	        },
	        "targets": 0
	      },
	      {
	        "render": function (data, type, row) {
	        	 var value = row["orgname"];
	          
				 if (value == "*")
				 {
					  value = "All Stores";
				 }
				 
				 return value;
	        },
	        "targets": 1
	      },
	      {
	    	  "render": function (data, type, row) {
		        	 var value = row["isactive"];
		          
					 if (value == "Y")
					 {
						  value = "Yes";
					 }
					 else
					 {
						 value = "No";
					 }
					 
					 return value;
		        },
		     "targets": 5
	      }
	    ]
	});
};

BackOffice.afterCloseAlert = function() {
	var id = BackOffice.model[BackOffice.ID_COLUMN_NAME];
	
	setTimeout(function() 
			{
				BackOffice.clearMessage();
				BackOffice.initViewActionButtons();
				
				if (id > 0)
				{
					BackOffice.view(id);
				}
				
			}, 1000);
};

BackOffice.afterRenderView = function(id) {
	BackOffice.preloader.show();
	
	var query = {};
	query["m_warehouse_id"] = {"==":id};
	
	var terminal = BackOffice.linkedTerminals(query).get();
	
	BackOffice.renderOtherViews("#view-terminals", "#show-terminals", terminal[0]);
	
	BackOffice.preloader.hide();
};

BackOffice.onShowDetail = function(){
	var model = this.model;
	
	if (model==null){
		return;
	}
	
	var c_country_id = model["c_country_id"];
	renderStatesDropdown(c_country_id);
	
	var c_region_id = model["c_region_id"];	
	jQuery("#c_region_id").val(c_region_id);
};

BackOffice.beforeSave = function(model) {
	/**
	 * Server expects numeric value. Sending empty string. 
	 */
	var select = jQuery("#ad_org_id").get(0);
	model["orgname"] = DataTable.utils.getSelectedOptionText(select);
	
	var c_region_id = model["c_region_id"];
	
	if (c_region_id == "") {
		model["c_region_id"] = 0;
	};
};

BackOffice.showDefaultCountry = function(){
	
	var countryValue = jQuery("#c_country_id:eq(0)").val();
	if(countryValue == "")
	{
		jQuery("#c_country_id").val("245");
		
		var c_country_id = jQuery("#c_country_id").val();
		
		var countryDB = BackOffice.countryDB;
		
		var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
		var regions = country.regions;
		
		DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
	}
	
	var regionValue = jQuery("#c_region_id:eq(0)").val();
	if(regionValue == "")
	{
		jQuery("#c_region_id").val("0");
	}
};

jQuery(document).ready(function(){
	var select  = jQuery("#c_country_id");
	select.on("change", function(e){
		var c_country_id = jQuery("#c_country_id").val();
		renderStatesDropdown(c_country_id);
	});
	
	
	var fields = {
			warehousename:{
				tooltip: Translation.translate("enter.name.for.this.warehouse.here.tooltip")
			},
			ad_org_id:{
				tooltip:Translation.translate("select.which.store.will.use.warehouse.tooltip")
			},
			address1 :{
				tooltip: Translation.translate("enter.first.line.the.address.here")
			},
			address2 :{
				tooltip: Translation.translate("enter.second.line.address.here.applicable.tooltip")
			},
			city :{
				tooltip: Translation.translate("enter.the.city.warehouse.located.in.tooltip")
			},
			postal :{
				tooltip: Translation.translate("enter.zip.postal.code.warehouse.located.in.tooltip")
			},
			c_region_id :{
				tooltip: Translation.translate("select.state.applicable.tooltip")
			},
			c_country_id :{
				tooltip: Translation.translate("select.country.warehouse.located.in.tooltip")
			}	
	};
	
	jQuery("#tabs").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 5, borderRadius :  5, width:300});
});

function renderStatesDropdown(c_country_id){
	var countryDB = BackOffice.countryDB;
	
	var country = countryDB({"c_country_id" :{'==':c_country_id}}).first();	
	var regions = country.regions;
	
	DataTable.utils.populateSelect(jQuery("#c_region_id"), regions, "c_region_name", "c_region_id");
}