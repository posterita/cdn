angular.module("app", ['datatables']).controller('CustomerDashboardController', function($scope, $timeout, $http){
			
	$scope.showModal = false;
	
	$scope.colors = {
		"RED" : "#d62728",
		"BLUE" : "#1f77b4",
		"ORANGE" : "#ff7f0e",
		"GREEN" : "#2ca02c",
		"PINK" : "#e377c2",
		"PURPLE" : "#9467bd",
		"BROWN" : "#8c5646"
	};
	
	$scope.default_org_id = default_org_id;
	
	$scope.data = null;
	
	$scope.formatCurrency = function( val ){
		return new Number(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
	};
	
	$scope.formatNumber = function( val ){
		return (val + '').replace(/\d(?=(\d{3})+(\.|$))/g, '$&,');
	};
	
	$scope.formatDate = function(val){
		return moment(val).format('D-MMM-YYYY');
	};
	
	$scope.renderForm = function(){
		
		$http.get("DataTableAction.do?action=getStoresAndTerminals").then(function successCallback(response){
			
			var stores = response.data.stores;
			stores.splice(0,1); //remove org 0
			
			$scope.stores = stores;
									
			$timeout(function(){
				jQuery('#ad_org_id').SumoSelect({'selectAll':true});	
				
				$timeout(function(){
					$scope.refresh();
				});
			});			
			
		}, function errorCallback(response){
			
		})
	};		
			
	$scope.refresh = function(){
		
		//check stores
		var stores = jQuery("#ad_org_id").val();
		if(stores == null || stores.length == 0){
			alert("Please select atleast one store!");
			return;
		}
						
		$scope.showModal = true;
		
		$http.get("DashboardAction.do?action=getCustomerDashboardData&" + jQuery("#dashboard-form").serialize()).then (
			function success(response) {
				
				var data = response.data;					
				
				$scope.renderData( data );
			}, 
			function error(response) {
				$scope.showModal = false;
			}
		);
	};
	
	$scope.renderData = function(data){
		
		$scope.data = data;
		
		$scope.periodInfo = moment(data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') 
			+ ' - ' + moment(data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY');
			
		var selectedStores = [];
		
		jQuery('#ad_org_id option:selected').each((x, y) => {
			selectedStores.push(jQuery(y).text().trim())
		});
	
		$scope.selectedStores = selectedStores;
		
		$scope.renderChart1();
		$scope.renderChart2();
		
		$timeout(function(){
			/* bug fix scale charts */
			window.dispatchEvent(new Event('resize'));
			
			$scope.showModal = false;
		});
	};
	
	$scope.renderChart1 = function(){
		
		var trace1 = {
				x : [],
				y : [],
				marker: {color: $scope.colors.ORANGE },
			    type: 'bar',
		}
		
		$scope.data.salesByAgeGroup.forEach(function(x){						
			trace1.x.push(x.ageGroup);
			trace1.y.push(x.total);
		});
		
		var layout = {
				title: {
					text:'Sales Per Age Group'
				}, 
				xaxis : {
					type:'category'
				}
			};
		
		Plotly.newPlot('sales_per_age', [trace1], layout, {responsive: true});
	
	};
	
	$scope.renderChart2 = function(){
		
		var trace1 = {
				  values: [],
				  labels: [],
				  type: 'pie'
				};
		
		$scope.data.salesByGender.forEach(function(x){						
			trace1.labels.push(x.gender);
			trace1.values.push(x.total);
		});
		
		var layout = {
				title: {
					text:'Sales Per Gender'
				},
				margin: {"t": 40, "b": 10, "l": "auto", "r": "auto"},
		  		height: 400
		};

		Plotly.newPlot('sales_per_gender', [trace1], layout);
		
	};
	
	
	$timeout(function(){
		$scope.renderForm();
	});
		
	$scope.exportPDF = async function(){
		var dd = {
				footer: function(currentPage, pageCount) { 
					return { text : 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment:'center', fontSize: 10 }; 
				},
				pageSize: 'A4',
				pageOrientation : 'portrait',
				
				styles : {
					
					header : {
						fontSize: 24,
						bold: true,
						alignment:'center'
					},
					
					header2 : {
						color: '#5b5b5b',
						fontSize: 18,
						bold: true,
						alignment:'center'
					},
					
					summary_1 : {
						bold: true, 
						alignment:'center', 
						fontSize: 18,
						color: '#1d1d1d',
						margin: [0, 20, 0, 0]
					},
					
					summary_1_label : {
						bold: true, 
						alignment:'center', 
						fontSize: 12,
						color: '#5b5b5b',
						margin: [0, 0, 0, 20]
					},
					
					summary_2_header : {
						bold: true, 
						alignment:'center', 
						fontSize: 16,
						color: '#1d1d1d',
						margin: [0, 20, 0, 20]
					},
					
					summary_2_label : {
						alignment:'left',
						fontSize: 10,
						color: '#5b5b5b'
					},
					
					summary_2_text : {
						alignment:'right',
						fontSize: 10,
						color: '#1d1d1d'
					},
					
					list_header : {
						fontSize: 12,
						color: '#1d1d1d',
						margin: [0, 0, 0, 5]
					},
					
					list_item : {
						fontSize: 10,
						color: '#5b5b5b',
						italics: true,
						margin: [0, 0, 0, 5]
					},
					
					quote: {
						italics: true
					},
					
					small: {
						fontSize: 8
					},
					
					
				},
				content: [
					{
						text : 'Customer Dashboard', 
						style: 'header'
					},
					
					{
						text : 'Period: ' + moment($scope.data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') + ' - ' + moment($scope.data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY'),
						style: 'header2'
					}
				]					
			};
		
			// stores		
			
			var storeList = {
				type: 'square',
				ul : []
			};
			
			var store, stores;
			
			stores = $scope.selectedStores;
			
			for(var i=0; i<stores.length; i++){
				store = stores[i];				
				storeList.ul.push( { text: store, style: 'list_header' } );
			}
			
			/*dd.content.push(storeList);*/
			
			dd.content.push({
				margin: [0, 20],	
			      layout: 'noBorders', // optional
			      table: {
			        widths: [ 'auto', '*'],
			        body: [
			          	[ 
			        	  	{ 
				        		text: "Stores:", 
				        		color: '#5b5b5b',
								fontSize: 18,
								bold: true 
							}, 
			        	  	storeList
			          	]
			        ]
			      }
			});
		
			var table1 = {
			  margin: [0, 20],	
		      layout: 'noBorders', // optional
		      table: {
		        widths: [ '*', '*', 'auto', '*', '*' ],
		        body: [
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) span').text() , style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) span').text(), style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(4) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(5) span').text(), style: 'summary_1', fillColor: '#dff0d8' }
		          	],
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(4) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(5) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }
		        	]
		        ]
		      }
		    };
			
			dd.content.push(table1);
			
			
			var table2 = {
			  margin: [0, 20],	
		      layout: 'noBorders', // optional
		      table: {
		        widths: [ '*', '*' ],
		        body: [
		          	[ 
		        	  { text: 'Satisfaction' , style: 'summary_2_header', fillColor: '#fcf8e3' }, 
		        	  { text: 'Loyalty', style: 'summary_2_header', fillColor: '#fcf8e3' }
		          	],
		          	[ 
		        	  {
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [
						        	[
						        		{ text: jQuery('#summary-2-table-1 .details:nth-child(2) label').text() , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-1 .details:nth-child(2) span').text() , style: 'summary_2_text' },
						        	]
						        ]
		        		  }
		        	  },
		        	  
		        	  {
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [
						        	[
						        		{ text: jQuery('#summary-2-table-2 .details:nth-child(2) label').text() , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-2 .details:nth-child(2) span').text() , style: 'summary_2_text' },
						        	]
						        ]
		        		  }
		        	  }		        	  
		        	]
		        ]
		      }
		    };
			
			dd.content.push(table2);
			
			/* Customer Profile */						
			dd.content.push({
				text: 'Customer Profile', 
				style:'header2', 
				pageOrientation: 'portrait'
			});
			
			var table3 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : '', bold: true },
				        		{ text : 'Known', bold: true },
				        		{ text : 'Unknown', bold: true },
				        	]
				        ]
				      }
			};
			
			var tbl = jQuery('#customer-profile').get(0);
			var rows = tbl.rows;
			
			var tbody = table3.table.body;
			var tr, children = null;			
			
			for(var i=1; i<rows.length; i++){
				
				tr = rows[i];
				children = tr.children;
				
				tbody.push([
					{ text : children[0].innerText, bold: true },					
					children[1].innerText,
					children[2].innerText					
				])
			}						
			
			dd.content.push(table3);
			
			var chart1 = "";
			await Plotly.toImage('sales_per_age',{format: 'svg'}).then(
		             async function(url){ chart1 = url; });
			
			chart1 = chart1.substr(19);
			chart1 = decodeURIComponent(chart1);
			
			var chart2 = "";
			await Plotly.toImage('sales_per_gender',{format: 'svg'}).then(
		             async function(url){ chart2 = url; });
			
			chart2 = chart2.substr(19);
			chart2 = decodeURIComponent(chart2);						
			
			dd.content.push({
				svg: chart1, alignment:'center', pageOrientation: 'landscape', pageBreak: 'before'
			});
			
			dd.content.push({
				svg: chart2,  alignment:'center', pageOrientation: 'landscape', pageBreak: 'before'
			});
						
			/* Last 10 Sales */
			dd.content.push({
				text: 'Last 10 Sales', 
				style:'header2', 
				pageOrientation: 'landscape', 
				pageBreak: 'before'
			});
			
			var table4 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ 'auto', '*', 'auto', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Date', bold: true },
				        		{ text : 'Customer', bold: true },
				        		{ text : 'Store', bold: true },
				        		{ text : 'Receipt No.', bold: true },
				        		{ text : 'Total (' + $scope.data.currencySymbol + ')', bold: true }
				        	]
				        ]
				      }
			};
			
			var lastSales = $scope.data.lastSales;
			var sales = null;
			var tbody = table4.table.body;
			
			for(var i=0; i<lastSales.length; i++){
				sales = lastSales[i];
								
				tbody.push([
					$scope.formatDate(sales.dateOrdered),
					sales.name,
					sales.store,
					sales.documentNo,
					$scope.formatCurrency(sales.total)
				])
			}						
			
			dd.content.push(table4);
						
			/* Top Customers */
			dd.content.push({
				text: 'Top Customers', 
				style:'header2', 
				pageOrientation: 'portrait', 
				pageBreak: 'before'
			});
			
			var table5 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Customer', bold: true },
				        		{ text : 'No Of Orders', bold: true },
				        		{ text : 'Total', bold: true },
				        		{ text : 'Latest Sales', bold: true }
				        	]
				        ]
				      }
			};
			
			var topCustomers = $scope.data.topCustomers;
			var topCustomer = null;
			var tbody = table5.table.body;
			
			for(var i=0; i<topCustomers.length; i++){
				topCustomer = topCustomers[i];
				
				tbody.push([
					topCustomer.name,
					topCustomer.noOfOrders,
					$scope.formatCurrency(topCustomer.total),
					$scope.formatDate(topCustomer.lastDateOrdered)
				])
			}						
			
			dd.content.push(table5);

			pdfMake.createPdf(dd).download("customer-dashboard");
			//pdfMake.createPdf(dd).open();
	};
	
});
