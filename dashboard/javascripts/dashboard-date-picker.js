jQuery(function() {
			
    var start = moment().startOf('month');
    var end = moment();
    
    /* init starting dates*/
    jQuery('#date_from').val(start.format('YYYY-MM-DD'));
	jQuery('#date_to').val(end.format('YYYY-MM-DD'));

    function cb(start, end) {
    	jQuery('#report-date-picker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    jQuery('#report-date-picker').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment()],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment()]
        }
    }, cb);

    cb(start, end);
    
    jQuery('#report-date-picker').on('apply.daterangepicker', function(ev, picker) {
    	  console.log(picker.startDate.format('YYYY-MM-DD'));
    	  console.log(picker.endDate.format('YYYY-MM-DD'));
    	  
    	  jQuery('#date_from').val(picker.startDate.format('YYYY-MM-DD'));
    	  jQuery('#date_to').val(picker.endDate.format('YYYY-MM-DD'));
    	  
    	  jQuery('#report-date-picker').show();
    });
    
    jQuery('#report-date-picker').on('outsideClick.daterangepicker', function(){
    	jQuery('#report-date-picker').show();
    });
    
    jQuery('#report-date-picker').on('hide.daterangepicker', function(){
    	jQuery('#report-date-picker').show();
    });
    
    jQuery('#report-date-picker').on('cancel.daterangepicker', function(){
    	jQuery('#report-date-picker').show();
    }); 		    

});
