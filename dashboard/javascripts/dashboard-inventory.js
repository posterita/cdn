angular.module("app", ['datatables']).controller('InventoryDashboardController', function($scope, $timeout, $http, $q, $window){
	
	$scope.showModal = false;
	
	$scope.colors = {
		"RED" : "#d62728",
		"BLUE" : "#1f77b4",
		"ORANGE" : "#ff7f0e",
		"GREEN" : "#2ca02c",
		"PINK" : "#e377c2",
		"PURPLE" : "#9467bd",
		"BROWN" : "#8c5646"
	};
	
	$scope.data = null;
	
	$scope.default_org_id = default_org_id;
	$scope.default_warehouse_id = default_warehouse_id;
	$scope.default_sales_pricelist_id = default_sales_pricelist_id;
	$scope.default_purchase_pricelist_id = default_purchase_pricelist_id;
	
	$scope.purchase_currency = default_currency_id;
	
	$scope.formatCurrency = function( val ){
		return new Number(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
	};
	
	
	$scope.formatNumber = function( val ){
		return (val + '').replace(/\d(?=(\d{3})+(\.|$))/g, '$&,');
	}
	
	$scope.formatDate = function(val){
		return moment(val).format('D-MMM-YYYY');
	};
	
	$scope.currencyRate = 1;
	
	$window.setCurrencyRate = function(){
		
		$timeout(function(){
			var rate = jQuery("#c_currency_id").val();
			$scope.currencyRate = rate;	
		});
		
	}
	
	$scope.renderForm = function(){
		
		var defer1 = $q.defer();
		
		$http.get("DataTableAction.do?action=getStoresAndWarehouses").then(function successCallback(response){
			
			var stores = response.data.stores;
			
			$scope.stores = stores;		
			
			defer1.resolve();
			
		}, function errorCallback(response){
			
			defer1.reject();
			
		});
		
		var defer2 = $q.defer();
		
		$http.get("DataTableAction.do?action=getPriceList").then(function successCallback(response){
			
			var priceLists = response.data;
			
			$scope.priceLists = priceLists.data;
						
			defer2.resolve();
			
		}, function errorCallback(response){
			
			defer2.reject();
			
		});
		
		var defer3 = $q.defer();
		
		$http.get("DataTableAction.do?action=getCurrencies").then(function successCallback(response){
			
			var currencies = response.data;
			
			$scope.currencies = currencies;
						
			defer3.resolve();
			
		}, function errorCallback(response){
			
			defer3.reject();
			
		});
		
		$q.all([ defer1.promise, defer2.promise, defer3.promise ]).then(function(result){
	        
			$timeout(function(){
				
				jQuery('#m_warehouse_id').SumoSelect({'selectAll':true});				
				
				$timeout(function(){
					$scope.refresh();
				});
				
			});	
			
	    });
	};
	
	$timeout(function(){
		$scope.renderForm();
	});
	
	$scope.refresh = function(){
		
		$scope.showModal = true;
		
		$http.get("DashboardAction.do?action=getInventoryDashboardData&" + jQuery("#dashboard-form").serialize()).then (
			function success(response) {
				
				var data = response.data;
				
				$scope.renderData( data );
				
			}, 
			function error(response) {
				$scope.showModal = false;
			}
		);
	};
	
	$scope.renderData = function(data){
		
		if(data.error){
			alert("Failed to load dashboard data! Reason: " + data.reason);
			$scope.showModal = false;
			return;
		}
		
		$scope.groupName = "primarygroup";// reset department group selector	
		
		$scope.data = data;							
		
		$scope.periodInfo = moment(data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') 
			+ ' - ' + moment(data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY');
		
		$scope.salesPriceListName = $scope.priceLists.find( x => x.m_pricelist_id == $scope.data.sales_pricelist_id ).name;
		$scope.purchasePriceListName = $scope.priceLists.find( x => x.m_pricelist_id == $scope.data.purchase_pricelist_id ).name;
		
		$scope.selectedWarehouses = $scope.getSelectedWarehouses();
		
		$scope.renderChart1();
						
		$timeout(function(){
			/* bug fix scale charts */
			window.dispatchEvent(new Event('resize'));
			
			$scope.showModal = false;
		});
		
	};
	
	$scope.getSelectedWarehouses = function(){
		
		var options = jQuery('#m_warehouse_id').get(0).options;

	    var warehouseNameList = [], warehouseName;

	    for(var i=0; i<options.length; i++){

	        option = options[i];

	        if( $scope.data.m_warehouse_ids.indexOf(parseInt(option.value)) < 0 ) continue;
	        
	        warehouseName = option.label;

	        warehouseNameList.push(warehouseName);
	    }
	    
	    return warehouseNameList;
	};
	
	$scope.renderChart1 = function(){
		
		
		var data = [];
		var store, trace;
		var traceMap = {};
		
		var months = [];
		var m;
		
		for(var i=5; i>=0; i--){
			m = moment().subtract(i, 'months');
			m = m.format("MMM YY");
			months.push(m);
		}		
		
		$scope.data.stockLevels.forEach(function(x){
			
			store = x.store;
			
			trace = traceMap[store];
			
			if(trace == null){
				trace = {
						x: [],
					    y: [],
					    type: 'scatter',
					    name: store + '',
					    line: {shape: 'spline'}
				}
				
				traceMap[store] = trace;
				data.push(trace);
			}
			
			trace.x = months;
			trace.y = x.levels;
		});
		
		var layout = {title: {text:'Inventory Level for Last 6 Months'}};		
		
		if($scope.data.stockLevels.length > 1){
			//layout.legend = {"orientation": "h", xanchor : "center", x:0.5 };
			
			var total = {
					x: months,
				    y: [0,0,0,0,0,0],
				    type: 'scatter',
				    name: 'Total',
				    line: {shape: 'spline'}
			}
			
			$scope.data.stockLevels.forEach(function(x){
				
				for(var i=0; i<6; i++){
					total.y[i] += x.levels[i];
				}
			});
			
			data.push(total);
		}
		
		
		
		Plotly.newPlot('stock_over_6_month', data, layout, {responsive: true});
	
	};
	
	$scope.exportPDF = async function(){
		var dd = {
				footer: function(currentPage, pageCount) { 
					return { text : 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment:'center', fontSize: 10 }; 
				},
				pageSize: 'A4',
				pageOrientation : 'portrait',
				
				styles : {
					
					header : {
						fontSize: 24,
						bold: true,
						alignment:'center'
					},
					
					header2 : {
						/*color: '#5b5b5b',*/
						fontSize: 16
					},					
					
					summary_1 : {
						bold: true, 
						alignment:'center', 
						fontSize: 18,
						color: '#1d1d1d',
						margin: [0, 20, 0, 0]
					},
					
					summary_1_label : {
						bold: true, 
						alignment:'center', 
						fontSize: 12,
						color: '#5b5b5b',
						margin: [0, 0, 0, 20]
					},
					
					summary_2_header : {
						bold: true, 
						alignment:'center', 
						fontSize: 16,
						color: '#1d1d1d',
						margin: [0, 20, 0, 20]
					},
					
					summary_2_label : {
						alignment:'left',
						fontSize: 10,
						color: '#5b5b5b'
					},
					
					summary_2_text : {
						alignment:'right',
						fontSize: 10,
						color: '#1d1d1d'
					},
					
					list_header : {
						fontSize: 12,
						color: '#1d1d1d',
						margin: [0, 0, 0, 5]
					},
					
					list_item : {
						fontSize: 10,
						color: '#5b5b5b',
						italics: true,
						margin: [0, 0, 0, 5]
					},
					
					quote: {
						italics: true
					},
					
					small: {
						fontSize: 8
					},
					
					bold: {
						bold: true
					},
					
					center: {
						alignment:'center'
					}
					
				},
				content: [
					{
						text : 'Inventory Dashboard', 
						style: 'header'
					},
					
					{
						style: 'header2',
						margin: [0, 20, 0, 0],
						text : [
							'Period: ',
							{
								text : '' + moment($scope.data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') + ' - ' + moment($scope.data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY'),
								style : 'bold'
							}
						]						
						
					},					
					
					{
						style: 'header2',
						text : [
							'Sales Pricelist: ', 
							{ 
								text: $scope.salesPriceListName, 
								style: 'bold' 
							} 
						]
						
					},
					
					{
						style: 'header2',
						margin: [0, 0, 0, 20],
						text : [
							'Purchase Pricelist: ',
							{ 
								text: $scope.purchasePriceListName, 
								style: 'bold' 
							}
						]
					}
				]					
			};
		
			// warehouses
			var warehouses = $scope.getSelectedWarehouses();			
			
			var warehouseList = {
					type: 'square',
					ul : []
			};
			
			var warehouse, warehouses;
			
			for(var i=0; i<warehouses.length; i++){
				warehouse = warehouses[i];				
				warehouseList.ul.push( { text: warehouse, style: 'list_header' } );
			}
						
			dd.content.push({
				margin: [0, 0],	
			      layout: 'noBorders', // optional
			      table: {
			        widths: [ 'auto', '*'],
			        body: [
			          	[ 
			        	  	{ 
				        		text: "Warehouses:", 
				        		style: 'header2'
							}, 
							warehouseList
			          	]
			        ]
			      }
			});
		
			var table1 = {
			  margin: [0, 20],	
		      layout: 'noBorders', // optional
		      table: {
		        widths: [ 'auto', 'auto', 'auto', 'auto', 'auto' ],
		        body: [
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) span').text() , style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) span').text(), style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(4) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(5) span').text(), style: 'summary_1', fillColor: '#dff0d8' }
		          	],
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(4) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(5) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }
		        	]
		        ]
		      }
		    };
			
			dd.content.push(table1);
			
			
			var chart1 = "";
			await Plotly.toImage('stock_over_6_month',{format: 'svg'}).then(
		             async function(url){ chart1 = url; });
			
			chart1 = chart1.substr(19);
			chart1 = decodeURIComponent(chart1);
				
			
			dd.content.push({
				svg: chart1, alignment:'center', pageOrientation: 'landscape', pageBreak: 'before'
			});
			
			//tables
			var rows, row, tds, td, line;
			
			/* Last Inventory Count */						
			dd.content.push({
				text: 'Last Inventory Count', 
				style:'header2', 
				pageOrientation: 'portrait', 
				pageBreak: 'before'
			});
			
			var table1 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto'],
				        body: []
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (columnIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			rows = jQuery("#last-inventory-count tr");
			
			for(var i=0; i<rows.length; i++){
				
				row = rows[i];
				tds = row.children;
				
				table1.table.body.push([
					{ text : tds[0].innerText, bold: true },
					{ text : tds[1].innerText }
				]);
			}
			
			dd.content.push(table1);
			
			/* Last 5 Purchase Orders */
			dd.content.push({
				text: 'Last 5 Purchase Orders', 
				style:'header2', 
				pageOrientation: 'portrait'
			});
			
			var table2 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto' ],
				        body: []
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			rows = jQuery("#last-5-purchases tr");
			
			for(var i=0; i<rows.length; i++){
				
				row = rows[i];
				tds = row.children;
				
				line = [];
				
				for(var j=0; j<tds.length; j++){
					
					td = tds[j];
					
					line.push( i == 0 ? { text : td.innerText, bold: true } : { text : td.innerText })
				}
				
				table2.table.body.push(line);
			}
			
			dd.content.push(table2);
			
			/* Top Primary Groups */
			dd.content.push({
				text: 'Top Primary Groups', 
				style:'header2', 
				pageOrientation: 'portrait', 
				pageBreak: 'before'
			});
			
			var table3 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Primary Group', bold: true },
				        		{ text : 'Qty', bold: true },
				        		{ text : 'Sales Value(' + $scope.data.currencySymbol + ')', bold: true },
				        		{ text : 'COGS(' + $scope.data.currencySymbol + ')', bold: true }
				        	]
				        ]
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			var topGroupList = $scope.data.topGroupList["primarygroup"];
			var topGroup = null;
			var tbody = table3.table.body;
			
			for(var i=0; i<topGroupList.length; i++){
				topGroup = topGroupList[i];
				
				tbody.push([
					topGroup.group,
					$scope.formatNumber(topGroup.qty),
					$scope.formatCurrency(topGroup.value),
					$scope.formatCurrency(topGroup.cost * $scope.currencyRate)
				])
			}						
			
			dd.content.push(table3);
			
			/* Moving Items */
			dd.content.push({
				text: 'Moving Items', 
				style:'header2', 
				pageOrientation: 'landscape', 
				pageBreak: 'before'
			});
			
			var table4 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Item Name', bold: true },
				        		{ text : 'Description', bold: true },
				        		{ text : 'Qty In System', bold: true },
				        		{ text : 'Qty Sold', bold: true },
				        		{ text : 'Sales Value(' + $scope.data.currencySymbol + ')', bold: true },
				        		{ text : 'COGS(' + $scope.data.currencySymbol + ')', bold: true }
				        	]
				        ]
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			var movingItems = $scope.data.movingItems;
			var movingItem = null;
			var tbody = table4.table.body;
			
			for(var i=0; i<movingItems.length; i++){
				movingItem = movingItems[i];
				
				tbody.push([
					movingItem.name,
					movingItem.description,
					$scope.formatNumber(movingItem.qtyavailable),
					$scope.formatNumber(movingItem.qtysold),
					$scope.formatCurrency(movingItem.salesvalue),
					$scope.formatCurrency(movingItem.cogs * $scope.currencyRate)
				])
			}						
			
			dd.content.push(table4);

			//pdfMake.createPdf(dd).download("inventory-dashboard");
			pdfMake.createPdf(dd).open();
	};
	
});

