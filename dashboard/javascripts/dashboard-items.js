angular.module("app", ['datatables']).controller('ItemsDashboardController', function($scope, $timeout, $http){
			
	$scope.showModal = false;
	
	$scope.colors = {
		"RED" : "#d62728",
		"BLUE" : "#1f77b4",
		"ORANGE" : "#ff7f0e",
		"GREEN" : "#2ca02c",
		"PINK" : "#e377c2",
		"PURPLE" : "#9467bd",
		"BROWN" : "#8c5646"
	};
	
	$scope.data = null;
	
	$scope.formatCurrency = function( val ){
		return new Number(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
	};
	
	$scope.formatNumber = function( val ){
		return (val + '').replace(/\d(?=(\d{3})+(\.|$))/g, '$&,');
	};
	
	$scope.formatDate = function(val){
		return moment(val).format('D-MMM-YYYY');
	};
	
	$scope.refresh = function(){
		
		$scope.showModal = true;
		
		$http.get("DashboardAction.do?action=getItemsDashboardData&" + jQuery("#dashboard-form").serialize()).then (
			function success(response) {
				
				var data = response.data;
				
				$scope.groupName = "primarygroup";// reset department group selector	
				
				$scope.data = data;							
				
				$scope.periodInfo = moment(data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') 
					+ ' - ' + moment(data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY');
				
				$scope.salesPriceListName = $scope.salesPriceLists.find( x => x.value == $scope.data.m_pricelist_id ).name;
								
				$timeout(function(){
					/* bug fix scale charts */
					window.dispatchEvent(new Event('resize'));
					
					$scope.showModal = false;
				});
			}, 
			function error(response) {
				$scope.showModal = false;
			}
		);
	};
	
	$scope.renderForm = function(){
		
		$http.get("DataTableAction.do?action=getSalesPriceList").then(function successCallback(response){
			
			var salesPriceLists = response.data.salesPrice;
			
			$scope.salesPriceLists = salesPriceLists;
			$scope.default_pricelist_id = response.data.selected;
			
			$timeout(function(){
				$timeout(function(){
					$scope.refresh();
				});
			});		
			
		}, function errorCallback(response){
			
		})
	};
	
	$timeout(function(){
		$scope.renderForm();
	});
	
	$scope.exportPDF = async function(){
		var dd = {
				footer: function(currentPage, pageCount) { 
					return { text : 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment:'center', fontSize: 10 }; 
				},
				pageSize: 'A4',
				pageOrientation : 'portrait',
				
				styles : {
					
					header : {
						fontSize: 24,
						bold: true,
						alignment:'center'
					},
					
					header2 : {
						color: '#5b5b5b',
						fontSize: 18,
						bold: true,
						alignment:'center'
					},
					
					summary_1 : {
						bold: true, 
						alignment:'center', 
						fontSize: 18,
						color: '#1d1d1d',
						margin: [0, 20, 0, 0]
					},
					
					summary_1_label : {
						bold: true, 
						alignment:'center', 
						fontSize: 12,
						color: '#5b5b5b',
						margin: [0, 0, 0, 20]
					},
					
					summary_2_header : {
						bold: true, 
						alignment:'center', 
						fontSize: 16,
						color: '#1d1d1d',
						margin: [0, 20, 0, 20]
					},
					
					summary_2_label : {
						alignment:'left',
						fontSize: 10,
						color: '#5b5b5b'
					},
					
					summary_2_text : {
						alignment:'right',
						fontSize: 10,
						color: '#1d1d1d'
					},
					
					list_header : {
						fontSize: 12,
						color: '#1d1d1d',
						margin: [0, 0, 0, 5]
					},
					
					list_item : {
						fontSize: 10,
						color: '#5b5b5b',
						italics: true,
						margin: [0, 0, 0, 5]
					},
					
					quote: {
						italics: true
					},
					
					small: {
						fontSize: 8
					},
					
					
				},
				content: [
					{
						text : 'Items Dashboard', 
						style: 'header'
					},
					
					{
						text : 'Period: ' + moment($scope.data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') + ' - ' + moment($scope.data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY'),
						style: 'header2'
					},
					
					{
						text : 'Sales Pricelist: ' + $scope.salesPriceListName ,
						style: 'header2'
					}
				]					
			};
		
			
			var table1 = {
			  margin: [0, 20],	
		      layout: 'noBorders', // optional
		      table: {
		        widths: [ '*', '*', '*', '*', '*' ],
		        body: [
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) span').text(), style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) span').text(), style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(4) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(5) span').text(), style: 'summary_1', fillColor: '#dff0d8' }
		          	],
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(4) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(5) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }
		        	]
		        ]
		      }
		    };
			
			dd.content.push(table1);
			
			
			var table2 = {
			  margin: [0, 20],	
		      layout: 'noBorders', // optional
		      table: {
		        widths: [ '*', '*', '*' ],
		        body: [
		          	[ 
		        	  { text: jQuery('#summary-2-table-1 tr.header td:nth-child(1) span').text() , style: 'summary_2_header', fillColor: '#fcf8e3' }, 
		        	  { text: jQuery('#summary-2-table-2 tr.header td:nth-child(1) span').text(), style: 'summary_2_header', fillColor: '#fcf8e3' }, 
		        	  { text: jQuery('#summary-2-table-3 tr.header td:nth-child(1) span').text(), style: 'summary_2_header', fillColor: '#fcf8e3' }
		          	],
		          	[ 
		        	  {
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [
						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-1 tr.details label')[0]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-1 tr.details span')[0]).text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-1 tr.details label')[1]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-1 tr.details span')[1]).text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-1 tr.details label')[2]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-1 tr.details span')[2]).text() , style: 'summary_2_text' },
						        	]
						        ]
		        		  }
		        	  },
		        	  
		        	  {
		        		  alignment:'center',
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [

						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-2 tr.details label')[0]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-2 tr.details span')[0]).text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-2 tr.details label')[1]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-2 tr.details span')[1]).text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-2 tr.details label')[2]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-2 tr.details span')[2]).text() , style: 'summary_2_text' },
						        	]
						        
						        ]
		        		  }
		        	  },
		        	  
		        	  {
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [

						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-3 tr.details label')[0]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-3 tr.details span')[0]).text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-3 tr.details label')[1]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-3 tr.details span')[1]).text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: jQuery(jQuery('#summary-2-table-3 tr.details label')[2]).text() , style: 'summary_2_label' },
						        		{ text: jQuery(jQuery('#summary-2-table-3 tr.details span')[2]).text() , style: 'summary_2_text' },
						        	]
						        
						        ]
		        		  }
		        	  }
		        	  
		        	]
		        ]
		      }
		    };
			
			dd.content.push(table2);
			
			
			
			/* Last 5 items that were updated */						
			dd.content.push({
				text: 'Last 5 items that were updated', 
				style:'header2'
			});
			
			var table3 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Item Name', bold: true },
				        		{ text : 'Description', bold: true },
				        		{ text : 'Updated On', bold: true },
				        	]
				        ]
				      }
			};
			
			var products = $scope.data.last5Updated;
			var product = null;
			var tbody = table3.table.body;
			
			for(var i=0; i<products.length; i++){
				product = products[i];
				
				tbody.push([
					product.name,
					product.description,
					$scope.formatDate(product.updated)
					
				])
			}						
			
			dd.content.push(table3);
			
			/* Recent Items Created */
			dd.content.push({
				text: 'Recent Items Created', 
				style:'header2', 
			});
			
			var table4 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Item Name', bold: true },
				        		{ text : 'Description', bold: true },
				        		{ text : 'Created On', bold: true }
				        	]
				        ]
				      }
			};
			
			var products = $scope.data.last5Created;
			var product = null;
			var tbody = table4.table.body;
			
			for(var i=0; i<products.length; i++){
				product = products[i];
				
				tbody.push([
					product.name,
					product.description,
					$scope.formatDate(product.created)
					
				])
			}							
			
			dd.content.push(table4);
			
			/* Top 10 oldest items */
			dd.content.push({
				text: 'Top 10 Oldest Items', 
				style:'header2', 
				pageOrientation: 'portrait', 
				pageBreak: 'before'
			});
			
			var table5 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Item Name', bold: true },
				        		{ text : 'Description', bold: true },
				        		{ text : 'Qty Sold', bold: true },
				        		{ text : 'Qty Available', bold: true },
				        		{ text : 'Date Created', bold: true },
				        		{ text : 'Last Sold', bold: true }
				        	]
				        ]
				      }
			};
			
			var products = $scope.data.top10Oldest;
			var product = null;
			var tbody = table5.table.body;
			
			for(var i=0; i<products.length; i++){
				product = products[i];
				
				tbody.push([
					product.name,
					product.description,
					product.qtysold,
					product.qtyavailable,
					$scope.formatDate(product.created),
					$scope.formatDate(product.lastsale),
				])
			}							
			
			dd.content.push(table5);
			
			/* No of sku per group */
			dd.content.push({
				text: 'No of sku per group', 
				style:'header2', 
				pageOrientation: 'portrait', 
				pageBreak: 'before'
			});
			
			//var headers = ['primarygroup', 'group1', 'group2', 'group3', 'group4', 'group5', 'group6', 'group7', 'group8'];
			var headers = [$scope.groupName];
			
			var data, group, tblbody;
			
			for(var i=0; i<headers.length; i++){			

				group = headers[i];
				data = $scope.data.groupCountMap[group];
				
				var tbl = {
						  margin: [0, 20],
					      table: {
					    	headerRows: 1,
					        widths: [ '*', 'auto', 'auto' ],
					        body: [
					        	[
					        		{ text : group, bold: true },
					        		{ text : 'Qty', bold: true },
					        		{ text : '%', bold: true }
					        	]
					        ]
					      }
				};
				
				tblbody = tbl.table.body;
				
				for(var j=0; j<data.length; j++){
					
					tblbody.push([
						data[j].group,
						data[j].count,
						data[j].percentage
					]);
				}
				
				dd.content.push(tbl);				
				
			}
			
			

			pdfMake.createPdf(dd).download("items-dashboard");
			//pdfMake.createPdf(dd).open();
	};
	
	$scope.groupName = "primarygroup";// reset department group selector
	$scope.getGroupLabel = function(){
		return jQuery('#groups-select :selected').text();
	};
		
});

