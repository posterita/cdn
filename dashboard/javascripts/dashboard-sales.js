angular.module("app", ['datatables']).controller('SalesDashboardController', function($scope, $timeout, $http, $q){
			
	$scope.showModal = false;
	
	$scope.colors = {
		"RED" : "#d62728",
		"BLUE" : "#1f77b4",
		"ORANGE" : "#ff7f0e",
		"GREEN" : "#2ca02c",
		"PINK" : "#e377c2",
		"PURPLE" : "#9467bd",
		"BROWN" : "#8c5646"
	};
	
	$scope.default_org_id = default_org_id;
	
	$scope.data = null;
	
	$scope.formatCurrency = function( val ){
		return new Number(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
	};
	
	$scope.formatNumber = function( val ){
		return (val + '').replace(/\d(?=(\d{3})+(\.|$))/g, '$&,');
	}
	
	$scope.getSelectedStoresAndTerminals = function(){

	    var optgroups = {};

	    var options = jQuery('#u_posterminal_id').get(0).options;

	    var option, optgroup, terminalName, storeName;

	    for(var i=0; i<options.length; i++){

	        option = options[i];

	        if( $scope.data.terminalIds.indexOf(parseInt(option.value)) < 0 ) continue;
	        
	        terminalName = option.label;

	        optgroup = jQuery(option).parent();
	        
	        storeName = optgroup.attr('label');

	        if(!optgroups[storeName]){				            
	            optgroups[storeName] = [];
	        }

	        optgroups[storeName].push(terminalName);
	    }

	    return optgroups;

	};
	
	
	
	$scope.refresh = function(){
		
		//check terminals
		var terminals = jQuery("#u_posterminal_id").val();
		if(terminals == null || terminals.length == 0){
			alert("Please select atleast one terminal!");
			return;
		}
		
		$scope.showModal = true;
		
		$http.get("DashboardAction.do?action=getSalesDashboardData&" + jQuery("#dashboard-form").serialize()).then (
			function success(response) {
				
				var data = response.data;
				
				$scope.groupName = "primarygroup";// reset department group selector	
				
				$scope.data = data;							
				$scope.renderChart1();
				$scope.renderChart2();
				$scope.renderChart3();
				$scope.renderChart4();					
				
				$scope.periodInfo = moment(data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') 
					+ ' - ' + moment(data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY');
				
				$scope.selectedStoreAndTerminals = $scope.getSelectedStoresAndTerminals();
								
				$timeout(function(){
					/* bug fix scale charts */
					window.dispatchEvent(new Event('resize'));
					
					$scope.showModal = false;
				});
			}, 
			function error(response) {
				$scope.showModal = false;
			}
		);
	};
	
	$scope.renderForm = function(){
		
		$http.get("DataTableAction.do?action=getStoresAndTerminals").then(function successCallback(response){
			
			var stores = response.data.stores;
			stores.splice(0,1); //remove org 0
			
			$scope.stores = stores;
									
			$timeout(function(){
				jQuery('#u_posterminal_id').SumoSelect({'selectAll':true});	
				
				$timeout(function(){
					$scope.refresh();
				});
			});						
			
			
			
		}, function errorCallback(response){
			
		});
		
		var defer1 = $q.defer();
		
		$http.get("DashboardAction.do?action=getCategoryNamings").then(function successCallback(response){
			
						
			$scope.categoryNaming = response.data.categoryNaming;
							
			defer1.resolve();
			
		}, function errorCallback(response){
			
			defer1.reject();
			
		});		
		
	}		
	
	$scope.renderChart1 = function(){					
							
		var data = [];
		var store, trace;
		var traceMap = {};
		
		$scope.data.saleByStoreByDateList.forEach(function(x){						
			store = x.store;
			
			trace = traceMap[store];
			
			if(trace == null){
				trace = {
						x: [],
					    y: [],
					    type: 'scatter',
					    name: store + '',
					    line: {shape: 'spline'}
				}
				
				traceMap[store] = trace;
				data.push(trace);
			}
			
			trace.x.push(x.date);
			trace.y.push(x.value);
		});
		
		var layout = {title: {text:'Sales Over Period'}};
		
		if(data.length > 1){
			
			var total = {
					x: [],
				    y: [],
				    type: 'scatter',
				    name: 'Total',
				    line: {shape: 'spline'}
			}
			
			$scope.data.saleByDateList.forEach(function(x){						
				total.x.push(x.date);
				total.y.push(x.value);
			});
			
			data.push(total);	
			
			layout.legend = {"orientation": "h", xanchor : "center", x:0.5 };
		}
		
		Plotly.newPlot('sales_over_period', data, layout, {responsive: true});
	};
	
	$scope.renderChart2 = function(){
		
		/*
		 
		 var trace1 = {
		  x: [1, 2, 3],
		  y: [40, 50, 60],
		  name: 'yaxis data',
		  type: 'bar',
		  offsetgroup: 1
		};
		
		var trace2 = {
		  x: [2, 3, 4],
		  y: [4, 5, 6],
		  name: 'yaxis2 data',
		  yaxis: 'y2',
		  type: 'bar',
		  offsetgroup: 2
		};
		
		var data = [trace1, trace2];
		
		var layout = {
		  title: 'Double Y Axis Example',
		  yaxis: {title: 'yaxis title'},
		  yaxis2: {
		    title: 'yaxis2 title',
		    titlefont: {color: 'rgb(148, 103, 189)'},
		    tickfont: {color: 'rgb(148, 103, 189)'},
		    overlaying: 'y',
		    side: 'right'
		  }
		};
		 
		 */
							
		var trace1 = {
				x: [],
			    y: [],
			    marker: {color: $scope.colors.ORANGE },
			    type: 'bar',
			    name : "Sales",
			    offsetgroup: 1
		};
		
		var trace2 = {
				x: [],
			    y: [],
			    yaxis: 'y2',
			    marker: {color: $scope.colors.PURPLE },
			    type: 'bar',
			    name : "Tickets",
			    offsetgroup: 2
			    
		}
		
		$scope.data.saleByHourList.forEach(function(x){						
			trace1.x.push(x.hour);
			trace1.y.push(x.value);
			
			trace2.x.push(x.hour);
			trace2.y.push(x.qty);
		});
		
		var layout = {
		  title: { text:'Sales / Tickets per Hour' },
		  yaxis: {title: 'Sales Value'},
		  yaxis2: {title: 'No of Tickets', 'overlaying': 'y', side: 'right'},
		  barmode: 'group',
		  legend: {"orientation": "h", xanchor : "center", x:0.5 }
		};
		
		Plotly.newPlot('sales_per_hour', [trace1, trace2], layout, {responsive: true});
	};
	
	$scope.renderChart3 = function(){
		
		var trace1 = {
				x : [],
				y : [],
				text: [],
				marker: {color: $scope.colors.GREEN },
			    type: 'bar',
			    orientation: 'h'
		}
		
		$scope.data.saleByDiscountCodeList.forEach(function(x){						
			trace1.x.push(x.value);
			trace1.y.push(x.discountCode);
			trace1.text.push(x.percentage + '%');
		});
		
		Plotly.newPlot('discount_codes', [trace1], {title: {text:'Discount Codes'}}, {responsive: true});
	};
	
	$scope.renderChart4 = function(){
		
		var trace1 = {
				x: [],
			    y: [],
			    marker: {color: $scope.colors.RED },
			    type: 'bar'
		}
		
		var labels = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
		
		$scope.data.saleByDayOfWeekList.forEach(function(x){						
			trace1.x.push(labels[x.dow]);
			trace1.y.push(x.value);
		});
		
		Plotly.newPlot('sales_per_dow', [trace1], {title: {text:'Sales Per Day of Week'}}, {responsive: true});
	};
	
	$timeout(function(){
		$scope.renderForm();
	});
	
	/*
	Plotly.toImage('sales_per_hour',{height:300,width:300}).then(function(url){
		console.log(url);
	})
	*/
	
	$scope.exportPDF = async function(){
		var dd = {
				footer: function(currentPage, pageCount) { 
					return { text : 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment:'center', fontSize: 10 }; 
				},
				pageSize: 'A4',
				pageOrientation : 'portrait',
				
				styles : {
					
					header : {
						fontSize: 24,
						bold: true,
						alignment:'center'
					},
					
					header2 : {
						color: '#5b5b5b',
						fontSize: 18,
						bold: true,
						alignment:'center'
					},
					
					summary_1 : {
						bold: true, 
						alignment:'center', 
						fontSize: 18,
						color: '#1d1d1d',
						margin: [0, 20, 0, 0]
					},
					
					summary_1_label : {
						bold: true, 
						alignment:'center', 
						fontSize: 12,
						color: '#5b5b5b',
						margin: [0, 0, 0, 20]
					},
					
					summary_2_header : {
						bold: true, 
						alignment:'center', 
						fontSize: 16,
						color: '#1d1d1d',
						margin: [0, 20, 0, 20]
					},
					
					summary_2_label : {
						alignment:'left',
						fontSize: 10,
						color: '#5b5b5b'
					},
					
					summary_2_text : {
						alignment:'right',
						fontSize: 10,
						color: '#1d1d1d'
					},
					
					list_header : {
						fontSize: 12,
						color: '#1d1d1d',
						margin: [0, 0, 0, 5]
					},
					
					list_item : {
						fontSize: 10,
						color: '#5b5b5b',
						italics: true,
						margin: [0, 0, 0, 5]
					},
					
					quote: {
						italics: true
					},
					
					small: {
						fontSize: 8
					},
					
					
				},
				content: [
					{
						text : 'Sales Dashboard', 
						style: 'header'
					},
					
					{
						text : 'Period: ' + moment($scope.data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') + ' - ' + moment($scope.data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY'),
						style: 'header2'
					}
				]					
			};
		
			// store and terminals
			var storeAndTerminals = $scope.getSelectedStoresAndTerminals();
			
			var storeList = {
				type: 'none',
				ul : []
			};
			
			var stores, store, terminals, terminal;
			
			stores = Object.keys(storeAndTerminals);
			
			for(var i=0; i<stores.length; i++){
				store = stores[i];	
				
				var terminalList = {
						type: 'square',
						ul : []
				};
				
				storeList.ul.push( { text: store, style: 'list_header' } );
				storeList.ul.push( terminalList );
				
				terminals = storeAndTerminals[ store ];							
				
				for(var j=0; j<terminals.length; j++){
					terminal = terminals[j];								
					terminalList.ul.push( { text: terminal, style: 'list_item' } );
				}
			}
			
			/*dd.content.push(storeList);*/
			
			dd.content.push({
				margin: [0, 20],	
			      layout: 'noBorders', // optional
			      table: {
			        widths: [ 'auto', '*'],
			        body: [
			          	[ 
			        	  	{ 
				        		text: "Stores & Terminals:", 
				        		color: '#5b5b5b',
								fontSize: 18,
								bold: true 
							}, 
			        	  	storeList
			          	]
			        ]
			      }
			});
		
			var table1 = {
			  margin: [0, 20],	
		      layout: 'noBorders', // optional
		      table: {
		        widths: [ '*', '*', '*', '*', '*' ],
		        body: [
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) span').text() , style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) span').text(), style: 'summary_1', fillColor: '#dff0d8' }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(4) span').text(), style: 'summary_1', fillColor: '#dff0d8' },
		        	  { text: jQuery('#summary-1 div:nth-child(5) span').text(), style: 'summary_1', fillColor: '#dff0d8' }
		          	],
		          	[ 
		        	  { text: jQuery('#summary-1 div:nth-child(1) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(2) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }, 
		        	  { text: jQuery('#summary-1 div:nth-child(3) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(4) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  },
		        	  { text: jQuery('#summary-1 div:nth-child(5) label').text(), style: 'summary_1_label', fillColor: '#dff0d8'  }
		        	]
		        ]
		      }
		    };
			
			dd.content.push(table1);
			
			
			var table2 = {
			  margin: [0, 20],	
		      layout: 'noBorders', // optional
		      table: {
		        widths: [ '*', '*', '*' ],
		        body: [
		          	[ 
		        	  { text: 'Receipts' , style: 'summary_2_header', fillColor: '#fcf8e3' }, 
		        	  { text: 'Exceptional Operations', style: 'summary_2_header', fillColor: '#fcf8e3' }, 
		        	  { text: 'Lines Sold', style: 'summary_2_header', fillColor: '#fcf8e3' }
		          	],
		          	[ 
		        	  {
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [
						        	[
						        		{ text: 'No. of Sales Receipts' , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-1 .details:nth-child(2) span').text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: 'Avg Receipt Amount' , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-1 .details:nth-child(3) span').text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: 'Avg No. of Items per Receipt' , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-1 .details:nth-child(4) span').text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: 'Highest Ticket' , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-1 .details:nth-child(5) span').text() , style: 'summary_2_text' },
						        	]
						        ]
		        		  }
		        	  },
		        	  
		        	  {
		        		  alignment:'center',
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [
						        	[
						        		{ text: 'No. of Exchanges' , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-2 .details:nth-child(2) span').text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: 'No. of Refunds' , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-2 .details:nth-child(3) span').text() , style: 'summary_2_text' },
						        	],
						        	[
						        		{ text: 'No. of Discounts' , style: 'summary_2_label' },
						        		{ text: jQuery('#summary-2-table-2 .details:nth-child(4) span').text() , style: 'summary_2_text' },
						        	]
						        ]
		        		  }
		        	  },
		        	  
		        	  {
		        		  fillColor: '#fcf8e3',
		        		  layout: 'noBorders',
		        		  margin: [5, 5],
		        		  table: {
						        widths: ['*', 'auto'],
						        body: [
						        	[
						        		{ text: 'No. of Different SKU Sold' , style: 'summary_2_label'},
						        		{ text: jQuery('#summary-2-table-3 .details:nth-child(2) span').text() , style: 'summary_2_text'},
						        	],
						        	[
						        		{ text: '% sold' , style: 'summary_2_label'},
						        		{ text: jQuery('#summary-2-table-3 .details:nth-child(3) span').text() , style: 'summary_2_text'},
						        	],
						        	[
						        		{ text: 'No. of Available SKU' , style: 'summary_2_label'},
						        		{ text: jQuery('#summary-2-table-3 .details:nth-child(4) span').text() , style: 'summary_2_text'},
						        	]
						        ]
		        		  }
		        	  }
		        	  
		        	]
		        ]
		      }
		    };
			
			dd.content.push(table2);
			
			var chart1 = "";
			await Plotly.toImage('sales_over_period',{format: 'svg'}).then(
		             async function(url){ chart1 = url; });
			
			chart1 = chart1.substr(19);
			chart1 = decodeURIComponent(chart1);
			
			var chart2 = "";
			await Plotly.toImage('sales_per_hour',{format: 'svg'}).then(
		             async function(url){ chart2 = url; });
			
			chart2 = chart2.substr(19);
			chart2 = decodeURIComponent(chart2);
			
			var chart3 = "";
			await Plotly.toImage('discount_codes',{format: 'svg'}).then(
		             async function(url){ chart3 = url; });
			
			chart3 = chart3.substr(19);
			chart3 = decodeURIComponent(chart3);
			
			var chart4 = "";
			await Plotly.toImage('sales_per_dow',{format: 'svg'}).then(
		             async function(url){ chart4 = url; });
			
			chart4 = chart4.substr(19);
			chart4 = decodeURIComponent(chart4);
			
			
			dd.content.push({
				svg: chart1, alignment:'center', pageOrientation: 'landscape', pageBreak: 'before'
			});
			
			dd.content.push({
				svg: chart2,  alignment:'center', pageOrientation: 'landscape', pageBreak: 'before'
			});
			
			dd.content.push({
				svg: chart4,  alignment:'center', pageOrientation: 'landscape', pageBreak: 'before'
			});
			
			dd.content.push({
				svg: chart3,  alignment:'center', pageOrientation: 'landscape', pageBreak: 'before'
			});
			
			/* Most Popular Items Sold */						
			dd.content.push({
				text: 'Top 25 Items Sold', 
				style:'header2', 
				pageOrientation: 'portrait', 
				pageBreak: 'before'
			});
			
			var table3 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Item', bold: true },
				        		{ text : 'Qty', bold: true },
				        		{ text : 'Value', bold: true },
				        	]
				        ]
				      }
			};
			
			var products = $scope.data.topProductList;
			var product = null;
			var tbody = table3.table.body;
			
			for(var i=0; i<products.length; i++){
				product = products[i];
				
				tbody.push([
					product.description,
					$scope.formatNumber(product.qty),
					$scope.formatCurrency(product.value)
					
				])
			}						
			
			dd.content.push(table3);
			
			/* Top 10 Department/Primary Groups */
			dd.content.push({
				text: 'Top 25 Department/Primary Groups', 
				style:'header2', 
				pageOrientation: 'landscape', 
				pageBreak: 'before'
			});
			
			var table4 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Department/Primary Group', bold: true },
				        		{ text : 'Qty', bold: true },
				        		{ text : 'Gross Sales', bold: true },
				        		{ text : 'Tax', bold: true },
				        		{ text : 'Discount', bold: true },
				        		{ text : 'Cost', bold: true },
				        		{ text : 'Qty %', bold: true },
				        		{ text : 'Gross Sales %', bold: true }
				        	]
				        ]
				      }
			};
			
			var primaryGroups = $scope.data.topGroupList["primarygroup"];
			var primaryGroup = null;
			var tbody = table4.table.body;
			
			for(var i=0; i<primaryGroups.length; i++){
				primaryGroup = primaryGroups[i];
				
				tbody.push([
					primaryGroup.group + '',
					$scope.formatNumber(primaryGroup.qty),
					$scope.formatCurrency(primaryGroup.grossSales),
					$scope.formatCurrency(primaryGroup.taxAmt),
					$scope.formatCurrency(primaryGroup.discountAmt),
					$scope.formatCurrency(primaryGroup.cost),
					primaryGroup.percentageQty,
					primaryGroup.percentage
				])
			}						
			
			dd.content.push(table4);
			
			/* Sales By Tender Types */
			dd.content.push({
				text: 'Sales By Tender Types', 
				style:'header2', 
				pageOrientation: 'portrait', 
				pageBreak: 'before'
			});
			
			var table5 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto' ],
				        body: [
				        	[
				        		{ text : 'Tender Type', bold: true },
				        		{ text : 'Qty', bold: true },
				        		{ text : 'Value', bold: true },
				        		{ text : '%', bold: true }
				        	]
				        ]
				      }
			};
			
			var tenderTypeList = $scope.data.tenderTypeList;
			var tenderType = null;
			var tbody = table5.table.body;
			
			for(var i=0; i<tenderTypeList.length; i++){
				tenderType = tenderTypeList[i];
				
				tbody.push([
					tenderType.tenderType,
					$scope.formatNumber(tenderType.qty),
					$scope.formatCurrency(tenderType.value),
					tenderType.percentage
				])
			}						
			
			dd.content.push(table5);

			pdfMake.createPdf(dd).download("sales-dashboard");
			//pdfMake.createPdf(dd).open();
	}
	
	$scope.groupName = "primarygroup";// reset department group selector
	$scope.getGroupLabel = function(){
		return jQuery('#groups-select :selected').text();
	};
});
