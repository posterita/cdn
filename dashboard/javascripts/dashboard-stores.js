angular.module("app", ['datatables']).controller('StoreDashboardController', function($scope, $timeout, $http, $q, $window){
	
	$scope.showModal = false;
	
	$scope.colors = {
		"RED" : "#d62728",
		"BLUE" : "#1f77b4",
		"ORANGE" : "#ff7f0e",
		"GREEN" : "#2ca02c",
		"PINK" : "#e377c2",
		"PURPLE" : "#9467bd",
		"BROWN" : "#8c5646"
	};
	
	$scope.data = null;
	
	$scope.default_org_id = default_org_id;
	$scope.default_purchase_pricelist_id = default_purchase_pricelist_id;
	
	$scope.purchase_currency = default_currency_id;
	
	$scope.formatCurrency = function( val ){
		return new Number(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
	};
	
	
	$scope.formatNumber = function( val ){
		return (val + '').replace(/\d(?=(\d{3})+(\.|$))/g, '$&,');
	}
	
	$scope.formatDate = function(val){
		return moment(val).format('D-MMM-YYYY');
	};
	
	$scope.currencyRate = 1;
	
	$window.setCurrencyRate = function(){
		
		$timeout(function(){
			var rate = jQuery("#c_currency_id").val();
			$scope.currencyRate = rate;	
		});
		
	}	
	
	$scope.calculateGrossProfitPercentage = function( financial ){
		
		var grossProfitPercentage;
		
		if( financial.netSales != 0 ){
			
			grossProfitPercentage = ( (financial.netSales - ( financial.cogs * $scope.currencyRate )) / financial.netSales ) * 100;			
		}
		
		else
		{
			grossProfitPercentage = 0;
		}
		
		return grossProfitPercentage;
	}
	
	$scope.getSelectedStores = function(){

	    var stores = []; 

	    var options = jQuery('#ad_org_id').get(0).options;

	    var option, storeName;

	    for(var i=0; i<options.length; i++){

	        option = options[i];

	        if( $scope.data.storeIds.indexOf(parseInt(option.value)) < 0 ) continue;
	        
	        storeName = option.label;
	        
	        stores.push(storeName);
	    }

	    return stores;

	};
	
	$scope.renderForm = function(){
		
		var defer1 = $q.defer();
		
		$http.get("DataTableAction.do?action=getStoresAndTerminals").then(function successCallback(response){
			
			var stores = response.data.stores;
			stores.splice(0,1); //remove org 0
			
			$scope.stores = stores;
			
			defer1.resolve();
									
			$timeout(function(){
				jQuery('#ad_org_id').SumoSelect({'selectAll':true});	
				
				$timeout(function(){
					$scope.refresh();
				});
			});						
						
			
		}, function errorCallback(response){
			
			defer1.reject();
			
		});
		
		var defer2 = $q.defer();
		
		$http.get("DataTableAction.do?action=getPriceList").then(function successCallback(response){
			
			var priceLists = response.data;
			
			$scope.priceLists = priceLists.data;
						
			defer2.resolve();
			
		}, function errorCallback(response){
			
			defer2.reject();
			
		});
		
		var defer3 = $q.defer();
		
		$http.get("DataTableAction.do?action=getCurrencies").then(function successCallback(response){
			
			var currencies = response.data;
			
			$scope.currencies = currencies;
						
			defer3.resolve();
			
		}, function errorCallback(response){
			
			defer3.reject();
			
		});
		
		$q.all([ defer1.promise, defer2.promise, defer3.promise ]).then(function(result){
	        
			$timeout(function(){
				
				jQuery('#ad_org_id').SumoSelect({'selectAll':true});				
				
				$timeout(function(){
					$scope.refresh();
				});
				
			});	
			
	    });
	};
	
	$timeout(function(){
		$scope.renderForm();
	});
	
	$scope.refresh = function(){
		
		//$scope.renderData(TEST_DATA);
		//return;
		
		$scope.showModal = true;
		
		$http.get("DashboardAction.do?action=getStoreDashboardData&" + jQuery("#dashboard-form").serialize()).then (
			function success(response) {
				
				var data = response.data;
				
				$scope.renderData( data );
				
				$scope.selectedStores = $scope.getSelectedStores();
				
			}, 
			function error(response) {
				$scope.showModal = false;
			}
		);
	};
	
	$scope.renderData = function(data){
		
		if(data.error){
			alert("Failed to load dashboard data! Reason: " + data.reason);
			$scope.showModal = false;
			return;
		}
		
		$scope.groupName = "primarygroup";// reset department group selector	
		
		$scope.data = data;	
		
		$scope.selectedStores = $scope.getSelectedStores();		
		
		$scope.periodInfo = moment(data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') 
			+ ' - ' + moment(data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY');
		
		$scope.purchasePriceListName = $scope.priceLists.find( x => x.m_pricelist_id == $scope.data.purchase_pricelist_id ).name;
		
		/*show empty stores*/
		
		$scope.data.financialDetails = $scope.crossTab( $scope.selectedStores, data.financialDetails, { avgReceiptValue: 0, cogs: 0, grossProfit: 0, netSales: 0, numberOfReceipts: 0, grossProfitPercentage: 0 } );
		
		$scope.data.inventoryPurchaseDetails = $scope.crossTab( $scope.selectedStores, data.inventoryPurchaseDetails, { inventoryQty: 0 , inventoryValue: 0, inventoryTurnoverRate: 0, incomingQty: 0, incomingValue: 0 } );
		
		$scope.data.theftLossDetails = $scope.crossTab( $scope.selectedStores, data.theftLossDetails, { tillDifference: 0, inventoryCountDifference: 0, shrinkage: 0 } );
		
		$scope.data.exceptionalOperationDetails = $scope.crossTab( $scope.selectedStores, data.exceptionalOperationDetails, { discount: 0, exchange: 0, refunds: 0, voided: 0, openDrawerCount: 0, rePrintsCount: 0, removedLinesCount: 0 } );
		
		$scope.data.customerDetails = $scope.crossTab( $scope.selectedStores, data.customerDetails, { avgSales :0 } );
						
		$timeout(function(){
			/* bug fix scale charts */
			window.dispatchEvent(new Event('resize'));
			
			$scope.showModal = false;
		});
				
	};
	
	$scope.crossTab = function (rows, data, defaultData){

	    var result = [];
	    var found = false;

	    for(var i=0; i<rows.length; i++){

	        found = false;

	        for(var j=0; j<data.length; j++){
	           
	            if(rows[i] == data[j].store){
	           
	                found = true;

	                result.push(data[j]);

	                break;
	            }
	        }
	       
	        if(!found){
	            var copy = Object.assign({}, defaultData);
	            copy.store = rows[i]
	            result.push( copy );
	        }	       

	    }

	    return result;
	}
	
	$scope.getSelectedWarehouses = function(){
		
		var options = jQuery('#m_warehouse_id').get(0).options;

	    var warehouseNameList = [], warehouseName;

	    for(var i=0; i<options.length; i++){

	        option = options[i];

	        if( $scope.data.m_warehouse_ids.indexOf(parseInt(option.value)) < 0 ) continue;
	        
	        warehouseName = option.label;

	        warehouseNameList.push(warehouseName);
	    }
	    
	    return warehouseNameList;
	};
	
	
	$scope.exportPDF = function(){
		var dd = {
				footer: function(currentPage, pageCount) { 
					return { text : 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment:'center', fontSize: 10 }; 
				},
				pageSize: 'A4',
				pageOrientation : 'portrait',
				
				styles : {
					
					header : {
						fontSize: 24,
						bold: true,
						alignment:'center'
					},
					
					header2 : {
						/*color: '#5b5b5b',*/
						fontSize: 16
					},					
					
					summary_1 : {
						bold: true, 
						alignment:'center', 
						fontSize: 18,
						color: '#1d1d1d',
						margin: [0, 20, 0, 0]
					},
					
					summary_1_label : {
						bold: true, 
						alignment:'center', 
						fontSize: 12,
						color: '#5b5b5b',
						margin: [0, 0, 0, 20]
					},
					
					summary_2_header : {
						bold: true, 
						alignment:'center', 
						fontSize: 16,
						color: '#1d1d1d',
						margin: [0, 20, 0, 20]
					},
					
					summary_2_label : {
						alignment:'left',
						fontSize: 10,
						color: '#5b5b5b'
					},
					
					summary_2_text : {
						alignment:'right',
						fontSize: 10,
						color: '#1d1d1d'
					},
					
					list_header : {
						fontSize: 12,
						color: '#1d1d1d',
						margin: [0, 0, 0, 5]
					},
					
					list_item : {
						fontSize: 10,
						color: '#5b5b5b',
						italics: true,
						margin: [0, 0, 0, 5]
					},
					
					quote: {
						italics: true
					},
					
					small: {
						fontSize: 8
					},
					
					bold: {
						bold: true
					},
					
					center: {
						alignment:'center'
					}
					
				},
				content: [
					{
						text : 'Store Dashboard', 
						style: 'header'
					},
					
					{
						style: 'header2',
						margin: [0, 20, 0, 0],
						text : [
							'Period: ',
							{
								text : '' + moment($scope.data.date_from, 'YYYY-MM-DD').format('MMMM D, YYYY') + ' - ' + moment($scope.data.date_to,'YYYY-MM-DD').format('MMMM D, YYYY'),
								style : 'bold'
							}
						]						
						
					},					
					
					
					{
						style: 'header2',
						margin: [0, 0, 0, 20],
						text : [
							'Purchase Pricelist: ',
							{ 
								text: $scope.purchasePriceListName, 
								style: 'bold' 
							}
						]
					}
				]					
			};
		
			// warehouses
			var stores = $scope.getSelectedStores();			
			
			var storeList = {
					type: 'square',
					ul : []
			};
			
			var store, stores;
			
			for(var i=0; i<stores.length; i++){
				store = stores[i];				
				storeList.ul.push( { text: store, style: 'list_header' } );
			}
						
			dd.content.push({
				margin: [0, 0],	
			      layout: 'noBorders', // optional
			      table: {
			        widths: [ 'auto', '*'],
			        body: [
			          	[ 
			        	  	{ 
				        		text: "Stores:", 
				        		style: 'header2'
							}, 
							storeList
			          	]
			        ]
			      }
			});

			
			
			/* Financials */
			dd.content.push({
				text: 'Financials', 
				style:'header2', 
				pageOrientation: 'portrait'
			});
			
			var table2 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto' ],
				        body: []
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			rows = jQuery("#financials-table tr");
			
			for(var i=0; i<rows.length; i++){
				
				row = rows[i];
				tds = row.children;
				
				line = [];
				
				for(var j=0; j<tds.length; j++){
					
					td = tds[j];
					
					line.push( i == 0 ? { text : td.innerText, bold: true } : { text : td.innerText })
				}
				
				table2.table.body.push(line);
			}
			
			dd.content.push(table2);
			
			/* Inventory/Purchasing */
			dd.content.push({
				text: 'Inventory/Purchasing', 
				style:'header2', 
				pageOrientation: 'portrait'
			});
			
			var table3 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto', 'auto' ],
				        body: []
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			rows = jQuery("#inventory-purchasing-table tr");
			
			for(var i=0; i<rows.length; i++){
				
				row = rows[i];
				tds = row.children;
				
				line = [];
				
				for(var j=0; j<tds.length; j++){
					
					td = tds[j];
					
					line.push( i == 0 ? { text : td.innerText, bold: true } : { text : td.innerText })
				}
				
				table3.table.body.push(line);
			}
			
			dd.content.push(table3);			
			
			
			/* Theft/Loss */
			dd.content.push({
				text: 'Theft/Loss', 
				style:'header2', 
				pageOrientation: 'portrait'
			});
			
			var table4 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto' ],
				        body: []
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			rows = jQuery("#theft-loss-table tr");
			
			for(var i=0; i<rows.length; i++){
				
				row = rows[i];
				tds = row.children;
				
				line = [];
				
				for(var j=0; j<tds.length; j++){
					
					td = tds[j];
					
					line.push( i == 0 ? { text : td.innerText, bold: true } : { text : td.innerText })
				}
				
				table4.table.body.push(line);
			}
			
			dd.content.push(table4);			
			
			/* Exceptional Operation */
			dd.content.push({
				text: 'Exception Operation', 
				style:'header2', 
				pageOrientation: 'portrait'
			});
			
			var table5 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto' ],
				        body: []
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			rows = jQuery("#exceptional-operation-table tr");
			
			for(var i=0; i<rows.length; i++){
				
				row = rows[i];
				tds = row.children;
				
				line = [];
				
				for(var j=0; j<tds.length; j++){
					
					td = tds[j];
					
					line.push( i == 0 ? { text : td.innerText, bold: true } : { text : td.innerText })
				}
				
				table5.table.body.push(line);
			}
			
			dd.content.push(table5);
			
			/* Customer */
			dd.content.push({
				text: 'Customer', 
				style:'header2', 
				pageOrientation: 'portrait'
			});
			
			var table6 = {
					  margin: [0, 20],
				      table: {
				    	headerRows: 1,
				        widths: [ '*', 'auto' ],
				        body: []
				      },
				      layout: {
							fillColor: function (rowIndex, node, columnIndex) {
								return (rowIndex === 0) ? '#DDDDDD' : null;
							}
					  }
			};
			
			rows = jQuery("#customer-table tr");
			
			for(var i=0; i<rows.length; i++){
				
				row = rows[i];
				tds = row.children;
				
				line = [];
				
				for(var j=0; j<tds.length; j++){
					
					td = tds[j];
					
					line.push( i == 0 ? { text : td.innerText, bold: true } : { text : td.innerText })
				}
				
				table6.table.body.push(line);
			}
			
			dd.content.push(table6);
			
			

			//pdfMake.createPdf(dd).download("inventory-dashboard");
			pdfMake.createPdf(dd).open();
	};
	
});

