var TEST_DATA = {
	    "c_currency_id": 280,
	    "currencySymbol": "Rs",
	    "date_from": "2021-05-01 11:58:40.309",
	    "date_to": "2021-09-08 11:58:40.309",
	    "inventoryTurnOverRate": 1.71,
	    "last5purchases": [
	        {
	            "date": 1625635004000,
	            "documentNo": "8001240",
	            "grandTotal": 1481.06,
	            "noOflines": 456,
	            "store": "Adopt Beau Plan"
	        },
	        {
	            "date": 1625630924000,
	            "documentNo": "8001239",
	            "grandTotal": 12617.43,
	            "noOflines": 5059,
	            "store": "Adopt Beau Plan"
	        },
	        {
	            "date": 1625210366000,
	            "documentNo": "8001238",
	            "grandTotal": 7220.86,
	            "noOflines": 2778,
	            "store": "Warehouse"
	        },
	        {
	            "date": 1625210239000,
	            "documentNo": "8001237",
	            "grandTotal": 5103.72,
	            "noOflines": 2224,
	            "store": "Warehouse"
	        },
	        {
	            "date": 1624271091000,
	            "documentNo": "8001236",
	            "grandTotal": 9818.96,
	            "noOflines": 3843,
	            "store": "Warehouse"
	        }
	    ],
	    "lastInventoryAdjustment": {
	        "date": 1624532813000,
	        "difference": 692,
	        "differenceCost": 1279.10,
	        "documentNo": "6200187",
	        "qtyBooked": 434,
	        "qtyCounted": 1126,
	        "store": "Adopt Trianon"
	    },
	    "m_warehouse_ids": [
	        10007712,
	        10007713,
	        10007714,
	        10007715,
	        10007716,
	        10007717,
	        10007718,
	        10007711,
	        10007719,
	        10007720,
	        10007808,
	        10007835,
	        10007955,
	        10008110,
	        10008156,
	        10007820,
	        10008132,
	        10008162
	    ],
	    "movingItems": [
	        {
	            "cogs": 1541.08,
	            "description": "30MLFAIRYLANDPA",
	            "m_product_id": 10878454,
	            "name": "30MLFAIRYLANDPA",
	            "qtyonhand": 492,
	            "qtysold": 653,
	            "salesvalue": 312049.65
	        },
	        {
	            "cogs": 1460.84,
	            "description": "30 ML PERFUME LADY GLITTER",
	            "m_product_id": 10688835,
	            "name": "30MLLADY GLITTER",
	            "qtyonhand": 1506,
	            "qtysold": 619,
	            "salesvalue": 293012.80
	        },
	        {
	            "cogs": 941.27,
	            "description": "FACE POWDER N°2",
	            "m_product_id": 10689618,
	            "name": "COVERFIT POUDRE 02",
	            "qtyonhand": 1014,
	            "qtysold": 473,
	            "salesvalue": 214769.60
	        },
	        {
	            "cogs": 1014.72,
	            "description": "30 ML PERFUME BLUE SUIT----",
	            "m_product_id": 10688910,
	            "name": "30MLBLUE SUIT",
	            "qtyonhand": 1002,
	            "qtysold": 453,
	            "salesvalue": 214046.05
	        },
	        {
	            "cogs": 788.04,
	            "description": "FACE POWDER N°4",
	            "m_product_id": 10690275,
	            "name": "COVERFIT POUDRE 04",
	            "qtyonhand": 122,
	            "qtysold": 396,
	            "salesvalue": 178941.40
	        },
	        {
	            "cogs": 857.92,
	            "description": "30 ML PERFUME YUMMY CANDY----",
	            "m_product_id": 10688983,
	            "name": "30MLYUMMY CANDY",
	            "qtyonhand": 1290,
	            "qtysold": 383,
	            "salesvalue": 181186.90
	        },
	        {
	            "cogs": 612.36,
	            "description": "WATERPROOF EYE PENCIL 1",
	            "m_product_id": 10689586,
	            "name": "CRAYWATER001 ADOPT",
	            "qtyonhand": 885,
	            "qtysold": 378,
	            "salesvalue": 94009.95
	        },
	        {
	            "cogs": 824.32,
	            "description": "30 ML PERFUME LEAU VIRILE----",
	            "m_product_id": 10688922,
	            "name": "30MLLEAU VIRILE",
	            "qtyonhand": 1183,
	            "qtysold": 368,
	            "salesvalue": 176770.75
	        },
	        {
	            "cogs": 866.12,
	            "description": "30 ML PERFUME MIDNIGHT FOR HER--",
	            "m_product_id": 10688969,
	            "name": "30MLMID FOR HE",
	            "qtyonhand": 206,
	            "qtysold": 367,
	            "salesvalue": 175124.05
	        },
	        {
	            "cogs": 530.44,
	            "description": "BLACK FELT TIP EYELINER----",
	            "m_product_id": 10689612,
	            "name": "EL FEUTRE ADOPT",
	            "qtyonhand": 451,
	            "qtysold": 356,
	            "salesvalue": 115150.00
	        },
	        {
	            "cogs": 690.53,
	            "description": "FACE  POWDER N°1",
	            "m_product_id": 10688857,
	            "name": "COVERFIT POUDRE 01",
	            "qtyonhand": 1275,
	            "qtysold": 347,
	            "salesvalue": 155987.40
	        },
	        {
	            "cogs": 732.48,
	            "description": "EDP 30ML NO RULES",
	            "m_product_id": 10913364,
	            "name": "30MLNORULES",
	            "qtyonhand": 504,
	            "qtysold": 327,
	            "salesvalue": 155014.35
	        },
	        {
	            "cogs": 475.50,
	            "description": "EYELINER N°1 NUCO/M330/16/FF(EK017)",
	            "m_product_id": 10688902,
	            "name": "EYELINER 01 ADOPT",
	            "qtyonhand": 807,
	            "qtysold": 317,
	            "salesvalue": 101710.35
	        },
	        {
	            "cogs": 741.04,
	            "description": "BOUQUETDAMO PERFUME 30 ML",
	            "m_product_id": 10933609,
	            "name": "30MLBOUQUETDAMO",
	            "qtyonhand": 142,
	            "qtysold": 314,
	            "salesvalue": 150827.40
	        },
	        {
	            "cogs": 581.08,
	            "description": "FACE  POWDER N°5",
	            "m_product_id": 10704737,
	            "name": "COVERFIT POUDRE 05",
	            "qtyonhand": 726,
	            "qtysold": 292,
	            "salesvalue": 133732.00
	        },
	        {
	            "cogs": 613.60,
	            "description": "30 ML PERFUME STAR NIGHT",
	            "m_product_id": 10913367,
	            "name": "30MLSTARNIGHT",
	            "qtyonhand": 1404,
	            "qtysold": 260,
	            "salesvalue": 123727.05
	        },
	        {
	            "cogs": 575.68,
	            "description": "30MLLADY GLI NP",
	            "m_product_id": 10865266,
	            "name": "30MLLADY GLI NP",
	            "qtyonhand": 872,
	            "qtysold": 257,
	            "salesvalue": 122354.80
	        },
	        {
	            "cogs": 507.50,
	            "description": "30 ML PERFUME PAMPLEMOUSSE GRENADE",
	            "m_product_id": 10688885,
	            "name": "30MLPAMPLEMOUS GRE",
	            "qtyonhand": 435,
	            "qtysold": 250,
	            "salesvalue": 118362.80
	        },
	        {
	            "cogs": 427.31,
	            "description": "Automatic brow liner 03",
	            "m_product_id": 10690377,
	            "name": "IDEAL BROW PL 03",
	            "qtyonhand": 480,
	            "qtysold": 247,
	            "salesvalue": 76920.20
	        },
	        {
	            "cogs": 546.75,
	            "description": "COVERCORRECT CORRECTEUR LIQUIDE HAU",
	            "m_product_id": 10738100,
	            "name": "109150001007",
	            "qtyonhand": 249,
	            "qtysold": 243,
	            "salesvalue": 117140.25
	        },
	        {
	            "cogs": 151.83,
	            "description": "MINI PENCIL",
	            "m_product_id": 10689033,
	            "name": "MINICRAY001 ADOPT",
	            "qtyonhand": 564,
	            "qtysold": 241,
	            "salesvalue": 35871.75
	        },
	        {
	            "cogs": 510.27,
	            "description": "EDP 30ML EXTREME SENSATIO",
	            "m_product_id": 10921026,
	            "name": "30MLEXTREMESENS",
	            "qtyonhand": 227,
	            "qtysold": 233,
	            "salesvalue": 111426.70
	        },
	        {
	            "cogs": 508.48,
	            "description": "30 ML PERFUME  SWEET ROMANCE",
	            "m_product_id": 10688979,
	            "name": "30MLSWEET ROMANCE",
	            "qtyonhand": 792,
	            "qtysold": 227,
	            "salesvalue": 105663.25
	        },
	        {
	            "cogs": 506.24,
	            "description": "30 ML PERFUMEINCTINCT MALE",
	            "m_product_id": 10688834,
	            "name": "30MLINSTINCT MALE",
	            "qtyonhand": 1205,
	            "qtysold": 226,
	            "salesvalue": 105788.00
	        },
	        {
	            "cogs": 470.40,
	            "description": "30 ML PERFUME BLACK ROSE----",
	            "m_product_id": 10688978,
	            "name": "30MLROSE NOIRE",
	            "qtyonhand": 1174,
	            "qtysold": 210,
	            "salesvalue": 97604.40
	        },
	        {
	            "cogs": 472.50,
	            "description": "COVERCORRECT CORRECTEUR LIQUIDE HAU",
	            "m_product_id": 10738009,
	            "name": "109150001008",
	            "qtyonhand": 304,
	            "qtysold": 210,
	            "salesvalue": 98976.65
	        },
	        {
	            "cogs": 474.36,
	            "description": "EDP 30ML OUI MON AMOUR",
	            "m_product_id": 10909928,
	            "name": "30MLOUIMONAMOUR",
	            "qtyonhand": 168,
	            "qtysold": 201,
	            "salesvalue": 95159.30
	        },
	        {
	            "cogs": 198.00,
	            "description": "VAO PRO NAIL COLOUR 043--",
	            "m_product_id": 10776752,
	            "name": "104003122043",
	            "qtyonhand": 201,
	            "qtysold": 200,
	            "salesvalue": 45250.40
	        },
	        {
	            "cogs": 445.76,
	            "description": "FRENCH BARBER PERFUME30ML",
	            "m_product_id": 10934225,
	            "name": "30MLFRENCHBA00",
	            "qtyonhand": 437,
	            "qtysold": 199,
	            "salesvalue": 96406.80
	        },
	        {
	            "cogs": 446.04,
	            "description": "30MLLOVEMOOD",
	            "m_product_id": 10793131,
	            "name": "30MLLOVEMOOD",
	            "qtyonhand": 566,
	            "qtysold": 189,
	            "salesvalue": 89321.00
	        },
	        {
	            "cogs": 344.10,
	            "description": "GEL DOUCHE BOUQUET D'AM",
	            "m_product_id": 10933649,
	            "name": "801801032001",
	            "qtyonhand": 210,
	            "qtysold": 186,
	            "salesvalue": 60734.70
	        },
	        {
	            "cogs": 524.48,
	            "description": "EDP 30ML SUNSET KISS",
	            "m_product_id": 10910821,
	            "name": "30MLSUNSETKISS",
	            "qtyonhand": 200,
	            "qtysold": 176,
	            "salesvalue": 84106.45
	        },
	        {
	            "cogs": 173.25,
	            "description": "Eyebrow pencil 03",
	            "m_product_id": 10688692,
	            "name": "IDEAL BROW PEN 03",
	            "qtyonhand": 470,
	            "qtysold": 175,
	            "salesvalue": 43575.00
	        },
	        {
	            "cogs": 378.56,
	            "description": "EDP 30ML INTO THE NIGHT",
	            "m_product_id": 10914418,
	            "name": "30MLINTOTHENIGH",
	            "qtyonhand": 407,
	            "qtysold": 169,
	            "salesvalue": 80388.90
	        },
	        {
	            "cogs": 572.70,
	            "description": "COVERPLUS FOND DE TEINT COMPACT MAT",
	            "m_product_id": 10778311,
	            "name": "109151003003",
	            "qtyonhand": 252,
	            "qtysold": 166,
	            "salesvalue": 110861.40
	        },
	        {
	            "cogs": 562.35,
	            "description": "COVERPLUS FOND DE TEINT COMPACT MAT",
	            "m_product_id": 10778316,
	            "name": "109151003004",
	            "qtyonhand": 356,
	            "qtysold": 163,
	            "salesvalue": 107995.50
	        },
	        {
	            "cogs": 365.12,
	            "description": "EDP 30ML MA CHERRY",
	            "m_product_id": 10908542,
	            "name": "30MLMACHERRY",
	            "qtyonhand": 100,
	            "qtysold": 163,
	            "salesvalue": 77020.65
	        },
	        {
	            "cogs": 362.88,
	            "description": "30 ML PERFUME MIDNIGHT FOR HIM--",
	            "m_product_id": 10688925,
	            "name": "30MLMID FOR HI",
	            "qtyonhand": 1299,
	            "qtysold": 162,
	            "salesvalue": 76721.25
	        },
	        {
	            "cogs": 344.96,
	            "description": "30 ML PERFUME RED DRESS----",
	            "m_product_id": 10688933,
	            "name": "30MLRED DRESS",
	            "qtyonhand": 517,
	            "qtysold": 154,
	            "salesvalue": 72754.20
	        },
	        {
	            "cogs": 329.28,
	            "description": "30 ML PERFUME DOUX BAISER----",
	            "m_product_id": 10688828,
	            "name": "30MLDOUX BAISER",
	            "qtyonhand": 380,
	            "qtysold": 147,
	            "salesvalue": 69186.35
	        },
	        {
	            "cogs": 324.80,
	            "description": "EDP 30ML LATOUT CHARME",
	            "m_product_id": 10921024,
	            "name": "30MLLATOUTCHARM",
	            "qtyonhand": 261,
	            "qtysold": 145,
	            "salesvalue": 70658.40
	        },
	        {
	            "cogs": 143.55,
	            "description": "VAO PRO NAIL COLOUR 040--",
	            "m_product_id": 10776750,
	            "name": "104003122040",
	            "qtyonhand": 409,
	            "qtysold": 145,
	            "salesvalue": 32701.20
	        },
	        {
	            "cogs": 274.62,
	            "description": "FACE  POWDER N°00",
	            "m_product_id": 10690259,
	            "name": "COVERFIT POUDRE 00",
	            "qtyonhand": 696,
	            "qtysold": 138,
	            "salesvalue": 60977.80
	        },
	        {
	            "cogs": 304.64,
	            "description": "EDP 30ML LIBERTY DREAM FD",
	            "m_product_id": 10859271,
	            "name": "30MLLIBERTY DRE",
	            "qtyonhand": 1057,
	            "qtysold": 136,
	            "salesvalue": 64271.20
	        },
	        {
	            "cogs": 621.76,
	            "description": "100ML PERFUME LADY GLITTER",
	            "m_product_id": 10688950,
	            "name": "100MLLADY GLITTER",
	            "qtyonhand": 276,
	            "qtysold": 134,
	            "salesvalue": 128871.00
	        },
	        {
	            "cogs": 300.16,
	            "description": "EDP 30ML COEUR A COEUR",
	            "m_product_id": 10895609,
	            "name": "30MLCOEURACOEUR",
	            "qtyonhand": 594,
	            "qtysold": 134,
	            "salesvalue": 63747.25
	        },
	        {
	            "cogs": 295.68,
	            "description": "30MLEAU DES LYS",
	            "m_product_id": 10865265,
	            "name": "30MLEAU DES LYS",
	            "qtyonhand": 955,
	            "qtysold": 132,
	            "salesvalue": 62375.00
	        },
	        {
	            "cogs": 224.46,
	            "description": "EPONGE A MAQUILLAGE",
	            "m_product_id": 10869943,
	            "name": "101102001001",
	            "qtyonhand": 141,
	            "qtysold": 129,
	            "salesvalue": 51471.00
	        },
	        {
	            "cogs": 160.02,
	            "description": "1.04117E+11",
	            "m_product_id": 10872051,
	            "name": "104117001001",
	            "qtyonhand": 306,
	            "qtysold": 127,
	            "salesvalue": 34930.80
	        },
	        {
	            "cogs": 219.71,
	            "description": "Automatic brow liner 04",
	            "m_product_id": 10690365,
	            "name": "IDEAL BROW PL 04",
	            "qtyonhand": 126,
	            "qtysold": 127,
	            "salesvalue": 36897.35
	        },
	        {
	            "cogs": 200.88,
	            "description": "WATER",
	            "m_product_id": 10690246,
	            "name": "CRAYWATER012 ADOPT",
	            "qtyonhand": 394,
	            "qtysold": 124,
	            "salesvalue": 30876.00
	        },
	        {
	            "cogs": 138.99,
	            "description": "TOP COAT EFFET GEL--",
	            "m_product_id": 10735443,
	            "name": "104002117001",
	            "qtyonhand": 126,
	            "qtysold": 123,
	            "salesvalue": 30228.60
	        },
	        {
	            "cogs": 273.28,
	            "description": "30MLLOVE MOO NP",
	            "m_product_id": 10865264,
	            "name": "30MLLOVE MOO NP",
	            "qtyonhand": 744,
	            "qtysold": 122,
	            "salesvalue": 57285.20
	        },
	        {
	            "cogs": 398.84,
	            "description": "EAU MICELLAIRE APAISANTE--",
	            "m_product_id": 10834075,
	            "name": "107138001001",
	            "qtyonhand": 268,
	            "qtysold": 118,
	            "salesvalue": 4491.00
	        },
	        {
	            "cogs": 317.84,
	            "description": "LIQUID FOUNDATION N°2",
	            "m_product_id": 10689601,
	            "name": "COVERFIT FDTFLU 02",
	            "qtyonhand": 309,
	            "qtysold": 116,
	            "salesvalue": 45524.00
	        },
	        {
	            "cogs": 259.84,
	            "description": "30 ML PERFUME LITTLE SUGAR",
	            "m_product_id": 10688923,
	            "name": "30MLLITTLE SUGAR",
	            "qtyonhand": 480,
	            "qtysold": 116,
	            "salesvalue": 54790.20
	        },
	        {
	            "cogs": 259.84,
	            "description": "30 ML PERFUME FREESIA MAGNOLIA",
	            "m_product_id": 10688963,
	            "name": "30MLFREESIA MAGNOL",
	            "qtyonhand": 610,
	            "qtysold": 116,
	            "salesvalue": 54291.20
	        },
	        {
	            "cogs": 184.00,
	            "description": "EXTREM PRECISE EYELINER 001",
	            "m_product_id": 10688645,
	            "name": "EL PRECIS 01 ADOPT",
	            "qtyonhand": 47,
	            "qtysold": 115,
	            "salesvalue": 34664.70
	        },
	        {
	            "cogs": 258.77,
	            "description": "LAIT CORPS NACRE BOUQUET",
	            "m_product_id": 10933650,
	            "name": "802808032001",
	            "qtyonhand": 157,
	            "qtysold": 113,
	            "salesvalue": 55305.75
	        },
	        {
	            "cogs": 250.88,
	            "description": "30 ML PERFUME MISS BLOOM",
	            "m_product_id": 10688720,
	            "name": "30MLMADMOIS BLOOM",
	            "qtyonhand": 964,
	            "qtysold": 112,
	            "salesvalue": 53168.45
	        },
	        {
	            "cogs": 220.89,
	            "description": "FACE  POWDER N°6",
	            "m_product_id": 10690260,
	            "name": "COVERFIT POUDRE 06",
	            "qtyonhand": 774,
	            "qtysold": 111,
	            "salesvalue": 50598.60
	        },
	        {
	            "cogs": 248.64,
	            "description": "EDP 30ML PEACH ME",
	            "m_product_id": 10908543,
	            "name": "30MLPEACHME",
	            "qtyonhand": 272,
	            "qtysold": 111,
	            "salesvalue": 52195.40
	        },
	        {
	            "cogs": 259.60,
	            "description": "BOITE CADEAU NOEL 2020",
	            "m_product_id": 10914419,
	            "name": "BOITE CADEA N20",
	            "qtyonhand": 192,
	            "qtysold": 110,
	            "salesvalue": 195.00
	        },
	        {
	            "cogs": 562.44,
	            "description": "COF ETUI PF",
	            "m_product_id": 10919461,
	            "name": "205223001130",
	            "qtyonhand": 236,
	            "qtysold": 109,
	            "salesvalue": 1750.00
	        },
	        {
	            "cogs": 729.00,
	            "description": "30GD INTO THE NIGHT PF",
	            "m_product_id": 10919464,
	            "name": "205223002111",
	            "qtyonhand": 9,
	            "qtysold": 108,
	            "salesvalue": 80367.70
	        },
	        {
	            "cogs": 119.78,
	            "description": "VERNIS A ONGLES GEL NAIL COLOUR 010",
	            "m_product_id": 10735450,
	            "name": "104002121010",
	            "qtyonhand": 558,
	            "qtysold": 106,
	            "salesvalue": 26194.80
	        },
	        {
	            "cogs": 153.70,
	            "description": "LIP & KISS VELVET 06",
	            "m_product_id": 10689097,
	            "name": "LIP&KISS VELVET 06",
	            "qtyonhand": 538,
	            "qtysold": 106,
	            "salesvalue": 31694.00
	        },
	        {
	            "cogs": 235.20,
	            "description": "30 ML PERFUME SUNNY VIBES",
	            "m_product_id": 10910817,
	            "name": "30MLSUNNYVIBES",
	            "qtyonhand": 411,
	            "qtysold": 105,
	            "salesvalue": 49101.60
	        },
	        {
	            "cogs": 247.20,
	            "description": "LIP&KISS LE ROUGE VELOURS 01",
	            "m_product_id": 10704948,
	            "name": "LIP&KISS RALVEL 01",
	            "qtyonhand": 325,
	            "qtysold": 103,
	            "salesvalue": 32834.20
	        },
	        {
	            "cogs": 100.98,
	            "description": "VERNIS A ONGLES PRO NAIL COLOUR 013",
	            "m_product_id": 10793651,
	            "name": "104003122013",
	            "qtyonhand": 374,
	            "qtysold": 102,
	            "salesvalue": 22980.15
	        },
	        {
	            "cogs": 228.48,
	            "description": "30 ML PERFUME WILD MUSK----",
	            "m_product_id": 10688852,
	            "name": "30MLWILD MUSK",
	            "qtyonhand": 1019,
	            "qtysold": 102,
	            "salesvalue": 48602.60
	        },
	        {
	            "cogs": 338.10,
	            "description": "COVERPLUS FOND DE TEINT COMPACT MAT",
	            "m_product_id": 10778312,
	            "name": "109151003002",
	            "qtyonhand": 426,
	            "qtysold": 98,
	            "salesvalue": 63189.60
	        },
	        {
	            "cogs": 247.95,
	            "description": "Masc volume MAXIMEYES",
	            "m_product_id": 10909921,
	            "name": "110157005001",
	            "qtyonhand": 234,
	            "qtysold": 95,
	            "salesvalue": 46506.80
	        },
	        {
	            "cogs": 210.56,
	            "description": "30 ML PERFUME PATCHOULY ROSE",
	            "m_product_id": 10688974,
	            "name": "30MLPATCHOULY ROSE",
	            "qtyonhand": 519,
	            "qtysold": 94,
	            "salesvalue": 43712.40
	        },
	        {
	            "cogs": 210.56,
	            "description": "30MLMIDN HER NP",
	            "m_product_id": 10865262,
	            "name": "30MLMIDN HER NP",
	            "qtyonhand": 465,
	            "qtysold": 94,
	            "salesvalue": 44510.80
	        },
	        {
	            "cogs": 157.17,
	            "description": "SHOWER GEL V2  FAIRYLAND",
	            "m_product_id": 10929982,
	            "name": "GD 250FAIRYLA02",
	            "qtyonhand": 209,
	            "qtysold": 93,
	            "salesvalue": 30383.15
	        },
	        {
	            "cogs": 426.88,
	            "description": "100MLFAIRYLANDP",
	            "m_product_id": 10878449,
	            "name": "100MLFAIRYLANDP",
	            "qtyonhand": 237,
	            "qtysold": 92,
	            "salesvalue": 88861.05
	        },
	        {
	            "cogs": 372.60,
	            "description": "GIFT SET FRENCH BARBER 30",
	            "m_product_id": 10934104,
	            "name": "205223034001",
	            "qtyonhand": 57,
	            "qtysold": 92,
	            "salesvalue": 72908.75
	        },
	        {
	            "cogs": 249.34,
	            "description": "LIQUID FOUNDATION N°3",
	            "m_product_id": 10689584,
	            "name": "COVERFIT FDTFLU 03",
	            "qtyonhand": 286,
	            "qtysold": 91,
	            "salesvalue": 35580.60
	        },
	        {
	            "cogs": 39.16,
	            "description": "HAIR CLIP IN PLASTIC BLACK--",
	            "m_product_id": 10691665,
	            "name": "1736734N",
	            "qtyonhand": 31,
	            "qtysold": 89,
	            "salesvalue": 8638.50
	        },
	        {
	            "cogs": 127.60,
	            "description": "LIP & KISS VELVET 01",
	            "m_product_id": 10689031,
	            "name": "LIP&KISS VELVET 01",
	            "qtyonhand": 381,
	            "qtysold": 88,
	            "salesvalue": 26312.00
	        },
	        {
	            "cogs": 107.88,
	            "description": "EDP 10ML LADY GLITTERP 20",
	            "m_product_id": 10921009,
	            "name": "10MLLADYGLITT02",
	            "qtyonhand": 20,
	            "qtysold": 87,
	            "salesvalue": 24049.80
	        },
	        {
	            "cogs": 934.82,
	            "description": "TROU30 FAIR PF",
	            "m_product_id": 10919459,
	            "name": "205223001126",
	            "qtyonhand": 41,
	            "qtysold": 86,
	            "salesvalue": 94514.00
	        },
	        {
	            "cogs": 191.25,
	            "description": "COVERCORRECT CORRECTEUR LIQUIDE HAU",
	            "m_product_id": 10738099,
	            "name": "109150001006",
	            "qtyonhand": 591,
	            "qtysold": 85,
	            "salesvalue": 40219.40
	        },
	        {
	            "cogs": 83.16,
	            "description": "VAO PRO NAIL COLOUR 034--",
	            "m_product_id": 10776743,
	            "name": "104003122034",
	            "qtyonhand": 314,
	            "qtysold": 84,
	            "salesvalue": 18915.40
	        },
	        {
	            "cogs": 224.10,
	            "description": "MAXIMEYES WATERPROOF",
	            "m_product_id": 10909922,
	            "name": "110157005002",
	            "qtyonhand": 345,
	            "qtysold": 83,
	            "salesvalue": 40718.40
	        },
	        {
	            "cogs": 185.92,
	            "description": "30 ML PERFUME BLACK PEPPER JASMINE",
	            "m_product_id": 10688932,
	            "name": "30MLPOIVRE JASMIN",
	            "qtyonhand": 922,
	            "qtysold": 83,
	            "salesvalue": 38622.60
	        },
	        {
	            "cogs": 140.27,
	            "description": "Shower gel Extreme sensat",
	            "m_product_id": 10921031,
	            "name": "806029801001",
	            "qtyonhand": 289,
	            "qtysold": 83,
	            "salesvalue": 27027.35
	        },
	        {
	            "cogs": 101.68,
	            "description": "EDP 10ML FAIRYLANDP",
	            "m_product_id": 10921000,
	            "name": "10MLFAIRYLAND",
	            "qtyonhand": 19,
	            "qtysold": 82,
	            "salesvalue": 22487.40
	        },
	        {
	            "cogs": 141.86,
	            "description": "Blush touch powder blush 05----",
	            "m_product_id": 10690288,
	            "name": "109148100005",
	            "qtyonhand": 577,
	            "qtysold": 82,
	            "salesvalue": 32239.20
	        },
	        {
	            "cogs": 138.58,
	            "description": "SHOWER GELV2 BLUESUIT",
	            "m_product_id": 10929974,
	            "name": "GD 250 BLUESU03",
	            "qtyonhand": 141,
	            "qtysold": 82,
	            "salesvalue": 26484.50
	        },
	        {
	            "cogs": 221.94,
	            "description": "LIQUID FOUNDATION N°5",
	            "m_product_id": 10689617,
	            "name": "COVERFIT FDTFLU 05",
	            "qtyonhand": 505,
	            "qtysold": 81,
	            "salesvalue": 30077.80
	        },
	        {
	            "cogs": 100.44,
	            "description": "EDP 10ML YUMMY CANDY",
	            "m_product_id": 10921002,
	            "name": "10MLYUMMYCANDY",
	            "qtyonhand": 33,
	            "qtysold": 81,
	            "salesvalue": 22152.60
	        },
	        {
	            "cogs": 140.13,
	            "description": "BRUSH CORRECTOR 04",
	            "m_product_id": 10689615,
	            "name": "CORLUMIERE04 ADOPT",
	            "qtyonhand": 883,
	            "qtysold": 81,
	            "salesvalue": 29047.20
	        },
	        {
	            "cogs": 60.00,
	            "description": "PENCIL SHARPENER ADOPT----",
	            "m_product_id": 10713106,
	            "name": "1356798",
	            "qtyonhand": 787,
	            "qtysold": 80,
	            "salesvalue": 14320.00
	        },
	        {
	            "cogs": 112.00,
	            "description": "EARRING IN METAL",
	            "m_product_id": 10813860,
	            "name": "1095C4A17-D",
	            "qtyonhand": 0,
	            "qtysold": 80,
	            "salesvalue": 15960.00
	        },
	        {
	            "cogs": 99.20,
	            "description": "EDP 10ML STARNIGHT",
	            "m_product_id": 10921013,
	            "name": "10MLSTARNIGHT",
	            "qtyonhand": 34,
	            "qtysold": 80,
	            "salesvalue": 22096.80
	        },
	        {
	            "cogs": 121.60,
	            "description": "NAIL POLISH REMOVER",
	            "m_product_id": 10929811,
	            "name": "104118001001",
	            "qtyonhand": 408,
	            "qtysold": 80,
	            "salesvalue": 21483.00
	        },
	        {
	            "cogs": 172.38,
	            "description": "Mascara volume LASH DELIR",
	            "m_product_id": 10909923,
	            "name": "110157006001",
	            "qtyonhand": 330,
	            "qtysold": 78,
	            "salesvalue": 37524.80
	        },
	        {
	            "cogs": 131.82,
	            "description": "Shower gel L'atout charme",
	            "m_product_id": 10921028,
	            "name": "806028801001",
	            "qtyonhand": 169,
	            "qtysold": 78,
	            "salesvalue": 25662.00
	        }
	    ],
	    "purchase_pricelist_id": 10012942,
	    "sales_pricelist_id": 10012941,
	    "stockLevels": [
	        {
	            "endingStock": 76486,
	            "levels": [
	                59163,
	                65799,
	                72783,
	                75081,
	                76489,
	                76486
	            ],
	            "movements": [
	                6636,
	                6984,
	                2298,
	                1408,
	                -3,
	                0
	            ],
	            "startingStock": 59163,
	            "store": "Adopt Bagatelle"
	        },
	        {
	            "endingStock": 12072,
	            "levels": [
	                0,
	                0,
	                0,
	                0,
	                12072,
	                12072
	            ],
	            "movements": [
	                0,
	                0,
	                0,
	                12072,
	                0,
	                0
	            ],
	            "startingStock": 0,
	            "store": "Adopt Beau Plan"
	        },
	        {
	            "endingStock": 16649,
	            "levels": [
	                10193,
	                12569,
	                16495,
	                17230,
	                16649,
	                16649
	            ],
	            "movements": [
	                2376,
	                3926,
	                735,
	                -581,
	                0,
	                0
	            ],
	            "startingStock": 10193,
	            "store": "Adopt Cascavelle"
	        },
	        {
	            "endingStock": 39878,
	            "levels": [
	                28413,
	                32722,
	                38475,
	                39124,
	                39878,
	                39878
	            ],
	            "movements": [
	                4309,
	                5753,
	                649,
	                754,
	                0,
	                0
	            ],
	            "startingStock": 28413,
	            "store": "Adopt Jumbo Phoenix"
	        },
	        {
	            "endingStock": 25881,
	            "levels": [
	                19798,
	                22138,
	                24342,
	                25514,
	                25881,
	                25881
	            ],
	            "movements": [
	                2340,
	                2204,
	                1172,
	                367,
	                0,
	                0
	            ],
	            "startingStock": 19798,
	            "store": "Adopt Jumbo Riche Terre"
	        },
	        {
	            "endingStock": 26254,
	            "levels": [
	                21327,
	                23417,
	                26190,
	                26646,
	                26254,
	                26254
	            ],
	            "movements": [
	                2090,
	                2773,
	                456,
	                -392,
	                0,
	                0
	            ],
	            "startingStock": 21327,
	            "store": "Adopt La Croisette"
	        },
	        {
	            "endingStock": 30675,
	            "levels": [
	                23820,
	                26210,
	                30820,
	                31723,
	                30675,
	                30675
	            ],
	            "movements": [
	                2390,
	                4610,
	                903,
	                -1048,
	                0,
	                0
	            ],
	            "startingStock": 23820,
	            "store": "Adopt Plaisance"
	        },
	        {
	            "endingStock": 38270,
	            "levels": [
	                29201,
	                33047,
	                38301,
	                39224,
	                38270,
	                38270
	            ],
	            "movements": [
	                3846,
	                5254,
	                923,
	                -954,
	                0,
	                0
	            ],
	            "startingStock": 29201,
	            "store": "Adopt Port Louis"
	        },
	        {
	            "endingStock": 39132,
	            "levels": [
	                31049,
	                34076,
	                37764,
	                38245,
	                39132,
	                39132
	            ],
	            "movements": [
	                3027,
	                3688,
	                481,
	                887,
	                0,
	                0
	            ],
	            "startingStock": 31049,
	            "store": "Adopt Super U Grand Baie"
	        },
	        {
	            "endingStock": 36521,
	            "levels": [
	                25183,
	                28777,
	                33686,
	                35994,
	                36521,
	                36521
	            ],
	            "movements": [
	                3594,
	                4909,
	                2308,
	                527,
	                0,
	                0
	            ],
	            "startingStock": 25183,
	            "store": "Adopt Trianon"
	        },
	        {
	            "endingStock": 122243,
	            "levels": [
	                102107,
	                118571,
	                123855,
	                141853,
	                122243,
	                122243
	            ],
	            "movements": [
	                16464,
	                5284,
	                17998,
	                -19610,
	                0,
	                0
	            ],
	            "startingStock": 102107,
	            "store": "Warehouse"
	        }
	    ],
	    "topGroupList": {
	        "group1": [
	            {
	                "cost": 113717.12,
	                "group": "EDP 30ML",
	                "qty": 50328.00,
	                "value": 25113173.00
	            },
	            {
	                "cost": 98919.02,
	                "group": "TEINT",
	                "qty": 32639.00,
	                "value": 20007087.50
	            },
	            {
	                "cost": 73127.67,
	                "group": "LEVRES",
	                "qty": 37420.00,
	                "value": 14773836.50
	            },
	            {
	                "cost": 40444.15,
	                "group": "BOUCLE D OREILLE",
	                "qty": 16751.00,
	                "value": 7911685.00
	            },
	            {
	                "cost": 39752.28,
	                "group": "EDP 100ML",
	                "qty": 8543.00,
	                "value": 8376615.00
	            },
	            {
	                "cost": 23913.18,
	                "group": "BRACELET",
	                "qty": 8617.00,
	                "value": 4718932.00
	            },
	            {
	                "cost": 23583.77,
	                "group": "YEUX",
	                "qty": 15620.00,
	                "value": 4666801.00
	            },
	            {
	                "cost": 19751.75,
	                "group": "COFFRETS",
	                "qty": 3924.00,
	                "value": 2521109.50
	            },
	            {
	                "cost": 19680.51,
	                "group": "LAVANT",
	                "qty": 11497.00,
	                "value": 3772848.50
	            },
	            {
	                "cost": 17269.40,
	                "group": "ONGLES",
	                "qty": 15840.00,
	                "value": 3696587.00
	            },
	            {
	                "cost": 16846.81,
	                "group": "PINCEAUX",
	                "qty": 6055.00,
	                "value": 3405757.00
	            },
	            {
	                "cost": 16430.40,
	                "group": "COLLIER",
	                "qty": 4678.00,
	                "value": 3146901.50
	            },
	            {
	                "cost": 12471.33,
	                "group": "BAGUE",
	                "qty": 4037.00,
	                "value": 2334064.00
	            },
	            {
	                "cost": 10863.11,
	                "group": "PALETTES",
	                "qty": 3025.00,
	                "value": 1981290.50
	            },
	            {
	                "cost": 10057.57,
	                "group": "HYDRATANT",
	                "qty": 4249.00,
	                "value": 2007320.50
	            },
	            {
	                "cost": 9663.26,
	                "group": "ACCESSOIRES COSMETIQUES",
	                "qty": 8007.00,
	                "value": 1985080.00
	            },
	            {
	                "cost": 8923.16,
	                "group": "ACCESSOIRES CHEVEUX",
	                "qty": 5831.00,
	                "value": 1682535.50
	            },
	            {
	                "cost": 7115.54,
	                "group": "SOINS MAINS",
	                "qty": 6063.00,
	                "value": 1388427.00
	            },
	            {
	                "cost": 6610.18,
	                "group": "SAC",
	                "qty": 673.00,
	                "value": 1208104.50
	            },
	            {
	                "cost": 5804.37,
	                "group": "SOURCILS",
	                "qty": 4252.00,
	                "value": 1327522.00
	            },
	            {
	                "cost": 4979.21,
	                "group": "TEXTILE",
	                "qty": 1458.00,
	                "value": 989085.50
	            },
	            {
	                "cost": 3486.84,
	                "group": "BOUGIE",
	                "qty": 1081.00,
	                "value": 589816.50
	            },
	            {
	                "cost": 3352.03,
	                "group": "MAROQUINERIE",
	                "qty": 1009.00,
	                "value": 629126.50
	            },
	            {
	                "cost": 3218.82,
	                "group": "DEMAQUILLANTS",
	                "qty": 1435.00,
	                "value": 456749.00
	            },
	            {
	                "cost": 2706.33,
	                "group": "ACIER",
	                "qty": 971.00,
	                "value": 435770.00
	            },
	            {
	                "cost": 1943.66,
	                "group": "EXFOLIANTS",
	                "qty": 662.00,
	                "value": 314898.00
	            },
	            {
	                "cost": 1788.05,
	                "group": "LUNETTE",
	                "qty": 423.00,
	                "value": 263862.00
	            },
	            {
	                "cost": 1607.88,
	                "group": "SOIN VISAGE",
	                "qty": 578.00,
	                "value": 209590.00
	            },
	            {
	                "cost": 1311.24,
	                "group": "COLOGNE",
	                "qty": 294.00,
	                "value": 293265.00
	            },
	            {
	                "cost": 1249.46,
	                "group": "BIJOUX PRECIEUX",
	                "qty": 240.00,
	                "value": 176712.50
	            },
	            {
	                "cost": 1177.92,
	                "group": "EDP 10ML",
	                "qty": 1073.00,
	                "value": 254872.50
	            },
	            {
	                "cost": 979.16,
	                "group": "DERIVES PARFUMES",
	                "qty": 350.00,
	                "value": 153642.00
	            },
	            {
	                "cost": 914.43,
	                "group": "EMBALLAGE",
	                "qty": 717.00,
	                "value": 47567.00
	            },
	            {
	                "cost": 862.45,
	                "group": "GADGET",
	                "qty": 399.00,
	                "value": 121507.50
	            },
	            {
	                "cost": 851.12,
	                "group": "ACCESSOIRES PARFUM",
	                "qty": 359.00,
	                "value": 114552.50
	            },
	            {
	                "cost": 772.83,
	                "group": "BOUGIES",
	                "qty": 93.00,
	                "value": 92767.50
	            },
	            {
	                "cost": 502.81,
	                "group": "PARAPLUIE",
	                "qty": 122.00,
	                "value": 59995.00
	            },
	            {
	                "cost": 478.17,
	                "group": "ACCESSOIRES COSMETIQUE",
	                "qty": 246.00,
	                "value": 85854.00
	            },
	            {
	                "cost": 430.28,
	                "group": "COFFRET",
	                "qty": 195.00,
	                "value": 33208.00
	            },
	            {
	                "cost": 348.42,
	                "group": "ECLAT",
	                "qty": 252.00,
	                "value": 61821.50
	            },
	            {
	                "cost": 327.00,
	                "group": "NOURISSANT",
	                "qty": 194.00,
	                "value": 46495.00
	            },
	            {
	                "cost": 294.30,
	                "group": "NOURRISSANT",
	                "qty": 195.00,
	                "value": 60855.00
	            },
	            {
	                "cost": 284.96,
	                "group": "EDP30ML",
	                "qty": 137.00,
	                "value": 34250.00
	            },
	            {
	                "cost": 278.72,
	                "group": "ANTI-POLLUTION",
	                "qty": 189.00,
	                "value": 56781.00
	            },
	            {
	                "cost": 247.24,
	                "group": "PURIFIANT",
	                "qty": 188.00,
	                "value": 52502.00
	            },
	            {
	                "cost": 244.00,
	                "group": "APAISANT",
	                "qty": 170.00,
	                "value": 50540.00
	            },
	            {
	                "cost": 183.08,
	                "group": "",
	                "qty": 31.00,
	                "value": 21469.00
	            },
	            {
	                "cost": 128.16,
	                "group": "COLIER",
	                "qty": 36.00,
	                "value": 26964.00
	            },
	            {
	                "cost": 95.48,
	                "group": "LAINAGE",
	                "qty": 28.00,
	                "value": 20972.00
	            },
	            {
	                "cost": 86.07,
	                "group": "ACCESOIRES PARFUM",
	                "qty": 57.00,
	                "value": 9063.00
	            },
	            {
	                "cost": 68.14,
	                "group": "DIVERS",
	                "qty": 50.00,
	                "value": 13442.00
	            },
	            {
	                "cost": 38.74,
	                "group": "COFFRET MODE",
	                "qty": 17.00,
	                "value": 6470.00
	            },
	            {
	                "cost": 18.62,
	                "group": "HYDRADANT",
	                "qty": 19.00,
	                "value": 4351.00
	            },
	            {
	                "cost": 14.97,
	                "group": "null",
	                "qty": 25.00,
	                "value": 3993.00
	            },
	            {
	                "cost": 10.86,
	                "group": "MERCHANDISING",
	                "qty": 3.00,
	                "value": 0.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Coffret",
	                "qty": 280.00,
	                "value": 19880.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Fournitures",
	                "qty": 51.00,
	                "value": 5100.00
	            }
	        ],
	        "group2": [
	            {
	                "cost": 71151.16,
	                "group": "30ML FEMME",
	                "qty": 31692.00,
	                "value": 15813809.00
	            },
	            {
	                "cost": 57358.44,
	                "group": "FOND DE TEINT",
	                "qty": 13732.00,
	                "value": 11088224.50
	            },
	            {
	                "cost": 25686.49,
	                "group": "30ML HOMME",
	                "qty": 11455.00,
	                "value": 5716045.00
	            },
	            {
	                "cost": 24789.62,
	                "group": "RAL MAT",
	                "qty": 10914.00,
	                "value": 4833846.00
	            },
	            {
	                "cost": 18728.48,
	                "group": "BRACELET ACIER",
	                "qty": 5972.00,
	                "value": 3683373.00
	            },
	            {
	                "cost": 18242.73,
	                "group": "BOUCLE D OREILLE ACIER",
	                "qty": 5415.00,
	                "value": 3620444.50
	            },
	            {
	                "cost": 16618.55,
	                "group": "100ML HOMME",
	                "qty": 3569.00,
	                "value": 3565431.00
	            },
	            {
	                "cost": 15844.59,
	                "group": "30ML PAILLETTES",
	                "qty": 6719.00,
	                "value": 3352781.00
	            },
	            {
	                "cost": 15740.75,
	                "group": "COFFRET MULTI",
	                "qty": 2588.00,
	                "value": 2085388.50
	            },
	            {
	                "cost": 14785.73,
	                "group": "100ML FEMME",
	                "qty": 3173.00,
	                "value": 3169827.00
	            },
	            {
	                "cost": 14180.78,
	                "group": "RAL CLASSIQUE",
	                "qty": 7181.00,
	                "value": 2738419.00
	            },
	            {
	                "cost": 14144.23,
	                "group": "COLLIER ACIER",
	                "qty": 4121.00,
	                "value": 2741294.50
	            },
	            {
	                "cost": 12275.84,
	                "group": "PENDANTE",
	                "qty": 5533.00,
	                "value": 2368160.50
	            },
	            {
	                "cost": 11882.79,
	                "group": "RAL LIQUIDE",
	                "qty": 5945.00,
	                "value": 2642240.50
	            },
	            {
	                "cost": 11732.48,
	                "group": "BAGUE ACIER",
	                "qty": 3876.00,
	                "value": 2193610.00
	            },
	            {
	                "cost": 11415.68,
	                "group": "PINCEAUX TEINT",
	                "qty": 3298.00,
	                "value": 2209926.00
	            },
	            {
	                "cost": 11140.03,
	                "group": "BRONZER",
	                "qty": 3383.00,
	                "value": 1926788.00
	            },
	            {
	                "cost": 10941.02,
	                "group": "POUDRES",
	                "qty": 5498.00,
	                "value": 2743502.00
	            },
	            {
	                "cost": 10520.52,
	                "group": "CORRECTEURS ANTICERNES",
	                "qty": 5478.00,
	                "value": 2314828.50
	            },
	            {
	                "cost": 9948.91,
	                "group": "GEL DOUCHE CLASSIQUE",
	                "qty": 5843.00,
	                "value": 1924567.00
	            },
	            {
	                "cost": 9726.27,
	                "group": "PALETTE YEUX",
	                "qty": 2489.00,
	                "value": 1711423.00
	            },
	            {
	                "cost": 9418.89,
	                "group": "VAO LAQUE",
	                "qty": 9507.00,
	                "value": 2107283.50
	            },
	            {
	                "cost": 8505.51,
	                "group": "CRAYONS LEVRES",
	                "qty": 6384.00,
	                "value": 1838499.50
	            },
	            {
	                "cost": 7622.06,
	                "group": "LAIT",
	                "qty": 3345.00,
	                "value": 1627418.50
	            },
	            {
	                "cost": 7049.82,
	                "group": "MASCARA",
	                "qty": 3461.00,
	                "value": 1445037.00
	            },
	            {
	                "cost": 6957.42,
	                "group": "100ML PAILLETTES",
	                "qty": 1501.00,
	                "value": 1499499.00
	            },
	            {
	                "cost": 6924.83,
	                "group": "CRAYONS YEUX",
	                "qty": 4875.00,
	                "value": 1125174.50
	            },
	            {
	                "cost": 5601.00,
	                "group": "CREME MAIN HYDRATANTE",
	                "qty": 4641.00,
	                "value": 1062789.00
	            },
	            {
	                "cost": 5523.06,
	                "group": "RAL LAQUE",
	                "qty": 3025.00,
	                "value": 1206460.00
	            },
	            {
	                "cost": 4930.45,
	                "group": "BLUSH",
	                "qty": 2763.00,
	                "value": 1099104.00
	            },
	            {
	                "cost": 4850.08,
	                "group": "GEL DOUCHE CLASIQUES",
	                "qty": 2738.00,
	                "value": 900802.00
	            },
	            {
	                "cost": 4835.16,
	                "group": "SAC",
	                "qty": 777.00,
	                "value": 863799.00
	            },
	            {
	                "cost": 4806.34,
	                "group": "CREOLE",
	                "qty": 2828.00,
	                "value": 924092.00
	            },
	            {
	                "cost": 4638.92,
	                "group": "CRAYONS SOURCILS",
	                "qty": 3537.00,
	                "value": 1016037.00
	            },
	            {
	                "cost": 4534.62,
	                "group": "GLOSS",
	                "qty": 2463.00,
	                "value": 958946.50
	            },
	            {
	                "cost": 4214.67,
	                "group": "OAP COMPACTE",
	                "qty": 3797.00,
	                "value": 865575.50
	            },
	            {
	                "cost": 4106.51,
	                "group": "PUCE",
	                "qty": 2342.00,
	                "value": 779353.50
	            },
	            {
	                "cost": 4044.56,
	                "group": "PINCEAUX YEUX",
	                "qty": 1995.00,
	                "value": 883156.50
	            },
	            {
	                "cost": 3992.12,
	                "group": "DEMAQUILLANTS",
	                "qty": 1828.00,
	                "value": 577657.50
	            },
	            {
	                "cost": 3698.07,
	                "group": "VAO A EFFET",
	                "qty": 3277.00,
	                "value": 798238.50
	            },
	            {
	                "cost": 3365.16,
	                "group": "SIGNATURE BG",
	                "qty": 1068.00,
	                "value": 570349.00
	            },
	            {
	                "cost": 3211.57,
	                "group": "RAL BRILLANT",
	                "qty": 1105.00,
	                "value": 442988.00
	            },
	            {
	                "cost": 2997.88,
	                "group": "MANUCURE",
	                "qty": 3392.00,
	                "value": 636389.00
	            },
	            {
	                "cost": 2920.38,
	                "group": "OAP STICK",
	                "qty": 1907.00,
	                "value": 727147.00
	            },
	            {
	                "cost": 2866.35,
	                "group": "ECHARPE",
	                "qty": 770.00,
	                "value": 549230.00
	            },
	            {
	                "cost": 2776.34,
	                "group": "SAVONS",
	                "qty": 1724.00,
	                "value": 494088.50
	            },
	            {
	                "cost": 2634.20,
	                "group": "REGARD",
	                "qty": 1766.00,
	                "value": 537597.50
	            },
	            {
	                "cost": 2517.00,
	                "group": "FEELING BOX",
	                "qty": 1471.00,
	                "value": 214681.00
	            },
	            {
	                "cost": 2474.07,
	                "group": "EYELINERS",
	                "qty": 1580.00,
	                "value": 503867.00
	            },
	            {
	                "cost": 2440.43,
	                "group": "null",
	                "qty": 838.00,
	                "value": 381489.00
	            },
	            {
	                "cost": 2416.58,
	                "group": "TROUSSE ou VANITY",
	                "qty": 743.00,
	                "value": 455073.50
	            },
	            {
	                "cost": 2303.49,
	                "group": "BASE / SPRAY",
	                "qty": 1154.00,
	                "value": 537088.00
	            },
	            {
	                "cost": 2247.45,
	                "group": "BRACELET LOT",
	                "qty": 1373.00,
	                "value": 465121.00
	            },
	            {
	                "cost": 2077.16,
	                "group": "BAGUE",
	                "qty": 700.00,
	                "value": 337055.00
	            },
	            {
	                "cost": 2063.19,
	                "group": "BRACELET RIGIDE",
	                "qty": 827.00,
	                "value": 394745.00
	            },
	            {
	                "cost": 1941.65,
	                "group": "ELASTIQUE",
	                "qty": 1236.00,
	                "value": 375831.00
	            },
	            {
	                "cost": 1897.33,
	                "group": "CHEVEUX",
	                "qty": 1073.00,
	                "value": 366718.00
	            },
	            {
	                "cost": 1813.74,
	                "group": "FOULARD IMPRIME",
	                "qty": 586.00,
	                "value": 374368.00
	            },
	            {
	                "cost": 1806.84,
	                "group": "PINCE",
	                "qty": 1768.00,
	                "value": 336654.50
	            },
	            {
	                "cost": 1788.05,
	                "group": "LUNETTE DE SOLEIL",
	                "qty": 423.00,
	                "value": 263862.00
	            },
	            {
	                "cost": 1728.56,
	                "group": "SHAMPOING",
	                "qty": 697.00,
	                "value": 312953.00
	            },
	            {
	                "cost": 1718.82,
	                "group": "EPONGE",
	                "qty": 1708.00,
	                "value": 365492.50
	            },
	            {
	                "cost": 1717.07,
	                "group": "SAUTOIR",
	                "qty": 393.00,
	                "value": 295313.00
	            },
	            {
	                "cost": 1514.54,
	                "group": "CREME MAINS HYDRATANTE",
	                "qty": 1422.00,
	                "value": 325638.00
	            },
	            {
	                "cost": 1463.16,
	                "group": "BB ET CC CREMES",
	                "qty": 520.00,
	                "value": 254944.00
	            },
	            {
	                "cost": 1430.48,
	                "group": "GOMMAGE",
	                "qty": 497.00,
	                "value": 270429.00
	            },
	            {
	                "cost": 1311.24,
	                "group": "COLOGNE FEMME",
	                "qty": 294.00,
	                "value": 293265.00
	            },
	            {
	                "cost": 1232.20,
	                "group": "BIJOUX ARGENT",
	                "qty": 235.00,
	                "value": 176212.50
	            },
	            {
	                "cost": 1179.46,
	                "group": "DISSOLVANT",
	                "qty": 668.00,
	                "value": 178654.50
	            },
	            {
	                "cost": 1165.45,
	                "group": "MASCARA SOURCILS",
	                "qty": 715.00,
	                "value": 311485.00
	            },
	            {
	                "cost": 1157.66,
	                "group": "BANDOULIERE",
	                "qty": 145.00,
	                "value": 202986.00
	            },
	            {
	                "cost": 1128.00,
	                "group": "POCHETTE",
	                "qty": 329.00,
	                "value": 199722.50
	            },
	            {
	                "cost": 1113.44,
	                "group": "10ML FEMME",
	                "qty": 1021.00,
	                "value": 242002.50
	            },
	            {
	                "cost": 1090.96,
	                "group": "SOIN ONGLES",
	                "qty": 773.00,
	                "value": 224415.00
	            },
	            {
	                "cost": 1025.15,
	                "group": "PALETTE SOURCILS",
	                "qty": 505.00,
	                "value": 251995.00
	            },
	            {
	                "cost": 1003.05,
	                "group": "COFFRET EDP100ML",
	                "qty": 135.00,
	                "value": 202365.00
	            },
	            {
	                "cost": 919.16,
	                "group": "VAO NACRE",
	                "qty": 921.00,
	                "value": 202941.00
	            },
	            {
	                "cost": 895.89,
	                "group": "MASQUES TISSU",
	                "qty": 433.00,
	                "value": 127281.50
	            },
	            {
	                "cost": 854.68,
	                "group": "LOT BOUCLE D OREILLE",
	                "qty": 595.00,
	                "value": 199865.50
	            },
	            {
	                "cost": 850.31,
	                "group": "DIVERS COS",
	                "qty": 372.00,
	                "value": 141485.00
	            },
	            {
	                "cost": 848.80,
	                "group": "FANTAISIE",
	                "qty": 396.00,
	                "value": 142358.00
	            },
	            {
	                "cost": 802.62,
	                "group": "MASQUE TISSU",
	                "qty": 819.00,
	                "value": 187551.00
	            },
	            {
	                "cost": 798.00,
	                "group": "LAIT CORP",
	                "qty": 266.00,
	                "value": 132734.00
	            },
	            {
	                "cost": 792.77,
	                "group": "BARRETTE",
	                "qty": 395.00,
	                "value": 151280.50
	            },
	            {
	                "cost": 772.83,
	                "group": "EDITION SPECIAL BG",
	                "qty": 93.00,
	                "value": 92767.50
	            },
	            {
	                "cost": 750.59,
	                "group": "BRACELET SOUPLE",
	                "qty": 293.00,
	                "value": 147115.50
	            },
	            {
	                "cost": 657.11,
	                "group": "BEURRE",
	                "qty": 191.00,
	                "value": 70577.50
	            },
	            {
	                "cost": 653.13,
	                "group": "PORTE FEUILLE",
	                "qty": 178.00,
	                "value": 139776.50
	            },
	            {
	                "cost": 645.00,
	                "group": "PAILETTES CORPS",
	                "qty": 172.00,
	                "value": 111628.00
	            },
	            {
	                "cost": 643.50,
	                "group": "NETTOYANTS",
	                "qty": 195.00,
	                "value": 77512.50
	            },
	            {
	                "cost": 630.69,
	                "group": "MASQUE CREME",
	                "qty": 228.00,
	                "value": 113772.00
	            },
	            {
	                "cost": 624.00,
	                "group": "BROSSE",
	                "qty": 351.00,
	                "value": 107402.00
	            },
	            {
	                "cost": 612.48,
	                "group": "ETUI ARGENT",
	                "qty": 176.00,
	                "value": 87560.00
	            },
	            {
	                "cost": 610.50,
	                "group": "DO IT YOURSELF",
	                "qty": 37.00,
	                "value": 36963.00
	            },
	            {
	                "cost": 603.26,
	                "group": "BRACELET CORDON",
	                "qty": 339.00,
	                "value": 119141.00
	            },
	            {
	                "cost": 592.79,
	                "group": "PINCEAUX SOURCILS",
	                "qty": 260.00,
	                "value": 136440.00
	            },
	            {
	                "cost": 569.94,
	                "group": "TROUSSE MAQ",
	                "qty": 180.00,
	                "value": 115295.00
	            },
	            {
	                "cost": 549.10,
	                "group": "DIVERS MODE",
	                "qty": 242.00,
	                "value": 115010.00
	            },
	            {
	                "cost": 548.82,
	                "group": "PARRURE",
	                "qty": 169.00,
	                "value": 115585.00
	            },
	            {
	                "cost": 529.76,
	                "group": "MANICURE",
	                "qty": 559.00,
	                "value": 121551.00
	            },
	            {
	                "cost": 527.94,
	                "group": "BASE ET TOP COAT ONGLES",
	                "qty": 432.00,
	                "value": 116748.00
	            },
	            {
	                "cost": 515.46,
	                "group": "FOULARD UNI",
	                "qty": 176.00,
	                "value": 109339.50
	            },
	            {
	                "cost": 499.72,
	                "group": "BASE LEVRES",
	                "qty": 403.00,
	                "value": 112437.00
	            },
	            {
	                "cost": 483.00,
	                "group": "PARAPLUIE IMPRIME",
	                "qty": 115.00,
	                "value": 57212.50
	            },
	            {
	                "cost": 471.52,
	                "group": "PINCEAUX LEVRES",
	                "qty": 288.00,
	                "value": 109834.00
	            },
	            {
	                "cost": 453.12,
	                "group": "BOITE CADEAU",
	                "qty": 192.00,
	                "value": 0.00
	            },
	            {
	                "cost": 429.81,
	                "group": "CRÃ\u0088ME",
	                "qty": 183.00,
	                "value": 81597.00
	            },
	            {
	                "cost": 368.14,
	                "group": "BOULE DE BAIN",
	                "qty": 389.00,
	                "value": 87544.00
	            },
	            {
	                "cost": 362.39,
	                "group": "COFFRET EDP30ML",
	                "qty": 166.00,
	                "value": 40050.00
	            },
	            {
	                "cost": 354.07,
	                "group": "PORTANT BIJOUX",
	                "qty": 150.00,
	                "value": 73771.50
	            },
	            {
	                "cost": 342.58,
	                "group": "GELEE",
	                "qty": 131.00,
	                "value": 64049.50
	            },
	            {
	                "cost": 341.90,
	                "group": "RAS DE COU",
	                "qty": 92.00,
	                "value": 58368.00
	            },
	            {
	                "cost": 341.48,
	                "group": "AUTRES",
	                "qty": 142.00,
	                "value": 17255.00
	            },
	            {
	                "cost": 308.20,
	                "group": "FRENCH MANICURE",
	                "qty": 134.00,
	                "value": 39530.00
	            },
	            {
	                "cost": 281.40,
	                "group": "PORTE CLES",
	                "qty": 105.00,
	                "value": 5250.00
	            },
	            {
	                "cost": 278.50,
	                "group": "BAGUE LOTS",
	                "qty": 80.00,
	                "value": 43820.00
	            },
	            {
	                "cost": 273.78,
	                "group": "GRANITE",
	                "qty": 81.00,
	                "value": 44469.00
	            },
	            {
	                "cost": 210.11,
	                "group": "UTILITAIRE",
	                "qty": 201.00,
	                "value": 31462.50
	            },
	            {
	                "cost": 200.87,
	                "group": "DORMEUSE",
	                "qty": 90.00,
	                "value": 38236.00
	            },
	            {
	                "cost": 193.10,
	                "group": "NETTOYANT PINCEAUX",
	                "qty": 137.00,
	                "value": 40887.00
	            },
	            {
	                "cost": 187.05,
	                "group": "HUILE",
	                "qty": 47.00,
	                "value": 30534.00
	            },
	            {
	                "cost": 183.08,
	                "group": "",
	                "qty": 31.00,
	                "value": 21469.00
	            },
	            {
	                "cost": 180.42,
	                "group": "MIROIR",
	                "qty": 141.00,
	                "value": 38079.00
	            },
	            {
	                "cost": 177.58,
	                "group": "SAC CUIR",
	                "qty": 12.00,
	                "value": 29388.00
	            },
	            {
	                "cost": 163.40,
	                "group": "CREMES VISAGE",
	                "qty": 38.00,
	                "value": 22705.00
	            },
	            {
	                "cost": 147.55,
	                "group": "HIGHLIGHTER",
	                "qty": 66.00,
	                "value": 22935.00
	            },
	            {
	                "cost": 147.04,
	                "group": "CREME",
	                "qty": 44.00,
	                "value": 21956.00
	            },
	            {
	                "cost": 145.02,
	                "group": "PLASTRON",
	                "qty": 31.00,
	                "value": 18770.00
	            },
	            {
	                "cost": 141.93,
	                "group": "ETUI PARFUM",
	                "qty": 106.00,
	                "value": 16854.00
	            },
	            {
	                "cost": 134.68,
	                "group": "EtUI  PU",
	                "qty": 91.00,
	                "value": 13422.50
	            },
	            {
	                "cost": 131.22,
	                "group": "COFFRET EDP 30ML",
	                "qty": 81.00,
	                "value": 11745.00
	            },
	            {
	                "cost": 128.16,
	                "group": "COLIER ACIER",
	                "qty": 36.00,
	                "value": 26964.00
	            },
	            {
	                "cost": 126.72,
	                "group": "VAO PAILLETTE",
	                "qty": 128.00,
	                "value": 28776.50
	            },
	            {
	                "cost": 121.68,
	                "group": "EDITION SPECIALE BG",
	                "qty": 13.00,
	                "value": 19467.50
	            },
	            {
	                "cost": 114.40,
	                "group": "SERUMS",
	                "qty": 22.00,
	                "value": 16445.00
	            },
	            {
	                "cost": 111.69,
	                "group": "PALETTE TEINT",
	                "qty": 31.00,
	                "value": 17872.50
	            },
	            {
	                "cost": 108.80,
	                "group": "GOMMAGE VISAGE",
	                "qty": 34.00,
	                "value": 13515.00
	            },
	            {
	                "cost": 107.59,
	                "group": "CLIP",
	                "qty": 44.00,
	                "value": 13727.50
	            },
	            {
	                "cost": 106.10,
	                "group": "TROUSSE VANITY",
	                "qty": 30.00,
	                "value": 3624.50
	            },
	            {
	                "cost": 106.08,
	                "group": "DIVERS GADGETS",
	                "qty": 51.00,
	                "value": 17799.00
	            },
	            {
	                "cost": 103.96,
	                "group": "EtUI  PARFUM",
	                "qty": 92.00,
	                "value": 13570.00
	            },
	            {
	                "cost": 103.40,
	                "group": "SOIN",
	                "qty": 22.00,
	                "value": 14245.00
	            },
	            {
	                "cost": 98.00,
	                "group": "BRUME",
	                "qty": 28.00,
	                "value": 10450.00
	            },
	            {
	                "cost": 79.58,
	                "group": "PINCEAUX ONGLES",
	                "qty": 46.00,
	                "value": 15985.00
	            },
	            {
	                "cost": 64.48,
	                "group": "10MLPAILLETTE",
	                "qty": 52.00,
	                "value": 12870.00
	            },
	            {
	                "cost": 58.80,
	                "group": "BASE TEINT",
	                "qty": 21.00,
	                "value": 8347.50
	            },
	            {
	                "cost": 55.56,
	                "group": "BASE /SPRAY",
	                "qty": 24.00,
	                "value": 11326.00
	            },
	            {
	                "cost": 49.58,
	                "group": "SET DE PINCEAUX",
	                "qty": 31.00,
	                "value": 9528.50
	            },
	            {
	                "cost": 35.10,
	                "group": "PENDANT",
	                "qty": 27.00,
	                "value": 7533.00
	            },
	            {
	                "cost": 29.04,
	                "group": "SAC PLAGE",
	                "qty": 6.00,
	                "value": 5541.00
	            },
	            {
	                "cost": 21.10,
	                "group": "COFFRET GADGET",
	                "qty": 10.00,
	                "value": 4020.00
	            },
	            {
	                "cost": 19.81,
	                "group": "PARAPLUIE UNI",
	                "qty": 7.00,
	                "value": 2782.50
	            },
	            {
	                "cost": 18.62,
	                "group": "MASQUES",
	                "qty": 19.00,
	                "value": 4351.00
	            },
	            {
	                "cost": 17.26,
	                "group": "PIERCING",
	                "qty": 5.00,
	                "value": 500.00
	            },
	            {
	                "cost": 16.92,
	                "group": "BOITRE CADEAU",
	                "qty": 9.00,
	                "value": 1755.00
	            },
	            {
	                "cost": 13.98,
	                "group": "PORTE MONNAIE",
	                "qty": 10.00,
	                "value": 500.00
	            },
	            {
	                "cost": 9.42,
	                "group": "AUTRES ELEMENT",
	                "qty": 2.00,
	                "value": 0.00
	            },
	            {
	                "cost": 8.48,
	                "group": "SHAMPOOING",
	                "qty": 106.00,
	                "value": 52894.00
	            },
	            {
	                "cost": 6.30,
	                "group": "SAVON LIQUIDE MAINS",
	                "qty": 1.00,
	                "value": 0.00
	            },
	            {
	                "cost": 4.90,
	                "group": "POCHON",
	                "qty": 14.00,
	                "value": 700.00
	            },
	            {
	                "cost": 1.44,
	                "group": "AUTRES ELEMENTS",
	                "qty": 1.00,
	                "value": 0.00
	            },
	            {
	                "cost": 0.00,
	                "group": "CHAINE DE CHEVILLE",
	                "qty": 0.00,
	                "value": 0.00
	            }
	        ],
	        "group3": [
	            {
	                "cost": 395471.59,
	                "group": "",
	                "qty": 157797.00,
	                "value": 78093118.50
	            },
	            {
	                "cost": 199268.78,
	                "group": "INT",
	                "qty": 96650.00,
	                "value": 41167280.00
	            },
	            {
	                "cost": 6181.10,
	                "group": "P18",
	                "qty": 2332.00,
	                "value": 1335991.50
	            },
	            {
	                "cost": 5300.86,
	                "group": "Promo Trianon May 19",
	                "qty": 1944.00,
	                "value": 659674.50
	            },
	            {
	                "cost": 2297.60,
	                "group": "PER",
	                "qty": 2231.00,
	                "value": 401641.50
	            },
	            {
	                "cost": 721.61,
	                "group": "Promo 50% Adopt Trianon",
	                "qty": 114.00,
	                "value": 46255.50
	            },
	            {
	                "cost": 627.55,
	                "group": "A17",
	                "qty": 374.00,
	                "value": 102113.00
	            },
	            {
	                "cost": 86.42,
	                "group": "P17",
	                "qty": 58.00,
	                "value": 13921.00
	            },
	            {
	                "cost": 85.85,
	                "group": "A14",
	                "qty": 51.00,
	                "value": 6615.00
	            },
	            {
	                "cost": 45.60,
	                "group": "DIV",
	                "qty": 19.00,
	                "value": 5605.00
	            },
	            {
	                "cost": 41.86,
	                "group": "P14",
	                "qty": 26.00,
	                "value": 6435.00
	            },
	            {
	                "cost": 30.97,
	                "group": "A15",
	                "qty": 29.00,
	                "value": 725.00
	            },
	            {
	                "cost": 15.00,
	                "group": "0",
	                "qty": 6.00,
	                "value": 870.00
	            },
	            {
	                "cost": 9.80,
	                "group": "E13",
	                "qty": 5.00,
	                "value": 125.00
	            },
	            {
	                "cost": 8.62,
	                "group": "A10",
	                "qty": 2.00,
	                "value": 50.00
	            },
	            {
	                "cost": 7.54,
	                "group": "E15",
	                "qty": 19.00,
	                "value": 1125.00
	            },
	            {
	                "cost": 6.07,
	                "group": "A11",
	                "qty": 4.00,
	                "value": 175.00
	            },
	            {
	                "cost": 4.70,
	                "group": "E14",
	                "qty": 1.00,
	                "value": 747.50
	            },
	            {
	                "cost": 3.93,
	                "group": "H15",
	                "qty": 3.00,
	                "value": 75.00
	            },
	            {
	                "cost": 3.66,
	                "group": "E18",
	                "qty": 2.00,
	                "value": 699.00
	            },
	            {
	                "cost": 2.97,
	                "group": "H13",
	                "qty": 3.00,
	                "value": 75.00
	            },
	            {
	                "cost": 1.98,
	                "group": "P16",
	                "qty": 2.00,
	                "value": 50.00
	            },
	            {
	                "cost": 1.96,
	                "group": "A13",
	                "qty": 4.00,
	                "value": 100.00
	            },
	            {
	                "cost": 0.00,
	                "group": "A16",
	                "qty": 0.00,
	                "value": 0.00
	            },
	            {
	                "cost": 0.00,
	                "group": "H17",
	                "qty": 0.00,
	                "value": 0.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Promo scarves-50%",
	                "qty": 0.00,
	                "value": 0.00
	            }
	        ],
	        "group4": [
	            {
	                "cost": 200425.54,
	                "group": "2021Mar31",
	                "qty": 88511.00,
	                "value": 42239968.50
	            },
	            {
	                "cost": 151806.85,
	                "group": "Current",
	                "qty": 73285.00,
	                "value": 30886942.00
	            },
	            {
	                "cost": 42381.01,
	                "group": "31-May-21",
	                "qty": 16171.00,
	                "value": 8229510.00
	            },
	            {
	                "cost": 30960.48,
	                "group": "2021Mar03",
	                "qty": 14400.00,
	                "value": 6611260.00
	            },
	            {
	                "cost": 29879.79,
	                "group": "2020Nov25",
	                "qty": 11632.00,
	                "value": 6124938.00
	            },
	            {
	                "cost": 26172.57,
	                "group": "2021Feb04",
	                "qty": 7009.00,
	                "value": 3812007.00
	            },
	            {
	                "cost": 25384.68,
	                "group": "",
	                "qty": 9347.00,
	                "value": 4631017.50
	            },
	            {
	                "cost": 23133.93,
	                "group": "15-Jun-21",
	                "qty": 8776.00,
	                "value": 4947687.00
	            },
	            {
	                "cost": 18324.83,
	                "group": "INT",
	                "qty": 5992.00,
	                "value": 3506784.00
	            },
	            {
	                "cost": 13153.04,
	                "group": "2020Dec30",
	                "qty": 5106.00,
	                "value": 2748874.00
	            },
	            {
	                "cost": 13065.36,
	                "group": "2018",
	                "qty": 6222.00,
	                "value": 2168218.50
	            },
	            {
	                "cost": 11286.22,
	                "group": "2020Nov30",
	                "qty": 3830.00,
	                "value": 1888739.00
	            },
	            {
	                "cost": 5261.64,
	                "group": "2021Feb25",
	                "qty": 2687.00,
	                "value": 1072353.00
	            },
	            {
	                "cost": 5156.23,
	                "group": "2019",
	                "qty": 1580.00,
	                "value": 817852.50
	            },
	            {
	                "cost": 4323.72,
	                "group": "Current-ACM",
	                "qty": 3779.00,
	                "value": 796727.00
	            },
	            {
	                "cost": 3931.95,
	                "group": "Xmas 2019",
	                "qty": 1160.00,
	                "value": 627907.00
	            },
	            {
	                "cost": 1734.08,
	                "group": "2017",
	                "qty": 596.00,
	                "value": 159685.50
	            },
	            {
	                "cost": 1232.20,
	                "group": "MAC",
	                "qty": 235.00,
	                "value": 176212.50
	            },
	            {
	                "cost": 1120.46,
	                "group": "P20",
	                "qty": 357.00,
	                "value": 158997.00
	            },
	            {
	                "cost": 920.10,
	                "group": "2020",
	                "qty": 395.00,
	                "value": 176852.50
	            },
	            {
	                "cost": 316.63,
	                "group": "2014 and older",
	                "qty": 189.00,
	                "value": 27622.00
	            },
	            {
	                "cost": 157.47,
	                "group": "2016",
	                "qty": 44.00,
	                "value": 9834.00
	            },
	            {
	                "cost": 94.35,
	                "group": "2015",
	                "qty": 90.00,
	                "value": 3100.00
	            },
	            {
	                "cost": 2.89,
	                "group": "PER",
	                "qty": 3.00,
	                "value": 498.50
	            },
	            {
	                "cost": 0.00,
	                "group": "A19",
	                "qty": 0.00,
	                "value": 0.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Xmas2020",
	                "qty": 280.00,
	                "value": 19880.00
	            }
	        ],
	        "group5": [
	            {
	                "cost": 262817.53,
	                "group": "COSMETIQUE",
	                "qty": 125649.00,
	                "value": 52798563.50
	            },
	            {
	                "cost": 193292.05,
	                "group": "PARFUM",
	                "qty": 74915.00,
	                "value": 40130400.50
	            },
	            {
	                "cost": 99386.74,
	                "group": "BIJOUX",
	                "qty": 36007.00,
	                "value": 19021059.50
	            },
	            {
	                "cost": 28685.41,
	                "group": "ACCESSOIRES MODE",
	                "qty": 10264.00,
	                "value": 5077659.00
	            },
	            {
	                "cost": 20103.23,
	                "group": "CORPS",
	                "qty": 11562.00,
	                "value": 3840691.00
	            },
	            {
	                "cost": 3365.16,
	                "group": "HOME",
	                "qty": 1068.00,
	                "value": 570349.00
	            },
	            {
	                "cost": 1400.97,
	                "group": "VISAGE",
	                "qty": 1014.00,
	                "value": 293766.00
	            },
	            {
	                "cost": 667.61,
	                "group": "PLV",
	                "qty": 394.00,
	                "value": 17775.00
	            },
	            {
	                "cost": 369.64,
	                "group": "Gift Box",
	                "qty": 449.00,
	                "value": 45729.50
	            },
	            {
	                "cost": 134.13,
	                "group": "Corps",
	                "qty": 51.00,
	                "value": 25449.00
	            },
	            {
	                "cost": 3.55,
	                "group": "",
	                "qty": 23.00,
	                "value": 2145.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Parfum",
	                "qty": 280.00,
	                "value": 19880.00
	            }
	        ],
	        "group6": [
	            {
	                "cost": 112973.44,
	                "group": "EDP 30ML",
	                "qty": 49996.00,
	                "value": 24947505.00
	            },
	            {
	                "cost": 98919.02,
	                "group": "TEINT",
	                "qty": 32639.00,
	                "value": 20007087.50
	            },
	            {
	                "cost": 73127.67,
	                "group": "LEVRES",
	                "qty": 37420.00,
	                "value": 14773836.50
	            },
	            {
	                "cost": 42109.27,
	                "group": "BOUCLE D OREILLE",
	                "qty": 17607.00,
	                "value": 8377041.00
	            },
	            {
	                "cost": 39136.44,
	                "group": "EDP 100ML",
	                "qty": 8582.00,
	                "value": 8249576.00
	            },
	            {
	                "cost": 23597.42,
	                "group": "YEUX",
	                "qty": 15627.00,
	                "value": 4668866.00
	            },
	            {
	                "cost": 23418.76,
	                "group": "BRACELET",
	                "qty": 8372.00,
	                "value": 4585513.00
	            },
	            {
	                "cost": 19007.04,
	                "group": "COFFRETS",
	                "qty": 3909.00,
	                "value": 2519259.50
	            },
	            {
	                "cost": 17269.40,
	                "group": "ONGLES",
	                "qty": 15840.00,
	                "value": 3696587.00
	            },
	            {
	                "cost": 17015.61,
	                "group": "COLLIER",
	                "qty": 4640.00,
	                "value": 3064692.50
	            },
	            {
	                "cost": 16872.61,
	                "group": "PINCEAUX",
	                "qty": 6061.00,
	                "value": 3409642.00
	            },
	            {
	                "cost": 16759.01,
	                "group": "DERIVES PARFUMES",
	                "qty": 9925.00,
	                "value": 3262986.00
	            },
	            {
	                "cost": 12872.98,
	                "group": "LAVANT",
	                "qty": 7289.00,
	                "value": 2477644.00
	            },
	            {
	                "cost": 12214.72,
	                "group": "BAGUE",
	                "qty": 3925.00,
	                "value": 2260975.00
	            },
	            {
	                "cost": 10863.11,
	                "group": "PALETTES",
	                "qty": 3025.00,
	                "value": 1981290.50
	            },
	            {
	                "cost": 9545.30,
	                "group": "ACCESSOIRES CHEVEUX",
	                "qty": 5803.00,
	                "value": 1729574.00
	            },
	            {
	                "cost": 8858.33,
	                "group": "ACCESSOIRES COSMETIQUES",
	                "qty": 7778.00,
	                "value": 1844118.00
	            },
	            {
	                "cost": 6724.51,
	                "group": "SAC",
	                "qty": 682.00,
	                "value": 1213923.00
	            },
	            {
	                "cost": 5876.95,
	                "group": "SOIN VISAGE",
	                "qty": 2520.00,
	                "value": 819773.50
	            },
	            {
	                "cost": 5804.37,
	                "group": "SOURCILS",
	                "qty": 4252.00,
	                "value": 1327522.00
	            },
	            {
	                "cost": 4718.71,
	                "group": "TEXTILE",
	                "qty": 1343.00,
	                "value": 919993.00
	            },
	            {
	                "cost": 3509.82,
	                "group": "SOINS MAINS",
	                "qty": 2834.00,
	                "value": 648986.00
	            },
	            {
	                "cost": 3460.68,
	                "group": "MAROQUINERIE",
	                "qty": 1075.00,
	                "value": 604075.00
	            },
	            {
	                "cost": 3290.61,
	                "group": "ACIER",
	                "qty": 1162.00,
	                "value": 538136.50
	            },
	            {
	                "cost": 2596.66,
	                "group": "HYDRATANT",
	                "qty": 1155.00,
	                "value": 514355.00
	            },
	            {
	                "cost": 2578.40,
	                "group": "",
	                "qty": 1149.00,
	                "value": 434827.00
	            },
	            {
	                "cost": 2555.60,
	                "group": "COLOGNE",
	                "qty": 907.00,
	                "value": 551614.00
	            },
	            {
	                "cost": 1788.05,
	                "group": "LUNETTE",
	                "qty": 423.00,
	                "value": 263862.00
	            },
	            {
	                "cost": 1628.65,
	                "group": "CORPS",
	                "qty": 487.00,
	                "value": 269840.50
	            },
	            {
	                "cost": 1359.52,
	                "group": "100ML FEMME",
	                "qty": 293.00,
	                "value": 292707.00
	            },
	            {
	                "cost": 1336.83,
	                "group": "EXFOLIANTS",
	                "qty": 447.00,
	                "value": 245403.00
	            },
	            {
	                "cost": 1249.46,
	                "group": "BIJOUX PRECIEUX",
	                "qty": 240.00,
	                "value": 176712.50
	            },
	            {
	                "cost": 1203.57,
	                "group": "BOUGIE",
	                "qty": 387.00,
	                "value": 211423.00
	            },
	            {
	                "cost": 1177.92,
	                "group": "EDP 10ML",
	                "qty": 1073.00,
	                "value": 254872.50
	            },
	            {
	                "cost": 956.65,
	                "group": "GADGET",
	                "qty": 411.00,
	                "value": 144295.50
	            },
	            {
	                "cost": 851.12,
	                "group": "ACCESSOIRES PARFUM",
	                "qty": 359.00,
	                "value": 114552.50
	            },
	            {
	                "cost": 667.61,
	                "group": "EMBALLAGE",
	                "qty": 394.00,
	                "value": 17775.00
	            },
	            {
	                "cost": 502.81,
	                "group": "PARAPLUIE",
	                "qty": 122.00,
	                "value": 59995.00
	            },
	            {
	                "cost": 294.30,
	                "group": "NOURRISSANT",
	                "qty": 195.00,
	                "value": 60855.00
	            },
	            {
	                "cost": 278.72,
	                "group": "ANTI-POLLUTION",
	                "qty": 189.00,
	                "value": 56781.00
	            },
	            {
	                "cost": 244.00,
	                "group": "APAISANT",
	                "qty": 170.00,
	                "value": 50540.00
	            },
	            {
	                "cost": 228.62,
	                "group": "PURIFIANT",
	                "qty": 169.00,
	                "value": 48151.00
	            },
	            {
	                "cost": 142.27,
	                "group": "ECLAT",
	                "qty": 128.00,
	                "value": 31742.00
	            },
	            {
	                "cost": 134.13,
	                "group": "-",
	                "qty": 51.00,
	                "value": 25449.00
	            },
	            {
	                "cost": 131.22,
	                "group": "TRIO",
	                "qty": 81.00,
	                "value": 11745.00
	            },
	            {
	                "cost": 95.48,
	                "group": "LAINAGE",
	                "qty": 28.00,
	                "value": 20972.00
	            },
	            {
	                "cost": 86.07,
	                "group": "ACCESOIRES PARFUM",
	                "qty": 57.00,
	                "value": 9063.00
	            },
	            {
	                "cost": 85.70,
	                "group": "Gift Box",
	                "qty": 108.00,
	                "value": 7540.00
	            },
	            {
	                "cost": 68.14,
	                "group": "DIVERS",
	                "qty": 50.00,
	                "value": 13442.00
	            },
	            {
	                "cost": 38.74,
	                "group": "COFFRET MODE",
	                "qty": 17.00,
	                "value": 6470.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Coffret",
	                "qty": 280.00,
	                "value": 19880.00
	            }
	        ],
	        "group7": [
	            {
	                "cost": 70072.36,
	                "group": "30ML FEMME",
	                "qty": 31218.00,
	                "value": 15577283.00
	            },
	            {
	                "cost": 57358.44,
	                "group": "FOND DE TEINT",
	                "qty": 13732.00,
	                "value": 11088224.50
	            },
	            {
	                "cost": 29148.81,
	                "group": "",
	                "qty": 12023.00,
	                "value": 5496087.00
	            },
	            {
	                "cost": 25686.49,
	                "group": "30ML HOMME",
	                "qty": 11455.00,
	                "value": 5716045.00
	            },
	            {
	                "cost": 24789.62,
	                "group": "RAL MAT",
	                "qty": 10914.00,
	                "value": 4833846.00
	            },
	            {
	                "cost": 16989.57,
	                "group": "BRACELET ACIER",
	                "qty": 5399.00,
	                "value": 3285846.00
	            },
	            {
	                "cost": 16618.55,
	                "group": "100ML HOMME",
	                "qty": 3569.00,
	                "value": 3565431.00
	            },
	            {
	                "cost": 16107.09,
	                "group": "BOUCLE D OREILLE ACIER",
	                "qty": 4719.00,
	                "value": 3126123.50
	            },
	            {
	                "cost": 15844.59,
	                "group": "30ML PAILLETTES",
	                "qty": 6719.00,
	                "value": 3352781.00
	            },
	            {
	                "cost": 14308.33,
	                "group": "COFFRET MULTI",
	                "qty": 2054.00,
	                "value": 1983507.50
	            },
	            {
	                "cost": 14180.78,
	                "group": "RAL CLASSIQUE",
	                "qty": 7181.00,
	                "value": 2738419.00
	            },
	            {
	                "cost": 13426.21,
	                "group": "100ML FEMME",
	                "qty": 2880.00,
	                "value": 2877120.00
	            },
	            {
	                "cost": 13039.31,
	                "group": "COLLIER ACIER",
	                "qty": 3780.00,
	                "value": 2483485.50
	            },
	            {
	                "cost": 11441.48,
	                "group": "PINCEAUX TEINT",
	                "qty": 3304.00,
	                "value": 2213811.00
	            },
	            {
	                "cost": 11140.03,
	                "group": "BRONZER",
	                "qty": 3383.00,
	                "value": 1926788.00
	            },
	            {
	                "cost": 10941.02,
	                "group": "POUDRES",
	                "qty": 5498.00,
	                "value": 2743502.00
	            },
	            {
	                "cost": 10573.72,
	                "group": "BAGUE ACIER",
	                "qty": 3478.00,
	                "value": 1943108.00
	            },
	            {
	                "cost": 10520.52,
	                "group": "CORRECTEURS ANTICERNES",
	                "qty": 5478.00,
	                "value": 2314828.50
	            },
	            {
	                "cost": 10159.23,
	                "group": "PENDANTE",
	                "qty": 4316.00,
	                "value": 1877985.00
	            },
	            {
	                "cost": 9948.91,
	                "group": "GEL DOUCHE CLASSIQUE",
	                "qty": 5843.00,
	                "value": 1924567.00
	            },
	            {
	                "cost": 9726.27,
	                "group": "PALETTE YEUX",
	                "qty": 2489.00,
	                "value": 1711423.00
	            },
	            {
	                "cost": 9418.89,
	                "group": "VAO LAQUE",
	                "qty": 9507.00,
	                "value": 2107283.50
	            },
	            {
	                "cost": 8505.51,
	                "group": "CRAYONS LEVRES",
	                "qty": 6384.00,
	                "value": 1838499.50
	            },
	            {
	                "cost": 7417.23,
	                "group": "RAL LIQUIDE",
	                "qty": 3701.00,
	                "value": 1634684.50
	            },
	            {
	                "cost": 7192.27,
	                "group": "CREOLE",
	                "qty": 4044.00,
	                "value": 1624696.00
	            },
	            {
	                "cost": 7049.82,
	                "group": "MASCARA",
	                "qty": 3461.00,
	                "value": 1445037.00
	            },
	            {
	                "cost": 6957.42,
	                "group": "100ML PAILLETTES",
	                "qty": 1501.00,
	                "value": 1499499.00
	            },
	            {
	                "cost": 6924.83,
	                "group": "CRAYONS YEUX",
	                "qty": 4875.00,
	                "value": 1125174.50
	            },
	            {
	                "cost": 5523.06,
	                "group": "RAL LAQUE",
	                "qty": 3025.00,
	                "value": 1206460.00
	            },
	            {
	                "cost": 4930.45,
	                "group": "BLUSH",
	                "qty": 2763.00,
	                "value": 1099104.00
	            },
	            {
	                "cost": 4917.55,
	                "group": "LAIT CORP",
	                "qty": 2068.00,
	                "value": 1027245.50
	            },
	            {
	                "cost": 4638.92,
	                "group": "CRAYONS SOURCILS",
	                "qty": 3537.00,
	                "value": 1016037.00
	            },
	            {
	                "cost": 4534.62,
	                "group": "GLOSS",
	                "qty": 2463.00,
	                "value": 958946.50
	            },
	            {
	                "cost": 4465.56,
	                "group": "ALL MATLONG",
	                "qty": 2244.00,
	                "value": 1007556.00
	            },
	            {
	                "cost": 4461.58,
	                "group": "GEL DOUCHE",
	                "qty": 2528.00,
	                "value": 831712.00
	            },
	            {
	                "cost": 4214.67,
	                "group": "OAP COMPACTE",
	                "qty": 3797.00,
	                "value": 865575.50
	            },
	            {
	                "cost": 4044.56,
	                "group": "PINCEAUX YEUX",
	                "qty": 1995.00,
	                "value": 883156.50
	            },
	            {
	                "cost": 3796.60,
	                "group": "DEMAQUILLANTS",
	                "qty": 1724.00,
	                "value": 536161.50
	            },
	            {
	                "cost": 3698.07,
	                "group": "VAO A EFFET",
	                "qty": 3277.00,
	                "value": 798238.50
	            },
	            {
	                "cost": 3637.02,
	                "group": "CREME MAIN HYDRATANTE",
	                "qty": 2940.00,
	                "value": 673260.00
	            },
	            {
	                "cost": 3442.57,
	                "group": "PUCE",
	                "qty": 2107.00,
	                "value": 638193.50
	            },
	            {
	                "cost": 3374.34,
	                "group": "SAC",
	                "qty": 518.00,
	                "value": 580010.00
	            },
	            {
	                "cost": 3232.52,
	                "group": "CREME MAIN",
	                "qty": 2918.00,
	                "value": 668222.00
	            },
	            {
	                "cost": 3211.57,
	                "group": "RAL BRILLANT",
	                "qty": 1105.00,
	                "value": 442988.00
	            },
	            {
	                "cost": 2934.03,
	                "group": "OAP STICK",
	                "qty": 1914.00,
	                "value": 729212.00
	            },
	            {
	                "cost": 2643.53,
	                "group": "ECHARPE",
	                "qty": 682.00,
	                "value": 494918.00
	            },
	            {
	                "cost": 2501.01,
	                "group": "TROUSSE ou VANITY",
	                "qty": 797.00,
	                "value": 430034.00
	            },
	            {
	                "cost": 2491.92,
	                "group": "FEELING BOX",
	                "qty": 1460.00,
	                "value": 210831.00
	            },
	            {
	                "cost": 2474.07,
	                "group": "EYELINERS",
	                "qty": 1580.00,
	                "value": 503867.00
	            },
	            {
	                "cost": 2470.65,
	                "group": "MANUCURE",
	                "qty": 2943.00,
	                "value": 530938.00
	            },
	            {
	                "cost": 2417.85,
	                "group": "BASE TEINT",
	                "qty": 1199.00,
	                "value": 556761.50
	            },
	            {
	                "cost": 2214.78,
	                "group": "LAIT",
	                "qty": 966.00,
	                "value": 444984.00
	            },
	            {
	                "cost": 2096.70,
	                "group": "AUTRES",
	                "qty": 1086.00,
	                "value": 400497.00
	            },
	            {
	                "cost": 1856.04,
	                "group": "BAGUE",
	                "qty": 616.00,
	                "value": 290541.00
	            },
	            {
	                "cost": 1811.25,
	                "group": "ELASTIQUE",
	                "qty": 1141.00,
	                "value": 341276.00
	            },
	            {
	                "cost": 1788.05,
	                "group": "LUNETTE DE SOLEIL",
	                "qty": 423.00,
	                "value": 263862.00
	            },
	            {
	                "cost": 1685.60,
	                "group": "CHEVEUX",
	                "qty": 944.00,
	                "value": 316495.00
	            },
	            {
	                "cost": 1642.29,
	                "group": "SAUTOIR",
	                "qty": 368.00,
	                "value": 274787.00
	            },
	            {
	                "cost": 1600.24,
	                "group": "FOULARD IMPRIME",
	                "qty": 522.00,
	                "value": 329074.50
	            },
	            {
	                "cost": 1479.47,
	                "group": "BRACELET RIGIDE",
	                "qty": 507.00,
	                "value": 266765.00
	            },
	            {
	                "cost": 1463.16,
	                "group": "BB ET CC CREMES",
	                "qty": 520.00,
	                "value": 254944.00
	            },
	            {
	                "cost": 1320.43,
	                "group": "BRACELET LOT",
	                "qty": 894.00,
	                "value": 257000.00
	            },
	            {
	                "cost": 1311.24,
	                "group": "COLOGNE FEMME",
	                "qty": 294.00,
	                "value": 293265.00
	            },
	            {
	                "cost": 1304.50,
	                "group": "PINCE",
	                "qty": 1401.00,
	                "value": 232541.50
	            },
	            {
	                "cost": 1233.98,
	                "group": "SAVON",
	                "qty": 886.00,
	                "value": 232553.50
	            },
	            {
	                "cost": 1232.20,
	                "group": "BIJOUX ARGENT",
	                "qty": 235.00,
	                "value": 176212.50
	            },
	            {
	                "cost": 1203.57,
	                "group": "SIGNATURE BG",
	                "qty": 387.00,
	                "value": 211423.00
	            },
	            {
	                "cost": 1179.46,
	                "group": "DISSOLVANT",
	                "qty": 668.00,
	                "value": 178654.50
	            },
	            {
	                "cost": 1165.45,
	                "group": "MASCARA SOURCILS",
	                "qty": 715.00,
	                "value": 311485.00
	            },
	            {
	                "cost": 1140.25,
	                "group": "POCHETTE",
	                "qty": 138.00,
	                "value": 196438.00
	            },
	            {
	                "cost": 1113.44,
	                "group": "10ML FEMME",
	                "qty": 1021.00,
	                "value": 242002.50
	            },
	            {
	                "cost": 1102.73,
	                "group": "GOMMAGE",
	                "qty": 382.00,
	                "value": 207294.00
	            },
	            {
	                "cost": 1090.96,
	                "group": "SOIN ONGLES",
	                "qty": 773.00,
	                "value": 224415.00
	            },
	            {
	                "cost": 1054.28,
	                "group": "BANDOULIERE",
	                "qty": 133.00,
	                "value": 182607.00
	            },
	            {
	                "cost": 1025.15,
	                "group": "PALETTE SOURCILS",
	                "qty": 505.00,
	                "value": 251995.00
	            },
	            {
	                "cost": 1003.05,
	                "group": "COFFRET EDP100ML",
	                "qty": 135.00,
	                "value": 202365.00
	            },
	            {
	                "cost": 928.20,
	                "group": "VOILE",
	                "qty": 420.00,
	                "value": 209580.00
	            },
	            {
	                "cost": 919.16,
	                "group": "VAO NACRE",
	                "qty": 921.00,
	                "value": 202941.00
	            },
	            {
	                "cost": 906.13,
	                "group": "EPONGE",
	                "qty": 1245.00,
	                "value": 211415.50
	            },
	            {
	                "cost": 848.80,
	                "group": "FANTAISIE",
	                "qty": 396.00,
	                "value": 142358.00
	            },
	            {
	                "cost": 827.37,
	                "group": "SAVONS",
	                "qty": 360.00,
	                "value": 152580.00
	            },
	            {
	                "cost": 774.85,
	                "group": "MASQUES",
	                "qty": 442.00,
	                "value": 115647.00
	            },
	            {
	                "cost": 770.28,
	                "group": "MASQUE TISSU",
	                "qty": 786.00,
	                "value": 179994.00
	            },
	            {
	                "cost": 750.59,
	                "group": "BRACELET SOUPLE",
	                "qty": 293.00,
	                "value": 147115.50
	            },
	            {
	                "cost": 714.21,
	                "group": "BARRETTE",
	                "qty": 362.00,
	                "value": 134663.50
	            },
	            {
	                "cost": 646.78,
	                "group": "REGARD",
	                "qty": 888.00,
	                "value": 151695.50
	            },
	            {
	                "cost": 645.00,
	                "group": "PAILETTES CORPS",
	                "qty": 172.00,
	                "value": 111628.00
	            },
	            {
	                "cost": 643.50,
	                "group": "NETTOYANTS",
	                "qty": 195.00,
	                "value": 77512.50
	            },
	            {
	                "cost": 630.69,
	                "group": "MASQUE CREME",
	                "qty": 228.00,
	                "value": 113772.00
	            },
	            {
	                "cost": 624.00,
	                "group": "BROSSE",
	                "qty": 351.00,
	                "value": 107402.00
	            },
	            {
	                "cost": 612.48,
	                "group": "ETUI ARGENT",
	                "qty": 176.00,
	                "value": 87560.00
	            },
	            {
	                "cost": 610.50,
	                "group": "DO IT YOURSELF",
	                "qty": 37.00,
	                "value": 36963.00
	            },
	            {
	                "cost": 592.79,
	                "group": "PINCEAUX SOURCILS",
	                "qty": 260.00,
	                "value": 136440.00
	            },
	            {
	                "cost": 558.37,
	                "group": "LOT BOUCLE D OREILLE",
	                "qty": 474.00,
	                "value": 137186.50
	            },
	            {
	                "cost": 549.10,
	                "group": "DIVERS MODE",
	                "qty": 242.00,
	                "value": 115010.00
	            },
	            {
	                "cost": 548.82,
	                "group": "PARRURE",
	                "qty": 169.00,
	                "value": 115585.00
	            },
	            {
	                "cost": 544.14,
	                "group": "TROUSSE MAQ",
	                "qty": 174.00,
	                "value": 111410.00
	            },
	            {
	                "cost": 527.94,
	                "group": "BASE ET TOP COAT ONGLES",
	                "qty": 432.00,
	                "value": 116748.00
	            },
	            {
	                "cost": 499.72,
	                "group": "BASE LEVRES",
	                "qty": 403.00,
	                "value": 112437.00
	            },
	            {
	                "cost": 483.00,
	                "group": "PARAPLUIE IMPRIME",
	                "qty": 115.00,
	                "value": 57212.50
	            },
	            {
	                "cost": 482.56,
	                "group": "PORTE FEUILLE",
	                "qty": 125.00,
	                "value": 97879.50
	            },
	            {
	                "cost": 471.52,
	                "group": "PINCEAUX LEVRES",
	                "qty": 288.00,
	                "value": 109834.00
	            },
	            {
	                "cost": 453.12,
	                "group": "BOITE CADEAU",
	                "qty": 192.00,
	                "value": 0.00
	            },
	            {
	                "cost": 441.45,
	                "group": "BEURRE",
	                "qty": 109.00,
	                "value": 70577.50
	            },
	            {
	                "cost": 438.80,
	                "group": "CREMES VISAGE",
	                "qty": 103.00,
	                "value": 60492.50
	            },
	            {
	                "cost": 354.07,
	                "group": "PORTANT BIJOUX",
	                "qty": 150.00,
	                "value": 73771.50
	            },
	            {
	                "cost": 342.58,
	                "group": "GELEE",
	                "qty": 131.00,
	                "value": 64049.50
	            },
	            {
	                "cost": 341.90,
	                "group": "RAS DE COU",
	                "qty": 92.00,
	                "value": 58368.00
	            },
	            {
	                "cost": 308.28,
	                "group": "COFFRET EDP30ML",
	                "qty": 86.00,
	                "value": 51343.00
	            },
	            {
	                "cost": 308.20,
	                "group": "FRENCH MANICURE",
	                "qty": 134.00,
	                "value": 39530.00
	            },
	            {
	                "cost": 302.96,
	                "group": "BRACELET CORDON",
	                "qty": 108.00,
	                "value": 54692.00
	            },
	            {
	                "cost": 290.64,
	                "group": "FOULARD UNI",
	                "qty": 83.00,
	                "value": 53282.50
	            },
	            {
	                "cost": 284.96,
	                "group": "COFFRET EDP30ML + BRACELETTE",
	                "qty": 137.00,
	                "value": 34250.00
	            },
	            {
	                "cost": 281.40,
	                "group": "PORTE CLES",
	                "qty": 105.00,
	                "value": 5250.00
	            },
	            {
	                "cost": 278.50,
	                "group": "BAGUE LOTS",
	                "qty": 80.00,
	                "value": 43820.00
	            },
	            {
	                "cost": 274.62,
	                "group": "MIROIR",
	                "qty": 153.00,
	                "value": 60867.00
	            },
	            {
	                "cost": 273.78,
	                "group": "GRANITE",
	                "qty": 81.00,
	                "value": 44469.00
	            },
	            {
	                "cost": 240.53,
	                "group": "DIVERS COS",
	                "qty": 164.00,
	                "value": 26743.00
	            },
	            {
	                "cost": 238.64,
	                "group": "EtUI  PU",
	                "qty": 183.00,
	                "value": 26992.50
	            },
	            {
	                "cost": 210.11,
	                "group": "UTILITAIRE",
	                "qty": 201.00,
	                "value": 31462.50
	            },
	            {
	                "cost": 200.87,
	                "group": "DORMEUSE",
	                "qty": 90.00,
	                "value": 38236.00
	            },
	            {
	                "cost": 193.10,
	                "group": "NETTOYANT PINCEAUX",
	                "qty": 137.00,
	                "value": 40887.00
	            },
	            {
	                "cost": 187.05,
	                "group": "HUILE",
	                "qty": 47.00,
	                "value": 30534.00
	            },
	            {
	                "cost": 177.58,
	                "group": "SAC CUIR",
	                "qty": 12.00,
	                "value": 29388.00
	            },
	            {
	                "cost": 147.55,
	                "group": "HIGHLIGHTER",
	                "qty": 66.00,
	                "value": 22935.00
	            },
	            {
	                "cost": 147.04,
	                "group": "CREME",
	                "qty": 44.00,
	                "value": 21956.00
	            },
	            {
	                "cost": 145.02,
	                "group": "PLASTRON",
	                "qty": 31.00,
	                "value": 18770.00
	            },
	            {
	                "cost": 131.22,
	                "group": "TRIO HOMME",
	                "qty": 81.00,
	                "value": 11745.00
	            },
	            {
	                "cost": 126.72,
	                "group": "VAO PAILLETTE",
	                "qty": 128.00,
	                "value": 28776.50
	            },
	            {
	                "cost": 114.40,
	                "group": "SERUMS",
	                "qty": 22.00,
	                "value": 16445.00
	            },
	            {
	                "cost": 111.69,
	                "group": "PALETTE TEINT",
	                "qty": 31.00,
	                "value": 17872.50
	            },
	            {
	                "cost": 108.80,
	                "group": "GOMMAGE VISAGE",
	                "qty": 34.00,
	                "value": 13515.00
	            },
	            {
	                "cost": 107.59,
	                "group": "CLIP",
	                "qty": 44.00,
	                "value": 13727.50
	            },
	            {
	                "cost": 106.10,
	                "group": "TROUSSE VANITY",
	                "qty": 30.00,
	                "value": 3624.50
	            },
	            {
	                "cost": 106.08,
	                "group": "DIVERS GADGETS",
	                "qty": 51.00,
	                "value": 17799.00
	            },
	            {
	                "cost": 102.08,
	                "group": "COFFRET BAIN",
	                "qty": 58.00,
	                "value": 17255.00
	            },
	            {
	                "cost": 98.00,
	                "group": "BRUME",
	                "qty": 28.00,
	                "value": 10450.00
	            },
	            {
	                "cost": 86.07,
	                "group": "ETUI PARFUM",
	                "qty": 57.00,
	                "value": 9063.00
	            },
	            {
	                "cost": 85.70,
	                "group": "Gift Box",
	                "qty": 108.00,
	                "value": 7540.00
	            },
	            {
	                "cost": 79.58,
	                "group": "PINCEAUX ONGLES",
	                "qty": 46.00,
	                "value": 15985.00
	            },
	            {
	                "cost": 64.48,
	                "group": "10MLPAILLETTE",
	                "qty": 52.00,
	                "value": 12870.00
	            },
	            {
	                "cost": 49.58,
	                "group": "SET DE PINCEAUX",
	                "qty": 31.00,
	                "value": 9528.50
	            },
	            {
	                "cost": 29.04,
	                "group": "SAC PLAGE",
	                "qty": 6.00,
	                "value": 5541.00
	            },
	            {
	                "cost": 21.10,
	                "group": "COFFRET GADGET",
	                "qty": 10.00,
	                "value": 4020.00
	            },
	            {
	                "cost": 19.81,
	                "group": "PARAPLUIE UNI",
	                "qty": 7.00,
	                "value": 2782.50
	            },
	            {
	                "cost": 17.26,
	                "group": "PIERCING",
	                "qty": 5.00,
	                "value": 500.00
	            },
	            {
	                "cost": 13.98,
	                "group": "PORTE MONNAIE",
	                "qty": 10.00,
	                "value": 500.00
	            },
	            {
	                "cost": 6.30,
	                "group": "SAVON LIQUIDE MAINS",
	                "qty": 1.00,
	                "value": 0.00
	            },
	            {
	                "cost": 4.90,
	                "group": "POCHON",
	                "qty": 14.00,
	                "value": 700.00
	            },
	            {
	                "cost": 0.00,
	                "group": "CHAINE DE CHEVILLE",
	                "qty": 0.00,
	                "value": 0.00
	            }
	        ],
	        "group8": [
	            {
	                "cost": 431652.86,
	                "group": "INT",
	                "qty": 198300.00,
	                "value": 88342975.00
	            },
	            {
	                "cost": 48850.67,
	                "group": "",
	                "qty": 18257.00,
	                "value": 10088374.00
	            },
	            {
	                "cost": 44526.19,
	                "group": "H20",
	                "qty": 11838.00,
	                "value": 7458485.00
	            },
	            {
	                "cost": 35055.56,
	                "group": "P21",
	                "qty": 11862.00,
	                "value": 7312858.00
	            },
	            {
	                "cost": 21127.16,
	                "group": "2021Winter 70",
	                "qty": 7955.00,
	                "value": 3718594.00
	            },
	            {
	                "cost": 4050.62,
	                "group": "2019 and older",
	                "qty": 1441.00,
	                "value": 585507.00
	            },
	            {
	                "cost": 2989.16,
	                "group": "Sale Mar 19",
	                "qty": 930.00,
	                "value": 271193.00
	            },
	            {
	                "cost": 2464.36,
	                "group": "2021Winter 50",
	                "qty": 534.00,
	                "value": 367188.00
	            },
	            {
	                "cost": 2263.77,
	                "group": "A20",
	                "qty": 591.00,
	                "value": 382369.00
	            },
	            {
	                "cost": 1637.02,
	                "group": "Current-ACM",
	                "qty": 1670.00,
	                "value": 290797.00
	            },
	            {
	                "cost": 1538.74,
	                "group": "2021Buy1 get 1 free",
	                "qty": 1476.00,
	                "value": 338004.00
	            },
	            {
	                "cost": 1385.04,
	                "group": "POUDRE COMPACTE",
	                "qty": 696.00,
	                "value": 347304.00
	            },
	            {
	                "cost": 1254.40,
	                "group": "30MLHOTWOOD",
	                "qty": 560.00,
	                "value": 279440.00
	            },
	            {
	                "cost": 1252.46,
	                "group": "E19",
	                "qty": 303.00,
	                "value": 179606.50
	            },
	            {
	                "cost": 1162.13,
	                "group": "100MLYUMMYCANDY",
	                "qty": 251.00,
	                "value": 250749.00
	            },
	            {
	                "cost": 995.54,
	                "group": "P20",
	                "qty": 359.00,
	                "value": 164812.00
	            },
	            {
	                "cost": 703.08,
	                "group": "CRAYON YEUX WAT",
	                "qty": 434.00,
	                "value": 108066.00
	            },
	            {
	                "cost": 686.25,
	                "group": "CORRECTEURLIQ05",
	                "qty": 305.00,
	                "value": 152195.00
	            },
	            {
	                "cost": 640.36,
	                "group": "E20",
	                "qty": 250.00,
	                "value": 118357.00
	            },
	            {
	                "cost": 631.43,
	                "group": "H21",
	                "qty": 319.00,
	                "value": 74451.00
	            },
	            {
	                "cost": 501.93,
	                "group": "CRAYON CONTOUR",
	                "qty": 507.00,
	                "value": 116103.00
	            },
	            {
	                "cost": 423.72,
	                "group": "Crayon sourcils",
	                "qty": 428.00,
	                "value": 106572.00
	            },
	            {
	                "cost": 393.79,
	                "group": "P19",
	                "qty": 162.00,
	                "value": 72469.00
	            },
	            {
	                "cost": 385.70,
	                "group": "LIP & KISS VELV",
	                "qty": 266.00,
	                "value": 79534.00
	            },
	            {
	                "cost": 364.65,
	                "group": "LC EAU DES ORAN",
	                "qty": 165.00,
	                "value": 82335.00
	            },
	            {
	                "cost": 336.15,
	                "group": "H18",
	                "qty": 195.00,
	                "value": 61325.00
	            },
	            {
	                "cost": 315.42,
	                "group": "H19",
	                "qty": 168.00,
	                "value": 24780.00
	            },
	            {
	                "cost": 315.00,
	                "group": "CORRECTEURLIQ02",
	                "qty": 140.00,
	                "value": 69860.00
	            },
	            {
	                "cost": 305.58,
	                "group": "ADOPT EDP NOUVE",
	                "qty": 66.00,
	                "value": 65934.00
	            },
	            {
	                "cost": 297.50,
	                "group": "RAL 10 PL19610",
	                "qty": 170.00,
	                "value": 55930.00
	            },
	            {
	                "cost": 256.20,
	                "group": "CREMMAINMADBLOO",
	                "qty": 244.00,
	                "value": 55876.00
	            },
	            {
	                "cost": 252.00,
	                "group": "CORRECTEURLIQ10",
	                "qty": 112.00,
	                "value": 55888.00
	            },
	            {
	                "cost": 184.50,
	                "group": "CORRECTEURLIQ11",
	                "qty": 82.00,
	                "value": 40918.00
	            },
	            {
	                "cost": 175.50,
	                "group": "CORRECTEURLIQ01",
	                "qty": 78.00,
	                "value": 38922.00
	            },
	            {
	                "cost": 120.21,
	                "group": "A14",
	                "qty": 60.00,
	                "value": 7515.00
	            },
	            {
	                "cost": 91.88,
	                "group": "H16",
	                "qty": 11.00,
	                "value": 8634.00
	            },
	            {
	                "cost": 70.35,
	                "group": "000",
	                "qty": 70.00,
	                "value": 3127.50
	            },
	            {
	                "cost": 66.13,
	                "group": "E18",
	                "qty": 20.00,
	                "value": 9590.00
	            },
	            {
	                "cost": 60.57,
	                "group": "A19",
	                "qty": 50.00,
	                "value": 8688.50
	            },
	            {
	                "cost": 53.46,
	                "group": "PRO NC 023",
	                "qty": 54.00,
	                "value": 12366.00
	            },
	            {
	                "cost": 50.26,
	                "group": "P14",
	                "qty": 33.00,
	                "value": 6610.00
	            },
	            {
	                "cost": 45.60,
	                "group": "DIV",
	                "qty": 19.00,
	                "value": 5605.00
	            },
	            {
	                "cost": 36.57,
	                "group": "A16",
	                "qty": 18.00,
	                "value": 825.00
	            },
	            {
	                "cost": 34.90,
	                "group": "A15",
	                "qty": 33.00,
	                "value": 800.00
	            },
	            {
	                "cost": 33.97,
	                "group": "H17",
	                "qty": 16.00,
	                "value": 4992.00
	            },
	            {
	                "cost": 33.03,
	                "group": "H15",
	                "qty": 18.00,
	                "value": 1767.50
	            },
	            {
	                "cost": 32.42,
	                "group": "E15",
	                "qty": 43.00,
	                "value": 1725.00
	            },
	            {
	                "cost": 29.02,
	                "group": "P16",
	                "qty": 15.00,
	                "value": 375.00
	            },
	            {
	                "cost": 21.14,
	                "group": "E13",
	                "qty": 11.00,
	                "value": 1415.00
	            },
	            {
	                "cost": 14.97,
	                "group": "H13",
	                "qty": 15.00,
	                "value": 375.00
	            },
	            {
	                "cost": 11.43,
	                "group": "A17",
	                "qty": 4.00,
	                "value": 1946.00
	            },
	            {
	                "cost": 8.62,
	                "group": "A10",
	                "qty": 2.00,
	                "value": 50.00
	            },
	            {
	                "cost": 6.90,
	                "group": "P18",
	                "qty": 2.00,
	                "value": 1099.00
	            },
	            {
	                "cost": 6.60,
	                "group": "A12",
	                "qty": 2.00,
	                "value": 1095.00
	            },
	            {
	                "cost": 6.27,
	                "group": "P17",
	                "qty": 3.00,
	                "value": 898.50
	            },
	            {
	                "cost": 6.07,
	                "group": "A11",
	                "qty": 4.00,
	                "value": 175.00
	            },
	            {
	                "cost": 4.70,
	                "group": "E14",
	                "qty": 1.00,
	                "value": 747.50
	            },
	            {
	                "cost": 3.90,
	                "group": "A13",
	                "qty": 6.00,
	                "value": 150.00
	            },
	            {
	                "cost": 2.61,
	                "group": "P13",
	                "qty": 1.00,
	                "value": 25.00
	            },
	            {
	                "cost": 0.00,
	                "group": "BOITE CADEAU GE",
	                "qty": 0.00,
	                "value": 0.00
	            },
	            {
	                "cost": 0.00,
	                "group": "E17",
	                "qty": 51.00,
	                "value": 5100.00
	            }
	        ],
	        "primarygroup": [
	            {
	                "cost": 261382.28,
	                "group": "COSMETIQUE",
	                "qty": 125117.00,
	                "value": 52596154.50
	            },
	            {
	                "cost": 177492.66,
	                "group": "PARFUM",
	                "qty": 64935.00,
	                "value": 36785513.00
	            },
	            {
	                "cost": 97420.16,
	                "group": "BIJOUX",
	                "qty": 35385.00,
	                "value": 18766871.00
	            },
	            {
	                "cost": 38206.44,
	                "group": "CORPS",
	                "qty": 22215.00,
	                "value": 7386894.00
	            },
	            {
	                "cost": 27890.25,
	                "group": "ACCESSOIRES MODE",
	                "qty": 10222.00,
	                "value": 5080241.00
	            },
	            {
	                "cost": 4259.67,
	                "group": "HOME",
	                "qty": 1174.00,
	                "value": 682584.00
	            },
	            {
	                "cost": 2124.22,
	                "group": "VISAGE",
	                "qty": 1327.00,
	                "value": 400705.50
	            },
	            {
	                "cost": 925.29,
	                "group": "PLV",
	                "qty": 720.00,
	                "value": 47567.00
	            },
	            {
	                "cost": 327.00,
	                "group": "VISSAGE",
	                "qty": 194.00,
	                "value": 46495.00
	            },
	            {
	                "cost": 183.08,
	                "group": "",
	                "qty": 31.00,
	                "value": 21469.00
	            },
	            {
	                "cost": 14.97,
	                "group": "null",
	                "qty": 25.00,
	                "value": 3993.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Gift Box",
	                "qty": 51.00,
	                "value": 5100.00
	            },
	            {
	                "cost": 0.00,
	                "group": "Parfum",
	                "qty": 280.00,
	                "value": 19880.00
	            }
	        ]
	    },
	    "totalInventoryCost": 610226.02,
	    "totalInventoryQty": 261676,
	    "totalInventoryValue": 121843467.00,
	    "totalNegativeInventoryQty": -716
	};

