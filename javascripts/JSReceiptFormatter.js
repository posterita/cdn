 var Printer_ESC_COMMANDS = {
            LINE_FEED : "\x0A",
            PAPER_CUT : "\x0A\x1D\x56\x42",       
           
            LEFT_ALIGN : "\x1B\x61\x30",   
            CENTER_ALIGN : "\x1B\x61\x01",
            RIGHT_ALIGN : "\x1B\x61\x02",
           
            FONT_NORMAL : "\x1B!\x45",
            FONT_NORMAL_BOLD : "\x1B!\x4D",
           
            FONT_SMALL : "\x1B!\x47",
            FONT_SMALL_BOLD : "\x1B!\x4F",   
          
            FONT_BIG : "\x1B!\x21",
            FONT_BIG_BOLD : "\x1B!\x29",

            FONT_H1 : "\x1B!\x36",
            FONT_H1_BOLD : "\x1B!\x3E",

            FONT_H2 : "\x1B!\x37",
            FONT_H2_BOLD : "\x1B!\x3F",

            FONT_H3 : "\x1B!\x28",
            FONT_H3_BOLD : "\x1B!\x2E",

            FONT_H4 : "\x1B!\x29",
            FONT_H4_BOLD : "\x1B!\x2F",
           
            OPEN_DRAWER : "\x0A\x1B\x70\x30\x37\x01"
    };


    var JSReceiptUtils = {
            replicate : function(str,n){
                var s = '';
                for(var i=0;i<n;i++) s += str;
                return s;
            },
           
            format : function(str,length,alignRight){
                str += '';
                if(str.length > length){
                    return str.substring(0,length);
                }
               
                var paddingLength = length - str.length;
                var padding = '';
                for(var i=0; i<paddingLength ; i++)    padding += ' ';

                if(alignRight)return padding + str;
               
                return str + padding ;
            },
            
            /*
             * Takes a long string and split it into different lines*/
            splitIntoLines : function(str, n)
            {
            	var lines = [];
            	
            	if (str.length < n)
        		{
            		lines.push(str);
            		return lines;
        		}
            	
            	while(true){
            	    var index = str.lastIndexOf(" ", n-1);
            	    if(index > 0 && (str.length) > n){
            	        var line = str.substring(0,index);
            	        str = str.substring(index+1);
            	        lines.push(line);
            	    }else{
            	    
            	    	lines.push(str);
            	        break;
            	    }
            	}
            	
            	/*If line does not contain any space
            	 * */
            	if (lines.length == 0)
            	{
            		while(true)
            		{
						if (n > str.length){
						     lines.push(str);
						     break;
						}
						
            			lines.push(str.substring(0, n));
            			str = str.substring(n);
            		}
            	}
            	
            	return lines;
            }
    };

    var JSReceiptFormatter = {
            receipt:null,
            options:null,
            initialize:function(data, options){
                this.data = data;
                this.options = options || {};
            },
           
            setOptions:function(){
                this.options.LINE_WIDTH = PrinterManager.getLineWidth();
            },
           
            /* return webprnt xml */
            /* See doc : http://www.starmicronics.com/support/Mannualfolder/en/_StarWebPrintBuilder-js.htm */
            formatAsWebPrnt:function(){
               
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
                var isCreditCardTransaction = false;

                if(receipt.header.cardAmt != 0){
                    isCreditCardTransaction = true;
                }
                               
                var printFormat = [
                               ['FEED'],
                               ['CENTER'],
                               ['N',LINE_SEPARATOR],
                               ['H1',receipt.header.client],
                               ['H2',receipt.header.orgName],
                               ['B',receipt.header.orgAddress1],
                               ['B',receipt.header.orgAddress2],
                               ['B',receipt.header.orgCity],
                               ['N',receipt.header.orgPhone, 'Phone: '],
                               ['N',receipt.header.orgFax, 'FAX: '],
                               ['N',receipt.header.orgTaxId, 'TaxNo: '],                              
                               ['N',LINE_SEPARATOR],
                               ['H3',receipt.header.title],
                               ['N', JSReceiptUtils.format((receipt.header.soTrx ? 'Customer' : 'Vendor') + ': ' + receipt.header.bpName + ((receipt.header.bpName2 != null && receipt.header.bpName2.length > 0) ? (' ' + receipt.header.bpName2) : ''),LINE_WIDTH)],
                               ['N', JSReceiptUtils.format('Terminal: ' + receipt.header.terminal,LINE_WIDTH)],
                               ['N', JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH)],
                               ['N', JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                               ['N', JSReceiptUtils.format('Payment: ' + receipt.header.paymentRuleName + " " + receipt.header.creditCardDetails,LINE_WIDTH)],
                               ['N', JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH)],
                               ['N', JSReceiptUtils.format(receipt.header.dateOrdered,LINE_WIDTH)],
                               ['CENTER'],
                               ['N',LINE_SEPARATOR],
                               ['B',JSReceiptUtils.format('Name',LINE_WIDTH-26)+ JSReceiptUtils.format('Price',8,true) + JSReceiptUtils.format('Qty',6,true)+ JSReceiptUtils.format('Total',12,true)],
                               ['N',LINE_SEPARATOR]                      
                              
                          ];
               
                /*-----------------------------------------------------------------------------------------*/
                /* add order body */
                   for(var i=0; i<receipt.lines.length; i++){
                       var line = receipt.lines[i];
                      
                       var text = line.description || line.productName;
                       while(text.length > (LINE_WIDTH-26))
                       {
                           printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                           text = text.substr(LINE_WIDTH);
                       }
                      
                       var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(line.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format(line.qtyEntered,6,true)+ JSReceiptUtils.format(Number(line.lineNetAmt).toFixed(2),12,true));
                      
                       printFormat.push(['N', s]);
                       
                       if(line.discountMessage != null){
                    	   printFormat.push(['N', JSReceiptUtils.format(line.discountMessage,LINE_WIDTH)]);
                       }
                      
                       if(line.boms != null)
                       for(var j=0; j<line.boms.length; j++){
                           var bom = line.boms[j];
                          
                           var text = " " + (bom.description || bom.productName);
                           while(text.length > (LINE_WIDTH-26))
                           {
                               printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                               text = text.substr(LINE_WIDTH);
                           }
                          
                           var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(bom.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format(bom.qtyEntered,6,true)+ JSReceiptUtils.format(Number(bom.lineNetAmt).toFixed(2),12,true));
                          
                           printFormat.push(['N', s]);                          
                          
                       }
                      
                       if(line.modifiers != null)
                       for(var j=0; j<line.modifiers.length; j++){
                           var modifier = line.modifiers[j];
                          
                           var text = " " + (modifier.description || modifier.productName);
                           while(text.length > (LINE_WIDTH-26))
                           {
                               printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                               text = text.substr(LINE_WIDTH);
                           }
                          
                           var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(modifier.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format('',6,true)+ JSReceiptUtils.format(Number(modifier.lineNetAmt).toFixed(2),12,true));
                          
                           printFormat.push(['N', s]);                          
                          
                       }
                   }
                  
                   /* add order total*/
                   printFormat.push(['N', LINE_SEPARATOR]);
                  
                   var cursymbol = receipt.header.currencySymbol;

                   var subTotalStr = JSReceiptUtils.format('Sub Total (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.subTotal).toFixed(2),12,true);
                                    
                   for(var j=0; j<receipt.taxes.length; j++)
                   {
                       var tax = receipt.taxes[j];
                       var taxStr = JSReceiptUtils.format('Tax - ' + tax.name + ' (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(tax.amt).toFixed(3),12,true);
                       printFormat.push(['N', taxStr]);                          
                      
                   }
                  
                   //var taxTotalStr = JSReceiptUtils.format('Tax (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.taxTotal).toFixed(2),12,true);
                   var discountStr = JSReceiptUtils.format('Discount (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.discountAmt).toFixed(2),12,true);
                   var writeOffStr = JSReceiptUtils.format('Write Off (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.writeOffAmt).toFixed(2),12,true);
                  
                   var totalStr = JSReceiptUtils.format('Grand Total (' + cursymbol + ')',LINE_WIDTH-18) + JSReceiptUtils.format(receipt.header.qtyTotal,6,true)+ JSReceiptUtils.format(Number(receipt.header.grandTotal).toFixed(2),12,true)
                  
                   printFormat.push(['N', subTotalStr]);
                   //printFormat.push(['N', taxTotalStr]);
                  
                   if(receipt.header.discountAmt > 0){
                       printFormat.push(['N', discountStr]);
                   }
                  
                   if(receipt.header.writeOffAmt > 0){
                       printFormat.push(['N', writeOffStr]);
                   }                  

                   printFormat.push(['N', LINE_SEPARATOR]);
                   printFormat.push(['B', totalStr]);
                   printFormat.push(['N', LINE_SEPARATOR]);
                   printFormat.push(['N', '']);

                   var paymentAmtStr = JSReceiptUtils.format('Amt Paid (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.payAmt).toFixed(2),12,true);
                   var dueAmtStr = JSReceiptUtils.format('Amt Due (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.openAmt).toFixed(2),12,true);
                  
                   printFormat.push(['N', paymentAmtStr]);
                  
                   if(receipt.header.docStatus == 'CO' && (receipt.header.paymentRule == 'P' || receipt.header.paymentRule == 'M'))
                   printFormat.push(['N', dueAmtStr]);

                   /* add payment details*/
                  
                   if(receipt.payments.length > 0){
                  
                       var cashAmtStr = JSReceiptUtils.format('Cash',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.cashAmt).toFixed(2),12,true);
                       var cashTenderedStr = JSReceiptUtils.format('Cash Tendered',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountTendered).toFixed(2),12,true);
                       var changeStr = JSReceiptUtils.format('Cash Refunded',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountRefunded).toFixed(2),12,true);
                       var cardAmtStr = JSReceiptUtils.format('Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.cardAmt).toFixed(2),12,true);
                       var externalCardAmtStr = JSReceiptUtils.format('External Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.externalCardAmt).toFixed(2),12,true);
                       var chequeAmtStr = JSReceiptUtils.format('Cheque',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.chequeAmt).toFixed(2),12,true);
                       var voucherAmtStr = JSReceiptUtils.format('Voucher',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.voucherAmt).toFixed(2),12,true);
                       
                       var giftAmtStr = JSReceiptUtils.format('Gift Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.giftCardAmt).toFixed(2),12,true);
                       var skwalletAmtStr = JSReceiptUtils.format('SKWallet',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.skwalletAmt).toFixed(2),12,true);
                       var zapperAmtStr = JSReceiptUtils.format('Zapper',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.zapperAmt).toFixed(2),12,true);
                          
                       if(receipt.header.cashAmt > 0){
                           printFormat.push(['N', cashAmtStr]);
                       }
                          
                       if(receipt.header.orderType == 'POS Order' && receipt.header.amountTendered > 0){
                           printFormat.push(['N', cashTenderedStr]);
                           printFormat.push(['N', changeStr]);
                       }
                      
                       if(receipt.header.cardAmt > 0){
                           printFormat.push(['N', cardAmtStr]);
                       }
                      
                       if(receipt.header.externalCardAmt > 0){
                           printFormat.push(['N', externalCardAmtStr]);
                       }
                      
                       if(receipt.header.chequeAmt > 0){
                           printFormat.push(['N', chequeAmtStr]);
                       } 
                      
                       if(receipt.header.voucherAmt > 0){
                           printFormat.push(['N', voucherAmtStr]);
                       }
                       
                       if(receipt.header.giftAmt > 0){
                    	   printFormat.push(['N', giftAmtStr]);
                       }
                       
                       if(receipt.header.zapperAmt > 0){
                    	   printFormat.push(['N', zapperAmtStr]);
                       }
                       
                       if(receipt.header.skwalletAmt > 0){
                    	   printFormat.push(['N', skwalletAmtStr]);
                       }
                  
                   }
                  
                   if(isCreditCardTransaction)
                   {
                       if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS")
                    {
                           printFormat.push(['FEED']);
                           printFormat.push(['FEED']);
                           printFormat.push(['B', 'Transaction Type: ' + receipt.header.transactionType]);
                           printFormat.push(['B', 'Merchant Location Code: ' + receipt.header.orgId]);
                           printFormat.push(['B', JSReceiptUtils.format("Entry: " + receipt.header.entryType,LINE_WIDTH)]);
                           printFormat.push(['B', JSReceiptUtils.format("Approval Code: " + receipt.header.creditCardAuthorizationCode,LINE_WIDTH)]);
                           printFormat.push(['B', JSReceiptUtils.format("Transaction ID: " + receipt.header.transactionId,LINE_WIDTH)]);
                           printFormat.push(['N', JSReceiptUtils.format("I agree to pay above total amount ",LINE_WIDTH)]);
                           printFormat.push(['N', JSReceiptUtils.format("according to card issuer agreement.",LINE_WIDTH)]);
                           
                           printFormat.push(['FEED']);
                           
                           if(receipt.header.signature){
                        	   printFormat.push(['SIGNATURE']);
                           }
                           else
                           {
                        	   printFormat.push(['FEED']);
                           }
                           
                           printFormat.push(['B', JSReceiptUtils.format("Signature:____________________________________",LINE_WIDTH)]);
                    }
                       else
                       {
                           printFormat.push(['FEED']);
                           
                           if(receipt.header.signature){
                        	   printFormat.push(['SIGNATURE']);
                           }
                           else
                           {
                        	   printFormat.push(['FEED']);
                           }
                           
                           printFormat.push(['B', JSReceiptUtils.format("Signature:____________________________________",LINE_WIDTH)]);
                           if(receipt.header.creditCardAccountHolderName != null && receipt.header.creditCardAccountHolderName != 'null')
                           printFormat.push(['B', JSReceiptUtils.format(receipt.header.creditCardAccountHolderName,LINE_WIDTH)]);
                           printFormat.push(['B', JSReceiptUtils.format("Authorisation: " + receipt.header.creditCardAuthorizationCode,LINE_WIDTH)]);
                           printFormat.push(['N', JSReceiptUtils.format("Buyer agrees to pay total amount above according to ",LINE_WIDTH)]);
                           printFormat.push(['N', JSReceiptUtils.format("cardholder agreement with issuer.",LINE_WIDTH)]);
                       }
                   }
                   else
                   {
                	   if(receipt.header.signature){
                		   printFormat.push(['FEED']);
                           printFormat.push(['SIGNATURE']);
                           printFormat.push(['B', JSReceiptUtils.format("Signature:_____________________________________",LINE_WIDTH)]);
                	   }
                	   
                   }
                  
                   printFormat.push(['FEED']);
                   printFormat.push(['BARCODE']);

                   printFormat.push(['FEED']);
                   printFormat.push(['FEED']);
                   if(receipt.header.receiptFooterMsg){
                	   
                	   var lines = JSReceiptUtils.splitIntoLines(receipt.header.receiptFooterMsg, LINE_WIDTH);
                	   
                	   if (lines.length > 0)
                       {
                     	  for(var i=0; i<lines.length; i++)
                     	  {
                     		  printFormat.push(['S', lines[i]]);
                     	  }
                       }
                   }
                   else{
                       printFormat.push(['S', "Thank you for shopping. See you soon."]);
                   }
                  
                   /*printing comments*/
                   var comments = $$('.comment');
                   var users = $$('.comment .sales-rep');
                   var dates = $$('.comment .date');
                   var messages = $$('.comment .message');
                  
                   for(var i=0; i<comments.length; i++){
                       var user = users[i].innerHTML.stripTags().strip();
                       var date = dates[i].innerHTML.stripTags().strip();
                       var message = messages[i].innerHTML.stripTags().strip();
                      
                       printFormat.push(['FEED']);
                       printFormat.push(['B', JSReceiptUtils.format(user,LINE_WIDTH - 20) + JSReceiptUtils.format(date,20,true)]);
                       printFormat.push(['S', message]);
                       printFormat.push(['FEED']);
                   }
                /*-----------------------------------------------------------------------------------------*/
               
               
                var builder = new StarWebPrintBuilder();
                var request = "";
               
                for(var i=0; i<printFormat.length; i++){
                    var line = printFormat[i];
                   
                    if(line.length == 1)
                    {
                        var command = line[0];
                       
                        switch(command){
                            case 'FEED' :
                                request += builder.createFeedElement({line:1});
                                break;
                            case 'SEPARATOR' :
                                request += builder.createRuledLineElement({thickness:'thin', width:LINE_WIDTH});
                                break;
                            case 'CENTER' :
                                request += builder.createAlignmentElement({position:'center'});
                                break;
                            case 'LEFT' :
                                request += builder.createAlignmentElement({position:'left'});
                                break;
                            case 'RIGHT' :
                                request += builder.createAlignmentElement({position:'right'});
                                break; 
                            case 'SIGNATURE' :
                            	/* see view-order.jsp line 763 */                            	
                            	
                            	if(canvas){ 
                            		var context = canvas.getContext('2d');
                            		request += builder.createBitImageElement({context:context, x:0, y:0, width:canvas.width, height:canvas.height});
                            	}
                            	
                            	break;
                            	
                            case 'BARCODE' :
                            	request += builder.createBarcodeElement({symbology:'Code39', data:receipt.header.documentNo});
                            	break;
                        }
                    }
                    else
                    {
                        var font = line[0];
                        var text = line[1];
                       
                        if(text == null) continue;
                       
                        if(line.length > 2){
                            var label = line[2];
                            text = label + text;
                        }
                       
                        var textElementJSON = {characterspace:0, linespace:32, codepage:'cp998', international:'usa', font:'font_a', width:1, height:1, emphasis:false, underline:false, invert:false, data:text};
                       
                        switch(font){
                            /*normal*/
                            case 'N':break;   
                           
                            /*bold*/
                            case 'B':
                                textElementJSON.emphasis = true;
                                break;   
                           
                            /*invert*/
                            case 'I':
                                textElementJSON.invert = true;
                                break;   
                           
                            /*underline*/
                            case 'U':
                                textElementJSON.underline = true;
                                break;   
                           
                            /*small*/
                            case 'S':
                                textElementJSON.font = 'font_b';
                                break;
                               
                            /*header 1*/
                            case 'H1':
                                textElementJSON.font = 'font_a';
                                textElementJSON.emphasis = true;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;
                               
                            /*header 2*/
                            case 'H2':
                                textElementJSON.font = 'font_a';
                                textElementJSON.emphasis = false;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;                               
                               
                            /*header 3*/
                            case 'H3':
                                textElementJSON.font = 'font_b';
                                textElementJSON.emphasis = true;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;
                               
                            /*header 4*/
                            case 'H4':
                                textElementJSON.font = 'font_b';
                                textElementJSON.emphasis = false;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;   
                        }
                       
                        request += builder.createTextElement(textElementJSON);
                        request += builder.createFeedElement({line:1});
                    }
                }
               
                request += builder.createCutPaperElement({feed:true, type:'full'});
               
                return request;           
               
               
            },
            
            formatAsWeb : function()
            {
            	this.setOptions();
                
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
                var isCreditCardTransaction = false;

                if(receipt.header.cardAmt != 0){
                    isCreditCardTransaction = true;
                }
                
                var printFormat = [
                                   ['FEED'],
                                   ['CENTER'],
                                   ['N',LINE_SEPARATOR],
                                   ['H1',receipt.header.client],
                                   ['H2',receipt.header.orgName],
                                   ['B',receipt.header.orgAddress1],
                                   ['B',receipt.header.orgAddress2],
                                   ['B',receipt.header.orgCity],
                                   ['N',receipt.header.orgPhone, 'Phone: '],
                                   ['N',receipt.header.orgFax, 'FAX: '],
                                   ['N',receipt.header.orgTaxId, 'TaxNo: '],                              
                                   ['N',LINE_SEPARATOR],
                                   ['H3',receipt.header.title],
                                   ['N', JSReceiptUtils.format((receipt.header.soTrx ? 'Customer' : 'Vendor') + ': ' + receipt.header.bpName + ((receipt.header.bpName2 != null && receipt.header.bpName2.length > 0) ? (' ' + receipt.header.bpName2) : ''),LINE_WIDTH)],
                                   ['N', JSReceiptUtils.format('Terminal: ' + receipt.header.terminal,LINE_WIDTH)],
                                   ['N', JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH)],
                                   ['N', JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                                   ['N', JSReceiptUtils.format('Payment: ' + receipt.header.paymentRuleName + " " + receipt.header.creditCardDetails,LINE_WIDTH)],
                                   ['N', JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH)],
                                   ['N', JSReceiptUtils.format(receipt.header.dateOrdered,LINE_WIDTH)],
                                   ['CENTER'],
                                   ['N',LINE_SEPARATOR],
                                   ['B',JSReceiptUtils.format('Name',LINE_WIDTH-26)+ JSReceiptUtils.format('Price',8,true) + JSReceiptUtils.format('Qty',6,true)+ JSReceiptUtils.format('Total',12,true)],
                                   ['N',LINE_SEPARATOR]                      
                                  
                              ];
                   
                    /*-----------------------------------------------------------------------------------------*/
                    /* add order body */
                       for(var i=0; i<receipt.lines.length; i++){
                           var line = receipt.lines[i];
                          
                           var text = line.description || line.productName;
                           while(text.length > (LINE_WIDTH-26))
                           {
                               printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                               text = text.substr(LINE_WIDTH);
                           }
                          
                           var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(line.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format(line.qtyEntered,6,true)+ JSReceiptUtils.format(Number(line.lineNetAmt).toFixed(2),12,true));
                          
                           printFormat.push(['N', s]);
                           
                           if(line.discountMessage != null){
                        	   printFormat.push(['N', JSReceiptUtils.format(line.discountMessage,LINE_WIDTH)]);
                           }
                          
                           if(line.boms != null)
                           for(var j=0; j<line.boms.length; j++){
                               var bom = line.boms[j];
                              
                               var text = " " + (bom.description || bom.productName);
                               while(text.length > (LINE_WIDTH-26))
                               {
                                   printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                                   text = text.substr(LINE_WIDTH);
                               }
                              
                               var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(bom.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format(bom.qtyEntered,6,true)+ JSReceiptUtils.format(Number(bom.lineNetAmt).toFixed(2),12,true));
                              
                               printFormat.push(['N', s]);                          
                              
                           }
                          
                           if(line.modifiers != null)
                           for(var j=0; j<line.modifiers.length; j++){
                               var modifier = line.modifiers[j];
                              
                               var text = " " + (modifier.description || modifier.productName);
                               while(text.length > (LINE_WIDTH-26))
                               {
                                   printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                                   text = text.substr(LINE_WIDTH);
                               }
                              
                               var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(modifier.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format('',6,true)+ JSReceiptUtils.format(Number(modifier.lineNetAmt).toFixed(2),12,true));
                              
                               printFormat.push(['N', s]);                          
                              
                           }
                       }
                      
                       /* add order total*/
                       printFormat.push(['N', LINE_SEPARATOR]);
                      
                       var cursymbol = receipt.header.currencySymbol;

                       var subTotalStr = JSReceiptUtils.format('Sub Total (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.subTotal).toFixed(2),12,true);
                                        
                       for(var j=0; j<receipt.taxes.length; j++)
                       {
                           var tax = receipt.taxes[j];
                           var taxStr = JSReceiptUtils.format('Tax - ' + tax.name + ' (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(tax.amt).toFixed(3),12,true);
                           printFormat.push(['N', taxStr]);                          
                          
                       }
                      
                       //var taxTotalStr = JSReceiptUtils.format('Tax (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.taxTotal).toFixed(2),12,true);
                       var discountStr = JSReceiptUtils.format('Discount (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.discountAmt).toFixed(2),12,true);
                       var writeOffStr = JSReceiptUtils.format('Write Off (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.writeOffAmt).toFixed(2),12,true);
                      
                       var totalStr = JSReceiptUtils.format('Grand Total (' + cursymbol + ')',LINE_WIDTH-18) + JSReceiptUtils.format(receipt.header.qtyTotal,6,true)+ JSReceiptUtils.format(Number(receipt.header.grandTotal).toFixed(2),12,true)
                      
                       printFormat.push(['N', subTotalStr]);
                       //printFormat.push(['N', taxTotalStr]);
                      
                       if(receipt.header.discountAmt > 0){
                           printFormat.push(['N', discountStr]);
                       }
                      
                       if(receipt.header.writeOffAmt > 0){
                           printFormat.push(['N', writeOffStr]);
                       }                  

                       printFormat.push(['N', LINE_SEPARATOR]);
                       printFormat.push(['B', totalStr]);
                       printFormat.push(['N', LINE_SEPARATOR]);
                       printFormat.push(['N', '']);

                       var paymentAmtStr = JSReceiptUtils.format('Amt Paid (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.payAmt).toFixed(2),12,true);
                       var dueAmtStr = JSReceiptUtils.format('Amt Due (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.openAmt).toFixed(2),12,true);
                      
                       printFormat.push(['N', paymentAmtStr]);
                      
                       if(receipt.header.docStatus == 'CO' && (receipt.header.paymentRule == 'P' || receipt.header.paymentRule == 'M'))
                       printFormat.push(['N', dueAmtStr]);

                       /* add payment details*/
                      
                       if(receipt.payments.length > 0){
                      
                           var cashAmtStr = JSReceiptUtils.format('Cash',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.cashAmt).toFixed(2),12,true);
                           var cashTenderedStr = JSReceiptUtils.format('Cash Tendered',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountTendered).toFixed(2),12,true);
                           var changeStr = JSReceiptUtils.format('Cash Refunded',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountRefunded).toFixed(2),12,true);
                           var cardAmtStr = JSReceiptUtils.format('Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.cardAmt).toFixed(2),12,true);
                           var externalCardAmtStr = JSReceiptUtils.format('External Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.externalCardAmt).toFixed(2),12,true);
                           var chequeAmtStr = JSReceiptUtils.format('Cheque',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.chequeAmt).toFixed(2),12,true);
                           var voucherAmtStr = JSReceiptUtils.format('Voucher',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.voucherAmt).toFixed(2),12,true);
                              
                           if(receipt.header.cashAmt > 0){
                               printFormat.push(['N', cashAmtStr]);
                           }
                              
                           if(receipt.header.orderType == 'POS Order' && receipt.header.amountTendered > 0){
                               printFormat.push(['N', cashTenderedStr]);
                               printFormat.push(['N', changeStr]);
                           }
                          
                           if(receipt.header.cardAmt > 0){
                               printFormat.push(['N', cardAmtStr]);
                           }
                          
                           if(receipt.header.externalCardAmt > 0){
                               printFormat.push(['N', externalCardAmtStr]);
                           }
                          
                           if(receipt.header.chequeAmt > 0){
                               printFormat.push(['N', chequeAmtStr]);
                           } 
                          
                           if(receipt.header.voucherAmt > 0){
                               printFormat.push(['N', voucherAmtStr]);
                           }
                      
                       }
                      
                       if(isCreditCardTransaction)
                       {
                           if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS")
                           {
                               printFormat.push(['FEED']);
                               printFormat.push(['FEED']);
                               printFormat.push(['B', 'Transaction Type: ' + receipt.header.transactionType]);
                               printFormat.push(['B', 'Merchant Location Code: ' + receipt.header.orgId]);
                               printFormat.push(['B', JSReceiptUtils.format("Entry: " + receipt.header.entryType,LINE_WIDTH)]);
                               printFormat.push(['B', JSReceiptUtils.format("Approval Code: " + receipt.header.creditCardAuthorizationCode,LINE_WIDTH)]);
                               printFormat.push(['B', JSReceiptUtils.format("Transaction ID: " + receipt.header.transactionId,LINE_WIDTH)]);
                               printFormat.push(['N', JSReceiptUtils.format("I agree to pay above total amount ",LINE_WIDTH)]);
                               printFormat.push(['N', JSReceiptUtils.format("according to card issuer agreement.",LINE_WIDTH)]);
                               printFormat.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.LINE_FEED]);
                               printFormat.push(['B', JSReceiptUtils.format("Signature:___________________",LINE_WIDTH)]);
                        }
                           else
                           {
                               printFormat.push(['FEED']);
                               printFormat.push(['FEED']);
                               printFormat.push(['B', JSReceiptUtils.format("Signature:___________________",LINE_WIDTH)]);
                               if(receipt.header.creditCardAccountHolderName != null && receipt.header.creditCardAccountHolderName != 'null')
                               printFormat.push(['B', JSReceiptUtils.format(receipt.header.creditCardAccountHolderName,LINE_WIDTH)]);
                               printFormat.push(['B', JSReceiptUtils.format("Authorisation: " + receipt.header.creditCardAuthorizationCode,LINE_WIDTH)]);
                               printFormat.push(['N', JSReceiptUtils.format("Buyer agrees to pay total amount above according to ",LINE_WIDTH)]);
                               printFormat.push(['N', JSReceiptUtils.format("cardholder agreement with issuer.",LINE_WIDTH)]);
                           }
                       }                                     
                      

                       printFormat.push(['FEED']);
                       printFormat.push(['FEED']);
                       if(receipt.header.receiptFooterMsg){
                    	   
                    	   var lines = JSReceiptUtils.splitIntoLines(receipt.header.receiptFooterMsg, LINE_WIDTH);
                    	   
                    	   if (lines.length > 0)
                           {
                         	  for(var i=0; i<lines.length; i++)
                         	  {
                         		  printFormat.push(['S', lines[i]]);
                         	  }
                           }
                       }
                       else{
                           printFormat.push(['S', "Thank you for shopping. See you soon."]);
                       }
                      
                       /*printing comments*/
                       var comments = $$('.comment');
                       var users = $$('.comment .user');
                       var dates = $$('.comment .date');
                       var messages = $$('.comment .message');
                      
                       for(var i=0; i<comments.length; i++){
                           var user = users[i].innerHTML.stripTags().strip();
                           var date = dates[i].innerHTML.strip();
                           var message = messages[i].innerHTML.strip();
                          
                           printFormat.push(['FEED']);
                           printFormat.push(['B', JSReceiptUtils.format(user,LINE_WIDTH - 20) + JSReceiptUtils.format(date,20,true)]);
                           printFormat.push(['S', message]);
                           printFormat.push(['FEED']);
                       }
                    /*-----------------------------------------------------------------------------------------*/
                
               var request = '<pre>';
                       
                for(var i=0; i<printFormat.length; i++){
                    var line = printFormat[i];
                   
                    if(line.length == 1)
                    {
                        var command = line[0];
                       
                        switch(command){
                            case 'FEED' :
                                request += '<br>';
                                break;
                            case 'SEPARATOR' :
                                request += '<div>' + LINE_SEPARATOR + '</div>';
                                break;
                            case 'CENTER' :
                                request += '<center>';
                                break;
                            case 'LEFT' :
                                request += "<div style='float:left;'>";
                                break;
                            case 'RIGHT' :
                                request += "<div style='float:right;'>";
                                break;                           
                        }
                    }
                    else
                    {
                        var font = line[0];
                        var text = line[1];
                       
                        if(text == null) continue;
                       
                        if(line.length > 2){
                            var label = line[2];
                            text = label + text;
                        }
                       
                        var textElementJSON = '';
                       
                        switch(font){
                            /*normal*/
                            case 'N':
                            	textElementJSON =  textElementJSON + '<div>';
                                textElementJSON = textElementJSON + text + '</div>';
                            	break;   
                           
                            /*bold*/
                            case 'B':
                                textElementJSON =  textElementJSON + '<b>';
                                textElementJSON = textElementJSON + text + '</b>';
                                break;   
                           
                            /*invert*/
                            case 'I':
                                textElementJSON = textElementJSON + '<i>';
                                textElementJSON = textElementJSON + text + '</i>';
                                break;   
                           
                            /*underline*/
                            case 'U':
                                textElementJSON = textElementJSON + '<u>';
                                textElementJSON = textElementJSON + text + '</u>';
                                break;   
                           
                            /*small*/
                            case 'S':
                                textElementJSON = textElementJSON + "<div font-size:'small;'>";
                                textElementJSON = textElementJSON + text;
                                break;
                               
                            /*header 1*/
                            case 'H1':
                                textElementJSON = textElementJSON + "<h1>";
                                textElementJSON = textElementJSON + text + '</h1>';
                                break;
                               
                            /*header 2*/
                            case 'H2':
                            	textElementJSON = textElementJSON + "<h2>";
                            	textElementJSON = textElementJSON + text + '</h2>';
                                break;                               
                               
                            /*header 3*/
                            case 'H3':
                            	textElementJSON = textElementJSON + "<h3>";
                            	textElementJSON = textElementJSON + text + '</h3>';
                                break;
                               
                            /*header 4*/
                            case 'H4':
                            	textElementJSON = textElementJSON + "<h4>";
                            	textElementJSON = textElementJSON + text + '</h4>';
                                break;   
                        }
                       
                        request += textElementJSON;
                        request += '<br>';
                    }
                }
                
                request += '</pre>';
                
                return request;
            	
            },
           
            formatAsRaw:function(){
                               
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
                var isCreditCardTransaction = false;

                if(receipt.header.cardAmt != 0){
                    isCreditCardTransaction = true;
                }               
               
                var headers = [
                       /* client & org info */
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       [Printer_ESC_COMMANDS.FONT_H2_BOLD, receipt.header.client],
                       [Printer_ESC_COMMANDS.FONT_H2, receipt.header.orgName]
                   ];
               
                if(receipt.header.orgAddress1){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.header.orgAddress1]);   
                }
               
               
                if(receipt.header.orgAddress2){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.header.orgAddress2]);   
                }
               
                if(receipt.header.orgCity){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.header.orgCity]);               
                }
               
                if(receipt.header.orgPhone){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'Phone: ' + receipt.header.orgPhone]);
                }
               
                if(receipt.header.orgFax){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'FAX: ' + receipt.header.orgFax]);
                }
               
                if(receipt.header.orgTaxId){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'TaxNo: ' + receipt.header.orgTaxId]);
                }
                      
                var orderHeader = [
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       /* order header */
                       [Printer_ESC_COMMANDS.FONT_BIG_BOLD, receipt.header.title],
                       //[Printer_ESC_COMMANDS.LEFT_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format((receipt.header.soTrx ? 'Customer' : 'Vendor') + ': ' + receipt.header.bpName + ((receipt.header.bpName2 != null && receipt.header.bpName2.length > 0) ? (' ' + receipt.header.bpName2) : ''),LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Terminal: ' + receipt.header.terminal,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Payment: ' + receipt.header.paymentRuleName + " " + receipt.header.creditCardDetails,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(receipt.header.dateOrdered,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                       /* order body */
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       [Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, (JSReceiptUtils.format('Name',LINE_WIDTH-26)+ JSReceiptUtils.format('Price',8,true) + JSReceiptUtils.format('Qty',6,true)+ JSReceiptUtils.format('Total',12,true))],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]
                      
                   ];
                  
                   headers = headers.concat(orderHeader);
                  
                   /* add order body */
                   for(var i=0; i<receipt.lines.length; i++){
                       var line = receipt.lines[i];
                      
                       var text = line.description || line.productName;
                       while(text.length > (LINE_WIDTH-26))
                       {
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(text,LINE_WIDTH)]);
                           text = text.substr(LINE_WIDTH);
                       }
                      
                       var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(line.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format(line.qtyEntered,6,true)+ JSReceiptUtils.format(Number(line.lineNetAmt).toFixed(2),12,true));
                      
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, s]);
                       
                       if(line.discountMessage != null){
                    	   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(line.discountMessage,LINE_WIDTH)]);
                       }
                      
                       if(line.boms != null)
                       for(var j=0; j<line.boms.length; j++){
                           var bom = line.boms[j];
                          
                           var text = " " + (bom.description || bom.productName);
                           while(text.length > (LINE_WIDTH-26))
                           {
                               headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(text,LINE_WIDTH)]);
                               text = text.substr(LINE_WIDTH);
                           }
                          
                           var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(bom.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format(bom.qtyEntered,6,true)+ JSReceiptUtils.format(Number(bom.lineNetAmt).toFixed(2),12,true));
                          
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, s]);                          
                          
                       }
                      
                       if(line.modifiers != null)
                       for(var j=0; j<line.modifiers.length; j++){
                           var modifier = line.modifiers[j];
                          
                           var text = " " + (modifier.description || modifier.productName);
                           while(text.length > (LINE_WIDTH-26))
                           {
                               headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(text,LINE_WIDTH)]);
                               text = text.substr(LINE_WIDTH);
                           }
                          
                           var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(modifier.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format('',6,true)+ JSReceiptUtils.format(Number(modifier.lineNetAmt).toFixed(2),12,true));
                          
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, s]);                          
                          
                       }
                   }
                  
                   /* add order total*/
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]);
                  
                   var cursymbol = receipt.header.currencySymbol;

                   var subTotalStr = JSReceiptUtils.format('Sub Total (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.subTotal).toFixed(2),12,true);
                  
                  
                   for(var j=0; j<receipt.taxes.length; j++)
                   {
                       var tax = receipt.taxes[j];
                       var taxStr = JSReceiptUtils.format('Tax - ' + tax.name + ' (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(tax.amt).toFixed(3),12,true);
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, taxStr]);                          
                      
                   }
                  
                   //var taxTotalStr = JSReceiptUtils.format('Tax (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.taxTotal).toFixed(2),12,true);
                   var discountStr = JSReceiptUtils.format('Discount (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.discountAmt).toFixed(2),12,true);
                   var writeOffStr = JSReceiptUtils.format('Write Off (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.writeOffAmt).toFixed(2),12,true);
                  
                   var totalStr = JSReceiptUtils.format('Grand Total (' + cursymbol + ')',LINE_WIDTH-18) + JSReceiptUtils.format(receipt.header.qtyTotal,6,true)+ JSReceiptUtils.format(Number(receipt.header.grandTotal).toFixed(2),12,true)
                  
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, subTotalStr]);
                   //headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, taxTotalStr]);
                  
                   if(receipt.header.discountAmt > 0){
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, discountStr]);
                   }
                  
                   if(receipt.header.writeOffAmt > 0){
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, writeOffStr]);
                   }                  

                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]);
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, totalStr]);
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]);
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, '']);

                   var paymentAmtStr = JSReceiptUtils.format('Amt Paid (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.payAmt).toFixed(2),12,true);
                   var dueAmtStr = JSReceiptUtils.format('Amt Due (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.openAmt).toFixed(2),12,true);
                  
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, paymentAmtStr]);
                  
                   if(receipt.header.docStatus == 'CO' && (receipt.header.paymentRule == 'P' || receipt.header.paymentRule == 'M'))
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, dueAmtStr]);

                   /* add payment details*/
                  
                   if(receipt.payments.length > 0){
                  
                       var cashAmtStr = JSReceiptUtils.format('Cash',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.cashAmt).toFixed(2),12,true);
                       var cashTenderedStr = JSReceiptUtils.format('Cash Tendered',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountTendered).toFixed(2),12,true);
                       var changeStr = JSReceiptUtils.format('Cash Refunded',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountRefunded).toFixed(2),12,true);
                       var cardAmtStr = JSReceiptUtils.format('Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.cardAmt).toFixed(2),12,true);
                       var externalCardAmtStr = JSReceiptUtils.format('External Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.externalCardAmt).toFixed(2),12,true);
                       var chequeAmtStr = JSReceiptUtils.format('Cheque',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.chequeAmt).toFixed(2),12,true);
                       var voucherAmtStr = JSReceiptUtils.format('Voucher',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.voucherAmt).toFixed(2),12,true);
                          
                       if(receipt.header.cashAmt > 0){
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, cashAmtStr]);
                       }
                          
                       if(receipt.header.orderType == 'POS Order' && receipt.header.amountTendered > 0){
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, cashTenderedStr]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, changeStr]);
                       }
                      
                       if(receipt.header.cardAmt > 0){
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, cardAmtStr]);
                       }
                      
                       if(receipt.header.externalCardAmt > 0){
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, externalCardAmtStr]);
                       }
                      
                       if(receipt.header.chequeAmt > 0){
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, chequeAmtStr]);
                       } 
                      
                       if(receipt.header.voucherAmt > 0){
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, voucherAmtStr]);
                       }
                  
                   }
                  
                   if(isCreditCardTransaction)
                   {
                       if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS")
                    {
                        headers.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.LINE_FEED]);
                        headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, 'Transaction Type: ' + receipt.header.transactionType]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, 'Merchant Location Code: ' + receipt.header.orgId]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format("Entry: " + receipt.header.entryType,LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format("Approval Code: " + receipt.header.creditCardAuthorizationCode,LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format("Transaction ID: " + receipt.header.transactionId,LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format("I agree to pay above total amount ",LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format("according to card issuer agreement.",LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.LINE_FEED]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format("Signature:___________________",LINE_WIDTH)]);
                    }
                       else
                       {
                           headers.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.LINE_FEED]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format("Signature:___________________",LINE_WIDTH)]);
                           if(receipt.header.creditCardAccountHolderName != null && receipt.header.creditCardAccountHolderName != 'null')
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format(receipt.header.creditCardAccountHolderName,LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format("Authorisation: " + receipt.header.creditCardAuthorizationCode,LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format("Buyer agrees to pay total amount above according to ",LINE_WIDTH)]);
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format("cardholder agreement with issuer.",LINE_WIDTH)]);
                       }
                   }    
                   
                   var barcodeLengthMap = ['\x04','\x05','\x06','\x07','\x08','\x09','\x0A','\x0B','\x0C','\x0D','\x0E','\x0F'];
                   var barcodeLength = barcodeLengthMap[receipt.header.documentNo.length - 4];
                   
                   var barcode = '\x1D' + 'h' + '\x64' + '\x1D' + 'w' + '\x02' + '\x1D' + 'H' + '\x02' + '\x1D' + 'k' + '\x45' + barcodeLength + receipt.header.documentNo;
                   headers.push([Printer_ESC_COMMANDS.LINE_FEED, barcode]);                  
                  

                   headers.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.LINE_FEED]);
                   if(receipt.header.receiptFooterMsg){
                       headers.push([Printer_ESC_COMMANDS.FONT_SMALL, receipt.header.receiptFooterMsg]);
                   }
                   else{
                       headers.push([Printer_ESC_COMMANDS.FONT_SMALL, "Thank you for shopping. See you soon."]);
                   }
                  
                   /*printing comments*/
                   var comments = $$('.comment');
                   var users = $$('.comment .user');
                   var dates = $$('.comment .date');
                   var messages = $$('.comment .message');
                  
                   for(var i=0; i<comments.length; i++){
                       var user = users[i].innerHTML.stripTags().strip();
                       var date = dates[i].innerHTML.strip();
                       var message = messages[i].innerHTML.strip();
                      
                       headers.push([Printer_ESC_COMMANDS.LINE_FEED,'']);
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format(user,LINE_WIDTH - 20) + JSReceiptUtils.format(date,20)]);
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, message]);
                       headers.push([Printer_ESC_COMMANDS.LINE_FEED,'']);
                   }
                  
                   headers.push([Printer_ESC_COMMANDS.PAPER_CUT, '']);
                  
                   /* open cash drawer */
                   if(this.options.OPEN_CASHDRAWER){
                       headers.push([Printer_ESC_COMMANDS.OPEN_DRAWER, '']);
                   }                  
                  
                var raw = '';
                   for(var i=0; i<headers.length; i++){
                       raw = raw + headers[i][0] + headers[i][1] + Printer_ESC_COMMANDS.LINE_FEED;
                   }
                return raw;
            },
           
            formatAsHTML:function(){
                this.setOptions();
               
                var LINE_WIDTH = 40; /*this.options.LINE_WIDTH;*/
               
                var receipt = this.data;               
                var headers = [
                      
                       ['<div style="text-align:center;">'],
                      
                       /* client & org info */
                       ['<hr>'],
                       ['<h1>', receipt.header.client, '</h1>'],
                       ['<h2>', receipt.header.orgName, '</h2>'],
                       ['<h3>', receipt.header.orgAddress1, '</h3>']];
               
                if(receipt.header.orgAddress2){
                    headers.push(['<h3>', receipt.header.orgAddress2, '</h3>']);
                }
               
                if(receipt.header.orgCity){
                    headers.push(['<h3>', receipt.header.orgCity, '</h3>']);
                }
               
                if(receipt.header.orgPhone){
                    headers.push(['<div style="text-align:center;">', 'Phone: ' + receipt.header.orgPhone, '</div>']);
                }
               
                if(receipt.header.orgFax){
                    headers.push(['<div style="text-align:center;">', 'FAX: ' + receipt.header.orgFax, '</div>']);
                }
               
                if(receipt.header.orgTaxId){
                    headers.push(['<div style="text-align:center;">', 'TaxNo: ' + receipt.header.orgTaxId, '</div>']);
                }
               
                   headers.push(['<hr>']);   
                  
                   /* order header */
                   var orderHeader = [
                       ['<h3>', receipt.header.title, '</h3>'],
                       [JSReceiptUtils.format((receipt.header.soTrx ? 'Customer' : 'Vendor') + ': ' + receipt.header.bpName + ((receipt.header.bpName2 != null && receipt.header.bpName2.length > 0) ? (' ' + receipt.header.bpName2) : ''),LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Terminal: ' + receipt.header.terminal,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Payment: ' + receipt.header.paymentRuleName + ' ' + receipt.header.creditCardDetails,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format(receipt.header.dateOrdered,LINE_WIDTH), '<br/>'],
                       /* order body */
                       ['<hr>'],
                       ['<strong>', (JSReceiptUtils.format('Name',LINE_WIDTH-26)+ JSReceiptUtils.format('Price',8,true) + JSReceiptUtils.format('Qty',6,true)+ JSReceiptUtils.format('Total',12,true)), '</strong>'],
                    ['<br/>'],                  
                    ['<hr>']
                      
                   ];
                  
                   headers = headers.concat(orderHeader);
                  
                                     
                   /* add order body */
                   for(var i=0; i<receipt.lines.length; i++){
                       var line = receipt.lines[i];
                      
                       var text = line.description;
                       while(text.length > (LINE_WIDTH-26))
                       {
                           headers.push([JSReceiptUtils.format(text,LINE_WIDTH),'<br/>']);
                           text = text.substr(LINE_WIDTH);
                       }
                      
                       var s = (JSReceiptUtils.format(text,LINE_WIDTH-26)+ JSReceiptUtils.format(Number(line.priceEntered).toFixed(2),8,true) + JSReceiptUtils.format(line.qtyEntered,6,true)+ JSReceiptUtils.format(Number(line.lineNetAmt).toFixed(2),12,true));
                      
                       headers.push([s,'<br/>']);
                   }
                  
                   /* add order total*/
                headers.push(['<hr>']);
               
                var cursymbol = receipt.header.currencySymbol;

                   var subTotalStr = JSReceiptUtils.format('Sub Total (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.subTotal).toFixed(2),12,true);
                   var taxTotalStr = JSReceiptUtils.format('Tax (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.taxTotal).toFixed(2),12,true);
                   var discountStr = JSReceiptUtils.format('Discount (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.discountAmt).toFixed(2),12,true);
                   var writeOffStr = JSReceiptUtils.format('Write Off (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.writeOffAmt).toFixed(2),12,true);
                  
                   var totalStr = JSReceiptUtils.format('Grand Total (' + cursymbol + ')',LINE_WIDTH-18) + JSReceiptUtils.format(receipt.header.qtyTotal,6,true)+ JSReceiptUtils.format(Number(receipt.header.payAmt).toFixed(2),12,true)
                  
                   headers.push([subTotalStr,'<br/>']);
                   headers.push([taxTotalStr,'<br/>']);
                  
                   if(receipt.header.discountAmt > 0){
                       headers.push([discountStr,'<br/>']);
                   }
                  
                   if(receipt.header.writeOffAmt > 0){
                       headers.push([writeOffStr,'<br/>']);
                   }

                   headers.push(['<hr>']),
                   headers.push(['<strong>', totalStr, '</strong>','<br/>']);
                   headers.push(['<hr>']),
                   headers.push(['<br/>']);

                  

                   /* add payment details*/
                  
                   if(receipt.payments.length > 0){
                  
                       var paymentAmtStr = JSReceiptUtils.format('Payment Amt (' + cursymbol + ')',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.payAmt).toFixed(2),12,true);
                       var cashTenderedStr = JSReceiptUtils.format('Cash Tendered',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountTendered).toFixed(2),12,true);
                       var changeStr = JSReceiptUtils.format('Cash Refunded',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.amountRefunded).toFixed(2),12,true);
                       var cardAmtStr = JSReceiptUtils.format('Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.cardAmt).toFixed(2),12,true);
                       var externalCardAmtStr = JSReceiptUtils.format('External Card',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.externalCardAmt).toFixed(2),12,true);
                       var chequeAmtStr = JSReceiptUtils.format('Cheque',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.chequeAmt).toFixed(2),12,true);
                       var voucherAmtStr = JSReceiptUtils.format('Voucher',LINE_WIDTH-12) + JSReceiptUtils.format(Number(receipt.header.voucherAmt).toFixed(2),12,true);
   
                       headers.push([paymentAmtStr,'<br/>']);
                      
                       if(receipt.header.amountTendered > 0){
                           headers.push([cashTenderedStr,'<br/>']);
                           headers.push([changeStr,'<br/>']);
                       }
                      
                       if(receipt.header.cardAmt > 0){
                           headers.push([cardAmtStr,'<br/>']);
                       }
                      
                       if(receipt.header.externalCardAmt > 0){
                           headers.push([externalCardAmtStr,'<br/>']);
                       }
                      
                       if(receipt.header.chequeAmt > 0){
                           headers.push([chequeAmtStr,'<br/>']);
                       }
                      
                       if(receipt.header.voucherAmt > 0){
                           headers.push([voucherAmtStr,'<br/>']);
                       }
                   }

                   headers.push(['<br/>', '<br/>']);                  
                  
                   if(receipt.header.receiptFooterMsg){
                       headers.push(['<small>', receipt.header.receiptFooterMsg, '</small>']);
                   }
                   else{
                       headers.push(['<small>', "Thank you for shopping. See you soon.", '</small>']);
                   }
                  

                   headers.push(['</div>']);
                  
                var html = '';
                   for(var i=0; i<headers.length; i++){
                    for(var h in headers[i]){
                        var field = headers[i][h];

                        if (field == null || typeof(field) == "function") {
                            continue;
                        }
                       
                        html = html + field;
                    }                  
                   }

                return '<pre>' + html + '</pre>';
            }
    };
   
   
    /* Close Till Receipt */
    var CloseTill = {
            test:function(){
            var json = {"summary":{"avgQtyPerOrder":"5.00","totalProductSold":"15","avgProductPrice":"2.74","totalNumberRefund":"0","totalNumberOfOrders":"3","totalDiscountGiven":"0.00"},"lines":[{"sales":1410.93,"employeeName":"Admin"}],"header":{"terminalName":"Terminal 1","cardAmt":"26.1000","cashDifference":"-1368.2500","grossSales":"47.67","netSales":"41.09","cashSales":"0","cashAmtEntered":"-1368.2500","endingBalance":"1832.2500","chequeAmt":"0","beginningBalance":"1000","openingDate":"2010-01-08 01:16:47.0","transferAmt":null,"closingDate":"2010-01-14 18:07:44.0","cashier":"Rita"}};
           
            /*var json = {"summary":{'avgQtyPerOrder' : avgQtyPerOrder, 'totalProductSold' : totalProductSold, 'avgProductPrice' : avgProductPrice,'totalNumberRefund' : totalNumberRefund, 'totalNumberOfOrders' : totalNumberOfOrders, 'totalDiscountGiven' : totalDiscountGiven},
            "lines":[{'sales' : sales, 'employeeName' : employeeName}],
            "header":{'terminalName' : terminalName, 'cardAmt' : cardAmt, 'cashDifference' : cashDifference , 'grossSales' : grossSales, 'netSales' : netSales, 'cashSales' : cashSales,  'cashAmtEntered' : cashAmtEntered, 'endingBalance' : endingBalance, 'chequeAmt' : chequeAmt, 'beginningBalance' : beginningBalance,'openingDate' : openingDate, 'transferAmt' : transferAmt, 'closingDate' : closingDate, 'cashier' : cashier}}  */                                                                                                                                                                                                                                                                        

            var txt = CloseTill.format(json);
                PrinterManager.sendPrintJob(txt);
            },
            format:function(json){
                               
                var LINE_WIDTH = PrinterManager.getLineWidth();
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var buffer = [
                              [''],
                              [Printer_ESC_COMMANDS.FONT_H1_BOLD, Printer_ESC_COMMANDS.CENTER_ALIGN, "Close Till Receipt", Printer_ESC_COMMANDS.FONT_NORMAL , Printer_ESC_COMMANDS.LEFT_ALIGN],
                              [LINE_SEPARATOR],
                              [Printer_ESC_COMMANDS.FONT_NORMAL_BOLD + JSReceiptUtils.format(("Store:  "+ removeDiacritics(json.header.storeName)),LINE_WIDTH), Printer_ESC_COMMANDS.FONT_NORMAL],
                              [Printer_ESC_COMMANDS.FONT_NORMAL_BOLD + JSReceiptUtils.format(("Terminal:  "+ removeDiacritics(json.header.terminalName)),LINE_WIDTH), Printer_ESC_COMMANDS.FONT_NORMAL],
                              [JSReceiptUtils.format(("Cashier:   "+ removeDiacritics(json.header.cashier)),LINE_WIDTH)],
                              [JSReceiptUtils.format(("Opened at: "+ json.header.openingDate),LINE_WIDTH)],
                              [JSReceiptUtils.format(("Closed at: "+ json.header.closingDate),LINE_WIDTH)],
                              [LINE_SEPARATOR],
                             
                              [JSReceiptUtils.format(("Beginning Balance:        "+ json.header.beginningBalance),LINE_WIDTH)],
                             
                              [JSReceiptUtils.format(("Cash Payments:            "+ json.header.cashPayments),LINE_WIDTH)],
                              [JSReceiptUtils.format(("Cash Adjustments:         "+ json.header.cashAdjustments),LINE_WIDTH)],
                              [JSReceiptUtils.format(("Cash Balance:             "+ json.header.cashBalance),LINE_WIDTH)],
                              
                              [JSReceiptUtils.format(("Cash amount entered:      "+ json.header.cashAmtEntered),LINE_WIDTH)],
                              [Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format(("Cash Difference:          "+ json.header.cashDifference),LINE_WIDTH)],
                              [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Transfer Amount:         "+ json.header.transferAmt),LINE_WIDTH)],
                              [JSReceiptUtils.format(("Ending Balance:           "+ json.header.endingBalance),LINE_WIDTH)],
                              [JSReceiptUtils.format(("Card Amount:              "+ json.header.cardAmt),LINE_WIDTH)],
                              [JSReceiptUtils.format(("Cheque Amount:            "+ json.header.chequeAmt),LINE_WIDTH)],
                              [JSReceiptUtils.format(("External Credit Card Amt: "+ json.header.externalCreditCardAmt),LINE_WIDTH)]
                             ];
               
                if (json.header.showExternalCardDetails)
                {
                	buffer.push([JSReceiptUtils.format(("External Credit Card Amt Entered: "+ json.header.externalCreditCardAmtEntered),LINE_WIDTH)]);
        			buffer.push([JSReceiptUtils.format(("External Credit Card Amt Difference: "+ json.header.externalCreditCardAmtDifference),LINE_WIDTH)]);
                }
                
                buffer.push([JSReceiptUtils.format(("Voucher Amount:           "+ json.header.voucherAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Voucher Amount Entered:   "+ json.header.voucherAmtEntered),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Voucher Difference:       "+ json.header.voucherDifference),LINE_WIDTH)]);
                
                buffer.push([JSReceiptUtils.format(("Mercury Gift Card Amount:         "+ json.header.giftCardAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Gift Card Amount:         "+ json.header.posteritaGiftCardAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("SKWallet Amount:          "+ json.header.skwalletAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Zapper Amount:            "+ json.header.zapperAmt),LINE_WIDTH)]);
        		buffer.push([JSReceiptUtils.format(("Loyalty Amount:            "+ json.header.loyaltyAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Coupon Amount:            "+ json.header.couponAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Deposit Amount:            "+ json.header.depositAmt),LINE_WIDTH)]);
                
                /* mobile payments */
                buffer.push([JSReceiptUtils.format(("MCB Juice Amount:            "+ json.header.mcbJuiceAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("MY.T Money Amount:            "+ json.header.mytMoneyAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Emtel Money Amount:            "+ json.header.emtelMoneyAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Gifts.mu Amount:            "+ json.header.giftsMuAmt),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("MIPS Amount:            "+ json.header.mipsAmt),LINE_WIDTH)]);
                
                buffer.push([LINE_SEPARATOR]);
               
                buffer.push([Printer_ESC_COMMANDS.FONT_H2_BOLD, "Summary"]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL,LINE_SEPARATOR]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Total Payments:  " + json.header.totalPayment),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Total Gross Sales:  " + json.header.grossSales),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format(("Sales & Payments Difference:          " +json.header.paymentDifference),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Total Tax:          " +json.header.taxAmt),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Total Net Sales:    " +json.header.netSales),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Cash Difference:    " + json.header.cashDifference),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Voucher Difference: "+ json.header.voucherDifference),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Total Credit sales: "+ json.header.totalCreditSales),LINE_WIDTH)]);
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(("Total Credit sales Paid: "+ json.header.totalCreditSalesPaid),LINE_WIDTH)]);               
                
                
                buffer.push([LINE_SEPARATOR]);
               
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD , Printer_ESC_COMMANDS.FONT_H2 , "Employees  Sales", Printer_ESC_COMMANDS.FONT_NORMAL]);
                
                
                for(var i=0; i<json.lines.length; i++)
                {
                    var line = json.lines[i];
                    buffer.push([JSReceiptUtils.format(line.employeeName,LINE_WIDTH -10), JSReceiptUtils.format(line.sales,10)]);
                }
               
                buffer.push([LINE_SEPARATOR]);
                                           
                buffer.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD , Printer_ESC_COMMANDS.FONT_H2 , "Stats", Printer_ESC_COMMANDS.FONT_NORMAL]);
                buffer.push([JSReceiptUtils.format(("Total No. of receipts:            " + json.summary.totalNumberOfOrders),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Total No. of items sold:     " + json.summary.totalProductSold),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Avg. No. of Items per Receipt:     " + json.summary.avgQtyPerOrder),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Avg. Receipt Amount:          " + json.summary.avgProductPrice),LINE_WIDTH)]);
                
                buffer.push([JSReceiptUtils.format(("Total discount given:           " + json.summary.totalDiscountGiven),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("	System discount:           " + json.summary.totalDiscountGiven),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("	al discount given:           " + json.summary.totalDiscountGiven),LINE_WIDTH)]);
                
                buffer.push([JSReceiptUtils.format(("Total No. of refunds:           " + json.summary.totalNumberRefund),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Total No. of items returned: " + json.summary.totalProductReturned),LINE_WIDTH)]);
                
                buffer.push([JSReceiptUtils.format(("Total No. of open drawer: " + json.summary.totalOpenDrawer),LINE_WIDTH)]);
                buffer.push([JSReceiptUtils.format(("Total No. of reprints: " + json.summary.totalRePrint),LINE_WIDTH)]);
                
                buffer.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.PAPER_CUT, Printer_ESC_COMMANDS.LINE_FEED]);
               
                var txt = '';
                   for(var i=0; i<buffer.length; i++){
                    for(var j in buffer[i]){
                        var field = buffer[i][j];

                        if (field == null || typeof(field) == "function") {
                            continue;
                        }
                       
                        txt = txt + field;
                    }
                   
                    txt = txt + Printer_ESC_COMMANDS.LINE_FEED;
                   }
       
                return txt;
            },
            
            formatAsWebPRNT:function(json){
                
                var LINE_WIDTH = PrinterManager.getLineWidth();
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var printFormat = [
                              ['FEED'],
                              ['CENTER'],
                              ['N',LINE_SEPARATOR],
                              ['H1', 'Close Till Receipt'],
                              ['N',LINE_SEPARATOR],
                              ['B',JSReceiptUtils.format(("Store:"),10) + JSReceiptUtils.format(removeDiacritics(json.header.storeName),LINE_WIDTH-10,true)],
                              ['B',JSReceiptUtils.format(("Terminal:"),10) + JSReceiptUtils.format(removeDiacritics(json.header.terminalName),LINE_WIDTH-10,true)],
                              ['N',JSReceiptUtils.format(("Cashier:"),10) + JSReceiptUtils.format(removeDiacritics(json.header.cashier),LINE_WIDTH-10, true)],
                              ['N',JSReceiptUtils.format(("Opened at:"),LINE_WIDTH-22) + JSReceiptUtils.format((json.header.openingDate),22, true)],
                              ['N',JSReceiptUtils.format(("Closed at:"),LINE_WIDTH-22) + JSReceiptUtils.format((json.header.closingDate),22, true)],
                              ['N',LINE_SEPARATOR],
                             
                              ['N',JSReceiptUtils.format(("Beginning Balance:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.beginningBalance).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Cash Payments:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.cashPayments).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Cash Adjustments:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.cashAdjustments).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Cash Balance:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.cashBalance).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Cash amount entered:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.cashAmtEntered).toFixed(2),10, true)],
                              ['B',JSReceiptUtils.format(("Cash Difference:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.cashDifference).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Transfer Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.transferAmt).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Ending Balance:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.endingBalance).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Card Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.cardAmt).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("Cheque Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.chequeAmt).toFixed(2),10, true)],
                              ['N',JSReceiptUtils.format(("External Credit Card Amt:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.externalCreditCardAmt).toFixed(2),10, true)]
                             ];
               
                
                if (json.header.showExternalCardDetails)
                {
                	printFormat.push(['N',JSReceiptUtils.format(("External Credit Card Amt Entered:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.externalCreditCardAmtEntered).toFixed(2),10, true)]);
                	printFormat.push(['N',JSReceiptUtils.format(("External Credit Card Amt Difference:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.externalCreditCardAmtDifference).toFixed(2),10, true)]);
                }
                
                printFormat.push(['N',JSReceiptUtils.format(("Voucher Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.voucherAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Voucher Amount Entered:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.voucherAmtEntered).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Voucher Difference:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.voucherDifference).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Mercury Gift Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.giftCardAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Gift Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.posteritaGiftCardAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Loyalty Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.loyaltyAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Coupon Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.couponAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Deposit Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.depositAmt).toFixed(2),10, true)]);
                
                /* mobile payments */
                printFormat.push(['N',JSReceiptUtils.format(("MCB Juice Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.mcbJuiceAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("MY.T Money Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.mytMoneyAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Emtel Money Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.emtelMoneyAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("Gifts.mu Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.giftsMuAmt).toFixed(2),10, true)]);
                printFormat.push(['N',JSReceiptUtils.format(("MIPS Amount:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.mipsAmt).toFixed(2),10, true)]);
                
                printFormat.push(['N',LINE_SEPARATOR]);
               
                printFormat.push(['H2', "Summary"]);
                printFormat.push(['N',LINE_SEPARATOR]);
                printFormat.push(['N', JSReceiptUtils.format(("Total Payments:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.totalPayment).toFixed(2),10, true)]);
                printFormat.push(['N', JSReceiptUtils.format(("Total Gross Sales:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.grossSales).toFixed(2),10, true)]);
                printFormat.push(['B', JSReceiptUtils.format(("Sales & Payments Difference:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.paymentDifference).toFixed(2),10, true)]);
                printFormat.push(['N', JSReceiptUtils.format(("Total Tax:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.taxAmt).toFixed(2),10, true)]);
                printFormat.push(['N', JSReceiptUtils.format(("Total Net Sales:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.netSales).toFixed(2),10, true)]);
                printFormat.push(['N', JSReceiptUtils.format(("Cash Difference:"),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.cashDifference).toFixed(2),10, true)]);
                printFormat.push(['N', JSReceiptUtils.format(("Voucher Difference: "),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.voucherDifference).toFixed(2),10, true)]);
                
                printFormat.push(['N', JSReceiptUtils.format(("Total Credit sales: "),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.totalCreditSales).toFixed(2),10, true)]);
                printFormat.push(['N', JSReceiptUtils.format(("Total Credit sales Paid: "),LINE_WIDTH-10) + JSReceiptUtils.format(Number(json.header.totalCreditSalesPaid).toFixed(2),10, true)]);
                
                printFormat.push(['N', LINE_SEPARATOR]);
                printFormat.push(['H2', "Employees  Sales"]);
                
                
                for(var i=0; i<json.lines.length; i++)
                {
                    var line = json.lines[i];
                    printFormat.push(['N', JSReceiptUtils.format(removeDiacritics(line.employeeName), LINE_WIDTH -10) + JSReceiptUtils.format(line.sales,10, true)]);
                }
               
                printFormat.push(['N',LINE_SEPARATOR]);
                                           
                printFormat.push(['H2',"Stats"]);
                
                var a = [
							['N','Total No. of receipts:', json.summary.totalNumberOfOrders],
							['N', "Avg. No. of items per receipt:", json.summary.avgQtyPerOrder],
							['N', "Avg. Receipt Amount:", json.summary.avgProductPrice],
							
							['N', "Total discount:", json.summary.totalDiscountGiven],							
							['N', "Discount by cashier:", json.summary.userDiscount],
							
							['N', "Total No. of refunds:", json.summary.totalNumberRefund],
							['N', "Total No. of open drawer:", json.summary.totalOpenDrawer],
							['N', "Total No. of reprints:", json.summary.totalRePrint],
							

							['N','Total No. of items sold:',json.summary.totalNoOfItemsSold],
							['N', "Total No. of items returned:", json.summary.totalNoOfItemsReturned],
							
							['N', "Total No. of services sold:", json.summary.totalNoOfServicesSold],
							['N', "Total No. of services returned:", json.summary.totalNoOfServicesReturned],
							
							['N', "Total No. of draft orders:", json.summary.totalNoOfDraftOrders],
							['N', "Total No. of open orders:", json.summary.totalNoOfOpenOrders]
                         ];
                
                for(var i=0; i<a.length; i++)
                {
                	printFormat.push([a[i][0], JSReceiptUtils.format((a[i][1]),LINE_WIDTH-10) + JSReceiptUtils.format((a[i][2]),10,true)]);
                }
                
                if( json.discountCodes && json.discountCodes.length > 0 )
                {
                	var discountCode;
                	
                	printFormat.push(['FEED']);
                	printFormat.push(['H2',"*** Discount Codes ***"]);
                	printFormat.push(['FEED']);
                	
                	for(var i = 0; i < json.discountCodes.length; i++)
                	{
                		discountCode = json.discountCodes[i];
                		
                		printFormat.push(["N", JSReceiptUtils.format( discountCode["qty"] + "x" + removeDiacritics(discountCode["name"]) , LINE_WIDTH-10 ) + 
                			JSReceiptUtils.format( discountCode["amt"] ,10 ,true ) ]);
                		
                	}
                	
                }
                              
                printFormat.push(['FEED']);
                printFormat.push(['PAPER_CUT']);
                
                      
                return printFormat;
            },
           
            formatPseudoPDF : function(json)
            {
                var LINE_WIDTH = PrinterManager.getLineWidth();
               
                var buffer = [
                                  ['<div class="profiledifference">'],
                                  ['<div class="totaldifference">'+Translation.translate("total.difference")+'</div>'],
                                  ['<div class="profilerate">' + JSReceiptUtils.format((json.header.totalDifference),LINE_WIDTH) + '</div>'],
                                  ['<div class="invoice">'],
                                      ['<div class="invoiceleft">'+Translation.translate("cash.difference")+'</div>'],
                                      ['<div class="invoiceright">' + JSReceiptUtils.format((json.header.cashDifference),LINE_WIDTH) + '</div>'],
                                  ['</div>'],
                                  
                                   ['<div class="invoice">'],
                                      ['<div class="invoiceleft">'+Translation.translate("voucher.difference")+'</div>'],
                                      ['<div class="invoiceright">' + JSReceiptUtils.format((json.header.voucherDifference),LINE_WIDTH) + '</div>'],
                                  ['</div>'],
                                  
                                   ['<div class="invoice">'],
                                      ['<div class="invoiceleft">'+Translation.translate("adjustments.in.cash")+'</div>'],
                                      ['<div class="invoiceright">' + JSReceiptUtils.format((json.header.cashAdjustments),LINE_WIDTH) + '</div>'],
                                  ['</div>'],

                                   ['<div class="invoice">'],
                                      ['<div class="invoiceleft">'+Translation.translate("sales.at.close.till")+'</div>'],
                                      ['<div class="invoiceright">' + JSReceiptUtils.format((json.header.grossSales),LINE_WIDTH) + '</div>'],
                                  ['</div>'],
                                 
                                  ['<div class="border_center">'],['</div>'],
                                  ['<div class="totaldifference">'+Translation.translate("employee.sales.capital")+'</div>']
                                     ];
                       
                        for(var i=0; i<json.lines.length; i++)
                        {
                            var line = json.lines[i];
                            buffer.push(['<div class="invoice">']);
                            buffer.push(['<div class="invoiceleft">',JSReceiptUtils.format(removeDiacritics(line.employeeName),LINE_WIDTH -10),'</div>']);
                            buffer.push(['<div class="invoiceright">',JSReceiptUtils.format(line.sales,10),'</div>']);
                            buffer.push(['</div>']);
                        }
                       
                        buffer.push(['</div>']);
                                                                                           
                var txt = '';
                   for(var i=0; i<buffer.length; i++){
                    for(var j in buffer[i]){
                        var field = buffer[i][j];

                        if (field == null || typeof(field) == "function") {
                            continue;
                        }
                       
                        txt = txt + field;
                    }
                   }
       
                return txt;
            },
           
            formatHTML:function(json){
               
                var LINE_WIDTH = PrinterManager.getLineWidth();
               
                var buffer = [
                              ['<div class="receiptimg">'],
                              ['<div class="h2 header-1">' + Translation.translate("run.end.receipt") + '</div>' + ((json.header.uuid != null && json.header.uuid.length > 0) ? '<div class="h2">(Offline)</div>' : '')],
                              ['</div>'],
                              ['<div class="white_bar2">'],
                             
                              ['<div class="receipt receipt-1" style="width:99% !important;">','<div class="right_side">'],
                              
	                              [' <div class="terms">'],
	                              ['<div class="leftdiv boldtext">' + Translation.translate("store") + '</div>'],
	                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.storeName),LINE_WIDTH) + '</div>'],
	                              ['</div>'],
	                              
                                  [' <div class="terms">'],
                                  ['<div class="leftdiv boldtext">' + Translation.translate("terminal") + '</div>'],
                                  ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.terminalName),LINE_WIDTH) + '</div>'],
                                  ['</div>'],
                                 
                                  [' <div class="terms">'],
                                  ['<div class="leftdiv">' + Translation.translate("cashier") + '</div>'],
                                  ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashier),LINE_WIDTH) + '</div>'],
                                  ['</div>'],
                                 
                                  [' <div class="terms">'],
                                  ['<div class="leftdiv">' + Translation.translate("open.at") + '</div>'],
                                  ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.openingDate),LINE_WIDTH) + '</div>'],
                                  ['</div>'],
                                 
                                  [' <div class="terms">'],
                                  ['<div class="leftdiv">' + Translation.translate("close.at") + '</div>'],
                                  ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.closingDate),LINE_WIDTH) + '</div>'],
                                  ['</div>'],
                              ['</div>','</div>'],
                             
                              ['<div class="receipt receipt-2" style="width:99% !important;">','<div class="right_side">'],
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("beginning.balance") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.beginningBalance),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              /*
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("cash.in") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashIn),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("cash.out") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashOut),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                              */
                              
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("cash.payments") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashPayments),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("cash.adjustments") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashAdjustments),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("cash.balance") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashBalance),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("cash.amount.entered") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashAmtEntered),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv boldtext">' + Translation.translate("cash.diff") + '</div>'],
                              ['<div class="rightdiv boldtext">' + JSReceiptUtils.format((json.header.cashDifference),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("transfer.amt") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.transferAmt),LINE_WIDTH) + '</div>'],
                              ['</div>'],

                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("ending.balance") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.endingBalance),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("card.amt") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cardAmt),LINE_WIDTH) + '</div>'],
                              ['</div>'],
                             
                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("cheque.amt") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.chequeAmt),LINE_WIDTH) + '</div>'],
                              ['</div>'],

                              [' <div class="terms">'],
                              ['<div class="leftdiv">' + Translation.translate("external.credit.card.amt") + '</div>'],
                              ['<div class="rightdiv">' + JSReceiptUtils.format((json.header.externalCreditCardAmt),LINE_WIDTH) + '</div>'],
                              ['</div>']
                             ];
               
                if (json.header.showExternalCardDetails)
                {
                	buffer.push([' <div class="terms">']);
                	buffer.push(['<div class="leftdiv">' + Translation.translate("external.credit.card.amt.entered") + '</div>']);
                    buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.externalCreditCardAmtEntered),LINE_WIDTH) + '</div>']);
                    buffer.push(['</div>']);
                   

                    buffer.push([' <div class="terms">']);
                    buffer.push(['<div class="leftdiv boldtext">' + Translation.translate("external.credit.card.amt.diff") + '</div>']);
                    buffer.push(['<div class="rightdiv boldtext">' + JSReceiptUtils.format((json.header.externalCreditCardAmtDifference),LINE_WIDTH) + '</div>']);
                    buffer.push(['</div>']);
                }
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("voucher.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.voucherAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
               

                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("voucher.amt.entered") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.voucherAmtEntered),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
               

                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv boldtext">' + Translation.translate("voucher.diff") + '</div>']);
                buffer.push(['<div class="rightdiv boldtext">' + JSReceiptUtils.format((json.header.voucherDifference),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("mercury.gift.card.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.giftCardAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                

                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("gift.card.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.posteritaGiftCardAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                /*buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("skwallet.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.skwalletAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);*/
                
                /*buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("zapper.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.zapperAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);*/
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("loyalty.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.loyaltyAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("coupon.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.couponAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("deposit.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.depositAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                /* mobile payments */
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("mcb.juice.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.mcbJuiceAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("myt.money.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.mytMoneyAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("emtel.money.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.emtelMoneyAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("gifts.mu.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.giftsMuAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
               
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("mips.amt") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.mipsAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
               
            buffer.push(['</div>','</div>']);
               
            buffer.push(['<div class="subheading header-2">' + Translation.translate("summary") + '</div>']);
        	buffer.push(['<div class="receipt receipt-3" style="width:99% !important;">','<div class="right_side">']);
               
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("total.payment") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.totalPayment),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("total.gross.sales") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.grossSales),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);

                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("sales.payments.difference") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.paymentDifference),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("total.tax") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.taxAmt),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
               
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("total.net.sales") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.netSales),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
               
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("cash.diff") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.cashDifference),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
               
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("voucher.diff") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.voucherDifference),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("total.credit.sales") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.totalCreditSales),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push([' <div class="terms">']);
                buffer.push(['<div class="leftdiv">' + Translation.translate("total.credit.sales.paid") + '</div>']);
                buffer.push(['<div class="rightdiv">' + JSReceiptUtils.format((json.header.totalCreditSalesPaid),LINE_WIDTH) + '</div>']);
                buffer.push(['</div>']);
                
                buffer.push(['</div>','</div>']);
               
            buffer.push(['<div class="subheading header-3">' + Translation.translate("employee.sales.capital") + '</div>']);
            buffer.push(['<div class="receipt receipt-4" style="width:99% !important;">','<div class="right_side">']);
                
                
                for(var i=0; i<json.lines.length; i++)
                {
                    var line = json.lines[i];
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">',JSReceiptUtils.format(line.employeeName,LINE_WIDTH -10),'</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format(line.sales,10),'</div>']);
                    buffer.push(['</div>']);
                }
               
                buffer.push(['</div>','</div>']);
               
                buffer.push(['<div class="subheading header-4">' + Translation.translate("stats.capital") + '</div>']);
                buffer.push(['<div class="receipt receipt-5" style="width:99% !important;">','<div class="right_side">']);
               
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("total.no.of.receipts") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalNumberOfOrders),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                                     
                   
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("average.no.of.items.per.receipt") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.avgQtyPerOrder),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                   
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("average.receipt.amount") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.avgProductPrice),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                   
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("total.discount.given") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalDiscountGiven),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("cashier.discount.given") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.userDiscount),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                   
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("total.no.of.refunds") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalNumberRefund),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']); 
                                      
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("total.no.of.items.sold") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalNoOfItemsSold),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("total.no.items.returned") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalNoOfItemsReturned),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("services.sold") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalNoOfServicesSold),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("services.returned") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalNoOfServicesReturned),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv">' + Translation.translate("non.selling.items") + '</div>']);
                    buffer.push(['<div class="rightdiv">',JSReceiptUtils.format((json.summary.totalNoOfNonSellingItems),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv boldtext">Total No of draft orders</div>']);
                    buffer.push(['<div class="rightdiv boldtext">',JSReceiptUtils.format((json.summary.totalNoOfDraftOrders),LINE_WIDTH),'</div>']);
                    buffer.push(['</strong></div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv boldtext">Total No of open orders</div>']);
                    buffer.push(['<div class="rightdiv boldtext">',JSReceiptUtils.format((json.summary.totalNoOfOpenOrders),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv boldtext">' + Translation.translate("total.no.open.drawer") + '</div>']);
                    buffer.push(['<div class="rightdiv boldtext">',JSReceiptUtils.format((json.summary.totalOpenDrawer),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
                    
                    buffer.push(['<div class="terms">']);
                    buffer.push(['<div class="leftdiv boldtext">' + Translation.translate("total.no.re.print") + '</div>']);
                    buffer.push(['<div class="rightdiv boldtext">',JSReceiptUtils.format((json.summary.totalRePrint),LINE_WIDTH),'</div>']);
                    buffer.push(['</div>']);
         
                   
                buffer.push(['</div>','</div>']);
                
                
                if( json.discountCodes && json.discountCodes.length > 0 )
                {
                	var discountCode;
                	
                	buffer.push(['<div class="subheading header-4">Discount Codes</div>']);
                    buffer.push(['<div class="receipt receipt-5" style="width:99% !important;">','<div class="right_side">']);
                	
                	for(var i = 0; i < json.discountCodes.length; i++)
                	{
                		discountCode = json.discountCodes[i];
                		
                		buffer.push(['<div class="terms">']);
                        buffer.push(['<div class="leftdiv">' + discountCode["qty"] + "x" + discountCode["name"] + '</div>']);
                        buffer.push(['<div class="rightdiv">',discountCode["amt"],'</div>']);
                        buffer.push(['</div>']);
                		
                	}
                	
                	buffer.push(['</div>','</div>']);
                	
                }
               
               
                
                var txt = '';
                   for(var i=0; i<buffer.length; i++){
                    for(var j in buffer[i]){
                        var field = buffer[i][j];

                        if (field == null || typeof(field) == "function") {
                            continue;
                        }
                       
                        txt = txt + field;
                    }
                   }
       
                return txt;
            }
           
    };
   
    /*Voucher*/
    var VoucherReceipt = {
            receipt:null,
            options:null,
            initialize:function(data, options){
                this.data = data;
                this.options = options || {};
            },
           
            setOptions:function(){
                this.options.LINE_WIDTH = PrinterManager.getLineWidth();
            },
           
            formatAsRaw:function(){
                               
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var headers = [
                       /* client & org info */
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       [Printer_ESC_COMMANDS.FONT_H2_BOLD, receipt.header.client],
                       [Printer_ESC_COMMANDS.FONT_H2, receipt.header.orgName],
                       [Printer_ESC_COMMANDS.FONT_BIG, receipt.header.orgAddress1]
                   ];
               
                if(receipt.header.orgAddress2){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.header.orgAddress2]);   
                }
               
                if(receipt.header.orgCity){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.header.orgCity]);               
                }
               
                if(receipt.header.orgPhone){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'Phone: ' + receipt.header.orgPhone]);
                }
               
                if(receipt.header.orgFax){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'FAX: ' + receipt.header.orgFax]);
                }
               
                if(receipt.header.orgTaxId){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'TaxNo: ' + receipt.header.orgTaxId]);
                }
                      
                var cursymbol = receipt.header.currencySymbol;
                var paymentAmt = new Number(0);
                if(receipt.payments.length > 0)
                {
                       paymentAmt = new Number(receipt.header.payAmt).toFixed(2);
                       paymentAmt = Math.abs(paymentAmt);
                }
                var paymentAmtStr = cursymbol + paymentAmt;
               
                var orderHeader = [
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       /* order header */
                       [Printer_ESC_COMMANDS.FONT_H1_BOLD, 'VOUCHER'],
                       [Printer_ESC_COMMANDS.FONT_H1_BOLD, paymentAmtStr],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       //[Printer_ESC_COMMANDS.LEFT_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Customer: ' + receipt.header.bpName + ((receipt.header.bpName2 != null && receipt.header.bpName2.length > 0) ? (' ' + receipt.header.bpName2) : ''),LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Terminal: ' + receipt.header.terminal,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Date Ordered: ' + receipt.header.dateOrdered,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                   ];
                  
                   headers = headers.concat(orderHeader);
                  
                   headers.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.LINE_FEED]);
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, JSReceiptUtils.format("Signature:_________________________",LINE_WIDTH)]);
                  
                   var disclaimer = "This voucher is not valid until it has been signed.";
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]);
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, disclaimer]);
                  
                   if(receipt.header.receiptFooterMsg){
                       headers.push([Printer_ESC_COMMANDS.FONT_SMALL, receipt.header.receiptFooterMsg]);
                   }
                   else{
                       headers.push([Printer_ESC_COMMANDS.FONT_SMALL, "Thank you for shopping. See you soon."]);
                   }
                  
                   headers.push([Printer_ESC_COMMANDS.PAPER_CUT, '']);
                  
                var raw = '';
                   for(var i=0; i<headers.length; i++){
                       raw = raw + headers[i][0] + headers[i][1] + Printer_ESC_COMMANDS.LINE_FEED;
                   }
                return raw;
            },
            
            formatAsWebPRNT:function(){
                
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var printFormat = [
                       /* client & org info */
                       ['FEED'],
                       ['CENTER'],
                       ['N',LINE_SEPARATOR],
                       ['H1', receipt.header.client],
                       ['H2', receipt.header.orgName],
                       ['B', receipt.header.orgAddress1]
                   ];
               
                if(receipt.header.orgAddress2){
                    printFormat.push(['B', receipt.header.orgAddress2]);   
                }
               
                if(receipt.header.orgCity){
                    printFormat.push(['B', receipt.header.orgCity]);               
                }
               
                if(receipt.header.orgPhone){
                    printFormat.push(['B', 'Phone: ' + receipt.header.orgPhone]);
                }
               
                if(receipt.header.orgFax){
                    printFormat.push(['B', 'FAX: ' + receipt.header.orgFax]);
                }
               
                if(receipt.header.orgTaxId){
                    printFormat.push(['B', 'TaxNo: ' + receipt.header.orgTaxId]);
                }
                      
                var cursymbol = receipt.header.currencySymbol;
                var paymentAmt = new Number(0);
                if(receipt.payments.length > 0)
                {
                       paymentAmt = new Number(receipt.header.payAmt).toFixed(2);
                       paymentAmt = Math.abs(paymentAmt);
                }
                var paymentAmtStr = cursymbol + paymentAmt;
               
                var orderHeader = [
                       ['N', LINE_SEPARATOR],
                       /* order header */
                       ['H1', 'VOUCHER'],
                       [Printer_ESC_COMMANDS.FONT_H1_BOLD, paymentAmtStr],
                       ['N', LINE_SEPARATOR],
                       //[Printer_ESC_COMMANDS.LEFT_ALIGN, ''],
                       ['N', JSReceiptUtils.format('Customer: ' + receipt.header.bpName + ((receipt.header.bpName2 != null && receipt.header.bpName2.length > 0) ? (' ' + receipt.header.bpName2) : ''),LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Terminal: ' + receipt.header.terminal,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Date Ordered: ' + receipt.header.dateOrdered,LINE_WIDTH)],
                   ];
                  
                   printFormat = printFormat.concat(orderHeader);
                  
                   printFormat.push(['FEED']);
                   printFormat.push(['N', JSReceiptUtils.format("Signature:_________________________",LINE_WIDTH)]);
                  
                   var disclaimer = "This voucher is not valid until it has been signed.";
                   printFormat.push(['N', LINE_SEPARATOR]);
                   
                  var lines = JSReceiptUtils.splitIntoLines(disclaimer, LINE_WIDTH);
                  
                  if (lines.length > 0)
                  {
                	  for(var i=0; i<lines.length; i++)
                	  {
                		  printFormat.push(['S', lines[i]]);
                	  }
                  }
                  
                   if(receipt.header.receiptFooterMsg){
                	   var lines = JSReceiptUtils.splitIntoLines(receipt.header.receiptFooterMsg, LINE_WIDTH);
                	   
                	   if (lines.length > 0)
                       {
                     	  for(var i=0; i<lines.length; i++)
                     	  {
                     		  printFormat.push(['S', lines[i]]);
                     	  }
                       }
                   }
                   else{
                       printFormat.push(['S', "Thank you for shopping. See you soon."]);
                   }
                  
                   
                   printFormat.push(['PAPER_CUT']);
                  
                /*var raw = '';
                   for(var i=0; i<printFormat.length; i++){
                       raw = raw + printFormat[i][0] + printFormat[i][1] + Printer_ESC_COMMANDS.LINE_FEED;
                   }
                return raw;*/
                
                return printFormat;
            }
    };
   
    /*Stock Transfer Receipt*/
    var StockTransferReceipt = {
            receipt:null,
            options:null,
            initialize:function(data, options){
                this.data = data;
                this.options = options || {};
            },
           
            setOptions:function(){
                this.options.LINE_WIDTH = PrinterManager.getLineWidth();
            },
           
            formatAsRaw:function(){
                               
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var headers = [
                       /* header info */
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_H2_BOLD, receipt.title],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       [Printer_ESC_COMMANDS.FONT_H2_BOLD, receipt.header.client],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('From ' + receipt.header.org.name,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('To ' + receipt.header.orgTo.name,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Doc No: ' + receipt.header.documentNo,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Date: ' + receipt.header.movementDate,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]
                   ];
                
               
                for(var i=0; i<receipt.lines.length; i++){
                       var line = receipt.lines[i];
                      
                       var text = line.description;
                       while(text.length > (LINE_WIDTH-8))
                       {
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(text,LINE_WIDTH)]);
                           text = text.substr(LINE_WIDTH);
                       }
                      
                       var s = (JSReceiptUtils.format(text,LINE_WIDTH-8) + JSReceiptUtils.format(line.qty,8,true));
                      
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, s]);
                   }
               
                var totalStr = (JSReceiptUtils.format('Total Received',LINE_WIDTH-8) + JSReceiptUtils.format(receipt.header.qtyTotal,8,true));               
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR);
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, totalStr);
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR);
            },
            
            formatAsWebPRNT:function(){
                
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var printFormat = [
                       /* header info */
                       ['CENTER'],
                       ['H2', receipt.title],
                       ['N', LINE_SEPARATOR],
                       ['H2', receipt.header.client],
                       ['N', JSReceiptUtils.format('From ' + receipt.header.org.name,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('To ' + receipt.header.orgTo.name,LINE_WIDTH)],
                       ['N', LINE_SEPARATOR],
                       ['N', JSReceiptUtils.format('Doc No: ' + receipt.header.documentNo,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Date: ' + receipt.header.movementDate,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Sales Rep: ' + receipt.header.salesRep,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                       ['N', LINE_SEPARATOR]
                   ];
               
                printFormat.push(['N', JSReceiptUtils.format('Name',LINE_WIDTH-8) + JSReceiptUtils.format('Qty',8)]);
                printFormat.push(['N', LINE_SEPARATOR]);
                
                for(var i=0; i<receipt.lines.length; i++){
                    var line = receipt.lines[i];
                   
                    var text = line.description;
                    /*while(text.length > (LINE_WIDTH-16))
                    {
                        printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                        text = text.substr(LINE_WIDTH-16);
                    }*/
                    
                    var lines = JSReceiptUtils.splitIntoLines(text, LINE_WIDTH-8);
             	   
             	   if (lines.length > 0)
                    {
                  	  for(var j=0; j<lines.length; j++)
                  	  {
                  		  if (lines.length == 1)
                  		  {
                  			printFormat.push(['N', (JSReceiptUtils.format(lines[j],LINE_WIDTH-8) + JSReceiptUtils.format(line.qty,8))]);
                  		  }
                  		  else
                  		  {
                  			printFormat.push(['N', (JSReceiptUtils.format(lines[j],LINE_WIDTH-8) + JSReceiptUtils.format('',8))]);
                  		  }
                  	  }
                    }
                    
             	   if (lines.length != 1)
	             	{
             		  var s = (JSReceiptUtils.format('',LINE_WIDTH-8) + JSReceiptUtils.format(line.qty,8));
                      printFormat.push(['N', s]);
	             	}
                    
                    if(receipt.receiptFooterMsg){
                 	   var lines = JSReceiptUtils.splitIntoLines(receipt.header.receiptFooterMsg, LINE_WIDTH);
                 	   
                 	   if (lines.length > 0)
                        {
                      	  for(var k=0; k<lines.length; k++)
                      	  {
                      		  printFormat.push(['S', lines[k]]);
                      	  }
                        }
                    }
                }
            
             var totalStr = (JSReceiptUtils.format('Total',LINE_WIDTH-8) + JSReceiptUtils.format(receipt.header.qtyTotal,8));               
             printFormat.push(['N', LINE_SEPARATOR]);
             printFormat.push(['N', totalStr]);
             printFormat.push(['N', LINE_SEPARATOR]);
             
                /*for(var i=0; i<receipt.lines.length; i++){
                       var line = receipt.lines[i];
                      
                       var text = line.description;
                       while(text.length > (LINE_WIDTH-8))
                       {
                           printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                           text = text.substr(LINE_WIDTH);
                       }
                      
                       var s = (JSReceiptUtils.format(text,LINE_WIDTH-8) + JSReceiptUtils.format(line.qty,8));
                      
                       printFormat.push(['N', s]);
                   }
               
                var totalStr = (JSReceiptUtils.format('Total',LINE_WIDTH-8) + JSReceiptUtils.format(receipt.header.qtyTotal,8));               
                printFormat.push('N', LINE_SEPARATOR);
                printFormat.push('N', totalStr);
                printFormat.push('N', LINE_SEPARATOR);*/
                
                return printFormat;
            }
    };
   
    /*Inventory Receipt*/
    var InventoryReceipt = {
            receipt:null,
            options:null,
            initialize:function(data, options){
                this.data = data;
                this.options = options || {};
            },
           
            setOptions:function(){
                this.options.LINE_WIDTH = PrinterManager.getLineWidth();
            },
           
            formatAsRaw:function(){
                               
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var headers = [
                       /* header info */
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_H1_BOLD, 'INVENTORY'],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       [Printer_ESC_COMMANDS.FONT_H2_BOLD, receipt.header.client],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(receipt.header.org.name,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(receipt.header.org.address,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(receipt.header.org.city,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(receipt.header.movementDate,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Counted by ' + receipt.header.salesRep,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],

                   ];
               
                headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Name',LINE_WIDTH-16) + JSReceiptUtils.format('Qty',8) + JSReceiptUtils.format('Qty',8)]);
                headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('',LINE_WIDTH-16) + JSReceiptUtils.format('Book',8) + JSReceiptUtils.format('Count',8)]);
               
                for(var i=0; i<receipt.lines.length; i++){
                       var line = receipt.lines[i];
                      
                       var text = line.description;
                       while(text.length > (LINE_WIDTH-16))
                       {
                           headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(text,LINE_WIDTH)]);
                           text = text.substr(LINE_WIDTH);
                       }
                      
                       var s = (JSReceiptUtils.format(text,LINE_WIDTH-16) + JSReceiptUtils.format(line.qtyBook,8,true) + JSReceiptUtils.format(line.qtyCount,8,true));
                       headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, s]);
                   }
               
                var totalStr = (JSReceiptUtils.format('Total',LINE_WIDTH-16) + JSReceiptUtils.format(receipt.header.qtyBookTotal,8,true) + JSReceiptUtils.format(receipt.header.qtyCountTotal,8,true));               
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR);
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, totalStr);
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR);
               
                var differenceStr = (JSReceiptUtils.format('Difference Qty',LINE_WIDTH-8) + JSReceiptUtils.format(receipt.header.differenceTotal,8,true));
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, differenceStr);
                headers.push(Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR);
                
                printFormat.push(['FEED']);
                
                /*printing comments*/
                var comments = $$('.comment');
                var users = $$('.comment .user');
                var dates = $$('.comment .date');
                var messages = $$('.comment .message');
               
                for(var i=0; i<comments.length; i++){
                    var user = users[i].innerHTML.stripTags().strip();
                    var date = dates[i].innerHTML.strip();
                    var message = messages[i].innerHTML.strip();
                   
                    printFormat.push(['FEED']);
                    printFormat.push(['B', JSReceiptUtils.format(user,LINE_WIDTH - 20) + JSReceiptUtils.format(date,20,true)]);
                    printFormat.push(['S', message]);
                    printFormat.push(['FEED']);
                }
            },
            
            formatAsWebPRNT:function(){
                
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var printFormat = [
                       /* header info */
                       ['CENTER'],
                       ['H1', 'INVENTORY'],
                       ['N', LINE_SEPARATOR],
                       ['H2', receipt.header.client],
                       ['N', JSReceiptUtils.format(receipt.header.org.name,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format(receipt.header.org.address,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format(receipt.header.org.city,LINE_WIDTH)],
                       ['N', LINE_SEPARATOR],
                       ['N', JSReceiptUtils.format('Order No: ' + receipt.header.documentNo,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Date: ' +receipt.header.movementDate,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Counted by: ' + receipt.header.salesRep,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Status: ' + receipt.header.docStatusName,LINE_WIDTH)],
                       ['N', LINE_SEPARATOR],

                   ];
               
                printFormat.push(['N', JSReceiptUtils.format('Name',LINE_WIDTH-16) + JSReceiptUtils.format('Qty ',8, true) + JSReceiptUtils.format('Qty ',8, true)]);
                printFormat.push(['N', JSReceiptUtils.format('',LINE_WIDTH-16) + JSReceiptUtils.format('Book',8, true) + JSReceiptUtils.format('Count',8, true)]);
                printFormat.push(['N', LINE_SEPARATOR]);
               
                for(var i=0; i<receipt.lines.length; i++){
                       var line = receipt.lines[i];
                      
                       var text = line.description;
                       /*while(text.length > (LINE_WIDTH-16))
                       {
                           printFormat.push(['N', JSReceiptUtils.format(text,LINE_WIDTH)]);
                           text = text.substr(LINE_WIDTH-16);
                       }*/
                       
                       var lines = JSReceiptUtils.splitIntoLines(text, LINE_WIDTH-16);
                	   
                	   if (lines.length > 1)
                       {
                     	  for(var j=0; j<lines.length; j++)
                     	  {
                     		  printFormat.push(['N', (JSReceiptUtils.format(lines[j],LINE_WIDTH-16) + JSReceiptUtils.format('',8) + JSReceiptUtils.format('',8))]);
                     	  }
                     	  
                     	 printFormat.push(['N',(JSReceiptUtils.format('',LINE_WIDTH-16) + JSReceiptUtils.format(line.qtyBook,8,true) + JSReceiptUtils.format(line.qtyCount,8,true))]);
                       }
                	   else
            		   {
                		   var s = (JSReceiptUtils.format(text,LINE_WIDTH-16) + JSReceiptUtils.format(line.qtyBook,8,true) + JSReceiptUtils.format(line.qtyCount,8,true));
                           printFormat.push(['N', s]);
            		   }
                       
                       
                       
                       if(receipt.receiptFooterMsg){
                    	   var lines = JSReceiptUtils.splitIntoLines(receipt.header.receiptFooterMsg, LINE_WIDTH);
                    	   
                    	   if (lines.length > 0)
                           {
                         	  for(var k=0; k<lines.length; k++)
                         	  {
                         		  printFormat.push(['S', lines[k]]);
                         	  }
                           }
                       }
                   }
               
                var totalStr = (JSReceiptUtils.format('Total',LINE_WIDTH-16) + JSReceiptUtils.format(receipt.header.qtyBookTotal,8,true) + JSReceiptUtils.format(receipt.header.qtyCountTotal,8,true));               
                printFormat.push(['N', LINE_SEPARATOR]);
                printFormat.push(['B', totalStr]);
                printFormat.push(['N', LINE_SEPARATOR]);
               
                var differenceStr = (JSReceiptUtils.format('Difference Qty',LINE_WIDTH-8) + JSReceiptUtils.format(receipt.header.differenceTotal,8,true));
                printFormat.push(['B', differenceStr]);
                printFormat.push(['N', LINE_SEPARATOR]);
                
                printFormat.push(['FEED']);
                                
                /*printing comments*/
                var comments = $$('.comment');
                var users = $$('.comment .user');
                var dates = $$('.comment .date');
                var messages = $$('.comment .message');
               
                for(var i=0; i<comments.length; i++){
                    var user = users[i].innerHTML.stripTags().strip();
                    var date = dates[i].innerHTML.strip();
                    var message = messages[i].innerHTML.strip();
                   
                    printFormat.push(['FEED']);
                    printFormat.push(['B', JSReceiptUtils.format(user,LINE_WIDTH - 20) + JSReceiptUtils.format(date,20,true)]);
                    printFormat.push(['S', message]);
                    printFormat.push(['FEED']);
                }
                
                printFormat.push(['PAPER_CUT']);
                
                return printFormat;
            }
    };
   
    var PaymentReceiptFormatter = {
            receipt:null,
            options:null,
            initialize:function(data, options){
                this.data = data;
                this.options = options || {};
            },
           
            setOptions:function(){
                this.options.LINE_WIDTH = PrinterManager.getLineWidth();
            },
           
            formatAsRaw:function(openDrawer){
                               
                this.setOptions();
               
                var LINE_WIDTH = this.options.LINE_WIDTH;
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var headers = [
                       /* client & org info */
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                    [Printer_ESC_COMMANDS.FONT_H2_BOLD, receipt.companyName]
                      // [Printer_ESC_COMMANDS.FONT_H2, receipt.orgName]
                   ];
               
                if(receipt.orgAddress1){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.orgAddress1]);   
                }
               
               
                if(receipt.orgAddress2){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.orgAddress2]);   
                }
               
                if(receipt.orgCity){
                    headers.push([Printer_ESC_COMMANDS.FONT_BIG, receipt.orgCity]);               
                }
               
                if(receipt.orgPhone){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'Phone: ' + receipt.orgPhone]);
                }
               
                if(receipt.orgFax){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'FAX: ' + receipt.orgFax]);
                }
               
                if(receipt.orgTaxId){
                    headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, 'TaxNo: ' + receipt.orgTaxId]);
                }
                      
                var orderHeader = [
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                       /* order header */
                       [Printer_ESC_COMMANDS.FONT_BIG_BOLD, 'Payment'],
                       //[Printer_ESC_COMMANDS.LEFT_ALIGN, ''],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format((receipt.isReceipt ? 'Customer' : 'Vendor') + ': ' + receipt.bpName + ((receipt.bpName2 != null && receipt.bpName2.length > 0) ? (' ' + receipt.bpName2) : ''),LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Terminal: ' + receipt.terminal,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Sales Rep: ' + receipt.salesRep,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Status: ' + receipt.docStatusName,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Tender Type: ' + receipt.tenderTypeName,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format('Payment No: ' + receipt.documentNo,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.FONT_NORMAL, JSReceiptUtils.format(receipt.paymentDate,LINE_WIDTH)],
                       [Printer_ESC_COMMANDS.CENTER_ALIGN, ''],
                      
                       /* order body */
                       [Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
                      
                   ];
                  
                   headers = headers.concat(orderHeader);
                  
                   /* add order body */
                  
                   /* add order total*/
                   var cursymbol = receipt.currencySymbol;

                   var totalStr = JSReceiptUtils.format('Amount Paid (' + cursymbol + ')',LINE_WIDTH-18) + JSReceiptUtils.format(Number(receipt.paymentAmt).toFixed(2),12,true)
                  

                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL_BOLD, totalStr]);
                   headers.push([Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]);

                   headers.push([Printer_ESC_COMMANDS.LINE_FEED, Printer_ESC_COMMANDS.LINE_FEED]);
                   if(receipt.receiptFooterMsg){
                       headers.push([Printer_ESC_COMMANDS.FONT_SMALL, receipt.receiptFooterMsg]);
                   }
                   else{
                       headers.push([Printer_ESC_COMMANDS.FONT_SMALL, "Thank you for shopping. See you soon."]);
                   }
                  
                   headers.push([Printer_ESC_COMMANDS.PAPER_CUT, '']);
                  
                   /* open cash drawer */
                   if(this.options.OPEN_CASHDRAWER){
                       headers.push([Printer_ESC_COMMANDS.OPEN_DRAWER, '']);
                   }                  
                  
                var raw = '';
                   for(var i=0; i<headers.length; i++){
                       raw = raw + headers[i][0] + headers[i][1] + Printer_ESC_COMMANDS.LINE_FEED;
                   }
                return raw;
            },
            
            formatAsWebPRNT:function(openDrawer){
                
                var LINE_WIDTH = PrinterManager.getLineWidth();
                var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);
               
                var receipt = this.data;
               
               
                var printFormat = [
                       /* client & org info */
                       ['CENTER'],
                       ['N', LINE_SEPARATOR],
                       ['H2', receipt.companyName]
                      // ['H2', receipt.orgName]
                   ];
               
                if(receipt.orgAddress1){
                    printFormat.push(['N', receipt.orgAddress1]);   
                }
               
               
                if(receipt.orgAddress2){
                    printFormat.push(['N', receipt.orgAddress2]);   
                }
               
                if(receipt.orgCity){
                    printFormat.push(['N', receipt.orgCity]);               
                }
               
                if(receipt.orgPhone){
                    printFormat.push(['N', 'Phone: ' + receipt.orgPhone]);
                }
               
                if(receipt.orgFax){
                    printFormat.push(['N', 'FAX: ' + receipt.orgFax]);
                }
               
                if(receipt.orgTaxId){
                    printFormat.push(['N', 'TaxNo: ' + receipt.orgTaxId]);
                }
                      
                var orderHeader = [
                       ['N', LINE_SEPARATOR],
                       /* order header */
                       ['B', 'Payment'],
                       //[Printer_ESC_COMMANDS.LEFT_ALIGN, ''],
                       ['N', JSReceiptUtils.format((receipt.isReceipt ? 'Customer' : 'Vendor') + ': ' + receipt.bpName + ((receipt.bpName2 != null && receipt.bpName2.length > 0) ? (' ' + receipt.bpName2) : ''),LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Terminal: ' + receipt.terminal,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Sales Rep: ' + receipt.salesRep,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Status: ' + receipt.docStatusName,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Tender Type: ' + receipt.tenderTypeName,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Payment No: ' + receipt.documentNo,LINE_WIDTH)],
                       ['N', JSReceiptUtils.format('Date:' + receipt.paymentDate,LINE_WIDTH)],
                      
                       /* order body */
                       ['N', LINE_SEPARATOR],
                      
                   ];
                  
                   printFormat = printFormat.concat(orderHeader);
                  
                   /* add order body */
                  
                   /* add order total*/
                   var cursymbol = receipt.currencySymbol;

                   var totalStr = JSReceiptUtils.format('Amount Paid (' + cursymbol + ')',LINE_WIDTH-18) + JSReceiptUtils.format(Number(receipt.paymentAmt).toFixed(2),12,true)
                  

                   printFormat.push(['B', totalStr]);
                   printFormat.push(['N', LINE_SEPARATOR]);

                   printFormat.push(['FEED']);
                   if(receipt.receiptFooterMsg){
                	   var lines = JSReceiptUtils.splitIntoLines(receipt.header.receiptFooterMsg, LINE_WIDTH);
                	   
                	   if (lines.length > 0)
                       {
                     	  for(var i=0; i<lines.length; i++)
                     	  {
                     		  printFormat.push(['S', lines[i]]);
                     	  }
                       }
                   }
                   else{
                       printFormat.push(['S', "Thank you for shopping. See you soon."]);
                   }
                   
                   printFormat.push(['PAPER_CUT']);
                  
                   if(openDrawer != null && openDrawer == true){
	                   	var openDrawerFormat = [['OPEN_DRAWER']];
	                   	openDrawerFormat = openDrawerFormat.concat(printFormat);
	                   	printFormat = openDrawerFormat;
                   } 
                   
                return printFormat;
            },
           
            formatAsHTML:function(){
                this.setOptions();
               
                var LINE_WIDTH = 40; /*this.options.LINE_WIDTH;*/
               
                var receipt = this.data;               
                var headers = [
                      
                       ['<div style="text-align:center;">'],
                      
                       /* client & org info */
                       ['<hr>'],
                       //['<h1>', receipt.header.client, '</h1>'],
                       ['<h2>', receipt.orgName, '</h2>'],
                       ['<h3>', receipt.orgAddress1, '</h3>']];
               
                if(receipt.orgAddress2){
                    headers.push(['<h3>', receipt.orgAddress2, '</h3>']);
                }
               
                if(receipt.orgCity){
                    headers.push(['<h3>', receipt.orgCity, '</h3>']);
                }
               
                if(receipt.orgPhone){
                    headers.push(['<div style="text-align:center;">', 'Phone: ' + receipt.orgPhone, '</div>']);
                }
               
                if(receipt.orgFax){
                    headers.push(['<div style="text-align:center;">', 'FAX: ' + receipt.orgFax, '</div>']);
                }
               
                if(receipt.orgTaxId){
                    headers.push(['<div style="text-align:center;">', 'TaxNo: ' + receipt.orgTaxId, '</div>']);
                }
               
                   headers.push(['<hr>']);   
                  
                   /* order header */
                   var orderHeader = [
                       ['<h3>', 'Payment', '</h3>'],
                       [JSReceiptUtils.format((receipt.soTrx ? 'Customer' : 'Vendor') + ': ' + receipt.bpName + ((receipt.bpName2 != null && receipt.bpName2.length > 0) ? (' ' + receipt.bpName2) : ''),LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Terminal: ' + receipt.terminal,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Sales Rep: ' + receipt.salesRep,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Status: ' + receipt.docStatusName,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Tender Type: ' + receipt.tenderTypeName, LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format('Order No: ' + receipt.documentNo,LINE_WIDTH), '<br/>'],
                       [JSReceiptUtils.format(receipt.paymentDate,LINE_WIDTH), '<br/>'],
                       /* order body */
                       ['<hr>'],
                       ['<strong>', (JSReceiptUtils.format('Name',LINE_WIDTH-26)+ JSReceiptUtils.format('Price',8,true) + JSReceiptUtils.format('Qty',6,true)+ JSReceiptUtils.format('Total',12,true)), '</strong>'],
                    ['<br/>'],                  
                    ['<hr>']
                      
                   ];
                  
                   headers = headers.concat(orderHeader);
                  
                                     
                   /* add order body */
                   /* add order total*/
               
                var cursymbol = receipt.currencySymbol;

                   var totalStr = JSReceiptUtils.format('Amount Paid (' + cursymbol + ')',LINE_WIDTH-18) + JSReceiptUtils.format(Number(receipt.paymentAmt).toFixed(2),12,true)
                  
                  
                   headers.push(['<hr>']),
                   headers.push(['<strong>', totalStr, '</strong>','<br/>']);
                   headers.push(['<hr>']),
                   headers.push(['<br/>']);

                  

                   /* add payment details*/
                  
                  
                   if(receipt.header.receiptFooterMsg){
                       headers.push(['<small>', receipt.receiptFooterMsg, '</small>']);
                   }
                   else{
                       headers.push(['<small>', "Thank you for shopping. See you soon.", '</small>']);
                   }
                  

                   headers.push(['</div>']);
                  
                var html = '';
                   for(var i=0; i<headers.length; i++){
                    for(var h in headers[i]){
                        var field = headers[i][h];

                        if (field == null || typeof(field) == "function") {
                            continue;
                        }
                       
                        html = html + field;
                    }                  
                   }

                return '<pre>' + html + '</pre>';
            }
    };
    
    var JSONToWebPRNT = {
    		
    		getWebPRNTRequest : function(printFormat)
    		{
    			var builder = new StarWebPrintBuilder();
                var request = "";
               
                for(var i=0; i<printFormat.length; i++){
                    var line = printFormat[i];
                   
                    if(line.length == 1)
                    {
                        var command = line[0];
                       
                        switch(command){
                            case 'FEED' :
                                request += builder.createFeedElement({line:1});
                                break;
                            case 'SEPARATOR' :
                                request += builder.createRuledLineElement({thickness:'thin', width:LINE_WIDTH});
                                break;
                            case 'CENTER' :
                                request += builder.createAlignmentElement({position:'center'});
                                break;
                            case 'LEFT' :
                                request += builder.createAlignmentElement({position:'left'});
                                break;
                            case 'RIGHT' :
                                request += builder.createAlignmentElement({position:'right'});
                                break;                           
                        }
                    }
                    else
                    {
                        var font = line[0];
                        var text = line[1];
                       
                        if(text == null) continue;
                       
                        if(line.length > 2){
                            var label = line[2];
                            text = label + text;
                        }
                       
                        var textElementJSON = {characterspace:0, linespace:32, codepage:'cp998', international:'usa', font:'font_a', width:1, height:1, emphasis:false, underline:false, invert:false, data:text};
                       
                        switch(font){
                            /*normal*/
                            case 'N':break;   
                           
                            /*bold*/
                            case 'B':
                                textElementJSON.emphasis = true;
                                break;   
                           
                            /*invert*/
                            case 'I':
                                textElementJSON.invert = true;
                                break;   
                           
                            /*underline*/
                            case 'U':
                                textElementJSON.underline = true;
                                break;   
                           
                            /*small*/
                            case 'S':
                                textElementJSON.font = 'font_b';
                                break;
                               
                            /*header 1*/
                            case 'H1':
                                textElementJSON.font = 'font_a';
                                textElementJSON.emphasis = true;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;
                               
                            /*header 2*/
                            case 'H2':
                                textElementJSON.font = 'font_a';
                                textElementJSON.emphasis = false;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;                               
                               
                            /*header 3*/
                            case 'H3':
                                textElementJSON.font = 'font_b';
                                textElementJSON.emphasis = true;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;
                               
                            /*header 4*/
                            case 'H4':
                                textElementJSON.font = 'font_b';
                                textElementJSON.emphasis = false;
                                textElementJSON.width = 2;
                                textElementJSON.height = 2;
                                break;   
                        }
                       
                        request += builder.createTextElement(textElementJSON);
                        request += builder.createFeedElement({line:1});
                    }
                }
               
                request += builder.createCutPaperElement({feed:true, type:'full'});
               
                return request;
    		}
    };