
var ReportItemDetails = {
		widgetUrl : null,
		reportUrl : null,
		
	displayProductDetails: function(productId)
	{
		var panel = new ProductInfoPanel();
		panel.getInfo(productId);
	},
	
	displayPartnerDetails : function(partnerId)
	{
		var panel = new BPInfoPanel();
		panel.setId(partnerId);
		panel.show();
	},
	
	displayOrderInfo : function(orderId)
	{
		var panel = new OrderInfoPanel(orderId);
		panel.getInfo();
	},
	
	/*	" +
	"displayProductDetailsByPrimaryGroup: function(primaryGroup)
	{
		var url = "PrimaryGroupDetails.do?primaryGroup=" + primaryGroup;
		ReportItemDetails.showData(url);
	},
	
	displayOrderHtmlPrint: function(orderId)
	{
		var url = "OrderHtmlPrint.do?orderId=" + orderId;
		ReportItemDetails.showData(url);
	},
	
	showData: function(url)
	{
		$('detailsFrame').src = url;
		$('detailsContainer').show();
	},
	
	displayBPartnerDetails: function(bPartnerId)
	{
		var url = "BPartnerDetails.do?bpartnerId=" + bPartnerId;
		ReportItemDetails.showData(url);
	},

	displayWidget: function(processKey, recordId)
	{
		ReportItemDetails.widgetUrl = 'WidgetAction.do?action=loadWidget&processKey=' + processKey + '&recordId='+ recordId;
		ReportItemDetails.showData('Widget.do');
	},
	*/
	retrieveWidget: function(processKey, params)
	{
		/*ReportItemDetails.widgetUrl = 'WidgetAction.do?action=retrieveWidget&processKey=' + processKey;
		
		for (var name in params)
		{
			var value = params[name];
			ReportItemDetails.widgetUrl = ReportItemDetails.widgetUrl + '&' + name + '=' + value;
		}
		
		ReportItemDetails.showData('Widget.do');
		*/
		if (processKey == 'EmployeeWidget')
		{
			ReportItemDetails.setCommissionPerEmployeeReportAction(params);
		}
	},
	
	/*
	getWidget: function(url)
	{
		
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'get', 
			onSuccess: ReportItemDetails.showWidget, 
			onFailure: ReportItemDetails.reportError
		});
	},*/
	
	setCommissionPerEmployeeReportAction: function(params)
	{
		ReportItemDetails.reportUrl = 'CommissionAction.do?action=getCommissionPerEmployeeReport';
		
		for (var name in params)
		{
			var value = params[name];
			ReportItemDetails.reportUrl = ReportItemDetails.reportUrl + '&' + name + '=' + value;
		}
		
		$('commissionPerEmployeeReportAction').style.display = 'inline';
		$('commissionPerEmployeeReportAction').href = ReportItemDetails.reportUrl;
	}
	
	/*,
	
	showWidget: function(response)
	{
		window.frames[0].$('receipt').innerHTML = response.responseText;
	},
	
	reportError: function(request)
	{
		alert(request.responseText);
	}*/
};