var UnPaidInvoice = Class.create({
	
	initialize : function(json) 
	{
		this.invoiceId = 0;
		this.date = null;
		this.documentNo = null;
		this.invoiceAmt = 0;
		this.openAmt = 0;
		this.applied = 0;
		this.checked = false;
		this.docType = null;
		
		this.init(json);
	},
	
	init : function(json)
	{
		if (json != null)
		{
			this.setInvoiceId(new Number(json.c_invoice_id));
			this.setDocumentNo(json.documentno);
			this.setInvoiceAmt(new Number(json.grandtotal));
			//this.setApplied(new Number(json.overunderamt));
			this.setApplied(new Number(0));
			this.setDate(json.dateinvoiced);
			this.setOpenAmt(new Number(json.open));
			this.docType = json.doctype;
		}
	},
	
	setDate : function(date)
	{
		this.date = date;
	},
	setDocumentNo : function(documentNo)
	{
		this.documentNo = documentNo;
	},
	setInvoiceAmt : function(invoiceAmt)
	{
		this.invoiceAmt = invoiceAmt;
	},
	setOpenAmt : function(openAmt)
	{
		this.openAmt = openAmt;
	},
	setApplied : function(applied)
	{
		this.applied = applied;
	},
	setChecked : function(isChecked)
	{
		this.checked = isChecked;
	},
	setInvoiceId : function(invoiceId)
	{
		this.invoiceId = invoiceId;
	},
	getInvoiceId : function()
	{
		return this.invoiceId;
	},
	getDate : function()
	{
		return this.date;
	},
	getDocumentNo : function()
	{
		return this.documentNo;
	},
	getInvoiceAmt : function()
	{
		return this.invoiceAmt;
	},
	getApplied : function()
	{
		return this.applied;
	},
	getOpenAmt : function()
	{
		return this.openAmt;
	},
	getChecked : function()
	{
		return this.checked;
	},
	getDocType : function()
	{
		return this.docType;
	},
	equals : function(unPaidInvoice)
	{
		return (this.getInvoiceId() == unPaidInvoice.getInvoiceId());
	}
});

var UnAllocatedPayment = Class.create({
	
	initialize : function(json)
	{
		this.paymentId = 0;
		this.date = null;
		this.documentNo = null;
		this.paymentAmt = 0;
		this.openAmt = 0;
		this.applied = 0;
		this.checked = false;
		
		this.init(json);
	},
	
	init : function(json)
	{
		if (json != null)
		{
			this.setPaymentId(new Number(json.c_payment_id));
			this.setDocumentNo(json.documentno);
			this.setPaymentAmt(new Number(json.payamt));
			this.setDate(json.datetrx);
			this.setOpenAmt(new Number(json.open));
		}
	},
	
	setDate : function(date)
	{
		this.date = date;
	},
	setDocumentNo : function(documentNo)
	{
		this.documentNo = documentNo;
	},
	setPaymentAmt : function(paymentAmt)
	{
		this.paymentAmt = paymentAmt;
	},
	setOpenAmt : function(openAmt)
	{
		this.openAmt = openAmt;
	},
	setApplied : function(applied)
	{
		this.applied = applied;
	},
	setChecked : function(isChecked)
	{
		this.checked = isChecked;
	},
	setPaymentId : function(paymentId)
	{
		this.paymentId = paymentId;
	},
	getPaymentId : function()
	{
		return this.paymentId;
	},
	getDate : function()
	{
		return this.date;
	},
	getDocumentNo : function()
	{
		return this.documentNo;
	},
	getPaymentAmt : function()
	{
		return this.paymentAmt;
	},
	getOpenAmt : function()
	{
		return this.openAmt;
	},
	getApplied : function()
	{
		return this.applied;
	},
	getChecked : function()
	{
		return this.checked;
	},
	equals : function(unAllocatedPayment)
	{
		return (this.getPaymentId() == unAllocatedPayment.getPaymentId());
	}
});

var UnPaidInvoiceEditor = Class.create({
	
	initialize : function(unPaidInvoice)
	{
		if (unPaidInvoice != null)
		{
			this.unPaidInvoice = unPaidInvoice;
			this.checkbox = null;
			this.inputApplied = null;
			this.initComponent();
			this.listeners = new ArrayList();
		}
	},
	
	initComponent : function()
	{
		this.checkbox = document.createElement('input');
		this.checkbox.setAttribute('type', 'checkbox');
		this.checkbox.setAttribute('id', this.unPaidInvoice.getInvoiceId() + 'checkbox');
		this.checkbox.onclick = this.setChecked.bind(this);
		
		this.inputApplied = document.createElement('input');
		this.inputApplied.setAttribute('type', 'text');
		this.inputApplied.setAttribute('id', this.unPaidInvoice.getInvoiceId() + 'input');
		this.inputApplied.setAttribute('readOnly', 'true');
		this.inputApplied.setAttribute('style', 'width:60px');
		this.inputApplied.setAttribute('class', 'numeric');
		this.inputApplied.onkeyup = this.setApplied.bind(this);
	},
	
	showError : function()
	{
		this.inputApplied.setStyle('background-color: #DD6666; color: #ffffff');
		this.inputApplied.onclick = function(e)
		{
			this.removeAttribute('style');
		};
	},
	
	getCheckbox : function()
	{
		return this.checkbox;
	},
	
	getInputApplied : function()
	{
		return this.inputApplied;
	},
	
	addListener : function(listener)
	{
		this.listeners.add(listener);
	},
	
	fireInvoiceChange : function()
	{
		for (var i=0; i<this.listeners.size(); i++)
		{
			var listener = this.listeners.get(i);
			listener.invoiceChange();
		}
	},
	
	setChecked : function()
	{
		var checked = this.checkbox.checked;
		this.unPaidInvoice.setChecked(checked);
		
		if (checked)
		{
			this.unPaidInvoice.setApplied(this.unPaidInvoice.getOpenAmt());
			this.inputApplied.value = this.unPaidInvoice.getOpenAmt();
			this.inputApplied.readOnly = false;
		}
		else
		{
			this.unPaidInvoice.setApplied(0);
			this.inputApplied.value = '';
			this.inputApplied.readOnly = true;
		}
		
		this.fireInvoiceChange();
	},
	
	setApplied : function(e)
	{
		var value = this.inputApplied.value;
		value = new Number(value);
		
		if(isNaN(value)) return;
		
		var openAmt = this.unPaidInvoice.getOpenAmt();
		if ((openAmt > 0 && value < 0) || openAmt < 0 && value > 0)
		{
			value = 0 - value;
		}
		
		/*this.inputApplied.value = value;*/
		
		if (value.abs() > openAmt.abs())
		{
			alert(Translation.translate("applied.amt.is.greater.than.open.amt","Applied amount is greater than open amount!"));
			this.inputApplied.value = this.unPaidInvoice.getOpenAmt();
			return;
		}
		
		if (value != this.unPaidInvoice.getApplied())
		{
			this.unPaidInvoice.setApplied(value);
		}
		
		this.fireInvoiceChange();
	},
	
	getUnPaidInvoice : function()
	{
		return this.unPaidInvoice;
	},
	
	equals : function(unPaidInvoiceEditor)
	{
		var unPaidInvoice = unPaidInvoiceEditor.getUnPaidInvoice();
		var _unPaidInvoice = this.getUnPaidInvoice();
		return (_unPaidInvoice.getInvoiceId() == unPaidInvoice.getInvoiceId());
	}
});

var UnPaidInvoiceManager = Class.create({
	
	initialize : function()
	{
		this.panel = $('unPaidInvoices');
		/*this.panelHeader = $('unpaid-invoice-table-header');*/
		this.totalOpenAmt = 0;
		this.totalAppliedAmt = 0;
		this.unPaidInvoiceEditors = new ArrayList(new UnPaidInvoiceEditor());
		this.stateChangeListeners = new ArrayList();
	},
	
	getTotalOpenAmt : function()
	{
		return this.totalOpenAmt;
	},
	
	getTotalAppliedAmt : function()
	{
		return this.totalAppliedAmt;
	},
	
	addStateChangeListener : function(listener)
	{
		this.stateChangeListeners.add(listener);
	},
	
	getInvoicesToAllocate : function()
	{
		var invoicesToAllocate = new ArrayList(new UnPaidInvoiceEditor());
		for (var i=0; i<this.unPaidInvoiceEditors.size(); i++)
		{
			var editor = this.unPaidInvoiceEditors.get(i);
			var unPaidInvoice = editor.getUnPaidInvoice();
			var checked = unPaidInvoice.getChecked();
			if (checked)
			{
				invoicesToAllocate.add(editor);
			}
		}
		return invoicesToAllocate;
	},
	
	validateInvoicesToAllocate : function()
	{
		var invoicesToAllocate = this.getInvoicesToAllocate();
		var valid = true;
		
		if (invoicesToAllocate.size() == 0)
		{
			alert(Translation.translate('no.invoices.selected','Please select an invoice.'));
			valid = false;
			return;
		}
		else
		{
			for (var i=0; i<invoicesToAllocate.size(); i++)
			{
				var editor = invoicesToAllocate.get(i);
				var unPaidInvoice = editor.getUnPaidInvoice();
				var applied = unPaidInvoice.getApplied();
				if (applied == 0)
				{
					editor.showError();
					valid = false;
					return;
				}
			}
		}
		return valid;
	},
	
	fireStateChange : function()
	{
		for (var i=0; i<this.stateChangeListeners.size(); i++)
		{
			var listener = this.stateChangeListeners.get(i);
			listener.stateChange();
		}
	},
	
	invoiceChange : function()
	{
		this.totalAppliedAmt = 0;
		
		for (var i=0; i<this.unPaidInvoiceEditors.size(); i++)
		{
			var editor = this.unPaidInvoiceEditors.get(i);
			var unPaidInvoice = editor.getUnPaidInvoice();
			var applied = unPaidInvoice.getApplied();
			
			this.totalAppliedAmt = this.totalAppliedAmt + applied;
		}
		
		this.fireStateChange();
	},
	
	clear : function()
	{
		this.totalOpenAmt = 0;
		this.totalAppliedAmt = 0;
		this.unPaidInvoiceEditors.clear();
		this.stateChangeListeners.clear();
		
		var length = this.panel.childNodes.length;
		
		if (length == 2)
			return;
		else
		{
			for(var i=0;i<length-2;i++)
			{
				this.panel.childNodes[i+2].innerHTML = '';
			}
		}
	},
	
	addInvoices : function(unPaidInvoicesList)
	{
		for(var i=0;i<unPaidInvoicesList.size();i++)
		{
			var unPaidInvoice = unPaidInvoicesList.get(i);
			var unPaidInvoiceEditor = new UnPaidInvoiceEditor(unPaidInvoice);
			this.addUnPaidInvoice(unPaidInvoiceEditor);
		}
	},
	
	addUnPaidInvoice : function(unPaidInvoiceEditor)
	{
		this.unPaidInvoiceEditors.add(unPaidInvoiceEditor);
		unPaidInvoiceEditor.addListener(this);
		
		var checkbox = unPaidInvoiceEditor.getCheckbox();
		var inputApplied = unPaidInvoiceEditor.getInputApplied();
		var unPaidInvoice = unPaidInvoiceEditor.getUnPaidInvoice();
		var invoiceId = unPaidInvoice.getInvoiceId();
		var date = unPaidInvoice.getDate();
		var documentNo = unPaidInvoice.getDocumentNo();
		var invoiceAmt = unPaidInvoice.getInvoiceAmt();
		var openAmt = unPaidInvoice.getOpenAmt();
		var applied = unPaidInvoice.getApplied();
		
		this.totalOpenAmt = this.totalOpenAmt + openAmt;
		this.totalAppliedAmt = this.totalAppliedAmt + applied;
		
		var tr = document.createElement('div');
		tr.setAttribute('id', invoiceId);
		tr.setAttribute('class', 'row');
		
		var tdSelect = document.createElement('div');
		tdSelect.setAttribute('class', 'col-md-1 col-xs-1 no-padding');
		
		var tdDate = document.createElement('div');
		tdDate.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
		
		var tdDocNo = document.createElement('div');
		tdDocNo.setAttribute('class', 'col-md-2 col-xs-2');
		
		var tdInvAmt = document.createElement('div');
		tdInvAmt.setAttribute('class', 'col-md-3 col-xs-3 no-padding');
		
		var tdOpen = document.createElement('div');
		tdOpen.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
		
		var tdApplied = document.createElement('div');
		tdApplied.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
		
		var spanDate = document.createElement('span');
		spanDate.innerHTML = date;
		
		var spanDocNo = document.createElement('span');
		spanDocNo.innerHTML = documentNo;
		
		var spanInvAmt = document.createElement('span');
		spanInvAmt.innerHTML = invoiceAmt;
		
		var spanOpen = document.createElement('span');
		spanOpen.innerHTML = openAmt;
		
		tdSelect.appendChild(checkbox);
		tdDate.appendChild(spanDate);
		tdDocNo.appendChild(spanDocNo);
		tdInvAmt.appendChild(spanInvAmt);
		tdOpen.appendChild(spanOpen);
		tdApplied.appendChild(inputApplied);
		
		tr.appendChild(tdSelect);
		tr.appendChild(tdDate);
		tr.appendChild(tdDocNo);
		tr.appendChild(tdInvAmt);
		tr.appendChild(tdOpen);
		tr.appendChild(tdApplied);
		
		this.panel.appendChild(tr);
		
	/*	var headerTds = this.panelHeader.getElementsByTagName('div');
		
		for(var i=0; i<headerTds.length; i++)
		{
			headerTds[i].style.borderBottomWidth='1px';
			headerTds[i].style.borderBottomColor='#000000';
		}*/
		
		var trs = this.panel.getElementsByClassName('row');
		
		for(var i=0; i<trs.length; i++)
		{
			if (i%2 == 0)
			{
				trs[i].className = 'row allocation-odd-row';
			}
			else
			{
				trs[i].className = 'row allocation-even-row';
			}
		}
	}
});

var UnAllocatedPaymentEditor = Class.create({
	
	initialize : function(unAllocatedPayment)
	{
		if (unAllocatedPayment != null)
		{
			this.unAllocatedPayment = unAllocatedPayment;
			this.checkbox = null;
			this.inputApplied = null;
			this.initComponent();
			this.listeners = new ArrayList();
		}
	},
	
	initComponent : function()
	{
		this.checkbox = document.createElement('input');
		this.checkbox.setAttribute('type', 'checkbox');
		this.checkbox.setAttribute('id', this.unAllocatedPayment.getPaymentId() + 'checkbox');
		this.checkbox.onclick = this.setChecked.bind(this);
		
		this.inputApplied = document.createElement('input');
		this.inputApplied.setAttribute('type', 'text');
		this.inputApplied.setAttribute('id', this.unAllocatedPayment.getPaymentId() + 'input');
		this.inputApplied.setAttribute('readOnly', 'true');
		this.inputApplied.setAttribute('style', 'width:60px');
		this.inputApplied.onkeyup = this.setApplied.bind(this);
	},

	showError : function()
	{
		this.inputApplied.setStyle('background-color: #DD6666; color: #ffffff');
		this.inputApplied.onclick = function(e)
		{
			this.removeAttribute('style');
		};
	},
	
	getCheckbox : function()
	{
		return this.checkbox;
	},
	
	getInputApplied : function()
	{
		return this.inputApplied;
	},
	
	addListener : function(listener)
	{
		this.listeners.add(listener);
	},
	
	firePaymentChange : function()
	{
		for (var i=0; i<this.listeners.size(); i++)
		{
			var listener = this.listeners.get(i);
			listener.paymentChange();
		}
	},
	
	setChecked : function()
	{
		var checked = this.checkbox.checked;
		this.unAllocatedPayment.setChecked(checked);
		if (checked)
		{
			this.unAllocatedPayment.setApplied(this.unAllocatedPayment.getOpenAmt());
			this.inputApplied.readOnly = false;
			this.inputApplied.value = this.unAllocatedPayment.getOpenAmt();
		}
		else
		{
			this.unAllocatedPayment.setApplied(0);
			this.inputApplied.readOnly = true;
			this.inputApplied.value = '';
		}
		
		this.firePaymentChange();
	},
	
	setApplied : function(e)
	{
		var value = new Number(this.inputApplied.value);
		
		var openAmt = this.unAllocatedPayment.getOpenAmt();
		if ((openAmt > 0 && value < 0) || openAmt < 0 && value > 0)
		{
			value = 0 - value;
		}
		
		/*this.inputApplied.value = value;*/
		
		if (value > openAmt)
		{
			alert(Translation.translate("applied.amt.is.greater.than.open.amt","applied.amt.is.greater.than.open.amt"));
			this.inputApplied.value = this.unAllocatedPayment.getOpenAmt();
			return;
		}
		
		if (value != this.unAllocatedPayment.getApplied())
		{
			this.unAllocatedPayment.setApplied(value);
		}
		
		this.firePaymentChange();
	},
	
	getUnAllocatedPayment :function()
	{
		return this.unAllocatedPayment;
	},
	
	equals : function(unAllocatedPaymentEditor)
	{
		var unAllocatedPayment = unAllocatedPaymentEditor.getUnAllocatedPayment();
		var _unAllocatedPayment = this.getUnAllocatedPayment();
		return (_unAllocatedPayment.getPaymentId() == unAllocatedPayment.getPaymentId());
	}
});

var UnAllocatedPaymentManager = Class.create({
	
	initialize : function()
	{
		this.panel = $('unAllocatedPayments');
		this.totalOpenAmt = 0;
		this.totalAppliedAmt = 0;
		this.unAllocatedPaymentEditors = new ArrayList(new UnAllocatedPaymentEditor());
		this.stateChangeListeners = new ArrayList();
		/*this.panelHeader = $('unallocated-payment-table-header');*/
	},
	
	getPaymentsToAllocate : function()
	{
		var paymentsToAllocate = new ArrayList(new UnAllocatedPaymentEditor());
		for (i=0; i<this.unAllocatedPaymentEditors.size(); i++)
		{
			var editor = this.unAllocatedPaymentEditors.get(i);
			var unAllocatedPayment = editor.getUnAllocatedPayment();
			var checked = unAllocatedPayment.getChecked();
			if (checked)
			{
				paymentsToAllocate.add(editor);
			}
		}
		
		return paymentsToAllocate;
	},
	
	validatePaymentsToAllocate : function()
	{
		var valid = true;
		var paymentsToAllocate = this.getPaymentsToAllocate();
		if (paymentsToAllocate.size() == 0)
		{
			alert(Translation.translate("please.select.a.payment"));
			valid = false;
			return;
		}
		else
		{
			for (var i=0; i<paymentsToAllocate.size(); i++)
			{
				var editor = paymentsToAllocate.get(i);
				var unAllocatedPayment = editor.getUnAllocatedPayment();
				var appliedAmt = unAllocatedPayment.getApplied();
				if (appliedAmt == 0)
				{
					editor.showError();
					valid = false;
				}
			}
		}
		return valid;
	},
	
	getTotalOpenAmt : function()
	{
		return this.totalOpenAmt;
	},
	
	getTotalAppliedAmt : function()
	{
		return this.totalAppliedAmt;
	},
	
	addStateChangeListener : function(listener)
	{
		this.stateChangeListeners.add(listener);
	},
	
	fireStateChange : function()
	{
		for (var i=0; i<this.stateChangeListeners.size(); i++)
		{
			var listener = this.stateChangeListeners.get(i);
			listener.stateChange();
		}
	},
	
	paymentChange : function()
	{
		this.totalAppliedAmt = 0;
		
		for (var i=0; i<this.unAllocatedPaymentEditors.size(); i++)
		{
			var unAllocatedPaymentEditor = this.unAllocatedPaymentEditors.get(i);
			var unAllocatedPayment = unAllocatedPaymentEditor.getUnAllocatedPayment();
			var appliedAmt = unAllocatedPayment.getApplied();
			
			this.totalAppliedAmt = this.totalAppliedAmt + appliedAmt;
		}
		
		this.fireStateChange();
	},
	
	clear : function()
	{
		this.totalOpenAmt = 0;
		this.totalAppliedAmt = 0;
		this.unAllocatedPaymentEditors.clear();
		this.stateChangeListeners.clear();
		
		var length = this.panel.childNodes.length;
		
		if (length == 2)
			return
		else
		{
			for(var i=0;i<length-2;i++)
			{
				this.panel.childNodes[i+2].innerHTML = '';
			}
		}
	},
	
	addPayments : function(unAllocatedPaymentsList)
	{
		for(var i=0;i<unAllocatedPaymentsList.size();i++)
		{
			var unAllocatedPayment = unAllocatedPaymentsList.get(i);
			var unAllocatedPaymentEditor = new UnAllocatedPaymentEditor(unAllocatedPayment);
			this.addUnAllocatedPayment(unAllocatedPaymentEditor);
		}
	},
	
	addUnAllocatedPayment : function(unAllocatedPaymentEditor)
	{
		this.unAllocatedPaymentEditors.add(unAllocatedPaymentEditor);
		unAllocatedPaymentEditor.addListener(this);
		
		var checkbox = unAllocatedPaymentEditor.getCheckbox();
		var inputApplied = unAllocatedPaymentEditor.getInputApplied();
		var unAllocatedPayment = unAllocatedPaymentEditor.getUnAllocatedPayment();
		var paymentId = unAllocatedPayment.getPaymentId();
		var date = unAllocatedPayment.getDate();
		var documentNo = unAllocatedPayment.getDocumentNo();
		var paymentAmt = unAllocatedPayment.getPaymentAmt();
		var openAmt = unAllocatedPayment.getOpenAmt();
		var applied = unAllocatedPayment.getApplied();
		
		this.totalOpenAmt = this.totalOpenAmt + openAmt;
		this.totalAppliedAmt = this.totalAppliedAmt + applied;
		
		var tr = document.createElement('div');
		tr.setAttribute('id', paymentId);
		tr.setAttribute('class', 'row');
		
		var tdSelect = document.createElement('div');
		tdSelect.setAttribute('class', 'col-md-1 col-xs-1 no-padding');
		
		var tdDate = document.createElement('div');
		tdDate.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
		
		var tdDocNo = document.createElement('div');
		tdDocNo.setAttribute('class', 'col-md-2 col-xs-2');
		
		var tdPayAmt = document.createElement('div');
		tdPayAmt.setAttribute('class', 'col-md-3 col-xs-3 no-padding');
		
		var tdOpen = document.createElement('div');
		tdOpen.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
		
		var tdApplied = document.createElement('div');
		tdApplied.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
		
		var spanDate = document.createElement('span');
		spanDate.innerHTML = date;
		
		var spanDocNo = document.createElement('span');
		spanDocNo.innerHTML = documentNo;
		
		var spanPayAmt = document.createElement('span');
		spanPayAmt.innerHTML = paymentAmt;
		
		var spanOpen = document.createElement('span');
		spanOpen.innerHTML = openAmt;
		
		tdSelect.appendChild(checkbox);
		tdDate.appendChild(spanDate);
		tdDocNo.appendChild(spanDocNo);
		tdPayAmt.appendChild(spanPayAmt);
		tdOpen.appendChild(spanOpen);
		tdApplied.appendChild(inputApplied);
		
		tr.appendChild(tdSelect);
		tr.appendChild(tdDate);
		tr.appendChild(tdDocNo);
		tr.appendChild(tdPayAmt);
		tr.appendChild(tdOpen);
		tr.appendChild(tdApplied);
		
		this.panel.appendChild(tr);
		
		/*var headerTds = this.panelHeader.getElementsByTagName('div');
		
		for(var i=0; i<headerTds.length; i++)
		{
			headerTds[i].style.borderBottomWidth='1px';
			headerTds[i].style.borderBottomColor='#000000';
		}*/
		
		var trs = this.panel.getElementsByClassName('row');
		
		for(var i=0; i<trs.length; i++)
		{
			if (i%2 == 0)
			{
				trs[i].className = 'row allocation-odd-row';
			}
			else
			{
				trs[i].className = 'row allocation-even-row';
			}
		}
	}
});

var Info = Class.create({
	
	initialize : function()
	{
		this._totalInvoiceAmt = $('totalInvoiceAmt');
		this._totalPaymentAmt = $('totalPaymentAmt');
		this.totalPaymentOpenAmt = 0;
		this.totalInvoiceOpenAmt = 0;
		this._difference = $('diff');
		this.unAllocatedPaymentsPanel = $('unAllocatedPayments');
		this.unPaidInvoicesPanel = $('unPaidInvoices');
		
		this.init();
	},
	
	init : function()
	{
		this._totalInvoiceAmt.innerHTML = '0';
		this._totalPaymentAmt.innerHTML = '0';
		this._difference.innerHTML = '0';		
	},
	
	setInfo : function(unPaidInvoicesList, unAllocatedPaymentsList)
	{
		var totalOpenAmtInvoice = 0;
		var totalInvoiceAmount = 0;
		for(var i=0; i<unPaidInvoicesList.size(); i++)
		{
			var unPaidInvoice = unPaidInvoicesList.get(i);
			totalOpenAmtInvoice = totalOpenAmtInvoice + new Number(unPaidInvoice.getOpenAmt());
			totalOpenAmtInvoice = new Number(totalOpenAmtInvoice);
			
			totalInvoiceAmount = totalInvoiceAmount +  new Number(unPaidInvoice.getInvoiceAmt());
			totalInvoiceAmount = new Number(totalInvoiceAmount);
		}
		
		this.setTotalInvoice(totalOpenAmtInvoice.toFixed(2), totalInvoiceAmount.toFixed(2)); 
		
		var totalOpenAmtPayment = 0;
		var totalPaymentAmount = 0;
		for(var i=0; i<unAllocatedPaymentsList.size(); i++)
		{
			var unAllocatedPayment = unAllocatedPaymentsList.get(i);
			
			totalOpenAmtPayment = totalOpenAmtPayment + new Number(unAllocatedPayment.getOpenAmt());
			totalOpenAmtPayment = new Number(totalOpenAmtPayment);
			
			totalPaymentAmount = totalPaymentAmount + new Number(unAllocatedPayment.getPaymentAmt());
			totalPaymentAmount = new Number(totalPaymentAmount);
		}
		
		this.setPaymentTotal(totalOpenAmtPayment.toFixed(2), totalPaymentAmount.toFixed(2));
		
		var difference = totalOpenAmtPayment - totalOpenAmtInvoice;
		difference = new Number(difference);
		difference = difference.toFixed(2);
		this.setDifference(difference);
	},
	
	setTotalInvoice : function(totalOpenInvoiceAmount, totalInvoiceAmount)
	{
		this._totalInvoiceAmt.innerHTML = totalInvoiceAmount;
		
		var tr = document.createElement('div');
		tr.setAttribute('class', 'row allocation-total');
		
		var tdLabel = document.createElement('div');
		tdLabel.setAttribute('class', 'col-md-5 col-xs-5');
		tdLabel.setAttribute('id', 'total-format');
		
		var label = document.createElement('span');
		
		label.innerHTML = 'Total';
		tdLabel.appendChild(label);
		
		var tdInvAmt = document.createElement('div');
		tdInvAmt.setAttribute('class', 'col-md-3 col-xs-3');
		var spanInvAmt = document.createElement('span');
		spanInvAmt.innerHTML = totalInvoiceAmount;
		tdInvAmt.appendChild(spanInvAmt);
		
		var tdOpenAmt = document.createElement('div');
		tdOpenAmt.setAttribute('class', 'col-md-2 col-xs-2');
		var spanOpenAmt = document.createElement('span');
		spanOpenAmt.innerHTML = totalOpenInvoiceAmount;
		tdOpenAmt.appendChild(spanOpenAmt);
		
		var tdTotalApplied = document.createElement('div');
		tdTotalApplied.setAttribute('class', 'col-md-2 col-xs-2');
		var spanTotalApplied = document.createElement('span');
		spanTotalApplied.setAttribute('id', 'total-invoice-applied-amount');
		spanTotalApplied.innerHTML = '&nbsp';
		tdTotalApplied.appendChild(spanTotalApplied);
		
		tr.appendChild(tdLabel);
		tr.appendChild(tdInvAmt);
		tr.appendChild(tdOpenAmt);
		tr.appendChild(tdTotalApplied);
		
		this.unPaidInvoicesPanel.appendChild(tr);
	},
	
	setPaymentTotal : function(totalOpenAmt, totalPaymentAmount)
	{
		this._totalPaymentAmt.innerHTML = totalOpenAmt;
		
		var tr = document.createElement('div');
		tr.setAttribute('class', 'row allocation-total');
		
		var tdLabel = document.createElement('div');
		tdLabel.setAttribute('class', 'col-md-5 col-xs-5');
		tdLabel.setAttribute('id', 'total-format');
		var label = document.createElement('span');
		
		label.innerHTML = 'Total';
		tdLabel.appendChild(label);
		
		var tdPayAmt = document.createElement('div');
		tdPayAmt.setAttribute('class', 'col-md-3 col-xs-3');
		var spanPayAmt = document.createElement('span');
		spanPayAmt.innerHTML = totalPaymentAmount;
		tdPayAmt.appendChild(spanPayAmt);
		
		var tdOpenAmt = document.createElement('div');
		tdOpenAmt.setAttribute('class', 'col-md-2 col-xs-2');
		var spanOpenAmt = document.createElement('span');
		spanOpenAmt.innerHTML = totalOpenAmt;
		tdOpenAmt.appendChild(spanOpenAmt);
		
		var tdTotalApplied = document.createElement('div');
		tdTotalApplied.setAttribute('class', 'col-md-2 col-xs-2');
		var spanTotalApplied = document.createElement('span');
		spanTotalApplied.setAttribute('id', 'total-payment-applied-amount');
		spanTotalApplied.innerHTML = '&nbsp';
		tdTotalApplied.appendChild(spanTotalApplied);
		
		tr.appendChild(tdLabel);
		tr.appendChild(tdPayAmt);
		tr.appendChild(tdOpenAmt);
		tr.appendChild(tdTotalApplied);
		
		this.unAllocatedPaymentsPanel.appendChild(tr);
	},
	
	setDifference : function(difference)
	{
		this._difference.innerHTML = difference;
	}
});

var GridController = Class.create({
	
	initialize : function()
	{
		this._difference = $('diff');
		this._processBtn = $('processBtn');
		//this._processBtn.disabled = true;
		
		this.info = new Info();
		
		this.unAllocatedPaymentManager = new UnAllocatedPaymentManager();
		this.unPaidInvoiceManager = new UnPaidInvoiceManager();
		
		this._processBtn.onclick = this.process.bind(this);
	},
	
	init : function(json)
	{
		this.unAllocatedPaymentManager.clear();
		this.unPaidInvoiceManager.clear();
		$('unPaidInvoices').innerHTML="";
		$('unAllocatedPayments').innerHTML="";
		
		var unPaidInvoices = json.invoices;
		var unPaidInvoicesList = new ArrayList(new UnPaidInvoice());
		if (unPaidInvoices != null)
		{
			for(var i=0;i<unPaidInvoices.length;i++)
			{
				var unPaidInvoice = new UnPaidInvoice(unPaidInvoices[i]);
				unPaidInvoicesList.add(unPaidInvoice);
			}
		}
		
		var unAllocatedPayments = json.payments;
		var unAllocatedPaymentsList = new ArrayList(new UnAllocatedPayment());
		if (unAllocatedPayments != null)
		{
			for(var i=0;i<unAllocatedPayments.length;i++)
			{
				var unAllocatedPayment = new UnAllocatedPayment(unAllocatedPayments[i]);
				unAllocatedPaymentsList.add(unAllocatedPayment);
			}
		}
		
		//this.info.setInfo(unPaidInvoicesList, unAllocatedPaymentsList);
		
		this.unPaidInvoiceManager.addInvoices(unPaidInvoicesList);
		this.unPaidInvoiceManager.addStateChangeListener(this);
		
		this.unAllocatedPaymentManager.addPayments(unAllocatedPaymentsList);
		this.unAllocatedPaymentManager.addStateChangeListener(this);
		
		this.info.setInfo(unPaidInvoicesList, unAllocatedPaymentsList);
		
		this._totalPaymentAppliedAmt = $('total-payment-applied-amount');
		this._totalInvoiceAppliedAmt = $('total-invoice-applied-amount');
	},

	stateChange : function()
	{
		var invoiceTotalAppliedAmt = new Number(this.unPaidInvoiceManager.getTotalAppliedAmt()).toFixed(2);
		var paymentTotalAppliedAmt = new Number(this.unAllocatedPaymentManager.getTotalAppliedAmt()).toFixed(2);
		this.difference = new Number(paymentTotalAppliedAmt - invoiceTotalAppliedAmt);
		this.difference = new Number(this.difference).toFixed(2);
		this._difference.innerHTML = this.difference;
		this._totalPaymentAppliedAmt.innerHTML = paymentTotalAppliedAmt;
		this._totalInvoiceAppliedAmt.innerHTML = invoiceTotalAppliedAmt;
		
		if (this.difference != 0)
		{
			//alert('Cannot process allocation. Difference should be zero.');
			return;
		}
		//this._processBtn.disabled = (difference != 0);
	},
		
	process : function()
	{
		//this._processBtn.disabled = true;
		
		var validInvoices = this.unPaidInvoiceManager.validateInvoicesToAllocate();
		
		//check for credit memo
		var invoicesToAllocate = this.unPaidInvoiceManager.getInvoicesToAllocate();
		
		var foundInvoice = false;
		var foundCreditMemo = false;
		
		for(var i=0; i<invoicesToAllocate.size(); i++)
		{
			 var editor = invoicesToAllocate.get(i);
			 var invoiceToBeAllocated = editor.getUnPaidInvoice();
			 
			 if(invoiceToBeAllocated.getDocType() == 'AR Invoice')
			 {
			 	foundInvoice = true;
			 }
			 else if(invoiceToBeAllocated.getDocType() == 'AR Credit Memo')
			 {
			 	foundCreditMemo = true;
			 }
		}
		
		console.log(`foundCreditMemo:${foundCreditMemo} foundInvoice:${foundInvoice} difference:${this.difference}`);
		
		if(parseFloat(this.difference) === 0.0 && foundInvoice && foundCreditMemo)
		{
			/* allocating credit memo to invoice */
		}
		else
		{
			var validPayments = this.unAllocatedPaymentManager.validatePaymentsToAllocate();		
		
			if (!validPayments || !validInvoices)
			{
				alert(Translation.translate('applied.amount.cannot.be.zero','Applied amount(s) cannot be 0'));
				return;
			}
		
		}		
		
		if (this.difference != 0)
		{
			alert(Translation.translate('cannot.process.difference.should.be.zero','cannot.process.difference.should.be.zero'));
			return;
		}
		
		
		var paymentsToAllocate = this.unAllocatedPaymentManager.getPaymentsToAllocate();
		
		var invoicesJSON = "'invoices':[";
		var paymentsJSON = "'payments':[";
		var finalJSON = '{';
		var totalAppliedPayment = 0;
		var totalAppliedInvoice = 0;
		
		for(var i=0; i<invoicesToAllocate.size(); i++)
		{
			 var editor = invoicesToAllocate.get(i);
			 var invoiceToBeAllocated = editor.getUnPaidInvoice();
			 totalAppliedInvoice = totalAppliedInvoice + invoiceToBeAllocated.getApplied();
			 invoicesJSON = invoicesJSON + '{';
			 invoicesJSON = invoicesJSON + "'c_invoice_id':";
			 invoicesJSON = invoicesJSON + invoiceToBeAllocated.getInvoiceId();
			 invoicesJSON = invoicesJSON + ',';
			 invoicesJSON = invoicesJSON + "'open':";
			 invoicesJSON = invoicesJSON + invoiceToBeAllocated.getOpenAmt();
			 invoicesJSON = invoicesJSON + ',';
			 invoicesJSON = invoicesJSON + "'applied':";
			 invoicesJSON = invoicesJSON + invoiceToBeAllocated.getApplied();
			 invoicesJSON = invoicesJSON + '}'
			 
			 if (invoicesToAllocate.size() > 1)
			 {
				 invoicesJSON = invoicesJSON + ',';
			 }
		}
		
		invoicesJSON = invoicesJSON + '],';

		for(var i=0; i<paymentsToAllocate.size(); i++)
		{
			var editor = paymentsToAllocate.get(i);
			var paymentToBeAllocated = editor.getUnAllocatedPayment();
			totalAppliedPayment = totalAppliedPayment + paymentToBeAllocated.getApplied();
			paymentsJSON = paymentsJSON + '{';
			paymentsJSON = paymentsJSON + "'c_payment_id':";
			paymentsJSON = paymentsJSON + paymentToBeAllocated.getPaymentId();
			paymentsJSON = paymentsJSON + ',';
			paymentsJSON = paymentsJSON + "'open':";
			paymentsJSON = paymentsJSON + paymentToBeAllocated.getOpenAmt();
			paymentsJSON = paymentsJSON + ',';
			paymentsJSON = paymentsJSON + "'applied':";
			paymentsJSON = paymentsJSON + paymentToBeAllocated.getApplied();
			paymentsJSON = paymentsJSON + '}';
			
			if (paymentsToAllocate.size() > 1)
			{
				paymentsJSON = paymentsJSON + ',';
			}
		}
		paymentsJSON = paymentsJSON + ']';
		
		finalJSON = finalJSON + invoicesJSON;
		finalJSON = finalJSON + paymentsJSON;
		finalJSON = finalJSON + '}'
		
		$('JSON').value = finalJSON;
		
		var win = new ModalWindow();
		win.show('Processing. Please wait...');
		
		$('allocationForm').request({
				onComplete : function(request)
				{
					win.dispose();
					showUnPaidInvoicesAndUnAllocatedPayments(request);
				}
		});
	}
});

function getUnPaidInvoicesAndUnAllocatedPayments(c_bpartner_id)
{
	url = 'PaymentAllocationAction.do?action=getUnPaidInvoicesAndUnAllocatedPayments&bpartnerId=' + c_bpartner_id;
	var myAjax = new Ajax.Request( url, 
			{ 
				method: 'get', 
				onSuccess: showUnPaidInvoicesAndUnAllocatedPayments, 
				onFailure: ''
			});
}


function showUnPaidInvoicesAndUnAllocatedPayments(request) 
{
	var list = eval('('+request.responseText+')');
	var gridController = new GridController();
	gridController.init(list);
}
