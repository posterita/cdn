var ArrayList = Class.create({
	initialize: function(type)
	{
		this.array = new Array();
		if (type == null || type.equals == null)
		{
			this.equals = function(currentObj , testObj){
				return (currentObj.toString() == testObj.toString());
			};
		}
		else if (type.equals != null)
		{
			this.equals = function(currentObj, testObj){
				return currentObj.equals(testObj);
			};
		}			
	},
	
	addAll : function(arrayList)
	{
		if (arrayList != null && arrayList instanceof ArrayList)
		{
			for (var i=0; i<arrayList.size(); i++)
			{
				this.add(arrayList.get(i));
			}
		}
	},
	
	add: function(object)
	{
		var index = this.array.size();
		this.array[index] = object;
	},
	
	addIndex : function(index, object)
	{
		var newArray = new Array();
		newArray[index] = object;
		
		for (var i=0; i< this.array.size(); i++)
		{
			if (i < index)
				newArray[i] = this.array[i];
			else if (i > index)
				newArray[i] = this.array[i-1];
		}
		
		this.array = newArray.clone();
		newArray = null;
	},
	
	contains : function(object)
	{
		for (var i = 0 ; i< this.array.length ; i++)
		{
			var currentObj = this.array[i];
			if (currentObj != null)
			{
				var equals = this.equals(currentObj, object);
				
				if (equals)
					return equals;
			}
		}
		return false;
	},
	
	replace : function(object)
	{
		this.remove(object);
		this.add(object);
	},
	
	remove : function(object)
	{
		var index = this.indexOf(object);
		this.removeIndex(index);					
	},
	
	removeIndex : function(index)
	{
		if (index >= 0 && index < this.array.length)
		{
			var newArray = new Array();
			for (var i=0 ; i < this.array.length; i++)
			{
				if (i < index)
				{
					newArray[i] = this.array[i];
				}
				else if (i < (this.array.length -1))
				{
					newArray[i] = this.array[i+1];
				}
			}
			this.array = newArray.clone();
			newArray = null;
		}		
	},
	
	indexOf : function(object)
	{
		for (var i = 0 ; i< this.array.length ; i++)
		{
			var currentObj = this.array[i];
			if (currentObj != null)
			{
				var equals = this.equals(currentObj, object);
				if (equals)
				{
					return i;
				}
			}
		}
		return -1;
	},
	
	get : function(index)
	{
		if (index >= 0 && index < this.array.length)
		{
			return this.array[index];
		}
	},
	
	clone : function()
	{
		var clone = new ArrayList();
		clone.equals = this.equals;
		for (var i=0; i<this.size(); i++)
		{
			clone.add(this.get(i));
		}
		return clone;
	},
	
	sort : function()
	{
		return this.array.sort();
	},
	
	sort : function(func)
	{
		return this.array.sort(func);
	},
	
	size : function()
	{
		return this.array.size();
	},
	
	clear : function()
	{
		this.array.clear();
	},
	
	toJSON : function()
	{
		return this.array.toJSON();
	},
	
	isEmpty : function()
	{
		return (this.size() == 0);
	}
});