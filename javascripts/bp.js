var BP_Constants = { 
			TYPE : {
				CUSTOMER	:'Customer',
				VENDOR		:'Vendor'
			},
			
			SO_CREDIT_STATUS : {
				CREDIT_STOP		: "S",
				CREDIT_HOLD		: "H",
				CREDIT_WATCH 	: "W",
				NO_CREDIT_CHECK : "X",
				CREDIT_OK 		: "O"
			}
};

var BP = new Class.create({
	initialize: function(record){
		this.id = record.c_bpartner_id || 0;
		this.code = record.identifier || '';
		this.name = unescape(record.name || '') ;
		this.name2 = unescape(record.name2 || '');
		this.openBalance = record.totalopenbalance || 0;
		this.creditStatus = record.socreditstatus || BP_Constants.SO_CREDIT_STATUS.NO_CREDIT_CHECK;
		this.creditLimit = record.so_creditlimit || 0;
		this.creditUsed = record.so_creditused || 0;
		this.isTaxExempt = record.istaxexempt;
		this.taxId = record.taxid;
		this.priceListId = record.m_pricelist_id;
		this.paymentTermId = record.c_paymentterm_id;
		this.actualLifetimeValue = record.actuallifetimevalue || '';
		this.flatDiscount = record.flatdiscount || '';
		this.firstSale = record.firstsale || '';
		this.imageURL = record.imageURL;
		this.customerImageUrl = record.customerImageUrl || '';
		this.email = record.email || '';
		this.birthday = record.birthday || '';
		this.phone = record.phone || '';
		this.fax = record.fax || '';
		this.address1 = record.address1 || '';
		this.city = record.city || '';
		this.searchKey = record.identifier || '';
		this.creditStatusName = 'No Credit Check';
		this.description = record.description || '';
		this.mobile = record.phone2 || '';
		this.custom1 = record.custom1 || '';
		this.custom2 = record.custom2 || '';
		this.custom3 = record.custom3 || '';
		this.custom4 = record.custom4 || '';
		this.emailReceipt = record.emailReceipt || '';
		
		this.enableLoyalty = record.enableloyalty || 'N';
		this.loyaltyPoints = record.loyaltypoints;
		this.loyaltyPointsEarned = record.loyaltypointsearned;
		this.loyaltyPointsSpent = record.loyaltypointsspent;
		this.loyaltyStartingPoints = record.loyaltystartingpoints;
		
		this.u_pos_discountcode_id = record.u_pos_discountcode_id || 0;
		this.discountcode_expiry = record.discountcode_expiry || '';
		
		this.setCreditStatusName();
		
		this.lastDateRedeemed = record.lastdateredeemed || '';
	},
	getId:function(){
		return this.id;
	},
	getCode:function(){
		return this.code;
	},
	getName:function(){
		return this.name;
	},
	getName2:function(){
		return this.name2;
	},
	getFullName:function(){
		var name1 = this.getName();
		var name2 = this.getName2();
		
		var fullName = name1;
		if(name2 != null && name2.length > 0){
			fullName = fullName + ' ' + name2;
		}
		
		return fullName;
	},
	getCreditAvailable:function(){
		var openBalance = this.getOpenBalance();
		var creditLimit = this.getCreditLimit();
		
		var creditAvailable = parseFloat(new Number(creditLimit - openBalance).toFixed(2));
		
		return creditAvailable;		
	},
	getCreditLimit:function(){
		return this.creditLimit;
	},
	getCreditUsed:function(){
		return this.creditUsed;
	},
	getCreditStatus:function(){
		return this.creditStatus;
	},
	getOpenBalance:function(){
		return this.openBalance;
	},
	getLifetimeValue:function(){
		return this.actualLifetimeValue;
	},
	getFirstSale:function(){
		return this.firstSale;
	},
	
	getImageURL : function(){
		return this.imageURL;
	},
	
	getCustomerImageUrl : function()
	{
		return this.customerImageUrl;
	},
	
	getEmail : function()
	{
		return this.email;
	},
	
	getBirthday : function()
	{
		return this.birthday;
	},
	
	getPhone : function()
	{
		return this.phone;
	},
	
	getFax : function()
	{
		return this.fax;
	},
	
	getMobile : function()
	{
		return this.mobile;
	},
	
	getAddress1 : function()
	{
		return this.address1;
	},
	
	getCity : function()
	{
		return this.city;
	},
	
	getSearchKey : function()
	{
		return this.searchKey;
	},
	
	getDescription : function()
	{
		return this.description;
	},
	getCustom1 : function()
	{
		return this.custom1;
	},
	getCustom2 : function()
	{
		return this.custom2;
	},
	getCustom3 : function()
	{
		return this.custom3;
	},
	getCustom4 : function()
	{
		return this.custom4;
	},
	isEmailReceipt : function()
	{
		return this.emailReceipt;
	},
	getLoyaltyStartingPoints : function()
	{
		return this.loyaltyStartingPoints;
	},
			
	setCreditStatusName : function(){
		switch(this.creditStatus)
		{
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_STOP :
			this.creditStatusName = 'Credit Stop';
			break;
			
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_HOLD :
			this.creditStatusName = 'Credit Hold';
			break;
			
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_WATCH :
			this.creditStatusName = 'Credit Watch';
			break;
			
			case BP_Constants.SO_CREDIT_STATUS.NO_CREDIT_CHECK :
				this.creditStatusName = 'No Credit Check';
				break;
				
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_OK :
				this.creditStatusName = 'Credit OK';
				break;
			
			default: break;
		}
	},
	
	getCreditStatusName : function(){
		return this.creditStatusName;
	},
	
	getPaymentTermId : function(){
		return this.paymentTermId;
	},
	
	getLastDateRedeemed : function(){
		return this.lastDateRedeemed;
	}
});

var BP_Autocompleter = Class.create({
	 initialize: function(textfield, resultContainer, isSoTrx) {
	    this.textfield  = textfield;
	    this.resultContainer = resultContainer;
	    this.isSoTrx = isSoTrx;
	    
	    this.url = "BPartnerAction.do";
	    
	    this.options = {
	    		'frequency' : 1.0,
	    		'minChars' : 1,
	    		'paramName' : "searchTerm",
	    		'parameters' : ('action=search&isCustomer=' + (this.isSoTrx == null || this.isSoTrx))
	    };
	    	    
	    this.options.afterUpdateElement = this.afterUpdateElement.bind(this);	    	    
	    
	    new Ajax.Autocompleter(this.textfield, this.resultContainer, this.url, this.options);
	    
	    this.textfield = $(this.textfield);
	    
	    this.textfield.onclick = function(e){
	    	this.select();
	    };
	 },
	 
	 afterUpdateElement:function(text, li){
		 var json = li.getAttribute('json');		 
		 if(json == null) return;
		 
		 json = eval('(' + json + ')');
		 
		 
	 
	 
	 	if (json.c_bpartner_id == 0)
	 	{			
			//alert($input.val());
			//dlg.close();
			
			jQuery.post("BPartnerAction.do",
			  {
			    action : "getCMSCustomer",
			    "identifier" : json.identifier
			  },
			  function(data,status,xhr){
				  
				data = JSON.parse(data);
				  
				if(data.error){
					
					alert(data.error);
					return;										
				}
				
				//dlg.close();
				
				/*var bp = new BP({
					'c_bpartner_id': data['c_bpartner_id'], 
					'name' : data['name']
				});*/
				
				var bp = new BP(data);
				
				BPManager.setBP(bp);
				$('search-bp-textfield').value = data['name'];
				$('bp-name').innerHTML = data['name'];
				
				OrderScreen.setBPartnerId(data['c_bpartner_id']).done(function(){
					
					$('search-bp-textfield').value = data['name'];
					$('bp-name').innerHTML = data['name'];
					
					BPManager.setBP(bp);
					
					alert('Imported CMS Customer sucessfully.');
					
				});				
			    
			  });
			
			return;
		
	 	}
		 
		 
		 /* encapsulate bp data in a BP object */
		 
		 var bp = new BP(json);
		 
		 //Validation for Customer discount code
		 if(!ShoppingCartManager.isShoppingCartEmpty())
		 {
			var customerDiscountCodeId = bp['u_pos_discountcode_id'];
			var oldBpName = $('bp-name').innerHTML;
			
			if ((oldBpName != bp.getFullName()) && (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0))
			{
				//check for discount code expiry
				if( bp.discountcode_expiry && bp.discountcode_expiry.length > 0 )
				{
					var expired = false;
					
					expired = moment().startOf('day').isAfter(moment(bp.discountcode_expiry,'MM-DD-YYYY'))
					
					console.log("Discount code expired -> " + expired);
					
					if( ! expired )
					{
						if(!window.confirm(bp.getFullName() + ' has discount code. Any discount will be reset on ' + oldBpName + '. Do you want to proceed?')) return;
					}
				}
				
				
			}
		 }
		 
		 BPManager.setBP(bp);
		 
		 /* clear textbox */
		 this.textfield.value = "";
		 
		 this.afterUpdateBP(bp);
		 
		 var bpInfoPanel = new BPInfoPanel();
		 bpInfoPanel.show();
	 },
	 
	 afterUpdateBP:function(bp){}
	 
});

var BPManager = {
		defaultBP:null,
		bp:null,
		dialog:null,
		setDefaultBP:function(bp){
			this.defaultBP = bp;
			if(this.bp == null){
				this.bp = this.defaultBP;
			}
		},
		getDefaultBP:function(){
			return this.defaultBP;
		},
		setBP:function(bp){
			this.bp = bp;
		},
		getBP:function(){
			return this.bp;
		},
		isBPDefault:function(bp){
			return (bp.getId() == this.defaultBP.getId());
		},
		
		validateCreditStatus:function(bp, amt){
			/*
			if(BPManager.isBPDefault(bp)){
				//need to choose a bp
				alert('Choose a customer. Credit transactions are not allowed on default customer');
				return;
			}
			*/
			
			/*validate credit status*/
			if(BP_Constants.SO_CREDIT_STATUS.NO_CREDIT_CHECK == bp.getCreditStatus())
			{
				/*var errmsg = messages.get("js.set.customer.credit.status");*/
				alert(Translation.translate("invalid.credit.status.reason.no.credit.check","Invalid credit status! Reason: No Credit Check"));
				return false;
			}
			
			if(bp.getCreditLimit() == 0)
			{
				/*var errmsg = messages.get("js.set.customer.credit.limit");*/
				alert(Translation.translate("invalid.credit.status.reason.credit.limit.not.set","Invalid credit status! Reason: Credit limit not set"));
				return false;
			}
			
			/*
			if((bp.getOpenBalance() + amt) > bp.getCreditLimit())	
			{				
				alert("Invalid credit status! Reason: Credit limit exceeded");			
				return false;
			}
			
			if(bp.getCreditStatus() != BP_Constants.SO_CREDIT_STATUS.CREDIT_OK)
			{
				
				var errmsg = "Invalid credit status! Reason: ";
				
				var status = bp.getCreditStatus();
				
				if(status == BP_Constants.SO_CREDIT_STATUS.CREDIT_HOLD)
				{
					errmsg += ' Credit Hold';
				}
				
				if(status == BP_Constants.SO_CREDIT_STATUS.CREDIT_STOP)
				{
					errmsg += ' Credit Stop';
				}
				
				if(status == BP_Constants.SO_CREDIT_STATUS.CREDIT_WATCH)
				{
					errmsg += ' Credit Watch';
				}
				
				alert(errmsg);
				return false;
			}
			*/
			
			return true;			
		},
		
		emailReceipt : function(orderId,emailAdd)
		{
			this.dialog = new Dialog();
			this.dialog .show();
						
			
			var url = 'OrderAction.do';
			var pars = 'action=emailReceipt&orderId=' + orderId; 
			new Ajax.Request(url, {
				method:'post',
				parameters: pars,
				onSuccess: this.onSuccess.bind(this)					
			});
		},
		
		onSuccess : function(response)
		{
			this.dialog.hide();
			var json = response.responseText.evalJSON(true);
			alert(json.message);					
		}
};

var UserManager = {
		initResetPassword:function(userId){
			var url = 'UserAction.do';
			var pars = 'action=initResetPassword&userId=' + userId;
			new Ajax.Request(url, {
				method:'post',
				parameters: pars,
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true || json.success == 'true'){
						alert(Translation.translate('please.check.your.mail.to.reset.your.password','Please check your mail to reset your password'));
					}
					else{
						alert(json.error);
					}						
				}					
			});
		}
		
};

var SearchBP = {
		
		isSoTrx:true,
		input:null,
		param: new Hash(),
		bpJSON: new Hash(),
		
		initializeComponents:function(){
			SearchBP.input = $('search-bp-textfield');
			
			$('bp-search-button').onclick = function(e){
				SearchBP.search();
			};

			$('search-bp-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchBP.search();
				}
			};
			
			$('search-bp-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-bp-textfield').onclick = function(e){
				this.select();
			};
		},
		
		search : function()
		{
			SearchBP.param.set('action', 'searchBP');
			SearchBP.param.set('searchTerm', SearchBP.input.value);
			SearchBP.param.set('isCustomer', (OrderScreen.isSoTrx == null || OrderScreen.isSoTrx));
			
			new Ajax.Request('BPartnerAction.do', {
				method:'post',
				onSuccess: SearchBP.render,
				parameters: SearchBP.param
			});
		},
		
		render : function(response){
			var jsonArray = response.responseText.evalJSON(true);
			
			$('bp-list').innerHTML =  '<div class="col-md-12 col-xs-12 no-padding"></div>';
			
			if (jsonArray.length == 0)
			{
				alert(Translation.translate('no.results.found','No Results Found'));
				return;
			}
			
			for (var i=0; i<jsonArray.length; i++)
			{
				var json = jsonArray[i];
				var phone = json.phone;
				var name = json.name;
				var name2 = json.name2;
				
				
				SearchBP.bpJSON.set(json.c_bpartner_id, json);
				
				var styleClass = 'even-row';
				
				if (i%2 == 0)
				{
					styleClass = 'odd-row';
				}
				
				var div = document.createElement('div');
				div.setAttribute('class', 'row ' + styleClass);
				div.setAttribute('style', 'padding: 4px 0px;');
				
				var div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.updateBP('+ json.c_bpartner_id +');');
				
				var div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name2;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.updateBP('+ json.c_bpartner_id +');');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-3 col-xs-3 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.updateBP('+ json.c_bpartner_id +');');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = phone;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-1 col-xs-1 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.displayInfo('+ json.c_bpartner_id +');');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-right');
				div3.setAttribute('style', 'padding-right:5px;');
				
				var iconDiv = document.createElement('div')
				iconDiv.setAttribute('class', 'glyphicons glyphicons-pro circle_info');
				div3.appendChild(iconDiv);
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				$('bp-list').appendChild(div);
			}
		},
		
		updateBP : function(bPartnerId)
		{
			var json = SearchBP.bpJSON.get(bPartnerId);
			var bp = new BP(json);
			BPManager.setBP(bp);
			
			OrderScreen.setBPartnerId(bp.getId());
			
			//$('search-bp-textfield').value = bp.getFullName();
			$('search-bp-textfield').value = '';
			$('bp-name').innerHTML = bp.getFullName();
		},
		
		displayInfo : function(bPartnerId)
		{
			var infoPanel = new BPInfoPanel();
			infoPanel.setId(bPartnerId);
			infoPanel.show();
		}
};


function updateBPName(url){
	
var bpartnerId;

var dlg = jQuery("<div title='"+ Translation.translate("change.customer") +"'><label>" +Translation.translate("select.customer")+"</label><br>" +
		"<input type='text' id='change-customer-textfield' style='margin-top:10px;font-size:20px;width:90%;' >" +
		"</div>");

var upbpdlg = jQuery(dlg).dialog({
    width: "450",
    height: "350",
    modal: true,
    autoOpen: true,
    open: function(event) {
        
    },
    beforeclose: function(){ 
    	return true;
    },
    close: function() {
    	jQuery(dlg).dialog('destroy');
    },
    position:['middle',40],
    buttons: [			 
          {
        	text: Translation.translate("apply"),
            click: function() {
            	//var bpartnerId = jQuery('#change-customer-textfield').val();
            	
            	if (bpartnerId == undefined || bpartnerId == null || bpartnerId == '')
            	{
            		alert(Translation.translate("please.select.customer"));
            		return;
            	}
            	
            	window.location = url + "&bpartnerId=" + bpartnerId;
            }
          }
    ]});


jQuery("#change-customer-textfield").keyup(function () {
 	
	var searchterm = jQuery(this).val();

    if (searchterm.length >= 2 ) {
    	
    	jQuery.post("BPartnerAction.do", {
            action: "searchCustomer",
            searchTerm : searchterm
        },
        
        function(json, textStatus, jqXHR) {

            if (json == null || jqXHR.status != 200) {
                alert(Translation.translate("failed.to.load.customers","Failed to load customers!"));
                return;
            }
            
            var customers = json.customers;	 
            
            customers = jQuery.map(customers, function(item){
        		return {
        			label: item.name,
        			value: item.c_bpartner_id
        		};
        	});
            
            jQuery('#change-customer-textfield').autocomplete({
            	source: customers,
            	
            	focus: function(event, ui){
            		event.preventDefault();
            	},
            	
            	select: function(event, ui){
            		event.preventDefault();
            		jQuery('#change-customer-textfield').val(ui.item.label);
            		bpartnerId = ui.item.value;
            	}
            	
            });       
            
        },
        "json").fail(function() {
        alert(Translation.translate("failed.to.query.customers","Failed to query customers!"));
        });   	 
    }	
	
});

};
