var BulkSearch = Class.create({
	
	initialize : function()
	{
		this._searchInput = null;
		this._searchBtn = null;
		this.param = null;
		this._resultContainer = null;
	},
	
	initializeComponents : function()
	{
		this._searchInput = $('bulk-search-textfield');
		this._searchBtn = $('bulk-search-button');
		
		this._searchBtn.onclick = this.search.bind(this);
		this._searchInput.onkeyup = this.wait.bind(this);
		
		this._resultContainer = $('bulk-search-result-container');
		if (this._resultContainer != null)
			this._resultContainer.innerHTML = '';
	},
	
	wait : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN){
			this.search();
		}
	},
	
	search : function()
	{
		if (this._searchInput == null)
			return;
		
		this.param = new Hash();
		this.param.set('action', 'searchCustomer');
		this.param.set('searchTerm', this._searchInput.value);
		this.param = this.param.toQueryString();
		this.searchAction();		
	}	
});

var BulkCustomerSearch = Class.create(BulkSearch, {
	
	initialize : function($super)
	{
		$super();
		this.customers = new ArrayList(new BulkCustomer());
		this.customerListing = new Listing();
	},
	
	searchAction : function()
	{
		new Ajax.Request('BPartnerAction.do', {
			method:'post',
			onSuccess: this.render.bind(this),
			parameters:this.param
		});
	},
	
	render : function(response)
	{
		var json = response.responseText.evalJSON(true);
		this._resultContainer.innerHTML = '';
		
		var _searchResultTable = document.createElement('table');
		_searchResultTable.setAttribute('id', 'bulk-searchResultTable');
		_searchResultTable.setAttribute('border', 0);
		_searchResultTable.setAttribute('width', '100%');

		var customers = json.customers;
		
		if (!json.success)
		{
			var customerSearchErrorPanel = new CustomerSearchErrorPanel(this._searchInput.value);
			customerSearchErrorPanel.show();
		}
		
		for (var i=0; i<customers.length; i++)
		{
			var customerJSON = customers[i];
			var customer = new BulkCustomer(customerJSON);
			
			customer.addListener(this);
			
			this.customers.add(customer);
			
			var _customerRow = customer.getComponent();
			_customerRow.setAttribute('class', (i%2==0)? "odd" : "even");
			
			_searchResultTable.appendChild(_customerRow);
			
			if (customers.length == 1)
			{
				this.onSelect(customer);
			}
		}

		this._resultContainer.appendChild( _searchResultTable);
		
		jQuery(document).ready(function(){

			jQuery('#bulk-search-pane').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		});
	},
	
	onSelect : function(customer)
	{
		for (var i=0; i<this.customers.size(); i++)
		{
			var cust = this.customers.get(i);
			cust.removeHighlight();
		}
		
		customer.highlight();
		
		this.customerListing.add(customer);
	},
	
	getCustomerListing : function()
	{
		return this.customerListing;
	}
});

var Listing = Class.create({
	
	initialize : function()
	{
		this._listing = $('content-listing');
		this.customers = new ArrayList(new BulkCustomer());
	},
	
	updateList : function()
	{
		var table = document.createElement('table');
		table.setAttribute('cellspacing', 0);
		
		for (var i=0; i<this.customers.size(); i++)
		{
			var customer = this.customers.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdComp = document.createElement('td');
			
			tr.setAttribute('class', (i%2==0)? "odd" : "even");
			
			tdName.innerHTML = customer.getName();
			
			tr.appendChild(tdName);
			
			table.appendChild(tr);
		}
		
		this._listing.innerHTML = '';
		this._listing.appendChild(table);
	},
	
	add : function(customer)
	{
		if (!this.customers.contains(customer))
		{
			this.customers.add(customer);
		}
		
		this.updateList();
		
		jQuery('#content-listing').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	},
	
	onSelect : function(customer)
	{
		this.add(customer);
	},
	
	getCustomers : function()
	{
		return this.customers;
	}
});

var BulkCustomer = Class.create({
	
	initialize : function(customerJSON)
	{
		this.customerJSON = customerJSON;
		
		if (customerJSON != null)
		{
			this.customerId = customerJSON.c_bpartner_id;
			this.name = customerJSON.name;
			
			this.listeners = new ArrayList();
			
			this._component = null;
			
			this.build();
		}
	},
	
	build : function()
	{
		this._component = document.createElement('tr');
		this._component.style.cursor = 'pointer';
		
		var td = document.createElement('td');
		td.innerHTML = this.name;
		
		this._component.appendChild(td);
		
		this._component.onclick = this.onSelect.bind(this);		
	},
	
	addListener : function(listener)
	{
		this.listeners.add(listener);
	},
	
	onSelect : function()
	{
		for (var i=0; i<this.listeners.size(); i++)
		{
			var listener = this.listeners.get(i);
			listener.onSelect(this);
		}
	},
	
	highlight : function()
	{
		this._component.addClassName('search-highlight');
	},
	
	removeHighlight : function()
	{
		this._component.removeClassName('search-highlight');
	},
	
	getComponent : function()
	{
		return this._component;
	},
	
	getCustomerId : function()
	{
		return this.customerId;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	equals : function(customer)
	{
		return (this.customerId == customer.getCustomerId());
	},
	
	toJSON : function()
	{
		var hash = new Hash();
		hash.put('customerId', this.customerId);
		hash.put('name', this.name);
		
		return hash.toJSON();
	}
});

var BulkDeleteCustomerPopUp = Class.create(PopUpBase, {
	
	initialize : function()
	{
		this.createPopUp($('bulk.delete.panel'));
		this.customerSearch = new BulkCustomerSearch();;
		
		this.init();
	},
	
	init : function()
	{
		this.customerSearch.initializeComponents();
		this.initBtns();
	},
	
	initBtns : function()
	{
		this.okBtn = $('bulk.delete.okBtn');
		this.cancelBtn = $('bulk.delete.cancelBtn');
		
		this.okBtn.onclick = this.bulkDelete.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	bulkDelete : function()
	{
		this.hide();
		var customerSearchListing = this.customerSearch.getCustomerListing();
		var customers = customerSearchListing.getCustomers();
				
		var param = 'action=bulkDelete&customers=[';
		for (var i=0; i<customers.size(); i++)
		{
			var customer = customers.get(i);
			param += customer.getCustomerId();
			
			if (i < (customers.size() -1))
			{
				param += ",";
			}
		}
		param += "]";
		
		new Ajax.Request('BPartnerAction.do', {
			method:'post',
			onSuccess: this.afterDelete.bind(this),
			parameters: param
		});
	},
	
	afterDelete : function(response)
	{
		var json = eval('('+response.responseText + ')');
		var customers = json.customers;
		var list = "";
		if (customers != null)
		{
			list += "These customers have been used in transactions and cannot be deleted : \n";
			for (var i=0; i<customers.length; i++)
			{
				var customer = customers[i];
				var name = customer.name;
				
				list += name + "\n" ;
			}
			alert(list);
		}
		else
		{
			alert(Translation.translate('customers._deleted','Customers deleted'));
		}
	},
	
	onShow : function()
	{
		$('content-listing').innerHTML = '';
		jQuery('#content-listing').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	}
});
