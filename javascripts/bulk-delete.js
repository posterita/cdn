var BulkSearch = Class.create({
	
	initialize : function()
	{
		this._searchInput = null;
		this._searchBtn = null;
		this.param = null;
		this._resultContainer = null;
	},
	
	initializeComponents : function()
	{
		this._searchInput = $('bulk-search-textfield');
		this._searchBtn = $('bulk-search-button');
		
		this._searchBtn.onclick = this.search.bind(this);
		this._searchInput.onkeyup = this.wait.bind(this);
		
		this._resultContainer = $('bulk-search-result-container');
		if (this._resultContainer != null)
			this._resultContainer.innerHTML = '';
	},
	
	wait : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN){
			this.search();
		}
	},
	
	search : function()
	{
		if (this._searchInput == null)
			return;
		
		this.param = new Hash();
		this.param.set('action', 'search');
		this.param.set('searchTerm', this._searchInput.value);
		this.param.set('isSoTrx', this.isSoTrx);
		this.param = this.param.toQueryString();
		this.searchAction();		
	}	
});

var BulkProductSearch = Class.create(BulkSearch, {
	
	initialize : function($super)
	{
		$super();
		this.products = new ArrayList(new BulkProduct());
		this.productListing = new Listing();
	},
	
	searchAction : function()
	{
		new Ajax.Request('ProductAction.do', {
			method:'post',
			onSuccess: this.render.bind(this),
			parameters:this.param
		});
	},
	
	render : function(response)
	{
		var json = response.responseText.evalJSON(true);
		this._resultContainer.innerHTML = '';
		
		var _searchResultTable = document.createElement('table');
		_searchResultTable.setAttribute('id', 'bulk-searchResultTable');
		_searchResultTable.setAttribute('border', 0);
		_searchResultTable.setAttribute('width', '100%');

		var products = json.products;
		
		if (!json.success)
		{
			var productSearchErrorPanel = new ProductSearchErrorPanel(this._searchInput.value);
			productSearchErrorPanel.show();
		}
		
		for (var i=0; i<products.length; i++)
		{
			var productJSON = products[i];
			var product = new BulkProduct(productJSON);
			
			product.addListener(this);
			
			this.products.add(product);
			
			var _productRow = product.getComponent();
			_productRow.setAttribute('class', (i%2==0)? "odd" : "even");
			
			_searchResultTable.appendChild(_productRow);
			
			if (products.length == 1)
			{
				this.onSelect(product);
			}
		}

		this._resultContainer.appendChild( _searchResultTable);
		
		jQuery(document).ready(function(){

			jQuery('#bulk-search-pane').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		});
	},
	
	onSelect : function(product)
	{
		for (var i=0; i<this.products.size(); i++)
		{
			var prod = this.products.get(i);
			prod.removeHighlight();
		}
		
		product.highlight();
		
		this.productListing.add(product);
	},
	
	getProductListing : function()
	{
		return this.productListing;
	}
});

var Listing = Class.create({
	
	initialize : function()
	{
		this._listing = $('content-listing');
		this.products = new ArrayList(new BulkProduct());
	},
	
	updateList : function()
	{
		var table = document.createElement('table');
		table.setAttribute('cellspacing', 0);
		
		for (var i=0; i<this.products.size(); i++)
		{
			var product = this.products.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdComp = document.createElement('td');
			
			tr.setAttribute('class', (i%2==0)? "odd" : "even");
			
			tdName.innerHTML = product.getName();
			
			tr.appendChild(tdName);
			
			table.appendChild(tr);
		}
		
		this._listing.innerHTML = '';
		this._listing.appendChild(table);
	},
	
	add : function(product)
	{
		if (!this.products.contains(product))
		{
			this.products.add(product);
		}
		
		this.updateList();
		
		jQuery('#content-listing').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	},
	
	onSelect : function(product)
	{
		this.add(product);
	},
	
	getProducts : function()
	{
		return this.products;
	}
});

var BulkProduct = Class.create({
	
	initialize : function(productJSON)
	{
		this.productJSON = productJSON;
		
		if (productJSON != null)
		{
			this.productId = productJSON.m_product_id;
			this.name = productJSON.name;
			this.description = productJSON.description;
			
			this.listeners = new ArrayList();
			
			this._component = null;
			
			this.build();
		}
	},
	
	build : function()
	{
		this._component = document.createElement('tr');
		this._component.style.cursor = 'pointer';
		
		var td = document.createElement('td');
		td.innerHTML = this.name + '<br>' + this.description;
		
		this._component.appendChild(td);
		
		this._component.onclick = this.onSelect.bind(this);		
	},
	
	addListener : function(listener)
	{
		this.listeners.add(listener);
	},
	
	onSelect : function()
	{
		for (var i=0; i<this.listeners.size(); i++)
		{
			var listener = this.listeners.get(i);
			listener.onSelect(this);
		}
	},
	
	highlight : function()
	{
		this._component.addClassName('search-highlight');
	},
	
	removeHighlight : function()
	{
		this._component.removeClassName('search-highlight');
	},
	
	getComponent : function()
	{
		return this._component;
	},
	
	getProductId : function()
	{
		return this.productId;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getDescription : function()
	{
		return this.description;
	},
	
	equals : function(product)
	{
		return (this.productId == product.getProductId());
	},
	
	toJSON : function()
	{
		var hash = new Hash();
		hash.put('productId', this.productId);
		hash.put('name', this.name);
		
		return hash.toJSON();
	}
});

var BulkDeleteProductPopUp = Class.create(PopUpBase, {
	
	initialize : function()
	{
		this.createPopUp($('bulk.delete.panel'));
		this.productSearch = new BulkProductSearch();;
		
		this.init();
	},
	
	init : function()
	{
		this.productSearch.initializeComponents();
		this.initBtns();
	},
	
	initBtns : function()
	{
		this.okBtn = $('bulk.delete.okBtn');
		this.cancelBtn = $('bulk.delete.cancelBtn');
		
		this.okBtn.onclick = this.bulkDelete.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	bulkDelete : function()
	{
		this.hide();
		var productListing = this.productSearch.getProductListing();
		var products = productListing.getProducts();
				
		var param = 'action=bulkDelete&products=[';
		for (var i=0; i<products.size(); i++)
		{
			var product = products.get(i);
			param += product.getProductId();
			
			if (i < (products.size() -1))
			{
				param += ",";
			}
		}
		param += "]";
		
		new Ajax.Request('ProductAction.do', {
			method:'post',
			onSuccess: this.afterDelete.bind(this),
			parameters: param
		});
	},
	
	afterDelete : function(response)
	{
		var json = eval('('+response.responseText + ')');
		var products = json.products;
		var list = "";
		if (products != null)
		{
			list += "These products have been used in transactions and cannot be deleted : \n";
			for (var i=0; i<products.length; i++)
			{
				var product = products[i];
				var name = product.name;
				
				list += name + "\n" ;
			}
			alert(list);
		}
		else
		{
			alert(Translation.translate('products._deleted','Products deleted'));
		}
	},
	
	onShow : function()
	{
		$('content-listing').innerHTML = '';
		jQuery('#content-listing').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	}
});
