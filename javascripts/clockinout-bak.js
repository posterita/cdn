var SalesRep = Class.create({
		initialize : function (name, id)
		{
			this.name = name;
			this.id = id;
			this.current = false;
			this.identifier = null;
			this.currentSalesRepChangeEventListeners = new ArrayList();
		},
		
		setIdentifier : function(identifier)
		{
			this.identifier = identifier;
		},
		
		getIdentifier : function()
		{
			return this.identifier;
		},
		
		isCurrent : function()
		{
			return this.current;
		},
		
		setCurrent : function(current)
		{
			var old_value = this.current;
			var new_value = current;
			this.current = new_value;
			
			if (old_value != new_value)
			{
				var currentSalesRepChangeEvent = new CurrentSalesRepChangeEvent(this);
				this.fireCurrentSalesRepChangeEvent(currentSalesRepChangeEvent);
			}
		},
		
		getName : function()
		{
			return this.name;
		},
		
		getId : function()
		{
			return this.id;
		},
		
		equals : function(salesRep)
		{
			return (this.getId() == salesRep.getId());
		},
		
		addCurrentSalesRepChangeEventListener : function (listener)
		{
			if (!this.currentSalesRepChangeEventListeners.contains(listener))
				this.currentSalesRepChangeEventListeners.add(listener);
		},
		
		fireCurrentSalesRepChangeEvent : function (currentSalesRepChangeEvent)
		{
			for (var i=0; i<this.currentSalesRepChangeEventListeners.size(); i++)
			{
				var listener = this.currentSalesRepChangeEventListeners.get(i);
				listener.currentSalesRepChange(currentSalesRepChangeEvent);
			}
		}
});

var CurrentSalesRepChangeEvent = Class.create({
	initialize : function (salesRep)
	{
		this.salesRep = salesRep;
		this.value = salesRep.isCurrent();
	},
	
	getSource : function()
	{
		return this.salesRep;
	},
	
	getValue : function()
	{
		return this.value;
	}
});

var ClockedInSalesRepManager = {
		
		currentSalesRep : null,
		
		salesReps : new ArrayList(new SalesRep()),
		
		currentSalesRepChangeEventListeners : new ArrayList(),

		clockInOutEventListeners : new ArrayList(),
		
		onLoadClockedInSalesRepsListeners : new ArrayList(),
		
		reset : function() 
		{
			ClockedInSalesRepManager.salesReps = new ArrayList(new SalesRep()); 
		},
		
		addOnLoadClockedInSalesRepsListener : function(listener)
		{
			ClockedInSalesRepManager.onLoadClockedInSalesRepsListeners.add(listener);
		},
		
		fireClockedInSalesRepsLoadedEvent : function()
		{
			for (var i=0; i< ClockedInSalesRepManager.onLoadClockedInSalesRepsListeners.size(); i++)
			{
				var listener = ClockedInSalesRepManager.onLoadClockedInSalesRepsListeners.get(i);
				listener.clockedInSalesRepsLoaded();
			}
		},
		
		addClockInOutEventListener : function(listener)
		{
			ClockedInSalesRepManager.clockInOutEventListeners.add(listener);
		},
		
		fireClockInOutEvent : function()
		{
			var clockInOutEventListeners = ClockedInSalesRepManager.clockInOutEventListeners;
			for (var i=0; i<clockInOutEventListeners.size(); i++)
			{
				var clockInOutEventListener = clockInOutEventListeners.get(i);
				clockInOutEventListener.clockInOut();
			}
		},
		
		addSalesRep : function(salesRep)
		{
			if (ClockedInSalesRepManager.salesReps.contains(salesRep))
				return;
			else
			{
				ClockedInSalesRepManager.salesReps.add(salesRep);
				salesRep.addCurrentSalesRepChangeEventListener(ClockedInSalesRepManager);
			}
		},
		
		getSalesRep : function (salesRepId)
		{
			for (var i=0; i<ClockedInSalesRepManager.salesReps.size(); i++)
			{
				var salesRep = ClockedInSalesRepManager.salesReps.get(i);
				if (salesRep.getId() == salesRepId)
				{
					return salesRep;
				}
			}
		},
		
		currentSalesRepChange : function (currentSalesRepChangeEvent)
		{
			var salesRep = currentSalesRepChangeEvent.getSource();
			var isCurrent = currentSalesRepChangeEvent.getValue();
			if (isCurrent)
			{
				ClockedInSalesRepManager.setCurrentSalesRep(salesRep);
				ClockedInSalesRepManager.fireCurrentSalesRepChangeEvent(currentSalesRepChangeEvent);
			}
		},
		
		addCurrentSalesRepChangeEventListener : function (listener)
		{
			var currentSalesRepChangeEventListeners = ClockedInSalesRepManager.currentSalesRepChangeEventListeners;
			currentSalesRepChangeEventListeners.add(listener);
		},
		
		fireCurrentSalesRepChangeEvent : function(currentSalesRepChangeEvent)
		{
			var currentSalesRepChangeEventListeners = ClockedInSalesRepManager.currentSalesRepChangeEventListeners;
			for (var i=0; i<currentSalesRepChangeEventListeners.size(); i++)
			{
				var listener = currentSalesRepChangeEventListeners.get(i);
				listener.currentSalesRepChange(currentSalesRepChangeEvent);
			}
		},
		
		setCurrentSalesRep : function(salesRep)
		{
			ClockedInSalesRepManager.currentSalesRep = salesRep;
		},
		
		getCurrentSalesRep : function()
		{
			return ClockedInSalesRepManager.currentSalesRep;
		},
		
		getSalesReps : function()
		{
			return ClockedInSalesRepManager.salesReps;
		}
}

var ClockInOut = {
		
	clockInOutPanel : null,	

	show: function()
	{
		var content = "";
		
		content += "<table border='0' cellspacing='0' cellpadding='5' width='450px' height='320px' valign='top'>";
		content += "<tr><td><h2 id ='h2clockinout'>Clock In/Out</h2></td><td colspan='2' align='left' id='clockinoutErrorMsg'></td><td><a id='popupExit' href='javascript: ClockInOut.disposePanel()' ><img src='assets/images/keypad-close.png' title='' border='0' width='90px' height='50px' style='margin-top: 20px;'></a></td></tr>"
		content += "<tr valign='top'>";
		content += "<th>";
		content += "<table border='0' cellspacing='0' cellpadding='0' id='clockedinusers' width='310px' valign='top'>";
		content += "</table>";
		content += "</th>";
		content += "<th>";
			content += "<table border='0' cellspacing='0' cellpadding='2' id='clockin' width='150px'>";
			content += "<tr bgcolor='#999999'><td id='textcolor' colspan='2'>User Login</td></tr>";
			content += "<tr><td id='clockinoutlogin'>" + messages.get('js.username') + " </td><td><input type='text' size='15' class='clockinouttextfield' id='clockinoutusername'/></td></tr>";
			content += "<tr><td id='clockinoutlogin'>" + messages.get('js.password') + "&nbsp; </td><td><input type='password' size='15' class='clockinouttextfield' id='clockinoutpassword'/></td></tr>";
			content += "<tr><td id='clockinoutlogin'>" + messages.get('js.pin') + " </td><td><input type='password' size='15' class='clockinouttextfield' id='clockinoutpin'/></td></tr>";
			content += "<tr><td colspan='2' align='right'><a href='javascript: ClockInOut.clockInOut()'><img src='images/newUI/butn-clockin1.png' title='Clock In' border='0' width='140px' height='30px'></a></td>" ;
			content += "</table>";
		content += "</th>";
		content += "</tr>";
		content += "<tr><td colspan='4' align='right'><a href='javascript:Keyboard.toggle()'><img src='images/newIcons/keyboard.png'/></a></td></tr>";
		content += "</table>";
		
		ClockInOut.clockInOutPanel = new ModalWindow(content, false);
		
		var pinTextField = $('clockinoutpin');
		var usernameTextField = $('clockinoutusername');
		var passwordTextField = $('clockinoutpassword');
		
		var inputs = [usernameTextField,passwordTextField,pinTextField];
		for(var j=0; j<inputs.length; j++){
			var input = inputs[j];
			Event.observe(input,'click',Keyboard.setInput.bindAsEventListener(Keyboard),false);
		}
			
		/*Event.observe(pinTextField,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(pinTextField,'keyup',Keypad.keyUpHandler.bindAsEventListener(Keypad),false);*/
		
		ClockInOut.clockInOutPanel.show();
		
		ClockInOut.loadClockedInUsers();
	},
	
	disposePanel : function()
	{
		ClockInOut.clockInOutPanel.dispose();
	},
	
	loadClockedInUsers: function()
	{
		var url = "ClockInOutUserAction.do?action=getClockedInUsers";
		var pars = null;
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'POST', 
			parameters: pars, 
			onSuccess: ClockInOut.loadClockedInSuccess, 
			onFailure: ClockInOut.loadClockedInFailure
		});	
	},
	
	loadClockedInSuccess: function(request)
	{
		var response = request.responseText;
		var result = eval('(' + response + ')');
		
		var content = "";
		content += "<tr bgcolor='#999999'><td id='textcolor' width='120px'>" + messages.get('js.username') + "</td><td id='textcolor'>" + messages.get('js.clockInTime') + "</td></tr>";
				
		for (var i = 0; i < result.length; i++)
		{
			content += "<tr><td>" + result[i].userName + "</td><td>" + result[i].clockInTime + "</td></tr>";
		}
		
		$('clockedinusers').innerHTML = content;
	},
	
	loadClockedInFailure: function()
	{
		
	},
	
	clockInOut: function()
	{
		var url = "ClockInOutUserAction.do?action=clockInOutUser";
		var pars = "username=" + $('clockinoutusername').value + "&password=" + $('clockinoutpassword').value + "&userPIN=" + $('clockinoutpin').value + "&clockInTime=" + DateUtil.getCurrentDate();
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'POST', 
			parameters: pars, 
			onSuccess: ClockInOut.clockInOutSuccess, 
			onFailure: ClockInOut.clockInOutFailure
		});	
	},
	
	
	
	clockInOutSuccess: function(request)
	{
		var response = request.responseText;
		var result = eval('(' + response + ')');
		
		if (result.Success && result.Success == true)
		{
			$('clockinoutusername').value = '';
			$('clockinoutpassword').value = '';
			$('clockinoutpin').value = '';
			$('clockinoutErrorMsg').innerHTML = "&nbsp;";
			ClockInOut.loadClockedInUsers();
		}
		else
		{
			$('clockinoutErrorMsg').innerHTML = result.Message;
		}
		
		ClockInOut.loadClockInSalesRep();
	},
	
	clockInOutFailure: function(request)
	{
		alert('Clock in/out failed, please try again');
	},
	
	
	loadClockInSalesRep: function()
	{
		var url = "ClockInOutUserAction.do?action=getClockedInSalesRep";
		var pars = null;
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'get', 
			parameters: pars, 
			onSuccess: ClockInOut.loadClockedInSalesRepSuccess, 
			onFailure: ClockInOut.loadClockedInSalesRepFailure
		});	
	},
	
	loadClockedInSalesRepSuccess: function(request)
	{
		var response = request.responseText;
		var result = eval('(' + response + ')');
		
		var salesRepId = result.salesRepId;
		var name = result.name;
		
		var salesReps = result.salesreps;
		
		var clockedInSalesReps_before = ClockedInSalesRepManager.getSalesReps();
		var currentSalesRep = new SalesRep(name, salesRepId);
		var previousSalesRep = ClockedInSalesRepManager.getCurrentSalesRep();
		
		ClockedInSalesRepManager.reset();
		
		for (var i = 0; i < salesReps.length; i++)
		{
			var salesRep = new SalesRep(salesReps[i].userName, salesReps[i].userId);
			salesRep.setIdentifier(salesReps[i].identifier);
			ClockedInSalesRepManager.addSalesRep(salesRep);
		}
		ClockedInSalesRepManager.addSalesRep(currentSalesRep);
		
		if (previousSalesRep == null)
			previousSalesRep = currentSalesRep;
		
		ClockInOut.render(ClockedInSalesRepManager.getSalesReps(), currentSalesRep, previousSalesRep);
		ClockedInSalesRepManager.fireClockedInSalesRepsLoadedEvent();
		
		var clockedInSalesReps_after = ClockedInSalesRepManager.getSalesReps();
		
		if (clockedInSalesReps_before.size() != 0)
		{
			if (clockedInSalesReps_before.size() != clockedInSalesReps_after.size())
			{
				ClockedInSalesRepManager.fireClockInOutEvent();
			}
			else
			{
				for (var i=0; i<clockedInSalesReps_after.size(); i++)
				{
					var clockedInSalesRep_after = clockedInSalesReps_after.get(i);
					if (!clockedInSalesReps_before.contains(clockedInSalesRep_after))
					{
						ClockedInSalesRepManager.fireClockInOutEvent();
					}
				}
			}
		}
	},
	
	render : function(salesReps, currentSalesRep, previousSalesRep)
	{
		var content = '';
		for (var i=0; i<salesReps.size(); i++)
		{
			var isCurrent = false;
			var salesRep = salesReps.get(i);
			if (salesRep.equals(currentSalesRep))
			{
				isCurrent = true;
				salesRep.setCurrent(true);				
				
				if (previousSalesRep != null && !previousSalesRep.equals(currentSalesRep))
				{
					previousSalesRep.setCurrent(false);
				}
			}
			else
			{
				isCurrent = false;
			}
			
			var contentId = 'salesRep' + salesRep.getId();
			
			content = '<div id="salesRepName' + salesRep.getId() + '" class="employee"><img src="images/employee.gif" alt="employee"/>' +
				'<span>' + salesRep.getName() + '</span><br/>' +
		     	'<strong id="salesRepAmount' +salesRep.getId() +'">$AMT</strong></div>';
			
			/*
			content += '<li id="' + contentId 
			+ '" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal" jcarouselindex="' 
			+ (i+1) + '"><a href="javascript:ClockInOut.setSalesRep(\'' + salesRep.getIdentifier() + '\')">'
				    + ((isCurrent) ? '<div class="butn-user01"></div>' : '<div class="butn-user02-off"></div>')
				    + '<span class="data" id="salesRepName' +salesRep.getId() +'">' + salesRep.getName() + '<br><b id="salesRepAmount' +salesRep.getId() +'">$AMT</b></span></a></li>';
				    */
		}		
		
		
		if($('clockedInSalesRep')){
			$('clockedInSalesRep').innerHTML = content;
		}	
	},
	
	loadClockedInSalesRepFailure: function(request)
	{
		
	},
	
	setSalesRep: function(identifier)
	{
		var url = "ClockInOutUserAction.do?action=setSalesRep";
		var pars = "identifier=" + identifier;
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'get', 
			parameters: pars, 
			onSuccess: ClockInOut.setSalesRepSuccess, 
			onFailure: ClockInOut.setSalesRepFailure
		});	
	},
	
	setSalesRepSuccess: function(request)
	{
		ClockInOut.loadClockInSalesRep();
	},
	
	setSalesRepFailure: function(request)
	{
	}
}

Event.observe(window,'load',ClockInOut.loadClockInSalesRep,false);