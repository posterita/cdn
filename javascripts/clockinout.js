/*clockinout.js*/
var ClockInOutManager = {
		
		clockedInUsers : null,
		listeners : new Array(),
		
		getClockedInUsers : function(){
			/*request clocked in users*/
			var url = "ClockInOutUserAction.do?action=getClockedInUsers";
			var pars = null;
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'get', 
				parameters: pars, 
				onSuccess: ClockInOutManager.getClockedInUsersSuccess.bind(ClockInOutManager),  
				onFailure: ClockInOutManager.getClockedInUsersFailure.bind(ClockInOutManager)
			});
		},
		
		getClockedInUsersSuccess : function(request){
			/*parse json*/
			var response = request.responseText;
			var result = eval('(' + response + ')');
			
			var salesRepsList = result;
			this.clockedInUsers = salesRepsList;
			
		},
		
		getClockedInUsersFailure : function(request){
			/**/
			alert(Translation.translate('fail.to.load.clock.in.users','Fail to load clock in users'));
		},
		
		clockInOutUser : function(username, password, date){
			/*clock in/out user*/
			var url = "ClockInOutUserAction.do?action=clockInOutUser";	
			
			var pars = new Hash();			
			pars.set('username', username);
			pars.set('password', password);
			pars.set('clockInOutTime', date);
			
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'POST', 
				parameters: pars, 
				onSuccess: ClockInOutManager.clockInOutUserSuccess.bind(ClockInOutManager), 
				onFailure: ClockInOutManager.clockInOutUserFailure.bind(ClockInOutManager)
			});
		},
								
		clockInOutUserSuccess : function(request){
			/**/
			var response = request.responseText;
			var result = eval('(' + response + ')');
			
			var message = result.message;
			var clockIn = result.clockIn;
			
			if (result.userList != undefined)
			{
				var clockedInUsersList = result.userList;
				
				this.clockedInUsers = clockedInUsersList;
			}
			
			this.clockInOutUserHandler(message, clockedInUsersList, clockIn);
		},
		
		clockInOutUserFailure : function(request){
			/**/ 
			alert(Translation.translate('failed.to.clock.inout.user','Failed to Clock IN/OUT user!'));
		},
				
		clockInOutUserHandler : function(message, clockedInUsersList, clockIn){
			for(var i=0; i<this.listeners.length; i++){
				var listener = this.listeners[i];
				if(listener){
					listener.clockInOutUserHandler(message, clockedInUsersList, clockIn);
				}
			}
			
			/* clear session storage cache */
			sessionStorage.removeItem("CLOCK_IN_SALES_REPS");
			
			ClockInOut.loadClockInSalesRep();
		},
		
		init : function(){
			if($('header-clock-inout-button')){
				ClockInOutManager.getClockedInUsers();
			}			
		}
		
};

var SalesRep = Class.create({
		initialize : function (name, id)
		{
			this.name = name;
			this.id = id;
			this.current = false;
			this.identifier = null;
			this.hasAttachment = false;
			this.currentSalesRepChangeEventListeners = new ArrayList();
		},
		
		getHasAttachment : function()
		{
			return this.hasAttachment;
		},
		
		setHasAttachment : function(hasAttachment)
		{
			this.hasAttachment = hasAttachment;
		},
		
		setIdentifier : function(identifier)
		{
			this.identifier = identifier;
		},
		
		getIdentifier : function()
		{
			return this.identifier;
		},
		
		isCurrent : function()
		{
			return this.current;
		},
		
		setCurrent : function(current)
		{
			var old_value = this.current;
			var new_value = current;
			this.current = new_value;
			
			if (old_value != new_value)
			{
				var currentSalesRepChangeEvent = new CurrentSalesRepChangeEvent(this);
				this.fireCurrentSalesRepChangeEvent(currentSalesRepChangeEvent);
			}
		},
		
		getName : function()
		{
			return this.name;
		},
		
		getId : function()
		{
			return this.id;
		},
		
		equals : function(salesRep)
		{
			return (this.getId() == salesRep.getId());
		},
		
		addCurrentSalesRepChangeEventListener : function (listener)
		{
			if (!this.currentSalesRepChangeEventListeners.contains(listener))
				this.currentSalesRepChangeEventListeners.add(listener);
		},
		
		fireCurrentSalesRepChangeEvent : function (currentSalesRepChangeEvent)
		{
			for (var i=0; i<this.currentSalesRepChangeEventListeners.size(); i++)
			{
				var listener = this.currentSalesRepChangeEventListeners.get(i);
				listener.currentSalesRepChange(currentSalesRepChangeEvent);
			}
		}
});

var CurrentSalesRepChangeEvent = Class.create({
	initialize : function (salesRep)
	{
		this.salesRep = salesRep;
		this.value = salesRep.isCurrent();
	},
	
	getSource : function()
	{
		return this.salesRep;
	},
	
	getValue : function()
	{
		return this.value;
	}
});

var ClockedInSalesRepManager = {
		
		currentSalesRep : null,
		
		isCurrentUserSalesRep: false,
		
		salesReps : new ArrayList(new SalesRep()),
		
		currentSalesRepChangeEventListeners : new ArrayList(),

		clockInOutEventListeners : new ArrayList(),
		
		onLoadClockedInSalesRepsListeners : new ArrayList(),
		
		setCurrentUserAsSalesRep : function(isCurrentUserSalesRep)
		{
			ClockedInSalesRepManager.isCurrentUserSalesRep = isCurrentUserSalesRep;
		},
		
		getIsCurrentUserSalesRep : function()
		{
			return ClockedInSalesRepManager.isCurrentUserSalesRep;
		},
		
		reset : function() 
		{
			ClockedInSalesRepManager.salesReps = new ArrayList(new SalesRep()); 
		},
		
		addOnLoadClockedInSalesRepsListener : function(listener)
		{
			ClockedInSalesRepManager.onLoadClockedInSalesRepsListeners.add(listener);
		},
		
		removeOnLoadClockedInSalesRepsListener : function(listener)
		{
			ClockedInSalesRepManager.onLoadClockedInSalesRepsListeners.remove(listener);
		},
		
		fireClockedInSalesRepsLoadedEvent : function()
		{
			for (var i=0; i< ClockedInSalesRepManager.onLoadClockedInSalesRepsListeners.size(); i++)
			{
				var listener = ClockedInSalesRepManager.onLoadClockedInSalesRepsListeners.get(i);
				listener.clockedInSalesRepsLoaded();
			}
		},
		
		addClockInOutEventListener : function(listener)
		{
			ClockedInSalesRepManager.clockInOutEventListeners.add(listener);
		},
		
		removeClockInOutEventListener : function(listener)
		{
			ClockedInSalesRepManager.clockInOutEventListeners.remove(listener);
		},
		
		fireClockInOutEvent : function()
		{
			var clockInOutEventListeners = ClockedInSalesRepManager.clockInOutEventListeners;
			for (var i=0; i<clockInOutEventListeners.size(); i++)
			{
				var clockInOutEventListener = clockInOutEventListeners.get(i);
				clockInOutEventListener.clockInOut();
			}
		},
		
		addSalesRep : function(salesRep)
		{
			if (ClockedInSalesRepManager.salesReps.contains(salesRep))
				return;
			else
			{
				ClockedInSalesRepManager.salesReps.add(salesRep);
				salesRep.addCurrentSalesRepChangeEventListener(ClockedInSalesRepManager);
			}
		},
		
		getSalesRep : function (salesRepId)
		{
			for (var i=0; i<ClockedInSalesRepManager.salesReps.size(); i++)
			{
				var salesRep = ClockedInSalesRepManager.salesReps.get(i);
				if (salesRep.getId() == salesRepId)
				{
					return salesRep;
				}
			}
		},
		
		currentSalesRepChange : function (currentSalesRepChangeEvent)
		{
			var salesRep = currentSalesRepChangeEvent.getSource();
			var isCurrent = currentSalesRepChangeEvent.getValue();
			if (isCurrent)
			{
				ClockedInSalesRepManager.setCurrentSalesRep(salesRep);
				ClockedInSalesRepManager.fireCurrentSalesRepChangeEvent(currentSalesRepChangeEvent);
			}
		},
		
		addCurrentSalesRepChangeEventListener : function (listener)
		{
			var currentSalesRepChangeEventListeners = ClockedInSalesRepManager.currentSalesRepChangeEventListeners;
			currentSalesRepChangeEventListeners.add(listener);
		},
		
		fireCurrentSalesRepChangeEvent : function(currentSalesRepChangeEvent)
		{
			var currentSalesRepChangeEventListeners = ClockedInSalesRepManager.currentSalesRepChangeEventListeners;
			for (var i=0; i<currentSalesRepChangeEventListeners.size(); i++)
			{
				var listener = currentSalesRepChangeEventListeners.get(i);
				listener.currentSalesRepChange(currentSalesRepChangeEvent);
			}
		},
		
		setCurrentSalesRep : function(salesRep)
		{
			ClockedInSalesRepManager.currentSalesRep = salesRep;
		},
		
		getCurrentSalesRep : function()
		{
			return ClockedInSalesRepManager.currentSalesRep;
		},
		
		getSalesReps : function()
		{
			return ClockedInSalesRepManager.salesReps;
		}
};

var ClockInOut = {
		
	clockInOutPanel : null,	

	show: function()
	{
		var content = "";
		
		content += "<table border='0' cellspacing='0' cellpadding='5' width='450px' height='320px' valign='top'>";
		content += "<tr><td><h2 id ='h2clockinout'>Clock In/Out</h2></td><td colspan='2' align='left' id='clockinoutErrorMsg'></td><td><a id='popupExit' href='javascript: ClockInOut.disposePanel()' ><img src='assets/images/keypad-close.png' title='' border='0' width='90px' height='50px' style='margin-top: 20px;'></a></td></tr>"
		content += "<tr valign='top'>";
		content += "<th>";
		content += "<table border='0' cellspacing='0' cellpadding='0' id='clockedinusers' width='310px' valign='top'>";
		content += "</table>";
		content += "</th>";
		content += "<th>";
			content += "<table border='0' cellspacing='0' cellpadding='2' id='clockin' width='150px'>";
			content += "<tr bgcolor='#999999'><td id='textcolor' colspan='2'>User Login</td></tr>";
			content += "<tr><td id='clockinoutlogin'>" + messages.get('js.username') + " </td><td><input type='text' size='15' class='clockinouttextfield' id='clockinoutusername'/></td></tr>";
			content += "<tr><td id='clockinoutlogin'>" + messages.get('js.password') + "&nbsp; </td><td><input type='password' size='15' class='clockinouttextfield' id='clockinoutpassword'/></td></tr>";
			content += "<tr><td id='clockinoutlogin'>" + messages.get('js.pin') + " </td><td><input type='password' size='15' class='clockinouttextfield' id='clockinoutpin'/></td></tr>";
			content += "<tr><td colspan='2' align='right'><a href='javascript: ClockInOut.clockInOut()'><img src='images/newUI/butn-clockin1.png' title='Clock In' border='0' width='140px' height='30px'></a></td>" ;
			content += "</table>";
		content += "</th>";
		content += "</tr>";
		content += "<tr><td colspan='4' align='right'><a href='javascript:Keyboard.toggle()'><img src='images/newIcons/keyboard.png'/></a></td></tr>";
		content += "</table>";
		
		ClockInOut.clockInOutPanel = new ModalWindow(content, false);
		
		var pinTextField = $('clockinoutpin');
		var usernameTextField = $('clockinoutusername');
		var passwordTextField = $('clockinoutpassword');
		
		var inputs = [usernameTextField,passwordTextField,pinTextField];
		for(var j=0; j<inputs.length; j++){
			var input = inputs[j];
			Event.observe(input,'click',Keyboard.setInput.bindAsEventListener(Keyboard),false);
		}
			
		/*Event.observe(pinTextField,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(pinTextField,'keyup',Keypad.keyUpHandler.bindAsEventListener(Keypad),false);*/
		
		ClockInOut.clockInOutPanel.show();
		
		ClockInOut.loadClockedInUsers();
	},
	
	disposePanel : function()
	{
		ClockInOut.clockInOutPanel.dispose();
	},
	
	loadClockedInUsers: function()
	{
		var url = "ClockInOutUserAction.do?action=getClockedInUsers";
		var pars = null;
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'get', 
			parameters: pars, 
			onSuccess: ClockInOut.loadClockedInSuccess, 
			onFailure: ClockInOut.loadClockedInFailure
		});	
	},
	
	loadClockedInSuccess: function(request)
	{
		var response = request.responseText;
		var result = eval('(' + response + ')');
		
		var content = "";
		content += "<tr bgcolor='#999999'><td id='textcolor' width='120px'>" + messages.get('js.username') + "</td><td id='textcolor'>" + messages.get('js.clockInTime') + "</td></tr>";
				
		for (var i = 0; i < result.length; i++)
		{
			content += "<tr><td>" + result[i].userName + "</td><td>" + result[i].clockInTime + "</td></tr>";
		}
		
		$('clockedinusers').innerHTML = content;
	},
	
	loadClockedInFailure: function()
	{
		
	},
	
	clockInOut: function()
	{
		var url = "ClockInOutUserAction.do?action=clockInOutUser";
		var pars = "username=" + $('clockinoutusername').value + "&password=" + $('clockinoutpassword').value + "&userPIN=" + $('clockinoutpin').value + "&clockInTime=" + DateUtil.getCurrentDate();
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'POST', 
			parameters: pars, 
			onSuccess: ClockInOut.clockInOutSuccess, 
			onFailure: ClockInOut.clockInOutFailure
		});	
	},
	
	
	
	clockInOutSuccess: function(request)
	{
		var response = request.responseText;
		var result = eval('(' + response + ')');
		
		if (result.Success && result.Success == true)
		{
			$('clockinoutusername').value = '';
			$('clockinoutpassword').value = '';
			$('clockinoutpin').value = '';
			$('clockinoutErrorMsg').innerHTML = "&nbsp;";
			ClockInOut.loadClockedInUsers();
		}
		else
		{
			$('clockinoutErrorMsg').innerHTML = result.Message;
		}
		
		/* clear session storage cache */
		sessionStorage.removeItem("CLOCK_IN_SALES_REPS");
		
		ClockInOut.loadClockInSalesRep();
	},
	
	clockInOutFailure: function(request)
	{
		alert(Translation.translate('clock.inout.failed.please.try.again','Clock in/out failed, please try again'));
	},
	
	
	loadClockInSalesRep: function()
	{
		console.log(">>> loadClockInSalesRep");
		/* check session storage */
		var cache = sessionStorage.getItem("CLOCK_IN_SALES_REPS");
		
		if(cache != null){
			
			console.log(">>> loadClockInSalesRep >> load from cache");
			
			ClockInOut.loadClockedInSalesRepSuccess({
				"responseText" : cache
			});
			
			return;
		}
		
		var url = "ClockInOutUserAction.do?action=getClockedInSalesRep";
		var pars = null;
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'get', 
			parameters: pars, 
			onSuccess: ClockInOut.loadClockedInSalesRepSuccess, 
			onFailure: ClockInOut.loadClockedInSalesRepFailure
		});	
	},
	
	loadClockedInSalesRepSuccess: function(request)
	{
		var response = request.responseText;
		var result = null;  /* eval('(' + response + ')'); */
		
		try 
		{
			sessionStorage.setItem("CLOCK_IN_SALES_REPS", response);
			result = JSON.parse(response)
		}
		catch(e)
		{
			console.log(e);
		}
		
		
		var salesRepId = result.salesRepId;
		var name = result.name;
		var isCurrentUserSalesRep = result.isSalesRep;
		
		ClockedInSalesRepManager.setCurrentUserAsSalesRep(isCurrentUserSalesRep);
		
		var salesReps = result.salesreps;
		
		var clockedInSalesReps_before = ClockedInSalesRepManager.getSalesReps();
		var currentSalesRep = new SalesRep(name, salesRepId);
		var previousSalesRep = ClockedInSalesRepManager.getCurrentSalesRep();
		
		ClockedInSalesRepManager.reset();
		
		for (var i = 0; i < salesReps.length; i++)
		{
			var salesRep = new SalesRep(salesReps[i].username, salesReps[i].userId);
			salesRep.setIdentifier(salesReps[i].identifier);
			salesRep.setHasAttachment(salesReps[i].hasAttachment);
			ClockedInSalesRepManager.addSalesRep(salesRep);
		}
		//ClockedInSalesRepManager.addSalesRep(currentSalesRep);
		
		if (previousSalesRep == null)
			previousSalesRep = currentSalesRep;
		
		ClockInOut.render(ClockedInSalesRepManager.getSalesReps(), currentSalesRep, previousSalesRep);
		//ClockInOut.renderSalesRepList(ClockedInSalesRepManager.getSalesReps());
		
		/*Check if current user is sales rep before handling split order*/
		
		if (!isCurrentUserSalesRep)
		{
			alert("Current user " + name +" is not a sales rep and will not be able to make orders!");
			
			return;
		}
		
		ClockedInSalesRepManager.fireClockedInSalesRepsLoadedEvent();
		
		var clockedInSalesReps_after = ClockedInSalesRepManager.getSalesReps();
		
		if (clockedInSalesReps_before.size() != 0)
		{
			if (clockedInSalesReps_before.size() != clockedInSalesReps_after.size())
			{
				ClockedInSalesRepManager.fireClockInOutEvent();
			}
			else
			{
				for (var i=0; i<clockedInSalesReps_after.size(); i++)
				{
					var clockedInSalesRep_after = clockedInSalesReps_after.get(i);
					if (!clockedInSalesReps_before.contains(clockedInSalesRep_after))
					{
						ClockedInSalesRepManager.fireClockInOutEvent();
					}
				}
			}
		}
	},
	
	render : function(salesReps, currentSalesRep, previousSalesRep)
	{    
		var content = '';
		var pos = 0;
		
		var enableCarousel = false;
		if(salesReps.size() > 6) enableCarousel = true;
		
		if(enableCarousel)
		{
			content = content + '<ul id="mycarousel" class="jcarousel-skin-tango">';
		}
		
		for (var i=0; i<salesReps.size(); i++)
		{
			var isCurrent = false;
			var salesRep = salesReps.get(i);
			if (salesRep.equals(currentSalesRep))
			{
				isCurrent = true;
				salesRep.setCurrent(true);				
				
				if (previousSalesRep != null && !previousSalesRep.equals(currentSalesRep))
				{
					previousSalesRep.setCurrent(false);
				}
			}
			else
			{
				isCurrent = false;
			}
			
			var contentId = 'salesRep' + salesRep.getId();
			var hasAttachment = salesRep.getHasAttachment();
			
			//var imgUrl = (isCurrent) ? 'images/employee.gif' : 'images/employee_inactive.gif';		
			/*var style = (isCurrent) ? 'background-color : rgba(57, 191, 0, .4);' : 'background-color : #F7F3EF;'*/
			//var styleClass = 'employee btn-sm btn ';
			var styleClass = 'employee ';
			
			if (isCurrent)
			{
				styleClass = 'employee active-sales-rep';
			}
			else
			{
				styleClass = 'employee';
			}	
			
				
			if(isCurrent) pos = i;			
				
			if(enableCarousel)
			{
				content += '<li>';
				content += '<button data-test-id="'+ salesRep.getName() +'" id="' + contentId + '" style="margin-right:4px;width:100%;color:#A8A8A8;background:transparent;border:none;" class="'+ styleClass +'" onclick="ClockInOut.setSalesRep(\'' + salesRep.getIdentifier() + '\')">'
				/*+ ('<img src="'+imgUrl+'" alt="employee"/>')*/
				+ '<span>' + salesRep.getName() + '</span><br/>'
				+ '<span id="salesRepAmount' + +salesRep.getId() + '">00,000.00</span></button>';
					
				content += '</li>';
			}
			else
			{
				content += '<button data-test-id="' + salesRep.getName() + '" id="' + contentId + '" style="margin-right:4px;width:15%;color:#A8A8A8;background:transparent;border:none;" class="'+ styleClass +'" onclick="ClockInOut.setSalesRep(\'' + salesRep.getIdentifier() + '\')">'
				/*+ ('<img src="'+imgUrl+'" alt="employee"/>')*/
				+ '<span>' + salesRep.getName() + '</span><br/>'
				+ '<span id="salesRepAmount' + +salesRep.getId() + '">00,000.00</span></button>';
				
			}
		}	
		
		if(enableCarousel)
		{
			content += '</ul>';
		}
		
		
		if($('clockedInSalesRep')){
			$('clockedInSalesRep').innerHTML = content;
			
			if(salesReps.size() > 6){
				jQuery('#mycarousel').jcarousel({
			    	visible: 6
			    });
				
				jQuery('#mycarousel').jcarousel('scroll', pos, false);
			}
			
		}	
	},
	
	loadClockedInSalesRepFailure: function(request)
	{
		
	},
	
	setSalesRep: function(identifier)
	{
		var url = "ClockInOutUserAction.do?action=setSalesRep";
		var pars = "identifier=" + identifier;
		var myAjax = new Ajax.Request( url, 
		{ 
			method: 'POST', 
			parameters: pars, 
			onSuccess: ClockInOut.setSalesRepSuccess, 
			onFailure: ClockInOut.setSalesRepFailure
		});	
	},
	
	setSalesRepSuccess: function(request)
	{
		/* clear session storage cache */
		sessionStorage.removeItem("CLOCK_IN_SALES_REPS");
		ClockInOut.loadClockInSalesRep();
	},
	
	setSalesRepFailure: function(request)
	{
	},
	
	init: function()
	{
		$('sales-rep-list').innerHTML = '';
		ClockInOut.renderList();
	},
	
	renderList : function()
	{
		var salesRepsList = ClockInOutManager.clockedInUsers;
		//var salesRepsList = ClockedInSalesRepManager.salesReps;
		
		$('sales-rep-list').innerHTML = '';
		
		var styleClass = null;
		var style = null;
		
		for(var i=0; i<salesRepsList.length; i++)
		{
			styleClass = 'clock-in-row';
			style = '';
			
			  var salesRep = salesRepsList[i];
			  var username = salesRep.username;
			  var clockInTime = salesRep.clockInTime;
			  var clockOutTime = salesRep.clockOutTime;
			  
			  if (clockOutTime != null && clockInTime != null)
			  {
				styleClass = 'not-clock-in-row';
			  } 
			  
			  if (i==0)
			 {
			   style = 'margin-top: 16px;';
			 }
			  
			  var div = document.createElement('div');
				div.setAttribute('class', 'row ' + styleClass);
				div.setAttribute('onclick', 'javascript: ClockInOut.clockInOrClockOut(\''+username+'\')');
				
				var div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-6 col-xs-6 padding-left-24');
				div2.setAttribute('style', style);
				
				var div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = username;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 padding-left-16');
				div2.setAttribute('style', style);
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				
				if (clockOutTime == null || clockOutTime == 'null')
				  {
					div3.innerHTML = moment(clockInTime).fromNow();
				  } 
				else
				{
					div3.innerHTML = '&nbsp';
				}
				
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-2 col-xs-2 padding-right-8');
				div2.setAttribute('style', style);
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-right');
				
				
				var iconDiv = document.createElement('div')
				iconDiv.setAttribute('class', 'glyphicons glyphicons-pro clock');
				div3.appendChild(iconDiv);
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				$('sales-rep-list').appendChild(div);
		  }
	},
	
	clockInOrClockOut : function(username)
	{
		var clockInOutPanel =  new ClockInOutPanel();
		clockInOutPanel.setUser(username);
		clockInOutPanel.show();
	}
};

/*Event.observe(window,'load',ClockInOut.loadClockInSalesRep,false);*/
