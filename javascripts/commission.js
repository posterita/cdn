var commissionLevelGrid = null;
var commissionLineGrid = null;

function initScreen()
{
	var active = $('CommissionForm').active.value;

	if (active == 'true')
	{
		$('CommissionForm').name.removeAttribute('disabled');
		$('CommissionForm').flatPercent.removeAttribute('disabled');
	}
	else if (active == 'false')
	{
		$('CommissionForm').name.setAttribute('disabled', 'disabled');
		$('CommissionForm').flatPercent.setAttribute('disabled', 'disabled');
	}
	
	commissionLevelGrid = new CommissionLevelGrid();
	var levelsText = $('CommissionForm').levels.value;
	if (levelsText != null && levelsText != '')
	{
		var levelsJSON = eval('('+levelsText+')');
		var levelsArray = levelsJSON.levels;
		for (var index = 0; index < levelsArray.length; index++)
		{
			var level = levelsArray[index];
			var fromAmt = level.fromAmt;
			var toAmt = level.toAmt;
			var percentage = level.percentage;
			
			var commissionLvlEditor = commissionLevelGrid.addCommissionLevel();
			commissionLvlEditor.setFromAmtValue(fromAmt);
			commissionLvlEditor.setToAmtValue(toAmt);
			commissionLvlEditor.setPercentageValue(percentage);
			
			if (active == 'true')
				commissionLvlEditor.enable();
			else if (active = 'false')
				commissionLvlEditor.disable();
		}
	}
	
	commissionLineGrid = new CommissionLineGrid();
	var linesText = $('CommissionForm').lines.value;
	if (linesText != null && linesText != '')
	{
		var linesJSON = eval('('+linesText+')');
		var linesArray = linesJSON.lines;
		for (var index = 0; index < linesArray.length; index++)
		{
			var line = linesArray[index];
			var commissionLineId = line.commissionLineId;
			var salesRepName = line.salesRepName;
			var salesRepId = line.salesRepId;
			var checked = line.checked;
			var hourlyRate = line.hourlyRate;
			
			var commissionLine = new CommissionLine(commissionLineId, salesRepName, salesRepId, checked, hourlyRate);
			var commissionLineEditor = new CommissionLineEditor(commissionLine);
			
			if (active == 'true')
				commissionLineEditor.enable();
			else if (active == 'false')
				commissionLineEditor.disable();
			
			commissionLineGrid.addCommissionLine(commissionLineEditor);			
		}
	}
	
	var message = $('CommissionForm').message.value;
	showStatus(message);
	
	var deleteCommissionPopUp = new DeleteCommission('CommissionAction.do?action=deleteCommission&recordId=');
	$('delete').onclick = function(e)
	{
		var recordId = document.forms[0].commissionId.value;
		deleteCommissionPopUp.setRecordId(recordId);
		deleteCommissionPopUp.show();
	}
}

function createCommission()
{
	var commissionLevels = commissionLevelGrid.getCommissionLevels();
	var levelsJSON = {'levels' : commissionLevels.toJSON()};

	var commissionLines = commissionLineGrid.getCommissionLines();
	var linesJSON = {'lines' : commissionLines.toJSON()};
	
	$('CommissionForm').levels.value = Object.toJSON(levelsJSON);
	$('CommissionForm').lines.value = Object.toJSON(linesJSON);
	$('CommissionForm').submit();
}

function showStatus (message)
{
	if (message == null || message.length == 0)
		return;
	
	var json = eval ('('+message+')');
	var success = json.success;
	var msg = json.msg;
	$('indicator').hide();
	if (success == 'true')
	{
		$('message-display-container').innerHTML = 'SAVED';
		$('message-display-container').setAttribute('class', 'message-display-container-saved');
	}
	else
	{
		$('message-display-container').innerHTML = 'ERROR';
		$('message-display-container').setAttribute('class', 'message-display-container-error');
		$('help').innerHTML = msg;
		var log = {'Error' : msg}
		var errorPopUpPanel = new ErrorPopUpPanel(log);
		errorPopUpPanel.init();
	}
}

Event.observe(window,'load', initScreen, false);