var CommissionLevel = Class.create({
	initialize : function()
	{
		this.fromAmt = null;
		this.toAmt = null;
		this.percentage = null;
	},
	
	setFromAmt : function(fromAmt)
	{
		this.fromAmt = fromAmt;
	},
	
	setToAmt : function(toAmt)
	{
		this.toAmt = toAmt;
	},
	
	setPercentage : function(percentage)
	{
		this.percentage = percentage;
	},
	
	getFromAmt : function()
	{
		
		return parseFloat(this.fromAmt).toFixed(2);
	},
	
	getToAmt : function()
	{
		return parseFloat(this.toAmt).toFixed(2);
	},
	
	getPercentage : function()
	{
		return this.percentage;
	},
	
	toJSON : function()
	{
		var fromAmt = this.getFromAmt();
		var toAmt = this.getToAmt();
		var percentage = this.getPercentage();
		var json = {'fromAmt' : fromAmt, 'toAmt' : toAmt, 'percentage' : percentage};
		return Object.toJSON(json);
	}
});

var CommissionLevelEditor = Class.create({
	initialize : function()
	{
		this.fromAmtTxtBox = document.createElement('input');
		this.fromAmtTxtBox.setAttribute('type', 'text');
		this.fromAmtTxtBox.setAttribute('class', 'commLevel');
		
		this.toAmtTxtBox = document.createElement('input');
		this.toAmtTxtBox.setAttribute('type', 'text');
		this.toAmtTxtBox.setAttribute('class', 'commLevel');
		
		this.percentageTxtBox = document.createElement('input');
		this.percentageTxtBox.setAttribute('type', 'text');
		this.percentageTxtBox.setAttribute('class', 'commLevel');
		
		this.commissionLevel = new CommissionLevel();
		this.init();
	},
	
	enable : function()
	{
		this.fromAmtTxtBox.removeAttribute('disabled');
		this.toAmtTxtBox.removeAttribute('disabled');
		this.percentageTxtBox.removeAttribute('disabled');
	},
	
	disable : function()
	{
		this.fromAmtTxtBox.setAttribute('disabled', 'disabled');
		this.toAmtTxtBox.setAttribute('disabled', 'disabled');
		this.percentageTxtBox.setAttribute('disabled', 'disabled');
	},
	
	init : function()
	{
		this.fromAmtTxtBox.onkeyup = this.setFromAmt.bind(this);
		this.toAmtTxtBox.onkeyup = this.setToAmt.bind(this);
		this.percentageTxtBox.onkeyup = this.setPercentage.bind(this);
	},
	
	setFromAmt : function()
	{
		this.commissionLevel.setFromAmt(this.fromAmtTxtBox.value);
	},
	
	setToAmt : function()
	{
		this.commissionLevel.setToAmt(this.toAmtTxtBox.value);
	},
	
	setPercentage : function()
	{
		this.commissionLevel.setPercentage(this.percentageTxtBox.value);
	},
	
	setFromAmtValue : function (value)
	{
		this.fromAmtTxtBox.value = parseFloat(value).toFixed(2);
		this.commissionLevel.setFromAmt(value);
	},
	
	setToAmtValue : function(value)
	{
		this.toAmtTxtBox.value = parseFloat(value).toFixed(2);
		this.commissionLevel.setToAmt(value);
	},
	
	setPercentageValue : function(value)
	{
		this.percentageTxtBox.value = parseFloat(value).toFixed(2);
		this.commissionLevel.setPercentage(value);
	},
	
	getFromAmtTxtBox : function()
	{
		return this.fromAmtTxtBox;
	},
	
	getToAmtTxtBox : function()
	{
		return this.toAmtTxtBox;
	},
	
	getPercentageTxtBox : function()
	{
		return this.percentageTxtBox;
	},
	
	getCommissionLevel : function()
	{
		return this.commissionLevel;
	}
});

var CommissionLevelGrid = Class.create({
	initialize : function()
	{
		this.commissionLevels = null;
		
		this.addLevelBtn = $('addLevel');
		this.clearLevelsBtn = $('clearLevels');
		this.levelGrid = $('levelGrid');
		
		this.addLevelBtn.onclick = this.addCommissionLevel.bind(this);
		this.clearLevelsBtn.onclick = this.clearLevels.bind(this);
				
		this.init();
	},
	
	init : function()
	{
		this.commissionLevels = new ArrayList(new CommissionLevel());
	},
	
	addCommissionLevel : function()
	{
		var commissionLevelEditor = new CommissionLevelEditor();
		var commissionLevel = commissionLevelEditor.getCommissionLevel();
		this.commissionLevels.add(commissionLevel);
		
		var fromAmtTxtBox = commissionLevelEditor.getFromAmtTxtBox();
		var toAmtTxtBox = commissionLevelEditor.getToAmtTxtBox();
		var percentageTxtBox = commissionLevelEditor.getPercentageTxtBox();
		var removeBtn = document.createElement('input');
		removeBtn.setAttribute('type', 'button');		
		removeBtn.setAttribute('value', 'REMOVE');
		var index = this.commissionLevels.size() - 1;
		removeBtn.setAttribute('id', index);
		removeBtn.onclick = this.removeRow.bind(this, removeBtn);
		
		this.drawRow(fromAmtTxtBox, toAmtTxtBox, percentageTxtBox, removeBtn);
		return commissionLevelEditor;
	},
	
	removeRow : function(removeBtn)
	{
		var row = removeBtn.parentNode.parentNode;
		this.levelGrid.removeChild(row);
		var index = parseInt(removeBtn.getAttribute('id'));
		this.commissionLevels.removeIndex(index);
	},
	
	clearLevels : function()
	{
		this.levelGrid.innerHTML = '';
		this.init();
	},
	
	drawRow : function(fromAmtTxtBox, toAmtTxtBox, percentageTxtBox, removeBtn)
	{
		var row = document.createElement('tr');
		var tdFromAmt = document.createElement('td');
		var tdToAmt = document.createElement('td');
		var tdPercentageAmt = document.createElement('td');
		var tdRemove = document.createElement('td');
		
		tdFromAmt.appendChild(fromAmtTxtBox);
		tdToAmt.appendChild(toAmtTxtBox);
		tdPercentageAmt.appendChild(percentageTxtBox);
		tdRemove.appendChild(removeBtn);
		
		row.appendChild(tdFromAmt);
		row.appendChild(tdToAmt);
		row.appendChild(tdPercentageAmt);
		row.appendChild(tdRemove);
		
		this.levelGrid.appendChild(row);
	},
	
	getCommissionLevels : function()
	{
		return this.commissionLevels;
	}
});

