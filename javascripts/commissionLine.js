
var CommissionLine = Class.create({
	initialize : function(commissionLineId, salesRepName, salesRepId, checked, hourlyRate)
	{
		this.commissionLineId = commissionLineId;
		this.salesRepName = salesRepName;
		this.salesRepId = salesRepId;
		this.checked = checked;
		this.hourlyRate = hourlyRate;
	},
	
	getCommissionLineId : function()
	{
		return this.commissionLineId;
	},
	
	getSalesRepName : function()
	{
		return this.salesRepName;
	},
	
	getSalesRepId : function()
	{
		return this.salesRepId;
	},
	
	getHourlyRate : function()
	{
		return this.hourlyRate;
	},
	
	setChecked : function(checked)
	{
		this.checked = checked;
	},
	
	isChecked : function()
	{
		return this.checked;
	},
	
	setHourlyRate : function(hourlyRate)
	{
		this.hourlyRate = hourlyRate;
	},
	
	toJSON : function()
	{
		var commissionLineId = this.getCommissionLineId();
		var salesRepId = this.getSalesRepId();
		var checked = this.isChecked();
		var hourlyRate = this.getHourlyRate();
		
		return "{'commissionLineId':"+commissionLineId+", 'salesRepId':"+salesRepId+", 'checked':"+checked+", 'hourlyRate':"+hourlyRate+"}";
	},
	
	equals : function(commissionLine)
	{
		return this.getCommissionLineId() == commissionLine.getCommissionLineId();
	}
});

var CommissionLineEditor = Class.create({
	initialize : function(commissionLine)
	{
		this.commissionLine = commissionLine;
		this.label = commissionLine.getSalesRepName();
		this.checkbox = null;
		this.hourlyRateInput = null;
		this.initComponent();
	},
	
	initComponent : function()
	{
		this.checkbox = document.createElement('input');
		this.checkbox.setAttribute('type', 'checkbox');
		this.checkbox.setAttribute('id', this.commissionLine.getCommissionLineId());
		this.checkbox.onclick = this.setChecked.bind(this);
		
		this.hourlyRateInput = document.createElement('input');
		this.hourlyRateInput.setAttribute('class', 'commission-line-input');
		this.hourlyRateInput.setAttribute('type', 'text');
		this.hourlyRateInput.setAttribute('name', 'hourlyRate');
		this.hourlyRateInput.setAttribute('id', this.commissionLine.getSalesRepId())
		this.hourlyRateInput.onkeyup = this.setHourlyRate.bind(this);
		
		if (this.commissionLine.isChecked())
			this.check();
		else
			this.uncheck();
		this.hourlyRateInput.value = this.commissionLine.getHourlyRate();
	},
	
	enable : function()
	{
		this.checkbox.removeAttribute('disabled');
		this.hourlyRateInput.removeAttribute('disabled');
	},
	
	disable : function()
	{
		this.checkbox.setAttribute('disabled', 'disabled');
		this.hourlyRateInput.setAttribute('disabled', 'disabled');
	},
	
	setHourlyRate : function()
	{
		var value = this.hourlyRateInput.value;
		this.commissionLine.setHourlyRate(value);
	},
	
	getCommissionLine : function()
	{
		return this.commissionLine;
	},
	
	getCheckbox : function()
	{
		return this.checkbox;
	},
	
	getHourlyRateInput : function()
	{
		return this.hourlyRateInput;
	},
	
	setChecked : function()
	{
		var checked = this.checkbox.checked;
		this.commissionLine.setChecked(checked);
	},
	
	check : function()
	{
		this.checkbox.checked = true;
		this.setChecked();
	},
	
	uncheck : function()
	{
		this.checkbox.checked = false;
		this.setChecked();
	},
	
	equals : function (commissionLineEditor)
	{
	    var commissionLine = commissionLineEditor.getCommissionLine();
	    return (this.commissionLine.equals(commissionLine));
	}
});

var CommissionLineGrid = Class.create({
	initialize : function()
	{
		this.commissionLines = new ArrayList(new CommissionLine());
		this.commissionLineEditors = new ArrayList(new CommissionLineEditor(new CommissionLine()));
		this.lineGrid = $('lineGrid');
		this.selectAll = $('selectAllSalesRep');
		this.removeAll = $('removeAllSalesRep');
		this.init();
	},
	
	init : function()
	{
		this.selectAll.onclick = this.selectAllBoxes.bind(this);
		this.removeAll.onclick = this.removeAllBoxes.bind(this);
	},
	
	selectAllBoxes : function()
	{
		for (var i=0; i<this.commissionLineEditors.size(); i++)
		{
			var editor = this.commissionLineEditors.get(i);
			if (editor != null)
			{
				editor.check();
			}
		}
	},
	
	removeAllBoxes : function()
	{
		for (var i=0; i<this.commissionLineEditors.size(); i++)
		{
			var editor = this.commissionLineEditors.get(i);
			if (editor != null)
				editor.uncheck();
		}
	},
	
	addCommissionLine : function(commissionLineEditor)
	{
		var commissionLine = commissionLineEditor.getCommissionLine();
		
		var divCommissionLine = document.createElement('div');
		divCommissionLine.setAttribute('style', 'padding:10px;');
		divCommissionLine.setAttribute('id', commissionLine.getCommissionLineId() + "div");
		
		var checkbox = commissionLineEditor.getCheckbox();
		var hourlyRateInput = commissionLineEditor.getHourlyRateInput();
		
		var label =  document.createElement('span');
		label.setAttribute('style', 'padding-left:10px;');
		label.innerHTML = commissionLine.getSalesRepName();
		
		divCommissionLine.appendChild(checkbox);
		divCommissionLine.appendChild(label);
		
		var td1 = document.createElement('td');
		var td2 = document.createElement('td');
		var tr = document.createElement('tr');
		
		td1.appendChild(divCommissionLine);
		td2.appendChild(hourlyRateInput);
		
		tr.appendChild(td1);
		tr.appendChild(td2);
		
		this.lineGrid.appendChild(tr);
		
		this.commissionLines.add(commissionLine);
		this.commissionLineEditors.add(commissionLineEditor);
	},
	
	getCommissionLines : function()
	{
		return this.commissionLines;
	}
	
});
