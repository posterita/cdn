var TaxController = Class.create(Controller,{
	initialize : function($super, json)
	{
		var controller = json.controller;
		var tax = new Model(json.tax);
		
		$super(controller, tax);
		this.tree = 'TaxTree';
	}
});

var TaxCategoryController = Class.create(Controller,{
	initialize : function($super, json)
	{
		var controller = json.controller;
		var taxCategory = new Model(json.taxCategory);
		
		$super(controller, taxCategory);
		this.tree = 'TaxTree';
	}
});

var ProductController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json.product);
		var priceSO = new Model(json.priceSO);
		var pricePO = new Model(json.pricePO);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
		this.addDependent(pricePO);
		
		this.productParentId = null;
		
		this.resetDisplay();
	},
	
	onInit : function()
	{
		var options = $('product.ProductType').options;
		for (var i=0; i<options.length; i++)
		{
			var option = options[i];
			if (option.value == 'I' || option.value == 'S')
			{
				option.style.display = 'block';
			}
			else
			{
				option.style.display = 'none';
			}
		}

		var uomOptions = $('product.C_UOM_ID').options;
		if (this.getId() == 0)
		{
			for (var i=0; i<uomOptions.length; i++)
			{
				var option = uomOptions[i];
				if (option.value == '100')
				{
					option.selected = 'selected';
				}
				else
				{
					option.removeAttribute('selected');
				}
			}
		}
		
	},
	
	updateState : function(state)
	{
		if ($('product.summary.newPrice') != null)
		{
			$('product.summary.newPrice').innerHTML = state.PriceStd;
		}
		
		if ($('product.summary.oldPrice') != null)
		{
			$('product.summary.oldPrice').innerHTML = state.PriceList;
		}
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('product_Group' + i);
			var input = $('product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
					element.style.display = 'block';
				else
					element.style.display = 'none';
			}
		}
		
		$('product.PrimaryGroup').onkeyup = function(e)
		{
			$('product_Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('product.Group' + i);
			var group_next = this.getField('product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					
					var nextElement = $("product_" + name + (no+1));
					
					if (nextElement != null)
						nextElement.style.display = 'block';
				}
			}
		}
		
		this.variantDisplayLogic();
	},
	
	variantDisplayLogic : function()
	{
		this.resetDisplay();
		
		var variant1NameInput = $('product.Variant1Name');
		var variant2NameInput = $('product.Variant2Name');
		var variant3NameInput = $('product.Variant3Name');
		
		var variant1NameText = $('product.Variant1NameText');
		var variant2NameText = $('product.Variant2NameText');
		var variant3NameText = $('product.Variant3NameText');
		
		var variant1Input = $('product.Variant1');
		var variant2Input = $('product.Variant2');
		var variant3Input = $('product.Variant3');
		
		var variant1Div = $('product_Variant1');
		var variant2Div = $('product_Variant2');
		var variant3Div = $('product_Variant3');
		
		var parentProductDiv = $('product.parentId');
		
		var parentProductInput = $('product.M_Product_Parent_ID');
		var parentProductId = $('product.M_Product_Parent_IDText').innerHTML;
		
		var serialNoDiv = $('product_SerialNo');
		var isSerialNoDiv = $('product_IsSerialNo');
		var isSerialNoInput = $('product.IsSerialNo');
		
		var productDetailsContainer = $('product-details-container');
		
		var variantUPCDiv = $('variant-upc-container');
		var variantUPCInput = $('variant-upc');
		var variantSKUInput = $('variant-sku');
		var variantSKUDiv = $('variant-sku-container');
		
		var productUPCDiv = $('product-upc-container');
		var productUPCInput = $('product.UPC');
		
		var productSKUDiv = $('product-sku-container');
		var productSKUInput =$('product.SKU');
		
		variantUPCInput.onkeyup = function(e){
			productUPCInput.value = variantUPCInput.value;
		};
		
		variantSKUInput.onkeyup = function(e){
			productSKUInput.value = variantSKUInput.value;
		};
		
		variantUPCInput.onblur = function(e){
			productUPCInput.value = variantUPCInput.value;
		};
		
		variantSKUInput.onblur = function(e){
			productSKUInput.value = variantSKUInput.value;
		};
		
		if (parentProductId != '-1' && parentProductId != '' && parentProductId != null)
		{
			parentProductInput.value = parentProductId;
			
			//isSerialNoDiv.style.display = 'block';
			
			if (isSerialNoInput.checked == false)
			{
				variant1NameInput.style.display = 'none';
				variant2NameInput.style.display = 'none';
				variant3NameInput.style.display = 'none';
				
				variant1Div.style.display = 'block';
				variant2Div.style.display = 'block';
				variant3Div.style.display = 'block';
				
				variant1NameText.style.display = 'block';
				variant2NameText.style.display = 'block';
				variant3NameText.style.display = 'block';
				
				variant1NameText.style.paddingTop = '0px';
				variant2NameText.style.paddingTop = '0px';
				variant3NameText.style.paddingTop = '0px';
				
				if (variant1NameText.innerHTML == '')
				{
					variant1NameText.innerHTML = 'Variant1'; 
				}
				
				if (variant2NameText.innerHTML == '')
				{
					variant2NameText.innerHTML = 'Variant2'; 
				}
				
				if (variant3NameText.innerHTML == '')
				{
					variant3NameText.innerHTML = 'Variant3'; 
				}
			}
			else
			{
				variant1Div.style.display = 'none';
				variant2Div.style.display = 'none';
				variant3Div.style.display = 'none';
				parentProductDiv.style.display = 'none';
				serialNoDiv.style.display = 'block';
				
				var variantName = $('variant-name').value;
				var productName = $('product.Name').value;
				
				/*if ($('product.M_Product_Parent_ID').value != '' && variantName != '' && productName.indexOf(variantName) != -1 )
				{
					$('product.Name').value = $('variant-name').value
				}*/
				
				//this.setVariantBehaviour();
				//isSerialNoDiv.style.display = 'none';
			}
			
			this.setVariantBehaviour();
			
			productUPCDiv.style.display = 'none';
			productSKUDiv.style.display = 'none';
			
			variantSKUInput.value = productSKUInput.value;
			variantUPCInput.value = productUPCInput.value;
			
			variantSKUDiv.style.display = 'block';
			variantUPCDiv.style.display = 'block';
			
			$('product-description-container').style.display = 'none';
					
			var inputs = productDetailsContainer.getElementsByTagName('input');
			
			for (var i=0; i<inputs.length; i++)
			{
				inputs[i].style.color = 'black';
				inputs[i].style.backgroundColor = 'grey';
				inputs[i].readOnly = true;
			}
			
			var selects = productDetailsContainer.getElementsByTagName('select');
			
			for (var i=0; i<selects.length; i++)
			{
				selects[i].style.backgroundColor = 'grey';
				selects[i].disabled = true;
			}
		}
		else 
		{
			variant1Div.style.display = 'none';
			variant2Div.style.display = 'none';
			variant3Div.style.display = 'none';
			parentProductDiv.style.display = 'none';
			serialNoDiv.style.display = 'none';
			//isSerialNoDiv.style.display = 'none';
			variantUPCDiv.style.display = 'none';
			variantSKUDiv.style.display = 'none';
		}
	},
	
	resetDisplay : function()
	{
		var productDetailsContainer = $('product-details-container');
		
		var inputs = productDetailsContainer.getElementsByTagName('input');
		
		for (var i=0; i<inputs.length; i++)
		{
			inputs[i].style.backgroundColor = '#FFFFFF';
			inputs[i].readOnly = false;
			inputs[i].style.borderColor = '#CCCCCC';
		}
		
		var selects = productDetailsContainer.getElementsByTagName('select');
		
		for (var i=0; i<selects.length; i++)
		{
			selects[i].style.backgroundColor = '#FFFFFF';
			selects[i].disabled = false;
		}
		
		$('product-upc-container').style.display = 'block';
		$('product-sku-container').style.display = 'block';
		$('product.IsSerialNo').style.display = '';
		
		/*var variantName = $('variant-name').value;
		var productName = $('product.Name').value;
		
		if (variantName != '' && variantName.indexOf(productName) != -1)
		{
			$('product.Name').value = $('variant-name').value
		}*/
		
		$('product-name-container').style.display = 'block';
		$('product-description-container').style.display = 'block'; 
		
		$('variant-name-container').style.display = 'none';
		
		$('variant-sku-container').style.display = 'none';
		$('variant-upc-container').style.display = 'none';
		
		if (this.getId() == 0)
		{
			$('product.Variant1').value = '';
			$('product.Variant2').value = '';
			$('product.Variant3').value = '';
			$('product.SerialNo').value = '';
		}
		
		$('product_Variant1').style.display = 'none';
		$('product_Variant2').style.display = 'none';
		$('product_Variant3').style.display = 'none';
		
		/*$('product.M_Product_Parent_ID').value = '';
		$('product.M_Product_Parent_IDText').innerHTML = '';*/
		
		var variantName = $('variant-name').value;
		var productName = $('product.Name').value;
		
		/*if ($('product.M_Product_Parent_ID').value != '' && variantName != '' && productName.indexOf(variantName) != -1 )
		{
			$('product.Name').value = $('variant-name').value
		}*/
		
		$('product_SerialNo').style.display = 'none';
		
	},
	
	setVariantBehaviour : function()
	{
		var variant1Input = $('product.Variant1');
		var variant2Input = $('product.Variant2');
		var variant3Input = $('product.Variant3');
		var isSerialNoInput = $('product.IsSerialNo');
		var serialNoInput = $('product.SerialNo');
		var productNameInput = $('product.Name');
		var variantNameInput = $('variant-name');
		var productNameDiv = $('product-name-container');
		var variantNameDiv = $('variant-name-container');
		var variantUPCInput = $('variant-upc');
		var variantSKUInput = $('variant-sku');
		var productUPCInput = $('product.UPC');
		var productSKUInput =$('product.SKU');
		var productDescriptionDiv = $('product-description-container');
		
		productNameDiv.style.display = 'none';
		productDescriptionDiv.style.display = 'none';
		variantNameInput.value = productNameInput.value;
		variantNameDiv.style.display = 'inline-block';
		variantNameInput.style.display = 'inline-block';
		
		if (this.getId() == 0)
		{
			variantUPCInput.value = '';
			variantSKUInput.value = '';
			productUPCInput.value = '';
			productSKUInput.value = '';
		}
		
		if (isSerialNoInput.checked == true)
		{
			serialNoInput.onkeyup = function(e){
				
				productNameInput.value = variantNameInput.value + ' ' + serialNoInput.value
			}
			
			serialNoInput.onblur = function(e){
				
				productNameInput.value = variantNameInput.value + ' ' + serialNoInput.value
			}
		}
		else
		{
			var variant1 = variant1Input.value;
			var variant2 = variant2Input.value;
			var variant3 = variant3Input.value;
			
			if (variant1 == null || variant1 == 'null')
			{
				variant1 = '';
			}
			
			if (variant2 == null || variant2 == 'null')
			{
				variant2 = '';
			}
			
			if (variant3 == null || variant3 == 'null')
			{
				variant3 = '';
			}
			
			var productName = '';
			
			variant1Input.onkeyup = function(e){
				
				variant1 = variant1Input.value;
				productName = variantNameInput.value + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
				productNameInput.value = productName;
			}
			
			variant2Input.onkeyup = function(e){
				
				variant2 = variant2Input.value;
				productName = variantNameInput.value + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
				productNameInput.value = productName;
			}
			
			variant3Input.onkeyup = function(e){
				
				variant3 = variant3Input.value;
				productName = variantNameInput.value + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
				productNameInput.value = productName;
			}
			
			variant1Input.onblur = function(e){
				
				variant1 = variant1Input.value;
				productName = variantNameInput.value + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
				productNameInput.value = productName;
			}
			
			variant2Input.onblur = function(e){
				
				variant2 = variant2Input.value;
				productName = variantNameInput.value + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
				productNameInput.value = productName;
			}
			
			variant3Input.onblur = function(e){
				
				variant3 = variant3Input.value;
				productName = variantNameInput.value + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
				productNameInput.value = productName;
			}
		}
	},
	
	initEditVariant : function(searchObject)
	{
		if (!searchObject.isParent && searchObject.m_product_parent_id != '')
		{
			$('item-title').value = searchObject.name;
			var variantNameInput = $('variant-name');
			variantNameInput.value = searchObject.parentName;
			variantNameInput.style.display = 'inline-block';
			
			$('product-name-container').style.display = 'none';
			$('variant-name-container').style.display = 'block';
			
			var isSerialNo = searchObject.isSerialNo;
			var serialNoInput = $('product.SerialNo');
			var productNameInput = $('product.Name');
			var variantNameInput = $('variant-name');
			
			var variant1Input = $('product.Variant1');
			var variant2Input = $('product.Variant2');
			var variant3Input = $('product.Variant3');
			
			if (isSerialNo)
			{
				serialNoInput.onkeyup = function(e){
					
					productNameInput.value = searchObject.parentName + ' ' + serialNoInput.value
				}
				
				serialNoInput.onblur = function(e){
					
					productNameInput.value = searchObject.parentName + ' ' + serialNoInput.value
				}
			}
			else
			{
				var variant1 = variant1Input.value;
				var variant2 = variant2Input.value;
				var variant3 = variant3Input.value;
				
				if (variant1 == null || variant1 == 'null')
				{
					variant1 = '';
				}
				
				if (variant2 == null || variant2 == 'null')
				{
					variant2 = '';
				}
				
				if (variant3 == null || variant3 == 'null')
				{
					variant3 = '';
				}
				
				var productName = '';
				
				variant1Input.onkeyup = function(e){
					
					variant1 = variant1Input.value;
					productName = searchObject.parentName + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
					productNameInput.value = productName;
				}
				
				variant2Input.onkeyup = function(e){
					
					variant2 = variant2Input.value;
					productName = searchObject.parentName + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
					productNameInput.value = productName;
				}
				
				variant3Input.onkeyup = function(e){
					
					variant3 = variant3Input.value;
					productName = searchObject.parentName + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
					productNameInput.value = productName;
				}
				
				variant1Input.onblur = function(e){
					
					variant1 = variant1Input.value;
					productName = searchObject.parentName + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
					productNameInput.value = productName;
				}
				
				variant2Input.onblur = function(e){
					
					variant2 = variant2Input.value;
					productName = searchObject.parentName + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
					productNameInput.value = productName;
				}
				
				variant3Input.onblur = function(e){
					
					variant3 = variant3Input.value;
					productName = searchObject.parentName + ' ' + variant1  + ' ' + variant2  + ' ' + variant3;
					productNameInput.value = productName;
				}
			}
		}
	},
	
	beforeSave : function()
	{
		var isSerialNoInput = $('product.IsSerialNo');
		var serialNoInput = $('product.SerialNo');
		var parentProductId = $('product.M_Product_Parent_IDText').innerHTML;
		
		var variant1Input = $('product.Variant1');
		var variant2Input = $('product.Variant2');
		var variant3Input = $('product.Variant3');
		
		if (parentProductId != '-1' && parentProductId != '' && parentProductId != null)
		{
			if (isSerialNoInput.checked == true)
			{
				if (serialNoInput.value == null || serialNoInput.value ==  '')
				{
					alert(Translation.translate('serial.no.is.mandatory')+"!");
					return false;
				}
			}
			else
			{
				if ((variant1Input.value == '' || variant1Input.value == null) &&
						(variant2Input.value == '' || variant2Input.value == null) &&
						(variant3Input.value == '' || variant3Input.value == null))
				{
					alert(Translation.translate('fill.variant1.variant2.variant3'));
					return false;
				}
			}
		}
		
		return true;
	}
});


var ProductTemplateController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json.product);
		var priceSO = new Model(json.priceSO);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('product_Group' + i);
			var input = $('product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
					element.style.display = 'block';
				else
					element.style.display = 'none';
			}
		}
		
		$('product.PrimaryGroup').onkeyup = function(e)
		{
			$('product_Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('product.Group' + i);
			var group_next = this.getField('product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					var nextElement = $("product_" + name + (no+1));
					if (nextElement != null)
						nextElement.style.display = 'block';
				}
			}
		}
	}
});


var ProductLeftLookupController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json['left.product']);
		var priceSO = new Model(json['left.priceSO']);
		var pricePO = new Model(json['left.pricePO']);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
		this.addDependent(pricePO);
		
		this.setHelpComp('lookup-item-help');
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('left.product_Group' + i);
			var input = $('left.product.Group' + i);
			var copyLeftDiv = $('product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
				{
					element.style.display = 'block';
					copyLeftDiv.style.display = 'block';
				}
				else
				{
					element.style.display = 'none';
					copyLeftDiv.style.display = 'none';
				}
			}
		}
		
		$('left.product.PrimaryGroup').onkeyup = function(e)
		{
			$('left.product_Group1').style.display = 'block';
			$('product.Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('left.product.Group' + i);
			var group_next = this.getField('left.product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('left.product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					var nextElement = $('left.product_'+name + (no+1));
					if (nextElement != null)
					{
						nextElement.style.display = 'block';
						$('product.Group' + (no +1)).style.display = 'block';
					}
				}
			}
		}
	}
});

var ProductRightLookupController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json['right.product']);
		var priceSO = new Model(json['right.priceSO']);
		var pricePO = new Model(json['right.pricePO']);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
		this.addDependent(pricePO);
		
		this.setHelpComp('lookup-item-help');
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('right.product_Group' + i);
			var input = $('right.product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
					element.style.display = 'block';
				else
					element.style.display = 'none';
			}
		}
		
		$('right.product.PrimaryGroup').onkeyup = function(e)
		{
			$('right.product_Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('right.product.Group' + i);
			var group_next = this.getField('right.product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('right.product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					var nextElement = $('right.product_'+name + (no+1));
					if (nextElement != null)
						nextElement.style.display = 'block';
				}
			}
		}
	}
});

var OrgController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		
		var organisation = new Model(json.organisation);		
		var partner = new Model(json.partner);
		var user = new Model(json.user);
		var location = new Model(json.location);
		var orgInfo = new Model(json.orgInfo);
		
		$super(controller, organisation);
		this.tree = 'StoreTree';
		
		this.addDependent(partner);
		this.addDependent(user);
		this.addDependent(location);
		this.addDependent(orgInfo);
	},
	
	displayLogic : function()
	{
		/*var displayLogic = '(@location.C_Country_ID@ == "100")';
		this.addDisplayLogic('State', displayLogic);*/
	}
});

var BankController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var bankModel = new Model(json.bank);
		$super(json.controller, bankModel);
		this.tree = 'BankTree';
	}
});

var BankAccountController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var bankAccountModel = new Model(json.bankAccount);
		$super(json.controller, bankAccountModel);
		this.tree = 'BankTree';
	}
});

var RoleController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var roleModel = new Model(json.role);
		$super(json.controller, roleModel);
		this.tree = 'UserRoleTree';
	}
});

var PartnerController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var partner = new Model(json.partner);
		var user = new Model(json.user);
		var location = new Model(json.location);
		
		$super(controller, partner);
		
		this.addDependent(user);
		this.addDependent(location);
	}
});

var VendorController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var partner = new Model(json.vendor_partner);
		var user = new Model(json.vendor_user);
		var location = new Model(json.vendor_location);
		
		$super(controller, partner);
		
		this.addDependent(user);
		this.addDependent(location);
	}
});

var UserController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.partner = new Model(json.partner);
		this.user = new Model(json.user);
		this.location = new Model(json.location);
		this.userRoles = new Model(json.userRoles);
		
		$super(controller, this.user);
		this.tree = 'UserRoleTree';
		
		this.addDependent(this.partner);
		this.addDependent(this.location);
		this.addDependent(this.userRoles);
	},
	
	isActive : function()
	{
		return this.user.isActive();
	},
	
	disable : function($super)
	{
		$super();
		$('user.ConfirmPassword').disable();
	},
	
	enable : function($super)
	{
		$super();
		$('user.ConfirmPassword').enable();
	},
	
	init : function($super)
	{
		$super();
		var password = $('user.Password').value;
		$('user.ConfirmPassword').value = password;
	},
	
	beforeSave : function()
	{
		return this.confirmPassword();
	},
	
	confirmPassword : function()
	{
		var password = $('user.Password').value;
		var confirm = $('user.ConfirmPassword').value;
		
		if (password != confirm)
		{
			alert(Translation.translate('passwords.not.match'));
			return false;
		}
		return true;
	}
});

var PaymentTermController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var paymentTerm = new Model(json.paymentTerm);
		
		$super(controller, paymentTerm);
	},
	
	displayLogic : function()
	{
		var displayLogic = "(@paymentTerm.IsDueFixed@ == 'true')";
		this.addDisplayLogic('FixMonthDay', displayLogic);
		this.addDisplayLogic('FixMonthOffset', displayLogic);
		this.addDisplayLogic('FixMonthCutoff', displayLogic);
	}	
});


var PaymentController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var payment = new Model(json.payment);
		
		$super(controller, payment);
	},
	
	displayLogic : function()
	{
		var checkDL = "(@payment.TenderType@ == 'K')";
		this.addDisplayLogic('CheckNo', checkDL);
		
		var creditCardDL = "(@payment.TenderType@ == 'C')";
		this.addDisplayLogic('Swipe', creditCardDL);
		this.addDisplayLogic('CreditCardType', creditCardDL);
		this.addDisplayLogic('CreditCardNumber', creditCardDL);
		this.addDisplayLogic('CreditCardExpMM', creditCardDL);
		this.addDisplayLogic('CreditCardExpYY', creditCardDL);
		this.addDisplayLogic('CreditCardVV', creditCardDL);
		this.addDisplayLogic('AccountName', creditCardDL);
		this.addDisplayLogic('AccountStreet', creditCardDL);
		this.addDisplayLogic('AccountZip', creditCardDL);
	},
	
	initParameters : function($super)
	{
		$super();
		var d = new Date();
		var clientTime = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
		
		this.parametersMap.set('payment.DateAcct', clientTime);
		this.parametersMap.set('payment.DateTrx', clientTime);
	}
});

var PaymentProcessorController = Class.create(Controller, {
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.paymentProcessor = new Model(json.paymentProcessor);
		
		$super(controller, this.paymentProcessor);
		this.tree = 'BankTree';
		
		this.dynamicValidationJSON = new Hash();
		
		this.dynamic = new Hash();
		this.dynamic.set('columnName', 'AD_Org_ID');
		
		var bankAccount = new Hash();
		bankAccount.set('columnName', 'C_BankAccount_ID');
		bankAccount.set('prefix', this.paymentProcessor.prefix);
		bankAccount.set('tableId', this.paymentProcessor.tableId);
		
		var dependents = new ArrayList();
		dependents.add(bankAccount);
		
		this.dynamicValidationJSON.set('dependents', dependents);
	},
	
	init : function($super)
	{
		$super();
		$('paymentProcessor.PayProcessorClass').onchange = this.initCallout.bind(this);
		this.initDynamicValidation();
	},
	
	initCallout : function()
	{
		var payProcessorClass = $('paymentProcessor.PayProcessorClass').value;
		if (payProcessorClass == null) return; 
		
		if (payProcessorClass == 'org.compiere.model.PP_XWeb')
		{
			$('paymentProcessor.HostAddress').value = 'https://gw.t3secure.net/x-chargeweb.dll';
			$('paymentProcessor.HostPort').value = '0';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Authorize')
		{
			$('paymentProcessor.HostAddress').value = 'https://secure.authorize.net/gateway/transact.dll';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_ElementPS')
		{
			$('paymentProcessor.HostAddress').value = 'https://transaction.elementexpress.com';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Mercury_HF')
		{
			$('paymentProcessor.HostAddress').value = 'https://hc.mercurypay.com/hcws/hcservice.asmx';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Mercury_EE')
		{
			$('paymentProcessor.HostAddress').value = 'https://w2.backuppay.com/ws/ws.asmx';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Century')
		{
			$('paymentProcessor.HostAddress').value = 'https://secure.nmi.com/api/v2/three-step';
			$('paymentProcessor.HostPort').value = '443';
		}
		else
		{
			$('paymentProcessor.HostAddress').value = '';
			$('paymentProcessor.HostPort').value = '';
		}
	},
	
	dynamicValidation : function()
	{
		$('paymentProcessor.AD_Org_ID').onchange = this.initDynamicValidation.bind(this);
	},
	
	initDynamicValidation : function()
	{
		if (this.paymentProcessor.getId() != 0)
			return;
		
		this.dynamic.set('value', this.paymentProcessor.getColumnValue('paymentProcessor.AD_Org_ID'));
		this.dynamicValidationJSON.set('dynamic', this.dynamic);
		
		var url = 'ModelAction.do';
		var pars = 'action=dynamicValidation&controller=' + this.className + '&id=' + 
		this.controllerId + '&windowNo=' + this.windowNo + '&dynamicValidation=' + this.dynamicValidationJSON.toJSON();
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.updateDynamicFields.bind(this)
		});	
	},
	
	updateDynamicFields : function(response)
	{
		var fieldMap = this.paymentProcessor.getFieldMap();
		var json = eval('(' + response.responseText + ')');
		for (var j in json)
		{
			var column = json[j];
			var f = fieldMap.get(column.displayId);
			f.field = column.field;
			
			f.initDynUpdate();
		}
		
		this.initNonUpdateableFields();
	}
});

var TerminalController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.terminal = new Model(json.terminal);
		this.cashBook = new Model(json.cashBook);
		
		$super(controller, this.terminal);
		this.tree = 'StoreTree';
		
		this.addDependent(this.cashBook);
		
		this.dynamicValidationJSON = new Hash();
		
		this.dynamic = new Hash();
		this.dynamic.set('columnName', 'AD_Org_ID');
		
		var dependents = new ArrayList();
		var cardBA = new Hash();
		var warehouse = new Hash();
		
		cardBA.set('columnName', 'Card_BankAccount_ID');
		warehouse.set('columnName', 'M_Warehouse_ID');
		
		cardBA.set('prefix', this.terminal.prefix);
		warehouse.set('prefix', this.terminal.prefix);
		
		cardBA.set('tableId', this.terminal.tableId);
		warehouse.set('tableId', this.terminal.tableId);
		
		dependents.add(cardBA);
		dependents.add(warehouse);
		
		this.dynamicValidationJSON.set('dependents', dependents);
	},
	
	dynamicValidation : function()
	{
		$('terminal.AD_Org_ID').onchange = this.initDynamicValidation.bind(this);
	},
	
	initDynamicValidation : function()
	{
		this.dynamic.set('value', this.terminal.getColumnValue('terminal.AD_Org_ID'));
		this.dynamicValidationJSON.set('dynamic', this.dynamic);
		
		var url = 'ModelAction.do';
		var pars = 'action=dynamicValidation&controller=' + this.className + '&id=' + 
		this.controllerId + '&windowNo=' + this.windowNo + '&dynamicValidation=' + this.dynamicValidationJSON.toJSON();
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.updateDynamicFields.bind(this)
		});	
	},
	
	updateDynamicFields : function(response)
	{
		var fieldMap = this.terminal.getFieldMap();
		var json = eval('(' + response.responseText + ')');
		for (var j in json)
		{
			var column = json[j];
			var field = fieldMap.get(column.displayId);
			field.field = column.field;
			
			field.initReplace();
		}
	}
	
});

var WarehouseController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var warehouse = new Model(json.warehouse);
		var location = new Model(json.location);
		
		$super(controller, warehouse);
		
		this.addDependent(location);
	}	
});


var PriceListController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var priceList = new Model(json.priceList);
		var priceListVersion = new Model(json.priceListVersion);
		
		$super(controller, priceList);
		
		this.addDependent(priceListVersion);
	}	
});


var CloseTillController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.closeTill = new Model(json.closeTill);
		
		$super(controller, this.closeTill);
		
	},
	
	initKeyLogic : function()
	{
		$('closeTill.TotalCashAmount').onkeyup = this.computeTotalAmt.bind(this);
		$('closeTill.VoucherAmtEntered').onkeyup = this.computeTotalAmt.bind(this);		
		
		var beginningBalance = this.closeTill.getColumnValue('closeTill.BeginningBalance');
		var floatAmt = this.closeTill.getColumnValue('closeTill.FloatAmount');
		
		beginningBalance = (beginningBalance == null) ? 0 : beginningBalance;
		floatAmt = (floatAmt == null) ? 0 : floatAmt;
		
		var totalCash = parseFloat($('closeTill.TotalCashAmount').value);
		
		if (isNaN(totalCash) || totalCash == 0)
		{
			$('CashSales').innerHTML = 0;
			$('TotalCashAmount').innerHTML = new Number(beginningBalance).toFixed(2);
			$('TransferAmount').innerHTML = new Number(beginningBalance - floatAmt).toFixed(2);
			$('EndingBalance').innerHTML = new Number(floatAmt).toFixed(2);
		}
	},
	
	computeTotalAmt : function()
	{
		var totalCash = parseFloat($('closeTill.TotalCashAmount').value);
		if (isNaN(totalCash))
		{
			totalCash = 0;
		}
		
		var voucherAmt = parseFloat($('closeTill.VoucherAmtEntered').value);
		if (isNaN(voucherAmt))
		{
			voucherAmt = 0;
		}
		
		var cardAmt = parseFloat($('closeTill.CardAmount').value);
		var chequeAmt = parseFloat($('closeTill.ChequeAmount').value);
		var extCardAmt = parseFloat($('closeTill.ExternalCreditCardAmt').value);
				
		var totalAmt = totalCash + voucherAmt + cardAmt + chequeAmt + extCardAmt;
		totalAmt = (new Number(totalAmt)).toFixed(2);
		$('TotalAmount').innerHTML = totalAmt;
		
		var beginningBalance = this.closeTill.getColumnValue('closeTill.BeginningBalance');
		var floatAmt = this.closeTill.getColumnValue('closeTill.FloatAmount');
		
		beginningBalance = (beginningBalance == null) ? 0 : beginningBalance;
		floatAmt = (floatAmt == null) ? 0 : floatAmt;
		
		var cashSales = parseFloat(totalCash - beginningBalance).toFixed(2);
		
		$('CashSales').innerHTML = cashSales;
		$('TotalCashAmount').innerHTML = totalCash;
		
		var transferAmt = parseFloat(totalCash - floatAmt).toFixed(2);
		$('TransferAmount').innerHTML = transferAmt;
		
		$('EndingBalance').innerHTML = floatAmt;
	},	
	
	
	showCloseTillErrors : function()
	{
		$('closeTill.TotalCashAmount').value = 'ERROR';
	}
});

var SubscriptionController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var subscription = new Model(json.subscription);
		
		$super(controller, subscription);
		
	}
});

var EventNotifierController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var eventNotifier = new Model(json.evtNotifier);
		
		$super(controller, eventNotifier);
	}	
});

var ModifierGroupController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var modifierGroup = new Model(json.modifierGroup);
		
		$super(controller, modifierGroup);
	}	
});
