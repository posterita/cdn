var SummaryInfo = Class.create({
	initialize : function(summaryInfo)
	{
		this.summaryInfo = summaryInfo;
	},
	
	init : function()
	{
		for (var field in this.summaryInfo)
		{
			var summaryField = $(field);
			
			if (summaryField != null)
			{
				summaryField.style.display = 'block';
				summaryField.innerHTML = this.summaryInfo[field];
			}
		}
	},
	
	getSummaryInfo : function()
	{
		return this.summaryInfo;
	}
});

var ProductSummary = Class.create(SummaryInfo, {
	
	initialize : function($super, summary)
	{
		$super(summary);
	},
	
	init : function($super)
	{
		$super();
		
		var warehouses = this.summaryInfo['product.summary.allStock'];
		
		var totalQty = 0;
		
		for (var i=0; i<warehouses.length; i++)
		{
			var wh = warehouses[i];
			var name = wh.name;
			var id = wh.id;
			var qty = parseFloat(wh.qty);
			
			totalQty += qty;
		}
		
		$('product.summary.allStock').innerHTML = new Number(totalQty).toFixed(2);
		
		var productPairing = this.summaryInfo['product.summary.productPairing'];
		if (productPairing != null && productPairing.boms.length > 0)
		{
			var boms = productPairing.boms;
			
			var t = '<div class="row"><div class="col-xs-12 col-md-12"><div class="summary-block">Paired Item(s)</div></div></div><div class="row" style="height:90px;overflow:auto;"><table>';
			
			/*var t = '<table>';*/
			for (var i=0; i<boms.length; i++)
			{
				var bom = boms[i];
				var name = bom.name;
				var qty = bom.qty;
				
				t += '<tr><td>' + qty + 'x </td><td>' + name + '</td></tr>';
			}
			
			t+= '</table></div>';
			
			$('product.pairing').innerHTML = t;
		}
		else{
			$('product.pairing').innerHTML = "";
		}
	}
});

var TaxSummary = Class.create({
	
	initialize : function(summaryInfo)
	{
		this.summaryInfo = summaryInfo;
	},
	
	init : function()
	{
		var subTaxes = this.summaryInfo.subTaxes;
		
		var t = '<table>';
		
		for (var i=0; i<subTaxes.length; i++)
		{
			var subTax = subTaxes[i];
			var name = subTax.name;
			var rate = subTax.rate;
			
			t += '<tr><td>' + name + '</td><td>' + rate + '%</td></tr>';
		}
		
		t+= '</table>';
		
		$('sub.taxes').innerHTML = t;
	}
});
