var Callout = Class.create({
	
	initialize : function(methodName, id, prefix)
	{
		this.prefix = prefix;
		var params = new Hash();
		params.set('methodName', methodName);
		params.set('id', id);
		try
		{	
			var myAjax = new Ajax.Request('ModelAction.do?action=callout',
			{
				method: 'POST',
				parameters: params,
				onSuccess: this.displayData.bind(this)				
			});
		}
		catch(e)
		{}
	},
	
	displayData : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		var displayType = json.displayType;
		
		if (displayType == 'text')
		{
			for (var i=0; i<json.list.length; i++)
			{
				var j = json.list[i];
				
				var column = $(this.prefix + '.' + j.columnName);
				if (column != null)
				{
					var value = j.value;
					if (value != null)
						column.value = j.value;
				}
				
				var blockElement = $(this.prefix + "_" + j.columnName);
				if (blockElement != null)
				{
					blockElement.style.display = 'block';
				}
			}
		}
		else if (displayType == 'list')
		{
			var column = $(this.prefix + '.' + json.columnName);
			
			column.innerHTML = '';
			
			for (var i=0; i<json.values.length; i++)
			{
				var j = json.values[i];
				var id = j.regionId;
				var name = j.regionName;
				
				var option = document.createElement('option');
				option.setAttribute('value', id);
				option.innerHTML = name;
				
				column.appendChild(option);
			}
		}
	}
});

var Attributes = {
		globalHash : new Hash(),
		
		store : function(elementId, attribute, value)
		{
			var localHash = Attributes.globalHash.get(elementId);
			localHash = (localHash == null) ? new Hash() : localHash;
			
			localHash.set(attribute, value);
			
			Attributes.globalHash.set(elementId, localHash);
		},
		
		retrieve : function(elementId, attribute)
		{
			var localHash = Attributes.globalHash.get(elementId);
			return localHash.get(attribute);
		},
		
		getAll : function(elementId)
		{
			return Attributes.globalHash.get(elementId);
		}
}; 

var Field = Class.create({
		initialize : function(json)
		{
			this.itemTitle = $('item-title');
			
			if (json != null)
			{
				this.columnName = json.columnName;
				this.field = json.field;
				this.help = null;
				this.value = json.value;
				this.value_bak = this.value;
				this.displayValue = json.displayValue;
				this.displayId = json.displayId; //needed for search box
				this.label = json.label;
				this.isDate = json.isDate;
				this.isSearch = json.isSearch;
				
				this.callout = null;
				this.columnId = json.columnId;
				this.prefix = json.prefix;
				
				this.mandatory = false;
				this.unique = false;
				this.encrypted = false;
				this.password = false;
				this.alphanumeric = false;
				this.error = null;
				this.logicEventListeners = new ArrayList();
				this.isDisplayed = true;
				this.readOnly = false;
				this.textOnly = false;
				this.styleClass = null;
				this.updateable = true;
				this.defaultValue = null;
				this.hasError = false;
				/*this.helpComp = $('help');*/
				this.helpComp = null;
				
				this.textOnlyId = this.columnName + 'Text';
			}
		},
		
		revert : function()
		{
			this.value = this.value_bak;
		},
		
		doCallout : function(text, li)
		{
			$(this.columnName).value = li.getAttribute('id');
			new Callout(this.callout, li.getAttribute('id'), this.prefix);			
		},
		
		showTextOnly : function(show)
		{
			if ($(this.textOnlyId) != null)
			{
				var textStyle = (show) ? 'inline-block' : 'none';
				var displayElementStyle = (show) ? 'none' : 'inline-block';
				
				$(this.textOnlyId).style.display = textStyle;
				$(this.displayId).style.display = displayElementStyle;
			}
		},
		
		reloadPageDisplay : function()
		{
			this.showTextOnly(this.textOnly);
		},
		
		updatePage : function(refreshValuesOnly)
		{
			if (!refreshValuesOnly)
			{
				this.reloadPageDisplay();
			}
			
			this.displayValue = (this.displayValue == null) ? "" : this.displayValue;
			
			if (this.textOnly)
			{
				$(this.columnName).innerHTML = this.displayValue;
			}
			else if (this.isSearch)
			{
				$(this.columnName).value = this.value;
				$(this.displayId).value = this.displayValue;
			}
			else
			{
				if ($(this.displayId) != null)
				{
					var tagName = $(this.displayId).tagName;
					
					if (tagName == 'SELECT')
					{
						var options = $(this.displayId).options;
						for (var i=0; i<options.length; i++)
						{
							var option = options[i];
							var val = option.value;
							if (val == this.value)
							{
								$(this.displayId).selectedIndex = i;
							}
						}
					}
					else if (tagName == 'INPUT')
					{
						if ($(this.displayId).getAttribute('type') == 'checkbox')
						{
							$(this.displayId).checked = this.displayValue;
						}
						else
						{
							$(this.displayId).value = this.displayValue;						
						}
					}
				}
			}
			
			if ($(this.textOnlyId)!=null)
				$(this.textOnlyId).innerHTML = this.displayValue;
		},
		
		initReplace : function()
		{
			Element.replace(this.columnName, this.field);
			
			//Set Item-title
			if (this.columnName.indexOf('.Name') != -1)
			{
				if (this.itemTitle != null)
				{
					this.itemTitle.value = this.displayValue;
				}
			}
			else 
				if (this.columnName.indexOf('.AccountNo') != -1)
				{
					if (this.itemTitle != null)
					{
						this.itemTitle.value = this.displayValue;
					}
				}
			
			if (this.encrypted || this.password)
			{
				var name = $(this.columnName).getAttribute('name');
				var id = $(this.columnName).getAttribute('id');
				var value = $(this.columnName).getAttribute('value');
				
				var encryptedInput = document.createElement('input');
				encryptedInput.setAttribute('type', 'password');
				encryptedInput.setAttribute('name', name);
				encryptedInput.setAttribute('id', id);
				encryptedInput.setAttribute('value', value);
				
				Element.replace(this.columnName, encryptedInput);
			}
			
			var div = document.createElement('div');
			div.setAttribute('class', 'text-only');
			div.setAttribute('id', this.textOnlyId);
			div.innerHTML = (this.displayValue == null) ? "" : this.displayValue;
			div.style.display = 'none';
			
			Element.insert($(this.displayId), {after:div});
			
			$(this.columnName).setAttribute('class', this.styleClass);
			$(this.columnName).removeAttribute('title');
			
			if (this.readOnly || this.textOnly)
			{
				if ($(this.columnName) != null)
				{
					if (this.readOnly)
						$(this.columnName).disabled = 'disabled';
					
					this.showTextOnly(this.textOnly);
					
					if (this.isDate)
						$(this.displayId + 'Btn').style.display = 'none';
				}
			}
			else
			{
				$(this.displayId).onclick = this.onClick.bind(this);
				$(this.displayId).onblur = this.onBlur.bind(this);
				
				if (this.isSearch)
				{
					if (this.callout != null)
					{
						$(this.displayId).Autocompleter=new Ajax.Autocompleter(this.displayId,
								this.columnName + 'QuerySearchResult','ModelAction.do', {
							'paramName': 'Name',
							'parameters': 'action=search&columnId=' + this.columnId,
							'afterUpdateElement': this.doCallout.bind(this),
							'minChars': 0
						});
					}
					else
					{
						$(this.displayId).Autocompleter=new Ajax.Autocompleter(this.displayId,
								this.columnName + 'QuerySearchResult','ModelAction.do', {
										'paramName': 'Name',
										'parameters': 'action=search&columnId='+this.columnId,
										'afterUpdateElement': this.search.bind(this),
										'minChars': 0
						});
					}
				}
			}
			
			if (this.isDate)
			{
				Calendar.setup({
					inputField: this.displayId, 
					/*ifFormat : '%Y-%m-%d',*/
					showTime : false, 
					button : this.displayId + 'Btn'
				});
			}
		},
		
		setTextOnly : function()
		{
			if ($(this.textOnlyId) != null)
			{
				$(this.textOnlyId).innerHTML = (this.displayValue == null) ? "" : this.displayValue;
				this.showTextOnly(true);
			}
		},
		
		search : function(text, li){
			$(this.columnName).value = li.getAttribute('id');
		},
	
		init : function()
		{
			this.isDisplayed =  ($(this.columnName) != null);
				
			if (this.isDisplayed)
			{
				try
				{
					var info = eval('(' + $(this.columnName).innerHTML + ')');
					if (info != null)
					{
						this.unique = info.unique;
						this.mandatory = info.mandatory;
						this.encrypted = info.encrypted;
						this.password = info.password;
						this.alphanumeric = info.alphanumeric;
						this.readOnly = info.readOnly;
						this.textOnly = info.textOnly;
						this.styleClass = info.styleClass;
						this.help = info.help;
						this.defaultValue = info.defaultValue;
						this.callout = info.callout;
						
						if (info.updateable != null)
							this.updateable = info.updateable;
					}
				}
				catch (e){}
				
				this.initReplace();
			}
			
			if (this.disabled)
				this.disable();
			else
				this.enable();
			
			if ($(this.displayId) != null)
			{
				$(this.displayId).onchange = this.updateDisplayValue.bind(this);
			}
			
			/*this.helpComp.onmouseover = this.showHelp.bind(this);*/
			
			if (this.helpComp != null)
			{
				this.helpComp.title = this.help;
				jQuery(this.helpComp).tipTip();
			}
			
			if ($(this.displayId) != null)
			{
				var tagName = $(this.displayId).tagName;
				
				if (tagName == 'SELECT')
				{
					var options = $(this.displayId).options;
					
					for(var i=0; i<options.length; i++)
					{
						if (options[i].value == this.value)
						{
							options[i].selected = 'selected';
						}
					}
				}
			}
		},
		
		updateDisplayValue : function()
		{
			if ($(this.displayId) != null)
			{
				var tagName = $(this.displayId).tagName;
				
				if (tagName == 'SELECT')
				{
					var selectedIndex = $(this.displayId).selectedIndex;
					var option = $(this.displayId).options[selectedIndex];
					if (option != null)
					{
						var label = option.label;
						this.value = $(this.displayId).value;
						this.displayValue = label;
					}
					
					if (this.callout != null)
					{
						new Callout(this.callout, $(this.displayId).value , this.prefix);			
					}
				}
				else if (tagName == 'INPUT')
				{
					var type = $(this.displayId).type
					if (type == 'checkbox')
					{
						this.value = $(this.displayId).checked;
					}
					else
						this.value = $(this.displayId).value;
					
					this.displayValue = this.value;
				}
				else if (tagName == 'DIV')
				{
					this.value = $(this.displayId).innerHTML;
					this.displayValue = this.value;
				}
			}
		},
		
		initDynUpdate : function()
		{
			if ($(this.columnName) != null)
			{
				Element.replace(this.columnName, this.field);
				$(this.columnName).setAttribute('class', this.styleClass);
				$(this.columnName).removeAttribute('title');
				
				if (this.readOnly || this.textOnly)
				{
						if (this.readOnly)
							$(this.columnName).disabled = 'disabled';
						
						if (this.textOnly)
							Element.replace(this.columnName, this.value);
				}
				else
				{
					$(this.displayId).onclick = this.onClick.bind(this);
				}
			}
		},
		
		setHelpComp : function(id)
		{
			if ($(id) != null)
				this.helpComp = $(id);
		},
		
		isUpdateable : function()
		{
			return this.updateable;
		},
		
		disable : function()
		{
			var displayEl = $(this.displayId);
			if (displayEl != null)
				displayEl.setAttribute('disabled', 'disabled');
		},
		
		enable : function()
		{
			var displayEl = $(this.displayId);
			if (displayEl != null && !this.readOnly)
				displayEl.removeAttribute('disabled');
		},
		
		addLogicEventListener : function(listener)
		{
			this.logicEventListeners.add(listener);
		},
		
		onClick : function()
		{
			/*this.showHelp();*/
			this.fireLogicEvent();			
		},
		
		onBlur : function()
		{
			/*var keypad = document.getElementById('keypad-popup-panel');
			if (keypad.style.display == 'block')
			{
				keypad.style.display = 'none';
			}*/
		},
		
		fireLogicEvent : function()
		{
			for (var i=0; i<this.logicEventListeners.size(); i++)
			{
				var listener = this.logicEventListeners.get(i);
				listener.logicEvent(this.getColumnName());
			}
		},
		
		setError : function(error)
		{
			this.hasError = true;
			this.error = error;
			$(this.displayId).setAttribute('style', 'border:2px solid #CC0000;');
		},
		
		removeError : function()
		{
			if (this.hasError)
			{
				var comp = $(this.displayId);
			
				if (comp != null)
					comp.removeAttribute('style');
				
				this.hasError = false;
			}
		},
		
		initDisplayError : function()
		{
			$(this.displayId).onclick = this.errorHandle.bind(this);
		},
		
		errorHandle : function()
		{
			this.showError();
			this.fireLogicEvent();
		},
		
		showError : function()
		{
			//this.helpComp.innerHTML = this.error;
		},
		
		showHelp : function()
		{
			this.helpComp.title = this.help;
			jQuery(this.helpComp).tipTip();
		},
	
		getColumnName : function()
		{
			return this.columnName;
		},
		
		getDisplayId : function()
		{
			return this.displayId;
		},
		
		getMandatory : function()
		{
			return this.mandatory;
		},
		
		getPassword : function()
		{
			return this.password;
		},
		
		getUnique : function()
		{
			return this.unique;
		},
		
		getEncrypted : function()
		{
			return this.encrypted;
		},
		
		getAlphanumeric : function()
		{
			return this.alphanumeric;
		},
		
		isDisplayed : function()
		{
			return this.isDisplayed;
		},
		
		setValue : function(value)
		{
			this.value = value;
			var _element = $(this.columnName);
			if (_element != null)
			{
				_element.value = value;
			}
		},
		
		getValue : function()
		{
			var _element = $(this.columnName);
			if (_element != null)
			{
				if (_element.type == 'checkbox')
					this.value = _element.checked;
				else
					this.value = _element.value;
				
				var tagName = _element.tagName;
				
				if (tagName == 'SELECT')
				{
					var selectedIndex = _element.selectedIndex;
					var option = _element.options[selectedIndex];
					if (option != null)
					{
						var label = option.label;
						this.displayValue = label;
					}
					
					if (this.callout != null)
					{
						new Callout(this.callout, _element.value , this.prefix);			
					}
				}
			}
			return this.value;
		},
	
		getField : function()
		{
			return this.field;
		},
		
		getElement : function()
		{
			return $(this.columnName);
		},
	
		getHelp : function()
		{
			return this.help;
		},
	
		equals : function(element)
		{
			return (this.columnName == element.getColumnName());
		},
		
		setUnique : function(unique)
		{
			this.unique = unique;
		},
		
		setMandatory : function(mandatory)
		{
			this.mandatory = mandatory;
		}
	
	});
	
var Model = Class.create({
	
	initialize : function(json)
	{
		if (json != null)
		{
			this.id = json.id;
			this.prefix = json.prefix;
			this.tableId = json.tableId;
			this.className = json.className;
			this.image = json.image;
									
			this.img = $(this.prefix + ".image");
			this.imgLink = $(this.prefix + ".image.link");
			
			this.defaultImageElement = $('object-image');
			
			if (this.imgLink != null)
			{
				this.imgLink.style.display = 'block';
			}
			
			if (this.img != null)
			{
				this.img.style.display = 'block';
			}
			
			if (this.img != null)
			{
				if (this.image != null)
				{
					this.img.src = this.image;
					
					if (this.defaultImageElement != null)
					{
						this.defaultImageElement.className = '';
					}
				}
				else
				{
					this.img.style.display = 'none';
					
					if (this.defaultImageElement != null)
					{
						this.defaultImageElement.className = 'glyphicons ' + this.prefix + '-image object-image no-padding';
					}
				}
			}
			
			if (this.imgLink != null)
			{
				if (this.image != null)
				{
					this.imgLink.href = this.image;
					if (this.defaultImageElement != null)
					{
						this.defaultImageElement.className = '';
					}
				}
				else
				{
					this.imgLink.style.display = 'none';
					
					if (this.defaultImageElement != null)
					{
						this.defaultImageElement.className = 'glyphicons ' + this.prefix + '-image object-image no-padding';
					}
				}
				
			}
			
			if (this.image != null)
			{
				jQuery(document).ready(function(){
					/* THEMES : default, dark_rounded, dark_square, facebook, light_rounded, light_square */
					jQuery("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
				});
			}
			this.fields = new ArrayList(new Field());
			
			this.fieldMap = new Hash();
			for (var i in json)
			{
				var column = json[i];
				var field = new Field(column);
				this.fields.add(field);
				this.fieldMap.set(column.displayId, field);
			}
			
			this.setHelpComp();
		}
	},
	
	reloadImage : function(imageSrc)
	{
		if (this.img != null)
		{
			if (this.image == null)
			{
				this.img.src = imageSrc;
				if (this.defaultImageElement != null)
				{
					this.defaultImageElement.className = 'glyphicons ' + this.prefix + '-image object-image no-padding';
				}
			}
		}
		
		if (this.imgLink != null)
		{
			if (this.image == null)
			{
				this.imgLink.href = imageSrc;
				if (this.defaultImageElement != null)
				{
					this.defaultImageElement.className = 'glyphicons ' + this.prefix + '-image object-image no-padding';
				}
			}
		}
	},
	
	setHelpComp : function()
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			//field.setHelpComp(id);
			var helpId = 'help.' + field.columnName;
			field.setHelpComp(helpId);
		}
	},
	
	getFields : function()
	{
		return this.fields;
	},
	
	getFieldMap : function()
	{
		return this.fieldMap;
	},
	
	init : function()
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.init();
		}
		
		this.defaultImage = this.image;
	},
	
	updatePage : function(refreshValuesOnly)
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.updatePage(refreshValuesOnly);
			
			if (this.id != 0 && !field.isUpdateable())
			{
				field.showTextOnly(true);
			}
		}
	},
	
	disableFields : function()
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.disable();
		}
	},
	
	enableFields : function()
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.enable();
		}
	},

	getColumnValue : function(columnName)
	{
		var field = this.fieldMap.get(columnName);
		return field.getValue();
	},
	
	setColumnValue : function(columnName, value)
	{
		var field = this.fieldMap.get(columnName);
		field.setValue(value);
	},
	
	setUnique : function(columnName, unique)
	{
		var field = this.fieldMap.get(columnName);
		field.setUnique(unique);
	},
	
	setId : function(id)
	{
		this.id = id;
	},
	
	getId : function()
	{
		return this.id;
	},
	
	getTableId : function()
	{
		return this.tableId;
	},
	
	getClassName : function()
	{
		return this.className;
	},
	
	setActive : function(active)
	{
		var isActiveField = this.fieldMap.get(this.prefix + '.IsActive');
		isActiveField.value = active;
	},
	
	isActive : function()
	{
		var isActiveField = this.fieldMap.get(this.prefix + '.IsActive');
		return isActiveField.value;
	}
});

var Controller = Class.create({
	
	initialize : function(controller, root)
	{
		this.className = controller.name;
		this.controllerId = controller.id;
		this.processKey = controller.processKey;
		this.windowNo = controller.windowNo;
		this.root = root;
		this.interactives = new ArrayList(new Model());
		this.informatives = new ArrayList(new Model())
		this.parameters = '';
		this.parametersMap = new Hash();
		this.monitorDisplayLogic = new Hash();
		this.watchFieldMonitors = new Hash();
		
		this.deactivateListeners = new ArrayList();
		this.activateListeners = new ArrayList();
		this.deleteListeners = new ArrayList();
		this.onSaveListeners = new ArrayList();
		
		this.inactiveSticker = $('inactive_sticker');
		this.itemTitle = $('item-title');
		this.tree = controller.tree;
	},
	
	isActive : function()
	{
		return this.root.isActive();
	},
	
	addDeactivateListener : function(listener)
	{
		this.deactivateListeners.add(listener);
	},
	
	addActivateListener : function(listener)
	{
		this.activateListeners.add(listener);
	},
	
	addDeleteListener : function(listener)
	{
		this.deleteListeners.add(listener);
	},
	
	addOnSaveListener : function(listener)
	{
		this.onSaveListeners.add(listener);
	},
	
	fireOnSaveEvent : function()
	{
		for (var i=0; i<this.onSaveListeners.size(); i++)
		{
			var listener = this.onSaveListeners.get(i);
			listener.controllerSaved(this.controllerId);
		}
	},
	
	fireDeleteEvent : function()
	{
		for (var i=0; i<this.deleteListeners.size(); i++)
		{
			var listener = this.deleteListeners.get(i);
			listener.controllerDeleted();
		}
	},
	
	fireDeactivateEvent : function()
	{
		for (var i=0; i<this.deactivateListeners.size(); i++)
		{
			var listener = this.deactivateListeners.get(i);
			listener.controllerDeactivated();
		}
	},
	
	fireActivateEvent : function()
	{
		for (var i=0; i<this.activateListeners.size(); i++)
		{
			var listener = this.activateListeners.get(i);
			listener.controllerActivated();
		}
	},
	
	disable : function()
	{
		this.applyInactiveSticker();
		this.root.setActive(false);
		this.root.disableFields();
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.disableFields();
		}
	},
	
	enable : function()
	{
		this.removeInactiveSticker();
		this.root.setActive(true);
		this.root.enableFields();
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.enableFields();
		}
	},
	
	applyInactiveSticker : function()
	{
		if (this.inactiveSticker != null)
			this.inactiveSticker.style.display = 'inline';
	},
	
	removeInactiveSticker : function()
	{
		if (this.inactiveSticker != null)
			this.inactiveSticker.style.display = 'none';
	},
	
	getClassName : function()
	{
		return this.className;
	},
	
	getControllerId : function()
	{
		return this.controllerId;
	},
	
	getRoot : function()
	{
		return this.root;
	},
	
	setHelpComp : function(id)
	{
		this.root.setHelpComp();
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.setHelpComp();
		}
	},
	
	updatePage : function(refreshValuesOnly)
	{
		this.root.updatePage(refreshValuesOnly);
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.updatePage(refreshValuesOnly);
		}
		
		for (var i=0; i<this.informatives.size(); i++)
		{
			var informative = this.informatives.get(i);
			informative.updatePage(refreshValuesOnly);			
		}
		
		this.displayLogic();
		
		if (this.isActive())
		{
			this.enable();
		}
		else
		{
			this.disable();
		}
		
		//this.variantDisplayLogic();
	},
	
	init : function()
	{
		this.root.init();
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.init();
		}
		
		for (var i=0; i<this.informatives.size(); i++)
		{
			var informative = this.informatives.get(i);
			informative.init();
		}
		
		this.displayLogic();
		
		if (!this.isActive())
		{
			this.disable();
		}
		else
		{
			this.enable();
		}
		
		//this.variantDisplayLogic();
		
		if (this.getControllerId() == 0)
		{
			if (this.itemTitle != null)
				this.itemTitle.value = 'New ' + this.getClassName();
		}
		
		this.initNonUpdateableFields();
		
		this.onInit();
	},
	
	onInit : function()
	{
		
	},
	
	getURL : function(url)
	{
		window.location = url;
	},
	
	showHelp : function()
	{
		var helpPanel = new HelpPanel();
		helpPanel.show();
	},
	
	initNonUpdateableFields : function()
	{
		if (this.controllerId != 0)
		{
			var fields = this.root.getFields();
			
			for (var i=0; i<fields.size(); i++)
			{
				var field = fields.get(i);
				if (!field.isUpdateable())
				{
					field.setTextOnly();
				}
			}
			
			for (var i=0; i<this.interactives.size(); i++)
			{
				var interactive = this.interactives.get(i);
				var fields = interactive.getFields();
				for (var j=0; j<fields.size(); j++)
				{
					var field = fields.get(j);
					if (!field.isUpdateable())
					{
						field.setTextOnly();
					}
				}
			}
		}
	},
	
			
	deleteAction : function()
	{
		if (confirm('Are you sure to delete?'))
		{
			var url = 'ModelAction.do';
			var pars = 'action=delete&controller=' + this.className + '&id=' + this.controllerId;
			
			var ajax = new Ajax.Request(url,
			{ 
						method: 'POST', 
						parameters: pars, 
						onComplete: this.afterDelete.bind(this)
			});	
		}
	},
	
	afterDelete : function(response)
	{
		var json = eval('('+response.responseText + ')');
		var errorMsg = json.errorMsg;
		if (errorMsg != null)
		{
			alert(errorMsg);
		}
		else
		{
			this.fireDeleteEvent();
		}
	},
	
	activate : function()
	{
		var url = 'ModelAction.do';
		var pars = 'action=activate&controller=' + this.className + '&id=' + this.controllerId;
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.afterActivate.bind(this)
		});	
	},
	
	deactivate : function()
	{
		var url = 'ModelAction.do';
		var pars = 'action=deactivate&controller=' + this.className + '&id=' + this.controllerId;
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.afterDeactivate.bind(this)
		});	
	},
	
	afterActivate : function(response)
	{
		
		try
		{
			var json = eval('(' + response.responseText + ')');
			var errorMsg = json.errorMsg;
			if (errorMsg != null)
			{
				alert(errorMsg);
			}
			else
			{
				this.enable();
				this.initNonUpdateableFields();
				this.fireActivateEvent();
			}
		}
		catch(e)
		{
			this.enable();
			this.fireActivateEvent();
		}
	},
	
	afterDeactivate : function(response)
	{
		try
		{
			var json = eval('(' + response.responseText + ')');
			var errorMsg = json.errorMsg;
			if (errorMsg != null)
			{
				alert(errorMsg);
			}
			else
			{
				this.disable();
				this.fireDeactivateEvent();
			}
		}
		catch(e)
		{
			this.disable();
			this.fireDeactivateEvent();
		}
	},
	
	addDependent : function(interactive)
	{
		this.interactives.add(interactive);
	},
	
	addDisplayOnly : function(informative)
	{
		this.informatives.add(informative);
	},
	
	
	addParameters : function(fields)
	{
		for (var i=0; i<fields.size(); i++)
		{
			var field = fields.get(i);
			var columnName = field.getColumnName();
			var value = field.getValue();
			
			var mandatory = field.getMandatory();
			var unique = field.getUnique();
			var password = field.getPassword();
			var encrypted = field.getEncrypted();
			var alphanumeric = field.getAlphanumeric();
			
			this.parameters += '&' + columnName + '=' + value;
			
			this.parameters += '&' + columnName + '.mandatory=' + mandatory;
			
			this.parameters += '&' + columnName +'.unique=' + unique;
			
			this.parameters += '&' + columnName +'.encrypted=' + encrypted;
			
			this.parameters += '&' + columnName +'.password=' + password;
			
			this.parameters += '&' + columnName +'.alphanumeric=' + alphanumeric;
			
			this.parameters += '&' + columnName +'.updateable=' + field.updateable;
				
		}
	},
	
	addParametersMap : function(fields)
	{
		for (var i=0; i<fields.size(); i++)
		{
			var field = fields.get(i);
			var columnName = field.getColumnName();
			var value = field.getValue();
			field.updateDisplayValue();
			
			var mandatory = field.getMandatory();
			var unique = field.getUnique();
			var encrypted = field.getEncrypted();
			var password = field.getPassword();
			var alphanumeric = field.getAlphanumeric();
			
			this.parametersMap.set(columnName, value);
			this.parametersMap.set(columnName +'.mandatory', mandatory);
			this.parametersMap.set(columnName +'.unique', unique);
			this.parametersMap.set(columnName +'.encrypted', encrypted);
			this.parametersMap.set(columnName +'.password', password);
			this.parametersMap.set(columnName +'.updateable', field.updateable);
			this.parametersMap.set(columnName +'.alphanumeric', alphanumeric);
		}
	},
	
	getParametersMap : function()
	{
		this.parametersMap = new Hash();
		
		var fields = this.root.getFields();
		
		this.addParametersMap(fields);
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			this.addParametersMap(fields);
		}
		
		return this.parametersMap;
	},
	
	getParameters : function()
	{
		this.initParameters();
		return this.parameters;
	},
	
	initParameters : function()
	{
		this.parameters = '';
		var fields = this.root.getFields();
		
		this.addParametersMap(fields);
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			this.addParametersMap(fields);
		}
		
		this.parametersMap.set('controllerId', this.controllerId);
	},
	
	beforeSave : function()
	{
		return true;
	},
	
	save : function()
	{
		if (!this.beforeSave())
			return;
		
		this.initParameters();
		
		var url = 'ModelAction.do';
		var pars = 'action=save&controller=' + this.className  + '&windowNo=' + 
					this.windowNo + '&id=' + this.controllerId + '&' + this.parametersMap.toQueryString();
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.showStatus.bind(this)
		});	
	},
	
	showStatusLog : function(log)
	{
		var localLogs = log.local;
		var globalLogs = log.global;
		
		var errorFields = new ArrayList(new Field());
		if (localLogs != null || globalLogs != null)
		{	
			for (var displayId in localLogs)
			{
				var errorMsg = localLogs[displayId];
					
				var field = this.getField(displayId);
				if (field != null)
				{
					errorFields.add(field);
					field.setError(errorMsg);
					field.initDisplayError();
				}
			}
			
			this.clearOtherErrors(errorFields);
			
			/*var errorPopUpPanel = new ErrorPopUpPanel(globalLogs);
			errorPopUpPanel.init();
			*/
			var errorMessage = '';
			for (var key in globalLogs)
			{
				var msg = globalLogs[key];
				errorMessage += key + ' : ' + msg + '<br>';
			}
			
			if (errorMessage != '')
			{
				//$('msg-box').setAttribute('class', 'msg-box-error');
				
				$('success-msg-box').style.display = 'none';
				$('error-msg-box').style.display = 'block';
				 
				//$('msg-box').innerHTML = 'Error';
				$('ErrorMessages').innerHTML = errorMessage;
			}
		}
		else
		{
			this.clearAllErrors();
			//$('msg-box').setAttribute('class', 'msg-box-save');
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = Translation.translate("saved","Saved");
			$('ErrorMessages').innerHTML = ''
			var state = log.state;
			var controllerInfo = eval('(' +state.controllerInfo+')');
			
			if (state != null)
			{
				this.controllerId = state.controllerId;
				this.initNonUpdateableFields();
				//this.setText();
				this.updateState(state);
				this.fireOnSaveEvent();
				this.setItemTitle();
				this.refreshTree();
			}
		}
	},
	
	setText : function()
	{
		var fields = this.root.getFields();
		
		for (var i=0; i<fields.size(); i++)
		{
			var field = fields.get(i);
			field.setTextOnly();
		}
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			for (var j=0; j<fields.size(); j++)
			{
				var field = fields.get(j);
				field.setTextOnly();
			}
		}
	},
	
	updateFields : function()
	{
		
	},
	
	showStatus : function(response)
	{
		var log = eval('(' + response.responseText + ')');
		this.showStatusLog(log);
		this.initHideStatusInfo();
	},
	
	initHideStatusInfo : function()
	{
		jQuery(window).on('click', function(){
			hideStatusInfo();
		});
	},
	
	clearAllErrors : function()
	{
		var rootFields = this.root.getFields();
		for (var i=0; i<rootFields.size(); i++)
		{
			var field = rootFields.get(i);
			field.removeError();
		}
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			
			for (var i=0; i<fields.size(); i++)
			{
				var interactiveField = fields.get(i);
				interactiveField.removeError();
			}
		}
		
		$('error-msg-box').style.display = 'none';
		$('ErrorMessages').innerHTML = ''
	},
	
	clearOtherErrors : function(errorFields)
	{
		if (errorFields == null || errorFields.size() == 0)
		{
			this.clearAllErrors();
			return;
		}
		
		var rootFields = this.root.getFields();
		for (var i=0; i<rootFields.size(); i++)
		{
			var field = rootFields.get(i);

			if (!errorFields.contains(field))
			{
				field.removeError();
			}
		}
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			
			for (var i=0; i<fields.size(); i++)
			{
				var interactiveField = fields.get(i);
				if (!errorFields.contains(interactiveField))
				{
					interactiveField.removeError();
				}
			}
		}
		
	},
	
	getField : function(displayId)
	{
		var mainMap = this.root.getFieldMap();
		var field = mainMap.get(displayId);
		if (field != null)
			return field;
		else
		{
			for (var i=0; i<this.interactives.size(); i++)
			{
				var interactive = this.interactives.get(i);
				var fieldMap = interactive.getFieldMap();
				field = fieldMap.get(displayId);
				if (field != null)
					return field;
			}
		}
		return null;
	},
	
	updateState : function(state)
	{},
	
	displayLogic : function()
	{},
	
	addDisplayLogic : function(monitor, displayLogic)
	{
		this.monitorDisplayLogic.set(monitor, displayLogic);
		
		var watchFields = this.getWatchFields(displayLogic);
		for (var i=0; i<watchFields.size(); i++)
		{
			var watchField = watchFields.get(i);
			watchField.addLogicEventListener(this);
			
			var monitors = this.watchFieldMonitors.get(watchField);
			
			if (monitors == null)
			{
				monitors = new ArrayList();
				this.watchFieldMonitors.set(watchField, monitors);
			}
			
			monitors.add(monitor);
			watchField.fireLogicEvent();
		}
	},
	
	getWatchFields : function(displayLogic)
	{
		var watchFields = new ArrayList();
		var splits = displayLogic.match(/\@.*?\@/g);
		for (var i=0; i<splits.length; i++)
		{
			var split = splits[i];
			
			var watchColumnName = split.substring(1, (split.length-1));
			var watchField = this.getField(watchColumnName);
			if (watchField != null)
			{
				watchFields.add(watchField);
			}
		}
		return watchFields;
	},
	
	logicEvent : function(watchColumnName)
	{
		var watchField = this.getField(watchColumnName);
		if (watchField != null)
		{
			var watchValue = watchField.getValue();
			var monitors = this.watchFieldMonitors.get(watchField);
			if (monitors != null)
			{
				for (var i=0; i<monitors.size(); i++)
				{
					var monitor = monitors.get(i);
					var displayLogic = this.monitorDisplayLogic.get(monitor);
					var watchFields = this.getWatchFields(displayLogic);
					var display = this.evaluateLogic(displayLogic, watchFields);
					var monitorElement = $(monitor);
					if (monitorElement != null)
					{
						var styleDisplay = (display)? 'block' : 'none';
						monitorElement.style.display = styleDisplay;
					}
				}
			}
		}
	},
	
	evaluateLogic : function(displayLogic, watchFields)
	{
		displayLogic = displayLogic.replace(/\@/g, '');
		for (var i=0; i<watchFields.size(); i++)
		{
			var watchField = watchFields.get(i);
			var columnName = watchField.getColumnName();
			var value = watchField.getValue();
			
			if (value == null || value == '')
				return false;
			
			var regExp = new RegExp(columnName, 'g');
			displayLogic = displayLogic.replace(regExp, "'" + value + "'");
		}
		
		return eval('('+displayLogic+')');
	},
	
	getProcessKey : function()
	{
		return this.processKey;
	},
	
	getId : function()
	{
		return this.controllerId;
	},
	
	refreshTree : function()
	{
		var tree = parent.document.getElementById('tree');
		
		if (tree == null)
		{
			return;
		}
		
		var treeManager = new TreeManager(tree, this.tree, this.controllerId, this.className);
		treeManager.refreshTree();
		
	},
	
	setItemTitle: function()
	{
		var flds = new Array();
		flds =this.root.getFields();
		
		for(var i=0; i<flds.size(); i++)
		{
			var f = new Field(flds.get(i));
			
			if (f.columnName != null)
			{
				if (f.columnName.indexOf('.Name') != -1)
				{
					if (this.itemTitle != null)
					{
						this.itemTitle.value = f.displayValue;
					}
				}
				else 
					if (this.getClassName() == 'BankAccount')
					{
						if (f.columnName.indexOf('.AccountNo') != -1)
						{
							if (this.itemTitle != null)
							{
								this.itemTitle.value = f.displayValue;
							}
						}
					}
			}
		}
	},
	
	getItemTitle : function()
	{
		var flds = new Array();
		flds =this.root.getFields();
		
		for(var i=0; i<flds.size(); i++)
		{
			var f = new Field(flds.get(i));
			var itemTitle = '';
			
			if (f.columnName != null)
			{
				if (f.columnName.indexOf('.Name') != -1)
				{
					itemTitle =  f.displayValue;
					break;
				}
				else 
					if (this.getClassName() == 'BankAccount')
					{
						if (f.columnName.indexOf('.AccountNo') != -1)
						{
							itemTitle = f.displayValue;
							break;
						}
					}
			}
		}
		
		return itemTitle;
	}
	
});


var ControllerFactory = Class.create({
	initialize : function()
	{
		this.className = null;
		this.controllerJSON = null;
		this.summaryJSON = null;
		this.loadListener = null;
	},
	
	setLoadListener : function(listener)
	{
		this.loadListener = listener;
	},
	
	load : function(className, id)
	{
		if (className == null)
			return;
		else
			this.className = className;
		
		var url = 'ModelAction.do';
		var pars = 'action=loadPanel&controller=' + this.className + '&id=' + id;
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'GET', 
			parameters: pars, 
			onComplete: this.afterLoad.bind(this)
		});				
	},
	
	afterLoad : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		
		this.controllerJSON = json;
		this.summaryJSON = json.summaryInfo;
		
		var controller = this.getController();
		var summary = this.getSummary();
		
		if (this.loadListener != null)
		{
			this.loadListener.controllerLoaded(controller, summary);
		}
	},
	
	getController : function()
	{
		if (this.className == 'Product')
			return new ProductController(this.controllerJSON);
		if (this.className == 'ProductTemplate')
			return new ProductTemplateController(this.controllerJSON);
		else if (this.className == 'Customer')
			return new PartnerController(this.controllerJSON);
		else if (this.className == 'Vendor')
			return new VendorController(this.controllerJSON);
		else if (this.className == 'PriceList')
			return new PriceListController(this.controllerJSON);
		else if (this.className == 'Warehouse')
			return new WarehouseController(this.controllerJSON);
		else if (this.className == 'EventNotifier')
			return new EventNotifierController(this.controllerJSON);
		else if (this.className == 'PaymentTerm')
			return new PaymentTermController(this.controllerJSON);
		else if (this.className == 'Organisation')
			return new OrgController(this.controllerJSON);
		else if (this.className == 'Terminal')
			return new TerminalController(this.controllerJSON);
		else if (this.className == 'TaxCategory')
			return new TaxCategoryController(this.controllerJSON);
		else if (this.className == 'Tax')
			return new TaxController(this.controllerJSON);
		else if (this.className == 'Bank')
			return new BankController(this.controllerJSON);
		else if (this.className == 'BankAccount')
			return new BankAccountController(this.controllerJSON);
		else if (this.className == 'PaymentProcessor')
			return new PaymentProcessorController(this.controllerJSON);
		else if (this.className == 'Role')
			return new RoleController(this.controllerJSON);
		else if (this.className == 'User')
			return new UserController(this.controllerJSON);
		else if (this.className == 'ModifierGroup')
			return new ModifierGroupController(this.controllerJSON);
	},
	
	getSummary : function()
	{
		if (this.className == 'Product' || this.className == 'ProductTemplate')
			return  new ProductSummary(this.summaryJSON);
		else if (this.className == 'Tax')
			return new TaxSummary(this.summaryJSON);
		else
			return new SummaryInfo(this.summaryJSON);
	}
});

function hideStatusInfo()
{
	if ($('success-msg-box') != null)
    {
		$('success-msg-box').style.display = 'none';
    }
    
    if ($('error-msg-box') != null)
    {
    	$('error-msg-box').onblur = function(e){
    		$('error-msg-box').style.display = 'none';
    	}
    }
}
