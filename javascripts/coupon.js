//coupon
var redeemCouponDlg = null;

function redeemCoupon(){
	
	if( redeemCouponDlg == null ){
		
		var dlg = jQuery("<div/>",{title:Translation.translate("redeem.coupon","Redeem Coupon"),class:"row gift-card-popup"});
		var couponNo = jQuery("<input>",{type:"text", id:"coupon-no", class:"numeric textField pull-right"});
		var redeemBtn = jQuery("<input>",{type:"button", value:Translation.translate("Redeem","Redeem"), class:"pull-right"});
		
		redeemBtn.on("click", function(){
		    var number = couponNo.val();
		    
		    if(number == null || number == ""){
		    	alert(Translation.translate("invalid.coupon.number","Invalid Coupon Number!"));
		    }    			
			else 
			{
				jQuery.post("CouponAction.do",
						  {
						    action : "redeemCoupon",
						    couponNo : number,
						    date : DateUtil.getCurrentDate()
						  },
						  function(data,status,xhr){
							  
							jQuery(redeemCouponDlg).dialog("close"); 
							 
						    if(status != "success"){
						    	alert(Translation.translate("failed.to.redeem.coupon","Failed to redeem coupon!"));
						    }
						    else
						    {
						    	jQuery('#shopping-cart-container').html(data);	    	
						    }					     
						    
						  });
	    	}//else
		});

		var row = jQuery("<div/>", {class:"row"});
		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
		col1.append(jQuery("<label>" +Translation.translate("coupon.no","Coupon No")+ ":</label>"));
		col2.append(couponNo)
		row.append(col1);
		row.append(col2);
		dlg.append(row);
		
		/*var row = jQuery("<div/>", {class:"row"});
		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
		col1.append(jQuery("<label>Card CVV</label>"));
		col2.append(cardCvv)
		row.append(col1);
		row.append(col2);
		giftDlg.append(row);*/
		
		
		var row = jQuery("<div/>", {class:"row"});
		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
		col2.append(redeemBtn)
		row.append(col1);
		row.append(col2);
		dlg.append(row);
		    
		redeemCouponDlg = dlg;		
		
	}

	jQuery(redeemCouponDlg).dialog({
		position: ['center',40],
		modal:true,
		open:function(){
			jQuery('#coupon-no').val('');
		},
		close:function(){
			Keypad.hide();
		}
	});
	
}