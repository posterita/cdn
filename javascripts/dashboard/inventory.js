jQuery(document).ready(function() {
	//hide the summary part 
	jQuery(".summary-style").hide();
	
	jQuery("#dateRange").val(dateRange);
	jQuery("#orgId").val(orgId);
	jQuery("#salesPriceListId").val(salesPriceListId);
	jQuery("#purchasePriceListId").val(purchasePriceListId);

	 //Header display store name and period 
    jQuery("#header_org_name").html(jQuery("#orgId option:selected" ).text());
    
    if (fromDateHeader == toDateHeader){
    	jQuery("#header_date").html("<span class='header-unimportant'>as at</span> <span class='date'>" + toDateHeader + "</span>");
    }
    else{
    	jQuery("#header_date").html("<span class='header-unimportant'>from</span> <span class='date' style='font-weight:00'>" + fromDateHeader + "</span> <span class='header-unimportant'>to</span> <span class='date' style='font-weight:00'>" + toDateHeader + "</span>");
    }
    
    
    //dropdown formatting
	jQuery("#manage").select2();

	var urlmenu = document.getElementById('manage');
	urlmenu.onchange = function() 
	{
       window.location.href = jQuery("#manage option:selected").val();
	}

	jQuery("#dateRange").select2();
	jQuery("#orgId").select2();
	jQuery("#select-sales").select2();
	jQuery("#select-purchases").select2();

	//adding values in sales pricelist dropdown and purchase pricelist dropdown
	jQuery('#orgId').on('change', function(e) 
	{
       var selectedStoreId = this.value;

       jQuery('#salesPriceListId option').each(function() 
	   {

	       jQuery('#salesPriceListId').val('');

	       var orgId = new Number(this.getAttribute('org_id'));

	       if (orgId != 0 && orgId != selectedStoreId) 
	       {
	           this.style.display = 'none';
	       } 
	       else 
	       {
	           this.style.display = 'block';
	       }

	       jQuery('#salesPriceListId').val(jQuery('#salesPriceListId option:visible').first().val());

	   });

	   jQuery('#purchasePriceListId option').each(function() 
	   {
	       jQuery('#purchasePriceListId').val('');

	       var orgId = new Number(this.getAttribute('org_id'));

	       if (orgId != 0 && orgId != selectedStoreId) 
	       {
	           this.style.display = 'none';
	       } 
	       else 
	       {
	           this.style.display = 'block';
	       }

	       jQuery('#purchasePriceListId').val(jQuery('#purchasePriceListId option:visible').first().val());
	       });
	   }); 
	
	jQuery(".date").each(function(i, element) {
        var date = element.innerHTML;
        element.innerHTML = moment(date, 'YYYY-MM-DD').format("YYYY-MM-DD");
    });
	
	//help description codes
    var helpText = [
		{ "helpId":"totalInventoryQty", "helpMessage":"1" },
		{ "helpId":"inventoryValuePurchasePrice", "helpMessage":"2" },
		{ "helpId":"inventoryValueSalesPrice", "helpMessage":"3" }, 
		{ "helpId":"inventoryTurnoverRate", "helpMessage":"4" },
	    { "helpId":"inventoryLevel6MonthsHelp", "helpMessage":"5" },
	    { "helpId":"lastInventoryCountHelp", "helpMessage":"6" },
	    { "helpId":"top10PrimaryGroupHelp", "helpMessage":"7" }, 
	    { "helpId":"last5PurchaseOrdersHelp", "helpMessage":"8" },
	    { "helpId":"5MovingItemsHelp", "helpMessage":"9" }
	];
    
    for(i = 0; i < helpText.length; i++) {
    	jQuery('#'+ helpText[i].helpId +' span.msg').html(helpText[i].helpMessage);
    };
});