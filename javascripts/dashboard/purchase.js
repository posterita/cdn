jQuery(document).ready(function() {

    jQuery("#dateRange").val('<%=dateRange%>');
    jQuery("#orgId").val('<%=ad_org_id %>');
    jQuery("#purchasePriceListId").val('<%=purchasePriceListId%>');

    jQuery("#manage").select2();

    var urlmenu = document.getElementById('manage');
    urlmenu.onchange = function() {
        window.location.href = jQuery("#manage option:selected").val();
    }

    jQuery("#dateRange").select2();
    jQuery("#orgId").select2();
    jQuery("#select-purchases").select2();

    jQuery('#orgId').on('change', function(e) {
        var selectedStoreId = this.value;

        jQuery('#purchasePriceListId option').each(function() {
            jQuery('#purchasePriceListId').val('');

            var orgId = new Number(this.getAttribute('org_id'));

            if (orgId != 0 && orgId != selectedStoreId) {
                this.style.display = 'none';
            } else {
                this.style.display = 'block';
            }

            jQuery('#purchasePriceListId').val(jQuery('#purchasePriceListId option:visible').first().val());
        });
    });
});