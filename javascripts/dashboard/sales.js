jQuery(document).ready(function() {

    jQuery("#dateRange").val('<%=dateRange%>');
    jQuery("#orgId").val('<%=ad_org_id%>');

    jQuery("#manage").select2();

    var urlmenu = document.getElementById('manage');
    urlmenu.onchange = function() {
        window.location.href = jQuery("#manage option:selected").val();
    }

    jQuery("#dateRange").select2();
    jQuery("#orgId").select2();
});