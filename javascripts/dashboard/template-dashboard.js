jQuery.noConflict();
var currentPage = null;

//for image popup
jQuery(document).ready(function(){
	/* THEMES : default, dark_rounded, dark_square, facebook, light_rounded, light_square */
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
});

jQuery(document).ready(function() {
	jQuery('.popup').css({display:'none'});

	jQuery( window ).scroll(function() {
		var width = jQuery("#main").width();
		jQuery("#top-nav").width(width);
	});	
	
	//hidden rows
    jQuery(".unavailable").hide();
    jQuery(".activity-style").hide();
    jQuery(".report-style").hide();

    //for links selection on the dropdown Manage
    function getURL(url) {
        window.location = url;
    }

    //date formatting
    jQuery(".moment-ago").each(function(i, element) {
        var date = element.innerHTML;
        element.innerHTML = moment(date, 'YYYY-MM-DD hh:mm:ss').fromNow();
    });

    jQuery(".date").each(function(i, element) {
        var date = element.innerHTML;
        element.innerHTML = moment(date, 'YYYY-MM-DD').format("YYYY-MM-DD");
    });

    jQuery(".format_date").each(function(i, element) {
        var date = element.innerHTML;
        element.innerHTML = moment(date).format('DD MMM YYYY');
    });

    jQuery(".format_date_count").each(function(i, element) {
        var date = element.innerHTML;
        element.innerHTML = moment(date).format('DD/MM/YYYY');
    });

    //for calendar - date range
    var dateFrom = jQuery('#dateFrom');
    var dateTo = jQuery('#dateTo');

    if (dateFrom.val() == null || dateFrom.val() == 'null' || dateFrom.val() == '') {
       dateFrom.val(moment().startOf('month').format('YYYY-MM-DD')); //start of month
       //dateFrom.val(moment().format('YYYY-MM-DD'));
    }

    if (dateTo.val() == null || dateTo.val() == 'null' || dateTo.val() == '') {
        //dateTo.val(moment().endOf('month').format('YYYY-MM-DD'));
        dateTo.val(moment().format('YYYY-MM-DD')); //today date
    }

    var select = jQuery('#dateRange');
    select.on("change", function(e) {

        switch (this.selectedIndex) {
            case 0:
                dateFrom.val(moment().format('YYYY-MM-DD'));
                dateTo.val(moment().format('YYYY-MM-DD'));
                break;
            case 1:
                dateFrom.val(moment().subtract('days', 1).format('YYYY-MM-DD'));
                dateTo.val(moment().subtract('days', 1).format('YYYY-MM-DD'));
                break;
            case 2:
                dateFrom.val(moment().subtract('days', 6).format('YYYY-MM-DD'));
                dateTo.val(moment().format('YYYY-MM-DD'));
                break;
            case 3:
                dateFrom.val(moment().subtract('days', 29).format('YYYY-MM-DD'));
                dateTo.val(moment().format('YYYY-MM-DD'));
                break;
            case 4:
                dateFrom.val(moment().startOf('month').format('YYYY-MM-DD'));
                //dateTo.val(moment().endOf('month').format('YYYY-MM-DD'));
                dateTo.val(moment().format('YYYY-MM-DD'));
                break;
            case 5:
                dateFrom.val(moment().subtract('month', 1).startOf('month').format('YYYY-MM-DD'));
                dateTo.val(moment().subtract('month', 1).endOf('month').format('YYYY-MM-DD'));
                break;
            case 6:
                var dlg = jQuery('<div title="'+Translation.translate("select.date.range")+'">'+Translation.translate("from")+' <input id="date-from-selector" type="text"> <br> '+Translation.translate("to")+' <input id="date-to-selector" type="text"></div>');
                var inputs = dlg.find("input");

                /* jQuery.each(inputs, function(i, val){
							jQuery(val).datepicker({
								defaultDate: "+1w",
								changeMonth: true,
								numberOfMonths: 3
							});
							
						});  */

                jQuery(dlg).dialog({
                    modal: true,
                    open: function() {
                        var inputs = jQuery(dlg).find("input[type|='text']");
                        inputs.datepicker({
                            format: 'yyyy-mm-dd',
                            endDate: '+0d',
                            autoclose: true
                        });

                        jQuery(inputs[0]).datepicker("show");

                        jQuery.each(inputs, function(i, val) {
                            jQuery(inputs[i]).on('hide', function(e) {
                                e.preventDefault();
                            });
                        });
                    },
                    buttons: [{
                        text: "Ok",
                        id: "ok-btn",
                        click: function(e) {
                            var inputs = jQuery(dlg).find("input[type|='text']");

                            dateFrom.val(jQuery(inputs[0]).val());
                            dateTo.val(jQuery(inputs[1]).val());
                            jQuery(dlg).dialog('close');
                        }
                    }, {
                        text: "Cancel",
                    	id: "cancel-btn",
                        click: function(e) {
                            var inputs = jQuery(dlg).find("input[type|='text']");
                            jQuery(inputs[0]).datepicker("close");

                            jQuery(dlg).dialog('close');
                        }
                    }]
                }).show();
                
                jQuery("#ok-btn").html(Translation.translate("ok"));
                jQuery("#cancel-btn").html(Translation.translate("cancel"));
                break;
            default: break;
        }
    });

	// COLLAPSE LEFT NAV
    jQuery('body').toggleClass("minified");
    
    //for refine button options
    jQuery("#refine").click(function() {
    	jQuery("#header_options").toggle();
    });
    
    
});