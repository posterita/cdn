var DataTable = {};

DataTable.utils = {};

DataTable.utils.mapModelToForm = function(model, form){
	/* get inputs */
	var inputs = jQuery(form).find("input");
	for(var i=0; i<inputs.length; i++){
		var input = inputs[i];
		var name = input.name;
		var type = input.type;
		
		if(type == "radio" || type == "checkbox"){
			var value = input.value;
			if(value == model[name]){
				input.checked = true;
			}
			else{
				input.checked = false;
			}
			
			continue;
		}
		
		if(model[name] === undefined){
			jQuery(input).val("");
		}
		else{			
			jQuery(input).val(model[name]);
		}			
	}
	
	/* get selects */
	var selects = jQuery(form).find("select");
	for(var i=0; i<selects.length; i++){
		var select = selects[i];
		var name = select.name;
		
		if(model[name] === undefined){
			jQuery(select).val("");
		}
		else{			
			jQuery(select).val(model[name]);
		}
	}
	
	/* get textareas */
	var textareas = jQuery(form).find("textarea");
	for(var i=0; i<textareas.length; i++){
		var textarea = textareas[i];
		var name = textarea.name;
		
		if(model[name]){
			jQuery(textarea).val(model[name]);
		}
		else{
			jQuery(textarea).val("");
		}
	}
};

DataTable.utils.mapFormToModel = function(form, model){
	var s = jQuery(form).serialize();
	form = jQuery(form)[0];
	
	var params = s.split("&");
	for(var i=0; i<params.length; i++){
		var keyvalue = params[i].split("=");
		var key = keyvalue[0];
		var value = decodeURIComponent(keyvalue[1]);
		value = value.replace(/\+/g, ' ');
		
		var input = form[key];
		if(input){
			if("numeric" == jQuery(input).attr("data-type")){
				value = parseFloat(value);
			}
		}
		
		model[key] = value;
	}
};

DataTable.utils.populateSelect = function(select, jsonArray, label, value){

	var select = jQuery(select);
	select.append(jQuery("<option/>",{value:"", html:""}));
	
	for(var i=0;i<jsonArray.length;i++)
	{
		var json = jsonArray[i];
		select.append(jQuery("<option/>",{value:json[value], html:json[label]}));
	}
};
