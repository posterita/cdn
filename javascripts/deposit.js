// deposit
    var selectDepositTransactionDlg = null;
    var issueDepositDlg = null;
    var redeemDepositDlg = null;
    var refundDepositDlg = null;
    
/* select issue deposit, redeem or refund*/ 
    
    function selectDepositTransaction() 
    {
    	if(selectDepositTransactionDlg == null)
    	{
    		/*initialize*/
    		var selectTransactiondlg = jQuery("<div/>",{title:Translation.translate('deposit.transactions', 'Deposit Transactions'), class:"deposit-popup-transactions"});
    		var issueDepositBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.issue.deposit" , "Issue Deposit"), class:"", id:"deposit-transactions-dialog-issue-button"});
    		var redeemDepositBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.redeem.deposit", "Redeem Deposit"), class:"", id:"deposit-transactions-dialog-redeem-button"});
    		var refundDepositBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.refund.deposit", "Refund Deposit"), class:"", id:"deposit-transactions-dialog-refund-button"});
    		
    		issueDepositBtn.on("click", function(){
    			jQuery(selectDepositTransactionDlg).dialog("close");
    			issueDeposit();
    		});
    		    		
    		redeemDepositBtn.on("click", function(){
    			jQuery(selectDepositTransactionDlg).dialog("close");
    			redeemDeposit();
    		});
    		
    		refundDepositBtn.on("click", function(){
    			jQuery(selectDepositTransactionDlg).dialog("close");
    			refundDeposit();
    		});
    		
    		var row = jQuery("<div/>", {class:"row"});
    		row.append(issueDepositBtn);
    		selectTransactiondlg.append(row);    		
    		    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(redeemDepositBtn);
    		selectTransactiondlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(refundDepositBtn);
    		selectTransactiondlg.append(row);
    		
    		selectDepositTransactionDlg = selectTransactiondlg;
    	}
    	
    	jQuery(selectDepositTransactionDlg).dialog({modal:true, position: ['center',40]});
    	
    }
    
  /*issue deposit*/
    
    function issueDeposit()
    {
    	if(issueDepositDlg == null)
    	{
    		/*initialize*/
    		var issueDlg = jQuery("<div/>",{title:Translation.translate("issue.deposit","Issue Deposit"), class:"deposit-popup"});
    		var amount = jQuery("<input>",{type:"text",class:"textField pull-right", id:"issue-deposit-dialog-amount-textfield"});
    		var cancelBtn = jQuery("<input>",{type:"button", value:Translation.translate("cancel","Cancel"),class:"textField pull-right"});
    		var okBtn = jQuery("<input>",{type:"button", value:Translation.translate("issue","Issue"), class:"pull-right", id:"issue-deposit-dialog-issue-button"});
    		
    		okBtn.on("click", function(){
    			var amt = amount.val();
    			
    			amt = parseFloat(amt);
    			
    			if(amt == null || amt == ""){
    				 alert(Translation.translate("invalid.amount","Invalid Amount"));
    				 amount.focus();
    			}
    			 else
    				 if(isNaN(amt) ||(amt < 0.0)){
    					 alert(Translation.translate("invalid.amount","Invalid Amount"));
    					 amount.focus();
    				 }
    			else {
    				
				jQuery.post("DepositAction.do",
					  {
					    action : "issueDeposit",
					    amount : amt
					  },
					  function(data,status,xhr){
						  
						jQuery(issueDepositDlg).dialog("close"); 
						 
					    if(status != "success"){
					    	alert(Translation.translate("failed.to.issue.deposit","Failed to issue deposit!"));
					    }
					    else
					    {
					    	jQuery('#shopping-cart-container').html(data);	    	
					    }					     
					    
					  });
    			}
    		});
    		    		   		   		
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("amt","Amount")+ ":</label>"));
    		col2.append(amount)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		issueDlg.append(jQuery("<br>"));
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(okBtn)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		issueDepositDlg = issueDlg;
    	}
    	jQuery(issueDepositDlg).dialog({
    		modal:true, 
    		position: ['center',40],
    		open:function(){
	    		var inputs = jQuery(issueDepositDlg).find("input[type|='text']");
	    		for(var i=0; i<inputs.length; i++){
	    			Event.observe(inputs[i],'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	    			jQuery(inputs[i]).val("");
	    		}
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	});
    }
             
    
    /*Redeem deposit*/

    function redeemDeposit(){
    	
    	if( redeemDepositDlg == null ){
    		
    		var dlg = jQuery("<div/>",{title:Translation.translate("redeem.deposit","Redeem Deposit"),class:"row deposit-popup"});
    		var depositNo = jQuery("<input>",{type:"text", id:"redeem-deposit-dialog-deposit-no-textfield", class:"numeric textField pull-right"});
    		var redeemBtn = jQuery("<input>",{type:"button", id:"redeem-deposit-dialog-redeem-button", value:Translation.translate("Redeem","Redeem"), class:"pull-right"});
    		
    		redeemBtn.on("click", function(){
    		    var number = depositNo.val();
    		    
    		    if(number == null || number == ""){
    		    	alert(Translation.translate("invalid.deposit.number","Invalid Deposit Number!"));
    		    }    			
    			else 
    			{
    				jQuery.post("DepositAction.do",
    						  {
    						    action : "redeemDeposit",
    						    depositno : number
    						  },
    						  function(data,status,xhr){
    							  
    							jQuery(redeemDepositDlg).dialog("close"); 
    							 
    						    if(status != "success"){
    						    	alert(Translation.translate("failed.to.redeem.deposit","Failed to redeem deposit!"));
    						    }
    						    else
    						    {
    						    	jQuery('#shopping-cart-container').html(data);	    	
    						    }					     
    						    
    						  });
    	    	}//else
    		});

    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("deposit.no","Deposit No")+ ":</label>"));
    		col2.append(depositNo)
    		row.append(col1);
    		row.append(col2);
    		dlg.append(row);
    		
    		dlg.append(jQuery("<br>"));
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(redeemBtn)
    		row.append(col1);
    		row.append(col2);
    		dlg.append(row);
    		    
    		redeemDepositDlg = dlg;		
    		
    	}

    	jQuery(redeemDepositDlg).dialog({
    		position: ['center',40],
    		modal:true,
    		open:function(){
    			jQuery('#deposit-no').val('');
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	});
    	
    }
    
    function refundDeposit()
    {
    	if(refundDepositDlg == null)
    	{
    		/*initialize*/
    		
    		var dialog = jQuery("<div/>",{title:Translation.translate("refund.deposit","Refund Deposit"),class:"row deposit-popup"});
    		var depositNo = jQuery("<input>",{type:"text", id:"refund-deposit-dialog-deposit-no-textfield", class:"numeric textField pull-right"});
    		var refundBtn = jQuery("<input>",{type:"button", id:"refund-deposit-dialog-refund-button", value:Translation.translate("refund","Refund"), class:"pull-right"});
    		
    		/*Event.observe(this.depositNo,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
    		
    		refundBtn.on("click", function() {
    		    var number = depositNo.val();

    		    if (number == null || number == "") {
    		        alert(Translation.translate("invalid.deposit.number", "Invalid deposit number!"));
    		    } else {

    		        //jQuery(this).val("Checking ...");   

    		        jQuery.post("DepositAction.do", {
    		                "action" : "refundDeposit",
    		                "depositno" : number
    		            },
    		            function(data, status, xhr) {

    		                jQuery(refundDepositDlg).dialog("close");

    		                if (status != "success") {
    		                    alert(Translation.translate("failed.to.refund.deposit", "Failed to refund deposit!"));
    		                } else {
    		                    jQuery('#shopping-cart-container').html(data);
    		                }

    		            });

    		    }
    		});

    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("deposit.no","Deposit No")+ ":</label>"));
    		col2.append(depositNo)
    		row.append(col1);
    		row.append(col2);
    		dialog.append(row);
    		
    		    		
    		dialog.append(jQuery("<br>"));
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(refundBtn)
    		row.append(col1);
    		row.append(col2);
    		dialog.append(row);
    		    
    		refundDepositDlg = dialog;
    	}
    	
    	jQuery(refundDepositDlg).dialog({
    		position: ['center',40],
    		modal:true,
    		open:function(){
	    		var inputs = jQuery(refundDepositDlg).find("input[type|='text']");
	    		for(var i=0; i<inputs.length; i++){
	    			jQuery(inputs[i]).val("");
	    			Event.observe(inputs[i],'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	    		};
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	});
    	
    }
    