/**
 * Small pricing model to calculate prices based on priceList, 
 * priceStd, priceLimit and priceEntered
 */
 var PPricing = Class.create({
	 initialize: function(priceEntered, priceStd, priceList, priceLimit, qty, taxRate, isTaxIncluded)
	 {
	 	this.pricePrecision = 2;
	 	this.priceEntered = priceEntered;
	 	this.priceStd = priceStd;
	 	this.priceList = priceList;
	 	this.priceLimit = priceLimit;	 	
	 	this.qty = qty;
	 	this.taxRate = taxRate;
	 	this.isTaxIncluded = isTaxIncluded;
	 	this.discount = 0; /*discount percentage*/
	 	
	 	/*Bug fix
	 	 * If user forgot to set list & limit prices set them to std*/
	 	if(this.priceList == 0.0) this.priceList = this.priceStd;
	 	if(this.priceLimit == 0.0) this.priceLimit = this.priceStd;
	 },
	 
	 setPrecision : function(price)
	 {
		 var p = new Number(price).toFixed(this.pricePrecision);
		 
		 return parseFloat(p);
	 },
	 
	 getPrice : function(price, taxIncluded)
	 {
		 var newPrice  = 0;
		 
		 if(this.isTaxIncluded == taxIncluded){
			 newPrice = price;
		 }
		 else{
			 /*Price without tax*/
			 if(this.isTaxIncluded && !taxIncluded)		
			 {
				 newPrice = this.calculatePrice(price, true);
			 }
			 else
			 {
				 newPrice = this.calculatePrice(price, false);
			 }
		 }
		 
		 newPrice = this.setPrecision(newPrice);		 
		 return newPrice;

	 },
	 
	 getDiscount : function()
	 {
		 var discount = this.priceList - this.priceEntered;
		 
		 if(this.priceList == 0) return 0.0; /*prevent divide by zero error*/
		 
		 discount = (discount/this.priceList)*100;		 
		 return this.setPrecision(discount);
	 },
	 
	 setDiscount : function(discount)
	 {		 
		 discount = parseFloat(discount);
		 
		 this.discount = discount;
		 
		 if(isNaN(discount)){
			 //alert(messages.get('js.invalid.discount'));
			 discount = 0.0;
		 }
		 
		 if(discount > 100.0)
		 {
			 //alert(messages.get('js.invalid.discount'));
			 discount = 100.0;
		 }
		 		 
		 var newPrice = (this.priceList*(100.00 - discount))/100.00;
		 newPrice = this.setPrecision(newPrice);
		 
		 //round price		 
		 this.priceEntered = newPrice;
	 },
	 
	 getTotal : function()
	 {
		 return this.priceEntered * this.qty;
	 },
	 
	 	 
	 calculatePrice : function(price, isTaxIncluded)
	 {
		 var newPrice = 0;
		 
		 //if price includes tax
		 if(isTaxIncluded)
		 {
			 newPrice = (price * 100)/(100 + this.taxRate);
		 }
		 else
		 {
			 newPrice = price + ((price*this.taxRate)/100);
		 }
		 
		 newPrice = this.setPrecision(newPrice);		 
		 return newPrice;
		 
	 },
	 
	 getDiscountAmt : function()
	 {
		 var discountAmt = (this.priceList - this.priceEntered) * this.qty;
		 
		 discountAmt = this.setPrecision(discountAmt);		 
		 return discountAmt;
	 },
	 
	 getPriceEntered : function()
	 {
		 return this.priceEntered;
	 }
	 
 });
 
 var DiscountPanel = Class.create(PopUpBase, {
	 
	 initialize:function(){
		 	this.createPopUp($('lineItemDiscountPanel'));
		 	this.unit_tf = $('unit');
		 	this.total_tf = $('total');
		 	this.discount_percentage_tf = $('discount.percentage');
		 	this.discount_resetBtn = $('discount.resetBtn');	
		 	this.discount_applyBtn = $('discount.applyBtn');
		 	
		 	
		 	
		 	this.discount_cancelBtn = $('discount.cancel.button');
		 	//this.discount_total_cancelBtn = $('discount.total.cancel.button');
		 	this.discount_cancelBtn.onclick = this.hide.bind(this);
		 	/*this.discount_total_cancelBtn.onclick = function(e){
		 		this.hide();
		 	}; */
		 	
		 	/* add keypad */
		 	/*Event.observe(this.unit_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		 	Event.observe(this.total_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		 	Event.observe(this.discount_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			this.discount_percentage_tf.panel = this;
			this.discount_percentage_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscount();
				}
				
				this.panel.pp.setDiscount(this.value);
				this.panel.render(this);
				this.focus();
				//this.select();
			};	
			
			
			this.unit_tf.panel = this;
			this.unit_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscount();
				}
				
				var priceList = parseFloat(this.panel.pp.priceList);
				var oldValue = priceList; // this.panel.pp.getPrice(priceList, false);
				var newValue = parseFloat(this.value);
				
				var discount = ((oldValue-newValue)/oldValue)*100;
				
				this.panel.pp.setDiscount(discount);
				this.panel.render(this);
			};
			
			this.total_tf.panel = this;
			this.total_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscount();
				}
				
				var totalPrice = parseFloat(this.panel.pp.priceList * this.panel.pp.qty);
				var oldValue = totalPrice; //this.panel.pp.getPrice(totalPrice, false);
				var newValue = parseFloat(this.value);
				
				var discount = ((oldValue-newValue)/oldValue)*100;
				
				this.panel.pp.setDiscount(discount);
				this.panel.render(this);
			};	
			
			this.discount_resetBtn.panel = this;
			this.discount_resetBtn.onclick = function(e){
				this.panel.reset();
			};		
			
			this.discount_applyBtn.panel = this;
			this.discount_applyBtn.onclick = function(e){
				this.panel.applyDiscount();
			};
			
			/* discount on total*/
			
			this.discountOnTotal_percentage_tf = $('discountOnTotal.percentageTextfield');
			this.discountOnTotal_amount_tf = $('discountOnTotal.amountTextfield');
			this.discountOnTotal_total_tf = $('discountOnTotal.totalTextfield');
			
			this.discountOnTotal_applyBtn = $('discountOnTotal.applyButton');	
			this.discountOnTotal_resetBtn = $('discountOnTotal.resetButton');
			
			/* add keypad */
			/*Event.observe(this.discountOnTotal_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
			Event.observe(this.discountOnTotal_amount_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
			Event.observe(this.discountOnTotal_total_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			
			this.discountOnTotal_percentage_tf.panel = this;
			this.discountOnTotal_percentage_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscountOnTotal();
				}
				
				var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
				
				var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
				baseAmt = baseAmt + discountOnTotal;
				
				var percentage = this.panel.discountOnTotal_percentage_tf.value;
				percentage = parseFloat(percentage);
				
				if(isNaN(percentage)){
					percentage = 0.0;
				}
				
				var discountAmt = (baseAmt * percentage)/100.0;
				discountAmt = PriceManager.setPrecision(discountAmt, 2);
				
				var baseAmt = baseAmt - discountAmt;
				baseAmt = PriceManager.setPrecision(baseAmt, 2);
				
				this.panel.discountOnTotal_amount_tf.value = discountAmt;
				this.panel.discountOnTotal_total_tf.value = baseAmt;
				
			};
			
			this.discountOnTotal_amount_tf.panel = this;
			this.discountOnTotal_amount_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscountOnTotal();
				}
				
				var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
				
							
				var discountAmt = this.panel.discountOnTotal_amount_tf.value;
				discountAmt = parseFloat(discountAmt);
				
				if(isNaN(discountAmt)){
					discountAmt = 0;
				}
				
				discountAmt = PriceManager.setPrecision(discountAmt, 2);
				
				var percentage = 0;
				
				if(baseAmt != 0){
					percentage = (discountAmt * 100)/baseAmt;
				}
				
				percentage = PriceManager.setPrecision(percentage, 2);
				
				baseAmt = baseAmt - discountAmt;
				baseAmt = PriceManager.setPrecision(baseAmt, 2);
				
				this.panel.discountOnTotal_percentage_tf.value = percentage;
				this.panel.discountOnTotal_total_tf.value = baseAmt;
			};
			
			this.discountOnTotal_total_tf.panel = this;
			this.discountOnTotal_total_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscountOnTotal();
				}
				
				var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
				
				var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
				baseAmt = baseAmt + discountOnTotal;
				
				var amtEntered = this.panel.discountOnTotal_total_tf.value;
				
				amtEntered = parseFloat(amtEntered);
				
				if(isNaN(amtEntered)){
					amtEntered = 0;
				}
				
				amtEntered = PriceManager.setPrecision(amtEntered, 2);
				
				var discountAmt = baseAmt - amtEntered;
				var percentage = 0;
				
				if(baseAmt != 0){
					percentage = (discountAmt * 100)/baseAmt;
				}				
				
				discountAmt = PriceManager.setPrecision(discountAmt, 2);
				percentage = PriceManager.setPrecision(percentage, 2);
				
				this.panel.discountOnTotal_percentage_tf.value = percentage;
				this.panel.discountOnTotal_amount_tf.value = discountAmt;			
			};
			
			
			this.discountOnTotal_amount_tf.panel = this;
			this.discountOnTotal_total_tf.panel = this;
			
			this.discountOnTotal_applyBtn.panel = this;
			this.discountOnTotal_applyBtn.onclick = function(e){			
				this.panel.applyDiscountOnTotal();
			};
			
			this.discountOnTotal_resetBtn.panel = this;
			this.discountOnTotal_resetBtn.onclick = function(e){
				this.panel.resetDiscountOnTotal();
			};
			
			
			var keypadButtons = $$('#lineItemDiscountPanel .keypad-button');
            for(var i=0; i<keypadButtons.length; i++){
                var btn = keypadButtons[i];
                btn.onclick = this.keypadHandler.bind(this);
            }
            
            var inputs = $$('#lineItemDiscountPanel .numeric-input');
            for(var i=0; i<inputs.length; i++){
            	Event.observe(inputs[i],'click',this.clickHandler.bindAsEventListener(this),false);
            }           
            
	 	},
	 	
	 	clickHandler : function(e){
	 		var element = Event.element(e);
	 		this.textField = element;
	 		this.textField.focus();
	 		this.textField.select();
	 		
	 		/*this.textField.onkeyup = function(e){
	 			if (isNaN(this.value))
				{
	 				this.value = '';
					return;
				}
	 		}*/
	 	},
	 	
	 	keypadHandler:function(e){
	 		var button = Event.element(e); 
	        var element = button.getElementsByClassName('keypad-value')[0];
	        
	        /*Fix for google chrome*/
	          if (element == null)
	          {
	          		element = button;  
	          }
	          /**********************/
	          
	        var value = element.innerHTML;
                         
            var inputValue = this.textField.value;
            
            if (this.textField.selectionStart != this.textField.selectionEnd)
            {
            	inputValue = '';
				this.textField.value = '';
				this.textField.focus();
            }
            
            //if(inputValue == '') inputValue = 0;
            
			if('Del' == element.innerHTML.strip()){
				
				if(inputValue.length > 0)
				{
					inputValue = inputValue.substring(0,inputValue.length-1);
				}
            }
			else if('-' == element.innerHTML.strip()){
				
				if(inputValue.indexOf('-') == -1){
					inputValue = '-' + inputValue;
				}
				else{
					inputValue = inputValue.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(inputValue.indexOf('.') == -1)
				{
					inputValue = inputValue + value;
				}
			}
			
			else{
				inputValue = inputValue + value;
			}
				
			this.textField.value = inputValue;
				
             
              if(isNaN(inputValue) || (inputValue < 0.0)){           
                  return;
              }
             
              var onkeyup = this.textField.onkeyup;
	  			if(onkeyup){
	  				var event = {keyCode:65};
	  				onkeyup.call(this.textField,event);
	  			}
        },
	 
	 onShow:function(){		
	 
		 if(ShoppingCartManager.isShoppingCartEmpty()) return;
		 
		 if(!DiscountManager.isDiscountOnLineAllowed()) return;
		 
		 if(!DiscountManager.isDiscountOnTotalAllowed())
		 {
			/*hide discount on total panel*/
			DiscountManager.hideDiscountOnTotalPanel();
		 }
		 else
		 {
			DiscountManager.showDiscountOnTotalPanel();
		 }		
		
		this.loadLineDetails();
		this.renderDiscountRightsInfo();
		
		setTimeout(function(){
			$('unit').focus();
			$('unit').select();
		},500);
		
		this.textField = this.unit_tf;
		
		
	 },
	 
	 /*hide:function(){
		 $('discountContainer').hide();
		 $('discountRights').style.display = 'none';
		 $('item-details-panel').style.display = 'none';
		 $('discountOnTotalContainer').style.display = 'none';
		 $('shopping-cart-panel').show();	
		 $('glider-column').show();
		// $$('.sidebar-main-button')[0].show();
		 
		 this.visible = false;
	 },*/
 
	 
 	
 	applyDiscount:function(){
 		
 		//User discount is not allowed in case of customer discount code or product discount code
		var bp = BPManager.getBP();
		var line = shoppingCart.lines[shoppingCart.selectedIndex];
		 
		 if(bp != null)
		 {
			 var customerDiscountCodeId = bp['u_pos_discountcode_id'];
			 
			 if (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0 && line.isDiscountCodeApplied)
			 {
				 alert('Business partner has discount code. User discount is not possible.');
				 return;
			 }
		 }		 
		 
		 var discountCode = line.discountCode;
		 
        if( discountCode != null && line.isDiscountCodeApplied)
        {
        	alert('Product has discount code. User discount is not possible.');
			return;
        }
			 
 		/*get discount percentage*/
 		var discountPercentage = this.pp.getDiscount();
 		
 		var rights = DiscountManager.getDiscountRights();
 		var allowUpSell = rights.allowUpSell;
 		
 		if(!DiscountManager.isLineDiscountValid(discountPercentage))
 		{
 			return;
 		} 		
 		
 		if(allowUpSell){
 			//do nothing
 		}
 		else
 		{
 			if(!DiscountManager.overrideLimitPrice()){
 	 			var priceEntered = parseFloat(this.pp.priceEntered);
 	 	 		var priceLimit = parseFloat(this.pp.priceLimit);
 	 	 		
 	 	 		if(priceLimit > priceEntered){
 	 	 			alert('The Price Limit [' + priceLimit + '] has been exceeded by the Price Entered: ' + priceEntered);
 	 				return;
 	 	 		}
 	 		}
 		}
 		
 		
 		/*get discount amt*/
		var discountAmt = this.pp.getDiscountAmt();
		discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
		discountAmt = parseFloat(discountAmt);
		
		if(isNaN(discountAmt))
		{
			alert(Translation.translate('error.invalid.discount.amount') + ': ' + discountAmt);
			return;
		}
    		
		ShoppingCartManager.setDiscountOnLine(discountAmt);	
		this.hide();
 	},
 	
 	applyDiscountOnTotal:function(){
 		/*validate discount limit*/ 
		
		var discountPercentage = this.discountOnTotal_percentage_tf.value			
		if(!DiscountManager.isDiscountValid(discountPercentage)){
			return;
		}
		
		var discountAmt = this.discountOnTotal_amount_tf.value;
		discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
		
		if(discountAmt < 0.0){
			alert(Translation.translate('discount.amount.cannot.be.negative','Discount Amount cannot be negative!'));
			return;
		}
		
		var previousDiscountOnTotal = ShoppingCartManager.getDiscountOnTotal();
		if(previousDiscountOnTotal != 0){
			alert(Translation.translate("you.have.already.given.discount.on.total.please.clear.previous.discount.on.total.click.reset.to.clear.previous.discounts","You have already given discount on total! Please clear previous discount on total! Click reset to clear previous discounts!"));
			return;
		}
		
		ShoppingCartManager.setDiscountOnTotal(discountAmt);
		this.hide();
 	},
 	
 	loadLineDetails:function()
 	{
 		if(ShoppingCartManager.isShoppingCartEmpty()) return;
 		
 		var line = shoppingCart.lines[shoppingCart.selectedIndex];
		
 		var priceEntered = line.priceEntered;
		var priceList = line.priceList;
		var priceStd = line.priceStd;
		var priceLimit = line.priceLimit;
		var isTaxIncluded = line.isTaxIncluded;
		var taxRate = line.taxRate;
		var qty = line.qty;
		
		/*Parse values*/
		isTaxIncluded = new Boolean(isTaxIncluded);
		taxRate = parseFloat(taxRate);
		priceList = parseFloat(priceList);
		priceStd = parseFloat(priceStd);
		priceLimit = parseFloat(priceLimit);
		qty = parseFloat(qty);
		
		this.pp = new PPricing(priceEntered,priceStd,priceList,priceLimit,qty,taxRate,isTaxIncluded); 
		
		this.render(null);
		
		if(DiscountManager.isDiscountOnTotalAllowed())
		{
			/* render discount on total */
			this.renderDiscountOnTotal();
		}
		
		/* discount codes */
        var discountCodes = line.discountCodes;
        var ref = this;
        var discountCode;
        
        if( discountCodes.length > 0 ){
        	
        	jQuery('#discount-code-container').show();
        	
        	var html = "<option value=''></option>";
        	
        	for(var i=0; i<discountCodes.length; i++){
        		
        		discountCode = discountCodes[i];
        		
        		html += "<option value='" + discountCode['id'] + "'>" + discountCode['name']  + "</option>"
        	}
        	
        	jQuery('#discount-code-select').html(html);
        	
        	//populate discount code
        	if(line.isDiscountCodeApplied && line.discountCode ){
        		jQuery('#discount-code-select').val(line.discountCode.id);
        	}
        	
        	var btn = jQuery('#discount-code-apply-button')[0];
        	btn.onclick = function(){
        		
        		var id = jQuery('#discount-code-select').val();
        		
        		if(id == ''){
        			
        			alert('Please select a discount code');
        			return;
        			
        		}
        		
        		ShoppingCartManager.applyDiscountCode( id );
        		
        		ref.hide();
        		
        	};
        	
        	var btn = jQuery('#discount-code-reset-button')[0];
        	btn.onclick = function(){
        		
        		var bp = BPManager.getBP();
        		
        		if(bp != null)
	       		 {
	       			 var customerDiscountCodeId = bp['u_pos_discountcode_id'];
	       			 
	       			 if (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0 && line.isDiscountCodeApplied)
	       			 {
	       				 alert('Business partner has discount code. User discount is not possible.');
	       				 return;
	       			 }
	       		 }
        		
        		ShoppingCartManager.resetLineDiscountCode();
        		
        		ref.hide();
        		
        	};
        }
        else
        {
        	jQuery('#discount-code-container').hide();
        }
		
 	},
 	
 	isDiscountValid : function(discount){
 		return DiscountManager.isDiscountValid(discount);
 	},
 	
 	renderDiscountRightsInfo:function(){
 		
 		var rights = DiscountManager.getDiscountRights();
 		
 		//$('discountUpToLimitPriceLabel').innerHTML = rights.discountUpToLimitPrice ? 'Yes' : 'No';
 		$('overrideLimitPriceLabel').innerHTML = rights.overrideLimitPrice ? Translation.translate('yes') : Translation.translate('no');
 		$('allowDiscountOnTotalLabel').innerHTML = rights.allowDiscountOnTotal ? Translation.translate('yes') : Translation.translate('no');
 		$('discountLimitLabel').innerHTML = rights.discountLimit + '%';
 		
 		if(rights.allowOrderBackDate && OrderScreen.backDateButton){
 			OrderScreen.backDateButton.show();
		 }
 	},
 	
 	resetDiscountOnTotal:function(){
 		var subTotal = ShoppingCartManager.getSubTotal();
 		subTotal = parseFloat(subTotal.toFixed(2));
 		
 		subTotal = PriceManager.setPrecision(subTotal, 2);		
		this.discountOnTotal_amount_tf.value = 0;
		this.discountOnTotal_percentage_tf.value = 0;
		this.discountOnTotal_total_tf.value = subTotal;
		
		if(window.confirm(Translation.translate("confirmation.reset.discount"))){
			/* clear previous discounts */
			ShoppingCartManager.setDiscountOnTotal(0);
		}		
		
 	},
 	
 	renderDiscountOnTotal:function(){
 		
 		var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
		
		var discountAmt = ShoppingCartManager.getDiscountOnTotal();
 		var percentage = (discountAmt * 100)/(baseAmt + discountAmt);
 		
 		if(isNaN(percentage)) percentage = 0;
 		
		percentage = PriceManager.setPrecision(percentage, 2);
		discountAmt = PriceManager.setPrecision(discountAmt, 2);
		
		this.discountOnTotal_amount_tf.value = discountAmt;
		this.discountOnTotal_percentage_tf.value = percentage;
		this.discountOnTotal_total_tf.value = baseAmt;
 	},
 	
 	render:function(elementNotToRender)
 	{
 		var unitPrice = this.pp.priceEntered;
		var totalPrice = this.pp.getTotal();
		
		if(elementNotToRender != this.unit_tf)
		this.unit_tf.value = Number(unitPrice).toFixed(2); //Number(this.pp.getPrice(unitPrice, false)).toFixed(2);
		
		/*if(elementNotToRender != this.unit_incl_tf)
		this.unit_incl_tf.value = Number(this.pp.getPrice(unitPrice, true)).toFixed(2);	*/
		
		if(elementNotToRender != this.total_tf)
		this.total_tf.value = Number(totalPrice).toFixed(2); //Number(this.pp.getPrice(totalPrice, false)).toFixed(2);
		
		/*if(elementNotToRender != this.total_incl_tf)
		this.total_incl_tf.value = Number(this.pp.getPrice(totalPrice, true)).toFixed(2);*/
		
		if(elementNotToRender != this.discount_percentage_tf)
		this.discount_percentage_tf.value = Number(this.pp.getDiscount()).toFixed(2);
		
		
 	},
 	
 	reset:function(){
 		this.pp.priceEntered = this.pp.priceStd;
 		this.render(null);
 	}
 });
 
 

 
 var DiscountManager = {
 		
 		defaultDiscountRights:null,
 		discountRights:null,
 		
 		discountPanel:null,
 		discountOnTotalPanel:null,
 		
 		getDiscountPanel : function(){
 			
 			if (this.discountPanel == null)
 			{
 				alert(Translation.translate('discount.limit.has.not.been.set','Discount Limit has not been set!'));
 			}
 			else
			{
 				this.discountPanel.show();
			}
 		},
 		
 		getDiscountOnTotalPanel : function(){
 			
 			//Customer discount code overrides all other discounts
 			var bp = BPManager.getBP();
 			 
 			 if(bp != null)
 			 {
 				 var customerDiscountCodeId = bp['u_pos_discountcode_id'];
 				 
 				 if (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0)
 				 {
 					 alert('Business partner has discount code. All other discounts are not applicable.');
 					 return;
 				 }
 			 }
 			
 			if (this.discountOnTotalPanel == null)
 			{
 				alert(Translation.translate('discount.on.total.denied','Discount on total denied!'));
 			}
 			else
			{
 				if (ShoppingCartManager.isShoppingCartEmpty())
 				{
 					return;
 				}
 				this.discountOnTotalPanel.show();
			}
 		},
 		
		 /* set discount amt on cart total */
		 setDiscountOnTotal : function(discountAmt){
			 ShoppingCartManager.setDiscountOnTotal(discountAmt);
		 },
		 
		 isDiscountOnTotalAllowed : function(){
			 
			 var rights = DiscountManager.getDiscountRights();	
			 
			 return rights.allowDiscountOnTotal;
		 },
		 
		 isDiscountOnLineAllowed : function(){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
					 
			 if(rights.discountLimit > 0.0) return true;
			 
			 var msg = messages.get(Translation.translate('discount.limit.has.not.been.set','Discount Limit has not been set!')); 
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 overrideLimitPrice : function(){
			 
			 var rights = DiscountManager.getDiscountRights();	
			 
			 return rights.overrideLimitPrice;
		 },
		 
		 allowOrderBackDate : function(){
			 var rights = DiscountManager.getDiscountRights();				 
			 return rights.allowOrderBackDate;
		 },
		 
		 showDiscountOnTotalPanel : function(){
			 /*$('discountOnTotalDetails').show();*/
			/* $('discount-button').style.display = 'inline';*/
			 
			 
		 },
		 
		 hideDiscountOnTotalPanel : function(){
			 /*$('discountOnTotalDetails').hide();*/
			/* $('discount-button').style.display = 'none';*/
		 },
		 
		 isDiscountValid : function(discountPercentage){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
			 
			 /* user can give discount */
			 if(discountPercentage <= rights.discountLimit) return true;
			 
			 /* throw error message*/
			 var msg = messages.get('Discount is greater than discount limit!');
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 isLineDiscountValid : function(discountPercentage){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
			 
			 var discountLimit = rights.discountLimit;
			 
			 var pp = this.discountPanel.pp;
			 
			 var initialDiscount = 0;
			 
			 if( pp.priceList != 0.0 && pp.priceList != pp.priceStd )
			 {
				 initialDiscount = ((pp.priceList - pp.priceStd) * 100)/ pp.priceList;
			 }
			 
			 if(rights.discountOnCurrentPrice)
			 {
				 //calculate new discount limit
				 var d1 = 100 - initialDiscount;
				 var d2 = 100 - discountLimit;
					
				 var d3 = (d1 * d2) / (100);
				 var d4 = 100 - d3;
				 
				 discountLimit = parseFloat(new Number(d4).toFixed(2));
			 }
			 else
			 {
				 if( initialDiscount > discountLimit ){
					 discountLimit = initialDiscount;
				 }
			 }
			 
			 /* user can give discount */
			 if(discountPercentage <= discountLimit) return true;
			 
			 /* throw error message*/
			 var msg = messages.get('Discount is greater than discount limit!');
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 getDiscountRights : function(){
			 return this.discountRights;
		 },
		 
		 setDiscountRights : function(discountRights){
			 if(this.defaultDiscountRights == null) this.defaultDiscountRights = discountRights;
			 this.discountRights = discountRights;
			 
			 /* notifies changes in discount rights */
			 this.discountRightsChangeNotifier();
		 },
		 
		 resetDiscountRights:function(){
		 	this.setDiscountRights(this.defaultDiscountRights);
		 },
		 
		 discountRightsChangeNotifier : function(){
			 /* notifies changes in discount rights */
			 
			/* if(!DiscountPanel.visible) return;*/
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 if(rights.discountLimit > 0.0){
				 this.discountPanel = new DiscountPanel();
			 }
			 
			 if(rights.allowDiscountOnTotal){
				 this.discountOnTotalPanel = new DiscountOnTotalPanel();
			 }
			 
			 if (this.discountPanel != null)
			 {
				 this.discountPanel.renderDiscountRightsInfo();
			 }
		 },
		 
		 reportError:function(err){
			 alert(err);
		 }
 };
 
var PriceManager = {
		setPrecision : function(price, precision){
			return parseFloat(new Number(price).toFixed(precision));
		}
};
 
var DiscountRights =  Class.create({
	initialize : function(discountLimit, overrideLimitPrice, discountUpToLimitPrice, allowDiscountOnTotal, allowOrderBackDate, allowUpSell, discountOnCurrentPrice){
		
		if(ORDER_TYPES.POS_GOODS_RETURNED_NOTE == ShoppingCartManager.getOrderType()){
			
			this.discountLimit = 100;
		}
		else{
			
			this.discountLimit = discountLimit;
		}
		
		this.overrideLimitPrice = overrideLimitPrice;
		this.discountUpToLimitPrice = !overrideLimitPrice;
		this.allowDiscountOnTotal = allowDiscountOnTotal;
		this.allowOrderBackDate = allowOrderBackDate;
		this.allowUpSell = allowUpSell;
		this.discountOnCurrentPrice = discountOnCurrentPrice;
	}
});


var DiscountOnTotalPanel = Class.create(PopUpBase, {
		
	initialize:function(){
		/* discount on total*/
		
		this.createPopUp($('discountOnTotalContainer'));
		this.cancelBtn = $('create.customer.popup.close.button');
		
		
		this.initialized = false;	
		 
		this.visible = false;
		
		this.discountOnTotal_percentage_tf = $('discountOnTotal.percentageTextfield');
		this.discountOnTotal_amount_tf = $('discountOnTotal.amountTextfield');
		this.discountOnTotal_total_tf = $('discountOnTotal.totalTextfield');
		
		this.discountOnTotal_applyBtn = $('discountOnTotal.applyButton');	
		this.discountOnTotal_resetBtn = $('discountOnTotal.resetButton');
		this.discount_total_cancelBtn = $('discount.total.cancel.button');
		
	 	this.discount_total_cancelBtn.onclick = this.hide.bind(this);
		
		/* add keypad */
		/*Event.observe(this.discountOnTotal_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(this.discountOnTotal_amount_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(this.discountOnTotal_total_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
		
		
		this.discountOnTotal_percentage_tf.panel = this;
		this.discountOnTotal_percentage_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
			baseAmt = baseAmt + discountOnTotal;
			
			var percentage = this.panel.discountOnTotal_percentage_tf.value;
			percentage = parseFloat(percentage);
			
			if(isNaN(percentage)){
				percentage = 0.0;
			}
			
			var discountAmt = (baseAmt * percentage)/100.0;
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			
			var baseAmt = baseAmt - discountAmt;
			baseAmt = PriceManager.setPrecision(baseAmt, 2);
			
			this.panel.discountOnTotal_amount_tf.value = discountAmt;
			this.panel.discountOnTotal_total_tf.value = baseAmt;
			
		};
		
		this.discountOnTotal_amount_tf.panel = this;
		this.discountOnTotal_amount_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
						
			var discountAmt = this.panel.discountOnTotal_amount_tf.value;
			discountAmt = parseFloat(discountAmt);
			
			if(isNaN(discountAmt)){
				discountAmt = 0;
			}
			
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			
			var percentage = 0;
			
			if(baseAmt != 0){
				percentage = (discountAmt * 100)/baseAmt;
			}
			
			percentage = PriceManager.setPrecision(percentage, 2);
			
			baseAmt = baseAmt - discountAmt;
			baseAmt = PriceManager.setPrecision(baseAmt, 2);
			
			this.panel.discountOnTotal_percentage_tf.value = percentage;
			this.panel.discountOnTotal_total_tf.value = baseAmt;
		};
		
		this.discountOnTotal_total_tf.panel = this;
		this.discountOnTotal_total_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
			baseAmt = baseAmt + discountOnTotal;
			
			var amtEntered = this.panel.discountOnTotal_total_tf.value;
			
			amtEntered = parseFloat(amtEntered);
			
			if(isNaN(amtEntered)){
				amtEntered = 0;
			}
			
			amtEntered = PriceManager.setPrecision(amtEntered, 2);
			
			var discountAmt = baseAmt - amtEntered;
			var percentage = 0;
			
			if(baseAmt != 0){
				percentage = (discountAmt * 100)/baseAmt;
			}				
			
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			percentage = PriceManager.setPrecision(percentage, 2);
			
			this.panel.discountOnTotal_percentage_tf.value = percentage;
			this.panel.discountOnTotal_amount_tf.value = discountAmt;			
		};
		
		
		this.discountOnTotal_amount_tf.panel = this;
		this.discountOnTotal_total_tf.panel = this;
		
		this.discountOnTotal_applyBtn.panel = this;
		this.discountOnTotal_applyBtn.onclick = function(e){			
			this.panel.applyDiscountOnTotal();
		};
		
		this.discountOnTotal_resetBtn.panel = this;
		this.discountOnTotal_resetBtn.onclick = function(e){
			this.panel.resetDiscountOnTotal();
		};
		
		this.initialized = true;
		
		var keypadButtons = $$('#discountOnTotalContainer .keypad-button');
        for(var i=0; i<keypadButtons.length; i++){
            var btn = keypadButtons[i];
            btn.onclick = this.keypadHandler.bind(this);
        }
        
        
        var inputs = $$('#discountOnTotalContainer .numeric-input');
        for(var i=0; i<inputs.length; i++){
        	Event.observe(inputs[i],'click',this.clickHandler.bindAsEventListener(this),false);
        }
 	},
		 
 	toggle:function(){		
	 	 this.visible = !this.visible;	
	 
		 if(!this.initialized) this.initialize();
		 
		 if(ShoppingCartManager.isShoppingCartEmpty()) return;
		 
		 if(!DiscountManager.isDiscountOnLineAllowed()) return;
		 		 
		 if(!this.visible){			
			 $('discountOnTotalContainer').hide();
			 $('discountContainer').style.display = 'none';
			 $('discountRights').style.display = 'none';
			 $('item-details-panel').style.display = 'none';
			 $('glider-column').show();
			 //$$('.sidebar-main-button')[0].show();
			 
			 return;
		 }
		 else
		 {
			 $('glider-column').hide();
			 //$$('.sidebar-main-button')[0].hide();
			 $('discountContainer').style.display = 'none';
			 $('item-details-panel').style.display = 'block';
			 $('discountOnTotalContainer').show();	
			 $('discountRights').style.display = 'block';
		 }
		 
		 if(!DiscountManager.isDiscountOnTotalAllowed())
		 {
			/*hide discount on total panel*/
			DiscountManager.hideDiscountOnTotalPanel();
		 }
		 else
		 {
			DiscountManager.showDiscountOnTotalPanel();
		 }		
		
		this.loadLineDetails();
		this.renderDiscountRightsInfo();	
	 },
		 
		/* hide:function(){
			 $('discountOnTotalContainer').hide();
			 $('discountContainer').style.display = 'none';
			 $('discountRights').style.display = 'none';
			 $('item-details-panel').style.display = 'none';
			 $('glider-column').show();	
			 //$$('.sidebar-main-button')[0].show();
			 
			 this.visible = false;
		 },*/
	 
		 
	 	
	 	applyDiscountOnTotal:function(){
	 		/*validate discount limit*/ 
			
			var discountPercentage = this.discountOnTotal_percentage_tf.value			
			if(!DiscountManager.isDiscountValid(discountPercentage)){
				return;
			}
			
			var discountAmt = this.discountOnTotal_amount_tf.value;
			discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
			
			if(discountAmt < 0.0){
				alert(Translation.translate('discount.amount.cannot.be.negative','Discount Amount cannot be negative!'));
				return;
			}
			
			var previousDiscountOnTotal = ShoppingCartManager.getDiscountOnTotal();
			if(previousDiscountOnTotal != 0){
				alert(Translation.translate("you.have.already.given.discount.on.total.please.clear.previous.discount.on.total.click.reset.to.clear.previous.discounts","You have already given discount on total! Please clear previous discount on total! Click reset to clear previous discounts!"));
				return;
			}
			
			ShoppingCartManager.setDiscountOnTotal(discountAmt);
			//DiscountPanel.hide();
			this.hide();
	 	},
	 	
	 	loadLineDetails:function()
	 	{
	 		if(!this.visible) return;
	 		
	 		if(ShoppingCartManager.isShoppingCartEmpty()) return;
	 		
	 		var line = shoppingCart.lines[shoppingCart.selectedIndex];
			
	 		var priceEntered = line.priceEntered;
			var priceList = line.priceList;
			var priceStd = line.priceStd;
			var priceLimit = line.priceLimit;
			var isTaxIncluded = line.isTaxIncluded;
			var taxRate = line.taxRate;
			var qty = line.qty;
			
			/*Parse values*/
			isTaxIncluded = new Boolean(isTaxIncluded);
			taxRate = parseFloat(taxRate);
			priceList = parseFloat(priceList);
			priceStd = parseFloat(priceStd);
			priceLimit = parseFloat(priceLimit);
			qty = parseFloat(qty);
			
			this.pp = new PPricing(priceEntered,priceStd,priceList,priceLimit,qty,taxRate,isTaxIncluded); 
			
			this.render(null);
			
			if(DiscountManager.isDiscountOnTotalAllowed())
			{
				/* render discount on total */
				this.renderDiscountOnTotal();
			}
			
	 	},
	 	
	 	isDiscountValid : function(discount){
	 		return DiscountManager.isDiscountValid(discount);
	 	},
	 	
	 	renderDiscountRightsInfo:function(){
	 		
	 		var rights = DiscountManager.getDiscountRights();
	 		
	 		//$('discountUpToLimitPriceLabel').innerHTML = rights.discountUpToLimitPrice ? 'Yes' : 'No';
	 		$('overrideLimitPriceLabel').innerHTML = rights.overrideLimitPrice ? 'Yes' : 'No';
	 		$('allowDiscountOnTotalLabel').innerHTML = rights.allowDiscountOnTotal ? 'Yes' : 'No';
	 		$('discountLimitLabel').innerHTML = rights.discountLimit + '%';
	 		
	 		if(rights.allowOrderBackDate && OrderScreen.backDateButton){
	 			OrderScreen.backDateButton.show();
			 }
	 	},
	 	
	 	resetDiscountOnTotal:function(){
	 		var subTotal = ShoppingCartManager.getSubTotal();
	 		subTotal = parseFloat(subTotal.toFixed(2));
	 		
	 		subTotal = PriceManager.setPrecision(subTotal, 2);		
			this.discountOnTotal_amount_tf.value = 0;
			this.discountOnTotal_percentage_tf.value = 0;
			this.discountOnTotal_total_tf.value = subTotal;
			
			if(window.confirm(Translation.translate("confirmation.reset.discount"))){
				/* clear previous discounts */
				ShoppingCartManager.setDiscountOnTotal(0);
			}		
			
	 	},
	 	
	 	renderDiscountOnTotal:function(){
	 		
	 		var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountAmt = ShoppingCartManager.getDiscountOnTotal();
	 		var percentage = (discountAmt * 100)/(baseAmt + discountAmt);
	 		
	 		if(isNaN(percentage)) percentage = 0;
	 		
			percentage = PriceManager.setPrecision(percentage, 2);
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			
			this.discountOnTotal_amount_tf.value = discountAmt;
			this.discountOnTotal_percentage_tf.value = percentage;
			this.discountOnTotal_total_tf.value = baseAmt;
	 	},
	 	
	 	render:function(elementNotToRender)
	 	{
			
	 	},
	 	
	 	reset:function(){
	 		this.pp.priceEntered = this.pp.priceStd;
	 		this.render(null);
	 	},
	 	
	 	clickHandler : function(e){
	 		var element = Event.element(e);
	 		this.textField = element;
	 		this.textField.focus();
	 		this.textField.select();
	 		
	 		/*this.textField.onkeyup = function(e){
	 			if (isNaN(this.value))
				{
	 				this.value = '';
					return;
				}
	 		}*/
	 	},
	 	
	 	keypadHandler:function(e){
	 		var button = Event.element(e); 
	        var element = button.getElementsByClassName('keypad-value')[0];
	        
	        /*Fix for google chrome*/
	          if (element == null)
	          {
	          		element = button;  
	          }
	          /**********************/
	          
	        var value = element.innerHTML;
                         
            var inputValue = this.textField.value;
            
            if (this.textField.selectionStart != this.textField.selectionEnd)
			{
				inputValue = '';
				this.textField.value = '';
				this.textField.focus();
			}
            
			if('Del' == element.innerHTML.strip()){
          	  
				if(inputValue.length > 0)
				{
					inputValue = inputValue.substring(0,inputValue.length-1);
				}
            }
			else if('-' == element.innerHTML.strip()){
				
				if(inputValue.indexOf('-') == -1){
					inputValue = '-' + inputValue;
				}
				else{
					inputValue = inputValue.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(inputValue.indexOf('.') == -1)
				{
					inputValue = inputValue + value;
				}
			}
			
			else{
				inputValue = inputValue + value;
			}
				
			this.textField.value = inputValue;
				
             
              if(isNaN(inputValue) || (inputValue < 0.0)){           
                  return;
              }
             
              var onkeyup = this.textField.onkeyup;
	  			if(onkeyup){
	  				var event = {keyCode:65};
	  				onkeyup.call(this.textField,event);
	  			}
        },
        
        onShow:function(){		
        	var subTotal = ShoppingCartManager.getSubTotal();
     		subTotal = parseFloat(subTotal.toFixed(2));
     		
     		subTotal = PriceManager.setPrecision(subTotal, 2);		
    		this.discountOnTotal_amount_tf.value = 0;
    		this.discountOnTotal_percentage_tf.value = 0;
    		this.discountOnTotal_total_tf.value = subTotal;
    		
    		setTimeout(function(){
    			$('discountOnTotal.percentageTextfield').focus();
    			$('discountOnTotal.percentageTextfield').select();
    		},500);
    		
    		this.textField = this.discountOnTotal_percentage_tf;
   	 }
        
	 });
