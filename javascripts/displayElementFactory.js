var DisplayType = {
	_String			: 10,
	_Integer		: 11,
	_Amount 		: 12,
	_ID 			: 13,
	_Text 			: 14,
	_Date 			: 15,
	_DateTime 		: 16,
	_List       	: 17,
	_Table      	: 18,
	_TableDir   	: 19,
	_YesNo      	: 20,
	_Location   	: 21,
	_Number     	: 22,
	_Binary     	: 23,
	_Time       	: 24,
	_Account   	 	: 25,
	_RowID    	 	: 26,
	_Color   	 	: 27,
	_Button		 	: 28,
	_Quantity  	 	: 29,
	_Search     	: 30,
	_Locator    	: 31,
	_Image      	: 32,
	_Assignment 	: 33,
	_Memo       	: 34,
	_PAttribute 	: 35,
	_TextLong   	: 36,
	_CostPrice  	: 37,
	_FilePath  	 	: 38,
	_FileName  	 	: 39,
	_URL  			: 40,
	_PrinterName	: 42,
	
	isID : function(displayType)
	{
		if (displayType == DisplayType._ID || displayType == DisplayType._Table || displayType == DisplayType._TableDir
			|| displayType == DisplayType._Search || displayType == DisplayType._Location || displayType == DisplayType._Locator
			|| displayType == DisplayType._Account || displayType == DisplayType._Assignment || displayType == DisplayType._PAttribute
			|| displayType == DisplayType._Image || displayType == DisplayType._Color)			
			return true;
		
		return false;
	},
	
	isNumeric : function(displayType)
	{
		if (displayType == DisplayType._Amount || displayType == DisplayType._Number || displayType == DisplayType._CostPrice 
			|| displayType == DisplayType._Integer || displayType == DisplayType._Quantity)
			return true;
		
		return false;
	},
	
	isText : function(displayType)
	{
		if (displayType == DisplayType._String || displayType == DisplayType._Text 
			|| displayType == DisplayType._TextLong || displayType == DisplayType._Memo
			|| displayType == DisplayType._FilePath || displayType == DisplayType._FileName
			|| displayType == DisplayType._URL || displayType == DisplayType._PrinterName)
			return true;
		
		return false;
	},
	
	isDate : function(displayType)
	{
		if (displayType == DisplayType._Date || displayType == DisplayType._DateTime || displayType == DisplayType._Time)
			return true;
		
		return false;
	},
	
	isLookup : function(displayType)
	{
		if (displayType == DisplayType._List || displayType == DisplayType._Table
			|| displayType == DisplayType._TableDir || displayType == DisplayType._Search)
			return true;
		
		return false;
	}
};

var DisplayElementFactory = Class.create({
	initialize : function()
	{
		this.displayType = 0;
		this.element = null;
	},
	
	getWElement : function(json)
	{
		this.displayType = json.displayType;
		
		if (DisplayType.isLookup(this.displayType) || DisplayType.isID(this.displayType)) 
        {
            if (this.displayType == DisplayType._Search)
            {
            	this.element = new WSearch(json);
            }
	        else
	        {
	        	this.element = new WList(json);
	        }
        }
		else if (this.displayType == DisplayType._YesNo) 
        {
			this.element = new WCheck(json);
        }
		else if (DisplayType.isNumeric(this.displayType) || this.displayType == DisplayType._String)
        {
			var wText = new WText(json);
			
			if (json.range)
			{
				this.element = new WRange(wText);
			}
			else
			{
				this.element = wText;
			}
        }
		else if (DisplayType.isDate(this.displayType)) 
        {
			var wDate = new WDate(json);
			
			if (json.range)
			{
				this.element = new WDateRange(wDate);
			}
			else
			{
				this.element = wDate;
			}
        }
		else
        {
			var wText = new WText(json);
			
			if (json.range)
			{
				this.element = new WRange(wText);
			}
			else
			{
				this.element = wText;
			}
        }
		
		return this.element;
	}
});

var WElement = Class.create({
	initialize : function(json)
	{
		this.json = json;
		this.columnName = json.columnName;
		this.id = json.id;
		this.label = json.label;
		this.parameterId = json.parameterId;
		this.value = json.value;
		this.valueTo = json.valueTo;
		this.displayValue = json.displayValue;
		this.displayValueTo = json.displayValueTo;
		this.displayType = json.displayType;
		this.range = json.range;
		this.mandatory = json.mandatory;
		this.displayElementId = this.id;
		this._element = null;
		this.initElement(this.id);
	},
	
	setValue : function(value)
	{
		if (this._element != null)
			this._element.setAttribute('value', value);
	},
	
	init : function(){},
	
	getDisplayElementId : function()
	{
		return this.displayElementId;
	},
	
	getColumnName : function()
	{
		return this.columnName;
	},
	
	getLabel : function()
	{
		return this.label;
	},
	
	getElement : function()
	{
		return this._element;
	}
});

var WText = Class.create(WElement, {
	initialize : function($super, json)
	{
		$super(json);
	},
	
	clone : function(id)
	{
		var j = this.json;
		j.id = id;
		
		return new WText(j);
	},
	
	initElement : function(id)
	{
		this._element = document.createElement('input');
		this._element.setAttribute('type', 'text');
		this._element.setAttribute('name', this.columnName);
		this._element.setAttribute('id', id);
		
		this.value = (this.value == null) ? '' : this.value;
		this._element.setAttribute('value', this.value);
		
	},
	
	setName : function(name)
	{
		this._element.setAttribute('name', name);
	},
	
	setValue : function(value)
	{
		this.value = (value == null) ? '' : value;
		this._element.setAttribute('value', this.value);
	}
});

var WSearch = Class.create(WElement, {
	
	initialize : function($super, json)
	{
		$super(json);
	},

	clone : function(id)
	{
		var j = this.json;
		j.id = id;
		
		return new WSearch(j);
	},
	
	initElement : function(id)
	{
		this.queryInputId = id + 'Query';
		this.displayElementId = this.queryInputId;
		this.querySearchResultId = id + 'QuerySearchResult';
		
		this._hiddenInput = document.createElement('input');
		this._hiddenInput.setAttribute('type', 'hidden');
		this._hiddenInput.setAttribute('id', id);
		this._hiddenInput.setAttribute('name', this.columnName);
		
		this._queryInput = document.createElement('input');
		this._queryInput.setAttribute('id', this.queryInputId);

		this._queryResult = document.createElement('div');
		this._queryResult.setAttribute('id', this.querySearchResultId);
		this._queryResult.setAttribute('class', 'autocomplete');
		this._queryResult.setAttribute('opacity', 0);
		
		this._element = document.createElement('span');
		this._element.setAttribute('id', this.columnName + 'Container');
		this._element.appendChild(this._hiddenInput);
		this._element.appendChild(this._queryInput);
		this._element.appendChild(this._queryResult);
		
		if (this.value != -1)
		{
			this.setValue(this.value, this.displayValue);
		}
	},
	
	setValue : function(value, displayValue)
	{
		if (value == null)
		{
			this._hiddenInput.removeAttribute('value');
		}
		else
		{
			this._hiddenInput.setAttribute('value',value);
		}
		if (displayValue == null)
		{
			this._queryInput.removeAttribute('value');
		}
		else
		{
			this._queryInput.setAttribute('value',displayValue);
		}
	},
	
	init : function()
	{
		this._queryInput.Autocompleter = new Ajax.Autocompleter(this.queryInputId, this.querySearchResultId, 'AjaxAction.do', 
										 {
											paramName : 'Name',
											parameters : 'action=searchParameter&parameterId=' + this.parameterId,
											onShow : showResult,
											afterUpdateElement : this.update.bind(this),
											minchars : 0
										 });
	},
	
	update : function(text, li)
	{
		this._hiddenInput.value = li.getAttribute('id');
		this._queryInput.value = li.innerHTML;
		
		$(this.queryInputId).setAttribute('value', li.innerHTML);
		$(this.columnName).setAttribute('value', li.getAttribute('id'));
	}
});

var WList = Class.create(WElement, {
	initialize : function($super, json)
	{
		$super(json);
	},

	clone : function(id)
	{
		var j = this.json;
		j.id = id;
		
		return new WList(j);
	},
	
	setValue : function(value)
	{
		if (this._element != null)
		{
			for (var i=0; i<this._element.options.length; i++)
			{
				var opt = this._element.options[i];
				if (value == null || value.trim().length == 0)
				{
					opt.removeAttribute('selected');
				}
				else
				{
					if (opt.value == value)
					{
						opt.selected = true;
						opt.setAttribute('selected', 'selected');
					}
				}
			}
		}
	},
	
	getElement : function()
	{
		return this._element;
	},
	
	initElement : function(id)
	{
		this.options = this.json.options;
		this._element = document.createElement('select');
		this._element.setAttribute('id', id);
		this._element.setAttribute('name', this.columnName);
		
		if (!this.mandatory)
		{
			var _optionblank = document.createElement('option');
			this._element.appendChild(_optionblank);
		}
		
		for (var i=0; i<this.options.length; i++)
		{
			var opt = this.options[i];
			var _option = document.createElement('option');
			_option.setAttribute('value', opt.value);
			_option.setAttribute('name', opt.name);
			_option.innerHTML = opt.name;
			
			if (opt.value == this.value)
			{
				_option.selected = true;
				_option.setAttribute('selected', 'selected');
			}
			
			this._element.appendChild(_option);
		}
	}
});

var WDate = Class.create(WElement, {
	initialize : function($super, json)
	{
		$super(json);
	},

	clone : function(id)
	{
		var j = this.json;
		j.id = id;
		
		return new WDate(j);
	},
	
	initElement : function(id)
	{
		this._textBox = document.createElement('input');
		this._textBox.setAttribute('type', 'text');
		this._textBox.setAttribute('name', this.columnName);
		this._textBox.setAttribute('id', id);
		this._textBox.setAttribute('value', '');
		
		this._calendarBtn = document.createElement('input');
		this._calendarBtn.setAttribute('type', 'button');
		this._calendarBtn.setAttribute('id', id + 'Btn');
		/*this._calendarBtn.setAttribute('class', 'calendar-icon float-left');*/
		/*this._calendarBtn.setAttribute('class', '');*/
		this._calendarBtn.setAttribute('class', 'calendar-icon');
		
		this._element = document.createElement('div');
		this._element.setAttribute('class', 'datebox');
		this._element.appendChild(this._textBox);
		this._element.appendChild(this._calendarBtn);
	},
	
	setValue : function(value)
	{
		this._textBox.setAttribute('value', value);
	},
	
	setName : function(name)
	{
		this._textBox.setAttribute('name', name);
	},
	
	init : function()
	{
	   /* jQuery( "#" + this._textBox.getAttribute('id')).datepicker({
	      changeMonth: true,
	      changeYear: true,
	      dateFormat : 'yy-mm-dd'
	    });*/
	    
	    Calendar.setup({
			inputField: this._textBox.getAttribute('id'), 
			ifFormat : '%Y-%m-%d',
			showTime : true, 
			button : this._calendarBtn.getAttribute('id'),
			onSelect : this.updateDOMText.bind(this)
		});
	},
	
	updateDOMText : function(calendar, date)
	{
		$(this._textBox.id).setAttribute('value', date);
	}
});

var WCheck = Class.create(WElement, {
	initialize : function($super, json)
	{
		$super(json);
	},

	clone : function(id)
	{
		var j = this.json;
		j.id = id;
		
		return new WCheck(j);
	},
	
	initElement : function(id)
	{
		this._element = document.createElement('input');
		this._element.setAttribute('type', 'checkbox');
		this._element.setAttribute('name', this.columnName);
		this._element.setAttribute('id', id);
		
		if (this.value == 'Y')
		{
			this._element.checked = true;
		}
		else
		{
			this._element.checked = false;
		}
	}
});

var WRange = Class.create(WElement,{
	initialize : function($super, el)
	{
		this.el = el;
		$super(el.json);
	},
	
	initElement : function()
	{
		this.from = this.el.clone(this.id + 'From');
		this.to = this.el.clone(this.id + 'To');
		
		this.from.setName(this.columnName + 'From');
		this.to.setName(this.columnName + 'To');
		
		if (this.value != null)
			this.from.setValue(this.value);
		
		if (this.valueTo != null)
			this.to.setValue(this.valueTo);
		
		this._element = document.createElement('span');
		this._element.appendChild(this.from.getElement());
		this._element.appendChild(this.to.getElement());
	},
	
	clone : function(id)
	{
		var cl = this.el.clone(id);
		return new WRange(cl);
	},
	
	init : function()
	{
		this.from.init();
		this.to.init();
	}
});

var WDateRange = Class.create(WRange,{
	initialize : function($super, el)
	{
		$super(el);
	},
	
	initElement : function()
	{
		this.from = this.el.clone(this.id + 'From');
		this.to = this.el.clone(this.id + 'To');
		 
		this.from.setName(this.columnName + 'From');
		this.to.setName(this.columnName + 'To');
		
		var date = new Date();
		var y = date.getFullYear();
		var m = date.getMonth();
		var firstDay = new Date(y, m, 1);
		var lastDay = new Date(y, m+1, 0);
		
		if (this.value == null && this.valueTo == null)
		{
			var fromMonth = firstDay.getMonth() + 1;
			var fromDate = firstDay.getDate();
			
			var toMonth = lastDay.getMonth() + 1;
			var toDate = lastDay.getDate();
			
			var fromMonth = (fromMonth < 10) ? "0" + fromMonth : fromMonth;
			var fromDate = (fromDate < 10) ? "0" + fromDate : fromDate;
			
			var toMonth = (toMonth < 10) ? "0" + toMonth : toMonth;
			var toDate = (toDate < 10) ? "0" + toDate : toDate;
			
			this.value = firstDay.getFullYear() + '-' + fromMonth + '-' + fromDate;
			this.valueTo = lastDay.getFullYear() + '-' + toMonth + '-' + toDate;
			
			this.displayValue = this.value;
			this.displayValueTo = this.valueTo;
		}
		
		if (this.value != null)
			this.from.setValue(this.value);		
		if (this.valueTo != null)
			this.to.setValue(this.valueTo);
		
		if ((report.processKey == ('RV_POS_OrderHistory')) && (document.URL.toLowerCase().indexOf('pmenuid')== -1))
		{
			this.value = '';
			this.valueTo = '';
			
			this.displayValue = this.value;
			this.displayValueTo = this.valueTo;
			
			this.from.setValue('');
			this.to.setValue('');
		}
		
		this._element = document.createElement('div');
		this._element.setAttribute('class', 'date-style');
		this._element.setAttribute('id', this.columnName + 'Container');
		this._element.appendChild(this.from.getElement());
		this._element.appendChild(this.to.getElement());
		this._element;
	},
	
	init : function()
	{
		this.from.init();
		this.to.init();
	}
});