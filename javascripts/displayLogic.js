var DisplayLogic = Class.create({
	initialize : function()
	{
		this.monitorToDisplayFunction = new Hash();
		this.monitoredToMonitors = new Hash();
		this.monitoredList = new ArrayList();		
	},
	
	setMonitorDisplayLogic : function(monitor, displayLogicFunction)
	{
		this.monitorToDisplayFunction.set(monitor, displayLogicFunction)
	},
	
	addMonitor : function(monitor, listOfItemsMonitored)
	{
		for (var i=0;i<listOfItemsMonitored.length;i++)
		{
			var monitored = listOfItemsMonitored[i];
			var isElementPresentInList = this.monitoredList.contains(monitored);
			if (!isElementPresentInList)
			{
				this.monitoredList.add(monitored);
			}
			var monitors = this.monitoredToMonitors.get(monitored);
			if (monitors == null)
			{
				monitors = new ArrayList();
			}
			monitors.add(monitor);
			this.monitoredToMonitors.set(monitored, monitors);
		}
	},
	
	run : function()
	{
		var disp = new Display(this.monitorToDisplayFunction, this.monitoredToMonitors);
		for (var i=0; i<this.monitoredList.size(); i++)
		{
			var monitored = this.monitoredList.get(i);
			var monitoredEditor = new Editor(monitored);
			var monitoredTag = monitoredEditor.getColumnTag();
			
			disp.display(monitored);
			monitoredTag.onchange = function(e)
			{
				disp.display(this.id);
				
			};
		}		
	}
});

var Display = Class.create({
	initialize : function (monitorToDisplayFunction, monitoredToMonitors)
	{
		this.monitorToDisplayFunction = monitorToDisplayFunction;
		this.monitoredToMonitors = monitoredToMonitors;
	},
	
	isMonitorDisplayed : function(monitor)
	{
		var displayLogicFunction = this.monitorToDisplayFunction.get(monitor);
		return displayLogicFunction();
	},
	
	display : function(monitored)
	{
		var monitors = this.monitoredToMonitors.get(monitored);			
		for (var i=0; i<monitors.size(); i++)
		{
			var monitor = monitors.get(i);
			var monitorEditor = new Editor(monitor);
			var isDisplay = this.isMonitorDisplayed(monitor);
			
			var textOnly = $(monitorEditor.getColumnName() + 'TextOnly');
			if (textOnly != null)
			{
				var textOnlyEditor = new Editor(monitorEditor.getColumnName() + 'TextOnly');
				textOnlyEditor.display(isDisplay);
			}
			else
			{
				monitorEditor.display(isDisplay);
			}
		}
	}	
});

