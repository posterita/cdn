var Editor = Class.create({
	initialize: function(colName)
	{
		this.columnName = colName;
		this.columnTag = $(colName);
		this.tagName = this.columnTag.tagName;
		this.columnTagLabel = $(colName + 'Label');
		this.value = null;
	},
	
	getValue : function()
	{
		if (this.tagName == 'INPUT')
		{
			if (this.columnTag.type == 'checkbox')
			{
				if (this.columnTag.checked == true)
				{
					this.value = 'Y';
				}
				else
				{
					this.value = 'N';
				}
			}	
			else
			{
				this.value = this.columnTag.value;
			}
		}
		else if (this.tagName == 'SPAN')
		{
			this.value = this.columnTag.innerHTML;
		}
		else
		{
			this.value = this.columnTag.value;
		}
		
		return this.value;
	},

	setValue : function(val)
	{
		if (this.tagName == 'INPUT')
		{
			if (this.columnTag.type == 'checkbox')
			{
				this.columnTag.checked = val;
			}			
		}
		else if (this.tagName == 'SPAN')
		{
			this.columnTag.innerHTML = val; 
		}
		
		this.columnTag.value = val;
		this.value = val;
		var textOnly = $(this.columnName + 'TextOnly');
		if (textOnly != null)
			textOnly.innerHTML = this.value;
	},

	
	display : function(isDisplay)
	{
		if (isDisplay == true)
		{
			this.columnTag.style.display = 'inline';
			this.columnTagLabel.style.display = 'inline';
		}
		else
		{
			this.columnTag.style.display = 'none';
			this.columnTagLabel.style.display = 'none';
		}
	},
	
	getColumnTag : function()
	{
		return this.columnTag;
	},
	
	getColumnName : function()
	{
		return this.columnName;
	}
});

var NumberEditor = Class.create({
	initialize: function(colName)
	{
		this.editor = new Editor(colName);
	},
	
	setValue : function (val)
	{
		var value = this.getNumber(val);
		this.editor.setValue(value);
	},
	
	getValue : function()
	{
		var value = this.editor.getValue();
		return this.getNumber(value);
	},
	
	getNumber :  function(val)
	{
		if (val != null)
		{
			var value = String(val);
			var valNo = value.replace(/\,/g,'');
			var num = new Number(valNo).toFixed(2);
			return parseFloat(num);
		}
		else
			return null;
	}
});
