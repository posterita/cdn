// gift card
    var checkGiftCardBalanceDlg = null;
    var giftCardBalanceDlg = null;
    var selectGiftCardTransactionDlg = null;
    var issueGiftCardDlg = null;
    var reloadGiftCardDlg = null;
    var redeemGiftCardDlg = null;
    var refundGiftCardDlg = null;
    
    function checkGiftCardBalance()
    {
    	if(checkGiftCardBalanceDlg == null)
    	{
    		/*initialize*/
    		
    		var giftDlg = jQuery("<div/>",{title:Translation.translate("check.card.balance","Check Card Balance"),class:"row gift-card-popup"});
    		var cardNo = jQuery("<input>",{type:"text", id:"card-no", class:"numeric textField pull-right"});
    		var cardCvv = jQuery("<input>",{type:"text", class:"numeric textField pull-right"});
    		var checkBtn = jQuery("<input>",{type:"button", value:Translation.translate("check.balance","Check Balance"), class:"pull-right", id:"balance-gift-card-ok-button"});
    		
    		/*Event.observe(this.cardNo,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
    		
    		checkBtn.on("click", function(){
    		    var number = cardNo.val();
    		    var cvv = cardCvv.val();
    		    
    		    if(number == null || number == ""){
			    	alert(Translation.translate("invalid.card.number","Invalid Card Number!"));
			    }    			
    			else {
    		    
    		    //jQuery(this).val("Checking ...");     		    
    		    
    		    jQuery.post("GiftCardAction.do",
    		    		{ action: "checkGiftCardBalance", cardNo : number},
    		    		function(json, textStatus, jqXHR){	
    		    			
    		    			if(json == null || jqXHR.status != 200){
    		    				alert(Translation.translate("failed.to.check.balance","Failed to check balance!")); 
    		    				return;
    		    			}  
    		    			
    		    			if(json.error == "Invalid card number!"){
    		    				alert(json.error);
    		    				cardNo.val('');
    		        			cardCvv.val('');
    		    				return;
    		    			}
    		    			jQuery(checkGiftCardBalanceDlg).dialog("close"); 
    		    			displayGiftCard(json);
    		    		},
    					"json").fail(function(){
    						alert(Translation.translate("failed.to.send.check.balance","Failed to send check balance request!"));
    					}); 
    		    
		    		    jQuery(document).ready(function(){
		        			cardNo.val('');
		        			cardCvv.val('');
		        		});
		    		    
    			}
    		});

    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("card.no","Card No")+ ":</label>"));
    		col2.append(cardNo)
    		row.append(col1);
    		row.append(col2);
    		giftDlg.append(row);
    		
    		/*var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>Card CVV</label>"));
    		col2.append(cardCvv)
    		row.append(col1);
    		row.append(col2);
    		giftDlg.append(row);*/
    		
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(checkBtn)
    		row.append(col1);
    		row.append(col2);
    		giftDlg.append(row);
    		    
    		checkGiftCardBalanceDlg = giftDlg;
    	}
    	
    	jQuery(checkGiftCardBalanceDlg).dialog({
    		position: ['center',40],
    		modal:true,
    		open:function(){
	    		var inputs = jQuery(checkGiftCardBalanceDlg).find("input[type|='text']");
	    		for(var i=0; i<inputs.length; i++){
	    			jQuery(inputs[i]).val("");
	    			Event.observe(inputs[i],'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	    		};
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	}).parent().attr('id', 'check-giftcard-balance-popup');
    	
    }
    
    /*display gift balance*/
    
    function displayGiftCard(json)
    {
    	if(giftCardBalanceDlg == null)
    	{
    		var dlg = jQuery("<div/>",{title:Translation.translate("gift.card.details","Gift Card Details"), class:"gift-card-popup gift-card-details"});
    		    		
    		var row = jQuery("<div/>", {class:"row"});
    		row.append(jQuery("<label>" +Translation.translate("balance","Balance")+ ":</label>"));
    		row.append(jQuery("<span/>",{class:"card-balance"}));
    		dlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(jQuery("<label>" +Translation.translate("card.no","Card No")+ ":</label>"));
    		row.append(jQuery("<span/>",{class:"card-no"}));
    		dlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(jQuery("<label>" +Translation.translate("expiry","Expiry")+ ":</label>"));
    		row.append(jQuery("<span/>",{class:"card-expiry"}));
    		dlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(jQuery("<label>" +Translation.translate("date.issued","Date Issued")+ ":</label>"));
    		row.append(jQuery("<span/>",{class:"card-date-issued"}));
    		dlg.append(row);
    		
    		var additional = jQuery("<div/>", {class:"cart-reload-info"});
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(jQuery("<label>" +Translation.translate("last.reloaded.amount","Last Reloaded Amount")+ ":</label>"));
    		row.append(jQuery("<span/>",{class:"card-last-reloaded-amount"}));
    		additional.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(jQuery("<label>" +Translation.translate("last.reloaded.date","Last Reloaded Date")+ ":</label>"));
    		row.append(jQuery("<span/>",{class:"card-last-reloaded-date"}));
    		additional.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(jQuery("<label>" +Translation.translate("last.reloaded.store","Last Reloaded Store")+ ":</label>"));
    		row.append(jQuery("<span/>",{class:"card-last-reloaded-store"}));
    		additional.append(row);
    		
    		dlg.append(additional);
    		/*
    		row = jQuery("<div/>", {class:"row"});
    		row.append(checkBtn);
    		giftDlg.append(row);
    		*/
    		giftCardBalanceDlg = dlg;    		
    	}
    	
    	var cardNoSpan = giftCardBalanceDlg.find(".card-no")[0];
    	jQuery(cardNoSpan).html(json.accountnumber);
    	
    	var cardBalance = giftCardBalanceDlg.find(".card-balance")[0];
    	var balance = json.balance;
    	balance = new Number(balance).toFixed(2);
    	balance = ShoppingCartManager.getCurrencySymbol() + " " + balance;
    	
    	jQuery(cardBalance).html(balance);
    	
    	var cardExpiry = giftCardBalanceDlg.find(".card-expiry")[0];
    	jQuery(cardExpiry).html(json.expiry);
    	
    	var cardDateIssued = giftCardBalanceDlg.find(".card-date-issued")[0];
    	jQuery(cardDateIssued).html(json.dateissued);
    	
    	if(json.reloadedamount){
    		
    		giftCardBalanceDlg.find(".cart-reload-info").show();
    		
    		x = giftCardBalanceDlg.find(".card-last-reloaded-amount")[0];
        	jQuery(x).html(json.reloadedamount);
        	
        	x = giftCardBalanceDlg.find(".card-last-reloaded-date")[0];
        	jQuery(x).html(json.reloadeddate);
        	
        	x = giftCardBalanceDlg.find(".card-last-reloaded-store")[0];
        	jQuery(x).html(json.store);
    	}
    	else
    	{
    		giftCardBalanceDlg.find(".cart-reload-info").hide();
    	}
    	
    	jQuery(giftCardBalanceDlg).dialog({
    		modal:true, 
    		position: ['center',40], width:400
    	}).parent().attr('id', 'giftcard-balance-popup');
    }
    
    /*issue gift card*/
    
    function issueGiftCard()
    {
    	if(issueGiftCardDlg == null)
    	{
    		/*initialize*/
    		var issueDlg = jQuery("<div/>",{title:Translation.translate("issue.card","Issue Card"), class:"gift-card-popup"});
    		var cardNo = jQuery("<input>",{type:"text", id:"issue-gift-card-no" ,class:"textField pull-right numeric"});
    		var amount = jQuery("<input>",{type:"text", id:"issue-gift-card-amount", class:"textField pull-right"});
    		var cancelBtn = jQuery("<input>",{type:"button", value:Translation.translate("cancel","Cancel"),class:"textField pull-right"});
    		var okBtn = jQuery("<input>",{type:"button", value:Translation.translate("ok","Ok"),class:"pull-right", id:"issue-gift-card-ok-button"});
    		
    		okBtn.on("click", function(){
    			var number = cardNo.val();
    			var amt = amount.val();
    			
    			amt = parseFloat(amt);
    			
    			if(number == null || number == ""){
			    	alert(Translation.translate("invalid.card.number","Invalid Card Number!"));
			    }
    			else 
    				if(amt == null || amt == ""){
    				 alert(Translation.translate("invalid.amount","Invalid Amount"));
    				 amount.focus();
    			}
    			 else
    				 if(isNaN(amt) ||(amt < 0.0)){
    					 alert(Translation.translate("invalid.amount","Invalid Amount"));
    					 amount.focus();
    				 }
    			else {
    				
				jQuery.post("GiftCardAction.do",
					  {
					    action : "issueGiftCard",
					    code : number,
					    amount : amt
					  },
					  function(data,status,xhr){
						  
						jQuery(issueGiftCardDlg).dialog("close"); 
						 
					    if(status != "success"){
					    	alert(Translation.translate("failed.to.issue.gift.card","Failed to issue gift card!"));
					    }
					    else
					    {
					    	jQuery('#shopping-cart-container').html(data);	    	
					    }					     
					    
					  });
    			}
    		});
    		
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("card.no","Card No")+ ":</label>"));
    		col2.append(cardNo)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("amt","Amount")+ ":</label>"));
    		col2.append(amount)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(okBtn)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		issueGiftCardDlg = issueDlg;
    	}
    	jQuery(issueGiftCardDlg).dialog({
    		modal:true, 
    		position: ['center',40],
    		open:function(){
	    		var inputs = jQuery(issueGiftCardDlg).find("input[type|='text']");
	    		for(var i=0; i<inputs.length; i++){
	    			Event.observe(inputs[i],'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	    			jQuery(inputs[i]).val("");
	    		}
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	}).parent().attr('id', 'issue-giftcard-popup');
    }
    
   /*reload gift card*/
    
    function reloadGiftCard()
    {
    	if(reloadGiftCardDlg == null)
    	{
    		/*initialize*/
    		var issueDlg = jQuery("<div/>",{title:Translation.translate("reload.card","Reload Card"), class:"gift-card-popup"});
    		var cardNo = jQuery("<input>",{type:"text", id:"card-no", class:"textField pull-right"});
    		var amount = jQuery("<input>",{type:"text", class:"textField pull-right"});
    		var cancelBtn = jQuery("<input>",{type:"button", value:Translation.translate("cancel","Cancel"), class:"textField pull-right"});
    		var okBtn = jQuery("<input>",{type:"button", value:Translation.translate("ok","OK"), class:"textField pull-right", id:"reload-gift-card-ok-button"});
    		
    		okBtn.on("click", function(){
    			var number = cardNo.val();
    			var amt = amount.val();
    			
    			amt = parseFloat(amt);
    			
    			if(number == null || number == ""){
			    	alert(Translation.translate("invalid.card.number", "Invalid Card Number!"));
			    }
    			else 
    				if(amt == null || amt == ""){
    					alert(Translation.translate("invalid.amount","Invalid amount!"));
    				 amount.focus();
    			}
    			 else
    				 if(isNaN(amt) ||(amt < 0.0)){
    					 alert(Translation.translate("invalid.amount","Invalid amount!"));
    					 amount.focus();
    				 }
    			else {
    				
				jQuery.post("GiftCardAction.do",
					  {
					    action : "reloadGiftCard",
					    code : number,
					    amount : amt
					  },
					  function(data,status,xhr){
						  
						console.info(data);
						
					  	jQuery(reloadGiftCardDlg).dialog("close"); 
					  	
						if(status != "success"){
							alert("Failed to reload gift card!");
						}
						else
						{
							jQuery('#shopping-cart-container').html(data);	  	    	
						}
						
					  });
    			}
    		});
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("card.no","Card No")+ ":</label>"));
    		col2.append(cardNo)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("amt","Amount")+ ":</label>"));
    		col2.append(amount)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(okBtn)
    		row.append(col1);
    		row.append(col2);
    		issueDlg.append(row);
    		
    		reloadGiftCardDlg = issueDlg;
    	}
    	jQuery(reloadGiftCardDlg).dialog({
    		modal:true, 
    		position: ['center',40],
    		open:function(){
	    		var inputs = jQuery(reloadGiftCardDlg).find("input[type|='text']");
	    		for(var i=0; i<inputs.length; i++){
					Event.observe(inputs[i],'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		   			jQuery(inputs[i]).val("");
	    		}
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	}).parent().attr('id', 'reload-giftcard-popup');
    }
    
    
    
    
    /* select issue gift, reload or check balance*/ 
    
    function selectGiftCardTransaction() 
    {
    	if(selectGiftCardTransactionDlg == null)
    	{
    		/*initialize*/
    		var selectTransactiondlg = jQuery("<div/>",{title:Translation.translate('gift.card.transactions', 'Gift Card Transactions'), class:"gift-card-popup-transactions", id:"gift-card-popup-transactions"});
    		var issueGiftCardBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.issue.gift.card" , "Issue Gift Card"), class:"", id:"gift-card-popup-transactions-issue-button"});
    		var reloadGiftCardBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.reload.gift.card", "Reload Gift Card"), class:"", id:"gift-card-popup-transactions-reload-button"});
    		var checkGiftCardBalanceBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.check.balance", "Check Balance"), class:"", id:"gift-card-popup-transactions-balance-button"});
    		var redeemGiftCardBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.redeem.gift.card", "Redeem Gift Card"), class:"", id:"gift-card-popup-transactions-redeem-button"});
    		var refundGiftCardBtn = jQuery("<input>",{type:"button", value:Translation.translate("msg.refund.gift.card", "Refund Gift Card"), class:"", id:"gift-card-popup-transactions-refund-button"});
    		
    		issueGiftCardBtn.on("click", function(){
    			jQuery(selectGiftCardTransactionDlg).dialog("close");
    			issueGiftCard();
    		});
    		
    		reloadGiftCardBtn.on("click", function(){
    			jQuery(selectGiftCardTransactionDlg).dialog("close");
    			reloadGiftCard();
    		});
    		
    		checkGiftCardBalanceBtn.on("click", function(){
    			jQuery(selectGiftCardTransactionDlg).dialog("close");
    			checkGiftCardBalance();
    		});
    		
    		redeemGiftCardBtn.on("click", function(){
    			jQuery(selectGiftCardTransactionDlg).dialog("close");
    			redeemGiftCard();
    		});
    		
    		refundGiftCardBtn.on("click", function(){
    			jQuery(selectGiftCardTransactionDlg).dialog("close");
    			refundGiftCard();
    		});
    		
    		var row = jQuery("<div/>", {class:"row"});
    		row.append(issueGiftCardBtn);
    		selectTransactiondlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(reloadGiftCardBtn);
    		selectTransactiondlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(checkGiftCardBalanceBtn);
    		selectTransactiondlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(redeemGiftCardBtn);
    		selectTransactiondlg.append(row);
    		
    		row = jQuery("<div/>", {class:"row"});
    		row.append(refundGiftCardBtn);
    		selectTransactiondlg.append(row);
    		
    		selectGiftCardTransactionDlg = selectTransactiondlg;
    	}
    	
    	jQuery(selectGiftCardTransactionDlg).dialog({
    		modal:true, 
    		position: ['center',40]
    	}).parent().attr('id', 'select-giftcard-actions-popup');
    	
    }
    
    
    /*Redeem gift card*/

    function redeemGiftCard(){
    	
    	if( redeemGiftCardDlg == null ){
    		
    		var dlg = jQuery("<div/>",{title:Translation.translate("redeem.gift.card","Redeem Gift Card"),class:"row gift-card-popup"});
    		var giftCardNo = jQuery("<input>",{type:"text", id:"redeem-gift-card-no", class:"numeric textField pull-right"});
    		var redeemBtn = jQuery("<input>",{type:"button", value:Translation.translate("Redeem","Redeem"), class:"pull-right", id:"redeem-gift-card-ok-button"});
    		
    		redeemBtn.on("click", function(){
    		    var number = giftCardNo.val();
    		    
    		    if(number == null || number == ""){
    		    	alert(Translation.translate("invalid.gift.card.number","Invalid Gift Card Number!"));
    		    }    			
    			else 
    			{
    				jQuery.post("GiftCardAction.do",
    						  {
    						    action : "redeemGiftCard",
    						    giftCardNo : number,
    						    date : DateUtil.getCurrentDate()
    						  },
    						  function(data,status,xhr){
    							  
    							jQuery(redeemGiftCardDlg).dialog("close"); 
    							 
    						    if(status != "success"){
    						    	alert(Translation.translate("failed.to.redeem.gift.card","Failed to redeem gift card!"));
    						    }
    						    else
    						    {
    						    	jQuery('#shopping-cart-container').html(data);	    	
    						    }					     
    						    
    						  });
    	    	}//else
    		});

    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("card.no","Card No")+ ":</label>"));
    		col2.append(giftCardNo)
    		row.append(col1);
    		row.append(col2);
    		dlg.append(row);
    		
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(redeemBtn)
    		row.append(col1);
    		row.append(col2);
    		dlg.append(row);
    		    
    		redeemGiftCardDlg = dlg;		
    		
    	}

    	jQuery(redeemGiftCardDlg).dialog({
    		position: ['center',40],
    		modal:true,
    		open:function(){
    			jQuery('#gift-card-no').val('');
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	}).parent().attr('id', 'redeem-giftcard-popup');
    	
    }
    
    function refundGiftCard()
    {
    	if(refundGiftCardDlg == null)
    	{
    		/*initialize*/
    		
    		var giftDlg = jQuery("<div/>",{title:Translation.translate("refund.card.balance","Refund Card Balance"),class:"row gift-card-popup"});
    		var cardNo = jQuery("<input>",{type:"text", id:"card-no", class:"numeric textField pull-right"});
    		var cardCvv = jQuery("<input>",{type:"text", class:"numeric textField pull-right"});
    		var checkBtn = jQuery("<input>",{type:"button", value:Translation.translate("refund.balance","Refund Balance"), class:"pull-right", id:"refund-gift-card-ok-button"});
    		
    		/*Event.observe(this.cardNo,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
    		
    		checkBtn.on("click", function() {
    		    var number = cardNo.val();
    		    var cvv = cardCvv.val();

    		    if (number == null || number == "") {
    		        alert(Translation.translate("invalid.card.number", "Invalid Card Number!"));
    		    } else {

    		        //jQuery(this).val("Checking ...");   

    		        jQuery.post("GiftCardAction.do", {
    		                action: "refundGiftCard",
    		                giftCardNo: number
    		            },
    		            function(data, status, xhr) {

    		                jQuery(refundGiftCardDlg).dialog("close");

    		                if (status != "success") {
    		                    alert(Translation.translate("failed.to.redeem.gift.card", "Failed to redeem gift card!"));
    		                } else {
    		                    jQuery('#shopping-cart-container').html(data);
    		                }

    		            });

    		    }
    		});

    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>" +Translation.translate("card.no","Card No")+ ":</label>"));
    		col2.append(cardNo)
    		row.append(col1);
    		row.append(col2);
    		giftDlg.append(row);
    		
    		/*var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col1.append(jQuery("<label>Card CVV</label>"));
    		col2.append(cardCvv)
    		row.append(col1);
    		row.append(col2);
    		giftDlg.append(row);*/
    		
    		
    		var row = jQuery("<div/>", {class:"row"});
    		var col1 = jQuery("<div/>", {class:"col-md-4 col-xs-4"});
    		var col2 = jQuery("<div/>", {class:"col-md-8 col-xs-8"});
    		col2.append(checkBtn)
    		row.append(col1);
    		row.append(col2);
    		giftDlg.append(row);
    		    
    		refundGiftCardDlg = giftDlg;
    	}
    	
    	jQuery(refundGiftCardDlg).dialog({
    		position: ['center',40],
    		modal:true,
    		open:function(){
	    		var inputs = jQuery(refundGiftCardDlg).find("input[type|='text']");
	    		for(var i=0; i<inputs.length; i++){
	    			jQuery(inputs[i]).val("");
	    			Event.observe(inputs[i],'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	    		};
    		},
    		close:function(){
    			Keypad.hide();
    		}
    	}).parent().attr('id', 'refund-giftcard-popup');
    	
    }
    
    
    /*keypad panel*/
    var Keypad = {
    		keyPad:null,
    		initialized:false,
    		textField:null,
    		
    		getKeyPad : function(){
    			return $('keypad-popup-panel');
    		},
    		
    		init:function(){
    			this.keyPad = $('keypad-popup-panel');
    			if(this.keyPad == null) return;
    			
    			this.keyPad.style.zIndex = 2000000;
    			this.initialized = true;
    			
    			this.key9 = $('key9');
    			this.key8 = $('key8');
    			this.key7 = $('key7');
    			this.key6 = $('key6');
    			this.key5 = $('key5');
    			this.key4 = $('key4');
    			this.key3 = $('key3');
    			this.key2 = $('key2');
    			this.key1 = $('key1');
    			this.key0 = $('key0');
    			
    			this.keyClose = $('keyClose');
    			this.keyDelete = $('keyDelete');
    			this.keyDot = $('keyDot');
    			this.keyEnter = $('keyEnter');
    			this.keyMinus = $('keyMinus');
    			
    			/* add behaviour */	
    			for(var field in this){
    				var fieldContents = this[field];
    				
    				if (fieldContents == null || typeof(fieldContents) == "function") {
    					continue;
    			    }
    			    
    			    if(fieldContents.type == 'button'){
    			    	//register click handler
    			    	fieldContents.onclick = this.keyHandler.bindAsEventListener(this);	
    			    }
    			}
    			    
    			var inputs = $$('input.numeric');
    			for(var i=0; i<inputs.length; i++){
    				Event.observe(inputs[i],'click',this.clickHandler.bindAsEventListener(this),false);
    				Event.observe(inputs[i],'keyup',this.keyUpHandler.bindAsEventListener(this),false);				
    			}
    			
    			this.initialized = true;
    		},
    		
    		keyUpHandler:function(e){
    			
    			var element = Event.element(e);	
    			
    			var re = /[^0-9\.\-]/g;
    		    if(re.test(element.value))
    		    {
    		    	element.value = element.value.replace(re, '');
    		    }

    		},
    		
    		clickHandler:function(e){
    			
    			if((navigator.userAgent.match(/iPhone/i)) || 
    					(navigator.userAgent.match(/iPad/i)) ||
    					 (navigator.userAgent.match(/iPod/i))) {
    				return;
    			}
    			
    			var element = Event.element(e);		
    			if(!this.initialized) this.init();
    			this.textField = element;	
    			
    			var offset = Element.viewportOffset(element);
    			var width = $(element).getWidth();
    			
    			var bottom = offset.top + 10 + this.keyPad.getHeight();
    			var right = offset.left + width + 40 + this.keyPad.getWidth();
    			
    			var screenWidth = ViewPort.getWidth();
    			var screenHeight = ViewPort.getHeight();
    			
    			var topOffset = 10;
    			var leftOffset = width + 40;
    			
    			if(right > screenWidth){
    				leftOffset = -40 - this.keyPad.getWidth();
    			}
    			
    			if(bottom > screenHeight){
    				topOffset = bottom - screenHeight - this.keyPad.getHeight();
    			}			
    			
    			this.keyPad.style.top = (offset.top + topOffset) + 'px';
    			this.keyPad.style.left = (offset.left + leftOffset) + 'px';
    			this.keyPad.style.display='block';
    			
    			this.deleteAll = true;
    		},
    		
    		hide:function(){
    			this.textField = null;
    			var keyPad = this.getKeyPad();
    			if(keyPad != null) keyPad.hide();
    		},
    		
    		keyHandler:function(e){
    			var element = Event.element(e);			
    			var value = this.textField.value;
    			
    			switch(element){
    				case this.keyClose : 
    					this.hide();
    					return;
    					break;
    					
    				case this.keyDelete :
    					if(this.deleteAll == true){
    						value = '';
    						this.textField.value = value;
    						this.deleteAll = false;
    						return;
    					}
    					
    					if(value.length > 0)
    						value = value.substring(0,value.length-1);
    					break;
    					
    				case this.keyDot :
    					value = value + '.';
    					break;
    				
    				case this.key9 :
    					value = value + '9';
    					break;
    					
    				case this.key8 : 
    					value = value + '8';
    					break;
    					
    				case this.key7 : 
    					value = value + '7';
    					break;
    					
    				case this.key6 : 
    					value = value + '6';
    					break;
    					
    				case this.key5 : 
    					value = value + '5';
    					break;
    					
    				case this.key4 : 
    					value = value + '4';
    					break;
    					
    				case this.key3 : 
    					value = value + '3';
    					break;
    					
    				case this.key2 : 
    					value = value + '2';
    					break;
    					
    				case this.key1 : 
    					value = value + '1';
    					break;
    					
    				case this.key0 : 
    					value = value + '0';
    					break;
    					
    				case this.keyEnter :
    					
    					/*
    					var evt = document.createEvent("KeyboardEvent");
    					evt.initKeyEvent ("keyup", true, true, window, 0, 0, 0, 0,13, 13);					
    					this.textField.dispatchEvent(evt);	
    					*/
    					
    					switch (this.textField) {
    					case $('quantity-texfield'):
    						if(!ShoppingCartManager.isShoppingCartEmpty()){
    							var qty = parseFloat(this.textField.value);							
    							if(isNaN(qty)){
    								alert(Translation.translate("invalid.qty","Invalid Qty!"));
    								this.textField.selectAll();								
    								return;					
    							}
    							ShoppingCartManager.updateQty(qty);							
    						}
    						break;

    					default:
    						break;
    					}
    										
    					this.hide();
    					return;
    		
    				case this.keyMinus : 
    					if(value.indexOf('-') == -1){
    						value = '-' + value;
    					}
    					else{
    						value = value.substring(1);
    					}
    					
    					break;
    					
    				default: break;					
    			}
    			
    			this.deleteAll = false;
    						
    			this.textField.value = value;
    			var onkeyup = this.textField.onkeyup;
    			if(onkeyup){
    				var event = {keyCode:65};
    				onkeyup.call(this.textField,event);
    			}
    		}
    };
