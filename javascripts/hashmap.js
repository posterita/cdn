function HashMap()
{
	var _keys = [];
	var _data = {};
	
	this.size = function() {
		return _keys.length;
	}
	this.get = function(key) {
		return _data[key] || null;
	}
	this.remove = function(key) {
		var index = _keys.indexOf(key);
		if (index != -1) {
			_keys.splice(index, 1);
			_data[key] = null;
		}
	}
	this.toArray = function() {
		var r = [];
		for(var k in _data)
			if (_data[k]) r.push(_data[k]);
		return r;
	}
	this.put = function(key, value) {
		if (_keys.indexOf(key) == -1)
			_keys.push(key);
		_data[key] = value;
	}
	this.clear = function() {
		_keys = [];
		_data = {};
	}
	this.hasItem = function (key) {
		return (_keys.indexOf(key) != -1);
	}
	this.limit = function(start, num) {
		var sub = _keys.slice(start, num),
			result = [];
		for(var i=0,len=sub.length; i<len; i++) {
			result.push(_data[sub[i]]);
		}
		return result;
	}
	this.each = function(fn) {
		if (typeof fn != 'function') {
			return false;
		} else {
			var len = this.size();
			for(var i=0; i<len; i++) {
				var k = _keys[i];
				fn(k, _data[k], i);
			}
		}
	}
	/* backward compatibility*/
	
	this.set = this.put;
	this.unset = this.remove;
	this.values = this.toArray;
	
	this.keys = function(){
		return _keys;
	}
}