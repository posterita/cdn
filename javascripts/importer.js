var IMPORTER = null;
var SKIP = null;
var enableCellSelection = true;
var url = null;

var isValid = false;
var CSV_DATA = null;

var init = function(){
	
	if(IMPORTER == null){
		alert("Failed to initialise! Parameter 'IMPORTER' has not been initialised.");
	}
	
	jQuery('#loadBtn').click(function(){
		
		CSV_DATA = [];
		
		var filename = jQuery('input#fileInput').val();			
			
		if( filename.endsWith(".csv") == "" ){
			alert(Translation.translate('please.select.a.csv.file','Please select a .csv file'));
			return;
		}
		
		var limit = 500;
		if(IMPORTER == "ProductImporter"){
			limit = 10000;
		}
		
		jQuery('input#fileInput').parse({
			config: {
				
				skipEmptyLines: true,
				
				preview: limit +1,
				
				worker: true,
				
				step: function(results, parser) {
					/*
					console.log("Row data:", results.data);
					console.log("Row errors:", results.errors);
					*/
					CSV_DATA.push( results.data );
				},
				
				complete: function(results, file) {
					/*console.log("Parsing complete:", results, file);*/
					
					/*
					if(results.errors.length > 0){
						alert(results.errors[0].message);
						return;
					}					
					
					if(results.data.length >= limit){
						alert('Your csv file contains more than ' + limit +' lines!');						
						return;
					}
					
					CSV_DATA = results.data;
					*/
					
					if(CSV_DATA.length > limit + 1){
						alert('Your csv file contains more than ' + limit +' lines!');						
						return;
					}
					
					displayCSVData();
					isValid = true;
				},
				
				error: function(error, file){
					console.log("Parsing error:", error, file);					
				}
			}
		});
		
		function displayCSVData(){			
			
			var data = CSV_DATA;
			
			var select = "<select name='columns'>";
			select += "<option value=''></option>";
			
			var column;
			for(var i=0; i<columnsList.length; i++){
				column = columnsList[i];
				
				select += "<option value='" + column["name"] + "'>" + column["printedName"] + "</option>";
			}			
			select += "</select>";
			
			var columnCount = 0;
			if(data.length > 0){
				columnCount = data[0].length;
			}
			
			var html = "<table>";
			
			html += "<tr>";	
			for(var m=0; m < columnCount; m++){
				html += "<th>" + select + "</th>";
			}
			html += "</tr>";
			
			var row;
			
			for(var k=0; k<data.length && k<10; k++){
				
				row = data[k];
				
				if(row.length != columnCount) continue;
				
				html += "<tr>";	
				for(var j=0; j < columnCount; j++){
					html += "<td>" + row[j] + "</td>";
				}
				html += "</tr>";
			}
			
			jQuery("#csv-table").html( html );
			
			//set columns
			var selects = jQuery("#csv-table select");
			
			var noOfMatch = 0;
			
			for(var n=0; n<selects.length; n++){
				var select = selects[n];
				var header = data[0][n].trim();
				
				var options = select.options;
				var option;
				
				for(var p=1; p<options.length; p++){
					option = options[p];
					if( option.label.toUpperCase().replace(/\s/g, '') == header.toUpperCase().replace(/\s/g, '')){
						
						console.log(option.value);
						
						select.selectedIndex = p;
						
						noOfMatch ++;
						break;
					}
				}
			}
			
			//data contains header
			jQuery("#skipFirstRowContainer").show();			
			jQuery("#skipFirstRow").prop( "checked", (noOfMatch > 0) );
			
		}	
		
	});

	if($('default-button'))
	{	
		$('default-button').onclick = function(e){			
			new DefaultsPopUp().show();
			$('buttons-container').show();
		};
	}
			
	/*
	$('import-button').onclick = function(e){			
		new Dialog("",{closeable : false}).show();		
		$('importer-form').submit();		
	};
	*/
	
};

function validateBeforeImport(){
	
	var selects = jQuery("#csv-table select");
	var columnIndex = {};
	var select, columnName;
	
	for (var i = 0; i < selects.length; i++) {
		select = jQuery(selects[i]);
		columnName = select.val().trim();
		
		/* check duplicate columns. If column selected twice */
		if (typeof columnIndex[columnName] == "undefined") {}
		else {
			alert("Column: " + columnName + " is selected more than once!");
			return false;
		}
		if (columnName.length > 0) {
			columnIndex[columnName] = i;
		}
	}
	
	var selectedColumns = Object.keys(columnIndex);
	
	/* check if columns selected */
	if (selectedColumns.length == 0) {
		alert("No column selected!");
		return false;
	}
	
	var column, columnName, columnMap = {};
	
	/* build column map */
	for (var i = 0; i < columnsList.length; i++) {
		column = columnsList[i];
		columnName = column["name"];
		columnMap[columnName] = column;
	}
	
	/* check if mandatory columns selected */
	for (var i = 0; i < columnsList.length; i++) {
		
		column = columnsList[i];
		columnName = column["name"];
		var index = columnIndex[columnName];
		
		if (column.mandatory == true) {
			if (typeof index == "undefined") {
				alert("Column: " + columnName + " is mandatory!");
				return false;
			}
			else {
				/* check if mandatory columns values */
				for (var j = 0; j < CSV_DATA.length; j++) {
					if (CSV_DATA[j][index].trim().length == 0) {
						alert("Row: " + (j + 1) + ", Column: " + columnName + ", Value is required!");
						return false;
					}
				}
			}
		}
	}
	
	/* validate column type */
	/* text or numeric */
	var hasHeader = jQuery("#skipFirstRow").prop("checked");
	
	for (var i = 0; i < selectedColumns.length; i++) {
		
		var columnName = selectedColumns[i];
		var column = columnMap[columnName];
		var index = columnIndex[columnName];
		var dataType = column["dataType"];
		var cellValue;
		if (dataType == "numeric") {
			var startAt = hasHeader ? 1 : 0;
			for (var j = startAt; j < CSV_DATA.length; j++) {
				cellValue = CSV_DATA[j][index].trim();
				
				if(cellValue == "null"){
					CSV_DATA[j][index] = 0;
				}
				else
				{
					if (cellValue.length == 0 || isNaN(cellValue)) {
						alert("Row: " + ( j + 1 ) + ", Column: " + columnName + ", Invalid value! Value not a number.");
						return false;
					}
				}			
				
			}
		}
		else
		{
			if(cellValue == "null"){
				CSV_DATA[j][index] = "";
			}
		}
	}
	
	return true;
}

function importData(){
	
	var isValid = validateBeforeImport();
	
	if(!isValid) return;
	
	new Dialog("Importing. Please wait ...",{closeable : false}).show();	
	
	var form = document.getElementById("loaderFrm");
	form.submit();
}

Event.observe(window,'load',init,false);
