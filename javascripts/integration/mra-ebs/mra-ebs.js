(function(){
	var app = angular.module('app', []); 
	
	app.service('MraEbsService', function($http) {

		this.getConfiguration = function() {
			return $http.get("MraEbsAction.do?action=getConfiguration");
		};
		
		this.saveConfiguration = function(configuration) {
			return $http({
		        method: 'POST',
		        url: 'MraEbsAction.do',
		        data: jQuery.param({"action":"saveConfiguration", "configuration": JSON.stringify(configuration)}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		};
		
		this.testEbs = function({username, password, ebsMraId, areaCode}){
			return $http({
		        method: 'POST',
		        url: 'MraEbsAction.do',
		        data: jQuery.param({"action":"testEbs", username, password, ebsMraId, areaCode}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		};
		
		this.runTestScenarios = function(terminal_id){
			return $http({
		        method: 'POST',
		        url: 'MraEbsAction.do',
		        data: jQuery.param({"action":"runTestScenarios", "terminal_id":terminal_id}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		};
		
		this.reProcessFailedOrders = function(terminal_id){
			return $http({
		        method: 'POST',
		        url: 'MraEbsAction.do',
		        data: jQuery.param({"action":"reProcessFailedOrders", "terminal_id":terminal_id}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		};

	});
	
	app.controller("MraEbsConfigurationController", function($scope, $window, $timeout, MraEbsService) {
		
		$scope.loading = true;
		
		$scope.init = function(){
			
			MraEbsService.getConfiguration().then((response) => {
				
					var data = response.data;			
					$scope.configuration = data.configuration;
					$scope.pos = data.pos;
					$scope.failedCounts = data.failedCounts;
										
					initBtns();
					
					/*failed orders*/
					$scope.checkForFailedOrders(data.failedCounts);
				
				}, (err) => {
					alert("Failed to load MRA EBS configuration! Error: " + err);
				}
				
			).finally(() => {
				$scope.loading = false;
			});
		};
		
		var initBtns = function(){
			
			var btns = jQuery('.mra-ebs-save-button');
			btns.show();
			btns.click(function(){
				
				if(!validate()){
					return;
				}
				
				//new Dialog("Please wait ...").show();
				MraEbsService.saveConfiguration($scope.configuration).then(function(response){
					
					if(response.data.saved){
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_SUCCESS,
			                title: 'MRA EBS Configuration',
			                message: 'Configuration was successfully save.',
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
					}
					else
					{
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_DANGER,
			                title: 'MRA EBS Configuration',
			                message: 'Failed to save configuration!',
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
					}						 
					
					
				}, function(response) {
			        // This function will be called on error
			        console.log(response.statusText);
			        
			        BootstrapDialog.show({
		                type: BootstrapDialog.TYPE_DANGER,
		                title: 'MRA EBS Configuration',
		                message: 'Failed to save configuration! Reason: ' + response.statusText,
		                buttons: [{
		                    label: 'Ok',
		                    action: function(dialogRef){
		                        dialogRef.close();
		                    }
		                }]
		            });
			    });
				
			});
		};
		
		$timeout(function(){
			$scope.init();
		});
		
		$scope.testEbs = function(terminal_id){
			
			if(!validate()) return;
			
			var configuration = $scope.configuration;
			
			var username = configuration.account.username;
			var password = configuration.account.password;
			
			//validate terminal
			var terminal = configuration.terminal[terminal_id];
			var t = $scope.pos.stores.flatMap(x => x.terminals).find(x => x.terminal_id == terminal_id);
			var terminal_name = t.terminal_name;
			
			if(!terminal){
				alert(`EbsMraId for terminal - ${terminal_name} is required!`);
				return;
			}
			
			var ebsMraId = terminal.ebsmraid;
			if(!ebsMraId){
				alert(`EbsMraId for terminal - ${terminal_name} is required!`);
				return;
			}
			
			var areaCode = terminal.areacode;
			if(!areaCode){
				alert(`AreaCode for terminal - ${terminal_name} is required!`);
				return;
			}

			MraEbsService.testEbs({
				
				username, 
				password, 
				ebsMraId, 
				areaCode
				
			}).then((response) => {

				var data = response.data;

				if(data.status == 'ERROR'){
					
					BootstrapDialog.show({
		                type: BootstrapDialog.TYPE_DANGER,
		                title: 'Authenticate EBS',
		                message: 'Failed to authenticate EBS! Error: ' + data.errors.join(','),
		                buttons: [{
		                    label: 'Ok',
		                    action: function(dialogRef){
		                        dialogRef.close();
		                    }
		                }]
		            });
					
				}
				else if(data.status == 'SUCCESS')
				{
					$timeout(() => {
						$scope.configuration.terminal[terminal_id].status = 'AUTHENTICATED';
					});
					
					
					BootstrapDialog.show({
		                type: BootstrapDialog.TYPE_SUCCESS,
		                title: 'Authenticate EBS',
		                message: 'EBS was successfully authenticated.',
		                buttons: [{
		                    label: 'Ok',
		                    action: function(dialogRef){
		                        dialogRef.close();
		                    }
		                }]
		            });
				}
				else
				{
					alert(`Invalid status {data.status} was returned!`);
				}

			}, (err) => {
				alert("Failed to autenticate EBS! Error: " + err);
			}

			).finally(() => {

			});
		};
		
		$scope.runTestScenarios = function(terminal_id){
			if(!validate()) return;
			
			var configuration = $scope.configuration;
			
			var username = configuration.account.username;
			var password = configuration.account.password;
			
			//validate terminal
			var terminal = configuration.terminal[terminal_id];
			var t = $scope.pos.stores.flatMap(x => x.terminals).find(x => x.terminal_id == terminal_id);
			var terminal_name = t.terminal_name;
			
			if(!terminal){
				alert(`EbsMraId for terminal - ${terminal_name} is required!`);
				return;
			}
			
			var ebsMraId = terminal.ebsmraid;
			if(!ebsMraId){
				alert(`EbsMraId for terminal - ${terminal_name} is required!`);
				return;
			}
			
			var areaCode = terminal.areacode;
			if(!areaCode){
				alert(`AreaCode for terminal - ${terminal_name} is required!`);
				return;
			}
			
			MraEbsService.runTestScenarios(terminal_id).then(function(response){
					
					if(response.data.success){
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_SUCCESS,
			                title: 'Run EBS Tests',
			                message: 'Successfully ran all test scenarios.',
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
					}
					else
					{
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_DANGER,
			                title: 'Run EBS Tests',
			                message: 'Failed to ran all test scenarios! Reason: ' + response.data.reason,
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
					}						 
					
					
				}, function(response) {
			        // This function will be called on error
			        console.log(response.statusText);
			        
			        BootstrapDialog.show({
		                type: BootstrapDialog.TYPE_DANGER,
		                title: 'Run EBS Tests',
		                message: 'Failed to ran all test scenarios! Reason: ' + response.statusText,
		                buttons: [{
		                    label: 'Ok',
		                    action: function(dialogRef){
		                        dialogRef.close();
		                    }
		                }]
		            });
			    });
		};
		
		$scope.reprocessFailedOrders = function(terminal){
		
			if(terminal.offline == 'Y'){
			
				BootstrapDialog.show({
	                title: 'Reprocess Failed Invoices',
	                message: 'Terminal is an offline terminal. You must use the offline client to reprocess the failed invoices.',
	                buttons: [{
	                    label: 'Ok',
	                    action: function(dialogRef){
	                        dialogRef.close();
	                    }
	                }]
	            });
	            
	            return;
			}
			
			MraEbsService.reProcessFailedOrders(terminal.terminal_id).then(function(response){
			
					let status = response.data.status;
					
					if( status == "SUCCESS" ){
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_SUCCESS,
			                title: 'Reprocess Failed Invoices',
			                message: 'Successfully reprocessed all failed invoices.',
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }],
			                onhide: function(dialogRef){
				                $timeout(function(){
									terminal.failedCounts = 0;
								});
				            }
			            });			            
			            
					}
					else
					{
						let message = "";
						
						//check errorMessages
						if(response.data.errorMessages){
							message = response.data.errorMessages;
						}
						else
						{
							//check fiscalisedInvoices
							let fiscalisedInvoices = response.data.fiscalisedInvoices;
							
							let proccessed = fiscalisedInvoices.length;
							let successCount = 0;
							let failedCount = 0;
							
							fiscalisedInvoices.forEach(x => x.status == 'SUCCESS' ? ++ successCount : ++ failedCount);
							
							message = `Processed:${proccessed}. Success:${successCount}, Error:${failedCount}`;
						}
					
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_DANGER,
			                title: 'Reprocess Failed Invoices',
			                message: 'Failed to reprocess failed invoices! Reason: ' + message,
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
					}						 
					
					
				}, function(response) {
			        // This function will be called on error
			        console.log(response.statusText);
			        
			        BootstrapDialog.show({
		                type: BootstrapDialog.TYPE_DANGER,
		                title: 'Reprocess Failed Invoices',
		                message: 'Failed to reprocess failed invoices! Reason: ' + response.statusText,
		                buttons: [{
		                    label: 'Ok',
		                    action: function(dialogRef){
		                        dialogRef.close();
		                    }
		                }]
		            });
			    });
			
		};
		
		$scope.checkForFailedOrders = function(data){
		
			if(data.length == 0) return;
			
			let counts = 0;
			
			for(let i=0; i< data.length; i++){
				counts += data[i].count;
			}
						
			let msg = counts == 1 ? `<h3>1 invoice was not fiscalised!</h3>` :`<h3>${counts} invoices were not fiscalised!</h3>`;
			
				BootstrapDialog.show({
	                type: BootstrapDialog.TYPE_DANGER,
	                title: 'EBS Error',
	                message: jQuery(`<div>${msg}</div>`),
	                buttons: [
	                	{
	                    	label: 'Ok',
	                    	action: function(dialogRef){
	                        	dialogRef.close();
	                    	}
	                	}
	                ]
	         });
	         
	         
			$scope.updatedFailsCount(data);
		
		};
		
		$scope.updatedFailsCount = function(data){
		
			/*add failed counts to terminals*/
	         
	         const map1 = new Map();			 
	         
	         for(let i=0; i<data.length; i++){
	         
	         	let {u_posterminal_id, count} = data[i];
	         	
	         	map1.set(u_posterminal_id, count);
	         }
	         
	         let stores = $scope.pos.stores;
	         
	         for(let i=0; i<stores.length; i++){
	         
	         	let store = stores[i];
	         	
	         	for(let j=0; j<store.terminals.length; j++){
	         	
	         		let terminal = store.terminals[j];
	         		
	         		terminal.failedCounts = map1.get(terminal['terminal_id']) || 0;
	         	
	         	}
	         
	         }
	         
	         console.log($scope.pos.stores);
		
		};
		
		//validations
		let validations = [
			[
				"configuration.account.username",
				"Username is required!"
			],
			[
				"configuration.account.password",
				"Password is required!"
			],
			[
				"configuration.seller.name",
				"Name of Seller is required!"
			],
			[
				"configuration.seller.tan",
				"VAT Registration Number is required!"
			],
			[
				"configuration.seller.brn",
				"Business Registration Number (BRN) is required!"
			],
			[
				"configuration.seller.businessAddr",
				"Business Address is required!"
			]
		];
		
		function validate(){
			
			//validate seller account
			for(var v of validations){
				if(typeof eval('$scope.' + v[0]) === 'undefined'){
				
					alert(v[1]);
					
					return false;
				}
			}
			
			
			//validate tax
			for(var posTax of $scope.pos.taxes){
				
				if(typeof $scope.configuration.tax[posTax.c_tax_id] === 'undefined'){
		
					alert(`Tax mapping for tax - ${posTax.name} is required!`);
					
					return false;
				}
			}
			
			//check for duplicate ebsmraids
			var ebsmraids = [];
			
			//validate terminals
			for(var store of $scope.pos.stores){
				
				var store_name = store.store_name;
				
				for(var terminal of store.terminals){					
					
					var terminal_conf = $scope.configuration.terminal[terminal.terminal_id];
					
					if(terminal_conf && terminal_conf.isactive == 'true'){
						
						var terminal_name = terminal.terminal_name;
						
						if(!terminal_conf.ebsmraid){
							
							alert(`EbsMraId for terminal - ${terminal_name} of store - ${store_name} is required!`)
							
							return false;
						}
						else
						{
							ebsmraids.push(terminal_conf.ebsmraid);
						}
						
						if(!terminal_conf.areacode){
							
							alert(`AreaCode for terminal - ${terminal_name} of store - ${store_name} is required!`)
							
							return false;
						}
					}					
					
				}
			}
			
			//check for duplicate ebsmraids
			if(ebsmraids.length > 1){	
				
				let set = new Set();
				let id;
				
				for(var i=0; i<ebsmraids.length; i++){
					id = ebsmraids[i];
					
					if(!set.has(id)){
						set.add(id);
					}
					else
					{
						alert(`EbsId - ${id} has been used more than once!`);						
						return false;
					}
				}
				
			}
			
			return true;
		}
		
		
	});
	
	/*
	 
	 {"configuration":{"seller":{"tan":"20351590","name":"Posterita POS","businessAddr":"Coromandel","brn":"C07062336"},"isactive":"true","tax":{"10011755":"TC01","10011756":"TC03"},"terminal":{"10006545":{"isactive":"true","ebsmraid":"169441124153300YB3BMR161","areacode":"100"}},"account":{"password":"P05t3r1t@","username":"Posterita"}},"pos":{"stores":[{"store_id":10006331,"store_name":"Dummy1 HQ","terminals":[{"terminal_name":"Online","terminal_id":10007986},{"terminal_name":"Terminal 1","terminal_id":10006545},{"terminal_name":"Terminal 2","terminal_id":10008102},{"terminal_name":"Terminal 3","terminal_id":10008103}]},{"store_id":10006332,"store_name":"Dummy1 Mall","terminals":[{"terminal_name":"Dummy1 Mall default","terminal_id":10006546}]}],"taxes":[{"rate":15,"name":"Sales Tax","c_tax_id":10011755},{"rate":0,"name":"Tax Exempt","c_tax_id":10011756}]}}
	 
	 */
	
}());