(function(){
	var app = angular.module('app', []); 
		
	app.controller("QuickbooksSettingsController", function($scope, $window, $timeout, QuickbooksService) {				
				
		$scope.mappings = {
				sendEndOfDay : false,
				taxes : {},
				payments : {},
				terminals : {}
		};
						
		$scope.config = {};
				
		QuickbooksService.getQuickbooksConfiguration().then(function(response) {
			
			var data = response.data;				
			
			data.quickbooks.companyInfo = data.quickbooks.companyInfo.QueryResponse.CompanyInfo[0];
			data.quickbooks.taxes = data.quickbooks.taxes.QueryResponse.TaxCode;
			data.quickbooks.paymentMethods = data.quickbooks.paymentAccounts.QueryResponse.PaymentMethod;
			
			var _mappings = data.quickbooks.mappings;	
			
			let defaultStoreList = [{
				store_name : 'All',
				store_id : 0
			}];
			
			//map tax by store
			_mappings.mapTaxByStore = _mappings.mapTaxByStore || 'false';
			
			$scope.onMapTaxByStoreChange = function(){
				
				$scope.taxStoreList = $scope.mappings.mapTaxByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				
				//reset tax mappings
				$scope.mappings.taxes = {};
			};
			
			//map payments by store
			_mappings.mapPaymentByStore = _mappings.mapPaymentByStore || 'false';
			
			$scope.onMapPaymentByStoreChange = function(){
				
				$scope.paymentStoreList = $scope.mappings.mapPaymentByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				
				//reset payment mappings
				$scope.mappings.payments = {};
			};
			
			$scope.config = data;
			
			$timeout(function(){
				
				$scope.mappings = {..._mappings};				
				
				//render store lists for both taxes and payments
				$scope.taxStoreList = _mappings.mapTaxByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				$scope.paymentStoreList = _mappings.mapPaymentByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;				
				
				$scope.mappings.taxes = {};
				$scope.mappings.payments = {};
				
				$timeout(function(){
					//populate taxes after store list is rendered
					$scope.mappings.taxes = {..._mappings.taxes};
					//populate payments after store list is rendered
					$scope.mappings.payments = {..._mappings.payments};
				}, 100);
				
				var btns = jQuery("button.quickbooks-save-button");
				btns.show();
				btns.click(function(){
					//new Dialog("Please wait ...").show();
					QuickbooksService.saveMappings($scope.mappings).then(function(response){
						
						if(response.data.saved){
							BootstrapDialog.show({
				                type: BootstrapDialog.TYPE_SUCCESS,
				                title: 'Quickbooks Configuration',
				                message: 'Configuration was successfully save.',
				                buttons: [{
				                    label: 'Ok',
				                    action: function(dialogRef){
				                        dialogRef.close();
				                    }
				                }]
				            });
						}
						else
						{
							BootstrapDialog.show({
				                type: BootstrapDialog.TYPE_DANGER,
				                title: 'Quickbooks Configuration',
				                message: 'Failed to save configuration!',
				                buttons: [{
				                    label: 'Ok',
				                    action: function(dialogRef){
				                        dialogRef.close();
				                    }
				                }]
				            });
						}						 
						
						
					}, function(response) {
				        // This function will be called on error
				        console.log(response.statusText);
				        
				        BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_DANGER,
			                title: 'Quickbooks Configuration',
			                message: 'Failed to save configuration! Reason: ' + response.statusText,
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
				    });
					
				});
				
			});
		}, function(response) {
			
			jQuery('#progress-container').html(`<h3 class='text-danger'>${response.data.error}</h3>`);
			
			BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'Quickbooks Configuration',
                message: 'Failed to load settings! Reason: ' + response.data.error,
                buttons: [{
                    label: 'Ok',
                    action: function(dialogRef){
                        dialogRef.close();
                    }
                }]
            });
		});
		
		$scope.disconnect = function(){
			
			BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'Quickbooks Configuration',
                message: 'Do you want to disconnect Quickbooks?',
                buttons: [{
                    label: 'Yes',
                    action: function(dialogRef){
                        dialogRef.close();
                        _disconnect();
                    }
                },
                {
                    label: 'No',
                    action: function(dialogRef){
                        dialogRef.close();
                    }
                }]
            });
			
			var _disconnect = function(){

				QuickbooksService.disconnect().then(function(response){
					
					if(response.data.disconnected) {
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_SUCCESS,
			                title: 'Quickbooks Configuration',
			                message: 'App was successfully disconnected',
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                        $window.location.href = "QuickbooksAction.do?action=load";
			                    }
			                }]
			            });
					}
					else {
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_DANGER,
			                title: 'Quickbooks Configuration',
			                message: 'Failed to disconnect! Reason: ' + response.data.error,
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
					}
					
					
					
				}, function(err){
					BootstrapDialog.show({
		                type: BootstrapDialog.TYPE_DANGER,
		                title: 'Quickbooks Configuration',
		                message: 'Failed to disconnect! Reason: ' + err,
		                buttons: [{
		                    label: 'Ok',
		                    action: function(dialogRef){
		                        dialogRef.close();
		                    }
		                }]
		            });
				});			
			};
		}
	});

	app.service('QuickbooksService', function($http) {				

		this.getQuickbooksConfiguration = function() {
			return $http.get("QuickbooksAction.do?action=getQuickbooksConfiguration");
		};
		
		this.disconnect = function() {
			return $http.get("QuickbooksAction.do?action=disconnect");
		};
		
		this.saveMappings = function(mappings) {
			return $http({
		        method: 'POST',
		        url: 'QuickbooksAction.do',
		        data: jQuery.param({"action":"saveMappings", "mappings": JSON.stringify(mappings)}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		};

	});
}());
