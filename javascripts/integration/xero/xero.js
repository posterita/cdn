(function(){
	var app = angular.module('app', []); 
	
	app.controller("XeroAllowAccessController", function($scope, $window, XeroService) {

		var tenants = [];
		$scope.selected = null;
		$scope.tenants = tenants;

		XeroService.getTenants().then(function(response) {
			$scope.tenants = response.data;
		});

		$scope.allowAccess = function() {
			if ($scope.selected == null) {
				alert("Select an organisation");
				return;
			}

			XeroService.setTenantId($scope.selected.tenantId).then(function(response) {
				var result = response.data;

				if (!result.saved) {
					alert("Failed to allow access to organisation!");
					return;
				}

				$window.location.href = "xero-settings.do";

			});
		}
	});
	
	app.controller("XeroSettingsController", function($scope, $window, $timeout, XeroService) {				
				
		$scope.mappings = {
				sendEndOfDay : false,
				sendInvoiceSummary : false,
				createItems : false,
				taxes : {},
				payments : {},
				terminals : {}
		};
		
		$scope.onTrackingCategoryUpdate = function(trackingCategory){
			if(!trackingCategory){
				$scope.mappings.terminals = {};
				$scope.mappings.trackingCategoryName = "";
			}
			else
			{
				$scope.mappings.trackingCategoryName = trackingCategory.Name;
			}
			
		};
		
		$scope.config = {};
		
		var filterActive = function(record){
			return record["Status"] == "ACTIVE";
		};
		
		$scope.error = null; /* error message */
		
		XeroService.getXeroConfiguration().then(function(response) {
			
			var data = response.data;				
			
			data.xero.taxes.TaxRates = data.xero.taxes.TaxRates.filter(filterActive)
			data.xero.paymentAccounts.Accounts = data.xero.paymentAccounts.Accounts.filter(filterActive);
			
			var _mappings = data.xero.mappings;
			
			var trackingCategoryName = _mappings["trackingCategoryName"];
			
			if(trackingCategoryName){
				var _trackingCategories = data.xero.trackingCategories.TrackingCategories;
				var _trackingCategory = _trackingCategories.find(function(category){
					return category["Name"] == trackingCategoryName;
				});
				
				$scope.trackingCategory = _trackingCategory;
			}
			
			let defaultStoreList = [{
				store_name : 'All',
				store_id : 0
			}];
			
			//selected stores
			if(!_mappings.selectedStores){
				_mappings.selectedStores = {};
				data.pos.stores.forEach(function(store){
					_mappings.selectedStores[store.store_id] = 'true';
				});
			}
			
			
			//map tax by store
			_mappings.mapTaxByStore = _mappings.mapTaxByStore || 'false';
			
			$scope.onMapTaxByStoreChange = function(){
				
				$scope.taxStoreList = $scope.mappings.mapTaxByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				
				//reset tax mappings
				$scope.mappings.taxes = {};
			};
			
			//map payments by store
			_mappings.mapPaymentByStore = _mappings.mapPaymentByStore || 'false';
			
			$scope.onMapPaymentByStoreChange = function(){
				
				$scope.paymentStoreList = $scope.mappings.mapPaymentByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				
				//reset payment mappings
				$scope.mappings.payments = {};
			};
			
			//map account by store
			_mappings.mapAccountByStore = _mappings.mapAccountByStore || 'false';
			
			$scope.onMapAccountsByStoreChange = function(){
				
				$scope.accountStoreList = $scope.mappings.mapAccountByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				
				//reset account mappings
				$scope.mappings.accounts = {};
			};
			
			$scope.config = data;
			
			$timeout(function(){
				
				$scope.mappings = {..._mappings};				
				
				//render store lists for both taxes and payments
				$scope.taxStoreList = _mappings.mapTaxByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				$scope.paymentStoreList = _mappings.mapPaymentByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				$scope.accountStoreList = _mappings.mapAccountByStore === 'false' ? defaultStoreList : $scope.config.pos.stores;
				
				$scope.mappings.taxes = {};
				$scope.mappings.payments = {};
				$scope.mappings.accounts = {};
				
				$scope.mappings.selectedStores = {};
				
				$timeout(function(){
					//populate taxes after store list is rendered
					$scope.mappings.taxes = {..._mappings.taxes};
					//populate payments after store list is rendered
					$scope.mappings.payments = {..._mappings.payments};
					//populate accounts after store list is rendered
					$scope.mappings.accounts = {..._mappings.accounts};
					
					$scope.mappings.selectedStores = {... _mappings.selectedStores};
					
					
					console.log($scope.mappings);
					
					
				}, 100);
				
				var btns = jQuery("button.xero-save-button");
				btns.show();
				btns.click(function(){
					//new Dialog("Please wait ...").show();
					XeroService.saveMappings($scope.mappings).then(function(response){
						
						if(response.data.saved){
							BootstrapDialog.show({
				                type: BootstrapDialog.TYPE_SUCCESS,
				                title: 'Xero Configuration',
				                message: 'Configuration was successfully save.',
				                buttons: [{
				                    label: 'Ok',
				                    action: function(dialogRef){
				                        dialogRef.close();
				                    }
				                }]
				            });
						}
						else
						{
							BootstrapDialog.show({
				                type: BootstrapDialog.TYPE_DANGER,
				                title: 'Xero Configuration',
				                message: 'Failed to save configuration!',
				                buttons: [{
				                    label: 'Ok',
				                    action: function(dialogRef){
				                        dialogRef.close();
				                    }
				                }]
				            });
						}						 
						
						
					}, function(response) {
				        // This function will be called on error
				        console.log(response.statusText);
				        
				        BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_DANGER,
			                title: 'Xero Configuration',
			                message: 'Failed to save configuration! Reason: ' + response.statusText,
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
				    });
					
				});
				
			});
		},
		
		function(err){
			alert("Failed to load xero configuration! Error: " + err.data.error);
			
			$scope.error = err.data.error;
		}
		);
			
				
		$scope.disconnect = function(){
			
			BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'Xero Configuration',
                message: 'Do you want to disconnect Xero?',
                buttons: [{
                    label: 'Yes',
                    action: function(dialogRef){
                        dialogRef.close();
                        _disconnect();
                    }
                },
                {
                    label: 'No',
                    action: function(dialogRef){
                        dialogRef.close();
                    }
                }]
            });
			
			var _disconnect = function(){

				XeroService.disconnect().then(function(response){
					
					if(response.data.disconnected) {
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_SUCCESS,
			                title: 'Xero Configuration',
			                message: 'App was successfully disconnected',
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                        $window.location.href = "XeroAction.do?action=load";
			                    }
			                }]
			            });
					}
					else {
						BootstrapDialog.show({
			                type: BootstrapDialog.TYPE_DANGER,
			                title: 'Xero Configuration',
			                message: 'Failed to disconnect! Reason: ' + response.data.error,
			                buttons: [{
			                    label: 'Ok',
			                    action: function(dialogRef){
			                        dialogRef.close();
			                    }
			                }]
			            });
					}
					
					
					
				}, function(err){
					BootstrapDialog.show({
		                type: BootstrapDialog.TYPE_DANGER,
		                title: 'Xero Configuration',
		                message: 'Failed to disconnect! Reason: ' + err,
		                buttons: [{
		                    label: 'Ok',
		                    action: function(dialogRef){
		                        dialogRef.close();
		                    }
		                }]
		            });
				});			
			};
		}
			
		
	});

	app.service('XeroService', function($http) {

		this.getTenants = function() {
			return $http.get("XeroAction.do?action=getTenants");
		};		

		this.setTenantId = function(tenantId) {
			return $http.get("XeroAction.do?action=setTenantId&tenantId=" + tenantId);
		};		
		
		this.getXeroConfiguration = function() {
			return $http.get("XeroAction.do?action=getXeroConfiguration");
		};
		
		this.disconnect = function() {
			return $http.get("XeroAction.do?action=disconnect");
		};
		
		this.saveMappings = function(mappings) {
			return $http({
		        method: 'POST',
		        url: 'XeroAction.do',
		        data: jQuery.param({"action":"saveMappings", "mappings": JSON.stringify(mappings)}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		};

	});
}());
