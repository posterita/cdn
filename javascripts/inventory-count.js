/*Inventory Count*/
var InventoryCountManager = {};

InventoryCountManager.initializeButtons = function(){
	$('print.button').onclick = function(){
		printInventoryCount();
	}
};

InventoryCountManager.print = function(){
	var LINE_WIDTH = PrinterManager.getLineWidth();
	var LINE_SEPARATOR = JSReceiptUtils.replicate('-',LINE_WIDTH);

	var data = [	
					[Printer_ESC_COMMANDS.FONT_H1_BOLD, Printer_ESC_COMMANDS.CENTER_ALIGN, "Inventory Count", Printer_ESC_COMMANDS.FONT_NORMAL , Printer_ESC_COMMANDS.LEFT_ALIGN],							
					[Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR],
					['Terminal: ', printJSON.terminal],
					['Sales Rep: ', printJSON.salesRep],
					['Date: ', new Date().toDateString()],
					[Printer_ESC_COMMANDS.FONT_NORMAL, LINE_SEPARATOR]
				];

	var emptySpace = JSReceiptUtils.replicate('_',10);

	for(var i=0; i<printJSON.products.length; i++){
		var text = JSReceiptUtils.format(printJSON.products[i],(LINE_WIDTH - 10));
		data.push([text, emptySpace]);
	}

	data.push([Printer_ESC_COMMANDS.PAPER_CUT,'']);
	
	var printData = "";

	for(var i=0; i<data.length; i++){
	    for(var j in data[i]){
		    var field = data[i][j];

		    if (field == null || typeof(field) == "function") {
				continue;
		    }
		    
		    printData = printData + field;
	    }
	    
	    printData = printData + Printer_ESC_COMMANDS.LINE_FEED;
	}				

	PrinterManager.sendPrintJob(printData);				
};