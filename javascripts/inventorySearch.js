/** search product.js **/
var SearchProduct = {
		searchResultPlaceHolder:'productSearchPlaceHolder',
		isSoTrx:true,
		ignoreTerminalPricelist:false,
		input:null,
		active:null,
		filterQuery: 'searchproduct.m_product_parent_id is null',
		isSerialNoSearch:false,
		json:null,
		
		search:function(){
			
			if(SearchProduct.input == null) SearchProduct.input = $('search-product-button');
			
			var param = new Hash();
			param.set('action', 'search');
			param.set('searchTerm', SearchProduct.input.value);
			param.set('isSoTrx', SearchProduct.isSoTrx);
			param.set('bpPricelistVersionId', SearchProduct.bpPricelistVersionId);
			param.set('warehouseId', SearchProduct.warehouseId);
			param.set('ignoreTerminalPricelist', SearchProduct.ignoreTerminalPricelist);
			param.set('filterQuery', SearchProduct.filterQuery);
			
			SearchProduct.highlightTextField();
	
			new Ajax.Request('ProductAction.do', {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					SearchProduct.json =  json;
					
					if (!SearchProduct.isSerialNoSearch)
					{
						SearchProduct.render(json);
					}
				},
				parameters:param
			});
		},
		render:function(json){
			var container = $('searchResultContainer');
			container.innerHTML = '';
			
			var html = "<table id='searchResultTable' border='0' width='100%'>";

			var products = json.products;
			
			if (!json.success)
			{
				if (json.shopSavvy)
				{
					var shopSavvyPanel = new ShopSavvyProductPanel(json.shopSavvy);
					shopSavvyPanel.show();
				}
				else
				{
					var productSearchErrorPanel = new ProductSearchErrorPanel(SearchProduct.input.value);
					productSearchErrorPanel.show();
				}
			}
			
			for(var i=0; i<products.length; i++)
			{
				var product = products[i];
				
				var isParent = product.isparent == 'Y'? true : false;
				var isSerialNo = product.isserialno == 'Y' ? true : false;
				var m_product_parent_id = product.m_product_parent_id;
				
				if (products.length == 1)
				{
					if (isSerialNo && isParent && product.qtyonhand == 0)
					{
						var productSearchErrorPanel = new ProductSearchErrorPanel(SearchProduct.input.value);
						productSearchErrorPanel.show();
						return;
					}
				}
				
				if (isSerialNo && product.qtyonhand == 0)
				{
					//If product is serial no and has already been sold do not display in search
				}
				else
				{
					html += "<tr class='" + ((i%2==0)? "odd" : "even") + (product.isparent == 'Y' ? " parentProduct" : "") +"'>";
					html += "<td><span>";
					html += product.qtyonhand;
					html += "x</span></td>";
					html += "<td><span>";
					html += product.name;
					html += "<br>";
					html += product.description;
					html += "</span></td>";
					html += "</tr>";
				}
			}

			html += "</table>";
			
			container.innerHTML = html;
			
			/*add behaviour*/
			var table = $('searchResultTable');
			var rows = table.rows;
			
			if (products.length == 1)
			{
				var product = products[0];
				var isParent = product.isparent == 'Y'? true : false;
				var m_product_parent_id = product.m_product_parent_id;
				
				if (!isParent)
				{
					SearchProduct.onSelect(product);
				}
			}
			
			for(var i=0; i<rows.length; i++)
			{
				var row = rows[i];
				row.product = products[i];
				
				row.style.cursor = 'pointer';
				row.onclick = function(e){
					
					var isParent = this.product.isparent == 'Y'? true : false;
					var isSerialNo = this.product.isserialno == 'Y' ? true : false;
					
					//If product is parent and is serialno and qty is zero
					if (isParent && isSerialNo && this.product.qtyonhand ==  0)
					{
						new AlertPopUpPanel(Translation.translate('you.cannot.transfer.a.parent.with.quantity.zero','You cannot transfer a parent with quantity zero')).show();
						return;
					}
					
					//If an existing(old) product or child, add to cart
					if (!isParent)
					{
						SearchProduct.onSelect(this.product);
						
						var highlightColor = 'search-product-highlight';
						
						if(SearchProduct.active){
							SearchProduct.active.removeClassName(highlightColor);						
						}
						
						SearchProduct.active = this;
						this.addClassName(highlightColor);
					}
					else
					{
						/*Is a parent product and may have children, query children*/
						
						SearchProduct.filterQuery = 'searchproduct.m_product_parent_id=' + this.product.m_product_id;
						
						/*Non Instance Variants will always exists with children, render children list
						If product is serialNo, it may not have instance. Prompt to create specific instance
						If instances exist and all instances has been created, render children list
						If not all instances have been created, render children list and prompt to create more instance
						*/
						
						//If Non instance variant, search children and render list
						if (!isSerialNo)
						{
							SearchProduct.search();
						}
						else
						{
							//SerialNo
							SearchProduct.isSerialNoSearch = true;
							
							//Since serialNo, do not display child with qty 0
							SearchProduct.filterQuery = 'searchproduct.qtyonhand<>0;searchproduct.m_product_parent_id=' + this.product.m_product_id;
							SearchProduct.search();
							
							//If no Instance exists
							if  (!SearchProduct.json.success)
							{
								//Prompt to create new Instance
								var createInstanceProductPanel = new CreateInstanceProductPanel(this.product);
								createInstanceProductPanel.isAddParentToCart = true;
								createInstanceProductPanel.show();
							}
							else
							{
								//Render children List and Prompt to create new Instance if any
								//Check if we can create more instance
								if (this.product.qtyonhand > 1)
								{
									var createInstanceProductPanel = new CreateInstanceProductPanel(this.product);
									createInstanceProductPanel.isAddParentToCart = true;
									createInstanceProductPanel.show();
								}
								else
								{
									//add the only instance left to cart
									SearchProduct.onSelect(SearchProduct.json.products[0]);
								}
							}
						}
					}
				};
			}
			
			//reset filter
			SearchProduct.filterQuery = 'searchproduct.m_product_parent_id is null';
			SearchProduct.isSerialNoSearch = false;
			
			SearchProduct.afterRender();
		},
		afterRender:function(){
			jQuery('#pane3').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		},
		onSelect:function(product){
			var warehouseId = jQuery('#warehouse-dropdown option:selected').val();
			shoppingCart.addToCart(product.m_product_id,1,null,null,null,warehouseId);
		},
		initializeComponents:function(){
			SearchProduct.input = $('search-product-textfield');
			$('search-product-button').onclick = function(e){
				SearchProduct.search();
			};

			$('search-product-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchProduct.search();
				}
			};
			
			$('search-product-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-product-textfield').onclick = function(e){
				this.select();
			};
		},
		highlightTextField : function()
		{
			$('search-product-textfield').select();
		},
		clearTextField : function()
		{
			$('search-product-textfield').value = '';
		},
		disableTextFieldFocus : function()
		{
			$('search-product-textfield').blur();
		}
};

var SearchProductFilter = {
		data : null,
		selection : null,
		query : null,		
		
		setSelectionQuery : function(query){
			this.query = query;
			this.applySelection();
		},
				
		applySelection : function(){
			var filterQuery = this.query;
			
			var url = "ProductAction.do";
			
			var param = new Hash();
			
			param.set('action', 'search');			
			param.set('searchTerm', '');
			param.set('isSoTrx', SearchProduct.isSoTrx);
			param.set('bpPricelistVersionId', SearchProduct.bpPricelistVersionId);
			param.set('warehouseId', SearchProduct.warehouseId);
			param.set('filterQuery', filterQuery);
			param.set('ignoreTerminalPricelist', SearchProduct.ignoreTerminalPricelist);
			
			param = param.toQueryString();
			
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'get', 
				parameters: param, 
				onSuccess: function (response) {
					//update info
					var json = response.responseText.evalJSON(true);
					SearchProduct.render(json);
				}
			});
		}
};
