
function loadIWantTo(quickMenus)
{
	//var ul = document.createElement('ul');
	
	var div = $('quickMenuItems');
	
	for (var i=0; i<quickMenus.length; i++)
	{
		var quickMenu = quickMenus[i];
		if (quickMenu == null)
			continue;
		
		var name = quickMenu.name;
		var menuLink = quickMenu.menuLink;
		
		/* check if offline is enabled*/
		if(menuLink == 'javascript:APP.switchOffline();'){
			if(!OfflineManager.isOfflineEnabled()) continue;
		}
		
		//create button
		
		var a = document.createElement('a');

		a.setAttribute('href', menuLink);
		
		var button = document.createElement('button');
		button.setAttribute('type', 'button');
		button.setAttribute('class', 'btn btn-lg btn-default');
		
		var buttonLabel = document.createElement('div');
		buttonLabel.setAttribute('class', 'menu-button-label');
		buttonLabel.innerHTML = Translation.translate(name);
		//buttonLabel.innerHTML = name;
		
		var buttonIcon = document.createElement('div');
		
		var regex = new RegExp(' ', 'g');
		
		var className = name.replace(regex, '-');
		
		buttonIcon.setAttribute('class', 'glyphicons glyphicons-pro ' + className);
		
		button.appendChild(buttonIcon);
		button.appendChild(buttonLabel);
		
		a.appendChild(button);
		
		div.appendChild(a);
	}
	
	var a = document.createElement('a');

	a.setAttribute('href', 'hardware-setting.do');
	
	var button = document.createElement('button');
	button.setAttribute('type', 'button');
	button.setAttribute('class', 'btn btn-lg btn-default');
	
	var buttonLabel = document.createElement('div');
	buttonLabel.setAttribute('class', 'menu-button-label');
	buttonLabel.innerHTML = Translation.translate('smenu.hardware.setup', 'Hardware Settings');
	
	var buttonIcon = document.createElement('div');
	buttonIcon.setAttribute('class', 'glyphicons glyphicons-pro wrench');
	
	button.appendChild(buttonIcon);
	button.appendChild(buttonLabel);
	
	a.appendChild(button);
	
	div.appendChild(a);
	
	
	a = document.createElement('a');

	a.setAttribute('href', 'InitSupportAction.do?action=initRequestSupport');
	
	button = document.createElement('button');
	button.setAttribute('type', 'button');
	button.setAttribute('class', 'btn btn-lg btn-default');
	
	buttonLabel = document.createElement('div');
	buttonLabel.setAttribute('class', 'menu-button-label');
	buttonLabel.innerHTML = Translation.translate('smenu.request.support', 'Support');
	
	buttonIcon = document.createElement('div');
	buttonIcon.setAttribute('class', 'glyphicons glyphicons-pro phone_alt');
	
	button.appendChild(buttonIcon);
	button.appendChild(buttonLabel);
	
	a.appendChild(button);
	
	div.appendChild(a);
	

	//check price

	a = document.createElement('a');

	a.setAttribute('href', 'javascript:new ProductInfoPopup().show();');
	
	button = document.createElement('button');
	button.setAttribute('type', 'button');
	button.setAttribute('class', 'btn btn-lg btn-default');
	
	buttonLabel = document.createElement('div');
	buttonLabel.setAttribute('class', 'menu-button-label');
	buttonLabel.innerHTML = Translation.translate('smenu.product.info', 'Product Info');
	
	buttonIcon = document.createElement('div');
	buttonIcon.setAttribute('class', 'glyphicon glyphicon-search');
	
	button.appendChild(buttonIcon);
	button.appendChild(buttonLabel);
	
	a.appendChild(button);
	
	div.appendChild(a);
	
	
	//logout
	a = document.createElement('a');

	a.setAttribute('href', 'LoginAction.do?action=logout');
	
	button = document.createElement('button');
	button.setAttribute('type', 'button');
	button.setAttribute('class', 'btn btn-lg btn-default');
	
	buttonLabel = document.createElement('div');
	buttonLabel.setAttribute('class', 'menu-button-label');
	buttonLabel.innerHTML = Translation.translate('smenu.log.out', 'Log Out');
	
	buttonIcon = document.createElement('div');
	buttonIcon.setAttribute('class', 'glyphicon glyphicon-log-out');
	
	button.appendChild(buttonIcon);
	button.appendChild(buttonLabel);
	
	a.appendChild(button);
	
	div.appendChild(a);
	
	
	/*jQuery(document).ready(function() {
		jQuery("#iwantto").click(function(e) {
			jQuery("#quickMenuItems").toggle();
            e.preventDefault();
            e.stopPropagation();
        });
		
		jQuery(".dropdown ul li a").click(function() {
		    var text = jQuery(this).html();
		    jQuery(".dropdown ul").hide();
		});
		
		jQuery(document).bind('click', function(e) {
		    var clicked = jQuery(e.target);
		    if (! clicked.parents().hasClass("dropdown"))
		    	jQuery(".dropdown ul").hide();
		});
    });*/
}
