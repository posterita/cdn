/*-------------------------------------------------------------------------------------------*/
var Keyboard = {
		initialized:false,
		initialize:function(){
			/*initialize component*/ 
			if(this.initialized) return;
			
			this.input = null;			
			
			this.keyboardContainer = $('keyboard.container');
			if(this.keyboardContainer == null){
				new Insertion.Bottom(document.body, "<div id='keyboard.container' style='display:none;position:absolute;z-index:999999;'></div>");	
				this.keyboardContainer = $('keyboard.container');
			}
			
			this.keyboardContainer.innerHTML = '<div class="keyboardHandler"></div>' +
				'<div id="keyboard.uppercaseKeyPad" style="display:none;">' +
				'<table class="keyboard">' +
				'<tr>' +		
					'<td>1</td>' +
					'<td>2</td>' +
					'<td>3</td>' +
					'<td>4</td>' +
					'<td>5</td>' +
					'<td>6</td>' +
					'<td>7</td>' +
					'<td>8</td>' +
					'<td>9</td>' +
					'<td>0</td>' +
					'<td>HIDE</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Q</td>' +
					'<td>W</td>' +
					'<td>E</td>' +
					'<td>R</td>' +
					'<td>T</td>' +
					'<td>Y</td>' +
					'<td>U</td>' +
					'<td>I</td>' +
					'<td>O</td>' +
					'<td>P</td>' +
					'<td>BACK</td>' +
				'</tr>' +
				'<tr>' +
					'<td>A</td>' +
					'<td>S</td>' +
					'<td>D</td>' +
					'<td>F</td>' +
					'<td>G</td>' +
					'<td>H</td>' +
					'<td>J</td>' +
					'<td>K</td>' +
					'<td>L</td>' +
					'<td colspan="2">ENTER</td>' +
				'</tr>' +
				'<tr>' +
					'<td colspan="2">CAPS</td>' +	
					'<td>Z</td>' +
					'<td>X</td>' +
					'<td>C</td>' +
					'<td>V</td>' +
					'<td>B</td>' +
					'<td>N</td>' +
					'<td>M</td>' +
					'<td>,</td>	' +
					'<td>.</td>' +	
				'</tr>' +	
				'<tr>' +
					'<td colspan="11">SPACE</td>' +
				'</tr>' +
				'</table>' +
				'</div>' +
				'<div id="keyboard.lowercaseKeyPad">' +
				'<table class="keyboard">' +
				'<tr>' +		
					'<td>1</td>' +
					'<td>2</td>' +
					'<td>3</td>' +
					'<td>4</td>' +
					'<td>5</td>' +
					'<td>6</td>' +
					'<td>7</td>' +
					'<td>8</td>' +
					'<td>9</td>' +
					'<td>0</td>' +
					'<td>HIDE</td>' +
				'</tr>' +
				'<tr>' +
					'<td>q</td>' +
					'<td>w</td>' +
					'<td>e</td>' +
					'<td>r</td>' +
					'<td>t</td>' +
					'<td>y</td>' +
					'<td>u</td>' +
					'<td>i</td>' +
					'<td>o</td>' +
					'<td>p</td>' +
					'<td>BACK</td>' +
				'</tr>' +
				'<tr>' +
					'<td>a</td>' +
					'<td>s</td>' +
					'<td>d</td>' +
					'<td>f</td>' +
					'<td>g</td>' +
					'<td>h</td>' +
					'<td>j</td>' +
					'<td>k</td>' +
					'<td>l</td>' +
					'<td colspan="2">ENTER</td>' +
				'</tr>' +
				'<tr>' +
					'<td colspan="2">CAPS</td>' +	
					'<td>z</td>' +
					'<td>x</td>' +
					'<td>c</td>' +
					'<td>v</td>' +
					'<td>b</td>' +
					'<td>n</td>' +
					'<td>m</td>' +
					'<td>,</td>	' +
					'<td>.</td>' +	
				'</tr>' +	
				'<tr>' +
					'<td colspan="11">SPACE</td>' +
				'</tr>' +
				'</table>' +
				'</div>' +
				'<div class="keyboardHandler"></div>';
			
			this.uppercaseKeyPad = $('keyboard.uppercaseKeyPad');
			this.lowercaseKeyPad = $('keyboard.lowercaseKeyPad');
			
			new Draggable('keyboard.container',{handle:'div.keyboardHandler'});
			
			this.keys = this.keyboardContainer.getElementsByTagName('td');
			for(var i=0; i<this.keys.length; i++){
				this.keys[i].onclick = this.keyHandler.bindAsEventListener(this);
			}
			
			var inputs = document.getElementsByTagName('input');
			for(var j=0; j<inputs.length; j++){
				var input = inputs[j];
				Event.observe(input,'click',this.setInput.bindAsEventListener(this),false);
			}	
			
			this.initialized = true;
		},
		
		setInput:function(e){
			var element = Event.element(e);
			this.input = element;
		},
		
		toggle:function(){
			this.initialize();
			this.keyboardContainer.toggle();
			ViewPort.center(this.keyboardContainer);
		},
		
		toggleCapsLock:function(){
			this.uppercaseKeyPad.toggle();
			this.lowercaseKeyPad.toggle();
		},
		
		keyHandler:function(e){
			var element = Event.element(e);			
			var value = element.innerHTML;
			
			if(value == 'CAPS'){
				this.toggleCapsLock();
				return;
			}
			
			if(value == 'HIDE'){
				this.toggle();
				return;
			}
			
			if(this.input == null){
				return;
			}
			
			if(value == '&nbsp;'){
				this.input.focus();
				return;
			}
			
			if(value == 'SPACE'){
				value = ' ';
			}			
			
			if(value == 'BACK'){
				value = this.input.value;
				
				if(value.length == 0){
					this.input.focus();
					return;
				}					
				else{
					value = value.substring(0,value.length-1);
					this.input.value = value;
					
					//hacking autocompleter
					if(this.input.Autocompleter)
					{
						var autocompleter = this.input.Autocompleter;					
						var onKeyPress = autocompleter.onKeyPress;
						onKeyPress.call(autocompleter,event);
					}
					return;
				}					
			}
			
			if(value == 'ENTER'){			
				value = '\n';
				
				//simulating press ENTER
				//window.event.keyCode = 13;
				var event = {keyCode:13};
				
				if(this.input.onkeyup)
				{
					toConsole('simulating enter');
					var func = this.input.onkeyup;
					func.call(this.input,event);	
				}
				
				//hacking autocompleter
				if(this.input.Autocompleter)
				{
					var autocompleter = this.input.Autocompleter;					
					var onKeyPress = autocompleter.onKeyPress;
					onKeyPress.call(autocompleter,event);
				}
					else
				{
					if(this.input.form)
					{
						this.input.form.submit();
					}
				}
				return false;
							
			}			
			
			//insertAtCursor(textfield,value);
			this.input.value = this.input.value.toString() + value.toString();
			
			//hacking autocompleter
			if(this.input.Autocompleter)
			{
				var autocompleter = this.input.Autocompleter;
				var event = {keyCode:65};
				
				var onKeyPress = autocompleter.onKeyPress;
				onKeyPress.call(autocompleter,event);
			}
			else
			{
				this.input.focus();
			}
		}
};