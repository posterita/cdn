var messages = {
		messageMap : null,
		
		get : function(key){
				var msg = this.messageMap.get(key);
				if(msg == null){
					/*if(console)console.warn('No messages found -> ' + key);*/
					msg = key;
				}
				
				return msg;
		},
		
		set : function(key, value){
			this.messageMap.set(key,value);
		},
		
		format : function(){
			var args = arguments;			
			
			if(args != null && args.length > 1)
			{
				var pattern = /\{\d+\}/g;
				var key = args[0];
				
				var msg = this.get(key);				
			    return msg.replace(pattern, function(capture){ 
			    	var x = capture.match(/\d+/);			    	
			    	return args[Number(x) + 1]; 
			    });
			}
		},
				
		init : function(){
			/*if(console)console.info('Loading js messages');*/
			this.messageMap = new Hash();
			
			/*set messages*/
			this.messageMap.set("js.test","Test");
			this.messageMap.set("js.test2","Test {0} test {1}");	
		}
};

messages.init();
