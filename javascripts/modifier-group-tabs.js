var ModifierTab = Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'Modifiers');
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['modifier.group.modifiers.general.help'];
		modifierGroupId = json.modifierGroupId;
		
		modifiers = json.modifiers;
		displayModifiers();
		
		var createModifierPanel = new CreateModifierPanel();
		createModifierPanel.show();
		
		/*jQuery('#dialog-create-modifier').dialog({
			resizable: false,
			modal: true,
			width : 400,
			autoOpen : false,
			draggable : false,
			buttons: {				
				"Save": function() {
					if(jQuery('#create-modifier-name').val().length == 0){
						alert('Name is required!');
						return;
					}
					saveModifier();
					jQuery( this ).dialog( "close" );
					
				},
				
				"Cancel": function() {
					jQuery( this ).dialog( "close" );
				}
			}
		});
		
		jQuery( "#dialog-create-modifier" ).dialog( "option", "position", { my: "center top+80", at: "center center", of: jQuery("#content-header-container") } );*/
	},
	
	afterOk : function(json)
	{
		
	},
	
	getParameters : function()
	{		
		var h = new Hash();			
		
		var parameters = h.toJSON();		
		return parameters;
	}
});

var modifiers = null;
var currentModifier = null;
var modifierGroupId = 0;

function createModifier(){
	//edit modifier
	var form = $("create-modifier");
	form.reset();
	
	currentModifier = {};
	jQuery('#create-modifier-id').val(0);
	jQuery('#create-modifier-name').val("");
	jQuery('#create-modifier-position').val(0);
	jQuery('#create-modifier-price').val("0.00");
	
	Event.observe($('create-modifier-position'),'keyup',this.keyUpHandler.bindAsEventListener(this),false);	
	Event.observe($('create-modifier-price'),'keyup',this.keyUpHandler.bindAsEventListener(this),false);	
	
	//jQuery( "#dialog-create-modifier" ).dialog( "open" );
	var createModifierPanel = new CreateModifierPanel();
	createModifierPanel.show();
}

function editModifier(id){
	//edit modifier
	var modifier = null;
	for(var i=0; i<modifiers.length; i++){
		if(id == modifiers[i].u_modifier_id){
			modifier = modifiers[i];
			break;
		}
	}
	
	if(modifier == null){
		alert(Translation.translate('failed.to.load.modifier','Failed to load modifier!'));
		return;
	}
	
	currentModifier = modifier;
	
	var form = $("create-modifier");
	form.reset();
	
	jQuery('#create-modifier-id').val(modifier.u_modifier_id);
	jQuery('#create-modifier-name').val(modifier.name);
	jQuery('#create-modifier-position').val(modifier.position);
	jQuery('#create-modifier-price').val(new Number(modifier.price).toFixed(2));
	
	Event.observe($('create-modifier-position'),'keyup',this.keyUpHandler.bindAsEventListener(this),false);	
	Event.observe($('create-modifier-price'),'keyup',this.keyUpHandler.bindAsEventListener(this),false);
	
	//jQuery( "#dialog-create-modifier" ).dialog( "open" );
	var createModifierPanel = new CreateModifierPanel();
	createModifierPanel.show();
}

function deleteModifier(id){
	
	jQuery.post("ModifierGroupAction.do?action=deleteModifier&u_modifier_id=" + id).done(function(data){
		
		data = JSON.parse(data);
		
		if(data.error){
			alert('Failed to delete! Error : ' + data.error);
			return;
		}
		
		modifiers = data.modifiers;
		
		displayModifiers();
	});
}

function displayModifiers(){
	if(modifiers != null){
		
		var container = $('modifier.group.children');
		var html = "<table><tr><th>" +Translation.translate("Name","Name")+ "</th><th>" +Translation.translate("Position","Position")+ "</th><th>" +Translation.translate("price","Price")+ "</th><th><a href='javascript:createModifier();'><button class='btn btn-xs btn-tertiary'>" +Translation.translate("Create","Create")+ "</button></a></th></tr>";
		for(var i=0; i<modifiers.length; i++){
			html =  html + "<tr><td>" + modifiers[i].name + "</td><td>" + modifiers[i].position + "</td><td>" + new Number(modifiers[i].price).toFixed(2) + 
			"</td><td><div class='row'><div class='col-xs-6 col-md-6 padding-right-8'><a href='javascript:editModifier(" + modifiers[i].u_modifier_id + ")'><button class='btn btn-xs btn-secondary'>" +Translation.translate("edit","Edit")+ "</button></a></div><div class='col-xs-6 col-md-6'><a href='javascript:deleteModifier(" + modifiers[i].u_modifier_id + ")'>" +
					"<button class='btn btn-xs btn-danger'>" +Translation.translate("delete","Delete")+ "</button></a></div></div></td></tr>"
		}
		
		html =  html + "</table>";
		container.innerHTML = html;
	}
}

function saveModifier()
{
	jQuery('#create-modifier-group-id').val(modifierGroupId);
	
	jQuery.post("ModifierGroupAction.do?action=saveModifier", jQuery("#create-modifier").serialize()).done(function(data){
		
		data = JSON.parse(data);
		
		if(data.error){
			alert('Failed to save! Error : ' + data.error);
			return;
		}
		
		modifiers = data.modifiers;
		displayModifiers();
	});
	
	
}

function keyUpHandler(e){
	
	var element = Event.element(e);	
	
	var re = /[^0-9\.\-]/g;
    if(re.test(element.value))
    {
    	element.value = element.value.replace(re, '');
    }

}
