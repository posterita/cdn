(function () {
	var networkOkDialog = null;
	
	new APP.UTILS.NetworkEventListener().disconnected(function(){
		/*No internet connection*/
		jQuery.pnotify({
		    title: 'No internet connection',
		    text: '',
		    styling: 'jqueryui',
		    type: 'error',
		    icon: 'ui-icon ui-icon-signal'
		});
	}).connected(function(){
		/*connected to network*/
		jQuery.pnotify({
		    title: 'Connected to network',
		    text: '',
		    styling: 'jqueryui',
		    type: 'info',
		    icon: 'ui-icon ui-icon-signal'
		});
	});
	
	var serverReachableDialog = null;
	var CONTINUE_OFFLINE_FLAG = "continue-offline-flag";
	
	new APP.UTILS.ServerMonitor().unreachable(function(){
		
		window.localStorage.setItem(CONTINUE_OFFLINE_FLAG, 'false');
		
		if(serverReachableDialog){
			serverReachableDialog.dialog("close");
		}
	 }).reachable(function(){
		 
		 if(window.localStorage.getItem(CONTINUE_OFFLINE_FLAG) == 'true') return;
		 
		 serverReachableDialog = jQuery("<div title='Posterita Server'><p>Posterita server is available again.</p></div>").dialog({
				resizable: false,
				modal: true,
				autoOpen : true,
				draggable : false,
				position : {my: "top", at:"top+200"},
				buttons: {				
					"Continue offline": function() {
						jQuery( this ).dialog( "close" );
						window.localStorage.setItem(CONTINUE_OFFLINE_FLAG, 'true');
					},
					
					"Go online": function() {
						jQuery( this ).dialog( "close" );
						window.localStorage.setItem(CONTINUE_OFFLINE_FLAG, 'false');
						APP.switchOnline();
					},
				}
		});
	 });
	
}());