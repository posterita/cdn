var BPartnerManager = {
		initialised : false,
		
		init : function(){
			if(this.initialised) return;
			
			this.loadBps();			
			this.initialised = true;
		},
		
		loadBps : function(){
			console.log('[BPartnerManager] Loading bps');
			jQuery.db.values('bp', null, 10000).done(function(records){
				BPartnerManager.db = TAFFY(records);
				console.log('[BPartnerManager] Found ' + records.length + ' bps.');
				
				BPartnerManager.onReady();
		    });			
		},
		
		getBPartnerById : function(bPartnerById){
			this.init();
			var query = {"c_bpartner_id" : bPartnerById};			
			var results = this.db(query).get();
			
			if(results.length != 0){
				var bp = Object.clone(results[0]);
				return bp;
			}
			
			return null;
		},
		
		searchBPartners : function(query){
			this.init();
			var results = this.db(query).get();
			return results;
		},
		
		onReady : function(){}
};