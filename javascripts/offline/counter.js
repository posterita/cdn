/**/
var counter = 1;
var max = 150;

var appCacheNotifier = null;

var successOption = {};
successOption.title = "Done!";
successOption.type = "success";
successOption.hide = true;
successOption.closer = true;
successOption.sticker = true;
successOption.icon = 'picon picon-task-complete';
successOption.opacity = 1;
successOption.shadow = true;

appCacheLog = function() {
    var message = Array.prototype.join.call(arguments, " ");
    console.debug(message);
    if(!appCacheNotifier){
    	appCacheNotifier = $.pnotify({
            type: 'info',
            icon: 'picon picon-throbber',
            hide: false,
            closer: false,
            sticker: false,
            opacity: .75,
            shadow: false
        });
    }
    
    appCacheNotifier.pnotify({
    	text: message
    });
}

// log each of the events fired by window.applicationCache
window.applicationCache.onchecking = function(e) {
    appCacheLog("Checking for updates");
}

window.applicationCache.onnoupdate = function(e) {
    appCacheLog("No updates found");
    appCacheNotifier.pnotify(successOption);
    
}

window.applicationCache.onupdateready = function(e) {
    appCacheLog("Update complete");
    appCacheNotifier.pnotify(successOption);
}

window.applicationCache.onobsolete = function(e) {
    appCacheLog("Cache obsolete");
}

window.applicationCache.ondownloading = function(e) {
    appCacheLog("Downloading updates");
}

window.applicationCache.oncached = function(e) {
    appCacheLog("Cached");
    appCacheNotifier.pnotify(successOption);
}

window.applicationCache.onerror = function(e) {
    //appCacheLog("ApplicationCache error - " + e);
	appCacheNotifier.pnotify({
		title: "Error!",
    	text: "Caching failed! <a href='javascript:window.location.reload();'>Reload page to fix it.</a>",
    	type: "error",
    	hide: false,
    	closer: true,
    	sticker: true,
    	opacity : 1,
	    shadow : true,
	    icon : ''
    });
}

window.applicationCache.onprogress = function(e) {
    appCacheLog("Progress: " + parseInt((counter/max)*100) + " % complete.");
    counter++;
}

window.addEventListener("online", function(e) {
    //appCacheLog("You are online");
    
    $.pnotify({    
        text: 'You are online',
        type: 'success',
        icon: 'icon-signal'
        });
    
}, true);

window.addEventListener("offline", function(e) {
    //appCacheLog("You are offline");
    
    $.pnotify({    
        text: 'You are offline',
        type: 'error',
        icon: 'icon-signal'
        });
    
}, true);

// Convert applicationCache status codes into messages
showCacheStatus = function(n) {
    statusMessages = ["Uncached","Idle","Checking","Downloading","Update Ready","Obsolete"];
    return statusMessages[n];
};

onload = function(e) {
    // Check for required browser features
    if (!window.applicationCache) {
        appCacheLog("HTML5 offline web applications (ApplicationCache) are not supported in your browser.");
        return;
    }

    appCacheLog("Initial AppCache status: " + showCacheStatus(window.applicationCache.status));

};

/*
function dyn_notice() {
    var percent = 0;
    var notice = $.pnotify({
        title: "Please Wait",
        type: 'info',
        icon: 'picon picon-throbber',
        hide: false,
        closer: false,
        sticker: false,
        opacity: .75,
        shadow: false,
        width: "150px"
    });

    setTimeout(function() {
        notice.pnotify({
            title: false
        });
        var interval = setInterval(function() {
            percent += 2;
            var options = {
                text: percent + "% complete."
            };
            if (percent == 80) options.title = "Almost There";
            if (percent >= 100) {
                window.clearInterval(interval);
                options.title = "Done!";
                options.type = "success";
                options.hide = true;
                options.closer = true;
                options.sticker = true;
                options.icon = 'picon picon-task-complete';
                options.opacity = 1;
                options.shadow = true;
                options.width = $.pnotify.defaults.width;
                //options.min_height = "300px";
            }
            notice.pnotify(options);
        }, 120);
    }, 2000);
}
*/