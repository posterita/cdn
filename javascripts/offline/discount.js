/**
 * Small pricing model to calculate prices based on priceList, 
 * priceStd, priceLimit and priceEntered
 */
 var PPricing = Class.create({
	 initialize: function(priceEntered, priceStd, priceList, priceLimit, qty, taxRate, isTaxIncluded)
	 {
	 	this.pricePrecision = 2;
	 	this.priceEntered = priceEntered;
	 	this.priceStd = priceStd;
	 	this.priceList = priceList;
	 	this.priceLimit = priceLimit;	 	
	 	this.qty = qty;
	 	this.taxRate = taxRate;
	 	this.isTaxIncluded = isTaxIncluded;
	 	this.discount = 0; /*discount percentage*/
	 	
	 	/*Bug fix
	 	 * If user forgot to set list & limit prices set them to std*/
	 	if(this.priceList == 0.0) this.priceList = this.priceStd;
	 	if(this.priceLimit == 0.0) this.priceLimit = this.priceStd;
	 },
	 
	 setPrecision : function(price)
	 {
		 var p = new Number(price).toFixed(this.pricePrecision);
		 
		 return parseFloat(p);
	 },
	 
	 getPrice : function(price, taxIncluded)
	 {
		 var newPrice  = 0;
		 
		 if(this.isTaxIncluded == taxIncluded){
			 newPrice = price;
		 }
		 else{
			 /*Price without tax*/
			 if(this.isTaxIncluded && !taxIncluded)		
			 {
				 newPrice = this.calculatePrice(price, true);
			 }
			 else
			 {
				 newPrice = this.calculatePrice(price, false);
			 }
		 }
		 
		 newPrice = this.setPrecision(newPrice);		 
		 return newPrice;

	 },
	 
	 getDiscount : function()
	 {
		 var discount = this.priceList - this.priceEntered;
		 
		 if(this.priceList == 0) return 0.0; /*prevent divide by zero error*/
		 
		 discount = (discount/this.priceList)*100;		 
		 return this.setPrecision(discount);
	 },
	 
	 setDiscount : function(discount)
	 {		 
		 discount = parseFloat(discount);
		 
		 this.discount = discount;
		 
		 if(isNaN(discount)){
			 //alert(messages.get('js.invalid.discount'));
			 discount = 0.0;
		 }
		 
		 if(discount > 100.0)
		 {
			 //alert(messages.get('js.invalid.discount'));
			 discount = 100.0;
		 }
		 		 
		 var newPrice = (this.priceList*(100.00 - discount))/100.00;
		 newPrice = this.setPrecision(newPrice);
		 
		 //round price		 
		 this.priceEntered = newPrice;
	 },
	 
	 getTotal : function()
	 {
		 return this.priceEntered * this.qty;
	 },
	 
	 	 
	 calculatePrice : function(price, isTaxIncluded)
	 {
		 var newPrice = 0;
		 
		 //if price includes tax
		 if(isTaxIncluded)
		 {
			 newPrice = (price * 100)/(100 + this.taxRate);
		 }
		 else
		 {
			 newPrice = price + ((price*this.taxRate)/100);
		 }
		 
		 newPrice = this.setPrecision(newPrice);		 
		 return newPrice;
		 
	 },
	 
	 getDiscountAmt : function()
	 {
		 var discountAmt = (this.priceList - this.priceEntered) * this.qty;
		 
		 discountAmt = this.setPrecision(discountAmt);		 
		 return discountAmt;
	 },
	 
	 getPriceEntered : function()
	 {
		 return this.priceEntered;
	 }
	 
 });
 
 var DiscountPanel = {
	 initialized:false,	
	 
	 visible:false,
	 
	 toggle:function(){		
	 	 this.visible = !this.visible;	
	 
		 if(!this.initialized) this.initialize();
		 
		 if(ShoppingCartManager.isShoppingCartEmpty()) return;
		 
		 if(!DiscountManager.isDiscountOnLineAllowed()) return;
		 		 
		 if(!this.visible){			
			 $('discountContainer').hide();
			 $('buttonContainer').show();
			 $$('.sidebar-main-button')[0].show();
			 
			 return;
		 }
		 else
		 {
			 $('buttonContainer').hide();
			 $$('.sidebar-main-button')[0].hide();
			 $('discountContainer').show();			 
		 }
		 
		 if(!DiscountManager.isDiscountOnTotalAllowed())
		 {
			/*hide discount on total panel*/
			DiscountManager.hideDiscountOnTotalPanel();
		 }
		 else
		 {
			DiscountManager.showDiscountOnTotalPanel();
		 }		
		
		this.loadLineDetails();
		this.renderDiscountRightsInfo();	
		
		
		
	 },
	 
	 hide:function(){
		 $('discountContainer').hide();
		 $('buttonContainer').show();	
		 $$('.sidebar-main-button')[0].show();
		 
		 this.visible = false;
	 },
 
	 initialize:function(){
	 	this.unit_excl_tf = $('unit.excl');
	 	this.unit_incl_tf = $('unit.incl');
	 	this.total_excl_tf = $('total.excl');
	 	this.total_incl_tf = $('total.incl');
	 	this.discount_percentage_tf = $('discount.percentage');
	 	this.discount_resetBtn = $('discount.resetBtn');	
	 	this.discount_applyBtn = $('discount.applyBtn');
	 	
	 	/*Preset discounts*/
	 	this.discount_percentage_5_btn = $('discount.percentage.5');
	 	this.discount_percentage_5_btn.panel = this;
	 	this.discount_percentage_5_btn.onclick = function(e){
	 		this.panel.pp.setDiscount(5);
	 		this.panel.applyDiscount();
	 	};
	 	
	 	this.discount_percentage_10_btn = $('discount.percentage.10');
	 	this.discount_percentage_10_btn.panel = this;
	 	this.discount_percentage_10_btn.onclick = function(e){
	 		this.panel.pp.setDiscount(10);
	 		this.panel.applyDiscount();
	 	};	 	
	 	
	 	this.discount_percentage_20_btn = $('discount.percentage.20');
	 	this.discount_percentage_20_btn.panel = this;
	 	this.discount_percentage_20_btn.onclick = function(e){
	 		this.panel.pp.setDiscount(20);
	 		this.panel.applyDiscount();
	 	};
	 	
	 	this.discount_percentage_30_btn = $('discount.percentage.30');
	 	this.discount_percentage_30_btn.panel = this;
	 	this.discount_percentage_30_btn.onclick = function(e){
	 		this.panel.pp.setDiscount(30);
	 		this.panel.applyDiscount();
	 	};
	 	
	 	this.discount_percentage_40_btn = $('discount.percentage.40');
	 	this.discount_percentage_40_btn.panel = this;
	 	this.discount_percentage_40_btn.onclick = function(e){
	 		this.panel.pp.setDiscount(40);
	 		this.panel.applyDiscount();
	 	};
	 	
	 	this.discount_percentage_50_btn = $('discount.percentage.50');
	 	this.discount_percentage_50_btn.panel = this;
	 	this.discount_percentage_50_btn.onclick = function(e){
	 		this.panel.pp.setDiscount(50);
	 		this.panel.applyDiscount();
	 	};
	 	
	 	
	 	this.discount_cancelBtn = $('discount.cancel.button');
	 	this.discount_total_cancelBtn = $('discount.total.cancel.button');
	 	this.discount_cancelBtn.onclick = function(e){
	 		DiscountPanel.hide();
	 	};
	 	this.discount_total_cancelBtn.onclick = function(e){
	 		DiscountPanel.hide();
	 	}; 
	 	
	 	/* add keypad */
	 	Event.observe(this.unit_excl_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	 	Event.observe(this.unit_incl_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	 	Event.observe(this.total_excl_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	 	Event.observe(this.total_incl_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	 	Event.observe(this.discount_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		
		this.discount_percentage_tf.panel = this;
		this.discount_percentage_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscount();
			}
			
			this.panel.pp.setDiscount(this.value);
			this.panel.render(this);
		};	
		
		this.unit_incl_tf.panel = this;
		this.unit_incl_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscount();
			}
			
			var priceList = parseFloat(this.panel.pp.priceList);
			var oldValue = this.panel.pp.getPrice(priceList, true);
			var newValue = parseFloat(this.value);
			
			var discount = ((oldValue-newValue)/oldValue)*100;
			
			this.panel.pp.setDiscount(discount);
			this.panel.render(this);
		};	
		
		this.unit_excl_tf.panel = this;
		this.unit_excl_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscount();
			}
			
			var priceList = parseFloat(this.panel.pp.priceList);
			var oldValue = this.panel.pp.getPrice(priceList, false);
			var newValue = parseFloat(this.value);
			
			var discount = ((oldValue-newValue)/oldValue)*100;
			
			this.panel.pp.setDiscount(discount);
			this.panel.render(this);
		};
		
		this.total_incl_tf.panel = this;
		this.total_incl_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscount();
			}
			
			var totalPrice = parseFloat(this.panel.pp.priceList * this.panel.pp.qty);
			var oldValue = this.panel.pp.getPrice(totalPrice, true);
			var newValue = parseFloat(this.value);
			
			var discount = ((oldValue-newValue)/oldValue)*100;
			
			this.panel.pp.setDiscount(discount);
			this.panel.render(this);
		};	
		
		this.total_excl_tf.panel = this;
		this.total_excl_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscount();
			}
			
			var totalPrice = parseFloat(this.panel.pp.priceList * this.panel.pp.qty);
			var oldValue = this.panel.pp.getPrice(totalPrice, false);
			var newValue = parseFloat(this.value);
			
			var discount = ((oldValue-newValue)/oldValue)*100;
			
			this.panel.pp.setDiscount(discount);
			this.panel.render(this);
		};	
		
		this.discount_resetBtn.panel = this;
		this.discount_resetBtn.onclick = function(e){
			this.panel.reset();
		};		
		
		this.discount_applyBtn.panel = this;
		this.discount_applyBtn.onclick = function(e){
			this.panel.applyDiscount();
		};
		
		/* discount on total*/
		
		this.discountOnTotal_percentage_tf = $('discountOnTotal.percentageTextfield');
		this.discountOnTotal_amount_tf = $('discountOnTotal.amountTextfield');
		this.discountOnTotal_total_tf = $('discountOnTotal.totalTextfield');
		
		this.discountOnTotal_applyBtn = $('discountOnTotal.applyButton');	
		this.discountOnTotal_resetBtn = $('discountOnTotal.resetButton');
		
		/* add keypad */
		Event.observe(this.discountOnTotal_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(this.discountOnTotal_amount_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(this.discountOnTotal_total_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		
		
		this.discountOnTotal_percentage_tf.panel = this;
		this.discountOnTotal_percentage_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
			baseAmt = baseAmt.plus(discountOnTotal);
			
			var percentage = this.panel.discountOnTotal_percentage_tf.value;
			percentage = parseFloat(percentage);
			
			if(isNaN(percentage)){
				percentage = 0.0;
			}
			
			var discountAmt = baseAmt.times(percentage).dividedBy(100);
			
			var baseAmt = baseAmt.minus(discountAmt);
			
			this.panel.discountOnTotal_amount_tf.value = discountAmt.toFixed(2);
			this.panel.discountOnTotal_total_tf.value = baseAmt.toFixed(2);
			
		};
		
		this.discountOnTotal_amount_tf.panel = this;
		this.discountOnTotal_amount_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
						
			var discountAmt = this.panel.discountOnTotal_amount_tf.value;
			discountAmt = parseFloat(discountAmt);
			
			if(isNaN(discountAmt)){
				discountAmt = 0;
			}
			
			discountAmt = new BigNumber(discountAmt);
			
			var percentage = new BigNumber(0);
			
			if(!baseAmt.isZero()){
				percentage = discountAmt.times(100).dividedBy(baseAmt);
			}			
			
			baseAmt = baseAmt.minus(discountAmt);
			
			this.panel.discountOnTotal_percentage_tf.value = percentage.toFixed(2);
			this.panel.discountOnTotal_total_tf.value = baseAmt.toFixed(2);
		};
		
		this.discountOnTotal_total_tf.panel = this;
		this.discountOnTotal_total_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
			baseAmt = baseAmt.plus(discountOnTotal);
			
			var amtEntered = this.panel.discountOnTotal_total_tf.value;
			
			amtEntered = parseFloat(amtEntered);
			
			if(isNaN(amtEntered)){
				amtEntered = 0;
			}
			
			amtEntered = new BigNumber(amtEntered);
			
			var discountAmt = baseAmt.minus(amtEntered);
			
			var percentage = new BigNumber(0);
			
			if(!baseAmt.isZero()){
				percentage = discountAmt.times(100).dividedBy(baseAmt);
			}				
						
			this.panel.discountOnTotal_percentage_tf.value = percentage.toFixed(2);
			this.panel.discountOnTotal_amount_tf.value = discountAmt.toFixed(2);		
		};
		
		
		this.discountOnTotal_amount_tf.panel = this;
		this.discountOnTotal_total_tf.panel = this;
		
		this.discountOnTotal_applyBtn.panel = this;
		this.discountOnTotal_applyBtn.onclick = function(e){			
			this.panel.applyDiscountOnTotal();
		};
		
		this.discountOnTotal_resetBtn.panel = this;
		this.discountOnTotal_resetBtn.onclick = function(e){
			this.panel.resetDiscountOnTotal();
		};
		
		this.initialized = true;
 	},
 	
 	applyDiscount:function(){
 		/*get discount percentage*/
 		var discountPercentage = this.pp.discount;
 		
 		var rights = DiscountManager.getDiscountRights();
 		var allowUpSell = rights.allowUpSell;
 		
 		if(!DiscountManager.isDiscountValid(discountPercentage))
 		{
 			return;
 		} 		
 		
 		if(allowUpSell){
 			//do nothing
 		}
 		else
 		{
 			if(!DiscountManager.overrideLimitPrice()){
 	 			var priceEntered = parseFloat(this.pp.priceEntered);
 	 	 		var priceLimit = parseFloat(this.pp.priceLimit);
 	 	 		
 	 	 		if(priceLimit > priceEntered){
 	 	 			alert('The Price Limit [' + priceLimit + '] has been exceeded by the Price Entered: ' + priceEntered);
 	 				return;
 	 	 		}
 	 		}
 		}
 		
 		
 		/*get discount amt*/
		var discountAmt = this.pp.getDiscountAmt();
		discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
		discountAmt = parseFloat(discountAmt);
		
		if(isNaN(discountAmt))
		{
			alert('Invalid Discount Amount: ' + discountAmt);
			return;
		}
    		
		ShoppingCartManager.setDiscountOnLine(discountAmt);	
		DiscountPanel.hide();
 	},
 	
 	applyDiscountOnTotal:function(){
 		/*validate discount limit*/ 
		
		var discountPercentage = this.discountOnTotal_percentage_tf.value			
		if(!DiscountManager.isDiscountValid(discountPercentage)){
			return;
		}
		
		var discountAmt = this.discountOnTotal_amount_tf.value;
		discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
		
		if(discountAmt < 0.0){
			alert('Discount Amount cannot be negative!');
			return;
		}
		
		var previousDiscountOnTotal = ShoppingCartManager.getDiscountOnTotal();
		if(!previousDiscountOnTotal.isZero()){
			alert("You have already given discount on total! Please clear previous discount on total. Click reset to clear previous discounts.");
			return;
		}
		
		ShoppingCartManager.setDiscountOnTotal(discountAmt);
		DiscountPanel.hide();
 	},
 	
 	loadLineDetails:function()
 	{
 		if(!this.visible) return;
 		
 		if(ShoppingCartManager.isShoppingCartEmpty()) return;
 		
 		var lines = shoppingCart.getLines();
 		var line = lines[shoppingCart.selectedIndex];
		
 		var priceEntered = line.priceEntered;
		var priceList = line.product.pricelist;
		var priceStd = line.product.pricestd;
		var priceLimit = line.product.pricelimit;
		var isTaxIncluded = shoppingCart.priceListIncludeTax;
		var taxRate = line.tax.taxRate;
		var qty = line.qty;
		
		/*Parse values*/
		isTaxIncluded = new Boolean(isTaxIncluded);
		taxRate = parseFloat(taxRate);
		priceList = parseFloat(priceList);
		priceStd = parseFloat(priceStd);
		priceLimit = parseFloat(priceLimit);
		qty = parseFloat(qty);
		
		priceEntered = parseFloat(priceEntered.toFixed(2));
		
		if(line.product.editpriceonfly == 'Y'){
			priceStd = priceEntered;
			priceList = priceEntered;
		}
		
		this.pp = new PPricing(priceEntered,priceStd,priceList,priceLimit,qty,taxRate,isTaxIncluded); 
		
		this.render(null);
		
		if(DiscountManager.isDiscountOnTotalAllowed())
		{
			/* render discount on total */
			this.renderDiscountOnTotal();
		}
		
 	},
 	
 	isDiscountValid : function(discount){
 		return DiscountManager.isDiscountValid(discount);
 	},
 	
 	renderDiscountRightsInfo:function(){
 		
 		var rights = DiscountManager.getDiscountRights();
 		
 		//$('discountUpToLimitPriceLabel').innerHTML = rights.discountUpToLimitPrice ? 'Yes' : 'No';
 		$('overrideLimitPriceLabel').innerHTML = rights.overrideLimitPrice ? 'Yes' : 'No';
 		$('allowDiscountOnTotalLabel').innerHTML = rights.allowDiscountOnTotal ? 'Yes' : 'No';
 		$('discountLimitLabel').innerHTML = rights.discountLimit + '%';
 		
 		if(rights.allowOrderBackDate && OrderScreen.backDateButton){
 			OrderScreen.backDateButton.show();
		 }
 	},
 	
 	resetDiscountOnTotal:function(){
 		var subTotal = ShoppingCartManager.getSubTotal();
 		subTotal = parseFloat(subTotal.toFixed(2));
 		
 		subTotal = PriceManager.setPrecision(subTotal, 2);		
		this.discountOnTotal_amount_tf.value = 0;
		this.discountOnTotal_percentage_tf.value = 0;
		this.discountOnTotal_total_tf.value = subTotal;
		
		if(window.confirm("This will reset all the discount given. Do you want to continue?")){
			/* clear previous discounts */
			ShoppingCartManager.setDiscountOnTotal(0);
		}		
		
 	},
 	
 	renderDiscountOnTotal:function(){
 		
 		var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
		
		var discountAmt = ShoppingCartManager.getDiscountOnTotal();
 		var percentage = (discountAmt * 100)/(baseAmt + discountAmt);
 		
 		if(isNaN(percentage)) percentage = 0;
 		
		percentage = PriceManager.setPrecision(percentage, 2);
		discountAmt = PriceManager.setPrecision(discountAmt, 2);
		
		this.discountOnTotal_amount_tf.value = discountAmt;
		this.discountOnTotal_percentage_tf.value = percentage;
		this.discountOnTotal_total_tf.value = baseAmt;
 	},
 	
 	render:function(elementNotToRender)
 	{
 		var unitPrice = this.pp.priceEntered;
		var totalPrice = this.pp.getTotal();
		
		if(elementNotToRender != this.unit_excl_tf)
		this.unit_excl_tf.value = Number(this.pp.getPrice(unitPrice, false)).toFixed(2);
		
		if(elementNotToRender != this.unit_incl_tf)
		this.unit_incl_tf.value = Number(this.pp.getPrice(unitPrice, true)).toFixed(2);	
		
		if(elementNotToRender != this.total_excl_tf)
		this.total_excl_tf.value = Number(this.pp.getPrice(totalPrice, false)).toFixed(2);
		
		if(elementNotToRender != this.total_incl_tf)
		this.total_incl_tf.value = Number(this.pp.getPrice(totalPrice, true)).toFixed(2);
		
		if(elementNotToRender != this.discount_percentage_tf)
		this.discount_percentage_tf.value = Number(this.pp.getDiscount()).toFixed(2);
 	},
 	
 	reset:function(){
 		this.pp.priceEntered = this.pp.priceStd;
 		this.render(null);
 	}
 };
 
 

 
 var DiscountManager = {
 		
 		defaultDiscountRights:null,
 		discountRights:null,
 		
		 /* set discount amt on cart total */
		 setDiscountOnTotal : function(discountAmt){
			 ShoppingCartManager.setDiscountOnTotal(discountAmt);
		 },
		 
		 isDiscountOnTotalAllowed : function(){
			 
			 var rights = DiscountManager.getDiscountRights();	
			 
			 return rights.allowDiscountOnTotal;
		 },
		 
		 isDiscountOnLineAllowed : function(){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
					 
			 if(rights.discountLimit > 0.0) return true;
			 
			 var msg = messages.get('Discount Limit has not been set!'); 
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 overrideLimitPrice : function(){
			 
			 var rights = DiscountManager.getDiscountRights();	
			 
			 return rights.overrideLimitPrice;
		 },
		 
		 allowOrderBackDate : function(){
			 var rights = DiscountManager.getDiscountRights();				 
			 return rights.allowOrderBackDate;
		 },
		 
		 showDiscountOnTotalPanel : function(){
			 $('discountOnTotalDetails').show();
		 },
		 
		 hideDiscountOnTotalPanel : function(){
			 $('discountOnTotalDetails').hide();
		 },
		 
		 isDiscountValid : function(discountPercentage){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
			 
			 /* user can give discount */
			 if(discountPercentage <= rights.discountLimit) return true;
			 
			 /* throw error message*/
			 var msg = messages.get('Discount is greater than discount limit!');
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 getDiscountRights : function(){
			 return this.discountRights;
		 },
		 
		 setDiscountRights : function(discountRights){
			 if(this.defaultDiscountRights == null) this.defaultDiscountRights = discountRights;
			 this.discountRights = discountRights;
			 
			 /* notifies changes in discount rights */
			 this.discountRightsChangeNotifier();
		 },
		 
		 resetDiscountRights:function(){
		 	this.setDiscountRights(this.defaultDiscountRights);
		 },
		 
		 discountRightsChangeNotifier : function(){
			 /* notifies changes in discount rights */
			 
			 if(!DiscountPanel.visible) return;
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 if(rights.discountLimit <= 0.0){
				 DiscountPanel.hide();
			 }
			 else
			 {
				 if(!rights.allowDiscountOnTotal){
					 this.hideDiscountOnTotalPanel();
				 }
				 else{
					 this.showDiscountOnTotalPanel();
				 }
			 }
			 
			 DiscountPanel.renderDiscountRightsInfo();			 
		 },
		 
		 reportError:function(err){
			 alert(err);
		 }
 };
 
var PriceManager = {
		setPrecision : function(price, precision){
			return parseFloat(new Number(price).toFixed(precision));
		}
};
 
var DiscountRights =  Class.create({
	initialize : function(discountLimit, overrideLimitPrice, discountUpToLimitPrice, allowDiscountOnTotal, allowOrderBackDate, allowUpSell){
		this.discountLimit = discountLimit;
		this.overrideLimitPrice = overrideLimitPrice;
		this.discountUpToLimitPrice = !overrideLimitPrice;
		this.allowDiscountOnTotal = allowDiscountOnTotal;
		this.allowOrderBackDate = allowOrderBackDate;
		this.allowUpSell = allowUpSell;
	}
});