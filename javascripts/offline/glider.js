var gNode = function(parent,key,name){
    this.parent = parent;
    this.key = key;
    this.name = name;
    this.children = new Hash();
    /*link child to parent*/
    if(this.parent != null){
    	this.id = this.parent.id + "_" + this.parent.children.size();
    	this.parent.children.set(this.name, this);			
    }
    else
    {
    	this.id = "tile";
    }
};

gNode.prototype.get = function(name){
	return this.children.get(name);
};

gNode.prototype.getQuery = function(){
	var query = "";
	
	var parent = this;
	
	do
	{
		var value = parent.name;
		/*escape value*/
		value = escape(value);
		query = query + parent.key + "=" + value + ";";			
		parent = parent.parent;
		
	} while(parent.parent != null);
	
	return query;
};

gNode.prototype.getHTML = function(){
	var html = "";
	
	var children = this.children.values();
	
	if(children.length == 0) return html;
	
	html = html + "<ul id=\"" + this.id + "\" class='jGlide_001_tiles'>\n";
	
	for(var i=0; i<children.length; i++)
	{
		var child = children[i];
		
		if(child.children.keys().length == 0)
		{
			html = html + "\t<li><a href=\"javascript:void(0);\" clickEvent=\"SearchProductFilter.setSelectionQuery('" + child.getQuery() + "');\">" + child.name + "</a></li>\n";
		}
		else
		{
			/* Sendy Modifications case 2652 */
			html = html + "\t<li rel=\"" + child.id + "\" clickEvent=\"SearchProductFilter.setSelectionQuery('" + child.getQuery() + "');\">" + child.name + "</li>\n";
		}
		
	}
	
	html = html + "</ul>\n";
	
	var values = this.children.values();
	
	for(var k=0; k<values.length; k++)
	{
		var child = values[k];
		html = html + child.getHTML();
	}
	
	return html;
};

var ProductGlider = {
		getGlider:function(){
			var results = APP.PRODUCT.searchProducts({},10000);
			var lines = new Hash();

			for(var i=0; i<results.length; i++){
			    var product = results[i];
			    var line = null;
			    
			    if(product.primarygroup){
			        line = product.primarygroup;
			        
			        if(product.group1){
			            line += '-' + product.group1;
			            
			            if(product.group2){
			                line += '-' + product.group2;
			                
			                if(product.group3){
			                    line += '-' + product.group3;
			                    
			                    if(product.group4){
			                        line += '-' + product.group4;
			                    }
			                }
			            }
			        }
			    }
			    
			    if(line){
			        lines.set(line,'*');
			    }    
			}

			var lines = lines.keys();
			lines.sort();

			var root = new gNode(null, "tile", "tile");
			var left = null;
			var right = null;

			var columns = ['primarygroup','group1','group2','group3','group4'];
						
			for(var j=0; j<lines.length; j++){

			    var line = lines[j];
			    var groups = line.split('-'); 
			    
			    left = root;
				right = null;
							
			    for(var k=0; k<groups.length; k++){
			        var columnName = columns[k];
					var columnValue = groups[k];					
											
					right = left.get(columnValue);
								
								if(right == null)
								{
									right = new gNode(left, columnName, columnValue);					
								}
								
								left = right;
								right = null;
			    }
			}

			return root.getHTML();
		}
};


