
function loadIWantTo(quickMenus)
{
	var ul = document.createElement('ul');
	var styleClass = 'even';
	
	for (var i=0; i<quickMenus.length; i++)
	{
		var quickMenu = quickMenus[i];
		if (quickMenu == null)
			continue;
		
		var name = quickMenu.name;
		var menuLink = quickMenu.menuLink;

		var li = document.createElement('li');
		var a = document.createElement('a');

		a.setAttribute('class', styleClass);
		a.setAttribute('href', menuLink);
		a.innerHTML = name;

		li.appendChild(a);
		ul.appendChild(li);
		
		styleClass = (styleClass == 'even')? 'odd' : 'even';
	}
		
	$('quickMenuItems').innerHTML = '';
	$('quickMenuItems').appendChild(ul);

	jQuery(document).ready(function(){
		jQuery(".dropdown dt a").click(function() {
			jQuery(".dropdown dd ul").toggle();
		});
		jQuery(".dropdown dd ul li a").click(function() {
		    var text = jQuery(this).html();
		    jQuery(".dropdown dd ul").hide();
		});
		jQuery(document).bind('click', function(e) {
		    var clicked = jQuery(e.target);
		    if (! clicked.parents().hasClass("dropdown"))
		    	jQuery(".dropdown dd ul").hide();
		});
	});
}