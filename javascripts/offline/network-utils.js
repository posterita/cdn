/* network utils */
var NetworkMonitor = {
		isOnline : true,
		isServerOnline : true,
		serverResponseStatus : 'success',
		frequency : 5000,
		decay : 1000,
		isServerAccessible : true,
		isAccessible : function(){
			this.isOnline = navigator.onLine;
			console.log('Current network status: ' + ((this.isOnline == true) ? 'online' : 'offline'));
			return this.isOnline;
		},
		
		setIsOnline : function(status){
			this.isOnline = status;
			console.log('Current network status: ' + ((this.isOnline == true) ? 'online' : 'offline'));
			this.networkStateChange(this.isOnline);
		},	
		
		setIsServerAccessible : function(isServerAccessible){
			if(this.isServerAccessible != isServerAccessible){
				this.isServerAccessible = isServerAccessible;
				console.log('App server status: ' + ((this.isServerAccessible == true) ? 'up' : 'down'));				
				this.serverStateChange(this.isServerAccessible);
			}						
		},
		
		pingServer : function() {
			
			console.debug('ping server ...');
			
			NetworkMonitor.frequency = NetworkMonitor.frequency + NetworkMonitor.decay;
			
			
			if(NetworkMonitor.isOnline == false){
				NetworkMonitor.isServerAccessible = false;
				this.monitorServerTimeoutHandle = window.setTimeout(NetworkMonitor.pingServer,NetworkMonitor.frequency);
				return;
			}
			
			jQuery.ajax({
			    cache:false,
			    timeout:4000,
			    type:"HEAD",
			    url:'jsp/offline/dummy.jsp?' + Math.random(),
			    complete:function(xhr, status){ 
			    	var isServerAccessible = (xhr.status == 200);
			    	NetworkMonitor.serverResponseStatus = status;
				    NetworkMonitor.setIsServerAccessible(isServerAccessible);
				    this.monitorServerTimeoutHandle = window.setTimeout(NetworkMonitor.pingServer,NetworkMonitor.frequency);
			    }
			   });
			
			/*
			new Ajax.Request('offline.appcache?' + Math.random(), {
				  method:'head',
				  onComplete: function(response){
				    var status = response.status;
				    var isServerAccessible = (status == 200);
				    NetworkMonitor.setIsServerAccessible(isServerAccessible);
				  }
			});
			*/
		},
		
		monitorServer : function(){
			if(this.monitorServerTimeoutHandle){
				window.clearTimeout(NetworkMonitor.monitorServerTimeoutHandle)
			}
			
			console.debug('monitoring app server ...');
			this.monitorServerTimeoutHandle = window.setTimeout(NetworkMonitor.pingServer,this.frequency);
		},
		
		stopMonitorServer : function(){
			console.info('stopping app server monitoring ...');
			window.clearTimeout(NetworkMonitor.monitorServerTimeoutHandle)
		},
		
		networkStateChange : function(isAccessible){
			if(this.networkDialog == null){
				this.networkDialog = new Dialog('Your connection may have been interrupted. Please check your internet connection.');
				 $.pnotify({    
				        text: 'You are online',
				        type: 'success',
				        icon: 'icon-signal'
				        });
			}
			
			if(isAccessible == false){
				this.networkDialog.show();
			}else
			{
				this.networkDialog.hide();
			}
		},
				
		serverStateChange : function(isAccessible){
			if(this.serverDialog == null){
				this.serverDialog = new Dialog('Unable to reach ' + window.location.host + '. Please check your internet connection.');
			}
			
			if(isAccessible == false){
				this.serverDialog.show();
			}else
			{
				this.serverDialog.hide();
			}
		}
		
};

jQuery.noConflict();

jQuery(document).ready(function($){ 
	window.addEventListener("online", function(e) {
		NetworkMonitor.setIsOnline(navigator.onLine);
	});
	window.addEventListener("offline", function(e) {
		NetworkMonitor.setIsOnline(navigator.onLine);
	});
});