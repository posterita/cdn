/* check network status */

jQuery(document).ready(function($){

	if (!window.applicationCache) {
		console.info("HTML5 offline web applications (ApplicationCache) are not supported in your browser.");
        return;
    }
	
	var online = function(event){
		var status = navigator.onLine ? 'online' : 'offline';
		var type = event.type;
		console.info('Current network status: ' + status);
	};
	   
	$(window).on('online', online);
	$(window).on('offline', online);
	   
	console.info('Monitoring network status');
	
});


/* manfiest parsing */


posterita.offline.extractManifestURL = function() {
    var url = jQuery("html").attr("manifest");
    if(url === null) {
        alert("Preloader cannot find manifest URL from <html> tag");
        return null;
    }
    
    jQuery.get(url, {}, jQuery.proxy(posterita.offline.manifestLoadedCallback, this));
};

posterita.offline.manifestLoadedCallback = function(data, textStatus, xhr) { 
    this.debug("Manifest retrieved");
    manifestEntries = this.parseManifest(xhr.responseText); 
    this.debug("Parsed manifest entries:" + manifestEntries.length);
    
    posterita.offline.manifestEntries = manifestEntries;
};

posterita.offline.parseManifest = function(data) {
	
    function startswith(str, prefix) {
        return str.indexOf(prefix) === 0;
    }

    var entries = new jsSet();

    var sections = ["NETWORK", "CACHE", "FALLBACK"];
    var currentSection = "CACHE";

    var lines = data.split(/\r\n|\r|\n/);
    var i;

    if(lines.length <= 1) {
        throw "Manifest does not contain text lines";
    }

    var firstLine = lines[0];
    if(!(startswith(firstLine, "CACHE MANIFEST"))) {
        throw "Invalid cache manifest header:" + firstLine;
    }

    for(i=1; i<lines.length; i++) {

        var line = lines[i];
        /*this.debug("Parsing line:" + line);*/

        // If whitespace trimmed line is empty, skip it
        line = jQuery.trim(line);
        if(line == "") {
            continue;
        }

        if(line[0] == "#") {
            // skip comment;
            continue;
        }

        // Test for a new section
        var s = 0;
        var sectionDetected = false;
        for(s=0; s<sections.length; s++) {
            var section = sections[s];
            if(startswith(line, section + ":")) {
                currentSection = section;
                sectionDetected = true;
            }
        }

        if(sectionDetected) {
            continue;
        }

        // Otherwise assume we can check for cached url
        if(currentSection == "CACHE") {
        	this.debug("Pushing line:" + line);
            entries.add(line); 
        }

    }

    return entries.list();
};

posterita.offline.debug = function(log){
	console.log(log);
};

var jsSet = function() {

    // null can also be an element of the set, but needs
    // a separate indication to differentiate it from
    // the string "null" as well
    this.isNullAdded = false;

    // private member variable hence no 'this'
    var map = {};

    //  Scope for optimization
    //  could be cached instead of generating each time
    //  this.uniqueList = [];

    //  returns true if the element is in this set, false otherwise
    this.contains = function(key) {

        if (key === null)
            return this.isNullAdded;
        else if (key === undefined)
            return false;
        else
            return map[key] ? true : false;
    };

    //  adds the element to the set
    this.add = function(val) {

        if (val === null)
            this.isNullAdded = true;
        else if (val !== undefined)
            map[val] = true;
        return this;
    };

    //  adds all the elements of the array to the set
    this.addAll = function(val) {

        if (val !== null && val !== undefined && val instanceof Array) {
            for ( var idx = 0; idx < val.length; idx++) {
                this.add(val[idx]);
            }
        }
        return this;
    };

    //  removes the specified element from the set
    this.remove = function(val) {
        if (val === null)
            this.isNullAdded = false;
        else if (val !== undefined)
            delete map[val];
        return this;
    };

    //  removes all the element in the array from the set
    this.removeAll = function(val) {

        if (val !== null && val !== undefined && val instanceof Array) {
            for ( var idx = 0; idx < val.length; idx++) {
                console.log('val: %s:%s', idx, val[idx]);
                this.remove(val[idx]);
            }
        }
        return this;
    };

    //  empties the set of all values
    this.clear = function() {

        this.isNullAdded = false;
        map = {};
        return this;
    };

    //  returns the number of elements in the set
    this.size = function() {

        return this.list().length;
    };

    //  returns true if the set is empty, false otherwise
    this.isEmpty = function() {

        return this.list().length > 0? false: true;
    };

    //  returns the elements of the set as a list
    this.list = function() {
        var arr = [];

        if (this.isNullAdded)
            arr.push(null);

        for (o in map) {
            // protect from inherited properties such as
            //  Object.prototype.test = 'inherited property';
            if (map.hasOwnProperty(o))
                arr.push(o);
        }
        return arr;
    };
};
