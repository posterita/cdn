var OrderManager = {
		initialised : false,
		
		init : function(){
			if(this.initialised) return;
			
			this.loadOrders();			
			this.initialised = true;
		},
		
		loadOrders : function(){
			this.orders = null;		
			console.log('[OrderManager] Loading orders');
			jQuery.db.values('order', null, 10000).done(function(records){
				OrderManager.orders = records;
				console.log('[OrderManager] Found ' + records.length + ' orders.');
				
				OrderManager.onReady();
		     });				
		},
		
		saveOrder : function(order){
			this.init();
			
			if(this.orders == null){
				this.orders = [];
			}
			
			jQuery.db.put('order', order).done(function(key){
				var UUID = key;
				
				jQuery.db.get('order', UUID).done(function(record) {
					OrderManager.orders.push(record);
					OrderManager.onSave(true, record);					
					
				}).fail(function(e) {					
					throw e;
				});				
				
			}).fail(function(e) {
				OrderManager.onSave(false);
				throw e;
			});		
			
		},
		
		getOrders : function(){
			this.init();
			return this.orders;
		},
		
		getOrderByUUID : function(UUID){
			this.init();						
			
			for(var i=0; i<this.orders.length; i++){
				var order = this.orders[i];
				if(order.uuid == UUID){
					return order;
				}
			}
			
			return null;
		},
		
		getOrderByReference : function(reference){
			this.init();						
			
			for(var i=0; i<this.orders.length; i++){
				var order = this.orders[i];
				if(order.reference == reference){
					return order;
				}
			}
			
			return null;
		},
		
		getOrderByOfflineDocumentNo : function(offlineDocumentNo){
			this.init();						
			
			for(var i=0; i<this.orders.length; i++){
				var order = this.orders[i];
				if(order.offlineDocumentNo == offlineDocumentNo){
					return order;
				}
			}
			
			return null;
		},
		
		syncOrders : function(){
			this.init();
			
			var url = 'OrderAction.do';
			var pars = new Hash();
			pars.set('action','processOfflineOrders');
			pars.set('json', this.orders.toJSON());
			
			var myAjax = new Ajax.Request( url, 
			{ 
			method: 'post',
			parameters: pars,
				onSuccess: function(request){
					var response = request.responseText;
					var sinkedOrders = JSON.parse(response);
					
					for(var i=0; i<sinkedOrders.length; i++){
						var uuid = sinkedOrders[i];						
					}
					alert("Orders sinked");
				}, 
				onFailure: function(request){
					alert("Failed to sink orders!");
				}
			});
		},
		
		getTaxes : function(order){
			if(order.taxes) return order.taxes;
			
			var orderTaxes = order.orderTaxes;
			var taxes = [];
			
			for(var i=0; i<orderTaxes.length; i++){
				var taxId = parseInt(orderTaxes[i].taxId);
				var taxAmt = parseFloat(orderTaxes[i].taxAmt);
				var tax = TaxManager.getTaxById(taxId);
				
				if(tax.subTaxes == null){
					tax.subTaxes = [];
				}
				
				if(tax.subTaxes.length == 0){
					taxes.push({
						'name'	: tax.taxName,
						'amt'	: taxAmt
					});
				}
				else
				{
					for(var j=0; j<tax.subTaxes.length; j++){
						var subTax = tax.subTaxes[j];
						
						var subTaxAmt = new BigNumber(0);
						
						if(tax.taxRate != 0){
							subTaxAmt = new BigNumber(taxAmt).times(subTax.subTaxRate).dividedBy(tax.taxRate);
						}
						
						taxes.push({
							'name'	: subTax.subTaxName,
							'amt'	: subTaxAmt.toFixed(3)
						});
					}
				}
				
			}
			
			order.taxes = taxes;
			return taxes;
		},
		
		printOrder : function(order){
			var terminal = TerminalManager.terminal;
			if(terminal.id != order.terminalId){
				alert('Cannot print receipt of another terminal!');
				return;
			}
			
			JSReceiptFormatter.options = {LINE_WIDTH:60};
		    JSReceiptFormatter.data = { 
		      "comments" : [],
		      "commissions" : [],
		      "header" : { 	
		          "client" : terminal.clientName,
		    	  "orgName" : terminal.orgName,
		    	  "orgAddress1" : terminal.orgInfo.address1,
		          "orgAddress2" : terminal.orgInfo.address2,
		          "orgAddress3" : terminal.orgInfo.address3,
		          "orgAddress4" : terminal.orgInfo.address4,
		          "orgCity" : terminal.orgInfo.city,
		          "orgFax" : terminal.orgInfo.fax,    
		          "orgPhone" : terminal.orgInfo.phone,
		          "orgTaxId" : terminal.orgInfo.taxNo,
		          
		          "title" : "Sales Receipt",
		          "soTrx" : true,
		          "bpName" : order.bpName,
		          "bpName2" : " ",
		          "terminal" : terminal.name,
		          "salesRep" : order.salesRep,
		          "docStatus" : (order.prepareOrder == 'false') ? "CO" : "DR",
		          "docStatusName" : (order.prepareOrder == 'false') ? "Completed" : "Drafted",
		          "paymentRule" : "?",
		          "paymentRuleName" : order.tenderType,
		          "dateOrdered" : order.dateOrdered,
		          "currencySymbol" : terminal.currency,
		          "documentNo" : order.offlineDocumentNo,
		    	  
		    	  "amountRefunded" : order.amountRefunded,
		          "amountTendered" : order.amountTendered,
		          
		          "cardAmt" : "0",
		          "cardType" : "",
		          "cashAmt" : order.cashAmt,
		          "chequeAmt" : "0",
		          
		          
		          "discountAmt" : order.discountAmt,
		          "documentNo" : order.reference,
		          "externalCardAmt" : order.externalCardAmt,
		          "giftCardAmt" : "0",
		          "openAmt" : "0.00",
		          "orderType" : "POS Order",      
		          
		          "payAmt" : order.grandTotal,      
		          "qtyTotal" : order.qtyTotal,      
		          "taxTotal" : order.taxTotal,      
		          "totalLines" : order.subTotal,
		          "grandTotal" : order.grandTotal,
		          "voucherAmt" : "0",
		          "writeOffAmt" : "0",          
		          
		          "creditCardDetails" : ''
		        },
		        
		      "lines" : order.lines,
		      
		      "payments" : [ { "cardType" : "--",
		            "discountAmt" : 0,
		            "overUnderAmt" : 0,
		            "payAmt" : order.grandTotal,
		            "tenderType" : "?",
		            "writeOffAmt" : 0
		          } ],
		          
		      "taxes" : OrderManager.getTaxes(order)
		    };
		    
		    var receiptRaw = JSReceiptFormatter.formatAsRaw();
			 
			var printJob = receiptRaw;
		    var centerAlignment = '\x1B\x61\x01';
		    var imageCommand = '\x1C\x70\x01\x30\x0A';
		     
		    printJob = centerAlignment + imageCommand + printJob;
		     
		    PrinterManager.sendPrintJob(printJob);
		},
		
		onReady : function(){},
		
		onSave : function(saved, order){}
};

var UUIDGenerator = {
		getID : function(){
			var uuid = "", i, random;
			for (i = 0; i < 32; i++) {
				random = Math.random() * 16 | 0;

				if (i == 8 || i == 12 || i == 16 || i == 20) {
					uuid += "-"
				}
				uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
			}
			return uuid;
		}
}; 

