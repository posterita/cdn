var OrderScreen = {					
		setTenderType : function(tenderType){
			this.tenderType = tenderType;
		},
		
		setDeliveryRule : function(deliveryRule){
			this.deliveryRule = deliveryRule;
		},
		
		setPaymentTermId : function(paymentTermId){
			this.paymentTermId = paymentTermId;
		},
		
		getPaymentTermId : function(){
			return this.paymentTermId;
		},
		
		setPaymentRule : function(paymentRule){
			
			switch (paymentRule) {
			
			case PAYMENT_RULE.CASH:	
				this.setTenderType(TENDER_TYPE.CASH);
				break;
			
			case PAYMENT_RULE.CARD:	
				this.setTenderType(TENDER_TYPE.CARD);
				break;
				
			case PAYMENT_RULE.CHEQUE:	
				this.setTenderType(TENDER_TYPE.CHEQUE);
				break;
				
			case PAYMENT_RULE.MIXED:	
				this.setTenderType(TENDER_TYPE.MIXED);
				break;
				
			case PAYMENT_RULE.CREDIT:	
				this.setTenderType(TENDER_TYPE.CREDIT);
				break;
			
			case PAYMENT_RULE.VOUCHER:
				this.setTenderType(TENDER_TYPE.VOUCHER);
				break;
				
			case PAYMENT_RULE.EXTERNAL_CARD:
				this.setTenderType(TENDER_TYPE.EXTERNAL_CARD);
				break;
				
			case PAYMENT_RULE.GIFT_CARD:
				this.setTenderType(TENDER_TYPE.GIFT_CARD);
				break;
				
			default:
				break;
			}
		},
		
		setPayment : function(payment){
			this.payment = payment;			
			this.checkout();
		},
		
		setIsPaymentOnline : function(isOnline){
			this.isPaymentOnline = isOnline;
		},
		
		getPaymentDetails : function(){
			
			/*validate bp*/
			if(BPManager.getBP() == null){
				alert(((OrderScreen.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');
				return;
			}
			
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert('Cart is empty!');
				return;
			}
			
			if(!ShoppingCartManager.isShoppingCartReady()){
				alert('Cart is not ready!');
				return;
			}
			
			var isRefund = (this.getCartTotal() < 0.0) || (this.orderType == ORDER_TYPES.CUSTOMER_RETURNED_ORDER) || (this.orderType == ORDER_TYPES.POS_GOODS_RETURNED_NOTE);
						
			switch (this.tenderType) {
				
				case TENDER_TYPE.CASH:	
					if(isRefund){
						var payment = new Hash();
					  	payment.set('amountTendered', this.getCartTotal());
					  	payment.set('amountRefunded', 0.0);  	
					  	this.payment = payment;
					  	this.checkout();
						break;
					}
					
					var panel = new CashPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.EXTERNAL_CARD:
					/* manual creditcard processing */	
					
					if((OrderScreen.skipExternalCardPopUp && OrderScreen.skipExternalCardPopUp == true) 
							|| (countryId != '100' && countryId != '109'))
					{
						/*this.setExternalCardPayment(); //this method is called on checkout. See OrderScreen->checkout*/
						this.checkout();
						break;
					}
					
					if ((countryId == '100' || countryId == '109') && OrderScreen.paymentProcessor == '')
					{
						panel = new ExtCreditCardPanel();
						panel.paymentHandler = this.paymentHandler.bind(this);
						panel.show();
						break;
					}
					
					this.checkout();
					break;
				
				case TENDER_TYPE.CARD:	
					alert('You are currently offline!');
					break;
					
					var panel = null;				
					
					if(OrderScreen.paymentProcessor == '' && (countryId == '100' || countryId == '109')){
						//alert("Payment processor not configured!");
						//return;	
						panel = new ApplyPaymentProcessorPanel();
						panel.show();
					}
					
					
					
					if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_XWeb"){
						panel = new XWebPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_HF")
					{						
						if (ORDER_TYPES.CUSTOMER_RETURNED_ORDER == ShoppingCartManager.getOrderType())
						{
							var payment = new Hash();
							payment.set('cardType', 'M');
							this.payment = payment;
							this.checkout();
							
							return;
						}
						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert("The configured payment processor only supports sales transactions.");
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert("The configured payment processor does not support negative sales transactions.");
							return;	
						}
						
						panel = new MercuryPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS")
					{
						if (ORDER_TYPES.CUSTOMER_RETURNED_ORDER == ShoppingCartManager.getOrderType())
						{
							var payment = new Hash();
							payment.set('cardType', 'M');
							this.payment = payment;
							this.checkout();
							
							return;
						}
						
						if(ORDER_TYPES.POS_ORDER == ShoppingCartManager.getOrderType())
						{
							panel = new ElementPSPanel();	
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0)
						{
							alert("The configured payment processor does not support negative sales transactions.");
							return;	
						}
						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert("The configured payment processor only supports sales transactions.");
							return;						
						}
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_EE")
					{
						var panel = new E2EPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Century")
					{
						var panel = new CenturyPanel();					
					}
					
					else{
						panel = new CardPanel();
					}
					
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
				case TENDER_TYPE.CHEQUE:
					var panel = new ChequePanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
				case TENDER_TYPE.MIXED:
					/* Add support for mix payment */
					/*
					alert('You are currently offline!');
					break;
					*/
					
					if(isRefund){
						alert("Mix tender type is not supported for refunds.");
						return;
					}
					
					var panel = new DeliveryPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.CREDIT:
					alert('You are currently offline!');
					break;
					//validate BP
					var bp = BPManager.getBP();
					var amt = ShoppingCartManager.getGrandTotal();
					
					var isValid = false;
					
					if(this.isSoTrx == true){
						isValid = BPManager.validateCreditStatus(bp, amt);
					}
					else{
						/*don't check credit status for vendor*/
						isValid = true;
					}
					
					if(!isValid){
						return;
					}
					
					/*var paymentTermPanel = new PaymentTermPanel();
					paymentTermPanel.setPaymentTermId = this.setPaymentTermId.bind(this);
					paymentTermPanel.show();*/
					var panel = new DeliveryPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();	
					break;
					
				case TENDER_TYPE.VOUCHER:
					alert('You are currently offline!');
					break;
					
					if(isRefund){
						var payment = new Hash();
					  	payment.set('voucherAmt', this.getCartTotal());
					  	payment.set('voucherNo', null);
					  	payment.set('amountRefunded', 0.0);  	
					  	this.payment = payment;
					  	this.checkout();
						break;
					}
					var panel = new VoucherPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.GIFT_CARD:
					alert('You are currently offline!');
					break;
					
					var panel = new GiftCardPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				default:
					break;
			}
		},
		
		paymentHandler:function(payment){
			this.payment = payment;			
			this.checkout();
		},
		
		setDateOrdered:function(){
			
			if(this.dateOrdered){
				$("dateOrdered").value = this.dateOrdered;
			}
			else{
				var today = new Date();
					
				var year = today.getFullYear();
				var month = today.getMonth() + 1;					
				var date = today.getDate();
				var hours = today.getHours();
				var minutes = today.getMinutes();
				var seconds = today.getSeconds();
				
				if(month < 10) month = '0' + month;
				if(date < 10) date = '0' + date;
				if(hours < 10) hours = '0' + hours;
				if(minutes < 10) minutes = '0' + minutes;
				if(seconds < 10) seconds = '0' + seconds;
				
				var dateOrdered = year + "-" + month + "-" + date +
					" " + hours + ":" + minutes + ":" + seconds;
				
				$("dateOrdered").value = dateOrdered;
			}
		},
		
		checkout : function(){
			/* do normal submit */ 
			
			//1. reset form
			$("orderId").value = "0";
			$("bpartnerId").value = "0";
			$("docAction").value="CO";
			$("orderType").value="POS Order";
			$("tenderType").value="Cash";	
			$("isPaymentOnline").value="true";
			
			$("cashAmt").value="0";
			$("cardAmt").value="0";
			$("chequeAmt").value="0";
			$("externalCardAmt").value="0";
			
			$("voucherAmt").value="0";
			$("voucherNo").value="";
			
			$("amountTendered").value="0";
			$("amountRefunded").value="0";
			
			$("chequeNo").value="";
			
			$("cardTrackData").value="";
			$("cardTrackDataEncrypted").value="";
			$("cardType").value="";
			$("cardholderName").value="";
			$("cardExpDate").value="";
			$("cardCVV").value="";
			$("cardStreetAddress").value="";
			$("cardZipCode").value="";
			$("cardNo").value="";
			
			$("documentNo").value = "";
			$("referenceNo").value = "";
			$("dateOrdered").value = "";
			
			$("giftCardAmt").value = "0";
			$("giftCardTrackData").value = "";
			$("giftCardNo").value = "";
			$("giftCardCVV").value = "";
			
			//2. populate form
			$("orderType").value = this.orderType;
			$("deliveryRule").value = 'R';
			$("paymentTermId").value = 0;
			
			var bp = BPManager.getBP();
			if(bp == null)
			{
				if(this.isSoTrx)
				{
					alert('Choose a customer!');					
				}
				else
				{
					alert('Choose a vendor!');
				}
				
				return;
			}
			
			$("bpartnerId").value = bp.getId();
			$('orderId').value = ShoppingCartManager.getOrderId();
			
			if (OrderScreen.splitManager != null)
			{
				var splitSalesReps = OrderScreen.splitManager.getSplitSalesReps()
				var refSalesRep = OrderScreen.splitManager.getRefSalesRep();
				
				if (splitSalesReps != null)
				{
					if (refSalesRep != null)
						splitSalesReps.add(refSalesRep);
					$('splitSalesReps').value =  splitSalesReps.toJSON();
				}
				else
					$('splitSalesReps').value = "";
			}
			
			if(this.documentNumber) $("documentNo").value = this.documentNumber;			
			if(this.referenceNumber) $("referenceNo").value = this.referenceNumber;
			
			//set date ordered
			this.setDateOrdered();
			
			switch (this.tenderType) {
				case TENDER_TYPE.CASH :	
					this.setCashPayment();			
					break;
					
				case TENDER_TYPE.CHEQUE :
					this.setChequePayment();
					break;
					
				case TENDER_TYPE.CARD :
					this.setCardPayment();			  
					break;
					
				case TENDER_TYPE.EXTERNAL_CARD :
					this.setExternalCardPayment();			  
					break;
					
				case TENDER_TYPE.MIXED :
					this.setMixPayment();					
					break;
					
				case TENDER_TYPE.CREDIT :
					this.setOnCreditPayment();					
					break;
					
				case TENDER_TYPE.VOUCHER :
					this.setVoucherPayment();					
					break;
					
				case TENDER_TYPE.GIFT_CARD :
					this.setGiftCardPayment();					
					break;

				default:
					break;
			}				
			
			$('tenderType').value = this.tenderType;			
			
			/*
			$('order-form').submit();	
			this.processDialog.show();
			*/
			this.offlineCheckout();
		},
		
		offlineCheckout : function(){
			/*offline*/
			var UOM_PRECISION = 0;
			var CURRENCY_PRECISION = 2;
			
			var order = $('order-form').serialize(true);			
			
			var lines = [];
			shoppingCart.lines.each(function(pair) {
				var line = {};
				line.id = pair.value.product.m_product_id;
				line.qtyEntered = pair.value.qty.toFixed(UOM_PRECISION);				
				line.productName = pair.value.product.name;
				line.description = pair.value.description;
				line.priceEntered = pair.value.priceEntered.toFixed(CURRENCY_PRECISION);
				line.taxId = pair.value.tax.taxId;
				line.discountAmt = pair.value.discountAmt.toFixed(CURRENCY_PRECISION);
				line.discountMessage = pair.value.discountMessage;
				
				if(shoppingCart.priceListIncludeTax){
					line.lineNetAmt = pair.value.lineNetAmt.toFixed(CURRENCY_PRECISION);
				}else{
					line.lineNetAmt = pair.value.lineAmt.toFixed(CURRENCY_PRECISION);
				}				
				
				/* set boms */
				line.boms = [];
				
				var boms = pair.value.boms;
				
				for(var i=0; i<boms.length; i++){					
					var bomLine = {};
					var bom = boms[i];
					
					bomLine.id = bom.product.m_product_id;
					bomLine.qtyEntered = bom.qty.toFixed(UOM_PRECISION);				
					bomLine.productName = bom.product.name;
					bomLine.description = bom.description;
					bomLine.priceEntered = bom.priceEntered.toFixed(CURRENCY_PRECISION);
					bomLine.taxId = bom.tax.taxId;
					
					if(shoppingCart.priceListIncludeTax){
						bomLine.lineNetAmt = bom.lineNetAmt.toFixed(CURRENCY_PRECISION);
					}else{
						bomLine.lineNetAmt = bom.lineAmt.toFixed(CURRENCY_PRECISION);
					}					
					
					line.boms.push(bomLine);
				}/*for*/
				
				/* set modifiers */
				line.modifiers = [];
				
				var modifiers = pair.value.modifiers;
				
				for(var i=0; i<modifiers.length; i++){					
					var modifierLine = {};
					var modifier = modifiers[i];
					
					modifierLine.id = modifier.product.m_product_id;
					modifierLine.qtyEntered = modifier.qty.toFixed(UOM_PRECISION);				
					modifierLine.productName = modifier.product.name;
					modifierLine.description = modifier.description;
					modifierLine.priceEntered = modifier.priceEntered.toFixed(CURRENCY_PRECISION);
					modifierLine.taxId = modifier.tax.taxId;
					
					if(shoppingCart.priceListIncludeTax){
						modifierLine.lineNetAmt = modifier.lineNetAmt.toFixed(CURRENCY_PRECISION);
					}else{
						modifierLine.lineNetAmt = modifier.lineAmt.toFixed(CURRENCY_PRECISION);
					}	
					
					/* add modifier id */
					modifierLine.modifierId = 0;
					if(modifier.modifier){
						modifierLine.modifierId = modifier.modifier.modifierId;
					}
					
					
					line.modifiers.push(modifierLine);
				}/*for*/
				
				lines.push(line);
			});
			
			order.lines = lines;
								
			if(order.offlineDocumentNo == null || order.offlineDocumentNo == ''){
				var today = new Date();
				
				var year = today.getFullYear();
				var month = today.getMonth() + 1;					
				var date = today.getDate();
				var hours = today.getHours();
				var minutes = today.getMinutes();
				var seconds = today.getSeconds();
	    			
				if(month < 10) month = '0' + month;
				if(date < 10) date = '0' + date;
				if(hours < 10) hours = '0' + hours;
				if(minutes < 10) minutes = '0' + minutes;
				if(seconds < 10) seconds = '0' + seconds;
				
				var reference = (year + '').substr(2) + month + date + hours + minutes + seconds;			
				
				order.offlineDocumentNo = reference;				
			}
			
			if(order.uuid == null || order.uuid == ''){
				order.uuid = APP.UTILS.UUID.getUUID();
			}
			
			if(order.offlineOrderId != null && order.offlineOrderId != ''){
				order.id = parseInt(order.offlineOrderId);
			}
			
			/*order taxes*/
			order.orderTaxes = shoppingCart.orderTaxes;			
			
			order.clientId = TerminalManager.terminal.clientId;
			order.orgId = TerminalManager.terminal.orgId;
			order.terminalId = TerminalManager.terminal.id;
			order.taxId = TerminalManager.terminal.taxId;
			order.priceListId = shoppingCart.pricelistId;
			
			/* see offline/orderScreen.jsp line 231 */
			order.salesRepId = ACTIVE_SALES_REP.userId;
			order.salesRep = ACTIVE_SALES_REP.username;
			
			order.bpName = shoppingCart.bp.name;
			order.grandTotal = shoppingCart.grandTotal.toFixed(CURRENCY_PRECISION);
			order.qtyTotal = shoppingCart.qtyTotal.toFixed(UOM_PRECISION);
			order.taxTotal = shoppingCart.taxTotal.toFixed(CURRENCY_PRECISION);
			order.subTotal = shoppingCart.subTotal.toFixed(CURRENCY_PRECISION);
			order.discountAmt = shoppingCart.discountOnTotal.toFixed(CURRENCY_PRECISION);
			
			order.status = '';
			order.comments = [];
			order.payments = [];
			
			if(shoppingCart.priceListIncludeTax){
				order.subTotal = order.grandTotal;
			}
						
			/*offline/order.js*/
			OrderManager.saveOrder(order).done(function(){
				
				console.log(arguments[0]);
				
				var order = arguments[1];
				
				/* print order */
				var openDrawer = false;
				var receiptJSON = OrderManager.getReceiptJSON(order.id);
				
				if(order.docAction == 'CO'){
					openDrawer = true;
				}
				
				if(order.tenderType != "Mixed"){
					try{
						PrinterManager.printReceipt(receiptJSON, openDrawer); 
					}catch (err)
					{
						alert('Failed to print receipt!');
					}
				}
				
				var orderTaxes = OrderManager.getTaxes(order);								    			    
				
				ShoppingCartManager.clearCart();
				
				sessionStorage.removeItem('order');
				sessionStorage.setItem('order', Object.toJSON(order));
				
				sessionStorage.removeItem('orderTaxes');
				sessionStorage.setItem('orderTaxes', Object.toJSON(orderTaxes));
				
				sessionStorage.removeItem('shoppingCartLines');
				
				getURL('jsp/offline/view-order.jsp');
				
			}).fail(function(){
				PopUpManager.activePopUp.hide();	
				alert('Failed to checkout!');
			});		
			
			showProcessingDialog();		
			
		},
		
		setCashPayment : function(){
			$('amountTendered').value = this.payment.get('amountTendered');
			$('amountRefunded').value = this.payment.get('amountRefunded');	
			$('cashAmt').value = this.getCartTotal();
		},
		
		setCardPayment : function(){			
			$('cardAmt').value = this.getCartTotal();
			
			if(!this.isPaymentOnline)
			{
				$('isPaymentOnline').value = this.isPaymentOnline;
				return;
			}
			
			if(this.payment.get('cardTrackDataEncrypted'))
			{
				$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
				return;
			}			
			
			var isCardPresent = this.payment.get('isCardPresent');
			$('cardType').value = this.payment.get('cardType');
				
			if(isCardPresent){
				$('cardTrackData').value = this.payment.get('trackData');
			}
			else{
				$('cardNo').value = this.payment.get('cardNumber');
				$('cardExpDate').value = this.payment.get('cardExpiryDate');
				$('cardCVV').value = this.payment.get('cardCvv');
				$('cardholderName').value = this.payment.get('cardHolderName');
				$('cardStreetAddress').value = this.payment.get('streetAddress');
				$('cardZipCode').value = this.payment.get('zipCode');
			}	
		},
		
		setChequePayment : function(){
			$('chequeAmt').value = this.getCartTotal();
			$('chequeNo').value = this.payment.get('chequeNumber');
		},
		
		setMixPayment : function(){
			$('deliveryRule').value = this.payment.get('deliveryRule');
			$('deliveryDate').value = this.payment.get('deliveryDate');
		},
		
		setOnCreditPayment : function(){
			$('deliveryRule').value = this.payment.get('deliveryRule');
			$('deliveryDate').value = this.payment.get('deliveryDate');
			$('paymentTermId').value = this.payment.get('paymentTermId');
		},
				
		setExternalCardPayment:function(){
			$('externalCardAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},		
		
		setVoucherPayment : function(){
			$('voucherAmt').value = this.payment.get('voucherAmt');
			$('voucherNo').value = this.payment.get('voucherNo');
		},
		
		setEECardPayment : function(){
			$('cardAmt').value = this.payment.get('cardAmt');
			$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
		},
		
		setGiftCardPayment : function(){
			$('giftCardAmt').value = this.getCartTotal();
			$('giftCardTrackData').value = this.payment.get('giftCardTrackData');
			$('giftCardNo').value = this.payment.get('giftCardNo');
			$('giftCardCVV').value = this.payment.get('giftCardCVV');			
		},
				
		saveOrder : function(){
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert('Cart is empty!');
				return;
			}
			
			$('orderId').value = ShoppingCartManager.getOrderId();
			$("orderType").value = this.orderType;
			$('docAction').value = 'DR';
			
			var bp = BPManager.getBP();
			if(bp == null)
			{
				if(this.isSoTrx)
				{
					alert('Choose a customer!');					
				}
				else
				{
					alert('Choose a vendor!');
				}
				
				return;
			}
			
			$("bpartnerId").value = bp.getId();
			
			//set date ordered
			this.setDateOrdered();
			
			//$('order-form').submit();	
			this.offlineCheckout();
		},
		
		setBPartner : function(bp){
			var bpartnerId= bp.id;
			if(this.bpartnerId == bpartnerId) return;
			
			this.bpartnerId = bpartnerId;			
			$('bpartnerId').value = bpartnerId;			
			ShoppingCartManager.setBp(bp);
		},
		
		setOrderId : function(orderId){
			$('orderId').value = orderId;				
		},
		
		initialize: function(){						
						
			/* fields declaration */
			this.tenderType = TENDER_TYPE.CASH;
			this.payment = null;
			this.splitManager = null;
			
			this.initializeDiscount();				
			this.initializeComponents();			
			this.initAutoCompletes();
			ClockedInSalesRepManager.addOnLoadClockedInSalesRepsListener(this);
			
			shortcut.add("Up", function(){
				SmartProductSearch.moveUp();
			});
			
			shortcut.add("Down", function(){
				SmartProductSearch.moveDown();
			});
			
			/* render search product filter 
			SearchProductFilter.initialize();*/
			
			ItemFields.init();
			CustomerFields.init();
			VendorFields.init();			
		},
		
		clockedInSalesRepsLoaded : function()
		{
			this.initSplitOrder();
		},
		
		initSplitOrder : function()
		{
			var splitOrderDetails = this.splitOrderDetails;
			var type = splitOrderDetails.type;
			var orderSplitSalesReps = eval('('+splitOrderDetails.salesReps+')');
			
			var orderDetailsManager = new SplitOrderDetailsManager(type, orderSplitSalesReps);
			this.splitManager = orderDetailsManager.getSplitManager();
			this.splitManager.updateSalesRepInfo();
			ClockedInSalesRepManager.addCurrentSalesRepChangeEventListener(this);
			ClockedInSalesRepManager.addClockInOutEventListener(this);
		},
		
		currentSalesRepChange : function (salesRepChangeEvent)
		{
			this.splitManager.currentSalesRepChange(salesRepChangeEvent);
		},
		
		clockInOut : function()
		{
			this.splitManager.clockInOut();
		},
		
		resetSplitAmounts : function()
		{
			if (this.splitManager != null)
				this.splitManager.resetAmounts();
		},
		
		updateSplitAmounts : function()
		{
			if (this.splitManager != null)
				this.splitManager.updateAmounts();
		},
		
		initializeComponents:function(){
			
			this.processDialog = new Dialog("Processing, please wait");
			
			/* search button & textfield */
			this.searchProductTextField = $('search-product-textfield');
			this.searchProductButton = $('search-product-button');
			
			/* header buttons */
			this.clockInOutButton = $('orderScreen.clockInOutButton');
			this.homeButton = $('orderScreen.homeButton');
			this.keyboardButton = $('orderScreen.keyboardButton');
			this.infoButton = $('orderScreen.infoButton');
			
			/* cart buttons & textfield */
			this.discountButton = $('discount-button');
			this.changePinButton = $('change-user-button');
			this.openCashDrawerButton = $('open-drawer-button');
			this.saveOrderButton = $('save-order-button');
			this.invokeOrderButton = $('load-button');
			this.clearCartButton = $('clear-button');
			this.decrementQtyButton = $('decrease-button');
			this.incrementQtyButton = $('add-button');			
			this.updateQtyTextField = $('quantity-texfield');	
			
			this.moreOptionsButton = $('more-options-button');
			this.lessOptionsButton = $('less-options-button');
			
			/* payment buttons */
			this.cashPaymentButton = $('orderScreen.cashPaymentButton');
			this.cardPaymentButton = $('orderScreen.cardPaymentButton');
			this.chequePaymentButton = $('orderScreen.chequePaymentButton');
			this.mixPaymentButton = $('orderScreen.mixPaymentButton');			
			this.onCreditPaymentButton = $('orderScreen.onCreditPaymentButton');			
			this.voucherPaymentButton = $('orderScreen.voucherPaymentButton');
			this.giftCardPaymentButton = $('orderScreen.giftCardPaymentButton');
			
			/* other buttons */
			this.bpInfoButton = $('bp-info-button');
			this.bpCreateButton = $('bp-create-button');
			this.checkoutButton = $('checkout-button');
			this.newItemButton = $('create-item-button');
			
			this.backDateButton = $('back-date-button');
			if(DiscountManager.allowOrderBackDate()){
				if(this.backDateButton){
					this.backDateButton.show();
				}
			}
			this.splitOrderButton = $('split-order-button');
			
			this.productInfoButton = $('product-info-button');
			
			/* drop downs */
			this.taxSelectList  = $('tax-dropdown');
			if(this.taxSelectList){
				this.taxSelectList.value = OrderTax.defaultTaxId;
			}
			
			/* set smart product search */
			SearchProduct.isSoTrx = this.isSoTrx;		
			
			this.selectQueryBox();
			 
			/* add behaviour */	
			for(var field in this){
				var fieldContents = this[field];
				
				if (fieldContents == null || typeof(fieldContents) == "function") {
					continue;
			    }
				
				if(fieldContents.type == 'button'){
			    	//register click handler
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.type == 'text'){
			    	//register key handler
			    	fieldContents.onkeyup = this.keyHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.type == 'select-one'){
			    	//register key handler
			    	fieldContents.onchange = this.changeHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.tagName == 'A'){
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.tagName == 'IMG'){
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			}
			
			/* add keypad to qty textfield */
			Event.observe(this.updateQtyTextField,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
			
			/* bug fix for updateQtyTextField */
			this.updateQtyTextField.onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					if(!ShoppingCartManager.isShoppingCartEmpty()){
						var qty = parseFloat(this.value);
						
						if(isNaN(qty)){
							alert('Invalid Qty!');
							this.selectAll();								
							return;					
						}
						ShoppingCartManager.updateQty(qty);							
					}
				}
			}
			
			/*tax rate*/
			var cart = ShoppingCartManager.getCart();
			cart.addBehaviourToLines();
			
		},
		
		clickHandler : function(e){
			var element = Event.element(e);
			
			/*bug fix for buttons*/
			if(Prototype.Browser.WebKit)
			if(element instanceof HTMLButtonElement 
					|| element instanceof HTMLLinkElement 
					|| (element instanceof HTMLImageElement && !(element.parentNode instanceof HTMLButtonElement)))
			{
				
			}
			else
			{
				element = element.parentNode;
			}
			
			switch(element){
				
				case this.searchProductButton : 
					SearchProduct.search();
					break;
				
				case this.clockInOutButton : break;
				case this.homeButton : break;
				case this.keyboardButton : break;
				case this.infoButton : break;
				
				case this.discountButton : 
					DiscountPanel.toggle();
					break;
				
				case this.changePinButton : 
					new PINPanel().show();
					break;
				
				case this.openCashDrawerButton : 
					OrderScreen.openCashDrawer();
					break;
				
				case this.saveOrderButton : 
					OrderScreen.saveOrder();
					break;
				
				case this.invokeOrderButton : 
					new InvokeOrderPanel().show();
					break;
				
				case this.clearCartButton :
					ShoppingCartManager.clearCart();
					OrderScreen.resetSplitAmounts();
					break;
				
				case this.decrementQtyButton : 
					ShoppingCartManager.decrementQty();
					break;
				
				case this.incrementQtyButton : 
					ShoppingCartManager.incrementQty();
					break;	
					
				/* Payment buttons */
				case this.cashPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CASH);
					break;
					
				case this.cardPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CARD);
					break;
					
				case this.chequePaymentButton :
					OrderScreen.setTenderType(TENDER_TYPE.CHEQUE); 
					break;
					
				case this.mixPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.MIXED);
					break;		
						
				case this.onCreditPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CREDIT);
					break;	
							
				case this.voucherPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.VOUCHER);
					break;
					
				case this.giftCardPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.GIFT_CARD);
					break;
					
				/* Checkout button */
				case this.checkoutButton :
					/*OrderScreen.getPaymentDetails();*/
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert('Cart is empty!');
						return;
					}
					
					if(!ShoppingCartManager.isShoppingCartReady()){
						alert('Cart is not ready!');
						return;
					}
					
					
					if(this.allowUpSell){
						if(ShoppingCartManager.getUpsellTotal() < 0.0){
							alert('The upsell buffer cannot be negative!');
							return;
						}
					}
					
					var cartTotal = this.getCartTotal();
					if(cartTotal == 0.0){
						this.tenderType = TENDER_TYPE.CASH;						
						var payment = new Hash();
					  	payment.set('amountTendered', cartTotal);
					  	payment.set('amountRefunded', 0.0);			  	
					  	this.payment = payment;
					  	this.checkout();
					  	return;
					}
					
					new PaymentPopUp().show();
					break;
					
				case this.bpInfoButton :
					new BPInfoPanel().show();
					break;
				
				case this.bpCreateButton : 
					alert('Feature not available offline!');
					/*
					var panel = null;
					
					if (this.isSoTrx == true)
						panel = new CreateCustomerPanel();
					else
						panel = new CreateVendorPanel();
					
					panel.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					panel.show();
					*/
					break;
					
				case this.backDateButton :
					var panel = new BackDatePanel();
					panel.updateElements = this.setBackDateValues.bind(this);
					panel.show();
					break;
					
				case this.productInfoButton : 
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert('Cart is empty!');
						return;
					}
					var panel = new ProductInfoPanel();
					panel.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					
					var lineId = ShoppingCartManager.getLineId();
					if(lineId == null) return;
					
					var currentLine = ShoppingCartManager.getCart().getLine(lineId);;
					var productId = currentLine.product.m_product_id;
					
					panel.getInfo(productId);
					break;
					
				case this.splitOrderButton : 
					OrderScreen.splitManager.showPanel();
					break;
				
				case this.lessOptionsButton : 
					$('more-buttonContainer').hide();
					$('buttonContainer').show();
					OrderScreen.selectQueryBox();
					break;
					
				case this.moreOptionsButton : 
					$('buttonContainer').hide();
					$('more-buttonContainer').show();
					OrderScreen.selectQueryBox();
					break;
				
				case this.newItemButton : 
					alert('Feature not available offline!');
					/*
					var p = new CreateProductPanel();
					p.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					p.show();
					*/
					
					break;
							
				default:break;
			}		
		},
		
		keyHandler : function(e){
			
			var element = Event.element(e);
			
			switch(element){
				case this.searchProductTextField : 
					if(e.keyCode == Event.KEY_RETURN){
						SearchProduct.search();
					}
					break;	
							
				case this.updateQtyTextField : 					
					break;
							
				default:break;
			}
		},
		
		changeHandler : function(e){
			var element = Event.element(e);
			
			switch(element){
				case this.taxSelectList : 
					var taxId = this.taxSelectList.value;
					ShoppingCartManager.setLineTax(taxId);
					break;	
					
				default:break;
			}
		},
				
		render : function(){
			/* call this to redraw screen */ 
			/*
			$('orderGrandTotal').innerHTML = ShoppingCartManager.getCurrencySymbol() + " " + new Number(ShoppingCartManager.getGrandTotal()).toFixed(2);
			
			*/
			
			PoleDisplayManager.display('Grand Total',ShoppingCartManager.getCurrencySymbol() + new Number(ShoppingCartManager.getGrandTotal()).toFixed(2));
			
			if(this.allowUpSell){
				$('orderScreen-upsellAmountContainer').innerHTML = new Number(ShoppingCartManager.getUpsellTotal()).toFixed(2);
			}
			else{
				$('orderScreen-upsellAmountContainer').innerHTML = '';
			}
			
		},
		
		selectQueryBox : function (){
			if(navigator.userAgent.match(/iPad/i)){
				/*prevent ipad keyboard from popping each time*/
				return;
			}
			
			var textfield = $('search-product-textfield');
			if( textfield != null){
				if(textfield.value.length > 0)
					textfield.select();
				else
					textfield.focus();
			}			
		},
		
		getCartTotal : function(){
			var UOM_PRECISION = 0;
			var CURRENCY_PRECISION = 2;
			
			var amt = ShoppingCartManager.getGrandTotal().toFixed(CURRENCY_PRECISION);	
			return parseFloat(amt);
		},
		
		getCurrencySymbol : function(){
			return ShoppingCartManager.getCurrencySymbol();
		},		
		
		initializeDiscount:function(){
			DiscountManager.setDiscountRights(DISCOUNT_RIGHTS);
			
			if($('orderScreen.backDateButton'))
			{
				if(!this.allowOrderBackDate){
					$('orderScreen.backDateButton').hide();
				}
				else
				{
					$('orderScreen.backDateButton').show();
				}
			}
			
		},
		
		openCashDrawer : function(){
			PrinterManager.print([['OPEN_DRAWER']]);
		},
		
		initAutoCompletes : function(){
			
			var updateBP = function(text, li){
				 var id = li.getAttribute('id');				 
				 id = parseInt(id);
				 
				 var bpJSON = BPartnerManager.getBPartnerById(id);
				 
				 var bp = new BP(bpJSON);
				 BPManager.setBP(bp);				 
								 
				 OrderScreen.afterUpdateBP(bp);
				 
			 };	
			
			var autocompleter = new Ajax.Autocompleter('search-bp-textfield', 'bp-autocomplete-choices-container', '', {afterUpdateElement:updateBP});
			
			autocompleter.getUpdatedChoices = function(){
				
				var searchTerm = this.getToken();
				
				var column = OrderScreen.isSoTrx ? 'iscustomer' : 'isvendor';
				var queryFilter = {};
				queryFilter[column] = 'Y';
				
				var query = [
						     {"name":{leftnocase:searchTerm}},
						     {"phone":{leftnocase:searchTerm}},
						     {"identifier":{leftnocase:searchTerm}},
						     {"c_bpartner_id":{'==':searchTerm}}
						];				
				
				var results = BPartnerManager.searchBPartners(queryFilter);
				
				var maxLength = 10;
				
				if(results.length < maxLength){
					maxLength = results.length;
				}
				
				var choices = '<ul>';
				
				for(var i=0; i<maxLength; i++){
					var bp = results[i];
					choices += '<li id="' + bp.c_bpartner_id + '">' + bp.name + '</li>';
				}
				
				choices += '</ul>';
				
				this.updateChoices(choices);
			};
						
		},
		
		afterUpdateBP : function(bp){
			this.setBPartner(bp);	
			$('search-bp-textfield').value = bp.getFullName();
		},
		
		setBackDateValues : function(values){
			this.referenceNumber = values.referenceNumber;
			this.documentNumber = values.documentNumber;
			this.dateOrdered = values.dateOrdered;
		}
};


var ItemFields = {
		
		fields : null,
		
		init : function()
		{
			ItemFields.fields = $('items.fields').innerHTML;
		}
}

var CustomerFields = {
		
		fields : null,
		
		init : function()
		{
			CustomerFields.fields = $('customer.fields').innerHTML;
		}
}

var VendorFields = {
		
		fields : null,
		
		init : function()
		{
			VendorFields.fields = $('vendor.fields').innerHTML;
		}
}


var ORDER_TYPES = { 
	POS_ORDER : 'POS Order',
	CUSTOMER_RETURNED_ORDER : 'Customer Returned Order',
	POS_GOODS_RECEIVE_NOTE : 'POS Goods Receive Note',		
	POS_GOODS_RETURNED_NOTE : 'POS Goods Returned Note'
};
	
var TENDER_TYPE = {
		CARD : 'Card',
		CASH : 'Cash',
		CHEQUE : 'Cheque',
		CREDIT : 'Credit',
		MIXED : 'Mixed',
		VOUCHER : 'Voucher',
		EXTERNAL_CARD : 'Ext Card',
		GIFT_CARD : 'Gift Card'
	};

var PAYMENT_RULE = {
		CARD : 'K',
		CASH : 'B',
		CHEQUE : 'S',
		CREDIT : 'P',
		MIXED : 'M',
		VOUCHER : 'V',
		EXTERNAL_CARD : 'E',
		GIFT_CARD : 'G'
};

var SO_CREDIT_STATUS = {
	CreditStop 		: "S",
	CreditHold 		: "H",
	CreditWatch 	: "W",
	NoCreditCheck 	: "X",
	CreditOK 		: "O"
};

var DELIVERY_RULE = {
		AFTER_RECEIPT : 'R',
		COMPLETE_ORDER : 'O',
		MANUAL : 'M'
};