var CashPanel =  Class.create(PopUpBase, {
  initialize: function() {
	this.createPopUp($('cash.popup.panel'));
    
    this.amountRefunded = 0.0;
    
    /*buttons*/
    this.okBtn = $('cash.popup.okButton');
    this.cancelBtn = $('cash.popup.cancelButton');
    
    /*textfields*/
    this.amountTenderedTextField = $('cash.popup.amountTenderedTextField');  
    this.payAmountTextField = $('cash.popup.payAmountTextField'); 
    
    /*labels*/
    this.totalLabel = $('cash.popup.totalLabel');
    this.changeLabel = $('cash.popup.changeLabel');
    /*this.writeOffCashLabel = $('cash.popup.writeOffCashLabel');
    this.paymentAmtLabel = $('cash.popup.paymentAmtLabel');*/
    
    this.cancelBtn.onclick = this.hide.bind(this);
    this.okBtn.onclick = this.okHandler.bind(this); 
    
    /*fields*/
    this.cartTotalAmount = parseFloat(PaymentManager.getOpenAmt());
    this.currencySymbol = PaymentManager.getCurrencySymbol();
    
    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
    /*this.paymentAmtLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);*/
    
    /*others*/
    this.amountTenderedTextField.panel = this;
    this.amountTenderedTextField.onkeypress = function(e){
    	
    	if(e.keyCode == Event.KEY_RETURN)
		{
			if(this.value == null || this.value == ''){
				this.panel.onError('Invalid Tendered Amount!');
				return;
			}
			
			this.panel.okHandler();	
			return;
		}
    	
    };
    
    this.amountTenderedTextField.onkeyup = function(e){
    	
    	var value = this.value;
    	if(value == null || value == '') value = 0.0;
    	
    	var amountTendered = parseFloat(value);
    	    	
    	if(isNaN(amountTendered)){    		
    		return;
    	}    
    	
    	var openAmt = this.panel.cartTotalAmount;
    	if(amountTendered <= openAmt){
    		this.panel.payAmountTextField.value = new Number(amountTendered).toFixed(2);
    		this.panel.changeLabel.innerHTML = '0.00';
    	}
    	else{
    		var changeAmt = 0.0;
    		if(amountTendered != 0.0){
    			changeAmt = amountTendered - openAmt;
    		}
    			
    		this.panel.payAmountTextField.value = new Number(openAmt).toFixed(2);;
    		this.panel.changeLabel.innerHTML = new Number(changeAmt).toFixed(2);
    	}
   
    };
    
    
    this.payAmountTextField.panel = this;
    this.payAmountTextField.onkeypress = function(e){
    	
    	if(e.keyCode == Event.KEY_RETURN)
		{
			if(this.value == null || this.value == ''){
				this.panel.onError('Invalid Payment Amount!');
				return;
			}
			
			this.panel.okHandler();	
			return;
		}
    	
    };
    
    this.payAmountTextField.onkeyup = function(e){
    	
    	var value = this.value;
    	if(value == null || value == '') value = 0.0;
    	
    	var paymentAmt = parseFloat(value);
    	var openAmt = this.panel.cartTotalAmount;
    	var amtTendered = this.panel.amountTenderedTextField.value;
    	
    	var changeAmt = amtTendered - paymentAmt;
    	this.panel.changeLabel.innerHTML = new Number(changeAmt).toFixed(2);
    	
    	/*
    	if(paymentAmt > openAmt){
    		alert('Payment amount cannot be more than open amount!');
    		return false;
    	}
    	
    	if(paymentAmt > amtTendered){
    		alert('Payment amount cannot be more than amount tendered!');
    		return false;
    	}
    	*/
    	    	    	
    };
    
  },  
  okHandler:function(){
	  if(!this.validate()) return;	  
	  this.hide();
	  this.paymentHandler(this.payment);
  },
  resetHandler:function(){
  	this.amountTenderedTextField.value = '';
  	this.payAmountTextField.value = '';
  	this.changeLabel.innerHTML ='0.00';
  },
  onShow:function(){
  	this.amountTenderedTextField.focus();
  	this.changeLabel.innerHTML = '0.00';
  },
  validate:function(){
	  
	  var tenderedAmt = parseFloat(this.amountTenderedTextField.value);
	  var payAmt = parseFloat(this.payAmountTextField.value);
	  	  
	  if(isNaN(tenderedAmt)){
		  this.onError('Invalid Amount tendered!');
		  this.amountTenderedTextField.focus();
		  return false;
	  }
	  
	  if(isNaN(payAmt)){
		  this.onError('Invalid Payment Amount!');
		  this.payAmountTextField.focus();
		  return false;
	  }
	  
	  if(payAmt > PaymentManager.openAmt){
		  this.onError('Payment amount cannot be more than open amount!');
		  this.payAmountTextField.focus();
		  return false;
	  }
	  
	  if(payAmt > tenderedAmt){
		  this.onError('Payment amount cannot be more than amount tendered!');
		  this.payAmountTextField.focus();
		  return false;
	  }
	  
	  
	  this.setPayment();
	  
	return true;
  },
  setPayment:function(){
  	var payment = new Hash();
  	payment.set('amt',this.payAmountTextField.value);
  	this.payment = payment;
  },
  paymentHandler:function(payment){
	  alert(payment);
  }
});


/* Card Panel */
var CardPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('card.popup.panel'));
	    
	    /*panes*/
	    this.trackDataDetailsPane = $('card.popup.trackDataDetailsPane');
	    this.cardDetailsPane = $('card.popup.cardDetailsPane');
	    
	    /*labels*/
	    this.totalLabel = $('card.popup.totalLabel');	 	    
	    
	    /*textfields*/
	    this.cardAmtTextField = $('card.popup.amountTextfield');
	    this.cardSelectList = $('card.popup.cardSelectList');
	    this.swipeCardCheckBox = $('card.popup.swipeCardCheckBox');
	    
	    this.trackDataTextField = $('card.popup.trackDataTextField');
	    this.cardNumberTextField = $('card.popup.cardNumberTextField');
	    this.cardExpiryDateTextField = $('card.popup.cardExpiryDateTextField');
	    this.cardCvvTextField = $('card.popup.cardCvvTextField');
	    this.cardHolderNameTextField = $('card.popup.cardHolderNameTextField');
	    this.streetAddressTextField = $('card.popup.streetAddressTextField');
	    this.zipCodeTextField = $('card.popup.zipCodeTextField');
	    
	    /*buttons*/
	    this.okBtn = $('card.popup.okButton');
	    this.cancelBtn = $('card.popup.cancelButton');	   
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = PaymentManager.getOpenAmt();
	    this.currencySymbol = PaymentManager.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*show details panes*/	    
	    this.cardDetailsPane.style.display = this.swipeCardCheckBox.checked ? 'none' : '';
	    this.trackDataDetailsPane.style.display = this.swipeCardCheckBox.checked ? '' : 'none';
			    
	    this.swipeCardCheckBox.panel = this;
	    this.swipeCardCheckBox.onchange = function(e){
	    	this.panel.cardDetailsPane.style.display = this.checked ? 'none' : '';
	 	    this.panel.trackDataDetailsPane.style.display = this.checked ? '' : 'none';			
		};
		
		/*track data*/
		this.trackDataTextField.panel = this;
		this.trackDataTextField.onkeypress = function(e){			
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('Swipe Credit Card');
					return;
				}
						
				this.panel.okHandler();		
			}
		};
		
		/*card number*/
		this.cardNumberTextField.panel = this;
		this.cardNumberTextField.onkeypress = function(e){			
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('Enter Credit Card Number');
					return;
				}
						
				this.panel.okHandler();		
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.swipeCardCheckBox.checked = true;
	  	this.cardSelectList.options[0].selected = true;
	    
	    this.trackDataTextField.value = '';
	    this.cardNumberTextField.value = '';
	    this.cardExpiryDateTextField.value = '';
	    this.cardCvvTextField.value = '';
	    this.cardHolderNameTextField.value = '';
	    this.streetAddressTextField.value = '';
	    this.zipCodeTextField.value = '';
	    this.cardAmtTextField.value = '';
	  },
	  onShow:function(){
	  	this.trackDataTextField.focus();
	  	this.cardAmtTextField.value = PaymentManager.cardAmt;
	  },
	  
	  validate:function(){
		  
		  var amt = parseFloat(this.cardAmtTextField.value);
		  
		  if(isNaN(amt)){
			  this.onError('Invalid amount!');
			  this.cardAmtTextField.focus();
			  return false;
		  }
		  
		  if(amt > PaymentManager.openAmt){
			  this.onError('Card amount cannot be more than open amount!');
			  this.cardAmtTextField.focus();
			  return false;
		  }
		  
		  if(this.swipeCardCheckBox.checked){
			  /*card was swipped*/ 
			  if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
				  this.onError('Swipe Credit Card');
				  return false;
			  }
		  }
		  else{
			  /*no card environment*/ 
			  if(this.cardNumberTextField.value == null || this.cardNumberTextField.value == ''){
				  this.onError('Credit Card Number is required!');
				  return false;
			  }
			  
			  if(this.cardExpiryDateTextField.value == null || this.cardExpiryDateTextField.value == ''){
				  this.onError('Credit Card has expired!');
				  return false;
			  }
				
			  
		  }
		  
		  this.setPayment();
		  		  
		  return true;
	  },
	  
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('amt', this.cardAmtTextField.value);
	  	payment.set('isCardPresent', this.swipeCardCheckBox.checked);
	  	payment.set('cardType',this.cardSelectList.value);
	  	
	  	if(this.swipeCardCheckBox.checked){
	  		payment.set('trackData',this.trackDataTextField.value);		  		
	  	}
	  	else{	  		
	  		payment.set('cardNumber',this.cardNumberTextField.value);
	  		payment.set('cardExpiryDate',this.cardExpiryDateTextField.value);
	  		payment.set('cardCvv',this.cardCvvTextField.value);
	  		payment.set('cardHolderName',this.cardHolderNameTextField.value);
	  		payment.set('streetAddress',this.streetAddressTextField.value);
	  		payment.set('zipCode',this.zipCodeTextField.value);
	  	}	  	
	  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* External Card Panel */
var CardAmtPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('cardAmt.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('cardAmt.popup.okButton');
	    this.cancelBtn = $('cardAmt.popup.cancelButton');
	    
	    /*labels*/
	    this.totalLabel = $('cardAmt.popup.totalLabel');
	    
	    /*textfields*/
	    this.amountTenderedTextField = $('cardAmt.popup.amountTenderedTextField');  
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = PaymentManager.getOpenAmt();
	    this.currencySymbol = PaymentManager.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

	    this.amountTenderedTextField.panel = this;
	    this.amountTenderedTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('Invalid amount!');
					return;
				}
						
				this.panel.validate();			
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  
		  PaymentManager.getPaymentDetails();
	  },
	  resetHandler:function(){
	  	this.amountTenderedTextField.value = '';
	  },
	  onShow:function(){
	  	this.amountTenderedTextField.focus();
	  },
	  validate:function(){		  
		  
		  var amt = parseFloat(this.amountTenderedTextField.value);
		  
		  if(isNaN(amt)){
			  this.onError('Invalid card amount!');
			  this.amountTenderedTextField.focus();
			  return false;
		  }
		  
		  if(amt > PaymentManager.openAmt){
			  this.onError('Card amount cannot be more than open amount!');
			  this.amountTenderedTextField.focus();
			  return false;
		  }
		  		  
		  PaymentManager.cardAmt = amt;
		  PaymentManager.tenderType = TENDER_TYPE.CARD;
		  
		  return true;
	  }
	  
});

/* Cheque Panel */
var ChequePanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('cheque.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('cheque.popup.okButton');
	    this.cancelBtn = $('cheque.popup.cancelButton');
	    
	    /*labels*/
	    this.totalLabel = $('cheque.popup.totalLabel');
	    
	    /*textfields*/
	    this.amountTenderedTextField = $('cheque.popup.amountTenderedTextField');  
	    this.chequeNumberTextField = $('cheque.popup.chequeNumberTextField');    
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = PaymentManager.getOpenAmt();
	    this.currencySymbol = PaymentManager.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

	    this.chequeNumberTextField.panel = this;
	    this.chequeNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('Enter Check Number');
					return;
				}
						
				this.panel.validate();			
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.chequeNumberTextField.value = '';
	  	this.amountTenderedTextField.value = '';
	  },
	  onShow:function(){
	  	this.amountTenderedTextField.focus();
	  },
	  validate:function(){	
		  
		  var amt = parseFloat(this.amountTenderedTextField.value);
		  
		  if(isNaN(amt)){
			  this.onError('Invalid amount!');
			  this.amountTenderedTextField.focus();
			  return false;
		  }
		  
		  if(amt > PaymentManager.openAmt){
			  this.onError('Check amount cannot be more than open amount!');
			  this.amountTenderedTextField.focus();
			  return false;
		  }
		  
		  if(this.chequeNumberTextField.value == null || this.chequeNumberTextField.value == ''){
			  this.onError('Enter Check Number');
			  return false;
		  }
		  
		  this.setPayment();
		  
		  return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('chequeNumber', this.chequeNumberTextField.value);
	  	payment.set('amt', this.amountTenderedTextField.value);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* External Card Panel */
var ExtCardPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('extcard.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('extcard.popup.okButton');
	    this.cancelBtn = $('extcard.popup.cancelButton');
	    
	    /*labels*/
	    this.totalLabel = $('extcard.popup.totalLabel');
	    
	    /*textfields*/
	    this.amountTenderedTextField = $('extcard.popup.amountTenderedTextField');  
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = PaymentManager.getOpenAmt();
	    this.currencySymbol = PaymentManager.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

	    this.amountTenderedTextField.panel = this;
	    this.amountTenderedTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('Invalid amount!');
					return;
				}
						
				this.panel.validate();			
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.amountTenderedTextField.value = '';
	  },
	  onShow:function(){
	  	this.amountTenderedTextField.focus();
	  },
	  validate:function(){		  
		  
		  var amt = parseFloat(this.amountTenderedTextField.value);
		  
		  if(isNaN(amt)){
			  this.onError('Invalid card amount!');
			  this.amountTenderedTextField.focus();
			  return false;
		  }
		  
		  if(amt > PaymentManager.openAmt){
			  this.onError('Card amount cannot be more than open amount!');
			  this.amountTenderedTextField.focus();
			  return false;
		  }
		  		  
		  this.setPayment();
		  
		  return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('isPaymentOnline', 'false');
	  	payment.set('otk', '');
	  	payment.set('amt', this.amountTenderedTextField.value);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/*Voucher Panel*/
var VoucherPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('voucher.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('voucher.popup.okButton');
	    this.cancelBtn = $('voucher.popup.cancelButton');
	    
	    /*textfields*/
	    this.voucherNoTextField = $('voucher.popup.voucherNoTextField');
	    
	    /*labels*/
	    this.totalLabel = $('voucher.popup.totalLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = PaymentManager.getOpenAmt();
	    this.currencySymbol = PaymentManager.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*others*/
	    this.voucherNoTextField.panel = this;
	    this.voucherNoTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('Invalid Voucher No.');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}    	
	    	
	    };
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.voucherNoTextField.value = '';
	  },
	  onShow:function(){
	  	this.voucherNoTextField.focus();
	  },
	  validate:function(){
		  
		  if(this.voucherNoTextField.value == null || this.voucherNoTextField.value == ''){
			  this.onError('Invalid voucher no.!');
			  this.voucherNoTextField.focus();
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();	  	
	  	payment.set('voucherNo', this.voucherNoTextField.value);
	  	payment.set('voucherAmt', null);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});

/*e2e Panel*/
var E2EPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('e2e.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('e2e.popup.okButton');
	    this.cancelBtn = $('e2e.popup.cancelButton');
	    
	    /*textfields*/
	    this.amountTextField = $('e2e.popup.amountTextField');   
	    this.trackDataTextField = $('e2e.popup.trackDataTextField');
	    
	    /*labels*/
	    this.totalLabel = $('e2e.popup.totalLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = PaymentManager.getOpenAmt();
	    this.currencySymbol = PaymentManager.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*others*/
	    this.amountTextField.panel = this;
	    this.amountTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('Invalid amount');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}    	
	    	
	    };
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.amountTextField.value = '';
	  },
	  onShow:function(){
	  	this.amountTextField.focus();
	  },
	  validate:function(){
		  
		  var amt = parseFloat(this.amountTextField.value);
		  
		  if(isNaN(amt)){
			  this.onError('Invalid amount!');
			  this.amountTextField.focus();
			  return false;
		  }
		  
		  if(amt > PaymentManager.openAmt){
			  this.onError('Card amount cannot be more than open amount!');
			  this.amountTextField.focus();
			  return false;
		  }
		  
		 		  
		  if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
			  this.onError('Card not swiped!');
			  this.trackDataTextField.focus();
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('cardAmt', this.amountTextField.value);
	  	payment.set('cardTrackDataEncrypted', this.trackDataTextField.value);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});


/* Payment panel */
var PaymentPopUp = Class.create(PopUpBase, {
	initialize: function() {
	    this.createPopUp($('payment.popup.panel'));
	    
	    /*buttons*/
	   // this.okBtn = $('payment.popup.okButton');
	    this.cancelBtn = $('payment.popup.cancelButton');
	    
	    this.cashBtn = $('payment.popup.cashPaymentButton');
	    this.cardBtn = $('payment.popup.cardPaymentButton');
	    this.chequeBtn = $('payment.popup.chequePaymentButton');
	    
	    /* external credit card processing */
	    this.externalCreditCardMachine = $('payment.popup.externalCreditCardMachineButton');
	    
	    /* voucher */
	    this.voucherBtn = $('payment.popup.voucherPaymentButton');
	    
	    this.giftCardBtn = $('payment.popup.giftCardPaymentButton');
	    	    
	    /* add behaviour */	
		for(var field in this){
			var fieldContents = this[field];
			
			if (fieldContents == null || typeof(fieldContents) == "function") {
				continue;
		    }
		    
		    if(fieldContents.type == 'button'){
		    	//register click handler
		    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
		    }
		}
		
		 /*override events*/
	    this.cancelBtn.onclick = this.cancelHandler.bind(this);
	   // this.okBtn.onclick = this.okHandler.bind(this); 
	},
	okHandler:function(){	  
		if(this.tenderType == null) this.tenderType = TENDER_TYPE.CASH;		
		this.hide();
	},
	cancelHandler:function(){
		this.tenderType = null;
		this.isPaymentOnline = true;
		this.hide();
	},
	onShow:function(){
		this.tenderType = TENDER_TYPE.CASH;
		this.cashBtn.focus();
	},
	onHide:function(){
		if(this.tenderType == null) return;	
		
		PaymentManager.setTenderType(this.tenderType);
		PaymentManager.getPaymentDetails();
		
	},
	clickHandler:function(e){
		var element = Event.element(e);
		
		var count = 0;
		while(!(element instanceof HTMLButtonElement)){
			count++;
			if(count > 10){
				alert('Fail to get event source!');
				break;
			} 
			element = element.parentNode;
		}
		
		switch(element){
			case this.cashBtn:
				this.tenderType = TENDER_TYPE.CASH;
				break;
			
			case this.cardBtn:
				this.tenderType = TENDER_TYPE.CARD;
				PaymentManager.setIsPaymentOnline(true);
				break;
				
			case this.chequeBtn:
				this.tenderType = TENDER_TYPE.CHEQUE;
				break;
				
			case this.externalCreditCardMachine:				
				this.tenderType = TENDER_TYPE.EXTERNAL_CARD;
				PaymentManager.setIsPaymentOnline(false);				
				break;
				
			case this.voucherBtn:
				this.tenderType = TENDER_TYPE.VOUCHER;
				break;
				
			case this.giftCardBtn:
				this.tenderType = TENDER_TYPE.GIFT_CARD;
				break;
			
			default:break;
		}
		
		this.okHandler();
	}
});

/*XWeb Panel*/
var XWebPanel =  Class.create(PopUpBase, {
  initialize: function() {
	this.createPopUp($('xweb.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('xweb.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = PaymentManager.openAmt;
    this.orderType = PaymentManager.orderType;
    
    /*others*/
    this.frame = $('xweb.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert('OK');
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("PaymentAction.do?action=getOTK&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*Mercury Panel*/
var MercuryPanel =  Class.create(PopUpBase, {
  initialize: function($super) {
	this.createPopUp($('mercury.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('mercury.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = PaymentManager.openAmt;
    this.orderType = PaymentManager.orderType;
    
    /*others*/
    this.frame = $('mercury.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert('OK');
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("PaymentAction.do?action=getOTK&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*ElementPS Panel*/
var ElementPSPanel =  Class.create(PopUpBase, {
  initialize: function() {
	this.createPopUp($('elementps.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('elementps.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = PaymentManager.openAmt;
    this.orderType = PaymentManager.orderType;
    
    /*others*/
    this.frame = $('elementps.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert('OK');
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("PaymentAction.do?action=getOTK&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*Century Card Panel*/
var CenturyPanel =  Class.create(PopUpBase, {
initialize: function() {
	this.createPopUp($('century.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('century.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = PaymentManager.openAmt;
    this.orderType = PaymentManager.orderType;
    
    /*others*/
    this.frame = $('century.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert('OK');
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("PaymentAction.do?action=getCenturyPaymentForm&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*Gift Card Panel*/
var GiftCardPanel =  Class.create(CheckoutPanel, {
  initialize: function() {
	this.createPopUp($('gift.popup.panel'));
    
    /*buttons*/
    this.okBtn = $('gift.popup.okButton');
    this.cancelBtn = $('gift.popup.cancelButton');
    
    /*textfields*/ 
    this.trackDataTextField = $('gift.popup.trackDataTextField');
    this.numberTextField = $('gift.popup.numberTextField');
    this.cvvTextField = $('gift.popup.cvvTextField');
    this.amountTextField = $('gift.popup.amountTextField');
    
    /*labels*/
    this.totalLabel = $('gift.popup.totalLabel');
    
    /*radios*/
    this.swipeRadio = $('gift.popup.swipeRadio');
    this.manualRadio = $('gift.popup.manualRadio');
    
    /*panels*/
    this.swipePanel = $('gift.popup.swipePanel');
    this.manualPanel = $('gift.popup.manualPanel');
    
    /*add behaviour to components*/
    this.swipeRadio.onclick = this.radioHandler.bind(this);
    this.manualRadio.onclick = this.radioHandler.bind(this);
    
    this.cancelBtn.onclick = this.hide.bind(this);
    this.okBtn.onclick = this.okHandler.bind(this); 
    
    this.cartTotalAmount = PaymentManager.openAmt;
    this.orderType = PaymentManager.orderType;
    this.currencySymbol = PaymentManager.currencySymbol;
    
    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
    
    this.trackDataTextField.panel = this;
    this.trackDataTextField.onkeypress = function(e){	    	
    	if(e.keyCode == Event.KEY_RETURN)
		{
			if(this.value == null || this.value == ''){
				this.panel.onError('Swipe Card');
				return;
			}
			
			this.panel.okHandler();	
			return;
		}	    	
    };
    
    this.amountTextField.panel = this;
    this.amountTextField.onkeypress = function(e){
    	
    	if(e.keyCode == Event.KEY_RETURN)
		{
			if(this.value == null || this.value == ''){
				this.panel.onError('Invalid amount');
				return;
			}
			
			this.panel.trackDataTextField.focus();	
			return;
		}    	
    	
    };
    	    
  }, 
  
  radioHandler:function(e){
	  var element = Event.element(e);
	  
	  this.trackDataTextField.value = '';
	  this.numberTextField.value = '';
	  
	  switch (element) {
	  
		case this.swipeRadio:	
			this.manualPanel.style.display = 'none';
			this.swipePanel.style.display = '';
			break;
			
		case this.manualRadio:	
			this.swipePanel.style.display = 'none';
			this.manualPanel.style.display = '';				
			break;

		default:
			break;
	  }
  },
  
  okHandler:function(){
	  if(!this.validate()) return;	  
	  this.hide();
	  this.paymentHandler(this.payment);
  },
  
  resetHandler:function(){
  	this.trackDataTextField.value = '';
  	this.numberTextField.value = '';
  	this.amountTextField.value = '';
  	this.swipeRadio.checked = true;
  	
  	this.manualPanel.style.display = 'none';
	this.swipePanel.style.display = '';
  },
  
  onShow:function(){		
	this.amountTextField.value = this.cartTotalAmount;
  	this.amountTextField.select();
  },
  
  validate:function(){
	  
	  if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
		  this.onError('Card not swiped!');
		  this.trackDataTextField.focus();
		  return;
	  }
	  
	  var amt = parseFloat(this.amountTextField.value);
	  
	  if(isNaN(amt)){
		  this.onError('Invalid amount!');
		  this.amountTenderedTextField.focus();
		  return false;
	  }
	  
	  if(amt > PaymentManager.openAmt){
		  this.onError('Gift card amount cannot be more than open amount!');
		  this.amountTenderedTextField.focus();
		  return false;
	  }
	  
	  this.setPayment();
	  
	return true;
  },
  
  setPayment:function(){
  	var payment = new Hash();
  	payment.set('giftCardAmt', this.amountTextField.value);
  	payment.set('giftCardTrackData', this.trackDataTextField.value);
  	payment.set('giftCardNo', '');
  	payment.set('giftCardCVV', '');	  	
  	this.payment = payment;
  },
  
  paymentHandler:function(payment){}
});


var PaymentManager = {
		orderType : 'POS Order',
		openAmt : 0,
		invoiceId: 0,
		orderId: 0,
		cardAmt: 0,
		tenderType : TENDER_TYPE.CASH,
		currencySymbol : '$',
		getOpenAmt : function(){return this.openAmt},
		getCurrencySymbol : function(){return this.currencySymbol},
		setTenderType : function(tenderType){this.tenderType = tenderType},
		getPaymentDetails : function(){
			switch (this.tenderType) {
			
				case TENDER_TYPE.CASH:											
					var panel = new CashPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.EXTERNAL_CARD:
					/* manual creditcard processing */
					var panel = new ExtCardPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;
				
				case TENDER_TYPE.CARD:	
					alert('You are currently offline!');
					break;
					
					var panel = null;
					
					var cardAmt = prompt("Enter card amount",PaymentManager.openAmt);					
					cardAmt = parseFloat(cardAmt);
					
					if(isNaN(cardAmt))
					{
						alert('Invalid card amount!');
						return;
					}
					
					if(cardAmt > PaymentManager.openAmt)
					{
						alert('Card amount cannot be greater than open amount!');
						return;
					}
					
					PaymentManager.cardAmt = cardAmt;
					
					
					if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_XWeb")
					{
						panel = new XWebPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_HF")
					{
						
						
						
						if(ORDER_TYPES.POS_ORDER != this.orderType){
							alert("The configured payment processor only supports sales transactions.");
							return;						
						}
						
						/*sales transactions can be negative*/
						if(this.openAmt < 0){
							alert("The configured payment processor does not support negative sales transactions.");
							return;	
						}
						
						panel = new MercuryPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS")
					{
						if(ORDER_TYPES.POS_ORDER != this.orderType)
						{
							alert("The configured payment processor only supports sales transactions.");
							return;						
						}
						
						/*sales transactions can be negative*/
						if(this.openAmt < 0)
						{
							alert("The configured payment processor does not support negative sales transactions.");
							return;	
						}
						
						panel = new ElementPSPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_EE")
					{
						var panel = new E2EPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Century")
					{
						var panel = new CenturyPanel();					
					}
					else
					{
						panel = new CardPanel();
					}
					
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
				case TENDER_TYPE.CHEQUE:
					var panel = new ChequePanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
									
				case TENDER_TYPE.VOUCHER:
					
					alert('You are currently offline!');
					break;
					
					var panel = new VoucherPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
					
				case TENDER_TYPE.GIFT_CARD:
					
					alert('You are currently offline!');
					break;
					
					var panel = new GiftCardPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
							
	
				default:
					break;
			}
		},
		
		checkout:function(){
		},
		
		paymentHandler:function(payment){
			this.payment = payment;			
			this.processPayment();
		},
		
		setIsPaymentOnline:function(isOnline){
			this.isPaymentOnline = isOnline;
		},
		
		setCashPayment : function(){
			$('cashAmt').value = this.payment.get('amt');
			$("payAmt").value = this.payment.get('amt');
		},
		
		setCardPayment : function(){			
			$('isPaymentOnline').value = this.isPaymentOnline;
			
			if(this.payment.get('cardTrackDataEncrypted') != null){
				$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
				if($('cardTrackDataEncrypted').value) return;
			}			
			
			if(!this.isPaymentOnline) return;
			
			var isCardPresent = this.payment.get('isCardPresent');
			$('cardAmt').value = this.payment.get('amt');
			$('cardType').value = this.payment.get('cardType');
			$("payAmt").value = this.payment.get('amt');
				
			if(isCardPresent){
				$('cardTrackData').value = this.payment.get('trackData');
			}
			else{
				$('cardNo').value = this.payment.get('cardNumber');
				$('cardExpDate').value = this.payment.get('cardExpiryDate');
				$('cardCVV').value = this.payment.get('cardCvv');
				$('cardholderName').value = this.payment.get('cardHolderName');
				$('cardStreetAddress').value = this.payment.get('streetAddress');
				$('cardZipCode').value = this.payment.get('zipCode');
			}	
		},
				
		setChequePayment : function(){
			$('chequeAmt').value = this.payment.get('amt');
			$("payAmt").value = this.payment.get('amt');
			$('chequeNo').value = this.payment.get('chequeNumber');
		},
						
		setExternalCardPayment:function(){
			$('externalCardAmt').value = this.payment.get('amt');
			$("payAmt").value = this.payment.get('amt');
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'false';
		},		
		
		setVoucherPayment : function(){
			$('voucherAmt').value = this.payment.get('voucherAmt');
			$('voucherNo').value = this.payment.get('voucherNo');
		},
		
		setEECardPayment : function(){
			$('cardAmt').value = this.payment.get('cardAmt');
			$("payAmt").value = this.payment.get('amt');
			$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
		},
		
		setGiftCardPayment : function(){
			$('giftCardAmt').value = this.payment.get('giftCardAmt');
			$("payAmt").value = this.payment.get('giftCardAmt');
			
			$('giftCardTrackData').value = this.payment.get('giftCardTrackData');
			$('giftCardNo').value = this.payment.get('giftCardNo');
			$('giftCardCVV').value = this.payment.get('giftCardCVV');			
		},
		
		setOTK :function(otk){
			$('otk').value = otk;
		},
		
		reset : function(){
			$('cashAmt').value = '';
			$('isPaymentOnline').value = 'false';
			$('cardAmt').value = '';
			$('cardType').value = '';
			$('cardTrackData').value = '';
			$('cardTrackDataEncrypted').value = '';
			$('cardNo').value = '';
			$('cardExpDate').value = '';
			$('cardCVV').value = '';
			$('cardholderName').value = '';
			$('cardStreetAddress').value = '';
			$('cardZipCode').value = '';
			$('chequeAmt').value = '';
			$('chequeNo').value = '';
			$('externalCardAmt').value = '';
			$('isPaymentOnline').value = '';
			$('voucherAmt').value = '';
			$('voucherNo').value = '';
			$('otk').value = '';
			
			$("giftCardAmt").value = "0";
			$("giftCardTrackData").value = "";
			$("giftCardNo").value = "";
			$("giftCardCVV").value = "";
			
			$("payAmt").value = "0";
		},
		
		processPayment : function(){
			
			this.reset();
			
			switch (this.tenderType) {
				case TENDER_TYPE.CASH :	
					this.setCashPayment();			
					break;
					
				case TENDER_TYPE.CHEQUE :
					this.setChequePayment();
					break;
					
				case TENDER_TYPE.CARD :
					this.setCardPayment();			  
					break;
					
				case TENDER_TYPE.EXTERNAL_CARD :
					this.setExternalCardPayment();			  
					break;
					
				case TENDER_TYPE.MIXED :
					this.setMixPayment();					
					break;
					
				case TENDER_TYPE.CREDIT :
					this.setOnCreditPayment();					
					break;
					
				case TENDER_TYPE.VOUCHER :
					this.setVoucherPayment();					
					break;
					
				case TENDER_TYPE.GIFT_CARD :
					this.setGiftCardPayment();					
					break;
						
				default:
					break;
			}	
			
			$("tenderType").value = this.tenderType;
			$("invoiceId").value = this.invoiceId;
			$("dateTrx").value = DateUtil.getCurrentDate();
			
			var payment = $('payment-form').serialize(true);
			
			var query = {'uuid' : ORDER.uuid};
			var order = OrderManager.searchOrders(query)[0];
			
			if(order.payments == null){
        		order.payments = [];
        	}
			
			order.payments.push(payment);
			
			OrderManager.saveOrder(order).done(function(){
				
				console.log(arguments[0]);
				
				var order = arguments[1];                        					
				var orderTaxes = OrderManager.getTaxes(order);
				
				sessionStorage.removeItem('order');
				sessionStorage.setItem('order', Object.toJSON(order));
				
				sessionStorage.removeItem('orderTaxes');
				sessionStorage.setItem('orderTaxes', Object.toJSON(orderTaxes));
				
				var openAmt = OrderManager.getOpenAmt(order);
				var launchPrinting = false;
				
				if(openAmt.equals(0)){
					launchPrinting = true;
				}
				
				if(launchPrinting == true){
					window.location.href = 'jsp/offline/view-order.jsp#launchPrinting=true';
					window.location.reload();
					return;
				}
				
				getURL('jsp/offline/view-order.jsp');
			});  			
						
			new Dialog("Processing, please wait").show();
		}
};