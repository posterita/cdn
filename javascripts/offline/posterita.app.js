var APP = {};
APP.switchOnline = function(){
	
	if (!confirm('Do you want to switch to online mode?')) {
	      return;
	}
	
	if(navigator.onLine == false){
		alert('Unable to reach ' + window.location.host + '. Please check your internet connection.');
		return;
	}
	
	jQuery.ajax({
	    cache:false,
	    timeout:4000,
	    type:"HEAD",
	    url:'jsp/offline/dummy.jsp?' + Math.random(),
	    complete:function(xhr, status){ 
	    	var isServerAccessible = (xhr.status == 200);
	    	if(isServerAccessible == false){
	    		alert('Unable to contact ' + window.location.host + '. Please try later.');
	    		return;
	    	}
	    	else
	    	{
	    		/*var basePath = jQuery("base").attr("href");*/
	    		window.location = 'select-user.do';
	    	}
	    }
	});
};

APP.switchOffline = function(){
	
	if (!confirm('Do you want to switch to offline mode?')) {
	      return;
	}
	
	if(!window.applicationCache){
		alert("HTML5 offline web applications (ApplicationCache) are not supported in your browser.");
		return;
	}
	
	if(jQuery.cookie('pos.domain') == null){
		/*domain not found*/
		alert('POS domain not set!');
		return;
	}
	
	if(jQuery.cookie('pos.terminal') == null){
		/*terminal not found*/
		alert('Terminal not set!');
		return;
	}
	
	/* see index.jsp line 79 */
	if(localStorage.getItem('appcache.ready') != 'true'){
		alert('Cached version not found!');
		return;
	}
	
	/* see posterita.offline.js line 80 */
	if(localStorage.getItem('db.ready') != 'true'){
		alert('Offline DB not found!');
		return;
	}
	
	window.location = 'offline/select-user.do';
};