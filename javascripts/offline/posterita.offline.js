var posterita = {
		offline:{
			initialised : false,
			TABLES_TIMESTAMP : TAFFY()
		}
};

posterita.offline.db_schema = {
	    stores: [	
	             	{
        	            name: 'product',
        	            keyPath: "m_product_id"
        	        },
        	        {
        	        	name: 'tax',
        	        	keyPath: 'taxId'
        	        },
        	        {
        	        	name: 'bom',
        	        	keyPath: 'm_product_bom_id',
        	        	indexes:[{keyPath:"bomId"},{keyPath:"productId"}]		        	        
        	        },
        	        {
        	        	name: 'bp',
        	        	keyPath: 'c_bpartner_id'
        	        },
        	        {
        	        	name: 'user',
        	        	keyPath: 'ad_user_id'
        	        },
        	        {
        	        	name: 'role',
        	        	keyPath: 'ad_role_id'
        	        },
        	        {
        	        	name: 'roleOrgAccess',
        	        	keyPath: 'roleOrgAccessId',
        	        	indexes:[{keyPath:"ad_org_id"},{keyPath:"ad_role_id"}]		        	        
        	        },
        	        {
        	        	name: 'terminal',
        	        	keyPath: 'terminalId'	        	        
        	        },
        	        {
        	        	name: 'order',
        	        	keyPath: 'id',
        	        	autoIncrement: true,        	        	
        	        	indexes: [{keyPath:"status"},{keyPath:"uuid"}]
        	        },
        	        {
        	        	name: 'system',
        	        	keyPath: 'table'
        	        }
        	    ]
}; 

posterita.offline.onReady = function(){};

posterita.offline.init = function(syncDB){
	
	if(posterita.offline.initialised){
		return;
	}
		
	var domain = jQuery.cookie('pos.domain');

	if(domain == null){
		//domain not found
		console.error('POS domain not set!');
	}
	else
	{
		var DB_NAME = "posterita_" + domain; 
		this.DB_NAME = DB_NAME;
			    
	    jQuery.db = new ydn.db.Storage(DB_NAME, posterita.offline.db_schema);
		
		jQuery.db.onReady = function(event) {
		  var e = event.getError();
		  if (e) {
		    console.error('Fail to connect to the database');
		    localStorage.setItem('db.ready', false);
		    throw e;
		  } else {
		    console.log('Database ' + jQuery.db.getName() + ' version ' + event.getVersion() +
		      ' [' + jQuery.db.getType() + '] ready.');
		    
		    if(syncDB){
		    	posterita.offline.syncDB();
		    }
		    
		    posterita.offline.initialised = true;
		    posterita.offline.onReady();
		    
		    localStorage.setItem('db.ready', true);
		    
		  }
		};
	}
};


posterita.offline.syncDB = function(){
	
	console.log('Synchronizing database [' + jQuery.db.getName() + ']');
			
	jQuery.db.values('system').done(function(records){
		if(records.length > 0){
			console.log('Loaded ' + records.length + ' timestamps');
			posterita.offline.TABLES_TIMESTAMP().remove();
			posterita.offline.TABLES_TIMESTAMP.insert(records); 
			
			posterita.offline.syncTables();
		}
		else{
			var timestamps = [];
			var tables = "product,tax,bom,bp,user,role,roleOrgAccess,terminal".split(',');
			for(var i=0; i<tables.length; i++){
				var table = tables[i];
				timestamps.push({'table':table, 'lastUpdated':'01/01/10 00:00:00'});
			}
			
			jQuery.db.put('system', timestamps).done(function(keys){
				console.log('Inserted ' + keys.length + ' timestamps');
				
				posterita.offline.syncDB();
				
			}).fail(function(e) {
				  throw e;
			});	
		}
    });
};                
        
        
posterita.offline.getLastUpdated = function(table){
	var lastUpdated = '01/01/10 00:00:00';
	var records = posterita.offline.TABLES_TIMESTAMP({table:table}).get();
	if(records.length > 0){
		lastUpdated = records[0].lastUpdated;
	}
	
	return lastUpdated;
};

posterita.offline.setLastUpdated = function(table, date){
	
	posterita.offline.TABLES_TIMESTAMP({table:table}).update({lastUpdated:date},false);
	
	jQuery.db.put('system', {'table':table, 'lastUpdated':date}).done(function(key){
		console.log(key + ' updated.');
		
	}).fail(function(e) {
		  throw e;
	});		
	
};

posterita.offline.addUpdates = function(table, records, updated){
	
	var dfd = new jQuery.Deferred();
	
	if(records.length == 0){
		console.log('[' + table + '] No updates found.');  
		posterita.offline.setLastUpdated(table, updated);
		dfd.resolve('updated');
		return dfd.promise();
	}
	
	if(records.length > 0){
		console.log('[' + table + '] Found ' + records.length + ' updates.');    			
		
		jQuery.db.put(table, records).done(function(keys){
			console.log('[' + table + '] Added ' + keys.length + ' updates');
			posterita.offline.setLastUpdated(table, updated);
			dfd.resolve('updated');
		}).fail(function(e) {
			  console.error(e);
			  dfd.reject();
		});
	} 
	
	return dfd.promise();
};

posterita.offline.syncTables = function(){
	/*this.syncProducts();*//* called by posterita.offline.syncTerminals() line 345 and 352*/
	this.syncBOMs();
	this.syncTaxes();
	this.syncBps();
	this.syncUsers();
	this.syncRoles();
	this.syncRoleOrgAccess();
	
	this.syncTerminals();
	/*this.syncOrders();*//*use smart order synchronizer*/
	posterita.offline.smartOrderSynchronizer.synchronize();
};

posterita.offline.syncProducts = function(){
	
	var lastUpdated = posterita.offline.getLastUpdated('product');	
	posterita.offline.syncProducts.dfd = new jQuery.Deferred();
	
	jQuery.post("OfflineAction.do",
    		{ action: "getProducts", lastUpdated : lastUpdated},
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('Failed to synchronise products');
    				posterita.offline.syncProducts.dfd.reject();
    				return;
    			}
    			
    			var updated = json.updated;
    			var columns = json.columns;
    			var records = json.data;
    			
    			var products = [];
    			
    			for(var i=0; i<records.length; i++){
    				var product = {};
    				for(var j=0; j<columns.length; j++){
    					if(records[i][j] == null){
    						records[i][j] = '';
    					}
    					product[columns[j]] = records[i][j];	
    				}
    				products.push(product);
    			}
    			
    			posterita.offline.addUpdates('product', products, updated).done(function(){
    				console.info('Done -- products');
					posterita.offline.syncProducts.dfd.resolve();
    			});     			
    			
    		},
    		"json").fail(function(){
    			posterita.offline.syncProducts.dfd.reject();
    			console.error('Failed to synchronise products');
    			}); 
	
	return posterita.offline.syncProducts.dfd;
};

posterita.offline.syncBOMs = function () {

    var lastUpdated = posterita.offline.getLastUpdated('bom');

    jQuery.post("OfflineAction.do", {
        action: "getBoms",
        lastUpdated: lastUpdated
    }, function (json, textStatus, jqXHR) {
    	
    	if(json == null || jqXHR.status != 200){
			console.error('Failed to synchronise product boms');
			return;
		}
    	
        var updated = json.updated;
        var records = json.boms;

        posterita.offline.addUpdates('bom', records, updated);
    },
    "json").fail(function(){console.error('Failed to synchronise product boms');});  

};
    	
posterita.offline.syncTaxes = function(){
	
	var lastUpdated = posterita.offline.getLastUpdated('tax');
	
	jQuery.post("OfflineAction.do",
    		{ action: "getTaxes", lastUpdated : lastUpdated},
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('Failed to synchronise taxes');
    				return;
    			}
    			
    			var updated = json.updated;
    			var records = json.taxes;
    			
    			posterita.offline.addUpdates('tax', records, updated);            			
    		},
			"json").fail(function(){console.error('Failed to synchronise taxes');});      		
};

posterita.offline.syncBps = function(){
	
	var lastUpdated = posterita.offline.getLastUpdated('bp');
	
	jQuery.post("OfflineAction.do",
    		{ action: "getBusinessPartners", lastUpdated : lastUpdated},
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('Failed to synchronise bps');
    				return;
    			}
    			
    			var updated = json.updated;
    			var records = json.bps;
    			
    			posterita.offline.addUpdates('bp', records, updated);            			
    		},
	"json").fail(function(){console.error('Failed to synchronise bps');});  
};

posterita.offline.syncUsers = function(){
	
	var lastUpdated = posterita.offline.getLastUpdated('user');
	
	jQuery.post("OfflineAction.do",
    		{ action: "getUsers", lastUpdated : lastUpdated},
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('Failed to synchronise users');
    				return;
    			}
    			
    			var updated = json.updated;
    			var users = json.users;
    			
    			posterita.offline.addUpdates('user', users, updated); 
    		},
	"json").fail(function(){console.error('Failed to synchronise users');});   
	
};

posterita.offline.syncRoles = function(){
	
	var lastUpdated = posterita.offline.getLastUpdated('role');
	
	jQuery.post("OfflineAction.do",
    		{ action: "getRoles", lastUpdated : lastUpdated},
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('Failed to synchronise roles');
    				return;
    			}
    			
    			var updated = json.updated;
    			var roles = json.roles;
    			
    			posterita.offline.addUpdates('role', roles, updated);
    		},
	"json").fail(function(){console.error('Failed to synchronise roles');});  
	
};

posterita.offline.syncRoleOrgAccess = function(){
	
	var lastUpdated = posterita.offline.getLastUpdated('roleOrgAccess');
	
	jQuery.post("OfflineAction.do",
    		{ action: "getRoleOrgAccess", lastUpdated : lastUpdated},
    		function(json, textStatus, jqXHR){
    			
    			if(json == null || jqXHR.status != 200){
    				console.error('Failed to synchronise role access');
    				return;
    			}
    			
    			var updated = json.updated;
    			var roleOrgAccess = json.roleOrgAccess;
    			
    			//genarate ids
    			for(var i=0; i<roleOrgAccess.length; i++){
    				roleOrgAccess[i].roleOrgAccessId = roleOrgAccess[i].ad_role_id + '_' + roleOrgAccess[i].ad_org_id; 
    			}
    			
    			posterita.offline.addUpdates('roleOrgAccess', roleOrgAccess, updated);
    		},
	"json").fail(function(){console.error('Failed to synchronise role access');});   
	
};  

posterita.offline.syncTerminals = function(){

    var lastUpdated = posterita.offline.getLastUpdated('terminal');

    jQuery.post("OfflineAction.do", {
        action: "getTerminals",
        lastUpdated: lastUpdated
    }, function (json, textStatus, jqXHR) {
    	
    	if(json == null || jqXHR.status != 200){
			console.error('Failed to synchronise terminals');
			return;
		}
    	
        var updated = json.updated;
        var records = json.terminals;

        posterita.offline.addUpdates('terminal', records, updated);
        
        /* normally we get only on terminal */
        /* check for change in plv */
        var plv = records[0].so_priceListVersionId;
        var oldPlv = posterita.offline.getLastUpdated('SO_PriceListVersion');        
        
        if(oldPlv != plv){
        	posterita.offline.setLastUpdated('SO_PriceListVersion', plv);        	
        	
        	jQuery.db.clear('product').done(function(){
        		console.log('Removed all products');
        		posterita.offline.setLastUpdated('product', '01/01/10 00:00:00');
        		
        		posterita.offline.syncProducts().done(function(){
        			console.info('Done -- sync products');
        		});
        		
            }).fail(function (e) {
                throw e;
            });
        }
        else{
        	posterita.offline.syncProducts().done(function(){
    			console.info('Done -- sync products');
    		});
        }
    },
    "json").fail(function(){console.error('Failed to synchronise terminals');});  

};

posterita.offline.onSyncOrdersComplete = function(results){};

posterita.offline.syncOrders = function () {

    jQuery.db.values('order', null, 10000).done(function (records) {

        var tmp = []; /*orders to be sync*/

        for (var i = 0; i < records.length; i++) {
            var order = records[i];
            
            if(jQuery.db.getType() == 'websql'){
				order.lines = jQuery.parseJSON(order.lines);
				order.orderTaxes = JSON.parse(order.orderTaxes);
			}
            
            if (order.status == null || order.status == '' || order.status == 'DR') {
            	tmp.push(order);
            }            
        }

        if (tmp.length == 0) {
            return;
        }

        console.log('[order] synchronizing ' + tmp.length + ' orders.');

        var orders = null;
        
        if(typeof Prototype == "undefined"){
        	orders = JSON.stringify(tmp);
        }
        else{
        	orders = Object.toJSON(tmp);
        }
        

        jQuery.post("OfflineAction.do", {
            action: "syncOrders",
            json: orders

        }, function (json, textStatus, jqXHR) {
        	
        	if(json == null || jqXHR.status != 200){
    			console.error('Failed to synchronise orders');
    			return;
    		}

            var results = json;
            for (var i = 0; i < results.length; i++) {

                var result = results[i];
                var uuid = result.uuid;
                var status = result.status;
                                
                if(status == 'ER'){
                	console.error('[order] Failed to synchronize ' + uuid + '! Error: ' + result.error);
                	
                	jQuery.db.get(new ydn.db.IndexValueCursors('order', 'uuid', ydn.db.KeyRange.only(uuid))).done(function(record){

                        var order = record;
                        order.error = true;
                        order.sync = false;
                        order.status = status;
                        order.errormsg = result.error;

                        jQuery.db.put('order', order).done(function (key) {
                            

                        }).fail(function (e) {
                            throw e;
                        });

                    }).fail(function (e) {
                        throw e;
                    });
                }
                else
                {  
                	jQuery.db.get(new ydn.db.IndexValueCursors('order', 'uuid', ydn.db.KeyRange.only(uuid))).done(function(record){

                        var order = record;
                        order.sync = true;
                        order.status = status;

                        jQuery.db.put('order', order).done(function (key) {
                        	if(status == 'DR'){
                        		console.log('[order] queued #' + key);
                        	}
                        	
                        	if(status == 'CO'){
                        		console.log('[order] synchronized #' + key);
                        	}                            

                        }).fail(function (e) {
                            throw e;
                        });

                    }).fail(function (e) {
                        throw e;
                    });
                	
                }/*if*/                
            }/*for*/
            
            posterita.offline.onSyncOrdersComplete(results);
        },
        "json").fail(function(){console.error('Failed to synchronise orders');});  
    });

};

posterita.offline.smartOrderSynchronizer = {
		limit : 100,
		offset : 0,
		pushFrequency : 2,
		status : 'IDLE',
		orderQueue : null,
		pushTimeoutHandle : null,
		synchronize : function(){
			if(this.status == 'RUNNING') return;
			
			this.status = 'RUNNING';
			this.loadOrderQueue();					
		},
		loadOrderQueue : function(){			
			
			posterita.offline.smartOrderSynchronizer.orderQueue = [];
			
			var iter_new = new ydn.db.IndexValueCursors('order', 'status', ydn.db.KeyRange.only(''));
			jQuery.db.values(iter_new).done(function(records){
				console.info('smartOrderSynchronizer: found -> ' + records.length + ' [NEW] orders');
			    for(var i=0; i<records.length; i++){			    	
			    	var order = records[i];
		            
		            if(jQuery.db.getType() == 'websql'){
						order.lines = jQuery.parseJSON(order.lines);
						order.orderTaxes = JSON.parse(order.orderTaxes);
					}
		            
		            posterita.offline.smartOrderSynchronizer.orderQueue.push(order);
			    }/*for*/
			    
			    var iter_drafted = new ydn.db.IndexValueCursors('order', 'status', ydn.db.KeyRange.only('DR'));
			    jQuery.db.values(iter_drafted).done(function(records){
			    	console.info('smartOrderSynchronizer: found -> ' + records.length + ' [IN PROGESS] orders');
				    for(var i=0; i<records.length; i++){
				    	var order = records[i];
			            
			            if(jQuery.db.getType() == 'websql'){
							order.lines = jQuery.parseJSON(order.lines);
							order.orderTaxes = JSON.parse(order.orderTaxes);
						}
			            
			            posterita.offline.smartOrderSynchronizer.orderQueue.push(order);
				    }/*for*/
				    
				    posterita.offline.smartOrderSynchronizer.onOrderQueueReady();
				});
			});
		},
		

		onOrderQueueReady : function(){
			if(this.orderQueue.length == 0) return;
			
			console.info('smartOrderSynchronizer: ready to process -> ' + this.orderQueue.length + ' orders');	
			this.offset = 0;
			this.push();
		},
		
		push : function(){
			var tmp = [];
			var orders = null;
			
			if(this.offset == this.orderQueue.length){
				console.info('Pushed all. Returning ...');
				posterita.offline.onSyncOrdersComplete();
				
				this.status = 'IDLE';
				return;
			}
			
			var startAt = this.offset;
			var stopAt = this.limit + this.offset;		
			
			if(stopAt > this.orderQueue.length){
				stopAt = this.orderQueue.length;
			} 
			
			console.info('Pushing ' + startAt + ' to ' + stopAt);
						
			for(var i=startAt; i<stopAt; i++){
				tmp.push(this.orderQueue[i]);
			}
			
			this.offset = stopAt;
	        
	        if(typeof Prototype == "undefined"){
	        	orders = JSON.stringify(tmp);
	        }
	        else{
	        	orders = Object.toJSON(tmp);
	        }
	        
	        /*--------------------------------------------------------------------------------------*/
	        jQuery.post("OfflineAction.do", {
	            action: "syncOrders",
	            json: orders

	        }, function (json, textStatus, jqXHR) {
	        	
	        	if(json == null || jqXHR.status != 200){
	        		posterita.offline.smartOrderSynchronizer.abort();
	        		return;
	        	}

	            var results = json;
	            for (var i = 0; i < results.length; i++) {

	                var result = results[i];
	                var uuid = result.uuid;
	                var status = result.status;
	                                
	                if(status == 'ER'){
	                	console.error('[order] Failed to synchronize ' + uuid + '! Error: ' + result.error);
	                	
	                	jQuery.db.get(new ydn.db.IndexValueCursors('order', 'uuid', ydn.db.KeyRange.only(uuid))).done(function(record){

	                        var order = record;
	                        order.error = true;
	                        order.sync = false;
	                        order.status = status;
	                        order.errormsg = result.error;

	                        jQuery.db.put('order', order).done(function (key) {
	                            

	                        }).fail(function (e) {
	                            throw e;
	                        });

	                    }).fail(function (e) {
	                        throw e;
	                    });
	                }
	                else
	                {  
	                	jQuery.db.get(new ydn.db.IndexValueCursors('order', 'uuid', ydn.db.KeyRange.only(uuid))).done(function(record){

	                        var order = record;
	                        order.sync = true;
	                        order.status = status;

	                        jQuery.db.put('order', order).done(function (key) {
	                        	if(status == 'DR'){
	                        		console.log('[order] queued #' + key);
	                        	}
	                        	
	                        	if(status == 'CO'){
	                        		console.log('[order] synchronized #' + key);
	                        	}                            

	                        }).fail(function (e) {
	                            throw e;
	                        });

	                    }).fail(function (e) {
	                        throw e;
	                    });
	                	
	                }/*if*/                
	            }/*for*/
	            
	            /*wait for sometime before pushing again*/
	            window.clearTimeout(this.pushTimeoutHandle);
		        this.pushTimeoutHandle = window.setTimeout(function(){
		        	posterita.offline.smartOrderSynchronizer.push();
		        }, posterita.offline.smartOrderSynchronizer.pushFrequency*1000);
	        },
	            "json").fail(function(){
	            	posterita.offline.smartOrderSynchronizer.abort();
	            	});  
	        /*--------------------------------------------------------------------------------------*/	        
		},
		
		abort : function(){
			console.info('Pushed aborted. Returning ...');
			posterita.offline.onSyncOrdersComplete();
			
			this.status = 'IDLE';
			return;
		}
		
};