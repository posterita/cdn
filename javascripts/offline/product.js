var ProductManager = {
		initialised : false,
		
		init : function(){
			if(this.initialised) return;
			
			this.loadProducts();			
			this.initialised = true;
		},
		
		loadProducts : function(){	
			console.log('[ProductManager] Loading products');
			 jQuery.db.values('product', null, 10000).done(function(records){
				 ProductManager.db = TAFFY(records);
				 console.log('[ProductManager] Found ' + records.length + ' products.');
				 
				 ProductManager.onReady();
		     });			
		},
		
		getProductById : function(productId){
			this.init();
			var query = {"m_product_id" : productId};			
			var results = this.db(query).get();
			
			if(results.length != 0){
				var product = Object.clone(results[0]);
				return product;
			}
			
			return null;
		},
		
		searchProducts : function(query){
			this.init();
			var results = this.db(query).limit(50).get();
			return results;
		},
		
		onReady : function(){}
};

var ProductBOMManager = {
		initialised : false,
		
		init : function(){
			if(this.initialised) return;
			
			this.loadBoms();			
			this.initialised = true;
		},
		
		loadBoms : function(){
			console.log('[ProductBOMManager] Loading boms');
			jQuery.db.values('bom').done(function(records){
				ProductBOMManager.db = TAFFY(records);
				console.log('[ProductBOMManager] Found ' + records.length + ' boms.');
				
				ProductBOMManager.onReady();
		     });
		},
		
		getBoms : function(productId){
			this.init();
			var query = {"productId" : productId};			
			return this.db(query).get();
		},
		
		onReady : function(){}
};