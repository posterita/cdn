/** search product.js **/
var SearchProduct = {
		searchResultPlaceHolder:'productSearchPlaceHolder',
		isSoTrx:true,
		ignoreTerminalPricelist:false,
		input:null,
		active:null,
		search:function(){
			if(SearchProduct.input == null) SearchProduct.input = $('search-product-button');
						
			var searchTerm = SearchProduct.input.value;
			
			if(searchTerm == null || searchTerm == ""){
				this.executeSearchOffline(null);
				return;
			}
			
			var query = [
			     {"upc":searchTerm},
			     {"sku":searchTerm},
			     {"name":{leftnocase:searchTerm}},
			     {"primarygroup":{leftnocase:searchTerm}},
			     {"product_category":{leftnocase:searchTerm}},
			     {"description":{likenocase:searchTerm}} 
			     
			];
			
			this.executeSearchOffline(query);			
		},
		
		executeSearchOffline:function(query){
			var records = ProductManager.searchProducts(query);
			var json = {};
			json.success = true;
			json.products = records;
			
			this.render(json);
		},
		
		render:function(json){
			var container = $('searchResultContainer');
			container.innerHTML = '';
			
			var html = "<table id='searchResultTable' border='0' width='100%'>";

			var results = json.products;
			
			//filter modifiers
			var products = [];
			
			for(var i=0; i<results.length; i++){
				var record = results[i];
				if(record.ismodifier == 'Y') continue;
				
				products.push(record);
			}
			
			if (!json.success)
			{
				if (json.shopSavvy)
				{
					var shopSavvyPanel = new ShopSavvyProductPanel(json.shopSavvy);
					shopSavvyPanel.show();
				}
				else
				{
					var productSearchErrorPanel = new ProductSearchErrorPanel(SearchProduct.input.value);
					productSearchErrorPanel.show();
				}
			}
			
			for(var i=0; i<products.length; i++)
			{
				var product = products[i];
				
				html += "<tr class='" + ((i%2==0)? "odd" : "even" ) +"'>";
				html += "<td>";
				html += product.name;
				html += "<br>";
				html += product.description;
				html += "</td>";
				html += "<td>";
				html += product.qtyonhand;
				html += "</td>";
				html += "</tr>";

			}

			html += "</table>";
			
			container.innerHTML = html;

			/*add behaviour*/
			var table = $('searchResultTable');
			var rows = table.rows;
			
			if (products.length == 1)
			{
				var product = products[0];

				SearchProduct.onSelect(product);
			}
			
			for(var i=0; i<rows.length; i++)
			{
				var row = rows[i];
				row.product = products[i];

				row.style.cursor = 'pointer';
				row.onclick = function(e){
					SearchProduct.onSelect(this.product);
					
					var highlightColor = 'search-product-highlight';
					
					if(SearchProduct.active){
						SearchProduct.active.removeClassName(highlightColor);						
					}
					
					SearchProduct.active = this;
					this.addClassName(highlightColor);
				};
			}
			
			SearchProduct.afterRender();
			
		},
		afterRender:function(){
			jQuery('#pane3').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		},
		onSelect:function(product){
			shoppingCart.addToCart(product.m_product_id,1);
		},
		initializeComponents:function(){
			SearchProduct.input = $('search-product-textfield');
			$('search-product-button').onclick = function(e){
				SearchProduct.search();
			};

			$('search-product-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchProduct.search();
				}
			};
			
			$('search-product-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-product-textfield').onclick = function(e){
				this.select();
			};
		},
		highlightTextField : function()
		{
			$('search-product-textfield').select();
		},
		clearTextField : function()
		{
			$('search-product-textfield').value = '';
		},
		disableTextFieldFocus : function()
		{
			$('search-product-textfield').blur();
		}
};

var SearchProductFilter = {
		data : null,
		selection : null,
		query : null,		
		
		setSelectionQuery : function(query){
			this.query = query;
			this.applySelection();
		},
				
		applySelection : function(){
						
			/*
			
			var filterQuery = this.query;
			
			var url = "ProductAction.do";
			
			var param = new Hash();
			
			param.set('action', 'search');			
			param.set('searchTerm', '');
			param.set('isSoTrx', SearchProduct.isSoTrx);
			param.set('bpPricelistVersionId', SearchProduct.bpPricelistVersionId);
			param.set('warehouseId', SearchProduct.warehouseId);
			param.set('filterQuery', filterQuery);
			param.set('ignoreTerminalPricelist', SearchProduct.ignoreTerminalPricelist);
			
			param = param.toQueryString();
			
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'get', 
				parameters: param, 
				onSuccess: function (response) {
					//update info
					var json = response.responseText.evalJSON(true);
					SearchProduct.render(json);
				}
			});
			*/
			
			/*parse filter query*/	
			var filterQuery = this.query;
			
			var query = {};
			var params = filterQuery.split(';');
			for(var i=0; i< params.length-1; i++){
			    var param = params[i];
			    var keyValue = param.split('=');
			    var key = keyValue[0];
			    var value = keyValue[1];
			    
			    query[key] = value;
			}
						
			SearchProduct.executeSearchOffline(query);
		}
};