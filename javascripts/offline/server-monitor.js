jQuery(document).ready(function(){
	NetworkMonitor.frequency = 1*60*1000;
	NetworkMonitor.decay = 30*1000;
	NetworkMonitor.monitorServer();

	NetworkMonitor.networkStateChange = function(on){
		if(on == true){
			jQuery.pnotify({    
		        text: 'Network connected',
		        type: 'success',
		        icon: 'icon-signal'
		        });
		}
		else{
			jQuery.pnotify({    
		        text: 'We are experiencing network problem',
		        type: 'error',
		        icon: 'icon-signal'
		        });
		}
	};
	NetworkMonitor.serverStateChange = function(on){
		if(on == true){
			jQuery.pnotify({    
		        text: window.location.host + ' is up',
		        type: 'success',
		        icon: 'icon-signal'
		        });
		}
		else{
			jQuery.pnotify({    
		        text: window.location.host + ' is unreachable',
		        type: 'error',
		        icon: 'icon-signal'
		        });
		}
	};
});