/**
 * Call ShoppingCartAction 
 * Available actions 
 * 1. getCart - no param 
 * 2. addToCart - param1 productId, param2 qty 
 * 3. clearCart 
 * 4. removeFromCart - param1 productId 
 * 5. incrementQty - param1 productId, param2 qty
 * 6. decrementQty - param1 productId, param2 qty
 */
function reportShoppingCartReferencingError()
{
	alert("Please use ShoppingCart Class instead!");
}

function reportShoppingCartError()
{
	LOGGER.warn('Operation on cart FAILED!');
}

var ShoppingCart = Class.create( {
	initialize : function(orderType) {
		this.orderType = orderType;
		this.tax = null;
		this.isSoTrx = true;
		this.bpId = null;
		this.pricelistId = null;
		this.lastUpdatedProductId = 0;
		this.orderId = 0;
		this.lines = new Hash();
		this.lineCount = 0;
		this.container = null;		
		this.selectedIndex = -1;
		this.active = false;
		this.currencySymbol = '$';
		
		this.subTotal = new BigNumber(0);
		this.grandTotal = new BigNumber(0);
		this.taxTotal = new BigNumber(0);
		this.qtyTotal = new BigNumber(0);
		this.discountOnTotal = new BigNumber(0);
		this.upsellTotal = new BigNumber(0);
		
		this.initializeShortcuts();
	},
	setContainer : function(container) {
		this.container = container;
	},
	updateCart : function(){
		LOGGER.info('Update shoppingcart');
		
		/*update total*/
		this.updateCartTotal();
		
		/*render HTML*/
		var html = shoppingCartRenderer.renderAsHTML();
		$(this.container).innerHTML = html;	
		
		this.addBehaviourToLines();
	},
	
	resetDiscountOnTotal : function(){
		this.discountOnTotal = new BigNumber(0);
	},
	
	resetDiscountOnLines : function(){
		var lines = this.getLines();
		for(var i=0; i<lines.length; i++){
			var line = lines[i];
			line.resetLine();
		}
	},
	
	updateCartTotal : function(){
		/*reset totals*/
		this.subTotal = new BigNumber(0);
		this.grandTotal = new BigNumber(0);
		this.taxTotal = new BigNumber(0);
		this.qtyTotal = new BigNumber(0);
		/*this.discountOnTotal = new BigNumber(0);*/
		this.upsellTotal = new BigNumber(0);
		
		var taxAmtMap = new Hash();/*ShoppingCart.java line 350*/
		
		var lines = this.getLines();
		for(var i=0; i<lines.length; i++){
			var line = lines[i];
			
			var qty = line.qty;
			var lineAmt = line.lineAmt;
			var lineNetAmt = line.lineNetAmt;
			var taxAmt = line.taxAmt;
			
			this.qtyTotal = this.qtyTotal.plus(qty);
			this.taxTotal = this.taxTotal.plus(taxAmt);
			this.subTotal = this.subTotal.plus(lineAmt.toFixed(2));
			this.grandTotal = this.grandTotal.plus(lineNetAmt.toFixed(2));
			
			this.upsellTotal = this.upsellTotal.plus(line.priceEntered.times(line.qty));
			this.upsellTotal = this.upsellTotal.minus(line.qty.times(line.product.pricelimit));
			
			var amt = taxAmtMap.get(line.tax.taxId);
			if(amt == null){
				amt = [];
			}
			
			amt.push(lineAmt);
			
			taxAmtMap.set(line.tax.taxId, amt);
			
			
			for(var j=0; j<line.boms.length; j++){
				var bom = line.boms[j];
				
				var bomQty = bom.qty;
				var bomLineAmt = bom.lineAmt;
				var bomLineNetAmt = bom.lineNetAmt;
				var bomTaxAmt = bom.taxAmt;
				
				this.qtyTotal = this.qtyTotal.plus(bomQty);
				this.taxTotal = this.taxTotal.plus(bomTaxAmt);
				this.subTotal = this.subTotal.plus(bomLineAmt);
				this.grandTotal = this.grandTotal.plus(bomLineNetAmt);
				
				this.upsellTotal = this.upsellTotal.plus(bom.priceEntered.times(bom.qty));
				this.upsellTotal = this.upsellTotal.minus(bom.qty.times(bom.product.pricelimit));
				
				var amt = taxAmtMap.get(bom.tax.taxId);
				if(amt == null){
					amt = [];
				}
				
				amt.push(bomLineAmt);
				
				taxAmtMap.set(bom.tax.taxId, amt);
			}
			
			for(var j=0; j<line.modifiers.length; j++){
				var modifier = line.modifiers[j];
				
				var modifierQty = modifier.qty;
				var modifierLineAmt = modifier.lineAmt;
				var modifierLineNetAmt = modifier.lineNetAmt;
				var modifierTaxAmt = modifier.taxAmt;
				
				/* don't count modifier */
				/*this.qtyTotal = this.qtyTotal.plus(modifierQty);*/
				this.taxTotal = this.taxTotal.plus(modifierTaxAmt);
				this.subTotal = this.subTotal.plus(modifierLineAmt);
				this.grandTotal = this.grandTotal.plus(modifierLineNetAmt);
				
				this.upsellTotal = this.upsellTotal.plus(modifier.priceEntered.times(modifier.qty));
				this.upsellTotal = this.upsellTotal.minus(modifier.qty.times(modifier.product.pricelimit));
				
				var amt = taxAmtMap.get(modifier.tax.taxId);
				if(amt == null){
					amt = [];
				}
				
				amt.push(modifierLineAmt);
				
				taxAmtMap.set(modifier.tax.taxId, amt);
			}
		}
		
		this.orderTaxes = [];
		this.taxTotal = new BigNumber(0)
		
		var keys = taxAmtMap.keys();
		for(var k=0; k<keys.length; k++){
			var key = keys[k];
			
			var baseAmts = taxAmtMap.get(key);
			var taxId = parseInt(key);
			var tax = TaxManager.getTaxById(taxId);			
			
			var taxAmt = new BigNumber(0);
			
			for(var i=0; i<baseAmts.length; i++){
				var baseAmt = baseAmts[i];				
				var t = baseAmt.times(tax.taxRate).dividedBy(100).toFixed(2);
				taxAmt = taxAmt.plus(t);
			}
			
			this.taxTotal = this.taxTotal.plus(taxAmt);
			
			var orderTax = {};
			orderTax.taxId = taxId;
			orderTax.taxAmt = taxAmt.toFixed(3);
			
			this.orderTaxes.push(orderTax);
		}
		
		this.grandTotal = this.subTotal.plus(this.taxTotal);		
		
	},
	
	successNotifier : function(response) {
		/**/ 
		LOGGER.info('Operation on cart succeded');
	},
	completeNotifier : function(response) {
		/**/ 
		LOGGER.info('Operation on cart completed');
		this.requestCounter --;
	},
	failureNotifier : function(response) {
		/**/ 
		LOGGER.warn('Operation on cart FAILED!');
	},
	exceptionNotifier : function(response) {
		/**/ 
		LOGGER.error('Operation on cart FAILED TO REQUEST!');
		this.requestCounter --;
	},
	getCart : function() {		
		this.updateCart();
	},
	getLineByProductId : function(productId){
		var id = productId;	
		var lines = this.lines.values();
		for(var i=0; i<lines.length; i++){
			var line = lines[i];
			if(line.product.m_product_id == id){
				return line;
			}
		}
		return null;
	},
	getLine : function(lineId){		
		var line = this.lines.get(lineId);
		return line;
	},
	getLines : function(){
		return this.lines.values();
	},
	addLine : function(shoppingCartLine){
		shoppingCartLine.calculateAmt();
		this.lines.set(shoppingCartLine.lineId, shoppingCartLine);		
		this.lastUpdatedLineId = shoppingCartLine.lineId;
	},
	clearCart : function() {
		this.lastUpdatedProductId = 0;
		this.lastUpdatedLineId = 0;
		this.selectedIndex = -1;
		this.lineCount = 0;
		
		this.subTotal = new BigNumber(0);
		this.grandTotal = new BigNumber(0);
		this.taxTotal = new BigNumber(0);
		this.qtyTotal = new BigNumber(0);
		this.discountOnTotal = new BigNumber(0);
		this.upsellTotal = new BigNumber(0);
		
		this.lines = new Hash();		
		
		this.updateCart();	 
	},
	addToCart : function(productId, qty, description, price, modifiers) {		
		var line = this.getLineByProductId(productId);
		
		/*Add modifiers on different lines*/
		if(modifiers != null){
			line = null; /*add new line*/
		}
		
		/*Bug Fix for edit on fly*/
		if((description != null) || (price != null)){
			line = null; /*add new line*/
		}		
		
		if(line == null){
						
			var product = ProductManager.getProductById(productId);
			if(product == null){
				alert('Failed to load product from cache!' + productId);
				return;
			}
			
			line = new ShoppingCartLine(this, product, qty);
			
			if(description){
				line.description = description;				
			}			
			
			if(price){
				line.priceEntered = new BigNumber(price);
				line.product.pricestd = price;
				line.calculateAmt();
			}				
			
			var newLineId = this.lineCount++;
			line.setLineId(newLineId);			
			this.addLine(line);
		}
		else
		{
			line.qty = line.qty.plus(qty);
			if(line.qty.comparedTo(0) < 1){
				this.lines.unset(line.lineId);
			}
			else
			{
				this.addLine(line);
			}
		}
		
		if(modifiers != null){
			var line = this.getLineByProductId(productId);
			line.setModifiers(modifiers);		
		}
		
		this.updateCart();	 
	},
	removeFromCart : function(lineId) {		
		this.lines.unset(lineId);		
		this.updateCart();	 
	},
	incrementQty : function(lineId) {
		this.lastUpdatedLineId = lineId;
		var line = this.getLine(lineId);
		
		var qty = line.qty.plus(1);
		this.updateQty(lineId, qty);
	},
	decrementQty : function(lineId) {
		this.lastUpdatedLineId = lineId;
		var line = this.getLine(lineId);
		
		var qty = line.qty.minus(1);
		this.updateQty(lineId, qty);
	},
	updateQty : function(lineId, qty) {
		this.lastUpdatedLineId = lineId;
		var line = this.getLine(lineId);
		
		line.qty = new BigNumber(qty);
		if(line.qty.comparedTo(0) < 1){
			this.lines.unset(line.lineId);
		}
		else
		{
			this.addLine(line);
		}
		
		this.updateCart();
	},
	splitLines : function(lineId, qty) {
		this.lastUpdatedLineId = lineId;

		var line = this.getLine(lineId);
		line.qty = line.qty.minus(qty);
		this.addLine(line);
		
		var splittedLine = new ShoppingCartLine(this, line.product, qty);
		splittedLine.setLineId(this.lineCount++);
		this.addLine(splittedLine);
		
		this.updateCart();	 
	},
	setDiscountOnLine : function(lineId, amt){
		/*see ShoppingCartManager.setDiscountOnLine*/
		this.lastUpdatedLineId = lineId;
		
		var line = this.getLine(lineId);
		var previousDiscountAmt = line.discountAmt;
		
		line.setDiscountAmt(amt);
		
		var allowUpSell = DISCOUNT_RIGHTS.allowUpSell;		
		if(allowUpSell && this.isSoTrx){
			
			if(this.upsellTotal.lessThan(0.0)){
				line.setDiscountAmt(previousDiscountAmt);
				alert('The upsell buffer cannot be negative!');
			}
		}
	},
	
	setDiscountOnTotal : function(amt){
				
		if(amt == 0){
			return;
		}

		var subTotal = this.subTotal;
		
		
		if(subTotal.equals(0)){
			return;
		}
		
		this.resetDiscountOnTotal();
		this.resetDiscountOnLines();
		this.updateCartTotal();
		
		var amt = new BigNumber(amt);
		var discountOnTotal = new BigNumber(0);
		
		var discountPercentage =  amt.dividedBy(subTotal).toFixed(4);
		
		var lastLine = null;
		
		var taxIncluded = this.priceListIncludeTax;
		
		var lines = this.lines.values();
		
		for(var i=0; i<lines.length; i++){
			var line = lines[i];
			
			var lineAmt = line.lineAmt;
			
			if(taxIncluded){
				lineAmt = line.lineNetAmt;
			}
			
			var discountAmt = lineAmt.times(discountPercentage);				
			discountOnTotal = discountOnTotal.plus(discountAmt);	
			
			discountAmt = discountAmt.plus(line.discountAmt);	
			line.setDiscountAmt(discountAmt);
		}
		
		this.discountOnTotal = discountOnTotal;
		this.updateCart();
	},
	
	setTax : function(lineId, taxId){

		this.lastUpdatedLineId = lineId;
		var line = this.getLine(lineId);
		
		var tax = TaxManager.getTaxById(taxId);
		if(tax == null){
			alert('Failed to load Tax[' + taxId + ']');
			return;
		}
		
		line.tax = tax;
		line.calculateAmt();
		
		this.updateCart();	
	},
	setBp : function(bp){
		
		var pricelistId = bp.priceListId;
		
		if((pricelistId == '') || (pricelistId == this.pricelistId)){
			this.bp = bp;
		}
		else
		{
			this.setPriceList(pricelistId);
		}		
			
	},
	setPriceList : function(pricelistId){

		alert('To be implemented');
		
		this.updateCart();
	},
	addBehaviourToLines : function(){
		
		LOGGER.info('Adding behaviour to cart lines');
		
		if(this.isEmpty())
		{
			if(ShoppingCartManager.quantityTextfield){
				ShoppingCartManager.quantityTextfield.value = '';
			}
			
			this.onChange();
			return;
		}
		
		var lines = this.getLines();
		
		if(this.selectedIndex < 0 
				|| this.selectedIndex == lines.length
				|| lines[this.selectedIndex].lineId != this.lastUpdatedLineId){
			this.selectedIndex = (lines.length - 1);
		}
						
		
		for(var i=0; i<lines.length; i++){
			var line = lines[i];
			var row = $('row' + (i+1));
			
			if(line.lineId == this.lastUpdatedLineId){
				this.selectedIndex = i;
			}
			
			if(row){
				line.element = row;
				row.onclick = this.setSelectedIndex.bind(this,i);
			}			
		}	
		
		this.renderLines();
	},
	setSelectedIndex : function(){
		var index = arguments[0];		
		this.selectedIndex = index;
		
		/*update qty textfield*/
		var lines = this.getLines();
		var currentLine  = lines[this.selectedIndex];
		var qty = currentLine.qty;		
		ShoppingCartManager.quantityTextfield.value = qty + '';
		
		this.renderLines();		
	},
	renderLines : function(){
		/*highlight active row*/
		var lines = this.getLines();
		for(var i=0; i<lines.length; i++){
			var currentLine = lines[i];
			
			var className = ((i%2 == 0) ? 'even' : 'odd');
			
			if(this.selectedIndex == i){
				var highlightColor = 'shopping-cart-highlight';
				className = (highlightColor + ' ' + className);	
			}	
			
			var element = $('row' + (i+1));			
			if(!element) continue;
			
			element.className = className;
			
		}/*for*/ 
		
		var activeRow = $('row' + (this.selectedIndex+1));
		activeRow.scrollIntoView(false);
		
		/*update qty textfield*/
		var currentLine  = lines[this.selectedIndex];
		var qty = currentLine.qty;	
		if(ShoppingCartManager.quantityTextfield){
			ShoppingCartManager.quantityTextfield.value = qty + '';
			
			//shoppingCartChangeNotifier();		
			this.onChange();
		}
		
		/*update tax dropdown*/
		var taxDropDown = $('tax-dropdown');
		if(taxDropDown){
			taxDropDown.value = currentLine.tax.taxId;
		}
		
	},
	isEmpty : function(){
		return (this.lines == null || this.lines.values().length == 0);
	},
	initializeShortcuts : function(){
		/*Add shortcut keys to shopping cart*/
		/*
		 * CTRL+UP move up
		 * CTRL+DOWN move down
		*/
		shortcut.add("Ctrl+Up", this.moveUp.bind(this));
		shortcut.add("Ctrl+Down", this.moveDown.bind(this));
	},
	moveDown : function(){		
		if(this.selectedIndex < (this.lines.values().length - 1))
		{
			this.selectedIndex ++;
			this.renderLines();
		}
	},
	moveUp : function(){
		if(this.selectedIndex > 0)
		{
			this.selectedIndex --;
			this.renderLines();
		}
	},
	onChange : function(){
		alert('onChange not initialized');
	},
	getTax : function(){
		return this.tax;
	},
	getCurrentLine : function(){		
		var line = this.lines.get(this.selectedIndex);
		return line;
	},
});

var ShoppingCartLine = Class.create( {
	initialize : function(shoppingCart, product, qty) {
		this.shoppingCart = shoppingCart;
		this.product = product;
		this.description = this.product.description;
		this.qty = new BigNumber(qty);
		this.priceEntered = new BigNumber(this.product.pricestd);
		this.tax = this.shoppingCart.getTax();
		this.loadBoms();
		this.modifiers = [];
		this.calculateAmt();
		
	},
	
	loadBoms : function(){
		var boms = ProductBOMManager.getBoms(this.product.m_product_id);		
		this.boms = [];
		
		for(var i=0; i<boms.length; i++){
			var bom = boms[i];			
			var product = ProductManager.getProductById(bom.bomId);
			var qty = new BigNumber(bom.qty);
			qty = qty.times(this.qty);
			
			var line = new ShoppingCartLine(this.shoppingCart, product, qty);
			line.bomQty = bom.bomqty;
			this.boms.push(line);			
		}
	},
	
	setModifiers : function(modifiers){
		this.modifiers = [];
		
		for(var i=0; i<modifiers.length; i++){
			var modifier = modifiers[i];
			var product = ProductManager.getProductById(modifier.productId);
			var qty = new BigNumber(1);
			qty = qty.times(this.qty);
			
			var line = new ShoppingCartLine(this.shoppingCart, product, qty);
			line.modifier = modifier;
			
			this.modifiers.push(line);
		}
	},

	getModifiers : function(){
		return this.modifiers;
	},
	
	setLineId : function(lineId){
		this.lineId = lineId;
	},
	
	calculateAmt : function(){
		var taxIncluded = this.shoppingCart.priceListIncludeTax;
		var scale = this.shoppingCart.scale;
		
		if(!this.tax){
			/*take tax from product*/
			var taxCategoryId = this.product.c_taxcategory_id;
			var tax = TaxManager.getTaxByTaxCategoryId(taxCategoryId, this.shoppingCart.isSoTrx);
			this.tax = tax;			
		}
		
		var taxRate = new BigNumber(this.tax.taxRate);		
		var priceEntered = new BigNumber(this.priceEntered);
		var priceList = new BigNumber(this.product.pricelist);		
		var qtyEntered = new BigNumber(this.qty);
		
		var baseAmt = priceEntered.times(qtyEntered);
		
		var taxAmt = null;		
		
		var lineAmt = new BigNumber(0);
		var lineNetAmt = new BigNumber(0);	
		
		if(taxIncluded)
        {		
            lineNetAmt = baseAmt;
            lineAmt = baseAmt.times(100).dividedBy(taxRate.plus(100));	            
            taxAmt = lineNetAmt.minus(lineAmt);
        }
        else
        {
        	taxAmt = taxRate.times(baseAmt).dividedBy(100);
            lineAmt = baseAmt;
            lineNetAmt = lineAmt.plus(taxAmt);
        }
		
		this.discountAmt = priceList.minus(priceEntered).times(qtyEntered);
		this.lineAmt = lineAmt;
		this.lineNetAmt = lineNetAmt;
		this.taxAmt = taxAmt;	
		
		/* discount info */
		this.discountMessage = null;		
		if(this.discountAmt.comparedTo(0) > 0)
		{
			var discountPercentage = priceList.minus(priceEntered);
			discountPercentage = discountPercentage.times(100);
			discountPercentage = discountPercentage.dividedBy(priceList);
			
			this.discountMessage = discountPercentage.toFixed(2) + "% off, Saved(" + this.discountAmt.toFixed(2) + ")";
		}
	},
	
	setDiscountAmt : function(amt){
		var discountAmt = new BigNumber(amt);
		discountAmt = discountAmt.dividedBy(this.qty).toFixed(2);
		
		var baseAmt = new BigNumber(this.product.pricestd);
		baseAmt = baseAmt.minus(discountAmt);
		
		this.priceEntered = baseAmt;
		this.calculateAmt();
		
		/*reset discount on total*/
		this.shoppingCart.resetDiscountOnTotal();		
		this.shoppingCart.updateCart();
	},
	
	resetLine : function(){
		this.priceEntered = new BigNumber(this.product.pricestd);
		this.calculateAmt();
	}
	
});


var ShoppingCartManager = {
		
		getCart : function(){
			return shoppingCart;
		},
		
		refreshCart : function(){
			this.getCart().getCart();
		},
		
		clearCart : function(){
			this.getCart().clearCart();
		},
		
		getOrderId : function(){
			return this.getCart().orderId;
		},
		
		getOrderType : function(){
			return this.getCart().orderType;
		},
		
		getLineId : function(){
			var lineId = null;
			
			var cart = this.getCart();
			
			if(!this.isShoppingCartEmpty()){
				var lines = cart.getLines();
				lineId = lines[cart.selectedIndex].lineId;
			}
			
			return lineId;
		},
		
		scrollUp : function(){
			this.getCart().moveUp();
		},
		
		scrollDown : function(){
			this.getCart().moveDown();
		},
		
		addToCart : function(productId){
			if(productId == null) return;			
			this.getCart().addToCart(productId,1);
		},
		
		incrementQty : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			this.getCart().incrementQty(lineId);
		},
		
		decrementQty : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			this.getCart().decrementQty(lineId);
		},
		
		updateQty : function(qty){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			
			var lines = cart.getLines();
			var currentLine  = lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
						
			cart.updateQty(lineId, qty);
		},
		
		splitLines : function(qty){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			
			var lines = cart.getLines();
			var currentLine  = lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
						
			cart.splitLines(lineId, qty);
		},
		
		removeFromCart : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			this.getCart().removeFromCart(lineId);
		},
		
		setLineTax : function(taxId){
			var lineId = this.getLineId();
			if(lineId != null && taxId != null) this.getCart().setTax(lineId, taxId);
		},
		
		setBp : function(bpId){
			this.getCart().setBp(bpId);
		},
		
		getGrandTotal : function(){
			return this.getCart().grandTotal;
		},
		
		getSubTotal : function(){
			return this.getCart().subTotal;
		},
		
		getDiscountOnTotal : function(){
			return this.getCart().discountOnTotal;
		},
		
		getTaxTotal : function(){
			return this.getCart().taxTotal;
		},
		
		getUpsellTotal:function(){
			return this.getCart().upsellTotal;
		},
		
		getLineNetTotal : function(){
			var lineNetTotal = ShoppingCartManager.getSubTotal() + ShoppingCartManager.getTaxTotal();
			return lineNetTotal;
		},
		
		getQtyTotal : function(){
			return this.getCart().qtyTotal;
		},
		
		getCurrencySymbol : function(){
			return this.getCart().currencySymbol;
		},
		
		isShoppingCartEmpty : function(){
			return this.getCart().isEmpty();
		},
		
		isShoppingCartReady : function(){
			return true;
		},
		
		setDiscountOnTotal : function(discountAmt){			
			this.getCart().setDiscountOnTotal(discountAmt);
		},
		
		setDiscountOnLine : function(discountAmt){
			var cart = this.getCart();
			var lines = cart.getLines();
			
			if(lines && lines.length > 0){
				var lineId = lines[cart.selectedIndex].lineId;
				cart.setDiscountOnLine(lineId,discountAmt);
			}
		},
		
		initializeComponents : function(){
			
			this.scrollUpButton = $('scroll-up-button');
			this.scrollDownButton = $('scroll-down-button');
			this.clearButton = $('clear-button');
			this.addButton = $('add-button');
			this.decreaseButton = $('decrease-button');
			this.removeButton = $('remove-button');
			this.productInfoButton = $('product-info-button');
			this.splitLineButton = $('split-line-button');
			
			this.quantityTextfield = $('quantity-texfield');
			
			/* add behaviour */	
			this.scrollUpButton.onclick = function(e){
				ShoppingCartManager.scrollUp();
			};
			
			this.scrollDownButton.onclick = function(e){
				ShoppingCartManager.scrollDown();
			};
			
			this.clearButton.onclick = function(e){
				ShoppingCartManager.clearCart();
			};
			
			this.addButton.onclick = function(e){
				ShoppingCartManager.incrementQty();
			};
			
			this.decreaseButton.onclick = function(e){
				ShoppingCartManager.decrementQty();
			};
			
			if(this.removeButton){
				this.removeButton.onclick = function(e){
					ShoppingCartManager.removeFromCart();
				};
			}
			
			if(this.productInfoButton){				
				this.productInfoButton.onclick = function(e){
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert('Cart is empty!');
						return;
					}
					
					var lines = shoppingCart.getLines();
					var currentLine  = lines[shoppingCart.selectedIndex];
					var productId = currentLine.productId;
					
					new ProductInfoPanel().getInfo(productId);
				};
			}
			
			if(this.splitLineButton){
				this.splitLineButton.onclick = function(e){
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert('Cart is empty!');
						return;
					}
						
					var lines = shoppingCart.getLines();
					var currentLine  = lines[shoppingCart.selectedIndex];
					var currentLineQty = currentLine.qty;
					
					if(currentLineQty == 1) return;
						
					var qty = window.prompt('Enter qty', '1');
					qty = parseFloat(qty);
					
					if(isNaN(qty)){
						alert('Invalid Qty!');						
						return;					
					}
					
					if(qty >= currentLineQty){
						alert('Qty entered must be less than ' + currentLineQty + '!');						
						return;	
					}
					
					ShoppingCartManager.splitLines(qty);	
				};
			}
			
			/* add keypad to qty textfield */
			Event.observe(this.quantityTextfield,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
			
			/* bug fix for updateQtyTextField */
			this.quantityTextfield.onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					if(!ShoppingCartManager.isShoppingCartEmpty()){
						var qty = parseFloat(this.value);						
						if(isNaN(qty)){
							alert('Invalid Qty!');
							this.selectAll();								
							return;					
						}
						ShoppingCartManager.updateQty(qty);							
					}
				}
			}
		}
		
};

var shoppingCartRenderer = {};
shoppingCartRenderer.renderAsHTML = function(){
	var shoppingCart = ShoppingCartManager.getCart();
	
	var UOM_PRECISION = 0;
	var CURRENCY_PRECISION = 2;
	
	var html = '<div id="shopping-cart-line-details-container"><div id="shopping-cart-lines-container"><div>' + 
	'<table border="0" cellpadding="4" cellspacing="0" width="100%"><tbody>';
	
	/* add lines as tr */
	var count = 0;
	shoppingCart.lines.each(function(pair){
		count ++;
		
		var line = pair.value;
		var name = line.product.name;
		var description = line.description;
		var qty = line.qty;
		var lineAmt = line.lineAmt;
		var lineNetAmt = line.lineNetAmt;
		var taxAmt = line.taxAmt;
		var discountMessage = line.discountMessage;
		
		html += '<tr class="' + ((count%2 == 0) ? 'odd' : 'even') + '" id="row' + count +'"><td width="25">' + qty.toFixed(UOM_PRECISION) + 'x</td>';
		html += '<td><p>' + name + '</p><p>' + description + '</p>' + (discountMessage == null  ? '' : ( '<p>' + discountMessage + '</p>')) + '</td>';
		html += '<td class="flushRight" width="60">' + lineAmt.toFixed(CURRENCY_PRECISION) + '</td></tr>';	
		
				
		for(var i=0; i<line.boms.length;i++){
			var bom = line.boms[i];
			
			html += '<tr class="' + ((i%2 == 0) ? 'odd' : 'even') +'"><td width="25">&nbsp;</td>';
			html += '<td>' + bom.qty.toFixed(UOM_PRECISION) + 'x <p>' + bom.product.name + '</p><p>' + bom.product.description + '</p></td>';
			html += '<td class="flushRight" width="60">' + bom.lineAmt.toFixed(CURRENCY_PRECISION) + '</td></tr>';	
		}

		for(var i=0; i<line.modifiers.length;i++){
			var modifier = line.modifiers[i];
			
			html += '<tr class="' + ((i%2 == 0) ? 'odd' : 'even') +'"><td width="25">&nbsp;</td>';
			html += '<td>&nbsp;<p>' + modifier.product.name + '</p><p>' + modifier.product.description + '</p></td>';
			html += '<td class="flushRight" width="60">' + modifier.lineAmt.toFixed(CURRENCY_PRECISION) + '</td></tr>';	
		}
		
	});
	
	html += '</tbody></table></div></div><div id="shopping-cart-total-container">' +
	'<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%"><tbody>';
	
	html += '<tr id="sub-total"><td width="25">&nbsp;</td><td>Subtotal</td><td width="75">' + shoppingCart.subTotal.toFixed(CURRENCY_PRECISION) + '</td></tr>';
 
	html += '<tr id="tax-total"><td width="25">&nbsp;</td><td>Tax</td><td>' + shoppingCart.taxTotal.toFixed(CURRENCY_PRECISION) + '</td></tr>';
 
	html += '<tr id="grand-total"><td width="25">' + shoppingCart.qtyTotal.toFixed(UOM_PRECISION) + 'x</td><td>Grand Total</td><td class="flushRight">' + shoppingCart.grandTotal.toFixed(CURRENCY_PRECISION) + '</td></tr>';
	
	html += '</tbody></table></div></div><div id="shopping-cart-bg3"></div>';
	
	return html;
};

/*blah blah blah*/
