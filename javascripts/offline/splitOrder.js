var SplitSalesRep = Class.create(SalesRep, {
	initialize : function($super, salesRep)
	{
		if (salesRep != null)
		{
			$super(salesRep.getName(), salesRep.getId());
			this.current = salesRep.isCurrent();
		}
		this.amount = 0;
	},
		
	setAmount : function (amount)
	{
		this.amount = amount;
	},
	
	getAmount : function()
	{
		return this.amount;
	},
	
	clone : function()
	{
		var clone = new SplitSalesRep();
		clone.id = this.id;
		clone.name = this.name;
		clone.amount = this.amount;
		clone.current = this.current;
		
		return clone;
	}
});

var SalesRepAmountChangeEvent = Class.create({
	initialize : function(salesRepEditor)
	{
		this.source = salesRepEditor;
		this.value = salesRepEditor.getValue();
	},
	
	getSource : function()
	{
		return this.source;
	},
	
	getValue : function()
	{
		return this.value;
	}
});

var SalesRepEditor = Class.create({
	
	initialize : function(splitSalesRep)
	{
		if (splitSalesRep != null)
		{		
			this.splitSalesRep = splitSalesRep;
			this.component = null;
			this.amountEditor = null;
			// listeners
			this.salesRepAmountChangeListeners = new ArrayList();
			this.onSelectSalesRepListeners = new ArrayList();
			
			this.initComponent();
		}
	},
	
	getSalesRep : function()
	{
		return this.splitSalesRep;
	},
	
	initComponent : function()
	{
		var name = this.splitSalesRep.getName();
		var amount = this.splitSalesRep.getAmount();
		
		var labelCell = document.createElement('td');
		var textCell = document.createElement('td');
		
		var labelElement = null;
		
		if (this.isReference())
		{
			labelElement = document.createElement('span');
			labelElement.innerHTML = name;
			
			this.amountEditor = document.createElement('span');
			this.amountEditor.innerHTML = amount;
		}
		else
		{
			labelElement = document.createElement('button');
			labelElement.setAttribute('type', 'button');
			labelElement.setAttribute('class', 'big-button')
			labelElement.innerHTML = name;
			labelElement.onclick = this.fireOnSelectSalesRepEvent.bind(this);
			
			this.amountEditor = document.createElement('input');
			this.amountEditor.setAttribute('type', 'text');
			this.amountEditor.setAttribute('class', 'salesRepInput');
			this.amountEditor.setAttribute('style', 'font-size:20px; height:40px;');
			this.amountEditor.value = amount;
			this.amountEditor.onkeyup = this.fireSalesRepAmountChangeEvent.bind(this);
			this.amountEditor.onclick = this.fireOnSelectSalesRepEvent.bind(this);
			
			Event.observe(this.amountEditor,'focus',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		}
		
		labelCell.appendChild(labelElement);
		textCell.appendChild(this.amountEditor);
		this.component = document.createElement('tr');
		this.component.appendChild(labelCell);
		this.component.appendChild(textCell);
	},
	
	fireSalesRepAmountChangeEvent : function()
	{
		var amtEntered = this.getValue();
		if (amtEntered > OrderAmount.getSubTotal())
		{
			this.setValue(this.splitSalesRep.getAmount());
			return;
		}
		
		this.splitSalesRep.setAmount(amtEntered);
		var salesRepAmountChangeEvent = new SalesRepAmountChangeEvent(this);
		for (var i=0; i<this.salesRepAmountChangeListeners.size(); i++)
		{
			var salesRepAmountChangeListener = this.salesRepAmountChangeListeners.get(i);
			salesRepAmountChangeListener.salesRepAmountChange(salesRepAmountChangeEvent);
		}
	},
	
	fireOnSelectSalesRepEvent : function()
	{
		for (var i=0; i<this.onSelectSalesRepListeners.size(); i++)
		{
			var onSelectSalesRepListener = this.onSelectSalesRepListeners.get(i);
			onSelectSalesRepListener.onSelectSalesRep(this);
		}
	},

	addSalesRepAmountChangeListener : function(listener)
	{
		this.salesRepAmountChangeListeners.add(listener);
	},
	
	addOnSelectSalesRepListener : function(listener)
	{
		this.onSelectSalesRepListeners.add(listener);
	},
	
	setFocus : function()
	{
		this.amountEditor.setAttribute('style', 'background-color:#9ACF30; font-weight:bold; font-size:20px; height:40px;');
	},
	
	resetFocus : function()
	{
		this.amountEditor.removeAttribute('style');
	},
	
	setStateChanged : function()
	{
		this.fireSalesRepAmountChangeEvent();
	},
	
	setValue : function(value)
	{
		this.splitSalesRep.setAmount(value);
		if (this.isReference())
		{
			this.amountEditor.innerHTML = value;
		}
		else
		{
			this.amountEditor.value = value;
		}
	},
	
	getValue : function()
	{
		if (this.isReference())
		{
			return parseFloat(this.amountEditor.innerHTML);
		}
		else
		{
			return parseFloat(this.amountEditor.value);
		}
	},
	
	isReference : function()
	{
		return this.splitSalesRep.isCurrent();
	},
	
	getId : function()
	{
		return this.splitSalesRep.getId();
	},
	
	getComponent : function()
	{
		return this.component;
	},
	
	equals : function (salesRepRowEditor)
	{
		return (this.getId() == salesRepRowEditor.getId());
	}
});

var SplitPanelManager = Class.create(PopUpBase, {
	
	initialize : function(refSalesRep, splitSalesRepList)
	{
		this.panel = $('split.order.popup.panel');
		this.innerPanel = $('split.order.popup.inner.panel');
		this.clear();
		//panel components
		this.subPanel = null;
		this.infoTotal = null;
		this.splitPanelToolBarManager = null;
		// editors
		this.refSalesRepEditor = null;
		this.salesRepEditors = new ArrayList(new SalesRepEditor());
		// event listeners
		this.salesRepAmountChangeListeners = new ArrayList();
		this.splitPanelClickOkListeners = new ArrayList();
		this.splitPanelClickCancelListeners = new ArrayList();
		// initialise components 
		this.initComponents(refSalesRep, splitSalesRepList);
		this.initPanel();
	},
	
	updateInfoTotal : function()
	{
		this.infoTotal.innerHTML = OrderAmount.getSubTotal();
	},
	
	initComponents : function(refSalesRep, splitSalesRepList)
	{
		this.refSalesRepEditor = new SalesRepEditor(refSalesRep);
		
		for (var i=0; i<splitSalesRepList.size(); i++)
		{
			var splitSalesRep = splitSalesRepList.get(i);
			
			var salesRepEditor = new SalesRepEditor(splitSalesRep);
			
			salesRepEditor.addSalesRepAmountChangeListener(this);
			salesRepEditor.addOnSelectSalesRepListener(this);
			
			this.salesRepEditors.add(salesRepEditor);
		}
	},
	
	initPanel : function()
	{
		this.createPopUp($(this.panel.getAttribute('id')));
	    
		this.subPanel =  document.createElement('table');
		this.subPanel.setAttribute('style', 'float:left');
		var infoTotalAmtTr = document.createElement('tr');
		var infoTotalTd = document.createElement('td');
		infoTotalTd.setAttribute('colspan', 2);
		infoTotalTd.setAttribute('align', 'right');
		
		infoTotalTd.setAttribute('style', 'font-weight:bold; font-size:20px');
		
		var labelTotal = document.createElement('div');
		labelTotal.innerHTML = Translation.translate('Net Total','Net Total');
		
		this.infoTotal = document.createElement('div');
		this.infoTotal.innerHTML = OrderAmount.getSubTotal();
		
		infoTotalTd.appendChild(labelTotal);
		infoTotalTd.appendChild(this.infoTotal);
		
		infoTotalAmtTr.appendChild(infoTotalTd);
		
		this.subPanel.appendChild(infoTotalAmtTr);
		
		this.splitPanelToolBarManager = new SplitPanelToolBarManager(this.salesRepEditors, this.refSalesRepEditor);
		
		var toolbar = this.splitPanelToolBarManager.getToolBar();
		this.subPanel.appendChild(toolbar);
		
		this.subPanel.appendChild(this.refSalesRepEditor.getComponent());
		
		for (var i=0; i<this.salesRepEditors.size(); i++)
		{
			var salesRepEditor = this.salesRepEditors.get(i);
			var component = salesRepEditor.getComponent();
			this.subPanel.appendChild(component);
		}
		
		this.okBtn = $('split.order.popup.okButton');
	    this.okBtn.onclick = this.splitPanelClickOk.bind(this);
		
		this.cancelBtn = $('split.order.popup.cancelButton');
	    this.cancelBtn.onclick = this.splitPanelClickCancel.bind(this);
	    
		this.innerPanel.appendChild(this.subPanel);
	},
	
	onSelectSalesRep : function(salesRepEditor)
	{
		this.splitPanelToolBarManager.setSelectedSalesRepEditor(salesRepEditor);
		
		for (var i=0; i<this.salesRepEditors.size(); i++)
		{
			var sRepEditor = this.salesRepEditors.get(i);
			if (sRepEditor.equals(salesRepEditor))
			{
				sRepEditor.setFocus();
			}
			else
			{
				sRepEditor.resetFocus();
			}
		}
	},
	
	splitPanelClickOk : function()
	{
		this.hide();
		for (var i=0; i<this.splitPanelClickOkListeners.size(); i++)
		{
			var listener = this.splitPanelClickOkListeners.get(i);
			listener.splitPanelClickOk();
		}
	},
	
	splitPanelClickCancel : function()
	{
		this.hide();
		for (var i=0; i<this.splitPanelClickCancelListeners.size(); i++)
		{
			var listener = this.splitPanelClickCancelListeners.get(i);
			listener.splitPanelClickCancel();
		}
	},
	
	getRefSalesRepEditor : function()
	{
		return this.refSalesRepEditor;
	},
	
	setRefSalesRepEditor : function(salesRepEditor)
	{
		this.refSalesRepEditor = salesRepEditor;
	},
	
	addSalesRepAmountChangeListener : function(listener)
	{
		this.salesRepAmountChangeListeners.add(listener);
	},
	
	fireSalesRepAmountChangeEvent : function(salesRepAmountChangeEvent)
	{
		for (var i=0; i<this.salesRepAmountChangeListeners.size(); i++)
		{
			var salesRepAmountChangeListener = this.salesRepAmountChangeListeners.get(i);
			salesRepAmountChangeListener.salesRepAmountChange(salesRepAmountChangeEvent);
		}
	},
	
	addSplitPanelClickOkListener : function(listener)
	{
		this.splitPanelClickOkListeners.add(listener);
	},
	
	addSplitPanelClickCancelListener : function(listener)
	{
		this.splitPanelClickCancelListeners.add(listener);
	},
	
	clear : function()
	{
		this.innerPanel.innerHTML = '';
	},
	
	salesRepAmountChange : function(salesRepAmountChangeEvent)
	{
		this.fireSalesRepAmountChangeEvent(salesRepAmountChangeEvent);
	},
	
	getSalesRepEditors : function()
	{
		return this.salesRepEditors;
	},
	
	getPanel : function()
	{
		return this.innerPanel;
	}
});


var SplitPanelToolBarManager = Class.create({
		
		initialize : function(salesRepEditors, refSalesRepEditor)
		{
			this.salesRepEditors = salesRepEditors;	
			this.selectedSalesRepEditor = null;
			this.refSalesRepEditor = refSalesRepEditor;
			this.toolBar = null;
			
			this.initToolBar();
		},
		
		initToolBar : function()
		{
			var tr = document.createElement('tr');
			var td = document.createElement('td');
			td.setAttribute('style', 'text-align:right');
			td.setAttribute('colspan', 2);
			
			var size = this.salesRepEditors.size() + 1;
			
			var resetBtn = document.createElement('button');
			resetBtn.setAttribute('type', 'button');
			resetBtn.setAttribute('class', 'ok-button');
			resetBtn.innerHTML = 'RESET';
			resetBtn.onclick = this.reset.bind(this);
			
			for (var i=1; i<=size; i++)
			{
				var btn = document.createElement('button');
				btn.setAttribute('type', 'button');
				btn.setAttribute('class', 'ok-button')
				btn.innerHTML = "1/" + (i);
				btn.setAttribute('id', (i));
				btn.onclick =  this.calculateAmt.bind(this, btn.getAttribute('id'));
				
				td.appendChild(btn);
				td.appendChild(resetBtn);
				tr.appendChild(td);
			}
			this.toolBar = tr;
		},
		
		reset : function()
		{
			if (this.salesRepEditors.size() == 0)
			{
				return;
			}
			
			for (var i=0; i<this.salesRepEditors.size(); i++)
			{
				var salesRepditor = this.salesRepEditors.get(i);
				salesRepditor.setValue(0);
				salesRepditor.setStateChanged();
			}
		},
		
		calculateAmt : function(id)
		{
			var divisor = new Number(id);
			var selectedSalesRepEditor = this.selectedSalesRepEditor;
			if (selectedSalesRepEditor != null)
			{
				var refValue = OrderAmount.getSubTotal();
				var calculatedAmt = refValue / divisor;
				calculatedAmt = new Number(calculatedAmt).toFixed(2);
				
				selectedSalesRepEditor.setValue(calculatedAmt);
				selectedSalesRepEditor.setStateChanged();
			}
		},
		
		setSelectedSalesRepEditor : function(salesRepEditor)
		{
			this.selectedSalesRepEditor = salesRepEditor;
		},
		
		getToolBar : function()
		{
			return this.toolBar;
		}
});

var SplitManager = Class.create({
	
	initialize : function(splitSReps)
	{
		this.splitPanelManager = null;
		this.splitSalesReps = null;
		this.refSalesRep = null;
		this.salesRepInfoUpdater = null;
		
		this.refSalesRep_bak = null;
		this.splitSalesReps_bak = null;
		
		this.initSalesReps(splitSReps);
		this.initPanelManager();
		
		this.salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		this.refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		this.initSalesRepInfoUpdater();
	},
	
	saveState : function()
	{
		this.refSalesRep_bak = this.refSalesRep.clone();
		this.splitSalesReps_bak = new ArrayList(new SplitSalesRep());
		for (var i=0; i<this.splitSalesReps.size(); i++)
		{
			var splitSalesRep = this.splitSalesReps.get(i);
			var splitSalesRepClone = splitSalesRep.clone();
			this.splitSalesReps_bak.add(splitSalesRepClone); 
		}
	},
	
	rollBack : function()
	{
		this.refSalesRep = this.refSalesRep_bak;
		this.splitSalesReps = this.splitSalesReps_bak;
		this.initPanelManager();
		this.initSalesRepInfoUpdater();
		this.updateSalesRepInfo();
	},
	
	initSalesReps : function(splitSReps)
	{
		this.splitSalesReps = new ArrayList(new SplitSalesRep())
		
		
		for (var i=0; i<splitSReps.size(); i++)
		{
			var splitSalesRep = splitSReps.get(i);
			
			if (splitSalesRep.isCurrent())
			{
				this.refSalesRep = splitSalesRep;
			}
			else
			{
				this.splitSalesReps.add(splitSalesRep);
			}
		}
	},
	
	initPanelManager : function()
	{
		var refSalesRep = this.refSalesRep;
		var splitSalesReps = this.splitSalesReps;
		
		this.splitPanelManager = new SplitPanelManager(refSalesRep, splitSalesReps);
		this.splitPanelManager.addSalesRepAmountChangeListener(this);
		this.splitPanelManager.addSplitPanelClickOkListener(this);
		this.splitPanelManager.addSplitPanelClickCancelListener(this);
		
	},
	
	initSalesRepInfoUpdater : function()
	{
		this.salesRepInfoUpdater = new SalesRepInfoUpdater(this.refSalesRep, this.splitSalesReps);
		this.splitPanelManager.addSplitPanelClickOkListener(this.salesRepInfoUpdater);
	},
	
	resetAmounts : function()
	{
		this.initSalesRepAmounts();
		this.updateSalesRepInfo();
	},
	
	splitPanelClickOk : function()
	{
		this.saveState();
	},
	
	splitPanelClickCancel : function()
	{
		this.rollBack();
	},
	
	initSalesRepAmounts : function()
	{
	},
	
	updateSalesRepInfo : function()
	{
		this.salesRepInfoUpdater.update();
	},
	
	updateInfoTotal : function()
	{
		this.splitPanelManager.updateInfoTotal();
	},
	
	updateAmounts : function()
	{
	},
	
	salesRepAmountChange : function(salesRepAmountChangeEvent)
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var editor = salesRepAmountChangeEvent.getSource();
		var amt = editor.getValue();
		
		var refAmt = refSalesRepEditor.getValue();
		
		var totalAmt = refAmt;
		
		for (var i=0; i<salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);
			totalAmt += salesRepEditor.getValue();			
		}
		
		var orderAmt = OrderAmount.getSubTotal();
		var diff = totalAmt - orderAmt;
		
		if (diff < 0)
		{
			refAmt -= diff;
			refAmt = new Number(refAmt).toFixed(2);
			refAmt = parseFloat(refAmt);
			refSalesRepEditor.setValue(refAmt);
		}
		else if (diff > 0)
		{
			diff = refAmt - diff;
			if (diff >= 0)
			{
				diff = new Number(diff).toFixed(2);
				diff = parseFloat(diff);
				refSalesRepEditor.setValue(diff);
			}
			else
			{
				refSalesRepEditor.setValue(0);
				
				for (var i=0; i<salesRepEditors.size(); i++)
				{
					var salesRepEditor = salesRepEditors.get(i);
					if (!salesRepEditor.equals(editor))
					{
						diff += salesRepEditor.getValue();
						if (diff >= 0)
						{
							diff = new Number(diff).toFixed(2);
							diff = parseFloat(diff);
							salesRepEditor.setValue(diff);
							return;
						}
						else
						{
							salesRepEditor.setValue(0);
						}
					}
				}
			}
		}
	},
	
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
	},
	
	clockInOut : function()
	{
	},
	
	showPanel : function()
	{
		this.saveState();
		this.splitPanelManager.show();
	},
	
	hidePanel : function()
	{
		this.splitPanelManager.hide();
	},
	
	getRefSalesRep : function()
	{
		return this.refSalesRep;
	},
	
	getSplitSalesReps : function()
	{
		return this.splitSalesReps;
	},
	
	addSplitPanelClickOkListener : function(listener)
	{
		this.splitPanelManager.addSplitPanelClickOkListener(listener);
	},
	
	addSplitPanelClickCancelListener : function(listener)
	{
		this.splitPanelManager.addSplitPanelClickCancelListener(listener);
	},
	
	getPanel : function()
	{
		return this.splitPanelManager.getPanel();
	}
});

var SplitOrderManager = Class.create(SplitManager, {
	
	initialize : function($super, splitSalesReps)
	{
		$super(splitSalesReps);
		
		this.initSalesRepAmounts();
	},
	
	initSalesRepAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		if (refSalesRepEditor != null)
			refSalesRepEditor.setValue(OrderAmount.getSubTotal());
				
		if (salesRepEditors != null)
		{
			for (var i=0; i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				salesRepEditor.setValue(0);
			}
		}
	},
	
	updateAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var refAmt = refSalesRepEditor.getValue();
		var sum = refAmt;
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			sum += salesRepEditor.getValue();
		}
		
		var diff = OrderAmount.getSubTotal() - sum;
		diff += refAmt;
		diff = new Number(diff).toFixed(2);
		diff = parseFloat(diff);
		
		if (diff >= 0)
		{
			refSalesRepEditor.setValue(diff);
		}
		else
		{
			refSalesRepEditor.setValue(0);
			for (var i=0; i< salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);				
				diff += salesRepEditor.getValue();
				diff = new Number(diff).toFixed(2);
				diff = parseFloat(diff);
				
				if (diff >= 0)
				{
					salesRepEditor.setValue(diff);
					this.updateInfo();
					return;
				}
				else
				{
					salesRepEditor.setValue(0);
				}
			}
		}
		this.updateInfo();
	},
	
	updateInfo : function()
	{
		this.updateSalesRepInfo();
		this.updateInfoTotal();
	},
			
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
		this.updateSalesRepInfo();
	},
	
	clockInOut : function()
	{
	}
});

var SplitReturnManager = Class.create(SplitManager, {
	
	initialize : function($super, salesReps)
	{
		$super(salesReps);
		
		this.salesRepRatios = null;
		this.initRatios();
	},
	
	initRatios : function()
	{
		this.salesRepRatios = new Hash();
		
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var totalAmt = refSalesRepEditor.getValue();
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			totalAmt += salesRepEditor.getValue();
		}
		
		var refRatio = new Number(refSalesRepEditor.getValue()/totalAmt);
		this.salesRepRatios.set(refSalesRepEditor.getId(), refRatio);
		
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);
			var value = salesRepEditor.getValue();
			
			var ratio = new Number(value/totalAmt);
			this.salesRepRatios.set(salesRepEditor.getId(), ratio);
		}
	},
	
	initSalesRepAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		if (refSalesRepEditor != null)
			refSalesRepEditor.setValue(OrderAmount.getSubTotal());
		if (salesRepEditors != null)
		{
			for (var i=0; i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				salesRepEditor.setValue(0);
			}
		}
	},
	
	updateAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var total = OrderAmount.getSubTotal();
		var sum = 0;
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			var ratio = this.salesRepRatios.get(salesRepEditor.getId());
			var value = new Number(ratio * total).toFixed(2);
			sum += parseFloat(value);
			salesRepEditor.setValue(value);
		}
		
		var refUpdatedValue = new Number(total - sum).toFixed(2);
		refSalesRepEditor.setValue(refUpdatedValue);
		
		this.updateSalesRepInfo();
		this.updateInfoTotal();
	},
	
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
		
	},
	
	clockInOut : function()
	{
		
	}
});

var SplitExchangeManager = Class.create(SplitManager, {
	
	initialize : function($super, salesReps)
	{
		$super(salesReps);
		this.currentUserEditor = null;
		this.salesRepRatios = null;
		this.initRatios();
	},
	
	initRatios : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		this.salesRepRatios = new Hash();
		
		var totalAmt = refSalesRepEditor.getValue();
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			totalAmt += salesRepEditor.getValue();
		}
		
		var refRatio = new Number(refSalesRepEditor.getValue()/totalAmt);
		this.salesRepRatios.set(refSalesRepEditor.getId(), refRatio);
		
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);
			var value = salesRepEditor.getValue();
			
			var ratio = new Number(value/totalAmt);
			this.salesRepRatios.set(salesRepEditor.getId(), ratio);
		}
	},
	
	initSalesRepAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		if (refSalesRepEditor != null)
			refSalesRepEditor.setValue(OrderAmount.getSubTotal());
		if (salesRepEditors != null)
		{
			for (var i=0; i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				salesRepEditor.setValue(0);
			}
		}
	},
	
	setCurrentUser : function(salesRep)
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var splitSalesRep = new SplitSalesRep(salesRep);
		var currentUserEditor = new SalesRepEditor(splitSalesRep);
		
		if (currentUserEditor.equals(refSalesRepEditor))
		{
			this.currentUserEditor = refSalesRepEditor;
		}
		else
		{
			for (var i=0 ;i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				if (salesRepEditor.equals(currentUserEditor))
				{
					this.currentUserEditor = salesRepEditor;
				}
			}
		}
	},
	
	updateAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var total = OrderAmount.getSubTotal();
		if (total > 0)
		{
			var sum = 0;
			for (var i=0; i< salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);	
				var value = salesRepEditor.getValue();
				if (value < 0)
				{
					salesRepEditor.setValue(0);
				}
				sum += salesRepEditor.getValue();
			}
			var refValue = refSalesRepEditor.getValue();
			if (refValue < 0)
			{
				refSalesRepEditor.setValue(0);
			}
			
			var updatedValue = OrderAmount.getSubTotal() - sum;
			updatedValue = new Number(updatedValue).toFixed(2);
			this.currentUserEditor.setValue(updatedValue);
		}
		else
		{
			var sum = 0;
			for (var i=0; i< salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);				
				var ratio = this.salesRepRatios.get(salesRepEditor.getId());
				var value = new Number(ratio * total).toFixed(2);
				sum += parseFloat(value);
				salesRepEditor.setValue(value);
			}
			
			var updatedValue = new Number(total - sum).toFixed(2);
			refSalesRepEditor.setValue(updatedValue);
		}		
		this.updateSalesRepInfo();
		this.updateInfoTotal();
	},
	
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
	},
	
	clockInOut : function()
	{
	}
});

var SalesRepInfoUpdater = Class.create({
		
		initialize : function(refSalesRep, splitSalesReps)
		{
			this.refSalesRep = refSalesRep;
			this.splitSalesReps = splitSalesReps;
			this.currencySymbol = OrderAmount.getCurrencySymbol();
		},
		
		splitPanelClickOk : function()
		{
			this.update();
		},
		
		update : function()
		{
			var salesRepAmount = $('salesRepAmount'+this.refSalesRep.getId());
			if (salesRepAmount != null)
				salesRepAmount.innerHTML = new Number(this.refSalesRep.getAmount()).toFixed(2);
			
			for (var i=0; i<this.splitSalesReps.size(); i++)
			{
				var splitSalesRep = this.splitSalesReps.get(i);
				salesRepAmount = $('salesRepAmount' + splitSalesRep.getId());
				if (salesRepAmount != null)
					salesRepAmount.innerHTML = this.currencySymbol + ' ' + new Number(splitSalesRep.getAmount()).toFixed(2);
			}
		}
});

var SplitOrderDetailsManager = Class.create({
	
	initialize : function(type, orderSplitSalesReps)
	{
		this.splitManager = null;
		
		if (type == 'sales')	
		{
			var clockedInSalesReps = ClockedInSalesRepManager.getSalesReps();
			var splitSalesReps = new ArrayList(new SplitSalesRep());
			
			for (var i=0; i<clockedInSalesReps.size(); i++)
			{
				var salesRep = clockedInSalesReps.get(i);
				var splitSalesRep = new SplitSalesRep(salesRep);
				splitSalesReps.add(splitSalesRep);
			}
			
			this.splitManager = new SplitOrderManager(splitSalesReps);
		}
		else if (type == 'return')
		{
			var clockedInSalesReps = ClockedInSalesRepManager.getSalesReps();
			var previousSalesRep = ClockedInSalesRepManager.getCurrentSalesRep();
			var currentSalesRep = previousSalesRep;
			var completeSalesReps = clockedInSalesReps.clone();
			
			var splitSalesReps = new ArrayList(new SplitSalesRep());
			for (var i=0; i<orderSplitSalesReps.size(); i++)
			{
				var orderSplitSalesRep = orderSplitSalesReps[i];
				var name = orderSplitSalesRep.name;
				var salesRepId = orderSplitSalesRep.salesRepId;
				var amount = orderSplitSalesRep.amount;
				var current = orderSplitSalesRep.isCurrent;
				
				var salesRep = new SalesRep(name, salesRepId);
				salesRep.setCurrent(current);
				
				if (salesRep.isCurrent())
				{
					currentSalesRep = salesRep;
				}
				
				if (!completeSalesReps.contains(salesRep))
				{
					completeSalesReps.add(salesRep);
				}
				
				var splitSalesRep = new SplitSalesRep(salesRep);
				splitSalesRep.setAmount(amount);
				splitSalesReps.add(splitSalesRep);
			}
			ClockInOut.render(completeSalesReps, currentSalesRep, previousSalesRep);				
			this.splitManager = new SplitReturnManager(splitSalesReps);
		}
		else if (type == 'exchange')
		{
			var clockedInSalesReps = ClockedInSalesRepManager.getSalesReps();
			var previousSalesRep = ClockedInSalesRepManager.getCurrentSalesRep();
			var currentSalesRep = previousSalesRep;
			var completeSalesReps = clockedInSalesReps.clone();
			
			var splitSalesReps = new ArrayList(new SplitSalesRep());
			for (var i=0; i<orderSplitSalesReps.size(); i++)
			{
				var orderSplitSalesRep = orderSplitSalesReps[i];
				var name = orderSplitSalesRep.name;
				var salesRepId = orderSplitSalesRep.salesRepId;
				var amount = orderSplitSalesRep.amount;
				var current = orderSplitSalesRep.isCurrent;
				
				var salesRep = new SalesRep(name, salesRepId);
				salesRep.setCurrent(current);
				
				if (salesRep.isCurrent())
				{
					currentSalesRep = salesRep;
				}
				
				if (!completeSalesReps.contains(salesRep))
				{
					completeSalesReps.add(salesRep);
				}
				
				var splitSalesRep = new SplitSalesRep(salesRep);
				splitSalesRep.setAmount(amount);
				splitSalesReps.add(splitSalesRep);
			}
			
			var isOrderSplit = (orderSplitSalesReps.size() > 0);
			for (var i=0; i<clockedInSalesReps.size(); i++)
			{
				var clockedInSalesRep = clockedInSalesReps.get(i);
				var splitSalesRep = new SplitSalesRep(clockedInSalesRep);
				splitSalesRep.setAmount(0);
				if (isOrderSplit)
				{
					splitSalesRep.setCurrent(false);
				}
				if (!splitSalesReps.contains(splitSalesRep))
				{
					splitSalesReps.add(splitSalesRep);
				}
			}
			
			ClockInOut.render(completeSalesReps, currentSalesRep, previousSalesRep);				
			this.splitManager = new SplitExchangeManager(splitSalesReps);
			this.splitManager.setCurrentUser(previousSalesRep);
		}
	},
	
	getSplitManager : function()
	{
		return this.splitManager;
	}
});

var OrderAmount = {
		
		isDraftedOrder : true,
		
		getSubTotal : function()
		{
			if(typeof ORDER != "undefined"){
				return parseFloat(ORDER.subTotal);
			}
			 
			if(typeof shoppingCart != "undefined"){
				return parseFloat(shoppingCart.subTotal.toFixed(2));
			}
		},
		
		getCurrencySymbol : function()
		{
			return TerminalManager.terminal.currency;
		}

}
