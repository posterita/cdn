var TaxManager = {
		initialised : false,
		
		init : function(){
			if(this.initialised) return;
			
			this.loadTaxes();			
			this.initialised = true;
		},
		
		loadTaxes : function(){
			console.log('[TaxManager] Loading taxes');
			jQuery.db.values('tax', null, 10000).done(function(records){
				TaxManager.db = TAFFY(records);
				console.log('[TaxManager] Found ' + records.length + ' taxes.');
				
				TaxManager.onReady();
		     });
		},
		
		getTaxById : function(taxId){
			this.init();
			taxId = parseInt(taxId);
			var query = {"taxId" : taxId};			
			var results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			return null;
		},
		
		getTaxByTaxCategoryId : function(taxCategoryId, isSoTrx){
			var tax = null;	
			
			if(!this.taxCategoryCache){
				this.taxCategoryCache = new Hash();
			}
			
			var cacheKey = taxCategoryId + '_' + isSoTrx;
			
			tax = this.taxCategoryCache.get(cacheKey);
			if(tax != null) return tax;
			
			tax = this.searchTaxByTaxCategoryId(taxCategoryId, isSoTrx);
			
			if(tax != null){
				this.taxCategoryCache.set(cacheKey, tax)
			}
			
			return tax;			
		},
		
		searchTaxByTaxCategoryId : function(taxCategoryId, isSoTrx){
			this.init();
			/* see ShoppingCartLine.java line 177 setTax() */
			var orgId = TerminalManager.terminal.orgId;
			
			var sopotype = 'P';
			
			if(isSoTrx){
				sopotype = 'S';
			}
			
			var query = null;
			var results = null;
			
			/*1*/
			query = {};
			query.taxCategoryId = taxCategoryId;
			query.orgId = orgId;
			query.isDefault = true;
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			/*2*/
			query = {};
			query.taxCategoryId = taxCategoryId;
			query.orgId = 0;
			query.isDefault = true;
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			/*3*/
			query = {};
			query.taxCategoryId = taxCategoryId;
			query.orgId = orgId;
			query.isDefault = false;
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			
			
			/*4*/
			query = {};
			query.taxCategoryId = taxCategoryId;
			query.orgId = 0;
			query.isDefault = false;
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			/*5*/
			query = {};
			query.orgId = orgId;
			query.isDefault = true;
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			/*6*/
			query = {};
			query.orgId = orgId;
			query.isDefault = false;
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			/*7*/
			query = {};
			query.isDefault = true;
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			/*8*/
			query = {};
			query.sopotype = ['B',sopotype];
			
			results = this.db(query).get();
			
			if(results.length != 0){
				var tax = Object.clone(results[0]);
				return tax;
			}
			
			return null;			
		},
		
		onReady : function(){}
};