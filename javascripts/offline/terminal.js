var TerminalManager = {
		initialised : false,
		
		init : function(){
			if(this.initialised) return;			
			this.loadTerminals();			
			this.initialised = true;
		},
		
		loadTerminals : function(){	
			console.log('[TerminalManager] Loading terminals.');
			 jQuery.db.values('terminal').done(function(records){
				 TerminalManager.db = TAFFY(records);
				 console.log('[TerminalManager] Found ' + records.length + ' terminals.');
		     });			
		},
		
		getTerminalById : function(terminalId){
			this.init();
			var query = {"terminalId" : terminalId};			
			var results = this.db(query).get();
			
			if(results.length != 0){
				var Terminal = Object.clone(results[0]);
				return Terminal;
			}
			
			return null;
		},
		
		searchTerminals : function(query){
			this.init();
			var results = this.db(query).get();
			return results;
		}
};