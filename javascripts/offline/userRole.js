var UserManager = {
		initialised : false,
		
		init : function(){
			if(this.initialised) return;
			
			this.loadUserAndRoles();			
			this.initialised = true;
		},
		
		loadUserAndRoles : function(){
			console.log('[UserManager] Loading users');
			jQuery.db.values('user', null, 10000).done(function(records){
				UserManager.user_db = TAFFY(records);
				console.log('[UserManager] Found ' + records.length + ' users.');
				
				console.log('[UserManager] Loading roles');
				jQuery.db.values('role', null, 10000).done(function(records){
					UserManager.role_db = TAFFY(records);
					console.log('[UserManager] Found ' + records.length + ' roles.');
					
					console.log('[UserManager] Loading org accesses');
					jQuery.db.values('roleOrgAccess', null, 10000).done(function(records){
						UserManager.role_org_access_db = TAFFY(records);
						console.log('[UserManager] Found ' + records.length + ' accesses.');
						
						UserManager.onReady();
						
				     });
			     });
		     });			
						
		},
		
		getUserById : function(userId){
			this.init();
			var query = {"ad_user_id" : userId};			
			var results = this.user_db(query).get();
			
			if(results.length != 0){
				var user = Object.clone(results[0]);
				return user;
			}
			
			return null;
		},
		
		searchUsers : function(query){
			this.init();
			var results = this.user_db(query).get();
			return results;
		},
		
		getRoleById : function(roleId){
			this.init();
			var query = {"ad_role_id" : roleId};			
			var results = this.role_db(query).get();
			
			if(results.length != 0){
				var role = Object.clone(results[0]);
				return role;
			}
			
			return null;
		},
		
		searchRoles : function(query){
			this.init();
			var results = this.role_db(query).get();
			return results;
		},
		
		getAccessibleOrgs : function(roleId){
			this.init();
			var query = {"ad_role_id" : roleId};
			var results = this.role_org_access_db(query).get();
			
			var accessibleOrgs = [];
			for(var i=0; i<results.length; i++){
				accessibleOrgs[i] = results[i].ad_org_id;
			}
			
			return accessibleOrgs;
		},
		
		getUser : function(username, password){
			var encryptedPassword = CryptoJS.SHA1(password);
			var query = {};
			query["name"] = username;
			query["password"] = encryptedPassword + '';
			
			var results = this.searchUsers(query);
			if(results == null || results.length == 0){
				return null;
			}
			
			return results[0];
		},
		
		onReady : function(){}
};