(function () {
	
	/* check whether offline is enabled */
	if(!OfflineManager.isOfflineEnabled()) return;
	
	var networkProblemDialog = null;
	
	new APP.UTILS.NetworkEventListener().connected(function(){
		if(networkProblemDialog){
			networkProblemDialog.dialog("close");
		}
		
	}).disconnected(function(){
		networkProblemDialog = jQuery("<div title='No network'><p>Unable to reach Posterita. Please check your internet connection.</p></div>").dialog({
				resizable: false,
				modal: true,
				autoOpen : true,
				draggable : false,
				position : {my: "top", at:"top+200"},
				buttons: {				
					"Continue offline": function() {
						jQuery( this ).dialog( "close" );
						APP.switchOffline();
					}
				}
		});
	});
	
	var serverUnReachableDialog = null;
	
	
	new APP.UTILS.ServerMonitor().reachable(function(){
		if(serverUnReachableDialog){
			serverUnReachableDialog.dialog("close");
		}
	 }).unreachable(function(){
		 serverUnReachableDialog = jQuery("<div title='Server Down'><p>Posterita server is unreachable.</p></div>").dialog({
				resizable: false,
				modal: true,
				autoOpen : true,
				draggable : false,
				position : {my: "top", at:"top+200"},
				buttons: {				
					"Continue offline": function() {
						jQuery( this ).dialog( "close" );
						APP.switchOffline();
					}
				}
		});
	 });
	
}());


/*
new APP.UTILS.NetworkEventListener().connected(function(){
	console.log('connected :)');
}).disconnected(function(){
	console.log('disconnected :(');
});



*/