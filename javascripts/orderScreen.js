var OrderScreen = {					
		setTenderType : function(tenderType){
			this.tenderType = tenderType;
		},
		
		setDeliveryRule : function(deliveryRule){
			this.deliveryRule = deliveryRule;
		},
		
		setPaymentTermId : function(paymentTermId){
			this.paymentTermId = paymentTermId;
		},
		
		getPaymentTermId : function(){
			return this.paymentTermId;
		},
		
		setPaymentRule : function(paymentRule){
			
			switch (paymentRule) {
			
			case PAYMENT_RULE.CASH:	
				this.setTenderType(TENDER_TYPE.CASH);
				break;
			
			case PAYMENT_RULE.CARD:	
				this.setTenderType(TENDER_TYPE.CARD);
				break;
				
			case PAYMENT_RULE.CHEQUE:	
				this.setTenderType(TENDER_TYPE.CHEQUE);
				break;
				
			case PAYMENT_RULE.MIXED:	
				this.setTenderType(TENDER_TYPE.MIXED);
				break;
				
			case PAYMENT_RULE.CREDIT:	
				this.setTenderType(TENDER_TYPE.CREDIT);
				break;
			
			case PAYMENT_RULE.VOUCHER:
				this.setTenderType(TENDER_TYPE.VOUCHER);
				break;
				
			case PAYMENT_RULE.EXTERNAL_CARD:
				this.setTenderType(TENDER_TYPE.EXTERNAL_CARD);
				break;
				
			case PAYMENT_RULE.GIFT_CARD:
				this.setTenderType(TENDER_TYPE.GIFT_CARD);
				break;
				
			case PAYMENT_RULE.LOYALTY:
				this.setTenderType(TENDER_TYPE.LOYALTY);
				break;
				
			case PAYMENT_RULE.SK_WALLET:
				this.setTenderType(TENDER_TYPE.SK_WALLET);
				break;
				
			case PAYMENT_RULE.ZAPPER:
				this.setTenderType(TENDER_TYPE.ZAPPER);
				break;
									
			case PAYMENT_RULE.MCB_JUICE:
				this.setTenderType(TENDER_TYPE.MCB_JUICE);
				break;
			
			case PAYMENT_RULE.MY_T_MONEY:
				this.setTenderType(TENDER_TYPE.MY_T_MONEY);
				break;
				
			case PAYMENT_RULE.EMTEL_MONEY:
				this.setTenderType(TENDER_TYPE.EMTEL_MONEY);
				break;
				
			case PAYMENT_RULE.GIFTS_MU:
				this.setTenderType(TENDER_TYPE.GIFTS_MU);
				break;
				
			case PAYMENT_RULE.MIPS:
				this.setTenderType(TENDER_TYPE.MIPS);
				break;
				
			default:
				break;
			}
		},
		
		setPayment : function(payment){
			this.payment = payment;			
			this.checkout();
		},
		
		setIsPaymentOnline : function(isOnline){
			this.isPaymentOnline = isOnline;
		},
		
		getPaymentDetails : function(){
			
			/*validate bp*/
			if(BPManager.getBP() == null){
				alert(((OrderScreen.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');
				return;
			}
			
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			
			if(!ShoppingCartManager.isShoppingCartReady()){
				alert(Translation.translate('cart.is.not.ready','Cart is not ready!'));
				return;
			}
			
			var isRefund = (this.getCartTotal() < 0.0) || (this.orderType == ORDER_TYPES.CUSTOMER_RETURNED_ORDER) || (this.orderType == ORDER_TYPES.POS_GOODS_RETURNED_NOTE);
						
			switch (this.tenderType) {			
				
				case TENDER_TYPE.CASH:	
					if(isRefund){
						var payment = new Hash();
					  	payment.set('amountTendered', this.getCartTotal());
					  	payment.set('amountRefunded', 0.0);  	
					  	this.payment = payment;
					  	this.checkout();
						break;
					}
					
					var panel = new CashPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.MCB_JUICE:
				case TENDER_TYPE.MY_T_MONEY:
				case TENDER_TYPE.EMTEL_MONEY:
				case TENDER_TYPE.MIPS:
				case TENDER_TYPE.GIFTS_MU:
				case TENDER_TYPE.EXTERNAL_CARD:
					/* manual creditcard processing */	
					
					if((OrderScreen.skipExternalCardPopUp && OrderScreen.skipExternalCardPopUp == true) 
							|| (countryId != '100' && countryId != '109'))
					{
						/*this.setExternalCardPayment(); //this method is called on checkout. See OrderScreen->checkout*/
						this.checkout();
						break;
					}
					
					if ((countryId == '100' || countryId == '109') && OrderScreen.paymentProcessor == '')
					{
						panel = new ExtCreditCardPanel();
						panel.paymentHandler = this.paymentHandler.bind(this);
						panel.show();
						break;
					}
					
					this.checkout();
					break;
				
				case TENDER_TYPE.CARD:					
					var panel = null;
					
					if(OrderScreen.paymentProcessor == '' && (countryId == '100' || countryId == '109')){
						//alert("Payment processor not configured!");
						//return;	
						panel = new ApplyPaymentProcessorPanel();
						panel.show();
					}
					
					
					
					if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_XWeb"){
						panel = new XWebPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_HF")
					{						
						if (ORDER_TYPES.CUSTOMER_RETURNED_ORDER == ShoppingCartManager.getOrderType())
						{
							var payment = new Hash();
							payment.set('cardType', 'M');
							this.payment = payment;
							this.checkout();
							
							return;
						}
						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new MercuryPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS")
					{
						if (ORDER_TYPES.CUSTOMER_RETURNED_ORDER == ShoppingCartManager.getOrderType())
						{
							var payment = new Hash();
							payment.set('cardType', 'M');
							this.payment = payment;
							this.checkout();
							
							return;
						}
						
						if(ORDER_TYPES.POS_ORDER == ShoppingCartManager.getOrderType())
						{
							panel = new ElementPSPanel();	
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0)
						{
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_EE")
					{
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						panel = new E2EPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Century")
					{
						panel = new CenturyPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Norse")
					{						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new NorseCardPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_USAEpay")
					{
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new UsaEpayPanel();					
					}
					
					else if((TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Merchant_Warehouse") && (PreferenceManager.getPreference(PreferenceManager.CONSTANTS.ENABLE_GENIUS_PAYMENT) == 'false'))
					{
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new MerchantWarehousePanel();					
					}
					
					else if((TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Merchant_Warehouse") && (PreferenceManager.getPreference(PreferenceManager.CONSTANTS.ENABLE_GENIUS_PAYMENT) == 'true'))
					{
						panel = new MerchantWarehouseGeniusPanel();
					}
					
					else{
						panel = new CardPanel();
					}
					
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
				case TENDER_TYPE.CHEQUE:
					var panel = new ChequePanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
				case TENDER_TYPE.MIXED:	
					if(isRefund){
						alert(Translation.translate("mix.tender.type.is.not.supported.for.refunds","Mix tender type is not supported for refunds"));
						return;
					}
					
					/*
					var panel = new DeliveryPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					*/
					
					var preference = TerminalManager.terminal.preference;
					var showDeliveryOption = preference.showDeliveryOption;
					
					if( showDeliveryOption ){
						
						var panel = new DeliveryPanel();
						panel.paymentHandler = this.paymentHandler.bind(this);
						panel.show();
					}
					else
					{
						var payment = new Hash();
						payment.set('deliveryRule','O');
						payment.set('deliveryDate', null);
						payment.set('paymentTermId', 0);	
					  	this.payment = payment;
					  	this.checkout();
					}					
					
					break;
					
				case TENDER_TYPE.CREDIT:
					//validate BP
					var bp = BPManager.getBP();
					var amt = ShoppingCartManager.getGrandTotal();
					
					var isValid = false;
					
					if(this.isSoTrx == true){
						isValid = BPManager.validateCreditStatus(bp, amt);
					}
					else{
						/*don't check credit status for vendor*/
						isValid = true;
					}
					
					if(!isValid){
						return;
					}
					
					/*var paymentTermPanel = new PaymentTermPanel();
					paymentTermPanel.setPaymentTermId = this.setPaymentTermId.bind(this);
					paymentTermPanel.show();*/
					var panel = new DeliveryPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();	
					break;
					
				case TENDER_TYPE.VOUCHER:	
					if(isRefund){
						var payment = new Hash();
					  	payment.set('voucherAmt', this.getCartTotal());
					  	payment.set('voucherNo', null);
					  	payment.set('voucherOrgId', TerminalManager.terminal.orgId);					  	
					  	payment.set('amountRefunded', 0.0);  	
					  	this.payment = payment;
					  	this.checkout();
						break;
					}
					var panel = new VoucherPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.GIFT_CARD:
					
					var screen = this;
					
					function showGiftCardPanel()
					{
						var panel = new GiftCardPanel();
						panel.paymentHandler = screen.paymentHandler.bind(screen);
						panel.show();
					}
					
					var terminal = TerminalManager.terminal;
					var paymentProcessor = terminal.paymentProcessor;
					
					if(paymentProcessor != null 
							&& paymentProcessor.indexOf('Mercury') > -1
							&& terminal.preference.enableGift == true){
						
						OrderScreen.setIsPaymentOnline(true);
		    			showGiftCardPanel();
					}
					
					/*function showGiftCardPanel()
					{
						var panel = new GiftCardPanel();
						panel.paymentHandler = screen.paymentHandler.bind(screen);
						panel.show();
					}
					
					var terminal = TerminalManager.terminal;
					var paymentProcessor = terminal.paymentProcessor;
					
					if(paymentProcessor != null 
							&& paymentProcessor.indexOf('Mercury') > -1
							&& terminal.preference.enableGift == true){
						
						var dlg = jQuery("<div/>",{title:Translation.translate("choose.gift.card.processor","Choose Gift Card Processor"), "class":"gift-card-popup-transactions"});
						var configuredBtn = jQuery("<input>",{type:"button", value:Translation.translate("mercury.gift","Mercury Gift"), "class":""});
			    		var posteritaBtn = jQuery("<input>",{type:"button", value:Translation.translate("posterita.gift","Posterita Gift"), "class":""});
			    		
			    		configuredBtn.on("click", function(){
			    			jQuery(dlg).dialog("close");
			    			console.info('Configured :' + paymentProcessor);
			    			
			    			OrderScreen.setIsPaymentOnline(true);
			    			showGiftCardPanel();
			    		});
			    		
			    		posteritaBtn.on("click", function(){
			    			jQuery(dlg).dialog("close");
			    			console.info('Posterita Gift card processor');
			    			
			    			OrderScreen.setIsPaymentOnline(false);
			    			showGiftCardPanel();
			    		});
			    		
			    		var row = jQuery("<div/>", {html:Translation.translate("more.than.one.gift.card.processor.is.configured.please.choose.one","More than one gift card processor is configured! Please choose one")});
			    		dlg.append(row);		    		
			    		
			    		row = jQuery("<div/>", {html:"&nbsp;"});
			    		dlg.append(row);
			    		
			    		row = jQuery("<div/>", {"class":"row"});
			    		row.append(configuredBtn);
			    		dlg.append(row);
			    		
			    		row = jQuery("<div/>", {"class":"row"});
			    		row.append(posteritaBtn);
			    		dlg.append(row);
			    		
			    		jQuery(dlg).dialog({modal:true, width:460});
					}
					else
					{
						showGiftCardPanel();
					}*/
					
					/*
					var panel = new GiftCardPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					*/
									
					break;
					
				
				case TENDER_TYPE.SK_WALLET:
					SKWallet.showCheckInCustomersDialog();				
					break;
					
				case TENDER_TYPE.ZAPPER:
					
					if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
						alert(Translation.translate("zapper only supports sales transactions","zapper.only.supports.sales.transactions."));
						return;						
					}
					
					/*sales transactions can be negative*/
					if(ShoppingCartManager.getGrandTotal() < 0){
						alert(Translation.translate("zapper.does.not.support.negative.sales.transactions","Zapper does not support negative sales transactions."));
						return;	
					}
					
					Zapper.showQRCodeDialog();				
					break;
					
				default:
					break;
				
				case TENDER_TYPE.LOYALTY:
					
					var panel = new LoyaltyPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
			}
		},
		
		paymentHandler:function(payment){
			this.payment = payment;			
			this.checkout();
		},
		
		setDateOrdered:function(){
			
			if(this.dateOrdered){
				$("dateOrdered").value = this.dateOrdered;
			}
			else{
				var today = new Date();
					
				var year = today.getFullYear();
				var month = today.getMonth() + 1;					
				var date = today.getDate();
				var hours = today.getHours();
				var minutes = today.getMinutes();
				var seconds = today.getSeconds();
				
				if(month < 10) month = '0' + month;
				if(date < 10) date = '0' + date;
				if(hours < 10) hours = '0' + hours;
				if(minutes < 10) minutes = '0' + minutes;
				if(seconds < 10) seconds = '0' + seconds;
				
				var dateOrdered = year + "-" + month + "-" + date +
					" " + hours + ":" + minutes + ":" + seconds;
				
				$("dateOrdered").value = dateOrdered;
			}
		},
		
		checkout : function(){
			/* signature capture */	
			var captureSignaturePreference = TerminalManager.terminal.preference.captureSignature;
			var paymentRule = this.getPaymentRuleForTenderType(this.tenderType);
			
			if(captureSignaturePreference == null || captureSignaturePreference.indexOf(paymentRule) < 0){
				OrderScreen.postData();
				return;
			}
			
			if(!OrderScreen.isSoTrx){
				OrderScreen.postData();
				return;
			}
			
			var signaturePopup = jQuery("<div class='signature-popup' title='"+Translation.translate("signature")+"'><canvas id='signature-canvas'></canvas><div class='sign-above'>"+Translation.translate("sign.above")+"</div></div>").dialog({width:"auto", height:"auto", modal: true, autoOpen: true, close: function( event, ui ) {
								
					}, 
					
					dialogClass : 'signature-popup',
					
					buttons: [{
				        id:"signature-popup-btn-skip",
				        text: Translation.translate("skip"),
				        click: function() {
				        	if(confirm(Translation.translate('you.want.to.skip.signature.capture','Do you want to skip signature capture?')))
							{
								signaturePopup.dialog( "close" );				        	
					        	OrderScreen.postData();
							}else
							{
								
							}	
				        }
				    },
				    {
				        id:"signature-popup-btn-clear",
				        text:Translation.translate("clear","Clear"),
				        click: function() {
				        	signaturePad.clear();
				        }
				    }, 
				    {
				        id:"signature-popup-btn-save",
				        text:Translation.translate("save","Save"),
				        click: function() {
				        	if(signaturePad.isEmpty()){
				        		alert(Translation.translate('signature.is.required','Signature is required!'));
				        		return;
				        	}
				        	
				        	var dataURL = signaturePad.toDataURL();
				        	jQuery('#signature').val(dataURL);
				        	
				        	signaturePopup.dialog( "close" );
				        	
				        	OrderScreen.postData();
				        }
				    }]					
			});
			
			var canvas = document.querySelector("canvas");
			var ratio =  window.devicePixelRatio || 1;
		    canvas.width = canvas.offsetWidth * ratio;
		    canvas.height = canvas.offsetHeight * ratio;
		    canvas.getContext("2d").scale(ratio, ratio);
		    
			var signaturePad = new SignaturePad(canvas);
		},
		
		postData : function(){
			/* do normal submit */ 
			/* TODO: check network*/			
			
			//1. reset form
			$("orderId").value = "0";
			$("bpartnerId").value = "0";
			$("prepareOrder").value="false";
			$("orderType").value="POS Order";
			$("tenderType").value="Cash";	
			$("isPaymentOnline").value="true";
			
			$("cashAmt").value="0";
			$("cardAmt").value="0";
			$("chequeAmt").value="0";
			$("externalCardAmt").value="0";
			
			$("mcbJuiceAmt").value="0";
			$("mytMoneyAmt").value="0";
			$("emtelMoneyAmt").value="0";
			$("mipsAmt").value="0";
			$("giftsMuAmt").value="0";
			
			
			$("voucherAmt").value="0";
			$("voucherOrgId").value="";
			$("voucherNo").value="";
			
			$("amountTendered").value="0";
			$("amountRefunded").value="0";
			
			$("chequeNo").value="";
			
			$("cardTrackData").value="";
			$("cardTrackDataEncrypted").value="";
			$("cardType").value="";
			$("cardholderName").value="";
			$("cardExpDate").value="";
			$("cardCVV").value="";
			$("cardStreetAddress").value="";
			$("cardZipCode").value="";
			$("cardNo").value="";
			
			$("documentNo").value = "";
			$("referenceNo").value = "";
			$("dateOrdered").value = "";
			
			$("giftCardAmt").value = "0";
			$("giftCardTrackData").value = "";
			$("giftCardNo").value = "";
			$("giftCardCVV").value = "";
			
			//$("skwalletAmt").value = "0";
			//$("zapperAmt").value = "0";
			
			//2. populate form
			$("orderType").value = this.orderType;
			$("deliveryRule").value = this.deliveryRule;
			$("paymentTermId").value = 0;
			
			var bp = BPManager.getBP();
			if(bp == null)
			{
				alert(((this.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');				
				
				return;
			}
			
			$("bpartnerId").value = bp.getId();
			$('orderId').value = ShoppingCartManager.getOrderId();
			
			if (OrderScreen.splitManager != null)
			{
				var splitSalesReps = OrderScreen.splitManager.getSplitSalesReps()
				var refSalesRep = OrderScreen.splitManager.getRefSalesRep();
				
				if (splitSalesReps != null)
				{
					if (refSalesRep != null)
						splitSalesReps.add(refSalesRep);
					$('splitSalesReps').value =  splitSalesReps.toJSON();
				}
				else
					$('splitSalesReps').value = "";
			}
			
			if(this.documentNumber) $("documentNo").value = this.documentNumber;			
			if(this.referenceNumber) $("referenceNo").value = this.referenceNumber;
			
			//set date ordered
			this.setDateOrdered();
			
			switch (this.tenderType) {
				case TENDER_TYPE.CASH :	
					this.setCashPayment();			
					break;
					
				case TENDER_TYPE.CHEQUE :
					this.setChequePayment();
					break;
					
				case TENDER_TYPE.CARD :
					this.setCardPayment();			  
					break;
					
				case TENDER_TYPE.EXTERNAL_CARD :
					this.setExternalCardPayment();			  
					break;
					
				case TENDER_TYPE.MCB_JUICE :
					this.setMCBJuicePayment();			  
					break;
				
				case TENDER_TYPE.MY_T_MONEY :
					this.setMyTMoneyPayment();			  
					break;
					
				case TENDER_TYPE.EMTEL_MONEY :
					this.setEmtelMoneyPayment();			  
					break;
				
				case TENDER_TYPE.MIPS :
					this.setMipsPayment();			  
					break;
					
				case TENDER_TYPE.GIFTS_MU :
					this.setGiftsMuPayment();			  
					break;
					
				case TENDER_TYPE.MIXED :
					this.setMixPayment();					
					break;
					
				case TENDER_TYPE.CREDIT :
					this.setOnCreditPayment();					
					break;
					
				case TENDER_TYPE.VOUCHER :
					this.setVoucherPayment();					
					break;
					
				case TENDER_TYPE.GIFT_CARD :
					this.setGiftCardPayment();					
					break;
					
				case TENDER_TYPE.SK_WALLET :
					this.setSKWalletPayment();					
					break;
					
				case TENDER_TYPE.ZAPPER :
					this.setZapperPayment();
					break;
					
				case TENDER_TYPE.LOYALTY :
					this.setLoyaltyPayment();					
					break;

				default:
					break;
			}				
			
			$('tenderType').value = this.tenderType;			
			
			$('order-form').submit();	
			this.processDialog.show();
		},
		
		setCashPayment : function(){
			$('amountTendered').value = this.payment.get('amountTendered');
			$('amountRefunded').value = this.payment.get('amountRefunded');	
			$('cashAmt').value = this.getCartTotal();
		},
		
		setCardPayment : function(){			
			$('cardAmt').value = this.getCartTotal();
			
			if(!this.isPaymentOnline)
			{
				$('isPaymentOnline').value = this.isPaymentOnline;
				return;
			}
			
			if(this.payment.get('cardTrackDataEncrypted'))
			{
				$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
				return;
			}			
			
			var isCardPresent = this.payment.get('isCardPresent');
			
			if(isCardPresent == true)
			{
				$('cardTrackData').value = this.payment.get('trackData');
			}
			else
			{
				$('cardTrackData').value = "";
				$('cardTrackDataEncrypted').value = "";
				$('cardNo').value = this.payment.get('cardNumber');
				$('cardExpDate').value = this.payment.get('cardExpiryDate');
				$('cardCVV').value = this.payment.get('cardCvv');
				$('cardholderName').value = this.payment.get('cardHolderName');
				$('cardStreetAddress').value = this.payment.get('streetAddress');
				$('cardZipCode').value = this.payment.get('zipCode');
			}			
		},
		
		setChequePayment : function(){
			$('chequeAmt').value = this.getCartTotal();
			$('chequeNo').value = this.payment.get('chequeNumber');
		},
		
		setMixPayment : function(){
			$('deliveryRule').value = this.payment.get('deliveryRule');
			$('deliveryDate').value = this.payment.get('deliveryDate');
			$('paymentTermId').value = this.payment.get('paymentTermId');
		},
		
		setOnCreditPayment : function(){
			$('deliveryRule').value = this.payment.get('deliveryRule');
			$('deliveryDate').value = this.payment.get('deliveryDate');
			$('paymentTermId').value = this.payment.get('paymentTermId');
		},
				
		setExternalCardPayment:function(){
			$('externalCardAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},	
		
		setMCBJuicePayment:function(){
			$('mcbJuiceAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setMyTMoneyPayment:function(){
			$('mytMoneyAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setEmtelMoneyPayment:function(){
			$('emtelMoneyAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setMipsPayment:function(){
			$('mipsAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setGiftsMuPayment:function(){
			$('giftsMuAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setVoucherPayment : function(){
			$('voucherAmt').value = this.payment.get('voucherAmt');
			$('voucherNo').value = this.payment.get('voucherNo');
			$('voucherOrgId').value = this.payment.get('voucherOrgId');
		},
		
		setEECardPayment : function(){
			$('cardAmt').value = this.payment.get('cardAmt');
			$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
		},
		
		setOtk : function(otk){
			$('otk').value = otk;
		},
		
		setGiftCardPayment : function(){
			$('giftCardAmt').value = this.getCartTotal();
			$('giftCardTrackData').value = this.payment.get('giftCardTrackData');
			$('giftCardNo').value = this.payment.get('giftCardNo');
			$('giftCardCVV').value = this.payment.get('giftCardCVV');
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setSKWalletPayment : function(){
			$('skwalletAmt').value = this.getCartTotal();
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'true';
		},
		
		setZapperPayment : function(){
			$('zapperAmt').value = this.getCartTotal();
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'true';
		},
		
		setLoyaltyPayment : function(){
			$('loyaltyAmt').value = this.getCartTotal();
		},
				
		saveOrder : function(){
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			
			var bp = BPManager.getBP();
			if(bp == null)
			{
				alert(((this.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');
				
				jQuery("#search-bp-textfield").focus();
				
				return;
			}
			
			$('orderId').value = ShoppingCartManager.getOrderId();
			$("orderType").value = this.orderType;
			$('prepareOrder').value = 'true';
			$('bpartnerId').value = bp.id;
			
			//set date ordered
			this.setDateOrdered();
			
			$('order-form').submit();
			this.processDialog.show();
		},
		
		setBPartnerId : function(bpartnerId){
			if(this.bpartnerId == bpartnerId){
				
				return jQuery.Deferred().resolve();
			}
			
			this.bpartnerId = bpartnerId;			
			$('bpartnerId').value = bpartnerId;			
			return ShoppingCartManager.setBp(bpartnerId);
		},
		
		setOrderId : function(orderId){
			$('orderId').value = orderId;				
		},
		
		initialize: function(){						
						
			/* fields declaration */
			this.tenderType = TENDER_TYPE.CASH;
			this.payment = null;
			this.splitManager = null;
			
			this.initializeDiscount();				
			this.initializeComponents();			
			this.initAutoCompletes();
			ClockedInSalesRepManager.addOnLoadClockedInSalesRepsListener(this);
			
			shortcut.add("Up", function(){
				SmartProductSearch.moveUp();
			});
			
			shortcut.add("Down", function(){
				SmartProductSearch.moveDown();
			});
			
			/* render search product filter 
			SearchProductFilter.initialize();*/
			
			ItemFields.init();
			CustomerFields.init();
			VendorFields.init();
			
			SearchBP.initializeComponents();
			SearchSalesRep.initializeComponents();
			//ClockInOut.init();
		},
		
		clockedInSalesRepsLoaded : function()
		{
			this.initSplitOrder();
		},
		
		initSplitOrder : function()
		{
			var splitOrderDetails = this.splitOrderDetails;
			var type = splitOrderDetails.type;
			var orderSplitSalesReps = eval('('+splitOrderDetails.salesReps+')');
			
			var orderDetailsManager = new SplitOrderDetailsManager(type, orderSplitSalesReps);
			this.splitManager = orderDetailsManager.getSplitManager();
			this.splitManager.updateSalesRepInfo();
			ClockedInSalesRepManager.addCurrentSalesRepChangeEventListener(this);
			ClockedInSalesRepManager.addClockInOutEventListener(this);
		},
		
		currentSalesRepChange : function (salesRepChangeEvent)
		{
			this.splitManager.currentSalesRepChange(salesRepChangeEvent);
		},
		
		clockInOut : function()
		{
			this.splitManager.clockInOut();
		},
		
		resetSplitAmounts : function()
		{
			if (this.splitManager != null)
				this.splitManager.resetAmounts();
		},
		
		updateSplitAmounts : function()
		{
			if (this.splitManager != null)
				this.splitManager.updateAmounts();
		},
		
		initializeComponents:function(){
			
			this.processDialog = new Dialog();
			
			/* search button & textfield */
			this.searchProductTextField = $('search-product-textfield');
			this.searchProductButton = $('search-product-button');
			
			/* header buttons */
			this.clockInOutButton = $('orderScreen.clockInOutButton');
			this.homeButton = $('orderScreen.homeButton');
			this.keyboardButton = $('orderScreen.keyboardButton');
			this.infoButton = $('orderScreen.infoButton');
			
			/* cart buttons & textfield */
			this.discountButton = $('discount-button');
			this.changePinButton = $('change-user-button');
			/*this.openCashDrawerButton = $('open-drawer-button');*/
			/*this.saveOrderButton = $('save-order-button');*/
			//this.commentsButton = $('comments-button');
			this.invokeOrderButton = $('load-button');
			this.clearCartButton = $('clear-button');
			/*this.decrementQtyButton = $('decrease-button');*/
			/*this.incrementQtyButton = $('add-button');*/			
			this.updateQtyTextField = $('quantity-texfield');	
			
			/* payment buttons */
			this.cashPaymentButton = $('orderScreen.cashPaymentButton');
			this.cardPaymentButton = $('orderScreen.cardPaymentButton');
			this.chequePaymentButton = $('orderScreen.chequePaymentButton');
			this.mixPaymentButton = $('orderScreen.mixPaymentButton');			
			this.onCreditPaymentButton = $('orderScreen.onCreditPaymentButton');			
			this.voucherPaymentButton = $('orderScreen.voucherPaymentButton');
			this.giftCardPaymentButton = $('orderScreen.giftCardPaymentButton');
			this.SKWalletPaymentButton = $('orderScreen.SKWalletPaymentButton');
			
			/* other buttons */
			this.bpInfoButton = $('bp-info-button');
			this.bpCreateButton = $('bp-create-button');
			/*this.checkoutButton = $('checkout-button');*/
			//this.newItemButton = $('create-item-button');			
			//this.giftCardButton = $('gift-card-button');
			//this.couponButton = $('redeem-coupon-button');
			
			this.backDateButton = $('back-date-button');
			if(DiscountManager.allowOrderBackDate()){
				if(this.backDateButton){
					this.backDateButton.show();
				}
			}
			
			
			
			this.referenceButton = $('reference-button');
			this.splitOrderButton = $('split-order-button');			
			//this.productInfoButton = $('product-info-button');
			
			/*Action buttons*/
			this.orderButton = $('order-button');
			this.terminalInfoButton = $('terminal-info-button');
			this.orderButtonContainer = $('order-actions-button-container');
			this.emptyCartButtonContainer = $('empty-cart-button-container');
			/*this.moreButton = $('more-button');*/
			
			/* drop downs */
			/*this.taxSelectList  = $('tax-dropdown');
			if(this.taxSelectList){
				this.taxSelectList.value = OrderTax.defaultTaxId;
			}*/
			
			/* set smart product search */
			SearchProduct.isSoTrx = this.isSoTrx;		
			
			this.selectQueryBox();
			 
			/* add behaviour */	
			for(var field in this){
				var fieldContents = this[field];
				
				if (fieldContents == null || typeof(fieldContents) == "function") {
					continue;
			    }
				
				if(fieldContents.type == 'button'){
			    	//register click handler
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.type == 'text'){
			    	//register key handler
			    	fieldContents.onkeyup = this.keyHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.type == 'select-one'){
			    	//register key handler
			    	fieldContents.onchange = this.changeHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.tagName == 'A'){
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.tagName == 'IMG'){
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			}
			
			/* add keypad to qty textfield */
			/*Event.observe(this.updateQtyTextField,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			/* bug fix for updateQtyTextField */
			this.updateQtyTextField.onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					if(!ShoppingCartManager.isShoppingCartEmpty()){
						var qty = parseFloat(this.value);
						
						if(isNaN(qty)){
							alert(Translation.translate('invalid.qty','Invalid Qty!'));
							this.selectAll();								
							return;					
						}
						ShoppingCartManager.updateQty(qty);							
					}
				}
			}
			
			/*tax rate*/
			var cart = ShoppingCartManager.getCart();
			cart.addBehaviourToLines();
			
			//Action button behaviour
			if(ShoppingCartManager.isShoppingCartEmpty())
			{
				this.orderButtonContainer.style.display = 'none';
				this.emptyCartButtonContainer.style.display = 'block';
			}
			else
			{
				this.emptyCartButtonContainer.style.display = 'none';
				this.orderButtonContainer.style.display = 'block';
			}
			
		},
		
		clickHandler : function(e){
			var element = Event.element(e);
			
			/*bug fix for buttons*/
			if(Prototype.Browser.WebKit)
			if(element instanceof HTMLButtonElement 
					|| element instanceof HTMLLinkElement 
					|| (element instanceof HTMLImageElement && !(element.parentNode instanceof HTMLButtonElement)))
			{
				
			}
			else
			{
				element = element.parentNode;
			}
			
			switch(element){
				
				case this.searchProductButton : 
					SearchProduct.search( {barcode : true} );
					break;
				
				case this.clockInOutButton : break;
				case this.homeButton : break;
				case this.keyboardButton : break;
				case this.infoButton : break;
				
				/*case this.discountButton : 
					new DiscountOnTotalPanel().show();
					break;*/
				
				case this.changePinButton : 
					new PINPanel().show();
					break;
				
				/*
				case this.openCashDrawerButton : 
					OrderScreen.openCashDrawer();
					break;*/
				/*
				case this.saveOrderButton : 
					//this.moreButtonContainer.style.display='none';
					OrderScreen.saveOrder();
					break;*/
					
				case this.commentsButton :					
					//OrderScreen.setComment();
					//this.moreButtonContainer.style.display='none';
					new CommentsPanel().show();
					break;
				
				/*
				case this.invokeOrderButton : 
					new InvokeOrderPanel().show();
					break;*/
				
				/*
				case this.clearCartButton :
					//this.moreButtonContainer.style.display='none';
					ShoppingCartManager.clearCart();
					OrderScreen.resetSplitAmounts();
					break;*/
				
				/*case this.decrementQtyButton : 
					ShoppingCartManager.decrementQty();
					break;
				
				case this.incrementQtyButton : 
					ShoppingCartManager.incrementQty();
					break;	*/
					
				/* Payment buttons */
				case this.cashPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CASH);
					break;
					
				case this.cardPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CARD);
					break;
					
				case this.chequePaymentButton :
					OrderScreen.setTenderType(TENDER_TYPE.CHEQUE); 
					break;
					
				case this.mixPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.MIXED);
					break;		
						
				case this.onCreditPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CREDIT);
					break;	
							
				case this.voucherPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.VOUCHER);
					break;
					
				case this.giftCardPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.GIFT_CARD);
					break;
					
				case this.SKWalletPaymentButton :
					OrderScreen.setTenderType(TENDER_TYPE.SK_WALLET);
					break;
					
				case this.ZapperPaymentButton :
					OrderScreen.setTenderType(TENDER_TYPE.ZAPPER);
					break;
					
				/* Checkout button */
				case this.checkoutButton :
					/*OrderScreen.getPaymentDetails();*/
					
					
					
				case this.bpInfoButton :
					new BPInfoPanel().show();
					break;
				
				case this.bpCreateButton : 
					var panel = null;
					
					if (this.isSoTrx == true)
						panel = new CreateCustomerPanel();
					else
						panel = new CreateVendorPanel();
					
					panel.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					panel.show();
					break;
					
				case this.backDateButton :
					var panel = new BackDatePanel();
					panel.updateElements = this.setBackDateValues.bind(this);
					panel.show();
					break;
					
				/*case this.productInfoButton : 
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert('Cart is empty!');
						return;
					}
					var panel = new ProductInfoPanel();
					panel.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					
					 var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
					 var productId = currentLine.productId;
					
					panel.getInfo(productId);
					break;*/
					
				case this.splitOrderButton : 
					OrderScreen.splitManager.showPanel();
					break;
					
				case this.referenceButton : 
					
					OrderScreen.referencePopup = jQuery("<div class='reference-popup' title='Reference'><label>Reference No. <input type='text' id='reference-popup-reference-textfield'/></div>").dialog({width:"auto", height:"auto", modal: true, autoOpen: true, close: function( event, ui ) {
						
					 }, buttons: {
				        "Ok": function() {
				        	OrderScreen.referenceNumber = jQuery('#reference-popup-reference-textfield').val();
				        	OrderScreen.referencePopup.dialog( "close" );
				          },
				        "Cancel": function() {
				        	OrderScreen.referencePopup.dialog( "close" );
				         }
				       }
					});
					
					break;
					
				case this.giftCardButton :
					selectGiftCardTransaction();
					break;
					
				case this.couponButton :
					redeemCoupon();
					break;
					
				case this.newItemButton : 
					/*
					var p = new CreateProductPanel();
					p.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					p.show();*/
					getURL('backoffice-products.do?#0')
					break;
				
				/*
				case this.moreButton :
					new MoreOptionsPanels().show();
					break;*/
				
							
				default:break;
			}		
		},
		
		test : function(){ alert("test");},
		
		xxx : function(){		

			//check age verification
			if(ShoppingCartManager.needAgeVerification()){
				/* show popup */	
				
				var customerName = BPManager.getBP().name;
				var customerDOB = BPManager.getBP().birthday;
				var today = moment();						
				var birthday = moment(customerDOB, 'MM/DD/YYYY');
				
				var age = today.diff(birthday, 'years');
				var minimumAge = TerminalManager.terminal.preference.age;
				
				var divProceed = jQuery('<div title="Customer Age">' +
						'<div>Customer Age: '+age+' yrs old</div>' +
						'<div>Accepted</div>' +
						'</div>');
				
				var divVerifyAge = jQuery('<div title="Verify Customer Age">' +
						'<div><label>D.O.B</label></div>' +
						'<div><input type="text" value="'+customerDOB+'"></div>' +
						'</div>');						
				
				if(age >= minimumAge){
					
					var dialogProceed = jQuery(divProceed).dialog({
			    		position: ['center',40],
			    		modal:true,
			    		open:function(){					    		
			    		},
			    		close:function(){
			    		},
			    		buttons : {
			    			"OK" : function(){
			    				OrderScreen.showPaymentOptions();			    				
			    				dialogProceed.dialog( "close" );
			    			}
			    		}
			    	});	    					
				}
				
				else
				{
					if((customerDOB !="") && (age < minimumAge))
					{
						alert("The Customer age is "+age+"yrs old.\nThe authorized age to buy this item is "+minimumAge+" yrs old")
					}
					
					if(customerDOB =="" || customerName =="Walk-in Customer")
					{
						var cal = divVerifyAge.find("input[type|='text']").datepicker({
							format: "mm/dd/yyyy",
							orientation: "top left",
							endDate: '+0d',
							autoclose: true
						}).on('hide', function (e) { e.preventDefault(); });
						
						var dialogVerifyAge = jQuery(divVerifyAge).dialog({
				    		position: ['center',40],
				    		modal:true,
				    		open:function(){					    		
				    		},
				    		close:function(){
				    		},
				    		buttons : {
				    			"OK" : function(){	   				    								    			
				    				
				    				var birthday = moment(cal.val(), 'MM/DD/YYYY');				    				
				    				var age = today.diff(birthday, 'years');					    				
				    				var minimumAge = TerminalManager.terminal.preference.age;
				    				
				    				var divProceed = jQuery('<div title="Customer Age">' +
											'<div>Customer Age: '+age+' yrs old</div>' +
											'<div>Accepted</div>' +
											'</div>');	
				    				
				    				if(age >= minimumAge){
				    					dialogVerifyAge.dialog( "close" );
				    					//OrderScreen.showPaymentOptions();
				    					
				    					var dialogProceed = jQuery(divProceed).dialog({
								    		position: ['center',40],
								    		modal:true,
								    		open:function(){					    		
								    		},
								    		close:function(){
								    		},
								    		buttons : {
								    			"OK" : function(){
								    				OrderScreen.showPaymentOptions();			    				
								    				dialogProceed.dialog( "close" );
								    			}
								    		}
								    	});					    					
				    				}
				    				else
				    				{
				    					alert("The Customer age is "+age+"yrs old.\nThe authorized age to buy this item is "+minimumAge+" yrs old")
				    				}
				    			},
				    			"Cancel" : function(){
				    				dialogVerifyAge.dialog( "close" );
				    			}
				    		}
				    	});								
					}
				}
				
				
				
				/*var customerDOB = BPManager.getBP().birthday;
				
				var div = jQuery('<div title="Verify Customer Age">' +
						'<div><label>D.O.B</label></div>' +
						'<div><input type="text" value="'+customerDOB+'"></div>' +
						'</div>');
				
				var cal = div.find("input[type|='text']").datepicker({
					format: "mm/dd/yyyy",
					orientation: "top left",
					endDate: '+0d',
					autoclose: true
				}).on('hide', function (e) { e.preventDefault(); });
				
				var dialog = jQuery(div).dialog({
		    		position: ['center',40],
		    		modal:true,
		    		open:function(){					    		
		    		},
		    		close:function(){
		    		},
		    		buttons : {
		    			"OK" : function(){
		    				if(customerDOB == "")
		    				{
		    					alert("Enter Age");
		    					return;
		    				}
		    				
		    				var birthday = moment(customerDOB, 'MM/DD/YYYY');
		    				
		    				var today = moment();
		    				
		    				var age = today.diff(birthday, 'years');
		    				
		    				var minimumAge = TerminalManager.terminal.preference.age;
		    				if(age >= minimumAge){
		    					dialog.dialog( "close" );
		    					OrderScreen.showPaymentOptions();
		    				}
		    				else{
		    					alert( age + " is less than authorized age [" + minimumAge + "]!");
		    				}
		    			},
		    			"Cancel" : function(){
		    				dialog.dialog( "close" );
		    			}
		    		}
		    	});*/
			}
			else 
			{
				OrderScreen.showPaymentOptions();
			}
		},
		
		showPaymentOptions : function(){
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			
			if(!ShoppingCartManager.isShoppingCartReady()){
				alert(Translation.translate('cart.is.not.ready','Cart is not ready!'));
				return;
			}
			
			var salesReps = ClockedInSalesRepManager.getSalesReps();
			
			if(salesReps.size() == 0){						
				alert(Translation.translate("no.user.has.been.clocked.in","No user has been clocked in"));
				return;
			}
			
			if(this.allowUpSell){
				if(ShoppingCartManager.getUpsellTotal() < 0.0){
					alert(Translation.translate("the.upsell.buffer.cannot.be.negative","The upsell buffer cannot be negative!"));
					return;
				}
			}
			
			if(OrderScreen.orderType == 'Quotation'){
				this.tenderType = TENDER_TYPE.CASH;						
				var payment = new Hash();
			  	payment.set('amountTendered', 0.0);
			  	payment.set('amountRefunded', 0.0);			  	
			  	this.payment = payment;
			  	this.checkout();
			  	return;
			}
			
			var cartTotal = this.getCartTotal();
			if(cartTotal == 0.0){
				this.tenderType = TENDER_TYPE.CASH;						
				var payment = new Hash();
			  	payment.set('amountTendered', cartTotal);
			  	payment.set('amountRefunded', 0.0);			  	
			  	this.payment = payment;
			  	this.checkout();
			  	return;
			}
			
			new PaymentPopUp().show();
		},
		
		backDate : function()
		{
			var panel = new BackDatePanel();
			panel.updateElements = this.setBackDateValues.bind(this);
			panel.show();
		},
		
		getProductInfo : function(productId)
		{
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			var panel = new ProductInfoPanel();
			panel.onHide = function(){
				OrderScreen.selectQueryBox();
			};
			
			 /*var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
			 var productId = currentLine.productId;*/
			
			panel.getInfo(productId);
		},
		
		toggleMoreButtonContainer : function(activeContainer)
		{
			this.toggleElement(activeContainer);
		},
		
		toggleElement : function(element){
			
			if (element != null)
			{
				if (element.style.display == 'none')
				{
					element.style.display = 'block';
				}
				else
				{
					element.style.display = 'none';
				}
			}
		},
		
		
		keyHandler : function(e){
			
			var element = Event.element(e);
			
			switch(element){
				case this.searchProductTextField : 
					if(e.keyCode == Event.KEY_RETURN){
						SearchProduct.search({barcode:true});
					}
					break;	
							
				case this.updateQtyTextField : 					
					break;
							
				default:break;
			}
		},
		
		changeHandler : function(e){
			var element = Event.element(e);
			
			switch(element){
				/*case this.taxSelectList : 
					var taxId = this.taxSelectList.value;
					ShoppingCartManager.setLineTax(taxId);
					break;	*/
					
				default:break;
			}
		},
				
		render : function(){
			/* call this to redraw screen */ 
			/*
			$('orderGrandTotal').innerHTML = ShoppingCartManager.getCurrencySymbol() + " " + new Number(ShoppingCartManager.getGrandTotal()).toFixed(2);
			
			*/
			
			PoleDisplayManager.display('Grand Total',ShoppingCartManager.getCurrencySymbol() + new Number(ShoppingCartManager.getGrandTotal()).toFixed(2));
			
			if(this.allowUpSell){
				$('orderScreen-upsellAmountContainer').innerHTML = new Number(ShoppingCartManager.getUpsellTotal()).toFixed(2);
			}
			else{
				$('orderScreen-upsellAmountContainer').innerHTML = '';
			}
			
		},
		
		selectQueryBox : function (){
			if(navigator.userAgent.match(/iPad/i)){
				/*prevent ipad keyboard from popping each time*/
				return;
			}
			
			var textfield = $('search-product-textfield');
			if( textfield != null){
				if(textfield.value.length > 0)
					textfield.select();
				else
					textfield.focus();
			}			
		},
		
		getCartTotal : function(){
			var amt = ShoppingCartManager.getGrandTotal();	
			return parseFloat(amt);
		},
		
		getCurrencySymbol : function(){
			return ShoppingCartManager.getCurrencySymbol();
		},		
		
		initializeDiscount:function(){
			/* see orderScreen.jsp line 163*/
			var discountLimit = this.discountLimit;
			var overrideLimitPrice = this.overrideLimitPrice;
			var discountUpToLimitPrice = this.discountUpToLimitPrice;
			var allowDiscountOnTotal = this.allowDiscountOnTotal;
			var allowOrderBackDate = this.allowOrderBackDate;
			var allowUpSell = this.allowUpSell;
			var discountOnCurrentPrice = this.discountOnCurrentPrice;
	
			var discountRights = new DiscountRights(discountLimit, overrideLimitPrice, discountUpToLimitPrice, allowDiscountOnTotal, allowOrderBackDate, allowUpSell, discountOnCurrentPrice);
			DiscountManager.setDiscountRights(discountRights);
			
			if($('orderScreen.backDateButton'))
			{
				if(!this.allowOrderBackDate){
					$('orderScreen.backDateButton').hide();
				}
				else
				{
					$('orderScreen.backDateButton').show();
				}
			}
			
		},
		
		openCashDrawer : function(){
			/*PrinterManager.showOpenDrawerDialog();*/
			PrinterManager.print([['OPEN_DRAWER']]);
		},
		
		initAutoCompletes : function(){
			var autocompleter = new BP_Autocompleter('search-bp-textfield','bp-autocomplete-choices-container', this.isSoTrx);
			
			//register updateBP function
			autocompleter.afterUpdateBP = this.afterUpdateBP.bind(this);
			
			var autocompleter = new SalesRep_Autocompleter('search-sales-rep-textfield','sales-rep-autocomplete-choices-container');
			
			jQuery("#search-bp-textfield").off("blur").on("blur", function(){
				
								
				var name = jQuery(this).val();
				
				if(name == ""){
					
					var terminal = TerminalManager.terminal;
					var defaultBpId = terminal['bpId'];
					
					if(defaultBpId <= 0){
						// no default customer												
						$('search-bp-textfield').value = "";
						$('bp-name').innerHTML = "";
						BPManager.setBP(null);
					}
					else
					{
						var defaultBpName = terminal['bpName'];
						
						var defaultBp = new BP({
							'c_bpartner_id': defaultBpId, 
							'name' : defaultBpName
						});
						
						
						$('search-bp-textfield').value = defaultBpName;
						$('bp-name').innerHTML = defaultBpName;					
						BPManager.setBP(defaultBp);	
					}
					
					OrderScreen.setBPartnerId(defaultBpId);				
					
				}
				else
				{
					if(BPManager.bp){
						$('search-bp-textfield').value = BPManager.bp.getFullName();
						$('bp-name').innerHTML = BPManager.bp.getFullName();
					}
					else
					{
						$('search-bp-textfield').value = "";
						$('bp-name').innerHTML = "";
					}
					
				}
				
			});
		},
		
		afterUpdateBP : function(bp){
			var id = bp.getId();
			this.setBPartnerId(id);
			
			$('search-bp-textfield').value = bp.getFullName();
			$('bp-name').innerHTML = bp.getFullName();
		},
		
		setBackDateValues : function(values){
			this.referenceNumber = values.referenceNumber;
			this.documentNumber = values.documentNumber;
			this.dateOrdered = values.dateOrdered;
		},
		
		isInventory : function()
		{
			if (OrderScreen.orderType == 'stock-transfer-send' 
				  || OrderScreen.orderType == 'stock-transfer-request')
			{
				return true;
			}
			
			return false;
		},
		
		/*setComment : function()
		{
			if(OrderScreen.commentDialog == null)
			{
				OrderScreen.commentDialog = jQuery("<div title='Enter Comment'><textarea id='order-comment'></textarea></div>").dialog(
					{   width: "auto",
						height : "auto",
						modal: true,
						buttons: {
							"Save": function() {
								var comment = jQuery("#order-comment").val();
								jQuery("#comment").val(comment);
								OrderScreen.commentDialog.dialog( "close" );
							},
							Cancel: function() {
								OrderScreen.commentDialog.dialog( "close" );
							}
						}
					}
				);
			}
			else
			{
				OrderScreen.commentDialog.dialog("open");
			}
			
		},*/
		
		getPaymentRuleForTenderType : function(tenderType){
			var map = [];
			map[TENDER_TYPE.CASH] = PAYMENT_RULE.CASH;
			map[TENDER_TYPE.CARD] = PAYMENT_RULE.CARD;
			map[TENDER_TYPE.CHEQUE] = PAYMENT_RULE.CHEQUE;
			map[TENDER_TYPE.MIXED] = PAYMENT_RULE.MIXED;
			map[TENDER_TYPE.VOUCHER] = PAYMENT_RULE.VOUCHER;
			map[TENDER_TYPE.EXTERNAL_CARD] = PAYMENT_RULE.EXTERNAL_CARD;
			map[TENDER_TYPE.GIFT_CARD] = PAYMENT_RULE.GIFT_CARD;
			map[TENDER_TYPE.CREDIT] = PAYMENT_RULE.CREDIT;
			map[TENDER_TYPE.SK_WALLET] = PAYMENT_RULE.SK_WALLET;
			
			return map[tenderType];
		},
		
		setMerchantWarehousePaymentDetails : function(details)
		{
			$('merchant-warehouse-payment-details').value = details;
		}
};


var ItemFields = {
		
		fields : null,
		
		init : function()
		{
			ItemFields.fields = $('items.fields').innerHTML;
		}
}

var CustomerFields = {
		
		fields : null,
		
		init : function()
		{
			CustomerFields.fields = $('customer.fields').innerHTML;
		}
}

var VendorFields = {
		
		fields : null,
		
		init : function()
		{
			VendorFields.fields = $('vendor.fields').innerHTML;
		}
}

var ClockInClockOut = {
		
		/*init: function()
		{
			$('sales-rep-list').innerHTML = '';
			ClockInClockOut.render();
		},
		
		render : function()
		{
			var salesRepsList = ClockInOutManager.clockedInUsers;
			
			for(var i=0; i<salesRepsList.length; i++){
				  var salesRep = salesRepsList[i];
				  var username = salesRep.username;
				  var clockInTime = salesRep.clockInTime;
				  var clockOutTime = salesRep.clockOutTime;
				  
				  var div = document.createElement('div');
					div.setAttribute('class', 'row ');
					div.setAttribute('style', 'padding: 4px 0px;');
					
					var div2 = document.createElement('div');
					div2.setAttribute('class', 'col-md-6 col-xs-6 no-padding');
					
					var div3 = document.createElement('div');
					div3.setAttribute('class', 'pull-left');
					div3.innerHTML = username;
					
					div2.appendChild(div3);
					div.appendChild(div2);
					
					div2 = document.createElement('div');
					div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
					
					div3 = document.createElement('div');
					div3.setAttribute('class', 'pull-left');
					div3.innerHTML = clockInTime;
					
					div2.appendChild(div3);
					div.appendChild(div2);
					
					div2 = document.createElement('div');
					div2.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
					div2.setAttribute('onclick', 'javascript: ClockInClockOut.clockInOrClockOut(\''+username+'\')');
					
					div3 = document.createElement('div');
					div3.setAttribute('class', 'pull-right');
					
					
					var iconDiv = document.createElement('div')
					iconDiv.setAttribute('class', 'glyphicons glyphicons-pro clock');
					div3.appendChild(iconDiv);
					
					div2.appendChild(div3);
					div.appendChild(div2);
					
					$('sales-rep-list').appendChild(div);
			  }
		},
		
		clockInOrClockOut : function(username)
		{
			var clockInOutPanel =  new ClockInOutPanel();
			clockInOutPanel.setUser(username);
			clockInOutPanel.show();
		}*/
};


var SearchSalesRep = {
		
		input:null,
		param: new Hash(),
		
		initializeComponents:function(){
			SearchSalesRep.input = $('search-sales-rep-textfield');
			
			$('sales-rep-search-button').onclick = function(e){
				SearchSalesRep.search();
			};

			$('search-sales-rep-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchSalesRep.search();
				}
			};
			
			$('search-sales-rep-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-sales-rep-textfield').onclick = function(e){
				this.select();
			};
		},
		
		search : function()
		{
			SearchSalesRep.param.set('action', 'loadSalesReps');
			SearchSalesRep.param.set('searchTerm', SearchSalesRep.input.value);
			
			new Ajax.Request('UserAction.do', {
				method:'post',
				onSuccess: SearchSalesRep.render,
				parameters: SearchSalesRep.param
			});
		},
		
		render : function(response){
			//var jsonArray = response.responseText.evalJSON(true);
			
			var jsonArray = eval('(' + response.responseText + ')');
			
			$('sales-rep-list').innerHTML =  '<div class="col-md-12 col-xs-12 no-padding"></div>';
			
			if (jsonArray.length == 0)
			{
				
				alert(Translation.translate('no.results.found','No Results Found'));
				return;
			}
			
			for (var i=0; i<jsonArray.length; i++)
			{
				var json = jsonArray[i];
				var id = json.id;
				var name = json.name;
				
				
				//SearchSalesRep.bpJSON.set(json.id, json);
				
				var styleClass = 'even-row';
				
				if (i%2 == 0)
				{
					styleClass = 'odd-row';
				}
				
				var div = document.createElement('div');
				div.setAttribute('class', 'row ' + styleClass);
				div.setAttribute('style', 'padding: 4px 0px;');
				div.setAttribute('onclick', 'javascript:SearchSalesRep.add("'+ json.name +'");');
				
				var div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				
				var div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				/*div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name;
				
				div2.appendChild(div3);
				div.appendChild(div2);*/
				
				/*div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-3 col-xs-3 no-padding');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = phone;
				
				div2.appendChild(div3);
				div.appendChild(div2);*/
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-1 col-xs-1 no-padding');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-right');
				
				
				var iconDiv = document.createElement('div')
				div3.appendChild(iconDiv);
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				$('sales-rep-list').appendChild(div);
			}
		},
		
		add : function(name)
		{
			 var clockInOutPanel =  new ClockInOutPanel();
			clockInOutPanel.setUser(name);
			clockInOutPanel.show();
			 
			 /* clear textbox */
			 $('search-sales-rep-textfield').value = "";
		}
};


var SalesRep_Autocompleter = Class.create({
	 initialize: function(textfield, resultContainer) {
	    this.textfield  = textfield;
	    this.resultContainer = resultContainer;
	    
	    this.url = "UserAction.do";
	    
	    this.options = {
	    		'frequency' : 1.0,
	    		'minChars' : 1,
	    		'paramName' : "searchTerm",
	    		'parameters' : ('action=getSalesReps')
	    };
	    	    
	    this.options.afterUpdateElement = this.afterUpdateElement.bind(this);	    	    
	    
	    new Ajax.Autocompleter(this.textfield, this.resultContainer, this.url, this.options);
	    
	    this.textfield = $(this.textfield);
	    
	    this.textfield.onclick = function(e){
	    	this.select();
	    };
	 },
	 
	 afterUpdateElement:function(text, li){
		 
		 var json = li.getAttribute('json');
		 
		 if(json == null) return;
		 
		 json = eval('(' + json + ')');
		 
		 
		 var clockInOutPanel =  new ClockInOutPanel();
		clockInOutPanel.setUser(json.name);
		clockInOutPanel.show();
		 
		 /* clear textbox */
		 this.textfield.value = "";
	 }
	 
	 
});


var ORDER_TYPES = { 
	POS_ORDER : 'POS Order',
	CUSTOMER_RETURNED_ORDER : 'Customer Returned Order',
	POS_GOODS_RECEIVE_NOTE : 'POS Goods Receive Note',		
	POS_GOODS_RETURNED_NOTE : 'POS Goods Returned Note'
};
	
var TENDER_TYPE = {
		CARD : 'Card',
		CASH : 'Cash',
		CHEQUE : 'Cheque',
		CREDIT : 'Credit',
		MIXED : 'Mixed',
		VOUCHER : 'Voucher',
		EXTERNAL_CARD : 'Ext Card',
		GIFT_CARD : 'Gift Card',
		SK_WALLET : 'SK Wallet',
		ZAPPER : 'Zapper',
		LOYALTY : 'Loyalty',
		MCB_JUICE : 'MCB Juice',
		MY_T_MONEY : 'MY.T Money',
		EMTEL_MONEY : 'Emtel Money',
		GIFTS_MU : 'Gifts.mu',
		MIPS : 'MIPS'
	};

var PAYMENT_RULE = {
		CARD : 'K',
		CASH : 'B',
		CHEQUE : 'S',
		CREDIT : 'P',
		MIXED : 'M',
		VOUCHER : 'V',
		EXTERNAL_CARD : 'E',
		GIFT_CARD : 'G',
		SK_WALLET : 'W',
		ZAPPER : 'Z',
		LOYALTY : 'L',
		MCB_JUICE : 'J',
		MY_T_MONEY : 'Y',
		EMTEL_MONEY : 'T',
		MIPS : 'I',
		GIFTS_MU : 'U'
		
};

var SO_CREDIT_STATUS = {
	CreditStop 		: "S",
	CreditHold 		: "H",
	CreditWatch 	: "W",
	NoCreditCheck 	: "X",
	CreditOK 		: "O"
};

var DELIVERY_RULE = {
		AFTER_RECEIPT : 'R',
		COMPLETE_ORDER : 'O',
		MANUAL : 'M'
};

/* define some variables */
OrderScreen.commentDialog = null;

/*
window.onerror=function(){
	OrderScreen.processDialog.hide();
};
*/

jQuery(function(){
	/*more options*/
	jQuery("#redeem-promotion-button").off('click').on('click', function(e){
		PopUpManager.clear();
		new RedeemPromotionPanel().show();
		
	});

	jQuery("#redeem-coupon-button").off('click').on('click', function(e){
		PopUpManager.clear();
		redeemCoupon();
	});

	jQuery(".gift-card-button").off('click').on('click', function(e){
		PopUpManager.clear();
		selectGiftCardTransaction();
	});
	
	jQuery(".deposit-button").off('click').on('click', function(e){
		PopUpManager.clear();
		selectDepositTransaction();
	});

	jQuery(".create-item-button").off('click').on('click', function(e){
		PopUpManager.clear();
		getURL('backoffice-products.do?#0');
	});

	jQuery("#split-order-button").off('click').on('click', function(e){
		PopUpManager.clear();
		OrderScreen.splitManager.showPanel();
	});

	jQuery("#discount-on-total-button").off('click').on('click', function(e){
		PopUpManager.clear();
		DiscountManager.getDiscountPanel();
	});

	jQuery("#change-tax-button").off('click').on('click', function(e){
		PopUpManager.clear();
		changeTaxPanel.show();			
	});

	jQuery("#comments-button").off('click').on('click', function(e){
		PopUpManager.clear();
		new CommentsPanel().show();		
	});
	
	jQuery("button.box-no-button").off('click').on('click', function(e){
		PopUpManager.clear();
		var box = window.prompt("Enter box/bag number");
		if(box){
			shoppingCart.setCurrentBoxNumber(box);
		}		
	});
	
	jQuery("button.search-by-barcode-only-button").off('click').on('click', function(e){
		PopUpManager.clear();
		/*
		var searchTerm = window.prompt("Search by SKU, Name, Description, Groups");
		if(searchTerm){
			SearchProduct.input.value = searchTerm;
			SearchProduct.search();
		}	
		*/	
		new SearchProductPanel().show();
	});
	
	/* transition from prototype.js to jquery */
	jQuery('#load-button').off('click').on('click', function(e){
		new InvokeOrderPanel().show();
	});
	
	jQuery('#import-csv-button').off('click').on('click', function(e){
		window.location = 'importer.do?importer=OrderLineImporter&orderType=' + shoppingCart.orderType;
	});
	
	jQuery('#open-drawer-button').off('click').on('click', function(e){
		OrderScreen.openCashDrawer();
	});
	
	jQuery('#checkout-button').off('click').on('click', function(e){
		
		var bp = BPManager.getBP();
		
		if(bp == null)
		{
			alert(((OrderScreen.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');
			
			jQuery("#search-bp-textfield").focus();
			
			return;
		}
		
		//check default bp
		if(OrderScreen.isSoTrx == true 
				&& (TerminalManager.terminal.bpId == bp.id) 
				&& TerminalManager.terminal.preference.cms == true)
		{
			
			var getPosteritaCardNo = function(){
				
				var $message = jQuery('<div></div>');
				$message.append('Enter Posterita Loyalty Card Number<br />');
		        
		        var $input = jQuery('<input type="text" class="form-control form-control-lg">');
		        
		        $input.css({
		        	'padding': '.5rem 1rem',
				  	'font-size': '1.25rem',
				  	'line-height': '1.5',
		        	'border-radius': '.3rem'
		        });
		        
		        $message.append($input);
				
				var dlg = BootstrapDialog.show({
					size: BootstrapDialog.SIZE_SMALL,
		            title: 'Posterita Loyalty',
		            message: $message,
		            buttons: [{
		                label: 'Ok',
		                cssClass: 'btn-primary',
		                action: function(dialog) {			                    
		                    
		                    //validate card number
		                    validateID();
		                    
		                    //OrderScreen.performAgeVerification();
		                }
		            }, {
		                label: 'Cancel',
		                action: function(dialog) {
		                	dialog.close();
		                }
		            }],
		            
		            onshown : function(){
		            	
		            	$input.focus();
		            }
		        });
				
				//enter listner
				$input.on('keyup', function (e) {
				    if (e.keyCode == 13) {
				        // validate
				    	validateID();
				    }
				});
				
				var validateID = function(){
					
					//alert($input.val());
					//dlg.close();
					
					jQuery.post("BPartnerAction.do",
					  {
					    action : "getCMSCustomer",
					    "identifier" : $input.val()
					  },
					  function(data,status,xhr){
						  
						data = JSON.parse(data);
						  
						if(data.error){
							
							alert(data.error);
							return;										
						}
						
						dlg.close();
						
						var bp = new BP({
							'c_bpartner_id': data['c_bpartner_id'], 
							'name' : data['name']
						});
						
						OrderScreen.setBPartnerId(data['c_bpartner_id']).done(function(){
							
							$('search-bp-textfield').value = data['name'];
							$('bp-name').innerHTML = data['name'];
							
							BPManager.setBP(bp);
							
							OrderScreen.xxx();
							
						});
						
					    
					  });
				};
				
			};
			
			//posterita card
			BootstrapDialog.show({
				size: BootstrapDialog.SIZE_SMALL,
	            title: 'Posterita Loyalty',
	            message: 'Does customer have a Posterita Loyalty Card?',
	            buttons: [{
	            	id: 'posterita_loyalty_card_popup_yes_button',
	                label: 'Yes',
	                cssClass: 'btn-primary',
	                action: function(dialog) {
	                    dialog.close();
	                    
	                    getPosteritaCardNo();
	                }
	            }, {
	            	id: 'posterita_loyalty_card_popup_no_button',
	                label: 'No',
	                action: function(dialog) {
	                	dialog.close();
	                	
	                	OrderScreen.xxx();
	                }
	            }]
	        });
		}
		else
		{
			OrderScreen.xxx();
		}	
	});
	
	jQuery('#clear-button').off('click').on('click', function(e){
		
		if(window.confirm('Do you want to clear cart?')){
			ShoppingCartManager.clearCart();
			OrderScreen.resetSplitAmounts();
		}		
		
	});
	
	jQuery('#save-order-button').off('click').on('click', function(e){
		OrderScreen.saveOrder();
	});
	
	jQuery('#more-button').off('click').on('click', function(e){
		new MoreOptionsPanels().show();
	});
	
	jQuery('#load-quotation-button').click(function(){
    	PopUpManager.clear();
    	new InvokeQuotationPanel().show();
    });
	
	/* apply preference  */
	
	if(OrderScreen.isSoTrx === true){
		
		var preference = TerminalManager.terminal.preference;
		
		if( preference.searchByBarcodeOnly === true ){
			
			SearchProduct.searchByBarcodeOnly = true;
			
			var tf = document.getElementById("search-product-textfield");
			tf.placeholder = "Barcode";
		}
		
		if( preference.trackManualProductEntry === true ){
			SearchProduct.trackManualProductEntry = true;
		}
	}

});

/* load orderscreen glider and other defaults like modifier groups and mappings */
jQuery(document).ready(function(){
	
	var gliderHTML = sessionStorage.getItem('GLIDER');
	
	if(gliderHTML == null){
		
		jQuery.getJSON("OrderAction.do?action=loadOrderScreenDefaults", function(data){
			
			var defaults = data;
			
			gliderHTML = defaults.productSearchFilter;
			sessionStorage.setItem('GLIDER', gliderHTML);
			
			//set other defaults
			sessionStorage.setItem('MODIFIER_GROUPS', Object.toJSON(defaults["modifierGroups"]));
			sessionStorage.setItem('MODIFIER_MAPPINGS', Object.toJSON(defaults["modifierMappings"]));
			
			gliderReady(gliderHTML);
		});
		
	}
	else
	{
		gliderReady(gliderHTML);
	}
	
});
