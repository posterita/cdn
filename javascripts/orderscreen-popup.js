var CheckoutPanel = Class.create(PopUpBase, {
	initialize : function(panelId)
	{
		/*If current user is not a sales rep, return*/
		
		if (!ClockedInSalesRepManager.getIsCurrentUserSalesRep())
		{
			alert("Current user " + ClockedInSalesRepManager.getCurrentSalesRep +" is not a sales rep and will not be able to make orders!");
			return;
		}
		
		
		
		
		var table = $('checkoutPanel' + panelId);
		if (table == null)
		{
			var table = document.createElement('table');
			table.setAttribute('id', 'checkoutPanel' + panelId);
		}
		else
		{
			table.innerHTML = '';
		}
		var tr = document.createElement('tr');
		table.appendChild(tr);
		
		var referenceSalesRep = null;		
		if(OrderScreen.splitManager != null) referenceSalesRep = OrderScreen.splitManager.getRefSalesRep();
		
		if (referenceSalesRep != null && referenceSalesRep.getAmount != 0)
		{
			var divImage = document.createElement('div');
			var divName = document.createElement('div');
			var divAmount = document.createElement('div');
			
			var span = document.createElement('span');
			span.setAttribute('class', 'employee');
			
			//divImage.innerHTML = '<div class="butn-user01"></div>';
			divName.innerHTML = referenceSalesRep.getName();
			divAmount.innerHTML = referenceSalesRep.getAmount();
			
			//divImage.setAttribute('class', 'salesRepInfoImage');
			divName.setAttribute('class', 'salesRepInfo');
			divAmount.setAttribute('class', 'salesRepInfo');
			
			span.appendChild(divName);
			span.appendChild(divAmount);
			
			var td = document.createElement('td');
			//divImage.appendChild(divName);
			//divImage.appendChild(divAmount);
			td.appendChild(span);
			tr.appendChild(td);
		}
		
		var splitSalesReps = null;
		if(OrderScreen.splitManager != null) splitSalesReps = OrderScreen.splitManager.getSplitSalesReps();
		if (splitSalesReps != null)
		{
			for (var i=0; i<splitSalesReps.size(); i++)
			{
				var splitSalesRep = splitSalesReps.get(i);
				var name = splitSalesRep.getName();
				var amount = splitSalesRep.getAmount();
				
				if (amount != 0)
				{
					//var divImage = document.createElement('div');
					var divName = document.createElement('div');
					var divAmount = document.createElement('div');
					
					var span = document.createElement('span');
					span.setAttribute('class', 'employee');
					
					//divImage.innerHTML = '<div class="butn-user02-off"></div>';
					divName.innerHTML = name;
					divAmount.innerHTML = amount;
					
					//divImage.setAttribute('class', 'salesRepInfoImage');
					divName.setAttribute('class', 'salesRepInfo');
					divAmount.setAttribute('class', 'salesRepInfo');
					
					var td = document.createElement('td');
					//divImage.appendChild(divName);
					//divImage.appendChild(divAmount);
					
					span.appendChild(divName);
					span.appendChild(divAmount);
					
					td.appendChild(span);
					
					tr.appendChild(td);
				}
			}
			
			new Insertion.Top(panelId + '.split', table);
			
		}
		this.createPopUp($(panelId));
	}
});

var CashPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('cash.popup.panel');
	    
	    this.amountRefunded = 0.0;
	    
	    /*buttons*/
	    this.okBtn = $('cash.popup.okButton');
	    this.cancelBtn = $('cash.popup.cancelButton');
	    
	    /*textfields*/
	    this.amountTenderedTextField = $('cash.popup.amountTenderedTextField');   
	    
	    /*labels*/
	    this.totalLabel = $('cash.popup.totalLabel');
	    //this.writeOffCashLabel = $('cash.popup.writeOffCashLabel');
	    //this.paymentAmtLabel = $('cash.popup.paymentAmtLabel');
	    this.amountRefundedLabel = $('cash.popup.amountRefundedLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    //this.paymentAmtLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*others*/
	    this.amountTenderedTextField.panel = this;
	    this.amountTenderedTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('invalid.amt');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}
	    	
	    	var value = this.value;
	    	if(value == null || value == '') value = 0.0;
	    	
	    	var amountTendered = parseFloat(value);
	    	var paymentAmt = this.panel.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){
	    		this.panel.onError('invalid.amt');
	    		return;
	    	}
	    };
	    
	    this.amountTenderedTextField.onkeyup = function(e){
	    	
	    	var value = this.value;
	    	if(value == null || value == '') value = 0.0;
	    	
	    	var amountTendered = parseFloat(value);
	    	var paymentAmt = this.panel.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){    		
	    		return;
	    	}
	    	
	    	var change = 0;
	    	if(amountTendered != 0.0){
	    		change = amountTendered - paymentAmt; 
	    	}
	    	   
	    	change =  new Number(change).toFixed(2);	
	    	
	    	this.panel.amountRefundedLabel.innerHTML = '' + change;    	
	    	this.panel.amountRefunded = change;
	    };
	    
	    /*cash-out panel*/
	    var cashOutButtons = $$('.cash-out-panel button');
	    for(var i=0; i<cashOutButtons.length; i++){
	    	var btn = cashOutButtons[i];
	    	btn.onclick = this.cashOutHandler.bind(this);
	    }
	    
	    /*generic keypad panel*/
	    var keypadButtons = $$('.key-pad-button');
	    
	    for(var j=0; j<keypadButtons.length; j++)
	    {
	    	var btn = keypadButtons[i];
	    	btn.onclick = this.keypadHandler.bind(this);
	    }
	    
	  },
	  
	  cashOutHandler:function(e){
		  var element = Event.element(e);
		  
		  var amt = 0;
		  
		  if(element.className == 'exact-amount'){
			  amt = this.cartTotalAmount;
			  
			  this.amountTenderedTextField.value = new Number(amt).toFixed(2);
			  this.amountRefundedLabel.innerHTML = '0.00';    	
		      this.amountRefunded = 0.00;
		      
		      return;
		  }
		  else
		  {
			  amt = parseFloat(element.innerHTML);
		  }
		  
		  			  
		  var amountTendered = this.amountTenderedTextField.value;
		  if(amountTendered == '') amountTendered = 0;
		  
		  if('CLR' == element.innerHTML.strip()){
			  amt = 0;
			  amountTendered = 0;
		  }
		  
		  amountTendered = parseFloat(amountTendered) + amt;
		  this.amountTenderedTextField.value = new Number(amountTendered).toFixed(2);
		  
		  /*code from this->amountTenderedTextField->onkeyup*/
		  var paymentAmt = this.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){    		
	    		return;
	    	}
	    	
	    	var change = 0;
	    	if(amountTendered != 0.0){
	    		change = amountTendered - paymentAmt; 
	    	}
	    	   
	    	change =  new Number(change).toFixed(2);	
	    	
	    	this.amountRefundedLabel.innerHTML = '' + change;    	
	    	this.amountRefunded = change;
	  },
	  
	  keypadHandler:function(e){
		  var element = Event.element(e);			  
		  var amt = parseFloat(element.innerHTML);
		  			  
		  var amountTendered = this.amountTenderedTextField.value;
		  if(amountTendered == '') amountTendered = 0;
		  
		  if('&larr;' == element.innerHTML.strip()){
			  amt = 0;
			  amountTendered = 0;
		  }
		  
		  amountTendered = amt;
		  this.amountTenderedTextField.value = new Number(amountTendered).toFixed(2);
		  
		  /*code from this->amountTenderedTextField->onkeyup*/
		  var paymentAmt = this.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){    		
	    		return;
	    	}
	    	
	    	var change = 0;
	    	if(amountTendered != 0.0){
	    		change = amountTendered - paymentAmt; 
	    	}
	    	   
	    	change =  new Number(change).toFixed(2);	
	    	
	    	this.amountRefundedLabel.innerHTML = '' + change;    	
	    	this.amountRefunded = change;
	  },
	  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.amountTenderedTextField.value = '';
	  	this.amountRefundedLabel.innerHTML = '0.00';  
	  	
	  	this.amountRefunded = 0.0;
	  },
	  onShow:function(){
	  	this.amountTenderedTextField.focus();
	  	this.amountRefundedLabel.innerHTML = '0.00';  
	  },
	  validate:function(){
		  
		  var amountTendered = parseFloat(this.amountTenderedTextField.value);
		  if(isNaN(amountTendered) || (amountTendered < 0.0)){
			  this.onError('invalid.amt');
			  return false;
		  }
		  
		  var paymentAmt = this.cartTotalAmount;
		  if(amountTendered < paymentAmt){
			  this.onError('invalid.amt');
			  return false;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('amountTendered',this.amountTenderedTextField.value);
	  	payment.set('amountRefunded', this.amountRefunded);  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});

/* Card Panel */
var CardPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('card.popup.panel');
	    
	    /*panes*/
	    this.trackDataDetailsPane = $('card.popup.trackDataDetailsPane');
	    this.cardDetailsPane = $('card.popup.cardDetailsPane');
	    
	    /*labels*/
	    this.totalLabel = $('card.popup.totalLabel');	 	    
	    
	    /*textfields*/
	    /*this.cardSelectList = $('card.popup.cardSelectList');*/
	    this.swipeCardCheckBox = $('card.popup.swipeCardCheckBox');
	    
	    this.trackDataTextField = $('card.popup.trackDataTextField');
	    this.cardNumberTextField = $('card.popup.cardNumberTextField');
	    this.cardExpiryDateTextField = $('card.popup.cardExpiryDateTextField');
	    this.cardCvvTextField = $('card.popup.cardCvvTextField');
	    this.cardHolderNameTextField = $('card.popup.cardHolderNameTextField');
	    this.streetAddressTextField = $('card.popup.streetAddressTextField');
	    this.zipCodeTextField = $('card.popup.zipCodeTextField');
	    
	    /*buttons*/
	    this.okBtn = $('card.popup.okButton');
	    this.cancelBtn = $('card.popup.cancelButton');	   
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*show details panes*/	    
	    this.cardDetailsPane.style.display = this.swipeCardCheckBox.checked ? 'none' : '';
	    this.trackDataDetailsPane.style.display = this.swipeCardCheckBox.checked ? '' : 'none';
			    
	    this.swipeCardCheckBox.panel = this;
	    this.swipeCardCheckBox.onchange = function(e){
	    	this.panel.cardDetailsPane.style.display = this.checked ? 'none' : '';
	 	    this.panel.trackDataDetailsPane.style.display = this.checked ? '' : 'none';	 	   
		    
		 	this.panel.trackDataTextField.value = '';
		 	this.panel.cardNumberTextField.value = '';
		 	this.panel.cardExpiryDateTextField.value = '';
		 	this.panel.cardCvvTextField.value = '';
		 	this.panel.cardHolderNameTextField.value = '';
		 	this.panel.streetAddressTextField.value = '';
		 	this.panel.zipCodeTextField.value = '';
		};
		
		/*track data*/
		this.trackDataTextField.panel = this;
		this.trackDataTextField.onkeypress = function(e){			
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.credit.card');
					return;
				}
						
				this.panel.okHandler();		
			}
		};
		
		/*card number*/
		this.cardNumberTextField.panel = this;
		this.cardNumberTextField.onkeypress = function(e){			
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('enter.credit.card.number');
					return;
				}
						
				this.panel.okHandler();		
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.swipeCardCheckBox.checked = true;
	  	/*this.cardSelectList.options[0].selected = true;*/
	    
	    this.trackDataTextField.value = '';
	    this.cardNumberTextField.value = '';
	    this.cardExpiryDateTextField.value = '';
	    this.cardCvvTextField.value = '';
	    this.cardHolderNameTextField.value = '';
	    this.streetAddressTextField.value = '';
	    this.zipCodeTextField.value = '';
	  },
	  onShow:function(){
	  	this.trackDataTextField.focus();
	  },
	  
	  validate:function(){
		  
		  if(this.swipeCardCheckBox.checked){
			  /*card was swipped*/ 
			  if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
				  this.onError('swipe.credit.card');
				  return false;
			  }
		  }
		  else{
			  /*no card environment*/ 
			  if(this.cardNumberTextField.value == null || this.cardNumberTextField.value == ''){
				  this.onError('credit.card.number.is.required');
				  return false;
			  }
			  
			  if(this.cardExpiryDateTextField.value == null || this.cardExpiryDateTextField.value == ''){
				  this.onError('credit.card.has.expired');
				  return false;
			  }
				
			  
		  }
		  
		  this.setPayment();
		  		  
		  return true;
	  },
	  
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('isCardPresent', this.swipeCardCheckBox.checked);
	  	payment.set('cardType',"M");
	  	payment.set('cardTrackDataEncrypted',null);
	  	
	  	if(this.swipeCardCheckBox.checked == true){
	  		payment.set('trackData',this.trackDataTextField.value);		  		
	  	}
	  	else{	  		
	  		payment.set('cardNumber',this.cardNumberTextField.value);
	  		payment.set('cardExpiryDate',this.cardExpiryDateTextField.value);
	  		payment.set('cardCvv',this.cardCvvTextField.value);
	  		payment.set('cardHolderName',this.cardHolderNameTextField.value);
	  		payment.set('streetAddress',this.streetAddressTextField.value);
	  		payment.set('zipCode',this.zipCodeTextField.value);
	  	}	  	
	  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* Cheque Panel */
var ChequePanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('cheque.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('cheque.popup.okButton');
	    this.cancelBtn = $('cheque.popup.cancelButton');
	    
	    /*labels*/
	    this.totalLabel = $('cheque.popup.totalLabel');
	    
	    /*textfields*/
	    this.chequeNumberTextField = $('cheque.popup.chequeNumberTextField');    
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

	    this.chequeNumberTextField.panel = this;
	    this.chequeNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('enter.check.number');
					return;
				}
						
				this.panel.validate();			
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.chequeNumberTextField.value = '';
	  },
	  onShow:function(){
	  	this.chequeNumberTextField.focus();
	  },
	  validate:function(){
		  if(this.chequeNumberTextField.value == null || this.chequeNumberTextField.value == ''){
			  this.onError('invalid.cheque.number');
			  return false;
		  }
		  
		  this.setPayment();
		  
		  return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('chequeNumber', this.chequeNumberTextField.value);	  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* Mix Panel */
var MixPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('mix.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('mix.popup.okButton');
	    this.cancelBtn = $('mix.popup.cancelButton');
	    
	    /*textfields*/
	    this.cashAmountTextField = $('mix.popup.cashAmountTextField');
	    this.cardAmountTextField = $('mix.popup.cardAmountTextField');	    
	    this.chequeAmountTextField = $('mix.popup.chequeAmountTextField');
	    this.voucherAmountTextField = $('mix.popup.voucherAmountTextField');
	    this.voucherNoTextField = $('mix.popup.voucherNoTextField');
	    
	    this.chequeNumberTextField = $('mix.popup.chequeNumberTextField');	    
	    
	    /*this.cardSelectList = $('mix.popup.cardSelectList');*/
	    this.swipeCardCheckBox = $('mix.popup.swipeCardCheckBox');	    
	    this.trackDataTextField = $('mix.popup.trackDataTextField');
	    this.cardNumberTextField = $('mix.popup.cardNumberTextField');
	    this.cardExpiryDateTextField = $('mix.popup.cardExpiryDateTextField');
	    this.cardCvvTextField = $('mix.popup.cardCvvTextField');
	    this.cardHolderNameTextField = $('mix.popup.cardHolderNameTextField');
	    this.streetAddressTextField = $('mix.popup.streetAddressTextField');
	    this.zipCodeTextField = $('mix.popup.zipCodeTextField');
	    
	    this.externalCardAmountTextField = $('mix.popup.externalCardAmountTextField');
	    
	    /*labels*/
	    this.totalLabel = $('mix.popup.totalLabel');
	    
	    /*panes*/
	    this.trackDataDetailsPane = $('mix.popup.trackDataDetailsPane');
	    this.cardDetailsPane = $('mix.popup.cardDetailsPane');
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*show details panes*/	    
	    this.cardDetailsPane.style.display = this.swipeCardCheckBox.checked ? 'none' : '';
	    this.trackDataDetailsPane.style.display = this.swipeCardCheckBox.checked ? '' : 'none';
			    
	    this.swipeCardCheckBox.panel = this;
	    this.swipeCardCheckBox.onchange = function(e){
	    	this.panel.cardDetailsPane.style.display = this.checked ? 'none' : '';
	 	    this.panel.trackDataDetailsPane.style.display = this.checked ? '' : 'none';			
		};

	    this.cashAmountTextField.panel = this;
	    this.cashAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	cashAmount = parseFloat(totalAmount);
	    	
	    	var cashAmount = this.value;
	    	cashAmount = parseFloat(cashAmount);
	    	
	    	if(isNaN(cashAmount)) return;
	    	
	    	if(cashAmount < 0.0 || cashAmount > totalAmount){
	    		this.panel.onError('invalid.cash.amt');
	    		return;
	    	}
	    };
	    
	    this.cashAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	cashAmount = parseFloat(totalAmount);
	    	
	    	var cashAmount = this.value;
	    	cashAmount = parseFloat(cashAmount);
	    	
	    	if(isNaN(cashAmount)) return;
	    	
	    	if(cashAmount < 0.0 || cashAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cardAmount = parseFloat(this.panel.cardAmountTextField.value);
	    	if(isNaN(cardAmount)) cardAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    	
	    	this.panel.cardAmountTextField.value = new Number(cardAmount).toFixed(2);
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
			
			/*update card text field*/
			var sum = chequeAmount + voucherAmount + externalCardAmount + cashAmount;
			var remainder = totalAmount - sum;
			this.panel.cardAmountTextField.value = new Number(remainder).toFixed(2);
	    };
	    
	    this.cardAmountTextField.panel = this; 
	    this.cardAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		this.panel.onError('invalid.card.amt');
	    		return;
	    	}
	    };
	    
	    this.cardAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    				
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
	    };
	    
	    this.chequeAmountTextField.panel = this;
	    this.chequeAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var chequeAmount = this.value;
	    	chequeAmount = parseFloat(chequeAmount);
	    	
	    	if(isNaN(chequeAmount)) return;
	    	
	    	if(chequeAmount < 0.0 || chequeAmount > totalAmount){
	    		this.panel.onError('invalid.cheque.amt');
	    		return;
	    	}
	    };
	    
	    this.chequeAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var chequeAmount = this.value;
	    	chequeAmount = parseFloat(chequeAmount);
	    	
	    	if(isNaN(chequeAmount)) return;
	    	
	    	if(chequeAmount < 0.0 || chequeAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    	var cardAmount = parseFloat(this.panel.cardAmountTextField.value);
	    	if(isNaN(cardAmount)) cardAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    		    	
	    	
	    	this.panel.cardAmountTextField.value = new Number(cardAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
	    };
	    
	    this.voucherAmountTextField.panel = this;
	    this.voucherAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var voucherAmount = this.value;
	    	voucherAmount = parseFloat(voucherAmount);
	    	
	    	if(isNaN(voucherAmount)) return;
	    	
	    	if(voucherAmount < 0.0 || voucherAmount > totalAmount){
	    		this.panel.onError('invalid.voucher.amount.');
	    		return;
	    	}
	    };
	    
	    this.voucherAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var voucherAmount = this.value;
	    	voucherAmount = parseFloat(voucherAmount);
	    	
	    	if(isNaN(voucherAmount)) return;
	    	
	    	if(voucherAmount < 0.0 || voucherAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cardAmount = parseFloat(this.panel.cardAmountTextField.value);
	    	if(isNaN(cardAmount)) cardAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    				
			this.panel.cardAmountTextField.value = new Number(cardAmount).toFixed(2);
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
	    };
	    
	    this.externalCardAmountTextField.panel = this;
	    this.externalCardAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		this.panel.onError('invalid.card.amt');
	    		return;
	    	}
	    };
	    
	    this.externalCardAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    				
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
	    };
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	/*amounts*/
	  	this.cashAmountTextField.value = '';
	    this.cardAmountTextField.value = '';   
	    this.chequeAmountTextField.value = '';
	    this.voucherAmountTextField.value = '';
	    this.externalCardAmountTextField.value = '';   
	    
	    /*cheque*/
	    this.chequeNumberTextField.value = '';
	    
	  	/*creditcard*/
	  	this.swipeCardCheckBox.checked = true;
	  	/*this.cardSelectList.options[0].selected = true;*/
	  		    
	    this.trackDataTextField.value = '';
	    this.cardNumberTextField.value = '';
	    this.cardExpiryDateTextField.value = '';
	    this.cardCvvTextField.value = '';
	    this.cardHolderNameTextField.value = '';
	    this.streetAddressTextField.value = '';
	    this.zipCodeTextField.value = '';
	    
	    /*voucher*/
	    this.voucherNoTextField.value = '';
	    
	  },
	  onShow:function(){
		var totalAmount = this.cartTotalAmount;
		this.cashAmountTextField.value = new Number(totalAmount).toFixed(2);
	  	this.cashAmountTextField.focus();
	  },
	  validate:function(){
		  var totalAmount = this.cartTotalAmount;
		  
		  var cashAmount = parseFloat(this.cashAmountTextField.value);
		  if(isNaN(cashAmount)){
		  	cashAmount = 0.0;
		  	this.cashAmountTextField.value = '0';
		  }
		  
		  var cardAmount = parseFloat(this.cardAmountTextField.value);
		  if(isNaN(cardAmount)){
		  	cardAmount = 0.0;
		  	this.cardAmountTextField.value = '0';
		  }
		  
		  var externalCardAmount = parseFloat(this.externalCardAmountTextField.value);
		  if(isNaN(externalCardAmount)){
			  externalCardAmount = 0.0;
		  	this.externalCardAmountTextField.value = '0';
		  }
		  
		  var chequeAmount = parseFloat(this.chequeAmountTextField.value);
		  if(isNaN(chequeAmount)){
		  	chequeAmount = 0.0;
		  	this.chequeAmountTextField.value = '0';
		  }
		  
		  var voucherAmount = parseFloat(this.voucherAmountTextField.value);
		  if(isNaN(voucherAmount)){
		  	voucherAmount = 0.0;
		  	this.voucherAmountTextField.value = '0';
		  }
		  
		  var voucherNo = this.voucherNoTextField.value;
		  
		  if (voucherAmount != new Number(0) && voucherNo == '')
		  {
			  this.onError('enter.order.no.for.voucher');
			  return false;
		  }
		  
		  var paymentAmt = cashAmount + cardAmount + chequeAmount + voucherAmount + externalCardAmount;
		  var difference = totalAmount - paymentAmt;
		  
		  difference = new Number(difference).toFixed(2)
		  difference = parseFloat(difference);
		  
		  if(difference > 0.0){
			  this.onError('amount.tendered.is.less');
			  return false;
		  }
		  
		
		  if(difference < 0.0){
			  this.onError('amount.tendered.is.more');
			  return false;
		  }
		  
		  if(paymentAmt == 0.0){
			  this.onError('please.enter.amt');
			  return false;
		  }
		  
		  if(chequeAmount != 0.0 || this.chequeNumberTextField.value != ''){
			/* cheque validation */
			if(chequeAmount == 0.0){
				this.onError('enter.cheque.amt');
				return false;
			}
			
			if(chequeAmount < 0.0){
				this.onError('invalid.cheque.amt');
				return false;
			}
			
			if(this.chequeNumberTextField.value == ''){
				this.onError('invalid.cheque.number');
				return false;
			}				
		  }				  
		  
		  
		  if(cardAmount > 0.0 || this.trackDataTextField.value != '' || this.cardNumberTextField.value != ''){
			/* card validation */ 
			if(cardAmount == 0.0){
				this.onError('enter.card.amt');
				return false;
			}
			
			if(cardAmount < 0.0){
				this.onError('invalid.card.amt');
				return false;
			}
			
			if(this.swipeCardCheckBox.checked){
				/*card was swipped*/ 
				if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
					this.onError('card.not.swiped');
					return false;
				}
			  }
			  else{
				  /*no card environment*/ 
				  if(this.cardNumberTextField.value == null || this.cardNumberTextField.value == ''){
					  this.onError('js.enter.card.no');
					  return false;
				  }
				  
				  if(this.cardExpiryDateTextField.value == null || this.cardExpiryDateTextField.value == ''){
					  this.onError('js.enter.card.expiry');
					  return false;
				  }
					
				  
			  }
			
		  }	
		  
		  if(voucherAmount != 0.0 || this.voucherNoTextField.value != ''){
				/* voucher validation */
				if(voucherAmount == 0.0){
					this.onError('enter.voucher.amt');
					return false;
				}
				
				if(voucherAmount < 0.0){
					this.onError('invalid.voucher.amt');
					return false;
				}
				
				if(this.voucherNoTextField.value == ''){
					this.onError('enter.order.no');
					return false;
				}				
			  }	
		  if (cardAmount > 0.0 && externalCardAmount > 0.0)
		  {
			  this.onError("can't.use.both.credit.card.and.external.card");
			  return false;
		  }
		  
		  this.setPayment();
		  
		  return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	
	  	payment.set('cashAmount', this.cashAmountTextField.value);
	  	payment.set('cardAmount', this.cardAmountTextField.value);
	  	payment.set('externalCardAmount', this.externalCardAmountTextField.value);
	  	payment.set('chequeAmount', this.chequeAmountTextField.value);
	  	
	  	payment.set('chequeNumber', this.chequeNumberTextField.value);
	  	
	  	payment.set('isCardPresent', this.swipeCardCheckBox.checked);
	  	/*payment.set('cardType',this.cardSelectList.value);*/
	  	
	  	if(this.swipeCardCheckBox.checked){
	  		payment.set('trackData',this.trackDataTextField.value);		  		
	  	}
	  	else{	  		
	  		payment.set('cardNumber',this.cardNumberTextField.value);
	  		payment.set('cardExpiryDate',this.cardExpiryDateTextField.value);
	  		payment.set('cardCvv',this.cardCvvTextField.value);
	  		payment.set('cardHolderName',this.cardHolderNameTextField.value);
	  		payment.set('streetAddress',this.streetAddressTextField.value);
	  		payment.set('zipCode',this.zipCodeTextField.value);
	  	}	  
	  	
	  	payment.set('voucherAmt', this.voucherAmountTextField.value);
	  	payment.set('voucherNo', this.voucherNoTextField.value);
	  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* OnCredit Panel */
var OnCreditPanel =  Class.create(CheckoutPanel, {
	  initialize: function() {
		$super('onCredit.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('onCredit.popup.okButton');
	    this.cancelBtn = $('onCredit.popup.cancelButton');
	    
	    /*textfields*/
	    
	    
	    /*labels*/
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	  },  
	  okHandler:function(){	  
		  this.hide();
	  },
	  resetHandler:function(){
	  },
	  onShow:function(){
	  },
	  setPayment:function(){
	  	var payment = new Hash();	  	
		  	this.paymentHandler(payment);
	  },
	  paymentHandler:function(payment){}
});

/* PIN Panel */
var PINPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('pin.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('pin.popup.okButton');
	    this.cancelBtn = $('pin.popup.cancelButton');
	    
	    /*textfields*/
	    this.pinTextField = $('pin.popup.pinTextField');	    
	    Event.observe(this.pinTextField,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.pinTextField.panel = this;
	    this.pinTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};
	    
	  }, 
	  validate:function(){
	  	if(this.pinTextField.value == null || this.pinTextField.value == ''){
			this.onError('invalid.pin');
			this.pinTextField.focus();
			return;
		}
		
	  	var url = 'OrderAction.do?action=validatePIN&pin=' + this.pinTextField.value;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});	
	  },
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.pinTextField.value = '';
	  },
	  onShow:function(){
	  	this.pinTextField.focus();
	  },
	  onComplete:function(request){		  
	  },
	  onSuccess:function(request){	  		
  		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
  		
  		if(result.error){
  			this.onError(result.error);
  			this.pinTextField.focus();
  			return;
  		}
  		
  		var roleId = result.roleId;
		var discountLimit = result.discountLimit;
		var overrideLimit = result.overrideLimit;
		var discountOnTotal = result.discountOnTotal;
		var discountUptoPriceLimit = result.discountUptoPriceLimit;	
		var allowOrderBackDate = result.allowOrderBackDate;	
		var allowUpSell = result.allowUpSell;
	  	
	  	var discountRights = new DiscountRights(discountLimit, overrideLimit, discountUptoPriceLimit, discountOnTotal, allowOrderBackDate, allowUpSell);
	  	
	  	/* notify discount manager */
	  	DiscountManager.setDiscountRights(discountRights);
	  	
		this.hide();
	  },
	  onFailure:function(request){	
		  this.onError("failed.to.process.request");
		  this.pinTextField.focus();
	  }
});


/*Delivery Panel*/
var DeliveryPanel =  Class.create(CheckoutPanel, {
  initialize: function() {
	this.createPopUp($('delivery.popup.panel'));
    
    /*buttons*/
    this.nowBtn = $('delivery.popup.now.button');    
    this.nowBtn.onclick = this.setDeliveryNow.bind(this);
    
    this.laterBtn = $('delivery.popup.later.button');    
    this.laterBtn.onclick = this.setDeliveryLater.bind(this);
    
    this.closeBtn = $('delivery.popup.close.button');
    this.closeBtn.onclick = this.hide.bind(this);
    
    this.okBtn = $('delivery.popup.ok.button');
    this.okBtn.onclick = this.okHandler.bind(this);
    
    /*panels*/
    this.confimationPanel = $('delivery.popup.confirmation.panel');
    this.datePanel = $('delivery.popup.date.panel');
    
    this.dateTextField = $('delivery.popup.date.textfield');
    this.dateButton = $('delivery.popup.date.button');
    this.dateButton.onclick = this.schedule.bind(this);
    
    this.paymentTermDropDown = $('payment-term-dropdown');
    this.paymentTermId = this.paymentTermDropDown.options[this.paymentTermDropDown.selectedIndex].value;
    this.paymentTermDropDown.onchange = this.setPaymentTerm.bind(this); 
    
    this.calendar = Calendar.setup({"inputField":this.dateTextField, 
    	"ifFormat":"%Y-%m-%d",
    	"daFormat":"%Y-%m-%d",
    	"button":this.dateButton});
  }, 
  
  resetHandler:function(){
	  this.deliveryRule = 'O';
	  this.confimationPanel.style.display = 'block';
	  this.datePanel.style.display = 'none';
	  this.dateTextField.value = "";
	  this.nowBtn.checked = false;
	  this.laterBtn.checked = false;
  },
  
  onShow:function(){
	  this.deliveryRule = 'O';
	  
	  var bpPaymentTermId = BPManager.getBP().getPaymentTermId();
	  
	  var options = this.paymentTermDropDown.options;
	  
	  for(var i=0; i<options.length; i++)
	  {
		  if (options[i].value == bpPaymentTermId)
		  {
			  options[i].selected = "selected";
			  break;
		  }
	  }
  },
  
  setDeliveryNow:function(){
	  if (this.nowBtn.checked)
	  {
		  this.deliveryRule = 'O';
		  this.laterBtn.checked = false;
	  }
	  else
	  {
		  this.laterBtn.checked = true;
		  this.setDeliveryLater();
	  }
  },
  
  setDeliveryLater:function(){
	  if (this.laterBtn.checked)
	  {
		  this.deliveryRule = 'M';
		  this.nowBtn.checked = false;
		  this.confimationPanel.style.display = 'none';
		  this.datePanel.style.display = 'block';
	  }
	  else
	  {
		  this.nowBtn.checked = true;
		  this.setDeliveryNow();
	  }
  },
  
  schedule:function(){
	  this.deliveryRule = 'M';
	  this.deliveryDate = this.dateTextField.value + ' 00:00:00';
  },
  
  setPayment:function(){
  	/*set payment*/
	var payment = new Hash();
	payment.set('deliveryRule',this.deliveryRule);
	
	if (this.deliveryRule == 'M')
	{
		this.deliveryDate = this.dateTextField.value + ' 00:00:00';
	}
	
	payment.set('deliveryDate',this.deliveryDate);
	payment.set('paymentTermId', this.paymentTermId);
	this.payment = payment;
	
  	this.hide();
	this.paymentHandler(this.payment);
  },
  
  paymentHandler:function(payment){},
  
  okHandler:function(){
	  this.setPayment();
  },
  
  setPaymentTerm:function(){
	  this.paymentTermId = this.paymentTermDropDown.options[this.paymentTermDropDown.selectedIndex].value;
  }
  
});

/*XWeb Panel*/
var XWebPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('xweb.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('xweb.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('xweb.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getOTK&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*Mercury Panel*/
var MercuryPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('mercury.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('mercury.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('mercury.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getMercuryOTK&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*ElementPS Panel*/
var ElementPSPanel =  Class.create(PopUpBase, {
  initialize: function() {
	this.createPopUp($('elementps.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('elementps.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('elementps.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getElementPSTransactionId&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*Merchant Warehouse Genius Panel*/
var MerchantWarehouseGeniusPanel =  Class.create(PopUpBase, {
  initialize: function() {
	this.createPopUp($('merchant.warehouse.genius.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('merchant.warehouse.genius.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('merchant.warehouse.genius.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getTransactionKeyGeniusDevice&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});


/*MerchantWarehouse Panel*/
var MerchantWarehousePanel =  Class.create(CheckoutPanel, {
	initialize: function($super) {
	$super('merchant.warehouse.popup.panel');
    
	/*buttons*/
    this.cancelBtn = $('merchant.warehouse.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('merchant.warehouse.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getTransactionKey&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/* Payment panel */
var PaymentPopUp = Class.create(PopUpBase, {
	initialize: function() {
	    this.createPopUp($('payment.popup.panel'));
	    
	    /*buttons*/
	   // this.okBtn = $('payment.popup.okButton');
	    this.cancelBtn = $('payment.popup.cancelButton');
	    
	    this.cashBtn = $('payment.popup.cashPaymentButton');
	    this.cardBtn = $('payment.popup.cardPaymentButton');
	    this.chequeBtn = $('payment.popup.chequePaymentButton');
	    this.mixBtn = $('payment.popup.mixPaymentButton');
	    this.onCreditBtn = $('payment.popup.onCreditPaymentButton');
	    
	    /* external credit card processing */
	    this.externalCreditCardMachine = $('payment.popup.externalCreditCardMachineButton');
	    
	    /* voucher */
	    this.voucherBtn = $('payment.popup.voucherPaymentButton');
	    
	    /* e2e */
	    this.e2eCardBtn = $('payment.popup.e2eCardPaymentButton');
	    
	    /* gift card */
	    this.giftCardBtn = $('payment.popup.giftCardPaymentButton');
	    
	    /* SK wallet */
	    this.SKWalletBtn = $('payment.popup.SKWalletPaymentButton');
	    
	    /* Zapper */
	    this.ZapperBtn = $('payment.popup.ZapperPaymentButton');
	    
	    /* Loyalty */
	    this.LoyaltyBtn = $('payment.popup.loyaltyPaymentButton');
	    
	    /* MCB Juice */
	    this.MCBJuiceBtn = $('payment.popup.MCBJuicePaymentButton');
	    
	    /* MY.T Money */
	    this.MYTMoneyBtn = $('payment.popup.MYTMoneyPaymentButton');
	    
	    /* Emtel Money */
	    this.EmtelMoney = $('payment.popup.EmtelMoneyPaymentButton');
	    
	    /* Gifts.mu */
	    this.GiftsMuBtn = $('payment.popup.GiftsMuPaymentButton');
	    
	    /* MIPS */
	    this.MipsBtn = $('payment.popup.MipsPaymentButton');
	    	    
	    /* add behaviour */	
		for(var field in this){
			var fieldContents = this[field];
			
			if (fieldContents == null || typeof(fieldContents) == "function") {
				continue;
		    }
		    
		    if(fieldContents.type == 'button'){
		    	//register click handler
		    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
		    }
		}
		
		 /*override events*/
	    this.cancelBtn.onclick = this.cancelHandler.bind(this);
	   // this.okBtn.onclick = this.okHandler.bind(this); 
	},
	okHandler:function(){	  
		if(this.tenderType == null) this.tenderType = TENDER_TYPE.CASH;		
		this.hide();
	},
	cancelHandler:function(){
		this.tenderType = null;
		this.isPaymentOnline = true;
		this.hide();
	},
	onShow:function(){
		
		this.tenderType = TENDER_TYPE.CASH;
		
		if(this.cashBtn){
			this.cashBtn.focus();
		}		
		
		if(this.LoyaltyBtn){
			if(BPManager.getBP().enableLoyalty == 'N'){
				this.LoyaltyBtn.style.display = "none";
			}
			else{
				this.LoyaltyBtn.style.display = "block";
			}
		}
		
	},
	onHide:function(){
		if(this.tenderType == null) return;	
		
		OrderScreen.setTenderType(this.tenderType);
		OrderScreen.getPaymentDetails();
	},
	clickHandler:function(e){
		var element = Event.element(e);
		
		var count = 0;
		while(!(element instanceof HTMLButtonElement)){
			count++;
			if(count > 10){
				alert(Translation.translate('fail.to.get.event.source','Fail to get event source!'));
				break;
			} 
			element = element.parentNode;
		}	
		
		switch(element){
			case this.cashBtn:
				this.tenderType = TENDER_TYPE.CASH;
				break;
			
			case this.cardBtn:
				this.tenderType = TENDER_TYPE.CARD;
				OrderScreen.setIsPaymentOnline(true);
				break;
				
			case this.chequeBtn:
				this.tenderType = TENDER_TYPE.CHEQUE;
				break;
				
			case this.mixBtn:
				if(!confirm('Do you want to proceed with Mix Payment?')) return;
				this.tenderType = TENDER_TYPE.MIXED;
				break;
				
			case this.onCreditBtn:
				this.tenderType = TENDER_TYPE.CREDIT;
				break;
		
			case this.MCBJuiceBtn:
				if(!confirm('Do you want to proceed with MCB Juice?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.MCB_JUICE;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.MYTMoneyBtn:
				if(!confirm('Do you want to proceed with MY.T Money?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.MY_T_MONEY;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.EmtelMoney:
				if(!confirm('Do you want to proceed with Blink?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.EMTEL_MONEY;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.GiftsMuBtn:
				if(!confirm('Do you want to proceed with Gifts.mu?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.GIFTS_MU;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.MipsBtn:
				if(!confirm('Do you want to proceed with MIPS?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.MIPS;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.externalCreditCardMachine:
				if(!confirm('Do you want to proceed with External Card Machine?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.EXTERNAL_CARD;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.voucherBtn:
				this.tenderType = TENDER_TYPE.VOUCHER;
				break;
				
			case this.e2eCardBtn:
				this.tenderType = TENDER_TYPE.E2E_CARD;
				break;
				
			case this.giftCardBtn:
				this.tenderType = TENDER_TYPE.GIFT_CARD;
				break;
				
			case this.SKWalletBtn:
				this.tenderType = TENDER_TYPE.SK_WALLET;
				break;
				
			case this.ZapperBtn:
				this.tenderType = TENDER_TYPE.ZAPPER;
				break;
				
			case this.LoyaltyBtn:
				this.tenderType = TENDER_TYPE.LOYALTY;
				break;
				
			/**/
			case this.MCBJuiceBtn:
				this.tenderType = TENDER_TYPE.MCB_JUICE;
				break;
			
			case this.MYTMoneyBtn:
				this.tenderType = TENDER_TYPE.MY_T_MONEY;
				break;
				
			case this.EmtelMoneyBtn:
				this.tenderType = TENDER_TYPE.EMTEL_MONEY;
				break;
			
			case this.MipsBtn:
				this.tenderType = TENDER_TYPE.MIPS;
				break;
			
			case this.GiftsMuBtn:
				this.tenderType = TENDER_TYPE.GIFTS_MU;
				break;
				
			default:break;
		}
		
		this.okHandler();
	}
});


/*Voucher Panel*/
var VoucherPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('voucher.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('voucher.popup.okButton');
	    this.cancelBtn = $('voucher.popup.cancelButton');
	    
	    /*textfields*/
	    this.voucherNoTextField = $('voucher.popup.voucherNoTextField');
	    
	    /*labels*/
	    this.totalLabel = $('voucher.popup.totalLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*others*/
	    this.voucherNoTextField.panel = this;
	    this.voucherNoTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('invalid.voucher.number');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    
	    this.organisationSelectList = $('voucher-popup-dropdown');	 
		
		var terminals = TerminalManager.terminalList;
		var organisations = new Array();
  
		/* this.organisationSelectList.options.length = 0; */
  
		for(var i=0; i<terminals.length; i++)
		{
			var terminal = terminals[i];
			var orgName = terminal.org_name;
			var orgId = terminal.org_id;
		
			if(organisations.indexOf(orgId) == -1){
				organisations.push(orgId);
				this.organisationSelectList.options[this.organisationSelectList.options.length] = new Option(orgName, orgId, false, false)					
			}   	   			
		}
	  
		var orgId = TerminalManager.terminal.orgId;
		this.organisationSelectList.setValue(orgId);
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.voucherNoTextField.value = '';
	  },
	  onShow:function(){
	  	this.voucherNoTextField.focus();
	  },
	  validate:function(){		  
		  
		  if(this.voucherNoTextField.value == null || this.voucherNoTextField.value == ''){
			  this.onError('invalid.voucher.number');
			  this.voucherNoTextField.focus();
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('voucherAmt', this.cartTotalAmount);
	  	payment.set('voucherNo', this.voucherNoTextField.value);
	  	payment.set('voucherOrgId', this.organisationSelectList.value);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});

/*Loyalty Panel*/
var LoyaltyPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('loyalty.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('loyalty.popup.okButton');
	    this.cancelBtn = $('loyalty.popup.cancelButton');	    
	    
	    /*labels*/
	    this.totalLabel = $('loyalty.popup.totalLabel');
	    this.loyaltyAvailableLabel = $('loyalty.popup.loyaltyAvailableLabel');
	    this.loyaltyRemainingLabel = $('loyalty.popup.loyaltyRemainingLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    var cart = ShoppingCartManager.getCart();	
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.loyaltyPoints = BPManager.bp.loyaltyPoints - cart.promotionPointsTotal;
	    this.loyaltyStartingPoints = BPManager.bp.loyaltyStartingPoints;
	    
	    this.loyaltyAvailableLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.loyaltyPoints).toFixed(2);
	    this.loyaltyRemainingLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.loyaltyPoints - this.cartTotalAmount).toFixed(2);
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	/*nothing*/
	  },
	  onShow:function(){
	  	/*nothing*/
	  },
	  validate:function(){	
		  
		  var diff = parseFloat(this.loyaltyPoints) - parseFloat(this.cartTotalAmount);
		  
		  if(diff < 0.0){
			  this.onError('Loyalty points is less than order total! Loyalty points:' 
					  + new Number(this.loyaltyPoints).toFixed(2) + ' Order total:' + new Number(this.cartTotalAmount).toFixed(2));			  
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('loyaltyAmt', this.cartTotalAmount);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});


var EditOnFlyPanel = Class.create(PopUpBase, {
	initialize: function() {
    this.createPopUp($('edit-on-fly-panel'));
    
    this.product = null;
    
    /*buttons*/
    this.okBtn = $('edit-on-fly-ok-button');
    this.cancelBtn = $('edit-on-fly-cancel-button');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    this.okBtn.onclick = this.okHandler.bind(this); 
    
    /*textfields*/
    this.priceTextfield = $('edit-on-fly-price-textfield');
    this.descriptionTextfield = $('edit-on-fly-description-textfield');    
	},

	resetHandler:function(){
	  	this.priceTextfield.value = '';
	  	this.descriptionTextfield.value = '';
	  	
	  	this.priceTextfield.disabled = false;
	  	this.descriptionTextfield.disabled = false;
	},
	
	onShow:function(){		
		
		if(this.product.editpriceonfly == 'Y'){
			this.priceTextfield.value = this.product.pricestd || '';
			this.priceTextfield.disabled = false;
			//$('edit-on-fly-price-row').show();
			this.priceTextfield.select();
		}
		else{
			this.priceTextfield.disabled = true;
			this.descriptionTextfield.select();
			//$('edit-on-fly-price-row').hide();
		}
			
		if(this.product.editdesconfly == 'Y'){
			this.descriptionTextfield.value = '';
			this.descriptionTextfield.disabled =false;
			//$('edit-on-fly-description-row').show();
		}
		else{
			this.descriptionTextfield.disabled =true;
			//$('edit-on-fly-description-row').hide();
		}
		
	},
	
	okHandler:function(){
		var price = this.priceTextfield.value;
		var description = this.descriptionTextfield.value;		
		shoppingCart.addToCart(this.product.m_product_id,1, description, price);
		this.hide();
	},
	
	
});


/*e2e Panel*/
var E2EPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('e2e.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('e2e.popup.okButton');
	    this.cancelBtn = $('e2e.popup.cancelButton');
	    
	    /*textfields*/ 
	    this.trackDataTextField = $('e2e.popup.trackDataTextField');
	    
	    /*labels*/
	    this.totalLabel = $('e2e.popup.totalLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.trackDataTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.card');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.trackDataTextField.value = '';
	  },
	  onShow:function(){
	  	this.trackDataTextField.focus();
	  },
	  validate:function(){
		  
		  if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
			  this.onError('card.not.swiped');
			  this.trackDataTextField.focus();
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('cardAmt', this.cartTotalAmount);
	  	payment.set('cardTrackDataEncrypted', this.trackDataTextField.value);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});


/*Gift Card Panel*/
var GiftCardPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('gift.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('gift.popup.okButton');
	    this.cancelBtn = $('gift.popup.cancelButton');
	    
	    /*textfields*/ 
	    this.trackDataTextField = $('gift.popup.trackDataTextField');
	    this.numberTextField = $('gift.popup.numberTextField');
	    /*this.cvvTextField = $('gift.popup.cvvTextField');*/
	    
	    /*labels*/
	    this.totalLabel = $('gift.popup.totalLabel');
	    
	    /*radios*/
	    this.swipeRadio = $('gift.popup.swipeRadio');
	    this.manualRadio = $('gift.popup.manualRadio');
	    
	    /*panels*/
	    this.swipePanel = $('gift.popup.swipePanel');
	    this.manualPanel = $('gift.popup.manualPanel');
	    
	    /*add behaviour to components*/
	    this.swipeRadio.onclick = this.radioHandler.bind(this);
	    this.manualRadio.onclick = this.radioHandler.bind(this);
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.trackDataTextField.onkeypress = function(e){	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.card');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    
	    this.numberTextField.onkeypress = function(e){	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('card.number.required');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    	    
	  }, 
	  
	  radioHandler:function(e){
		  var element = Event.element(e);
		  
		  this.trackDataTextField.value = '';
		  this.numberTextField.value = '';
		  /*this.cvvTextField.value = '';*/
		  
		  switch (element) {
		  
			case this.swipeRadio:	
				this.manualPanel.style.display = 'none';
				this.swipePanel.style.display = '';
				break;
				
			case this.manualRadio:	
				this.swipePanel.style.display = 'none';
				this.manualPanel.style.display = '';				
				break;
	
			default:
				break;
		  }
	  },
	  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  
	  resetHandler:function(){
	  	this.trackDataTextField.value = '';
	  	this.numberTextField.value = '';
	  	/*this.cvvTextField.value = "";*/
	  	
	  	this.swipeRadio.checked = true;
	  	
	  	this.manualPanel.style.display = 'none';
		this.swipePanel.style.display = '';
	  },
	  
	  onShow:function(){	
		  if(this.swipeRadio.checked)
		  {
			  this.trackDataTextField.focus();
		  }
		  else
		  {
			  this.numberTextField.focus();
		  }	  	
	  },
	  
	  validate:function(){
		  
		  if(this.swipeRadio.checked == true)
		  {
			  if(this.trackDataTextField.value == null || this.trackDataTextField.value.trim() == ""){
				  this.onError('card.not.swiped');
				  this.trackDataTextField.focus();
				  return;
			  }
		  }
		  else
		  {
			  if(this.numberTextField.value == null  || this.numberTextField.value.trim() == ""){
				  this.onError('card.number.required');
				  this.numberTextField.focus();
				  return;
			  }
			  
			  if (isNaN(this.numberTextField.value))
			  {
				  this.onError('card.number.should.contain.only.numeric.values');
				  this.numberTextField.focus();
				  return;
			  }
		  }		  
		  
		  this.setPayment();
		  
		return true;
	  },
	  
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('giftCardAmt', this.cartTotalAmount);
	  	payment.set('giftCardTrackData', this.trackDataTextField.value);
	  	payment.set('giftCardNo', this.numberTextField.value);
	  	/*payment.set('giftCardCVV', this.cvvTextField.value);*/	  	
	  	this.payment = payment;
	  },
	  
	  paymentHandler:function(payment){}
	});

/*Century Card Panel*/
var CenturyPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('century.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('century.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('century.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getCenturyPaymentForm&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*UsaEpay Card Panel*/
var UsaEpayPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('usaepay.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('usaepay.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('usaepay.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getUsaEpayPaymentForm&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/* apply payment processor popup*/
var ApplyPaymentProcessorPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('apply.payment.processor.popup'));
	    
	    /*buttons*/
	    this.applyPaymentProcessorBtn = $('apply.payment.processor.Btn');
	    this.applyPaymentProcessorCloseBtn = $('apply.payment.processor.popup.close.button');
	    this.applyPaymentProcessorCloseBtn.onclick = this.hide.bind(this);
	  
	    this.applyPaymentProcessorBtn.onclick = function(e){	    	
	    	window.location = 'PaymentProcessorAction.do?action=initPaymentRequestSupport'
	    };
	  
	  }  
	  
	});

/* external credit card popup*/
var ExtCreditCardPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('external-credit-card-popup'));
	    /*buttons*/
	    this.applyPaymentProcessorBtn = $('external-credit-card-apply-payment-processor-btn');
	    this.proceedBtn = $('external-credit-card-proceed-btn');
	    this.applyPaymentProcessorCloseBtn = $('external-credit-card-popup-close-button');
	    this.applyPaymentProcessorCloseBtn.onclick = this.hide.bind(this);
	    this.proceedBtn.onclick = this.okHandler.bind(this);
	    
	    this.applyPaymentProcessorBtn.onclick = function(e){	    	
	    	window.location = 'PaymentProcessorAction.do?action=initFormPaymentProcessor'
	    };
	        
	  },
	  
	  paymentHandler:function(payment){
		  
	  },
	  
	  okHandler:function(){ 
		  this.hide();
		  this.paymentHandler(this.payment);
	  }
	});

/*Create Instance Product Popup Panel*/
var CreateInstanceProductPanel =  Class.create(PopUpBase, {
	  initialize : function(parentProduct)
	  {
		this.createPopUp($('create-instance-product-popup'));
		
	    /*buttons*/
		this.chooseExistingBtn = $('create-instance-product-popup-choose-existing-btn');
	    this.saveBtn = $('create-instance-product-popup-save-button');
	    this.closeBtn = $('create-instance-product-popup-close-button');
	    this.addParentToCartBtn = $('create-instance-product-popup-add-parent-to-cart-button');
	    this.saveBtn.onclick = this.save.bind(this);
	    this.closeBtn.onclick = this.hide.bind(this);
	    this.chooseExistingBtn.onclick = this.chooseExistingSerialNo.bind(this);
	    this.parentProduct = parentProduct;
	    this.m_product_parent_id = parentProduct.m_product_id;
	    this.serialNoTextField = $('create-instance-product-popup-serialNo-textField');
	    this.serialNoTextFieldContainer = $('serial-no-textField-container');
	    this.serialNoPopUpContainer =  $('serial-no-popup-container');
	    this.serialNoMessage = $('serial-no-message');
	    this.noOfChildren = this.parentProduct.childrenQty;
	    this.parentQty = this.parentProduct.parentQty;
	    this.qtyOnHand = this.parentProduct.qtyOnHand;
	    this.productListJSON = null;
	    
	    this.resetValues();
	  },
	  
	  save : function()
	  { 
		  if (this.serialNoTextField.value == '' || this.serialNoTextField.value == null)
		  {
			  new AlertPopUpPanel(Translation.translate('serial.no.is.mandatory','Serial No is mandatory!')).show();
			  return;
		  }
		  
		  var url = 'ProductAction.do';
		  var params = 'action=createInstanceProduct&m_product_parent_id=' + this.m_product_parent_id + '&serialNo=' + this.serialNoTextField.value
		  				+ '&orderType=' + OrderScreen.orderType;
		  
		  	new Ajax.Request( url,{ 
		  		method: 'post', 
				parameters: params,
				onSuccess: this.onSuccess.bind(this), 
				onFailure: this.onFailure.bind(this)
			});	
		  	
		  	this.hide();

	  },
	  
	  onSuccess : function(request)
	  {
		  var response = eval('('+request.responseText+')');
		  
		  if (response.success)
		  {
			  SearchProduct.onSelect(response.product);
			  SearchProduct.filterQuery = 'm_product.m_product_id=' + this.m_product_parent_id;
			  SearchProduct.param.set('isSerialNoSearch', false);
			  SearchProduct.search();
		  }
		  else
		  {
			  new AlertPopUpPanel(response.error).show();
		  }
	  },
	  
	  onFailure : function()
	  {
		  
	  },
	  
	  chooseExistingSerialNo : function()
	  {
		  this.hide();
		  SearchProduct.render(this.productListJSON);
	  },
	  
	  addParentToCart : function()
	  {
		  this.hide();
		  SearchProduct.onSelect(this.parentProduct);
	  },
	  
	  displayButtons : function()
	  {
		  this.resetValues();
		  
		  if (((OrderScreen.orderType == 'stock-transfer-send' 
			  || OrderScreen.orderType == 'stock-transfer-request') && this.parentQty > this.noOfChildren) 
			  || OrderScreen.orderType == 'POS Goods Receive Note')
	      {
		    	this.addParentToCartBtn.style.display='block';
		    	this.addParentToCartBtn.onclick = this.addParentToCart.bind(this);
	      }
		  
		  //For purchase we can generate serial no even if parent qty is zero
		  
		  if (OrderScreen.orderType == 'POS Goods Receive Note')
		  {
			  this.saveBtn.style.display = 'block';
			  this.serialNoTextFieldContainer.style.display = 'block';
		  }
		  
		  if (this.parentQty > 0)
		  {
			//Parent with no child created yet
    		this.saveBtn.style.display = 'block';
    		this.serialNoTextFieldContainer.style.display = 'block';
		  }
		  
		  if (this.noOfChildren == this.qtyOnHand)
		  {
			  this.serialNoMessage.innerHTML = 'All Serial Nos have already been generated.';
			  this.serialNoMessage.style.display = 'block';
			  
			  if (this.noOfChildren > 0)
              {
                  this.chooseExistingBtn.style.display = 'block';
              }		  
		  }
		  
		  if (this.noOfChildren > 0)
		  {
			  this.chooseExistingBtn.style.display = 'block';
		  }
		  
		  if (this.parentQty == 0 && this.noOfChildren == 0 && OrderScreen.orderType != 'POS Goods Receive Note')
		  {
			  this.serialNoMessage.innerHTML = 'All Serial Nos have already been generated and sold.';
			  this.serialNoMessage.style.display = 'block';
		  }
		  
		  
		  /*if (this.noOfChildren == 0)
	      {
		    	//this.chooseExistingBtn.style.display = 'none';
		    	
		    	if (this.parentProduct.qtyonhand > 0)
		    	{
		    		//Parent with no child created yet
		    		this.saveBtn.style.display = 'block';
		    		this.serialNoTextFieldContainer.style.display = 'block';
		    	}
	      }
		  else
	      {
		    	this.chooseExistingBtn.style.display = 'block';
		    	
		    	if (this.noOfChildren == this.parentProduct.qtyonhand && OrderScreen.orderType != 'POS Goods Receive Note')
		    	{
		    		//We cannot create more child
		    		//this.saveBtn.style.display = 'none';
		    		this.serialNoMessage.innerHTML = 'All Serial Nos have already been generated';
		    		this.serialNoMessage.style.display = 'block';
		    		//this.serialNoTextFieldContainer.style.display = 'none';
		    	}
		    	else
	    		{
		    		//We can create more child
		    		this.saveBtn.style.display = 'block';
		    		this.serialNoTextFieldContainer.style.display = 'block';
		    	}
	      }*/
		  
		  this.serialNoPopUpContainer.style.display = 'block';
	  },
	  
	  resetValues : function()
	  {
		  this.saveBtn.style.display = 'none';
		  this.serialNoTextFieldContainer.style.display = 'none';
		  this.serialNoMessage.innerHTML = '';
		  this.chooseExistingBtn.style.display = 'none';
		  this.serialNoMessage.style.display = 'none';
		  this.addParentToCartBtn.style.display = 'none';
		  this.serialNoTextField.value = '';
	  }
});


/*Norse Card Panel*/

var NorseCardPanel1 =  Class.create(PopUpBase, {
	  initialize: function() {
		    
		this.createPopUp($('norse-popup-panel'));	    
	    /*buttons*/
	    this.okBtn = $('norse-popup-process-button');
	    this.cancelBtn = $('norse-popup-cancel-button');
	    
	    /*textfields*/ 	
	    this.cardnumberTextField = $('norse-popup-cardnumber-textfield');
	    this.cardholderTextField = $('norse-popup-cardholder-textfield');
	    this.cvvTextField = $('norse-popup-cvv-textfield');
	    
	    /*select*/
	    this.expmonthSelect = $('norse-popup-month-select');
	    this.expyearSelect = $('norse-popup-year-select');
	    
	    /*labels*/
	    this.titleLabel = $('norse-popup-title');
	    
	    /*radios*/
	    this.swipeRadio = $('norse-popup-swipe-radio');
	    this.manualRadio = $('norse-popup-manual-radio');	    
	    
	    /*add behaviour to components*/
	    this.swipeRadio.onclick = this.radioHandler.bind(this);
	    this.manualRadio.onclick = this.radioHandler.bind(this);
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.titleLabel.innerHTML = (OrderScreen.isSoTrx ? 'Sales ' : 'Refund ') + this.currencySymbol + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.cardnumberTextField.panel = this;
	    this.cardnumberTextField.onkeypress = function(e){	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.card');
					return;
				}
				
				/* parse data */
				  var swipe = this.value;
				  this.value = "";
				  
				  if(swipe.length > 16){
					  var swipeData = new SwipeParserObj(swipe);					  
					  
					  if(!swipeData.hasTrack1){
						  alert(Translation.translate('Difficulty Reading Card Information! Please swipe.card Again!','Difficulty Reading Card Information! Please Swipe Card Again!'));
						  this.panel.cardnumberTextField.focus();
						  return;
					  }
					  
					  this.panel.cardnumberTextField.value = swipeData.account;
					  this.panel.cardholderTextField.value = swipeData.account_name
						  
					  Form.Element.setValue(this.panel.expmonthSelect, swipeData.exp_month);
					  Form.Element.setValue(this.panel.expyearSelect, swipeData.exp_year.substr(2)); 				  
					  			  
				  }
				  
				  this.panel.okHandler();
			}	    	
	    }
	    
	  }, 
	  
	  radioHandler:function(e){
		  var element = Event.element(e);
		  
		  this.cardnumberTextField.value = '';
		  this.cardholderTextField.value = '';
		  this.cvvTextField.value = '';
		  
		  this.expmonthSelect.options[0].selected = true;
		  this.expyearSelect.options[0].selected = true;
		  
		  switch (element) {
		  
			case this.swipeRadio:
				this.cardholderTextField.disabled = true;
				this.expmonthSelect.disabled = true;
				this.expyearSelect.disabled = true;
				this.cvvTextField.disabled = true;
				
				this.cardnumberTextField.type = 'password';
				
				break;
				
			case this.manualRadio:	
				this.cardholderTextField.disabled = false;
				this.expmonthSelect.disabled = false;
				this.expyearSelect.disabled = false;
				this.cvvTextField.disabled = false;
				
				this.cardnumberTextField.type = 'text';
				
				break;
	
			default:
				break;
		  }
		  
		  this.cardnumberTextField.focus();
	  },
	  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  
	  resetHandler:function(){
		  this.cardnumberTextField.value = '';
		  this.cardholderTextField.value = '';
		  this.cvvTextField.value = '';
		  
		  this.expmonthSelect.options[0].selected = true;
		  this.expyearSelect.options[0].selected = true;
		  
		  this.swipeRadio.checked = true;
		  
		  this.cardholderTextField.disabled = true;
		  this.expmonthSelect.disabled = true;
		  this.expyearSelect.disabled = true;
		  this.cvvTextField.disabled = true;
	  },
	  
	  onShow:function(){
		this.resetHandler();
	  	this.cardnumberTextField.focus();
	  },
	  
	  validate:function(){
		  
		  if(this.cardnumberTextField.value == null || this.cardnumberTextField.value == ''){
			  
			  if(this.swipeRadio.checked){
				  alert(Translation.translate('card.not.swiped','Card not swiped!'));
			  }
			  else
			  {
				  alert(Translation.translate('enter.card.number','Enter Card Number!'));
			  }
			 
			  this.cardnumberTextField.focus();
			  return false;
		  }	
		  
		  if(this.cardholderTextField.value  == null || this.cardholderTextField.value == ''){			  
			  alert(Translation.translate('Enter name on Card!','Enter Name on Card!'));
			  this.cardholderTextField.focus();
			  return false;
		  }
		  
		  /* validate expiry */
		  var currentYear = new Date().getFullYear();
		  var currentMonth = new Date().getMonth() + 1;
		  
		  var month = this.expmonthSelect.value;
		  month = parseInt(month);
		  
		  var year = this.expyearSelect.value; 
		  
		  if(year === currentYear && month < currentMonth){
			  alert(Translation.translate('CreditCard has expired!','CreditCard has expired!'));
			  this.expmonthSelect.focus();
			  return false;
		  }		  
		  
		  this.setPayment();
		  
		return true;
	  },
	  
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('isCardPresent', this.swipeRadio.checked);
	  	payment.set('cardAmt', this.cartTotalAmount);
	  	payment.set('cardNumber',this.cardnumberTextField.value);
	  	
	  	var expiryDate = this.expmonthSelect.value + this.expyearSelect.value; 
  		payment.set('cardExpiryDate',  expiryDate);
  		
  		payment.set('cardCvv',this.cvvTextField.value);
  		payment.set('cardHolderName',this.cardholderTextField.value);
  		
  		payment.set('cardType', 'M');
	  	
	  	this.payment = payment;
	  },
	  
	  paymentHandler:function(payment){}
	});


/*NorseCardPanel Panel*/
var NorseCardPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('norse.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('norse.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('norse.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getNorsePaymentForm&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

var LineItemPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('itemLinePanel'));
	    
	    this.applyBtn = $('line-item-panel-apply-button');
	    this.cancelBtn = $('line-item-panel-cancel-button');
	    
	    this.quantityTextField = $('quantity-texfield');
	    
	    this.decreaseButton = $('decrease-button');
	    this.increaseButton = $('add-button');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.applyBtn.onclick = this.applyHandler.bind(this); 
	    
	    this.decreaseButton.onclick = this.decreaseQty.bind(this);
	    this.increaseButton.onclick = this.increaseQty.bind(this);
	    
	    var keypadButtons = $$('#itemLinePanel .keypad-button');
        for(var i=0; i<keypadButtons.length; i++){
            var btn = keypadButtons[i];
            btn.onclick = this.keypadHandler.bind(this);
        }
        
        this.quantityTextField.onkeyup = function(e)
	    {
 			if (isNaN(this.value))
			{
 				this.value = '';
				return;
			}
        }
	  },
	    
	    
	    
	  applyHandler:function(){
		  
		  this.validate();
		  
		  var quantity = parseFloat(this.quantityTextField.value);
		  ShoppingCartManager.updateQty(quantity);
		  this.hide();
	  },
	  
	  onShow:function(){
		  this.quantityTextField.focus();
		  this.quantityTextField.select();
	  },
	  
	  validate:function(){	
		  var quantity = parseFloat(this.quantityTextField.value);
		  if(isNaN(quantity) || (quantity < 0.0)){
			  alert(Translation.translate('invalid.amt','Invalid amount'));
			  return false;
		  }
	  },
	  
	  keypadHandler:function(e){
          var button = Event.element(e); 
          var element = button.getElementsByClassName('keypad-value')[0];
          
          /*Fix for google chrome*/
          if (element == null)
          {
          		element = button;  
          }
          /**********************/
	          
          var value = element.innerHTML;
         // var value = element.innerHTML;
                       
          var quantity = this.quantityTextField.value;
          
          	if (this.quantityTextField.selectionStart != this.quantityTextField.selectionEnd)
			{	
          		quantity = '';
				this.quantityTextField.value = '';
				this.quantityTextField.focus();
			}
          
          
			if('Del' == element.innerHTML.strip()){
				
				if(quantity.length > 0)
	        		  quantity = quantity.substring(0,quantity.length-1);
          }
			else if('-' == element.innerHTML.strip()){
				
				if(quantity.indexOf('-') == -1){
					quantity = '-' + quantity;
				}
				else{
					quantity = quantity.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(quantity.indexOf('.') == -1)
				{
					quantity = quantity + value;
				}
			}
			else{
				quantity = quantity + value;
			}
			
			this.quantityTextField.value = quantity;
			
           
            if(isNaN(quantity) || (quantity < 0.0)){           
                return;
            }
           
            if(quantity != 0.0 && !isNaN(quantity)){
            	quantity = parseFloat(quantity);
            }
      	},
      	
      	increaseQty : function()
      	{
      		var quantity = this.quantityTextField.value;
				
			if (!isNaN(quantity))
			{
				quantity = parseFloat(quantity) + 1;
				this.quantityTextField.value = quantity;
			}
			
			if(isNaN(quantity) || (quantity < 0.0)){           
                return;
            }
           
            if(quantity != 0.0 && !isNaN(quantity)){
            	quantity = parseFloat(quantity);
            }
      	},
      	
      	decreaseQty : function()
      	{
      		var quantity = this.quantityTextField.value;
				
			if (!isNaN(quantity))
			{
				quantity = parseFloat(quantity) - 1;
				this.quantityTextField.value = quantity;
			}
			
			if(isNaN(quantity) || (quantity < 0.0)){  
				this.quantityTextField.value = 0;
                return;
            }
           
            if(quantity != 0.0 && !isNaN(quantity)){
            	quantity = parseFloat(quantity);
            }
      	}
	});

var ChangeTaxPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('change-tax-pop-up'));
		this.taxDropdown = $('tax-dropdown');
		this.shoppingCart = null;
		
		this.okBtn = $('change-tax-pop-up-ok-button');
		this.cancelBtn = $('change-tax-pop-up-cancel-button');
		
        /*this.taxDropdown.onselect = this.onChangeHandler.bind(this);*/
        this.okBtn.onclick = this.okHandler.bind(this);
        this.taxDropdown.onkeyup = this.closePanel.bind(this);
        this.cancelBtn.onclick = this.hide.bind(this);
        this.taxDropdown.focus();
        
	  },
	  
	  onChangeHandler : function()
	  {
		  /*var taxId = this.taxDropdown.value;
		  ShoppingCartManager.setLineTax(taxId);
		  this.taxDropdown.focus();*/
	  },
	  
	  okHandler : function()
	  {
		  this.setTax();
		  this.hide();
	  },
	  
	  setTax : function()
	  {
		  var cart = ShoppingCartManager.getCart();
		  var lines = cart.lines;
		  
		  if(lines.length > 0)
		  {
		        var currentLine  = lines[cart.selectedIndex];
		        var taxId = currentLine.taxId;
		        var newTaxId = this.taxDropdown.value;
		        ShoppingCartManager.setLineTax(newTaxId);
	      }		 
	  },
	  
	  setShoppingCart : function(shoppingCart)
	  {
		  this.shoppingCart = shoppingCart;
		  this.taxDropdown.focus();
	  },
	  
	  closePanel : function(e)
	  {
		  if (e.keyCode == Event.KEY_RETURN)
		 {
			  this.hide();
		 }
	  },
	  
	  onShow : function(){
		  var cart = ShoppingCartManager.getCart();
		  var lines = cart.lines;
		  if(lines.length > 0)
		  {
		        var currentLine  = lines[cart.selectedIndex];
		        var taxId = currentLine.taxId;
		        jQuery('#tax-dropdown').val(taxId);     
	      }
	  }
	  
});

/* modifier popup */
var SelectModifierPanel = Class.create(PopUpBase, {
    initialize: function() {
    this.createPopUp($('select-modifier-panel'));
   
    this.product = null;
   
    /*buttons*/
    this.okBtn = $('select-modifier-ok-button');
    this.cancelBtn = $('select-modifier-cancel-button');
   
    this.cancelBtn.onclick = this.hide.bind(this);
    this.okBtn.onclick = this.okHandler.bind(this);
    },

    resetHandler:function(){
         
    },
   
    renderSelectModifierList : function(){
       
        var display = $('select-modifier-panel-modifiers');
        display.innerHTML = "";
       
        var groupsAvailable = this.groupsAvailable;
       
        for(var i=0; i<groupsAvailable.length; i++){
            var groupId = groupsAvailable[i].groupId;               
            var btns = jQuery("div#" + groupId + " > button");
           
            for(var j=0; j<btns.length; j++){
                var btn = btns[j];
               
                if(btn.selected){
                    display.innerHTML += "<div>" + jQuery(btn).attr('name') + "</div>";
                }
            }
        }
    },
   
    loadModifiers : function(product){
        var product_id = product.m_product_id;
        this.product_id = product_id;
       
        /*
        var MODIFIER_GROUPS = window.parent.frames[1].MODIFIER_GROUPS;
        var MODIFIER_MAPPINGS = window.parent.frames[1].MODIFIER_MAPPINGS;
        */
        
        var mg = sessionStorage.getItem('MODIFIER_GROUPS');
		var mm = sessionStorage.getItem('MODIFIER_MAPPINGS');
		
		var MODIFIER_GROUPS = JSON.parse(mg);
		var MODIFIER_MAPPINGS = JSON.parse(mm);
       
        var groups = {};
        for(var j=0; j<MODIFIER_GROUPS.length; j++){
            var group = MODIFIER_GROUPS[j];
            groups[group.groupId] = group;
        }
       
        this.groups = groups;
        var groupsAvailable = new Array();
       
        $('select-modifier-panel-product').innerHTML = product.name;
       
        var modifierGroupsContainer  = $('select-modifier-panel-modifier-groups-container');
        modifierGroupsContainer.innerHTML = "";
       
        var html = "";
       
        for(var i=0; i< MODIFIER_MAPPINGS.length; i++){
            var mapping = MODIFIER_MAPPINGS[i];
           
            if(mapping.product_id == product_id){
                var group_id = mapping.group_id;
                var group = groups[group_id];
                
                if(group == null) continue;
               
                groupsAvailable.push(group);
               
                html += "<div class='modifier-group'>";
                /* html += "<div class='modifier-group-header'>" + group.groupName + "</div>"  */
                html += "<div class='modifier-group-header'> Choose below: </div>"                 
               
                html += "<div class='modifier-buttons' id='" + group_id + "'>";
               
                var groupLines = group.lines;
                for(var j=0; j<groupLines.length; j++){
                    var groupLine = groupLines[j];
                                   
                    html += "<button class='modifier-button btn btn-lg btn-primary' group_id='" + group_id + "' groupline_id='" + groupLine.groupLineId + "' name='" + groupLine.name + "' product_id='" + groupLine.productId + "' price='" + groupLine.price + "'>" + groupLine.name + "</button>";
                }
               
                html += "</div>";
                html += "</div>";
            }
        }
       
        this.groupsAvailable = groupsAvailable;
       
        modifierGroupsContainer.innerHTML = html;
       
        var btns = $$('button.modifier-button');
       
        for(var i=0; i<btns.length; i++){
            var btn = btns[i];
            btn.panel = this;
           
            btn.onclick = function(){
                var modifier = jQuery(this);
                var groupId = modifier.attr('group_id');
                var groupLineId = modifier.attr('groupline_id');
                var name = modifier.attr('name');
                var productId = modifier.attr('product_id');
                var price = modifier.attr('price');
               
                var groups = this.panel.groups;
                var group = groups[groupId];
               
                if(group.isExclusive == 'Y'){
                    /* select only one */
                    /* reset all buttons */
                    var btns = jQuery("div#" + groupId + " > button");
                    for(var i=0; i<btns.length; i++){
                        jQuery(btns[i]).removeClass('selected');
                        btns[i].selected = false;
                    }
                }
               
                if(this.selected){
                    this.selected = false;
                    modifier.removeClass('selected');
                }
                else{
                    this.selected = true;
                    modifier.addClass('selected');
                }
               
                this.panel.renderSelectModifierList();                   
            };
        }
    },
   
    onShow:function(){           
        var display = $('select-modifier-panel-modifiers');
        display.innerHTML = "";
    },
   
    okHandler:function(){
        /*
        var price = this.priceTextfield.value;
        var description = this.descriptionTextfield.value;       
        shoppingCart.addToCart(this.product.m_product_id,1, description, price);       
        */
       
        var groupsAvailable = this.groupsAvailable;
        var modifiers = new Array();
       
        for(var i=0; i<groupsAvailable.length; i++){
           
            var groupId = groupsAvailable[i].groupId;
            var isMandatory = groupsAvailable[i].isMandatory;
           
            var btns = jQuery("div#" + groupId + " > button");
           
            var isOk = false;
           
            for(var j=0; j<btns.length; j++){
                var btn = btns[j];
               
                if(btn.selected){
                    isOk = true;
                   
                    var modifier = jQuery(btn);
                    var groupId = modifier.attr('group_id');
                    var groupLineId = modifier.attr('groupline_id');
                    var name = modifier.attr('name');
                    var productId = modifier.attr('product_id');
                    var price = modifier.attr('price');
                   
                    var obj = {};
                    obj.modifierGroupId = groupId;
                    obj.modifierId = groupLineId;
                    obj.name = name;
                    obj.productId = productId;
                    obj.price = price;
                   
                    modifiers.push(obj);
                }
            }
           
            if(isMandatory == 'Y' && !isOk){
                alert(groupsAvailable[i].groupName + ' option is required!');
                return;
            }
        }
       
        shoppingCart.addToCart(this.product_id,1, null, null, modifiers);
                   
        this.hide();
    }      
   
});

var CreateCustomerPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('create-customer-popup-panel'));
	 	
	 	var ref = this;
	 	
	 	var form = jQuery("#create-customer-popup-panel").find("#form")[0];
	 	form.reset();
	 	
	 	jQuery("#create-customer-popup-panel").find(".address").hide();
	 	jQuery("#create-customer-popup-panel").find("#add-address-btn").off('click').on("click", function(){
	 		
	 		var divs = jQuery("#create-customer-popup-panel").find(".address");
	 		
	 		if(divs[0].style.display == 'block') return;
	 		
	 		divs.show();
	 		
	 		//build country list
	 		var countryList = jQuery("#create-customer-popup-panel").find("#country")[0];
	 		countryList.onchange = function(){
	 			
	 			var option = this.options[this.selectedIndex];
	 			
	 			var regions = jQuery(option).attr('regions');
	 			regions = JSON.parse(regions);
	 			
	 			var regionList = jQuery("#create-customer-popup-panel").find("#state")[0];
	 			regionList.options.length = 0;
	 			
	 			var html = "<option value='0'></option>";
	 			var region;
	 			
	 			for(var i=0; i<regions.length; i++){
	 				
	 				region = regions[i];
	 				
	 				if(region['c_region_id'] == 0) continue;
	 				
	 				html += "<option value='" + region['c_region_id'] + "'>" + region['c_region_name'] + "</option>";
	 				
	 			}
	 			
	 			regionList.innerHTML = html;
	 			
	 		};
	 		
	 		var select = jQuery(countryList);
	 		
	 		if(countryList.options.length == 0){
	 			
	 			console.log("Customer Popup: Building country dropdown ..");	 			
	 			
	 			jQuery.getJSON( "DataTableAction.do?action=getCountries", function( data ) {
	 				
	 				var country;
	 				var option;
	 				
	 				for( var i=0; i<data.length; i++) {
	 					
	 					country = data[i];
	 					option = jQuery("<option value='" + country['c_country_id'] +"'>" + country['c_country_name'] + "</option>");
	 					
	 					option.attr('regions', Object.toJSON(country['regions']));
	 					
	 					select.append(option);
	 				}
	 				
	 				select.val(TerminalManager.terminal.orgInfo.countryId);
	 				
	 			});
	 			
	 			
	 			
	 		}
	 		
	 		select.val(TerminalManager.terminal.orgInfo.countryId);
	 		
	 	});
	 	
	 	
	 	jQuery("#create-customer-popup-panel").find("#close-button").off('click').on("click", function(){
	 		
	 		ref.hide();
	 		
	 	});
	 	
	 	jQuery("#create-customer-popup-panel").find("#save-button").off('click').on("click", function(){
	 		
	 		var form = jQuery("#create-customer-popup-panel").find("#form")[0];
	 		
	 		var name = form.name.value;
	 		var identifier = form.identifier.value;
	 		var email = form.email.value;
	 		var emailReceipt = form.emailReceipt.checked;
	 		
	 		var phone = form.phone.value;
	 		var address = form.address.value;
	 		var city = form.city.value;
	 		var postalcode = form.postalcode.value;
	 		var country = 0;
	 		var state = 0;
	 		
	 		if( form.country.options.length > 0 )
	 		{
	 			country = form.country.options[form.country.selectedIndex].value;
	 		}
	 		
	 		if( form.state.options.length > 0 )
	 		{
	 			state = form.state.options[form.state.selectedIndex].value;
	 		}
	 		
	 		if(identifier == ""){
	 			
	 			alert("Customer ID is required!");
	 			form.identifier.focus();
	 			
	 			return;
	 		}
	 		
	 		if(name == ""){
	 			
	 			alert("Name is required!");
	 			form.name.focus();
	 			
	 			return;
	 		}
	 		
	 		if(name == "" && identifier == "")
	 		{
	 			alert("Name / Customer ID is required!");
	 			form.name.focus();
	 		}
	 		else
	 		{
	 			if(name == "")
	 			{
	 				//use identifier as name
	 				name = identifier;
	 			}
	 			
	 			var model = {
	 			"value" : '',
				"title" : 'Mr',
				"name" : '',
				"email" : '',
				"phoneNo" : '',
				"address" : '',
				"city" : '',
				"postal" : '',
				"mobileNo" : '',
				"countryId" : '',
				"regionId" : 0,
				"gender" : 'Male',
				"dob" : '',
				"custom1" : '',
				"custom2" : '',
				"custom3" : '',
				"custom4" : '',
				"notes" : '',
				"emailReceipt" : false
	 			};
				
	 			
	 			model.name = name || '';
	 			model.identifier = identifier || '';
	 			model.email = email || '';
	 			model.phoneNo = phone || '';
	 			model.countryId = country || TerminalManager.terminal.orgInfo.countryId;
	 			model.regionId = state || 0;
	 			model.value = model.identifier;
	 			
	 			model.address = address || '';
	 			model.city = city || '';
	 			model.postal = postalcode || '';
	 			model.emailReceipt = emailReceipt || false;
	 			
	 			var postData = {'json' : Object.toJSON(model)};
	 			var url = "BPartnerAction.do?action=createCustomer";
	 			
	 			jQuery.post(url,
	 					postData,
	 		    		function(json, textStatus, jqXHR){
	 		    			
	 		    			if(json == null || jqXHR.status != 200){
	 		    				alert('Failed to save: session timed out!');
	 		    				return;
	 		    			}
	 		    			
	 		    			if(json.error){
	 		    				alert(json.error);
	 		    				return;
	 		    			}
	 		    			else
	 		    			{
	 		    				var id = json['c_bpartner_id'];	
	 		    				console.log( json );
	 		    				
	 		    				var bp = new BP(json);
	 		    				BPManager.setBP(bp);
	 		    				
	 		    				jQuery('#search-bp-textfield').val(bp.getName());
	 		    				OrderScreen.setBPartnerId(bp.getId());
	 		    				
	 		    				ref.hide();
	 		    			}
	 		    			
	 		    		},
	 					"json").fail(function(json, textStatus, jqXHR){
	 						
	 						if (json.status == 401)
	 						{
	 							jQuery.globalEval(json.responseText);
	 						}
	 						else
	 						{
	 							alert("Failed to process request");
	 						}
	 						
	 					}).done(function() {
	 							 						
	 		    			
	 					}).always(function() {
	 						 						
	 						
	 					});
	 			
	 			
	 		}	 		
	 		
	 	});
	},
	
	onShow : function(){
		
		var form = jQuery("#create-customer-popup-panel").find("#form")[0];
		form.name.focus();
	}
});

var MoreOptionsPanels = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('more-option-popup-panel'));
	 	this.cancelBtn = $('more-option-popup-cancelButton');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);	 	 	
	 	
	},
	
	onShow : function()
	{
		
	},
	
	cancelHandler: function()
	{
		this.hide();
	}
});

var RedeemPromotionPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('redeem.promotion.popup.panel'));
	 	this.cancelBtn = $('redeem.promotion.popup.cancelButton');
	 	this.closeBtn = $('redeem.promotion.popup.closeButton');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);
	 	this.closeBtn.onclick = this.cancelHandler.bind(this);
	},
	
	onShow : function()
	{
		 var currencySymbol = OrderScreen.getCurrencySymbol();
		 var cart = ShoppingCartManager.getCart();
		    
		 var points = BPManager.bp.loyaltyPoints - cart.promotionPointsTotal;
		 var customer = BPManager.bp.name;
		 		    
		jQuery("#redeem-promotion-popup span.customer").html(customer);
		jQuery("#redeem-promotion-popup span.points").html(points);
		
		var html = "";
		var promotion = null;
		
		var popup = this;
		
		jQuery.getJSON("DataTableAction.do?action=getPromotions&date=" + DateUtil.getCurrentDate(), function(json){
			
			var promotions = json['promotions'];
			var disabled = false;
			
			var today = moment().hour(0).minute(0).second(0);			
			
       	  	var expiry = null;
       	  	var isExpired = false;
       	  	
			for( var i=0; i<promotions.length; i++ )
			{				
				promotion = promotions[i];
				
				expiry = promotion['expirydate'];
				expiry = moment(expiry, "DD-MM-YYYY");
				
				isExpired = today.unix() > expiry.unix();
				
				if(isExpired) continue; // do not show
				
				disabled = promotion['points'] > points || isExpired;
				
				//console.log("promotion['points'] > points " + promotion['points'] + " > " + points + " ==> " + (promotion['points'] > points));
				//console.log("today.unix() > expiry.unix() " + today.unix() + " > " + expiry.unix() + " ==>" + (today.unix() > expiry.unix()));
				//console.log("disabled " + disabled);
				
				html += "<tr>" 
					+ "<td>" + promotion['description'] + "</td>"
					+ "<td>" + promotion['amount'] + "</td>"
					+ "<td>" + promotion['points'] + " points</td>"
					+ "<td>" + expiry.format("DD-MMM-YYYY") + "</td>"
					+ "<td><button style='width:100%;' class='btn " + ((disabled) ? "" : "btn-success") + "' " + ((disabled) ? "disabled='disabled'" : "") + " id='" + promotion['u_promotion_id'] + "'>Redeem</button></td>"
				+ "</tr>";
			}
			
			jQuery("#redeem-promotion-popup tbody.promotions").html(html);
			
			var buttons = jQuery("#redeem-promotion-popup tbody.promotions button");
			var button = null;
			
			for( var j=0; j<buttons.length; j++)
			{
				button = jQuery(buttons[j]);
				button.on('click', function(){
					
					var promotion_id = jQuery(this).attr('id');
					
					jQuery.post("PromotionAction.do",
						  {
						    action : "redeemPromotion",
						    "promotion_id" : promotion_id,
						    "date": DateUtil.getCurrentDate()
						  },
						  function(data,status,xhr){
							  
							popup.hide();
							 
						    if(status != "success"){
						    	alert(Translation.translate("failed.to.redeem.promotion","Failed to redeem points!"));
						    }
						    else
						    {
						    	jQuery('#shopping-cart-container').html(data);	    	
						    }					     
						    
						  });					
					
				});
				
			}
		});
		
		
		
		
	},
	
	cancelHandler: function()
	{
		this.hide();
	}	
	
});

var BatchAndExpiryPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('batch.expiry.popup.panel'));
	 	this.cancelBtn = $('batch.expiry.popup.cancelButton');
	 	this.closeBtn = $('batch.expiry.popup.closeButton');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);
	 	this.closeBtn.onclick = this.cancelHandler.bind(this);
	},
	
	onShow : function()
	{	
		var html = "";
		var promotion = null;
		
		var popup = this;
		var product = this.product;
		
		var m_product_id = product["m_product_id"];
		var m_warehouse_id = TerminalManager.terminal.warehouseId;
		
		jQuery.getJSON(`ProductAction.do?action=getLotAndExpiry&m_product_id=${m_product_id}&m_warehouse_id=${m_warehouse_id}`, function(json){
			
			var lots = json['data'];
			var disabled = false;
			
			var today = moment().hour(0).minute(0).second(0);			
			
       	  	var expiry = null;
       	  	var isExpired = false;
       	  	
       	  	var lot;
			for( var i=0; i<lots.length; i++ )
			{				
				lot = lots[i];
				
				expiry = lot['expirydate'];
				expiry = moment(expiry, "YYYY-MM-DD HH:mm:ss");
				
				isExpired = today.unix() > expiry.unix();
				
				if(isExpired) continue; // do not show
				
				//console.log("promotion['points'] > points " + promotion['points'] + " > " + points + " ==> " + (promotion['points'] > points));
				//console.log("today.unix() > expiry.unix() " + today.unix() + " > " + expiry.unix() + " ==>" + (today.unix() > expiry.unix()));
				//console.log("disabled " + disabled);
				
				html += "<tr>" 
					+ "<td>" + lot['lot'] + "</td>"
					+ "<td>" + expiry.format("DD-MMM-YYYY") + "</td>"
					+ "<td>" + lot['qty'] + "</td>"
					+ "<td><button style='width:100%;' class='btn btn-success' id='" + lot['m_attributesetinstance_id'] + "'>Add</button></td>"
				+ "</tr>";
			}
			
			jQuery("#batch-expiry-popup tbody.promotions").html(html);
			
			var buttons = jQuery("#batch-expiry-popup tbody.promotions button");
			var button = null;
			
			for( var j=0; j<buttons.length; j++)
			{
				button = jQuery(buttons[j]);
				button.on('click', function(){
					
					var m_attributesetinstance_id = jQuery(this).attr('id');
					
					popup.hide();
					
					//add to shoppingcart
					console.log(`add product ${m_product_id} m_attributesetinstance_id ${m_attributesetinstance_id}`);
					
					shoppingCart.addToCart(m_product_id, 1, null, null, null, null, true, m_attributesetinstance_id);
					
				});
				
			}
		});
		
		
		
		
	},
	
	cancelHandler: function()
	{
		this.hide();
	}	
	
});



var OfflineMarketingPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('offline-marketing-pop-up'));
		
		this.tryNowBtn = $('offline-marketing-pop-up-try-now-button');
		this.cancelBtn = $('offline-marketing-pop-up-cancel-button');

	    this.doNotShowCheckbox = $('offline-marketing-pop-up-check-box');
	    
		this.tryNowBtn.onclick = this.tryNowHandler.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
      
	  },
	  
	  tryNowHandler : function()
	  {
		  //save oprion donotshow into cache if checked
		  if (this.doNotShowCheckbox.checked) {
			
			  localStorage.setItem('POSTERITA_MARKETING', true);
		  }
		  else
		  {
			  sessionStorage.setItem('POSTERITA_MARKETING', true);
		  }

		  //redirect the user to the page to try the posterita offline
		  getURL('offline-instructions.do');
		  
		  this.hide();
	  },
	  
	  closePanel : function(e)
	  {
		  if (e.keyCode == Event.KEY_RETURN)
		 {
			//save oprion donotshow into cache if checked
			  if (this.doNotShowCheckbox.checked) {
					
				  localStorage.setItem('POSTERITA_MARKETING', true);
			  }
			  else
			  {
				  sessionStorage.setItem('POSTERITA_MARKETING', true);
			  }
			  
			  this.hide();
		 }
	  },
	  
	  onShow : function(){
		  this.tryNowBtn.focus();
	  },
	  
	  onHide : function(){

		if (this.doNotShowCheckbox.checked) {
					
		  localStorage.setItem('POSTERITA_MARKETING', true);
	    }
		else
		{
			sessionStorage.setItem('POSTERITA_MARKETING', true);
		}

	  }
	  
});

/* Product Info Popup Panel */
var ProductInfoPopup =  Class.create(PopUpBase, {
	
	  appendHTML : function() {
		  
		  var html = ' <div class="modal" style="display: block; visibility: visible; left: 0px; top: 10px; z-index: 100000;" id="product-info-popup-panel" tabindex="-1" role="dialog" aria-labelledby="product-info-popup-panel" aria-hidden="true"> ' +
			' 	<div class="modal-dialog modal-md"> ' +
			' 		<div class="modal-content"> ' +
			' 			<div class="modal-header"> ' +
			' 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="product-info-popup-cancelButton">×</button> ' +
			' 				<h4 class="modal-title"> ' +
			' 					<div class="glyphicons glyphicons-pro search"></div> ' +
			' 					&nbsp;' + Translation.translate('product.info') +
			' 				</h4> ' +
			' 			</div> ' +
			' 			<div class="modal-body"> ' +
			' 				<div class="row"> ' +
			' 					<div class="input-group float-left input-sm"> ' +
			//' 						<input type="text" class="form-control textField input-sm" placeholder="Enter barcode or name" id="product-info-popup-textfield"> ' +
			'<div class="input-group"><input autocomplete="off" type="text" class="form-control" id="product-info-popup-textfield" placeholder="Enter barcode or name"><span class="input-group-btn"><button class="btn btn-success" type="button" id="product-info-popup-okButton">Search</button></span></div>' +
			
			
			' 					</div> ' +
			' 				</div> ' +
			'				<div class="row" id="product-info-popup-panel-result"></div>' +
			' 			</div> ' +
			' 			<div class="modal-footer"> ' +
			' 				' +
			' 			</div> ' +
			' 		</div> ' +
			' 	</div> ' +
			' </div> ';
		  
		jQuery(document.body).append(html);
	  },
	
	  initialize: function() {
		
		if(jQuery('#product-info-popup-panel').length == 0) {
			
			this.appendHTML();
		}
		
		
	    this.createPopUp($('product-info-popup-panel'));
	    
	    /*buttons*/
	    this.okBtn = $('product-info-popup-okButton');
	    this.cancelBtn = $('product-info-popup-cancelButton');
	    
	    /*textfields*/
	    this.searchTermtextfield = $('product-info-popup-textfield');		    
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.searchTermtextfield.panel = this;
	    this.searchTermtextfield.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};		
	    
	  }, 
	  validate:function(){
	  	if(this.searchTermtextfield.value == null || this.searchTermtextfield.value == ''){
			this.onError('please.enter.search.criteria');
			this.searchTermtextfield.focus();
			return;
		}
		
	  	var url = 'ProductAction.do?action=search&searchTerm=' + this.searchTermtextfield.value + '&isSoTrx=' + OrderScreen.isSoTrx + '&bpPricelistVersionId=&warehouseId=' + SearchProduct.warehouseId + '&ignoreTerminalPricelist=false&filterQuery=' ;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});	
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.searchTermtextfield.value = '';
	  },
	  onShow:function(){
	  	this.searchTermtextfield.focus();
	  },
	  /* ajax callbacks */
	  onComplete:function(request){		  
	  },
	  onSuccess:function(request){	  	
  
  			var responseText = request.responseText.evalJSON(true);
			var results = responseText.products;
			
			//filter modifiers
			var products = [];
			
			for(var i=0; i<results.length; i++){
				var record = results[i];
				if(record.ismodifier == 'Y') continue;
				
				products.push(record);
			}
			
			
			if (products.length == 0)
			{
				this.onError('no.product.found');
			}
			else 
			{
				var container = jQuery("#product-info-popup-panel-result");
				
				var html = "<table class='table table-striped table-scroll'><thead><tr><th>Name/Description</th><th style='text-align: right;'>Stock in Hand</th></tr></thead><tbody>";
				
				for(var i=0; i<products.length; i++)
				{
					var product = products[i];
					
					html = html + "<tr><td><a href='javascript:void(0);' onclick='ProductInfoPopup_ProductHandler(" + product.m_product_id + ")'>" + product.name + "<br>" + product.description + "</a></td><td style='text-align: right; width:20%;'>" + product.qtyonhand + "</td></tr>"
					
				}
				
				html = html + "</tbody></table>";
				
				container.html( html );
			
			}
			
			if(!responseText.success){
				/* this.hide(); */
				alert(result.error);
				return;
			}  		
	  },
	  onFailure:function(request){	
		  alert("failed.to.process.request");
	  }
});

var ProductInfoPopup_ProductHandler = function(product_id){
	PopUpManager.activePopUp.hide();
	var panel = new ProductInfoPanel();
	panel.onHide = function(){
		new ProductInfoPopup().show();
	};
	panel.getInfo(product_id);
};
