var POPriceListTab = Class.create(Tab, {
	
	initialize : function($super, tabId, divId)
	{
		$super(tabId, 'POPriceListPanel', divId);
		this.selectComp = null;
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['vendor.pricelist.general.help'];
		
		this.selectComp = document.createElement('select');
		this.selectComp.setAttribute('class','price-list-select-dimen input-sm form-text');
		var priceLists = json.priceLists;
		for (var i=0; i<priceLists.length; i++)
		{
			var priceList = priceLists[i];
			var id = priceList.id;
			var name = priceList.name;
			
			var selected = priceList.selected;
			
			var option = document.createElement('option');
			option.setAttribute('value', id);
			option.innerHTML = name;
			if (selected)
			{
				option.setAttribute('selected', true);
			}
			
			this.selectComp.appendChild(option);
		}
		
		$('po.price.list.component').innerHTML = '';
		$('po.price.list.component').appendChild(this.selectComp);
	},
	
	afterOk : function(json)
	{
		$('partner.summary.priceListName').innerHTML = json.priceListName;
		$('partner.summary.priceListName').style.display= 'block';
	},
	
	getParameters : function()
	{
		var priceListId = this.selectComp.value;
		var parameters = "{'priceListId':"+ priceListId +"}";
		return parameters;
	}
});


var SOPriceListTab = Class.create(Tab, {
	
	initialize : function($super, tabId, divId)
	{
		$super(tabId, 'SOPriceListPanel', divId);
		this.selectComp = null;
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['customer.pricelist.general.help'];
		
		this.selectComp = document.createElement('select');
		this.selectComp.setAttribute('class','input-sm form-text');
		var priceLists = json.priceLists;
		for (var i=0; i<priceLists.length; i++)
		{
			var priceList = priceLists[i];
			var id = priceList.id;
			var name = priceList.name;
			
			var selected = priceList.selected;
			
			var option = document.createElement('option');
			option.setAttribute('value', id);
			option.innerHTML = name;
			if (selected)
			{
				option.setAttribute('selected', true);
			}
			
			this.selectComp.appendChild(option);
		}
		
		$('so.price.list.component').innerHTML = '';
		$('so.price.list.component').appendChild(this.selectComp);
	},
	
	afterOk : function(json)
	{
		$('partner.summary.priceListName').innerHTML = json.priceListName;
		$('partner.summary.priceListName').style.display= 'block';
	},
	
	getParameters : function()
	{
		var priceListId = this.selectComp.value;
		var parameters = "{'priceListId':"+ priceListId +"}";
		return parameters;
	}
});


var CreditManagementTab = Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'CreditManagementPanel');
		
		this.creditStatusComp = document.createElement('select');
		this.creditStatusComp.setAttribute('class','credit-status-select-dimen input-sm form-text');
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['credit.management.general.help'];
		
		this.creditStatusComp.innerHTML = '';
		var cs = json.creditStatus;
		for (var i=0; i<cs.length; i++)
		{
			var creditS = cs[i];
			var value = creditS.value;
			var display = creditS.display;
			var selected = creditS.selected;
			
			var option = document.createElement('option');
			option.setAttribute('value', value);
			option.innerHTML = display;
			if (selected)
			{
				option.setAttribute('selected', true);
			}
			
			this.creditStatusComp.appendChild(option);
		}
		
		$('credit.status').innerHTML = '';
		$('credit.status').appendChild(this.creditStatusComp);
		
		$('credit.limit').value = json.creditLimit;
		$('credit.watch').value = json.creditWatch;
	},
	
	afterOk : function(json)
	{
		$('partner.summary.creditStatus').innerHTML = json.creditStatus;
		$('partner.summary.creditLimit').innerHTML = json.creditLimit;
		$('partner.summary.creditWatch').innerHTML = json.creditWatch;
	},
	
	getParameters : function()
	{
		var creditStatus = this.creditStatusComp.value;
		var creditLimit = $('credit.limit').value
		var creditWatch = $('credit.watch').value
		
		var h = new Hash();
		h.set('creditStatus', creditStatus);
		h.set('creditLimit', creditLimit);
		h.set('creditWatch', creditWatch);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});

var CustomerAdvancedTab = new Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'CustomerAdvancedPanel');
		
		this.isVendorComp = $('isVendor');
		this.isUserComp = $('isUser');
		this.emailReceiptComp = $('emailReceipt');
		this.emailPromotionComp = $('emailPromotion');
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['customer.advanced.general.help'];
		this.isVendorComp.checked = json.isVendor;
		this.isUserComp.checked = json.isUser;
		this.emailReceiptComp.checked = json.emailReceipt;
		this.emailPromotionComp.checked = json.emailPromotion;
	},
	
	afterOk : function(json)
	{
		if (json.error)
		{
			//$('msg-box').setAttribute('class', 'msg-box-error');
			$('success-msg-box').style.display = 'none';
			$('error-msg-box').style.display = 'block';
			$('ErrorMessages').innerHTML = json.message;
		}
		else
		{
			//$('msg-box').setAttribute('class', 'msg-box-save');
			$('error-msg-box').style.display = 'none';
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.message;
		}
	},
	
	getParameters : function()
	{
		var h = new Hash();
		h.set('isVendor', this.isVendorComp.checked);
		h.set('isUser', this.isUserComp.checked);
		h.set('emailReceipt', this.emailReceiptComp.checked);
		h.set('emailPromotion', this.emailPromotionComp.checked);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});

var CustomerNotesTab = Class.create(Tab , {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'CustomerNotesPanel');
		
		this.notesComp = $('notes');
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['customer.notes.general.help'];
		this.notesComp.value = json.notes;
	},
	
	afterOk : function(json)
	{
		if (json.error)
		{
			//$('msg-box').setAttribute('class', 'msg-box-error');
			$('success-msg-box').style.display = 'none';
			$('error-msg-box').style.display = 'block';
			$('ErrorMessages').innerHTML = json.message;
		}
		else
		{
			//$('msg-box').setAttribute('class', 'msg-box-save');
			$('error-msg-box').style.display = 'none';
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.message;
		}
	},
	
	getParameters : function()
	{
		var h = new Hash();
		h.set('notes', this.notesComp.value);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});