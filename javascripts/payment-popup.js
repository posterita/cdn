jQuery(function () {

	let onReady = function(){
	
		initPaymentManager();

		var receiptJSON = $receiptJSON; 

		PaymentManager.invoiceId = receiptJSON.header.c_invoice_id;
		PaymentManager.orderId = receiptJSON.header.c_order_id;
		PaymentManager.openAmt = receiptJSON.header.openAmt; 
	
	};

	const KEY = 'jsp/includes/payment_popups.jsp?isSoTrx=' + $receiptJSON.header.soTrx;
	const PLACE_HOLDER = '#payment-popups-container'; 

	if(sessionStorage[KEY]){		
		jQuery(PLACE_HOLDER).html(sessionStorage[KEY]);
		onReady();
		
		return;		
	}

	var dlg = new Dialog();
	dlg.show();

	jQuery(PLACE_HOLDER).load(KEY, function(response, status, xhr){		
		if(status == "success" || status == "notmodified"){			
			sessionStorage[KEY] = response;
			onReady();									
		}
		
		dlg.hide();	
		
	});

	/*
	jQuery('#payment-popups-container').load('jsp/includes/payment_popups.jsp?isSoTrx=' + $receiptJSON.header.soTrx, function () {

		initPaymentManager();

		var receiptJSON = $receiptJSON; 

		PaymentManager.invoiceId = receiptJSON.header.c_invoice_id;
		PaymentManager.orderId = receiptJSON.header.c_order_id;
		PaymentManager.openAmt = receiptJSON.header.openAmt; 

	});
	*/
});

var PaymentManager, PaymentPopUp;

function initPaymentManager() {

	var TENDER_TYPE = {
		CARD: 'Card',
		CASH: 'Cash',
		CHEQUE: 'Cheque',
		CREDIT: 'Credit',
		MIXED: 'Mixed',
		VOUCHER: 'Voucher',
		EXTERNAL_CARD: 'Ext Card',
		GIFT_CARD: 'Gift Card',
		SK_WALLET: 'SK Wallet',
		ZAPPER: 'Zapper',
		LOYALTY: 'Loyalty',
		MCB_JUICE: 'MCB Juice',
		MY_T_MONEY: 'MY.T Money',
		EMTEL_MONEY: 'Emtel Money',
		MIPS: 'MIPS',
		GIFTS_MU: 'Gifts.mu'
	};

	var PAYMENT_RULE = {
		CARD: 'K',
		CASH: 'B',
		CHEQUE: 'S',
		CREDIT: 'P',
		MIXED: 'M',
		VOUCHER: 'V',
		EXTERNAL_CARD: 'E',
		GIFT_CARD: 'G',
		SK_WALLET: 'W',
		ZAPPER: 'Z',
		LOYALTY: 'L',
		MCB_JUICE: 'J',
		MY_T_MONEY: 'Y',
		EMTEL_MONEY: 'T',
		MIPS: 'I',
		GIFTS_MU: 'U'

	};


	PaymentManager = {
		orderType: 'POS Order',
		openAmt: 0,
		invoiceId: 0,
		orderId: 0,
		cardAmt: 0,
		tenderType: TENDER_TYPE.CASH,
		currencySymbol: '$',
		getOpenAmt: function () { return this.openAmt },
		getCurrencySymbol: function () { return this.currencySymbol },
		setTenderType: function (tenderType) { this.tenderType = tenderType },
		getPaymentDetails: function () {
			switch (this.tenderType) {

				case TENDER_TYPE.CASH:
					var panel = new CashPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;

				case TENDER_TYPE.MCB_JUICE:
				case TENDER_TYPE.MY_T_MONEY:
				case TENDER_TYPE.EMTEL_MONEY:
				case TENDER_TYPE.MIPS:
				case TENDER_TYPE.GIFTS_MU:
				case TENDER_TYPE.EXTERNAL_CARD:
					/* manual creditcard processing */
					var panel = new ExtCardPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;

				case TENDER_TYPE.CARD:
					var panel = null;

					var cardAmt = prompt("Enter card amount", PaymentManager.openAmt);
					cardAmt = parseFloat(cardAmt);

					if (isNaN(cardAmt)) {
						alert(Translation.translate('invalid.card.amt', 'Invalid card amount!'));
						return;
					}

					if (cardAmt > PaymentManager.openAmt) {
						alert(Translation.translate('card.amt.cannot.be.greater.than.open.amt', 'Card amount cannot be greater than open amount!'));
						return;
					}

					PaymentManager.cardAmt = cardAmt;


					if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_XWeb") {
						panel = new XWebPanel();
					}
					else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_HF") {



						if (ORDER_TYPES.POS_ORDER != this.orderType) {
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions', 'The configured payment processor only supports sales transactions'));
							return;
						}

						/*sales transactions can be negative*/
						if (this.openAmt < 0) {
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions", "The configured payment processor does not support negative sales transactions"));
							return;
						}

						panel = new MercuryPanel();
					}
					else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS") {
						if (ORDER_TYPES.POS_ORDER != this.orderType) {
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions', 'The configured payment processor only supports sales transactions'));
							return;
						}

						/*sales transactions can be negative*/
						if (this.openAmt < 0) {
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions", "The configured payment processor does not support negative sales transactions"));
							return;
						}

						panel = new ElementPSPanel();
					}
					else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_EE") {
						var panel = new E2EPanel();
					}
					else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Century") {
						var panel = new CenturyPanel();
					}
					else {
						panel = new CardPanel();
					}

					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;

				case TENDER_TYPE.CHEQUE:
					var panel = new ChequePanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;


				case TENDER_TYPE.VOUCHER:
					var panel = new VoucherPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;


				case TENDER_TYPE.GIFT_CARD:
					var panel = new GiftCardPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;


				case TENDER_TYPE.LOYALTY:
					var panel = new LoyaltyPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					break;


				case TENDER_TYPE.SK_WALLET:
					SKWallet.showCheckInCustomersDialog();
					break;

				case TENDER_TYPE.ZAPPER:

					if (ORDER_TYPES.POS_ORDER != PaymentManager.orderType) {
						alert(Translation.translate("zapper.only.supports.sales.transactions", "Zapper only supports sales transactions."));
						return;
					}

					/*sales transactions can be negative*/
					if (parseFloat(PaymentManager.openAmt) < 0) {
						alert(Translation.translate("zapper.does.not.support.negative.sales.transactions.", "Zapper does not support negative sales transactions."));
						return;
					}

					var zapperAmt = prompt("Enter Zapper amount", PaymentManager.openAmt);
					zapperAmt = parseFloat(zapperAmt);

					if (isNaN(zapperAmt)) {
						alert(Translation.translate("Invalid payment amt!", "Invalid payment amount!"));
						return;
					}

					if (zapperAmt > PaymentManager.openAmt) {
						alert(Translation.translate("payment.amt.cannot.be.greater.than.open.amt", "Payment amount cannot be greater than open amount!"));
						return;
					}

					PaymentManager.zapperAmt = zapperAmt;

					Zapper.showQRCodeDialog();
					break;

				default:
					break;
			}
		},

		checkout: function () {
		},

		paymentHandler: function (payment) {
			this.payment = payment;
			this.processPayment();
		},

		setIsPaymentOnline: function (isOnline) {
			this.isPaymentOnline = isOnline;
		},

		setCashPayment: function () {
			$('cashAmt').value = this.payment.get('amt');
			$('amountRefunded').value = this.payment.get('amountRefunded');
		},

		setCardPayment: function () {
			$('isPaymentOnline').value = this.isPaymentOnline;

			if (this.payment.get('cardTrackDataEncrypted') != null) {
				$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
				if ($('cardTrackDataEncrypted').value) return;
			}

			if (!this.isPaymentOnline) return;

			var isCardPresent = this.payment.get('isCardPresent');
			$('cardAmt').value = this.payment.get('amt');
			$('cardType').value = this.payment.get('cardType');

			if (isCardPresent) {
				$('cardTrackData').value = this.payment.get('trackData');
			}
			else {
				$('cardNo').value = this.payment.get('cardNumber');
				$('cardExpDate').value = this.payment.get('cardExpiryDate');
				$('cardCVV').value = this.payment.get('cardCvv');
				$('cardholderName').value = this.payment.get('cardHolderName');
				$('cardStreetAddress').value = this.payment.get('streetAddress');
				$('cardZipCode').value = this.payment.get('zipCode');
			}
		},

		setChequePayment: function () {
			$('chequeAmt').value = this.payment.get('amt');
			$('chequeNo').value = this.payment.get('chequeNumber');
		},

		setExternalPayment: function (tenderType) {

			var amt = this.payment.get('amt');
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'false';

			switch (tenderType) {

				case TENDER_TYPE.MCB_JUICE:
					$('mcbJuiceAmt').value = amt;
					break;

				case TENDER_TYPE.MY_T_MONEY:
					$('mytMoneyAmt').value = amt;
					break;

				case TENDER_TYPE.EMTEL_MONEY:
					$('emtelMoneyAmt').value = amt;
					break;

				case TENDER_TYPE.MIPS:
					$('mipsAmt').value = amt;
					break;

				case TENDER_TYPE.GIFTS_MU:
					$('giftsMuAmt').value = amt;
					break;

				case TENDER_TYPE.EXTERNAL_CARD:
					$('externalCardAmt').value = amt;
					break;

				default:
					break;
			}

		},

		setExternalCardPayment: function () {
			$('externalCardAmt').value = this.payment.get('amt');
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'false';
		},

		setVoucherPayment: function () {
			$('voucherAmt').value = this.payment.get('voucherAmt');
			$('voucherNo').value = this.payment.get('voucherNo');
			$('voucherOrgId').value = this.payment.get('voucherOrgId');
		},

		setEECardPayment: function () {
			$('cardAmt').value = this.payment.get('cardAmt');
			$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
		},

		setGiftCardPayment: function () {
			$('giftCardAmt').value = this.payment.get('giftCardAmt');
			$('giftCardTrackData').value = this.payment.get('giftCardTrackData');
			$('giftCardNo').value = this.payment.get('giftCardNo');
			$('giftCardCVV').value = this.payment.get('giftCardCVV');
		},

		setLoyaltyPayment: function () {
			$('loyaltyAmt').value = this.payment.get('amt');;
		},

		setSKWalletPayment: function () {
			$('skwalletAmt').value = this.payment.get('skwalletAmt');
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'true';
		},

		setZapperPayment: function () {
			$('zapperAmt').value = this.payment.get('zapperAmt');
			$('tipAmt').value = this.payment.get('tipAmt');
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'true';
		},

		setOTK: function (otk) {
			$('otk').value = otk;
		},

		reset: function () {
			$('cashAmt').value = '';
			$('isPaymentOnline').value = 'false';
			$('cardAmt').value = '0';
			$('cardType').value = '';
			$('cardTrackData').value = '';
			$('cardTrackDataEncrypted').value = '';
			$('cardNo').value = '';
			$('cardExpDate').value = '';
			$('cardCVV').value = '';
			$('cardholderName').value = '';
			$('cardStreetAddress').value = '';
			$('cardZipCode').value = '';
			$('chequeAmt').value = '0';
			$('chequeNo').value = '';
			$('externalCardAmt').value = '0';
			$('isPaymentOnline').value = '';
			$('voucherAmt').value = '0';
			$('voucherNo').value = '';
			$('voucherOrgId').value = '0';
			$('otk').value = '';

			$("giftCardAmt").value = "0";
			$("giftCardTrackData").value = "";
			$("giftCardNo").value = "";
			$("giftCardCVV").value = "";

			$("loyaltyAmt").value = "0";

			$("mcbJuiceAmt").value = "0";
			$("mytMoneyAmt").value = "0";
			$("emtelMoneyAmt").value = "0";
			$("mipsAmt").value = "0";
			$("giftsMuAmt").value = "0";

			/*
			$("skwalletAmt").value = "0";
			$("zapperAmt").value = "0";
			*/
		},

		processPayment: function () {

			this.reset();

			switch (this.tenderType) {
				case TENDER_TYPE.CASH:
					this.setCashPayment();
					break;

				case TENDER_TYPE.CHEQUE:
					this.setChequePayment();
					break;

				case TENDER_TYPE.CARD:
					this.setCardPayment();
					break;

				case TENDER_TYPE.EXTERNAL_CARD:
				case TENDER_TYPE.MCB_JUICE:
				case TENDER_TYPE.MY_T_MONEY:
				case TENDER_TYPE.EMTEL_MONEY:
				case TENDER_TYPE.MIPS:
				case TENDER_TYPE.GIFTS_MU:
					this.setExternalPayment(this.tenderType);
					break;

				case TENDER_TYPE.MIXED:
					this.setMixPayment();
					break;

				case TENDER_TYPE.CREDIT:
					this.setOnCreditPayment();
					break;

				case TENDER_TYPE.VOUCHER:
					this.setVoucherPayment();
					break;

				case TENDER_TYPE.GIFT_CARD:
					this.setGiftCardPayment();
					break;

				case TENDER_TYPE.LOYALTY:
					this.setLoyaltyPayment();
					break;

				case TENDER_TYPE.SK_WALLET:
					this.setSKWalletPayment();
					break;

				case TENDER_TYPE.ZAPPER:
					this.setZapperPayment();
					break;

				default:
					break;
			}

			var captureSignaturePreference = TerminalManager.terminal.preference.captureSignature;
			var paymentRule = this.getPaymentRuleForTenderType(this.tenderType);

			if (captureSignaturePreference == null || captureSignaturePreference.indexOf(paymentRule) < 0) {
				this.postData();
				return;
			}

			var signaturePopup = jQuery("<div class='signature-popup' title='Signature'><canvas id='signature-canvas'></canvas><div class='sign-above'>Sign above</div></div>").dialog({
				width: "auto", height: "auto", modal: true, autoOpen: true, close: function (event, ui) {

				}, buttons: [{
					id: "signature-popup-btn-skip",
					text: "Skip",
					click: function () {
						if (confirm('Do you want to skip signature capture?')) {
							signaturePopup.dialog("close");
							PaymentManager.postData();
						} else {

						}
					}
				},
				{
					id: "signature-btnsave",
					text: "Save",
					click: function () {
						if (signaturePad.isEmpty()) {
							alert(Translation.translate('signature.is.required', 'Signature is required!'));
							return;
						}

						var dataURL = signaturePad.toDataURL();
						jQuery('#signature').val(dataURL);

						signaturePopup.dialog("close");

						PaymentManager.postData();
					}
				}, {
					id: "signature-btnclear",
					text: "Clear",
					click: function () {
						signaturePad.clear();
					}
				}]

				/*buttons: {
					"Clear": function() {
						signaturePad.clear();
					  },
					"Save": function() {
						if(signaturePad.isEmpty()){
							alert('Signature is required!');
							return;
						}
						
						var dataURL = signaturePad.toDataURL();
						jQuery('#signature').val(dataURL);
						
						signaturePopup.dialog( "close" );
						
						PaymentManager.postData();
					  }
					}*/
			});

			var canvas = $("signature-canvas");
			var ratio = window.devicePixelRatio || 1;
			canvas.width = canvas.offsetWidth * ratio;
			canvas.height = canvas.offsetHeight * ratio;
			canvas.getContext("2d").scale(ratio, ratio);

			var signaturePad = new SignaturePad(canvas);

		},

		postData: function () {
			$("tenderType").value = this.tenderType;
			$("invoiceId").value = this.invoiceId;
			$("dateTrx").value = DateUtil.getCurrentDate();
			$("payment-form").submit();

			new Dialog("Processing, please wait").show();
		},

		getPaymentRuleForTenderType: function (tenderType) {
			var map = [];
			map[TENDER_TYPE.CASH] = PAYMENT_RULE.CASH;
			map[TENDER_TYPE.CARD] = PAYMENT_RULE.CARD;
			map[TENDER_TYPE.CHEQUE] = PAYMENT_RULE.CHEQUE;
			map[TENDER_TYPE.MIXED] = PAYMENT_RULE.MIXED;
			map[TENDER_TYPE.VOUCHER] = PAYMENT_RULE.VOUCHER;
			map[TENDER_TYPE.EXTERNAL_CARD] = PAYMENT_RULE.EXTERNAL_CARD;
			map[TENDER_TYPE.GIFT_CARD] = PAYMENT_RULE.GIFT_CARD;
			map[TENDER_TYPE.CREDIT] = PAYMENT_RULE.CREDIT;
			map[TENDER_TYPE.LOYALTY] = PAYMENT_RULE.LOYALTY;

			return map[tenderType];
		}
	};

	/* Payment panel */
	PaymentPopUp = Class.create(PopUpBase, {
		initialize: function () {

			//hide buttons based on preferences
			var acceptPaymentRule = TerminalManager.terminal.preference.acceptPaymentRule;

			//sales-tenders

			Array.from("BKSEPGMVLJYT").each(function (s) {
				if (acceptPaymentRule.indexOf(s) == -1) {
					console.log('hide -> ' + s);
					jQuery('.payment-popup-' + s).hide();
				}
			});

			if ($receiptJSON.header.soTrx == true) {
				jQuery('#purchase-payment-buttons').hide();
			}
			else {
				jQuery('#sales-payment-buttons').hide();
			}

			this.createPopUp($('payment.popup.panel'));

			/*buttons*/
			// this.okBtn = $('payment.popup.okButton');
			this.cancelBtn = $('payment.popup.cancelButton');

			this.cashBtn = $('payment-popup-cashPaymentButton');
			this.cardBtn = $('payment-popup-cardPaymentButton');
			this.chequeBtn = $('payment-popup-chequePaymentButton');

			/* external credit card processing */
			this.externalCreditCardMachine = $('payment-popup-externalCreditCardMachineButton');

			/* voucher */
			this.voucherBtn = $('payment-popup-voucherPaymentButton');
			this.giftCardBtn = $('payment-popup-giftCardPaymentButton');

			this.skwalletBtn = $('payment-popup-SKWalletPaymentButton');
			this.zapperBtn = $('payment-popup-ZapperPaymentButton');

			this.loyaltyBtn = $('payment-popup-loyaltyPaymentButton');

			/* MCB Juice */
			this.MCBJuiceBtn = $('payment-popup-MCBJuicePaymentButton');

			/* MY.T Money */
			this.MYTMoneyBtn = $('payment-popup-MYTMoneyPaymentButton');

			/* Emtel Money */
			this.EmtelMoney = $('payment-popup-EmtelMoneyPaymentButton');

			/* Gifts.mu */
			this.GiftsMuBtn = $('payment-popup-GiftsMuPaymentButton');

			/* MIPS */
			this.MipsBtn = $('payment-popup-MipsPaymentButton');

			/* add behaviour */
			for (var field in this) {
				var fieldContents = this[field];

				if (fieldContents == null || typeof (fieldContents) == "function") {
					continue;
				}

				if (fieldContents.type == 'button') {
					//register click handler
					fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
				}
			}

			/*override events*/
			this.cancelBtn.onclick = this.cancelHandler.bind(this);
			// this.okBtn.onclick = this.okHandler.bind(this); 
		},
		okHandler: function () {
			if (this.tenderType == null) this.tenderType = TENDER_TYPE.CASH;
			this.hide();
		},
		cancelHandler: function () {
			this.tenderType = null;
			this.isPaymentOnline = true;
			this.hide();
		},
		onShow: function () {
			this.tenderType = TENDER_TYPE.CASH;

			var btn = $('payment-popup-loyaltyPaymentButton');

			if (btn) {
				if (receiptJSON.header.enableLoyalty == true) {
					btn.style.display = 'block';
				}
				else {
					btn.style.display = 'none';
				}
			}

		},
		onHide: function () {
			if (this.tenderType == null) return;

			PaymentManager.setTenderType(this.tenderType);
			PaymentManager.getPaymentDetails();

		},
		clickHandler: function (e) {
			var element = Event.element(e);

			var count = 0;
			while (!(element instanceof HTMLButtonElement)) {
				count++;
				if (count > 10) {
					alert(Translation.translate('fail.to.get.event.source', 'Fail to get event source!'));
					break;
				}
				element = element.parentNode;
			}

			switch (element) {
				case this.cashBtn:
					this.tenderType = TENDER_TYPE.CASH;
					break;

				case this.cardBtn:
					this.tenderType = TENDER_TYPE.CARD;
					PaymentManager.setIsPaymentOnline(true);
					break;

				case this.chequeBtn:
					this.tenderType = TENDER_TYPE.CHEQUE;
					break;

				case this.voucherBtn:
					this.tenderType = TENDER_TYPE.VOUCHER;
					break;

				case this.giftCardBtn:
					this.tenderType = TENDER_TYPE.GIFT_CARD;
					break;

				case this.skwalletBtn:
					this.tenderType = TENDER_TYPE.SK_WALLET;
					break;

				case this.zapperBtn:
					this.tenderType = TENDER_TYPE.ZAPPER;
					break;

				case this.loyaltyBtn:
					this.tenderType = TENDER_TYPE.LOYALTY;
					break;

				case this.externalCreditCardMachine:
					this.tenderType = TENDER_TYPE.EXTERNAL_CARD;
					PaymentManager.setIsPaymentOnline(false);
					break;

				case this.MCBJuiceBtn:
					this.tenderType = TENDER_TYPE.MCB_JUICE;
					PaymentManager.setIsPaymentOnline(false);
					break;

				case this.MYTMoneyBtn:
					this.tenderType = TENDER_TYPE.MY_T_MONEY;
					PaymentManager.setIsPaymentOnline(false);
					break;

				case this.EmtelMoney:
					this.tenderType = TENDER_TYPE.EMTEL_MONEY;
					PaymentManager.setIsPaymentOnline(false);
					break;

				case this.MipsBtn:
					this.tenderType = TENDER_TYPE.MIPS;
					PaymentManager.setIsPaymentOnline(false);
					break;

				case this.GiftsMuBtn:
					this.tenderType = TENDER_TYPE.GIFTS_MU;
					PaymentManager.setIsPaymentOnline(false);
					break;

				default: break;
			}

			this.okHandler();
		}
	});	

	var CashPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('cash.popup.panel'));

			this.amountRefunded = 0.0;

			/*buttons*/
			this.okBtn = $('cash.popup.okButton');
			this.cancelBtn = $('cash.popup.cancelButton');

			/*textfields*/
			this.amountTenderedTextField = $('cash.popup.amountTenderedTextField');
			this.payAmountTextField = $('cash.popup.payAmountTextField');

			/*labels*/
			this.totalLabel = $('cash.popup.totalLabel');
			this.changeLabel = $('cash.popup.changeLabel');
			/*this.writeOffCashLabel = $('cash.popup.writeOffCashLabel');
			this.paymentAmtLabel = $('cash.popup.paymentAmtLabel');*/

			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/*fields*/
			this.cartTotalAmount = parseFloat(PaymentManager.getOpenAmt());
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
			/*this.paymentAmtLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);*/

			/*others*/
			this.amountTenderedTextField.panel = this;
			this.amountTenderedTextField.onkeypress = function (e) {

				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('invalid.tendered.amount');
						return;
					}

					this.panel.okHandler();
					return;
				}

			};

			this.amountTenderedTextField.onkeyup = function (e) {

				var value = this.value;
				if (value == null || value == '') value = 0.0;

				var amountTendered = parseFloat(value);

				if (isNaN(amountTendered)) {
					return;
				}

				var openAmt = this.panel.cartTotalAmount;
				if (amountTendered <= openAmt) {
					this.panel.payAmountTextField.value = new Number(amountTendered).toFixed(2);
					this.panel.changeLabel.innerHTML = '0.00';
				}
				else {
					var changeAmt = 0.0;
					if (amountTendered != 0.0) {
						changeAmt = amountTendered - openAmt;
					}

					this.panel.payAmountTextField.value = new Number(openAmt).toFixed(2);;
					this.panel.changeLabel.innerHTML = new Number(changeAmt).toFixed(2);
				}

			};


			this.payAmountTextField.panel = this;
			this.payAmountTextField.onkeypress = function (e) {

				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('invalid.payment.amount');
						return;
					}

					this.panel.okHandler();
					return;
				}

			};

			this.payAmountTextField.onkeyup = function (e) {

				var value = this.value;
				if (value == null || value == '') value = 0.0;

				var paymentAmt = parseFloat(value);
				var openAmt = this.panel.cartTotalAmount;
				var amtTendered = this.panel.amountTenderedTextField.value;

				var changeAmt = amtTendered - paymentAmt;
				this.panel.changeLabel.innerHTML = new Number(changeAmt).toFixed(2);

				/*
				if(paymentAmt > openAmt){
					alert('Payment amount cannot be more than open amount!');
					return false;
				}
			    
				if(paymentAmt > amtTendered){
					alert('Payment amount cannot be more than amount tendered!');
					return false;
				}
				*/

			};

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},
		resetHandler: function () {
			this.amountTenderedTextField.value = '';
			this.payAmountTextField.value = '';
			this.changeLabel.innerHTML = '0.00';
		},
		onShow: function () {
			this.amountTenderedTextField.focus();
			this.changeLabel.innerHTML = '0.00';
		},
		validate: function () {

			var tenderedAmt = parseFloat(this.amountTenderedTextField.value);
			var payAmt = parseFloat(this.payAmountTextField.value);

			if (isNaN(tenderedAmt)) {
				this.onError('invalid.amount.tendered');
				this.amountTenderedTextField.focus();
				return false;
			}

			if (isNaN(payAmt)) {
				this.onError('invalid.payment.amount');
				this.payAmountTextField.focus();
				return false;
			}

			if (payAmt > PaymentManager.openAmt) {
				this.onError('Payment amt cannot be more than open amount!');
				this.payAmountTextField.focus();
				return false;
			}

			if (payAmt > tenderedAmt) {
				this.onError('payment.amt.cannot.be.more.than.amt.tendered');
				this.payAmountTextField.focus();
				return false;
			}


			this.setPayment();

			return true;
		},
		setPayment: function () {
			var amountTendered = parseFloat(this.amountTenderedTextField.value);
			var paymentAmt = parseFloat(this.payAmountTextField.value);

			var amountRefunded = amountTendered - paymentAmt;
			changeAmt = new Number(amountRefunded).toFixed(2);

			var payment = new Hash();
			payment.set('amt', this.payAmountTextField.value);
			payment.set('amountTendered', amountTendered);
			payment.set('amountRefunded', amountRefunded);
			this.payment = payment;
		},
		paymentHandler: function (payment) {
			alert(payment);
		}
	});


	/* Card Panel */
	var CardPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('card.popup.panel'));

			/*panes*/
			this.trackDataDetailsPane = $('card.popup.trackDataDetailsPane');
			this.cardDetailsPane = $('card.popup.cardDetailsPane');

			/*labels*/
			this.totalLabel = $('card.popup.totalLabel');

			/*textfields*/
			this.cardAmtTextField = $('card.popup.amountTextfield');
			this.cardSelectList = $('card.popup.cardSelectList');
			this.swipeCardCheckBox = $('card.popup.swipeCardCheckBox');

			this.trackDataTextField = $('card.popup.trackDataTextField');
			this.cardNumberTextField = $('card.popup.cardNumberTextField');
			this.cardExpiryDateTextField = $('card.popup.cardExpiryDateTextField');
			this.cardCvvTextField = $('card.popup.cardCvvTextField');
			this.cardHolderNameTextField = $('card.popup.cardHolderNameTextField');
			this.streetAddressTextField = $('card.popup.streetAddressTextField');
			this.zipCodeTextField = $('card.popup.zipCodeTextField');

			/*buttons*/
			this.okBtn = $('card.popup.okButton');
			this.cancelBtn = $('card.popup.cancelButton');

			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/** adding behaviours **/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			/*show details panes*/
			this.cardDetailsPane.style.display = this.swipeCardCheckBox.checked ? 'none' : '';
			this.trackDataDetailsPane.style.display = this.swipeCardCheckBox.checked ? '' : 'none';

			this.swipeCardCheckBox.panel = this;
			this.swipeCardCheckBox.onchange = function (e) {
				this.panel.cardDetailsPane.style.display = this.checked ? 'none' : '';
				this.panel.trackDataDetailsPane.style.display = this.checked ? '' : 'none';
			};

			/*track data*/
			this.trackDataTextField.panel = this;
			this.trackDataTextField.onkeypress = function (e) {
				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('swipe.credit.card');
						return;
					}

					this.panel.okHandler();
				}
			};

			/*card number*/
			this.cardNumberTextField.panel = this;
			this.cardNumberTextField.onkeypress = function (e) {
				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('enter.credit.card.number');
						return;
					}

					this.panel.okHandler();
				}
			};

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},
		resetHandler: function () {
			this.swipeCardCheckBox.checked = true;
			this.cardSelectList.options[0].selected = true;

			this.trackDataTextField.value = '';
			this.cardNumberTextField.value = '';
			this.cardExpiryDateTextField.value = '';
			this.cardCvvTextField.value = '';
			this.cardHolderNameTextField.value = '';
			this.streetAddressTextField.value = '';
			this.zipCodeTextField.value = '';
			this.cardAmtTextField.value = '';
		},
		onShow: function () {
			this.trackDataTextField.focus();
			this.cardAmtTextField.value = PaymentManager.cardAmt;
		},

		validate: function () {

			var amt = parseFloat(this.cardAmtTextField.value);

			if (isNaN(amt)) {
				this.onError('invalid.amt');
				this.cardAmtTextField.focus();
				return false;
			}

			if (amt > PaymentManager.openAmt) {
				this.onError('Card amt cannot be more than open amount!');
				this.cardAmtTextField.focus();
				return false;
			}

			if (this.swipeCardCheckBox.checked) {
				/*card was swipped*/
				if (this.trackDataTextField.value == null || this.trackDataTextField.value == '') {
					this.onError('swipe.credit.card');
					return false;
				}
			}
			else {
				/*no card environment*/
				if (this.cardNumberTextField.value == null || this.cardNumberTextField.value == '') {
					this.onError('credit.card.number.is.required');
					return false;
				}

				if (this.cardExpiryDateTextField.value == null || this.cardExpiryDateTextField.value == '') {
					this.onError('credit.card.has.expired');
					return false;
				}


			}

			this.setPayment();

			return true;
		},

		setPayment: function () {
			var payment = new Hash();
			payment.set('amt', this.cardAmtTextField.value);
			payment.set('isCardPresent', this.swipeCardCheckBox.checked);
			payment.set('cardType', this.cardSelectList.value);

			if (this.swipeCardCheckBox.checked) {
				payment.set('trackData', this.trackDataTextField.value);
			}
			else {
				payment.set('cardNumber', this.cardNumberTextField.value);
				payment.set('cardExpiryDate', this.cardExpiryDateTextField.value);
				payment.set('cardCvv', this.cardCvvTextField.value);
				payment.set('cardHolderName', this.cardHolderNameTextField.value);
				payment.set('streetAddress', this.streetAddressTextField.value);
				payment.set('zipCode', this.zipCodeTextField.value);
			}

			this.payment = payment;
		},
		paymentHandler: function (payment) { }
	});

	/* External Card Panel */
	var CardAmtPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('cardAmt.popup.panel'));

			/*buttons*/
			this.okBtn = $('cardAmt.popup.okButton');
			this.cancelBtn = $('cardAmt.popup.cancelButton');

			/*labels*/
			this.totalLabel = $('cardAmt.popup.totalLabel');

			/*textfields*/
			this.amountTenderedTextField = $('cardAmt.popup.amountTenderedTextField');

			/*events*/
			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/** adding behaviours **/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			this.amountTenderedTextField.panel = this;
			this.amountTenderedTextField.onkeypress = function (e) {
				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('invalid.amt');
						return;
					}

					this.panel.validate();
				}
			};

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();

			PaymentManager.getPaymentDetails();
		},
		resetHandler: function () {
			this.amountTenderedTextField.value = '';
		},
		onShow: function () {
			this.amountTenderedTextField.focus();
		},
		validate: function () {

			var amt = parseFloat(this.amountTenderedTextField.value);

			if (isNaN(amt)) {
				this.onError('invalid.card.amt');
				this.amountTenderedTextField.focus();
				return false;
			}

			if (amt > PaymentManager.openAmt) {
				this.onError('Card amt cannot be more than open amount!');
				this.amountTenderedTextField.focus();
				return false;
			}

			PaymentManager.cardAmt = amt;
			PaymentManager.tenderType = TENDER_TYPE.CARD;

			return true;
		}

	});

	/* Cheque Panel */
	var ChequePanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('cheque.popup.panel'));

			/*buttons*/
			this.okBtn = $('cheque.popup.okButton');
			this.cancelBtn = $('cheque.popup.cancelButton');

			/*labels*/
			this.totalLabel = $('cheque.popup.totalLabel');

			/*textfields*/
			this.amountTenderedTextField = $('cheque.popup.amountTenderedTextField');
			this.chequeNumberTextField = $('cheque.popup.chequeNumberTextField');

			/*events*/
			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/** adding behaviours **/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			this.chequeNumberTextField.panel = this;
			this.chequeNumberTextField.onkeypress = function (e) {
				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('enter.check.number');
						return;
					}

					this.panel.validate();
				}
			};

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},
		resetHandler: function () {
			this.chequeNumberTextField.value = '';
			this.amountTenderedTextField.value = '';
		},
		onShow: function () {
			this.amountTenderedTextField.focus();
		},
		validate: function () {

			var amt = parseFloat(this.amountTenderedTextField.value);

			if (isNaN(amt)) {
				this.onError('invalid.amt');
				this.amountTenderedTextField.focus();
				return false;
			}

			if (amt > PaymentManager.openAmt) {
				this.onError('check.amt.cannot.be.more.than.open.amt');
				this.amountTenderedTextField.focus();
				return false;
			}

			if (this.chequeNumberTextField.value == null || this.chequeNumberTextField.value == '') {
				this.onError('enter.check.number');
				return false;
			}

			this.setPayment();

			return true;
		},
		setPayment: function () {
			var payment = new Hash();
			payment.set('chequeNumber', this.chequeNumberTextField.value);
			payment.set('amt', this.amountTenderedTextField.value);
			this.payment = payment;
		},
		paymentHandler: function (payment) { }
	});

	/* External Card Panel */
	var ExtCardPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('extcard.popup.panel'));

			/*buttons*/
			this.okBtn = $('extcard.popup.okButton');
			this.cancelBtn = $('extcard.popup.cancelButton');

			/*labels*/
			this.totalLabel = $('extcard.popup.totalLabel');

			/*textfields*/
			this.amountTenderedTextField = $('extcard.popup.amountTenderedTextField');

			/*events*/
			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/** adding behaviours **/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			this.amountTenderedTextField.panel = this;
			this.amountTenderedTextField.onkeypress = function (e) {
				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('invalid.amt');
						return;
					}

					this.panel.validate();
				}
			};

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},
		resetHandler: function () {
			this.amountTenderedTextField.value = '';
		},
		onShow: function () {

			this.amountTenderedTextField.focus();
			var title = "Payment Ext. Card";

			switch (PaymentManager.tenderType) {
				case TENDER_TYPE.MCB_JUICE:
					title = "Payment MCB Juice";
					break;
				case TENDER_TYPE.MY_T_MONEY:
					title = "Payment MY.T Money";
					break;
				case TENDER_TYPE.EMTEL_MONEY:
					title = "Payment Blink";
					break;
				case TENDER_TYPE.MIPS:
					title = "Payment MIPS";
					break;
				case TENDER_TYPE.GIFTS_MU:
					title = "Payment Gifts.mu";
					break;

				default: break;
			}

			document.getElementById('extcard.popup.title').innerHTML = title;
		},
		validate: function () {

			var amt = parseFloat(this.amountTenderedTextField.value);

			if (isNaN(amt)) {
				this.onError('invalid.amt');
				this.amountTenderedTextField.focus();
				return false;
			}

			if (amt > PaymentManager.openAmt) {
				this.onError('Amount cannot be more than open amount!');
				this.amountTenderedTextField.focus();
				return false;
			}

			this.setPayment();

			return true;
		},
		setPayment: function () {
			var payment = new Hash();
			payment.set('isPaymentOnline', 'false');
			payment.set('otk', '');
			payment.set('amt', this.amountTenderedTextField.value);
			this.payment = payment;
		},
		paymentHandler: function (payment) { }
	});

	/*Voucher Panel*/
	var VoucherPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('voucher.popup.panel'));

			/*buttons*/
			this.okBtn = $('voucher.popup.okButton');
			this.cancelBtn = $('voucher.popup.cancelButton');

			/*textfields*/
			this.voucherNoTextField = $('voucher.popup.voucherNoTextField');

			/*labels*/
			this.totalLabel = $('voucher.popup.totalLabel');

			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			/*others*/
			this.voucherNoTextField.panel = this;
			this.voucherNoTextField.onkeypress = function (e) {

				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('invalid.voucher.no');
						return;
					}

					this.panel.okHandler();
					return;
				}

			};

			this.organisationSelectList = $('voucher-popup-dropdown');

			var terminals = TerminalManager.terminalList;
			var organisations = new Array();

			/* this.organisationSelectList.options.length = 0; */

			for (var i = 0; i < terminals.length; i++) {
				var terminal = terminals[i];
				var orgName = terminal.org_name;
				var orgId = terminal.org_id;

				if (organisations.indexOf(orgId) == -1) {
					organisations.push(orgId);
					this.organisationSelectList.options[this.organisationSelectList.options.length] = new Option(orgName, orgId, false, false)
				}
			}

			var orgId = TerminalManager.terminal.orgId;
			this.organisationSelectList.setValue(orgId);

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},
		resetHandler: function () {
			this.voucherNoTextField.value = '';
		},
		onShow: function () {
			this.voucherNoTextField.focus();
		},
		validate: function () {

			if (this.voucherNoTextField.value == null || this.voucherNoTextField.value == '') {
				this.onError('invalid.voucher.no');
				this.voucherNoTextField.focus();
				return;
			}

			this.setPayment();

			return true;
		},
		setPayment: function () {
			var payment = new Hash();
			payment.set('voucherNo', this.voucherNoTextField.value);
			payment.set('voucherAmt', null);
			payment.set('voucherOrgId', this.organisationSelectList.value);
			this.payment = payment;
		},
		paymentHandler: function (payment) { }
	});

	/*e2e Panel*/
	var E2EPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('e2e.popup.panel'));

			/*buttons*/
			this.okBtn = $('e2e.popup.okButton');
			this.cancelBtn = $('e2e.popup.cancelButton');

			/*textfields*/
			this.amountTextField = $('e2e.popup.amountTextField');
			this.trackDataTextField = $('e2e.popup.trackDataTextField');

			/*labels*/
			this.totalLabel = $('e2e.popup.totalLabel');

			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			/*others*/
			this.amountTextField.panel = this;
			this.amountTextField.onkeypress = function (e) {

				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('invalid.amt');
						return;
					}

					this.panel.okHandler();
					return;
				}

			};

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},
		resetHandler: function () {
			this.amountTextField.value = '';
		},
		onShow: function () {
			this.amountTextField.focus();
		},
		validate: function () {

			var amt = parseFloat(this.amountTextField.value);

			if (isNaN(amt)) {
				this.onError('invalid.amt');
				this.amountTextField.focus();
				return false;
			}

			if (amt > PaymentManager.openAmt) {
				this.onError('Card amt cannot be more than open amount!');
				this.amountTextField.focus();
				return false;
			}


			if (this.trackDataTextField.value == null || this.trackDataTextField.value == '') {
				this.onError('card.not.swiped');
				this.trackDataTextField.focus();
				return;
			}

			this.setPayment();

			return true;
		},
		setPayment: function () {
			var payment = new Hash();
			payment.set('cardAmt', this.amountTextField.value);
			payment.set('cardTrackDataEncrypted', this.trackDataTextField.value);
			this.payment = payment;
		},
		paymentHandler: function (payment) { }
	});	

	/*XWeb Panel*/
	var XWebPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('xweb.popup.panel'));

			/*buttons*/
			this.cancelBtn = $('xweb.popup.cancelButton');

			this.cancelBtn.onclick = this.hide.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.openAmt;
			this.orderType = PaymentManager.orderType;

			/*others*/
			this.frame = $('xweb.popup.frame');

		},
		okHandler: function () {
			/*ok handler*/
			alert(Translation.translate('ok', 'OK'));
		},
		resetHandler: function () {
			this.frame.src = "";
		},
		onShow: function () {
			this.frame.src = ("PaymentAction.do?action=getOTK&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
		},

		setPayment: function () {
			/*set payment*/
		},

		paymentHandler: function (payment) { }
	});

	/*Mercury Panel*/
	var MercuryPanel = Class.create(PopUpBase, {
		initialize: function ($super) {
			this.createPopUp($('mercury.popup.panel'));

			/*buttons*/
			this.cancelBtn = $('mercury.popup.cancelButton');

			this.cancelBtn.onclick = this.hide.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.openAmt;
			this.orderType = PaymentManager.orderType;

			/*others*/
			this.frame = $('mercury.popup.frame');

		},
		okHandler: function () {
			/*ok handler*/
			alert(Translation.translate('ok', 'OK'));
		},
		resetHandler: function () {
			this.frame.src = "";
		},
		onShow: function () {
			this.frame.src = ("PaymentAction.do?action=getOTK&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
		},

		setPayment: function () {
			/*set payment*/
		},

		paymentHandler: function (payment) { }
	});


	/*Loyalty Panel*/
	var LoyaltyPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('loyalty.popup.panel'));

			/*buttons*/
			this.okBtn = $('loyalty.popup.okButton');
			this.cancelBtn = $('loyalty.popup.cancelButton');

			/*labels*/
			this.totalLabel = $('loyalty.popup.totalLabel');
			this.loyaltyPointsLabel = $('loyalty.popup.loyaltyPointsLabel');
			this.remainingPointsLabel = $('loyalty.popup.remainingPointsLabel');


			/*events*/
			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/** adding behaviours **/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.currencySymbol = PaymentManager.getCurrencySymbol();

			this.loyaltyPoints = receiptJSON.header.loyaltyPoints;

			this.payAmt = this.cartTotalAmount;

			if (parseFloat(this.loyaltyPoints) < parseFloat(this.cartTotalAmount)) {
				this.payAmt = this.loyaltyPoints;
			}

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			var remainder = parseFloat(this.loyaltyPoints) - parseFloat(this.payAmt);
			if (remainder < 0.0) {
				remainder = 0.0;
			}

			this.loyaltyPointsLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.loyaltyPoints).toFixed(2);
			this.remainingPointsLabel.innerHTML = this.currencySymbol + ' ' + new Number(remainder).toFixed(2);

		},
		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},
		resetHandler: function () {

		},
		onShow: function () {

		},
		validate: function () {

			if (parseFloat(this.loyaltyPoints) <= 0.0) {
				alert('No loyalty points available!');
				return false;
			}

			this.setPayment();

			return true;
		},

		setPayment: function () {
			var payment = new Hash();
			payment.set('amt', this.payAmt);
			this.payment = payment;
		},
		paymentHandler: function (payment) { }
	});

	/*ElementPS Panel*/
	var ElementPSPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('elementps.popup.panel'));

			/*buttons*/
			this.cancelBtn = $('elementps.popup.cancelButton');

			this.cancelBtn.onclick = this.hide.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.openAmt;
			this.orderType = PaymentManager.orderType;

			/*others*/
			this.frame = $('elementps.popup.frame');

		},
		okHandler: function () {
			/*ok handler*/
			alert(Translation.translate('ok', 'OK'));
		},
		resetHandler: function () {
			this.frame.src = "";
		},
		onShow: function () {
			this.frame.src = ("PaymentAction.do?action=getOTK&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
		},

		setPayment: function () {
			/*set payment*/
		},

		paymentHandler: function (payment) { }
	});

	/*Century Card Panel*/
	var CenturyPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('century.popup.panel'));

			/*buttons*/
			this.cancelBtn = $('century.popup.cancelButton');

			this.cancelBtn.onclick = this.hide.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.openAmt;
			this.orderType = PaymentManager.orderType;

			/*others*/
			this.frame = $('century.popup.frame');

		},
		okHandler: function () {
			/*ok handler*/
			alert(Translation.translate('ok', 'OK'));
		},
		resetHandler: function () {
			this.frame.src = "";
		},
		onShow: function () {
			this.frame.src = ("PaymentAction.do?action=getCenturyPaymentForm&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
		},

		setPayment: function () {
			/*set payment*/
		},

		paymentHandler: function (payment) { }
	});

	/*UsaEpay Card Panel*/
	var UsaEpayPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('usaepay.popup.panel'));

			/*buttons*/
			this.cancelBtn = $('usaepay.popup.cancelButton');

			this.cancelBtn.onclick = this.hide.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.openAmt;
			this.orderType = PaymentManager.orderType;

			/*others*/
			this.frame = $('usaepay.popup.frame');

		},
		okHandler: function () {
			/*ok handler*/
			alert(Translation.translate('ok', 'OK'));
		},
		resetHandler: function () {
			this.frame.src = "";
		},
		onShow: function () {
			this.frame.src = ("PaymentAction.do?action=getUsaEpayPaymentForm&orderType=" + this.orderType + "&cardAmt=" + PaymentManager.cardAmt + "&orderId=" + PaymentManager.orderId);
		},

		setPayment: function () {
			/*set payment*/
		},

		paymentHandler: function (payment) { }
	});

	var CreditCardPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('credit.card.popup.panel'));

			/*buttons*/
			this.okBtn = $('credit.card.popup.okButton');
			this.cancelBtn = $('credit.card.popup.cancelButton');

			/*textfields*/
			this.amountTextField = $('credit.card.popup.amountTextField');


			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			/*fields*/
			this.cartTotalAmount = PaymentManager.getOpenAmt();
			this.amountTextField.value = this.cartTotalAmount;

			this.paymentManager = null;
			/*others*/
			this.amountTextField.panel = this;
			this.amountTextField.onkeypress = function (e) {

				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('Invalid Amount');
						return;
					}

					this.panel.okHandler();
					return;
				}

			};

		},
		okHandler: function () {
			if (!this.validate()) return;

			var cardAmt = this.amountTextField.value;

			cardAmt = parseFloat(cardAmt);

			if (isNaN(cardAmt) && cardAmt != null && cardAmt != 'null' && cardAmt != '') {
				alert(Translation.translate('invalid.card.amt', 'Invalid card amount!'));
				return;
			}

			if (cardAmt > PaymentManager.openAmt) {
				alert(Translation.translate('card.amt.cannot.be.greater.than.open.amt', 'Card amount cannot be greater than open amount!'));
				return;
			}

			PaymentManager.cardAmt = cardAmt;


			if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_XWeb") {
				panel = new XWebPanel();
			}
			else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_HF") {

				if (ORDER_TYPES.POS_ORDER != this.orderType) {
					alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions', 'The configured payment processor only supports sales transactions'));
					return;
				}

				/*sales transactions can be negative*/
				if (this.openAmt < 0) {
					alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions", "The configured payment processor does not support negative sales transactions"));
					return;
				}

				panel = new MercuryPanel();
			}
			else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS") {
				if (ORDER_TYPES.POS_ORDER != this.orderType) {
					alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions', 'The configured payment processor only supports sales transactions'));
					return;
				}

				/*sales transactions can be negative*/
				if (this.openAmt < 0) {
					alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions", "The configured payment processor does not support negative sales transactions"));
					return;
				}

				panel = new ElementPSPanel();
			}
			else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_EE") {
				var panel = new E2EPanel();
			}
			else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Century") {
				var panel = new CenturyPanel();
			}
			else if (TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_USAEpay") {
				var panel = new UsaEpayPanel();
			}
			else {
				panel = new CardPanel();
			}

			this.hide();

			panel.paymentHandler = this.paymentManager.paymentHandler.bind(this);
			panel.show();
		},
		resetHandler: function () {
			this.amountTextField.value = '';
		},
		onShow: function () {
			this.amountTextField.focus();
		},
		validate: function () {

			if (this.amountTextField.value == null || this.amountTextField.value == '') {
				this.onError('invalid.amount');
				this.amountTextField.focus();
				return;
			}

			return true;
		},

		getCardAmount: function () {
			return this.amountTextField.value;
		},

		setPaymentManager: function (paymentManager) {
			this.paymentManager = paymentManager;
		}
	});

	/*Gift Card Panel*/
	var GiftCardPanel = Class.create(PopUpBase, {
		initialize: function () {
			this.createPopUp($('gift.popup.panel'));

			/*buttons*/
			this.okBtn = $('gift.popup.okButton');
			this.cancelBtn = $('gift.popup.cancelButton');

			/*textfields*/
			this.trackDataTextField = $('gift.popup.trackDataTextField');
			this.numberTextField = $('gift.popup.numberTextField');
			this.cvvTextField = $('gift.popup.cvvTextField');
			this.amountTextField = $('gift.popup.amountTextField');

			/*labels*/
			this.totalLabel = $('gift.popup.totalLabel');

			/*radios*/
			this.swipeRadio = $('gift.popup.swipeRadio');
			this.manualRadio = $('gift.popup.manualRadio');

			/*panels*/
			this.swipePanel = $('gift.popup.swipePanel');
			this.manualPanel = $('gift.popup.manualPanel');

			/*add behaviour to components*/
			this.swipeRadio.onclick = this.radioHandler.bind(this);
			this.manualRadio.onclick = this.radioHandler.bind(this);

			this.cancelBtn.onclick = this.hide.bind(this);
			this.okBtn.onclick = this.okHandler.bind(this);

			this.cartTotalAmount = PaymentManager.openAmt;
			this.orderType = PaymentManager.orderType;
			this.currencySymbol = PaymentManager.currencySymbol;

			this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

			this.trackDataTextField.panel = this;
			this.trackDataTextField.onkeypress = function (e) {
				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('swipe.card');
						return;
					}

					this.panel.okHandler();
					return;
				}
			};

			this.amountTextField.panel = this;
			this.amountTextField.onkeypress = function (e) {

				if (e.keyCode == Event.KEY_RETURN) {
					if (this.value == null || this.value == '') {
						this.panel.onError('invalid.amt');
						return;
					}

					this.panel.trackDataTextField.focus();
					return;
				}

			};

		},

		radioHandler: function (e) {
			var element = Event.element(e);

			this.trackDataTextField.value = '';
			this.numberTextField.value = '';

			switch (element) {

				case this.swipeRadio:
					this.manualPanel.style.display = 'none';
					this.swipePanel.style.display = '';
					break;

				case this.manualRadio:
					this.swipePanel.style.display = 'none';
					this.manualPanel.style.display = '';
					break;

				default:
					break;
			}
		},

		okHandler: function () {
			if (!this.validate()) return;
			this.hide();
			this.paymentHandler(this.payment);
		},

		resetHandler: function () {
			this.trackDataTextField.value = '';
			this.numberTextField.value = '';
			this.amountTextField.value = '';
			this.swipeRadio.checked = true;

			this.manualPanel.style.display = 'none';
			this.swipePanel.style.display = '';
		},

		onShow: function () {
			this.amountTextField.value = this.cartTotalAmount;
			this.amountTextField.select();
		},

		validate: function () {

			if (this.swipeRadio.checked == true) {
				if (this.trackDataTextField.value == null || this.trackDataTextField.value == '') {
					this.onError('gift.card.not.swiped');
					this.trackDataTextField.focus();
					return;
				}
			}
			else {
				if (this.numberTextField.value == null || this.numberTextField.value == '') {
					this.onError('gift.card.number.required');
					this.numberTextField.focus();
					return;
				}
			}

			var amt = parseFloat(this.amountTextField.value);

			if (isNaN(amt)) {
				this.onError('invalid.amt');
				this.amountTenderedTextField.focus();
				return false;
			}

			if (amt > PaymentManager.openAmt) {
				this.onError('Gift card amt cannot be more than open amount!');
				this.amountTenderedTextField.focus();
				return false;
			}

			this.setPayment();

			return true;
		},

		setPayment: function () {
			var payment = new Hash();
			payment.set('giftCardAmt', this.amountTextField.value);
			payment.set('giftCardTrackData', this.trackDataTextField.value);
			payment.set('giftCardNo', this.numberTextField.value);
			payment.set('giftCardCVV', '');
			this.payment = payment;
		},

		paymentHandler: function (payment) { }
	});
}




