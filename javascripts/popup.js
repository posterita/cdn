/******************************************************************************/
/**********************************Pop up Manager******************************/
/******************************************************************************/
var PopUpManager = {
		activePopUp:null,
		allowDisplay:function(){
			return (this.activePopUp == null);
		},
		setActivePopUp:function(popUp){
			this.activePopUp = popUp;
		},
		removeActivePopUp:function(popUp){
			this.activePopUp = null;
		},
		centerPopUp:function(){
			if(this.activePopUp == null) return;				
			this.activePopUp.center();
		},
		clear:function(){
			if(this.activePopUp == null) return;				
			this.activePopUp.hide();
			this.activePopUp = null;
		}
};

/******************************************************************************/
/*************************************Pop up base******************************/
/******************************************************************************/
var PopUpBase = {
	popUp:null,
	createPopUp:function(div){		
		this.container = div;
		
		/*script from dialog.js*/
		this.overlay = $('overlay');		
		if(this.overlay == null){
			new Insertion.Bottom(document.body, "<div id='overlay'></div>");
			
			var overlay = $('overlay');
			this.overlay = overlay;
							
			overlay.setStyle({
				width: ViewPort.getWidth() + "px",
				height: ViewPort.getHeight() + "px",
				backgroundColor:"#000000",
				position:"fixed",
				top:"0px",
				left:"0px",
				display:"none",
				zIndex:99999
			});	
					
			overlay.setOpacity(0.6);
		}
	},
	show:function(){
		
		/*request permission from popup manager*/
		if(!PopUpManager.allowDisplay()) return;
		
		/*show overlay*/
		Element.show(this.overlay);
		
		this.container.style.visibility = 'hidden';
		this.container.style.display = 'block';
		this.center();
		this.container.style.visibility = 'visible';
		this.container.style.zIndex = '100000';
		
		this.onShow();
		PopUpManager.setActivePopUp(this);
	},
	hide:function(){
		PopUpManager.removeActivePopUp(this);
		
		this.container.style.display = 'none';	
		
		/*hide overlay*/
		Element.hide(this.overlay);		
		
		/*reset component state*/
		this.resetHandler();
		
		/* hide keypad */
		Keypad.hide();
		
		this.onHide();
	},
	center:function(){
		
		this.overlay.setStyle({
			width: ViewPort.getWidth() + "px",
			height: ViewPort.getHeight() + "px"
		});	
		
		var xPos = (document.viewport.getWidth() - this.container.getDimensions().width)/2;	
		var yPos = (document.viewport.getHeight() - this.container.getDimensions().height)/2;
		
		if(yPos < 50) yPos = 50;
								
		this.container.setStyle({
			left : (xPos) + "px",
			top  : (yPos - 40) + "px"}
		);
		
		this.onCenter();
	},
	onError:function(messageKey, defaultMessage){
		alert(Translation.translate(messageKey, defaultMessage));
	},
	onShow:function(){},
	onHide:function(){},
	onCenter:function(){},
	resetHandler:function(){}	
};


/******************************************************************************/
/***************************** Popup class *********************************/
/******************************************************************************/

var Dialog = Class.create(PopUpBase, {
	initialize: function(msg, options) {
		
		this.options = options;		
		if(this.options == null) this.options = {
				closeable : true,
				style:{
					color: '#000',
					font: '14pt',
					padding: '10px'
				}
			};	
			
		var div = $('dialog.div');
		if(div == null){
			new Insertion.Bottom(document.body, "<div id='dialog.div' class='popup'>" +
						"<div id='dialog.div.header'></div>" +
					"</div>");
			
			/*new Insertion.Bottom(document.body, "<div id='dialog.div' class='popup'>" +
					"<div id='fadingBarsG'><div id='fadingBarsG_1' class='fadingBarsG'></div><div id='fadingBarsG_2' class='fadingBarsG'></div><div id='fadingBarsG_3' class='fadingBarsG'></div><div id='fadingBarsG_4' class='fadingBarsG'></div><div id='fadingBarsG_5' class='fadingBarsG'></div><div id='fadingBarsG_6' class='fadingBarsG'></div><div id='fadingBarsG_7' class='fadingBarsG'></div><div id='fadingBarsG_8' class='fadingBarsG'></div></div>" +
					"<div id='dialog.div.header' style='text-align:center;'></div>" +
				"</div>");*/
			
			
			div = $('dialog.div');			
		}
		
		var header = $('dialog.div.header');		
		//header.innerHTML = msg;
		
		header.innerHTML =  'Processing...';
		
		if (msg != null && msg != '')
		{
			header.innerHTML = msg;
		}
		
		div.style.width = 'auto';
		
		if(this.options.style){
			div.setStyle(this.options.style);	
		}			
		
		if(this.options.closeable == true){
			div.onclick = this.hide.bind(this);
		}
		else{
			div.onclick = function(e){};
		}
		
    	this.createPopUp(div);
	}
});


var ConfirmationBox = Class.create(PopUpBase, {
	initialize: function(identifier, header, content, footer) {	
		
		var h = header || '';
		var c = content || '';
		var f = footer || '';
		
		var div = $('confirmation.div');
		if(div == null){
			new Insertion.Bottom(document.body, "<div id='confirmation-box-" + identifier + "' class='confirmation-box'>" +
						"<div class='confirmation-box-header'>" + h + "</div>" +
						"<div class='confirmation-box-content'>" + c + "</div>" +
						"<div class='confirmation-box-footer'>" + f + "</div>" +
					"</div>");
			
			div = $('confirmation-box-' + + identifier);			
		}		
		
    	this.createPopUp(div);
	}
});
/******************************************************************************/

/* Invoke Document Panel */
var InvokeDocumentPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('invokeDocument.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('invokeDocument.popup.okButton');
	    this.cancelBtn = $('invokeDocument.popup.cancelButton');
	    
	    /*textfields*/
	    this.documentNumberTextField = $('invokeDocument.popup.documentNumberTextField');	    
	    
	    
	    this.url = null;
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.documentNumberTextField.panel = this;
	    this.documentNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};		
	    
	  }, 
	  validate:function(){
	  	if(this.documentNumberTextField.value == null || this.documentNumberTextField.value == ''){
			this.onError('invalid.documentno');
			this.documentNumberTextField.focus();
			return;
		}
		
	  	if(!this.url){
	  		alert(Translation.translate('set.url','Set URL'));
	  		return;
	  	}
	  	
	  	var url = this.url + this.documentNumberTextField.value;
	  	getURL(url);
	  	
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.documentNumberTextField.value = '';
	  },
	  onShow:function(){
	  	this.documentNumberTextField.focus();
	  }	  
});
/*error popup*/
var ErrorPopUpMsg =  Class.create(PopUpBase, {
	initialize: function(currentField){
		this.createPopUp($('error.popup.panel'));
		 
		 this.okBtn = $('error.popup.panel.okBtn');
		 this.cancelButton = $('error.popup.panel.cancelBtn');
		 
		 this.currentField = currentField;
		 /*events*/
		 this.okBtn.onclick = this.okHandler.bind(this); 
		 this.cancelButton.onclick = this.hide.bind(this);
	},
	okHandler:function(){	  
		  this.hide();
		  this.currentField.focus();
	  }
	
});

/* BP Info Panel */
var BPInfoPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('bpartnerInfo.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('bpartnerInfo.popup.okButton');
	    this.closeBtn = $('bpartnerInfo.popup.closeButton');
	    this.takePictureBtn = $('bpartnerInfo.popup.takePictureButton');
	    this.viewSalesBtn = $('bpartnerInfo.popup.viewsalesButton');
	    this.notesBtn = $('bpartnerInfo.popup.notes');
	    this.saveNotesBtn = $('bpartnerInfo.popup.panel.notes.save.button');
	    this.closeNotesBtn = $('bpartnerInfo.popup.panel.notes.close.button');
	    this.id = 0;
	    this.bpartnerName=null;
	    /*labels*/
	    this.codeLabel = $('bpartnerInfo.popup.codeLabel');	   
	    this.nameLabel = $('bpartnerInfo.popup.nameLabel');
	    this.creditAvailableLabel = $('bpartnerInfo.popup.creditAvailableLabel');
	    this.creditLimitLabel = $('bpartnerInfo.popup.creditLimitLabel');
	    this.creditStatusLabel = $('bpartnerInfo.popup.creditStatusLabel');
	    this.openBalanceLabel = $('bpartnerInfo.popup.openBalanceLabel');
	    this.image = $('bpartnerInfo.popup.image');	  
	    this.imageLink = $('bpartnerInfo.popup.image.link');
	    this.emailLabel = $('bpartnerInfo.popup.emailLabel');
	    this.address1Label = $('bpartnerInfo.popup.address1Label');
	    this.cityLabel = $('bpartnerInfo.popup.cityLabel');
	    this.dobLabel = $('bpartnerInfo.popup.dobLabel');
	    this.telLabel = $('bpartnerInfo.popup.telLabel');
	    //this.faxLabel = $('bpartnerInfo.popup.faxLabel');
	    this.moblie = $('bpartnerInfo.popup.mobile');
	    this.searchKey = $('bpartnerInfo.popup.searchKey');
	    this.notesTextArea = $('bpartnerInfo.popup.panel.textfield');
	    
	    this.notesPanel = $('bpartnerInfo.popup.notes-container');
	    this.summary = $('right_box_bp');
	    
	    this.loyaltyInfo = $('bp-loyalty-info');
	    this.loyaltyPointsEarned = $('bpartnerInfo.popup.loyaltyPointsEarned');
	    this.loyaltyPointsSpent = $('bpartnerInfo.popup.loyaltyPointsSpent');
	    this.loyaltyPoints = $('bpartnerInfo.popup.loyaltyPoints');
	    this.loyaltyStartingPoints = $('bpartnerInfo.popup.loyaltyStartingPoints');
	    this.lastDateRedeemed = $('bpartnerInfo.popup.lastDateRedeemed');
	    
	    /*events*/
	    this.okBtn.onclick = this.hide.bind(this);
	    this.closeBtn.onclick = this.hide.bind(this);
	    this.takePictureBtn.onclick = this.takePicture.bind(this);
	    this.viewSalesBtn.onclick = this.viewSales.bind(this);
	    this.notesBtn.onclick = this.showNotes.bind(this);
	    this.saveNotesBtn.onclick = this.saveNotes.bind(this);
	    this.closeNotesBtn.onclick = this.closeNotes.bind(this);
	    
	  }, 
	  
	  getId : function()
	  {
		  return this.id;
	  },
	  
	  setId : function(id)
	  {
		  this.id = id;
	  },
	 
	 
	  onShow:function(){
			  
		  if (this.id == 0)
		  {	  
			  var bp = BPManager.getBP();
			  this.id = bp.id;
			  this.bpartnerName = bp.name;
			  
		  }
		  var hash = new Hash();
			hash.set('id', this.id);
			
			try
			{	
				var myAjax = new Ajax.Request('BPartnerAction.do?action=reloadPartner',
				{
					method: 'post',
					parameters: hash,
					onSuccess:  this.updateBP.bind(this)
				});
			}
			catch(e)
			{}
			
			this.notesPanel.hide();
			this.summary.show();
	  },
	  
	  viewSales : function()
	  {				 	  
		 // var reportPath = "ReportAction.do?action=generateReportInputForProcess&isAjaxReport=false&processKey=" + "RV_POS_OrderHistory" + "&defaultParameterValueList=";
		  
		  var reportPath = "report-view-sales-history.do#c_bpartner_id=" + this.id + "&customer=" + this.bpartnerName;
		  //reportPath = reportPath + "{C_BPartner_ID :" + this.id + "}";
		  
		  reportPath = encodeURI(reportPath);		  
		  window.location = reportPath;
	  },
	  
	  showNotes : function()
	  {
		
		this.summary.hide();	
		this.notesPanel.show();
		this.notesTextArea.value = this.notesTextArea.innerHTML;		
	  },
	  	  
	  saveNotes : function()
	  {		  
		  try
			{	
				var myAjax = new Ajax.Request('BPartnerAction.do?action=saveNotes&bpartnerId='+ this.id + '&note=' + this.notesTextArea.value,
				{
					method: 'post',
					onSuccess: this.closeNotes.bind(this)
				});
			}
			catch(e)
			{}
			this.notesTextArea.innerHTML = this.notesTextArea.value;
	  }, 
	  
	  closeNotes : function() {
		  this.notesPanel.hide();
		  this.summary.show();
	  },
	  
	  resetHandler:function(){
		  this.notesTextArea.innerHTML = '';
	  },
		  
	  updateBP : function(response)
	  {
			var json = eval('(' + response.responseText + ')');
			var bp = new BP(json);
			BPManager.setBP(bp);
			
			this.bpartnerName = bp.name;
			
			/*set photobooth parameters*/
			PhotoBooth.parameters = "?table=C_BPartner&id=" + bp.getId();
			  
		  	/*populate labels*/ 		
			this.codeLabel.innerHTML = bp.getCode();
			/*this.codeLabel.innerHTML = '<a class="id_font_value" href="ModelAction.do?action=load&controller=Customer&processKey=RV_POS_Customer&id=' + bp.getId() + '">' + bp.getCode() + '</a>';*/
			this.nameLabel.innerHTML = bp.getFullName();
			this.openBalanceLabel.innerHTML = bp.getOpenBalance();
			this.image.src = bp.getCustomerImageUrl();
			this.imageLink.href = bp.getCustomerImageUrl();
		    //this.imageLink.href = bp.getCustomerImageUrl();
			
			if(bp.getCustomerImageUrl() == "")
	    	{
	    		this.image.src = bp.getImageURL();
	    		this.imageLink.href = bp.getImageURL();
	    	}   		
	    			    
		    try
		    {
		    	if(this.imageLink.href.indexOf('?') == -1){
			    	this.imageLink.href = this.imageLink.href + '?t=' + new Date().getTime();	    	
		    	}else{
		    		this.imageLink.href = this.imageLink.href + '&t=' + new Date().getTime();
		    	}
				
				if ((bp.getCustomerImageUrl() != "")||(bp.getImageURL() != ""))
				{
					jQuery(document).ready(function(){
						 /*THEMES : default, dark_rounded, dark_square, facebook, light_rounded, light_square*/ 
						jQuery("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
					});
				}
		    	
		    	/*if(bp.getCustomerImageUrl() != "")
		    	{
		    		this.image.src = bp.getCustomerImageUrl();
		    	}
		    	else
		    	{
		    		this.image.src = bp.getImageURL();
		    	}	
		    	*/
				this.emailLabel.innerHTML = bp.getEmail();
				
				var fullAddress = bp.getAddress1();
				
				var city = bp.getCity();
				
				if(city != null && city.length > 0){
					fullAddress += "<br>City: " + city;
				}
				
				this.address1Label.innerHTML = fullAddress;
				
				/*this.cityLabel.innerHTML = bp.getCity();*/
				this.dobLabel.innerHTML = bp.getBirthday();
				this.telLabel.innerHTML = bp.getPhone();
				//this.faxLabel.innerHTML = bp.getFax();
				this.moblie.innerHTML = bp.getMobile();
				this.searchKey.innerHTML = bp.getSearchKey();
				this.notesTextArea.innerHTML = bp.getDescription();
				
				this.creditStatusLabel.innerHTML = bp.getCreditStatusName();				
				
				/*if(BP_Constants.SO_CREDIT_STATUS.NO_CREDIT_CHECK == bp.getCreditStatus() || !OrderScreen.isSoTrx){
					this.creditAvailableLabel.innerHTML = 'N/A';
					this.creditLimitLabel.innerHTML = 'N/A';
				}
				else{
					this.creditAvailableLabel.innerHTML = bp.getCreditAvailable();
					this.creditLimitLabel.innerHTML = bp.getCreditLimit();
				}*/
				
				this.creditAvailableLabel.innerHTML = bp.getCreditAvailable();
				this.creditLimitLabel.innerHTML = bp.getCreditLimit();
				
				if(bp.enableLoyalty == 'Y')
				{
					this.loyaltyInfo.style.display = 'block';
					
					this.loyaltyStartingPoints.innerHTML = new Number(bp.loyaltyStartingPoints).toFixed(2);
					this.loyaltyPointsEarned.innerHTML = new Number(bp.loyaltyPointsEarned).toFixed(2);
				    this.loyaltyPointsSpent.innerHTML = new Number(bp.loyaltyPointsSpent).toFixed(2);
				    //this.loyaltyPoints.innerHTML = new Number(bp.loyaltyPoints).toFixed(2);
				    //this.loyaltyPoints.innerHTML = new Number(bp.loyaltyPointsEarned - bp.loyaltyPointsSpent +  bp.loyaltyStartingPoints).toFixed(2);
				    this.loyaltyPoints.innerHTML = new Number(bp.loyaltyPoints).toFixed(2);
				    this.lastDateRedeemed.innerHTML = bp.getLastDateRedeemed();
				}
				else
				{
					this.loyaltyInfo.style.display = 'none';
				}				
			
		    }
		    catch (e)
		    {
		    	console.error(e);
		    }
		    
			this.okBtn.focus();
	  },
	  
	  takePicture:function(){
		  this.hide();
		  PopUpManager.removeActivePopUp(this);
		  new PhotoBoothPanel().show();
	  }
});

/* Back date Panel*/
var BackDatePanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('backDate.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('backDate.popup.okButton');
	    this.cancelBtn = $('backDate.popup.cancelButton');
	    
	    /*textfields*/
	    /*this.documentNumberTextField = $('backDate.popup.documentNumberTextField');*/
	    /*this.referenceNumberTextField = $('backDate.popup.referenceNumberTextField');*/    
	    
	    /*selects*/
	    this.dateSelectList = $('backDate.popup.dateSelectList');
	    this.monthSelectList = $('backDate.popup.monthSelectList');
	    this.yearSelectList = $('backDate.popup.yearSelectList');
	    
	    this.hourSelectList = $('backDate.popup.hourSelectList');
	    this.minuteSelectList = $('backDate.popup.minuteSelectList');
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*Event.observe(this.referenceNumberTextField,'focus',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
	    
	   /* this.documentNumberTextField.panel = this;
	    this.documentNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};*/
		
		/*this.referenceNumberTextField.panel = this;
	    this.referenceNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};*/
		
		this.renderDateComponents();
				    
	  }, 
	  validate:function(){
		  var dateOrdered = this.yearSelectList.value + '-' + 
	  		this.monthSelectList.value + '-' +
	  		this.dateSelectList.value + ' ' +
	  		this.hourSelectList.value + ':' + this.minuteSelectList.value + ':00';
		  
		  var values = {};
		  /*values.referenceNumber = this.referenceNumberTextField.value;*/
		  /*values.documentNumber = this.documentNumberTextField.value;*/
		  values.dateOrdered = dateOrdered;
		  
		  this.updateElements(values);		  
		  this.hide(); 	
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	/*this.documentNumberTextField.value = '';*/
	  	/*this.referenceNumberTextField.value = '';*/
	  	//reset date selects
	  	this.setDate();
	  },
	  onShow:function(){
	  	/*this.referenceNumberTextField.focus();*/
	  },
	  
	  renderDateComponents:function(){
		  var date = new Date();
		  
		  var html = '';
		  
		  //render hour select
		  for(var i=0; i<24; i++){
			  html = html + "<option value='" + ((i<10) ? '0' : '') + i +"'>" + ((i<10) ? '0' : '') + i + "</option>";
		  }
		  
		  this.hourSelectList.innerHTML = html;
		  
		  //render minute select
		  html = '';		  
		  for(var i=0; i<60; i++){
			  html = html + "<option value='" + ((i<10) ? '0' : '') + i +"'>" + ((i<10) ? '0' : '') + i + "</option>";
		  }
		  
		  this.minuteSelectList.innerHTML = html;
		  
		  //render date select
		  html = '';		  
		  for(var i=1; i<=31; i++){
			  html = html + "<option value='" + ((i<10) ? '0' : '') + i +"'>" + ((i<10) ? '0' : '') + i + "</option>";
		  }
		  
		  this.dateSelectList.innerHTML = html;
		  
		  //render month select
		  html = '';		  
		  for(var i=1; i<=12; i++){
			  html = html + "<option value='" + ((i<10) ? '0' : '') + i +"'>" + ((i<10) ? '0' : '') + i + "</option>";
		  }
		  
		  this.monthSelectList.innerHTML = html;
		  
		  //render year select
		  var year = date.getFullYear();
		  html = '';		  
		  for(var i=-2; i<1; i++){
			  html = html + "<option value='" + (i+year) + "'>" + (i+year) + "</option>";
		  }
		  
		  this.yearSelectList.innerHTML = html;		  
		  this.setDate();
	  },
	  
	  setDate:function(){
		  var currentDate = new Date();
		  
		  var date = currentDate.getDate();
		  var month = currentDate.getMonth();
		  var year = currentDate.getFullYear();
		  
		  var hours = currentDate.getHours();
		  var minutes = currentDate.getMinutes();
		  
		  //set year first
		  var options = this.yearSelectList.options;
		  for(var i=0; i<options.length; i++){
			  var option = options[i];
			  if(option.value == year){
				  option.selected = true;
				  break;
			  }
		  }
		  
		  //set month
		  this.monthSelectList.options[month].selected = true;
		  
		  //set date
		  this.dateSelectList.options[date - 1].selected = true;
		  
		  //set hour
		  this.hourSelectList.options[hours].selected = true;
		  
		  //set minutes
		  this.minuteSelectList.options[minutes].selected = true;
	  },
	  
	  updateElements : function(){}
});

/* Discount panel */
/*
var DiscountPanel = Class.create(PopUpBase, {
	  initialize: function() {
		    this.createPopUp($('discount.popup.panel'));
		}
});
*/


/* Terminal Panel */
var ChangeTerminalPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
		/*insert html*/
		this.renderHTML();
		  
	    this.createPopUp($('change-terminal-popup-panel'));
	    
	    /*buttons*/
	    this.okBtn = $('change-terminal-popup-panel-ok-button');
	    this.cancelBtn = $('change-terminal-popup-panel-cancel-button');
	    
	    /*selects*/
	    this.organisationSelectList = $('change-terminal-popup-panel-organisation-dropdown');
	    this.terminalSelectList = $('change-terminal-popup-panel-terminal-dropdown');
	    
	    /*events*/
	    this.organisationSelectList.onchange = this.changeHandler.bindAsEventListener(this);
	    this.terminalSelectList.onchange = this.changeHandler.bindAsEventListener(this);	
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this);	
	    
	    this.terminalSelectList.onkeyup = this.submitPopUp.bind(this);
	    
	    /**/
	    this.loginDateTextField = $('change-terminal-popup-panel-logindate-hidden-textfield');
	    this.form = $('change-terminal-popup-panel-form');	       
	        
	  },  
	  okHandler:function(){
		  var orgId = this.organisationSelectList.value;
		  if(orgId <= 0){
			  alert(Translation.translate("please.choose.an.organisation","Please choose an organisation"));
			  return;
		  }		  
		  
		  if(this.terminalSelectList.value == TerminalManager.terminal.id) return;
		  
		  var loginDate = DateUtil.getCurrentDate();
 	   	  this.loginDateTextField.value = loginDate;
 	   	  this.form.submit();
		  /*TerminalManager.setTerminal(this.terminalSelectList.value);*/
	  },
	  resetHandler:function(){
		  this.organisationSelectList.options.length = 0;
		  this.terminalSelectList.options.length = 0;
	  },
	  changeHandler:function(e){
		  var element = Event.element(e);
		  
		  switch(element){
		  	case this.organisationSelectList:
		  		this.renderTerminalList();			    
		  		break;
		  		
		  	case this.terminalSelectList:break;
		  	
		  	default: break;
		  }
	  },
	  renderTerminalList:function(){
		  /*set terminal list*/
	  		var orgId = this.organisationSelectList.value;
		    var terminalList = TerminalManager.terminalList;
		    
		    this.terminalSelectList.options.length = 0;
		    
		    var count = 0;
		    for(var i=0; i<terminalList.length; i++){
		    	var terminal = terminalList[i];
		    	if(orgId == terminal.org_id){		    		
		    		var terminalName = terminal.terminal_name;
   	   	   			var terminalId = terminal.terminal_id;
   	   	   			this.terminalSelectList.options[count++] = new Option(terminalName, terminalId, false, false);
		    	}
		    }
	  },
	  onShow:function(){		  
		  /* parse terminal list for organisations. See select-terminal.jsp */
		  var terminals = TerminalManager.terminalList;
		  var organisations = new Array();
		  
		  this.organisationSelectList.options.length = 0;
		  this.terminalSelectList.options.length = 0;
		  
		  for(var i=0; i<terminals.length; i++){
			var terminal = terminals[i];
			var orgName = terminal.org_name;
			var orgId = terminal.org_id;
		
			if(organisations.indexOf(orgId) == -1){
				organisations.push(orgId);
				this.organisationSelectList.options[this.organisationSelectList.options.length] = new Option(orgName, orgId, false, false)					
			}   	   			
		  }
		  
		  var orgId = TerminalManager.terminal.orgId;
		  this.organisationSelectList.setValue(orgId);
		  
		  this.renderTerminalList();

		  var terminalId = TerminalManager.terminal.id;
		  this.terminalSelectList.setValue(terminalId);		  
	  },
	  
	  submitPopUp : function(e)
	  {
		  if (e.keyCode == Event.KEY_RETURN)
		 {
			  this.form.submit();
		 }
	  },
	  
	  renderHTML : function(){
		  if($('change-terminal-popup-panel') == null){
			  var html = '<div class="modal" style="display: none;" id="change-terminal-popup-panel" tabindex="-1" role="dialog" aria-labelledby="change-terminal-popup-panel" aria-hidden="true">' + 
			  '<div class="modal-dialog" style="width: 400px;">' + 
			  '<div class="modal-content">' + 
			  '<form action="/TerminalAction.do" id="change-terminal-popup-panel-form">' + 
			  '<div class="modal-header">' + 
			  '<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="change-terminal-popup-panel-cancel-button">&times;</button>' + 
			  '<h4 class="modal-title">' + 
			  '<div class="glyphicons glyphicons-pro imac">&nbsp;Change' + 
			  'Terminal</div>' + 
			  '</h4>' + 
			  '</div>' + 
			  '<div class="modal-body">' + 
			  '<input type="hidden" name="action" value="changeTerminal" />' + 
			  '<input type="hidden" id="change-terminal-popup-panel-logindate-hidden-textfield" name="loginDate" value="" />' + 
			  '<div class="row">' + 
			  '<div class="col-xs-12 col-md-12 popup-label">Organisation</div>' + 
			  '</div>' + 
			  '<div class="row">' + 
			  '<div class="col-xs-12 col-md-12">' + 
			  '<select class="input-sm" name="ad_org_id" id="change-terminal-popup-panel-organisation-dropdown"></select>' + 
			  '</div>' + 
			  '</div>' + 
			  '<div class="row padding-top-12">' + 
			  '<div class="col-xs-12 col-md-12 popup-label">Terminal</div>' + 
			  '</div>' + 
			  '<div class="row">' + 
			  '<div class="col-xs-12 col-md-12">' + 
			  '<select class="input-sm" name="u_posterminal_id" id="change-terminal-popup-panel-terminal-dropdown"></select>' + 
			  '</div>' + 
			  '</div>' + 
			  '</div>' + 
			  '<div class="modal-footer">' + 
			  '<div class="row">' + 
			  '<button type="button" class="btn btn-xs btn-success" style="width: 20%;" id="change-terminal-popup-panel-ok-button">' + 
			  '<div class="glyphicons glyphicons-pro ok_2"></div>' + 
			  '<div>Ok</div>' + 
			  '</button>' + 
			  '</div>' + 
			  '</div>' + 
			  '</form>' + 
			  '</div>' + 
			  '</div>' + 
			  '</div>';
			  
			  jQuery('body').append(html);
		  }
	  }
});

var MessageBox = {
		info : function(info){
			new Dialog(info, {closeable:true, style:{backgroundColor:'#9ACF30',fontSize:'14pt'}}).show();
		},
		warn: function(warning){},
		error: function(error){}
};


/*keypad panel*/
var Keypad = {
		keyPad:null,
		initialized:false,
		textField:null,
		
		getKeyPad : function(){
			return $('keypad-popup-panel');
		},
		
		init:function(){
			this.keyPad = $('keypad-popup-panel');
			if(this.keyPad == null || this.keyPad.length==0)
			{	
				return;
			}
			
			this.keyPad.style.zIndex = 2000000;
			this.initialized = true;
			
			this.key9 = $('key9');
			this.key8 = $('key8');
			this.key7 = $('key7');
			this.key6 = $('key6');
			this.key5 = $('key5');
			this.key4 = $('key4');
			this.key3 = $('key3');
			this.key2 = $('key2');
			this.key1 = $('key1');
			this.key0 = $('key0');
			
			this.keyClose = $('keyClose');
			this.keyDelete = $('keyDelete');
			this.keyDot = $('keyDot');
			this.keyEnter = $('keyEnter');
			this.keyMinus = $('keyMinus');
			
			/* add behaviour */	
			for(var field in this){
				var fieldContents = this[field];
				
				if (fieldContents == null || typeof(fieldContents) == "function") {
					continue;
			    }
			    
			    if(fieldContents.type == 'button'){
			    	//register click handler
			    	fieldContents.onclick = this.keyHandler.bindAsEventListener(this);	
			    }
			}
			    
			var inputs = $$('input.numeric');
			for(var i=0; i<inputs.length; i++){
				Event.observe(inputs[i],'click',this.clickHandler.bindAsEventListener(this),false);
				Event.observe(inputs[i],'keyup',this.keyUpHandler.bindAsEventListener(this),false);
				//Event.observe(inputs[i],'focus',this.clickHandler.bindAsEventListener(this),false);
			}
			
			this.initialized = true;
		},
		
		keyUpHandler:function(e){
			
			var element = Event.element(e);	
			
			var re = /[^0-9\.\-]/g;
		    if(re.test(element.value))
		    {
		    	element.value = element.value.replace(re, '');
		    }

		},
		
		clickHandler:function(e){
			
			if((navigator.userAgent.match(/iPhone/i)) || 
					(navigator.userAgent.match(/iPad/i)) ||
					 (navigator.userAgent.match(/iPod/i))) {
				return;
			}
			
			var element = Event.element(e);		
			if(!this.initialized) this.init();
			this.textField = element;	
			
			var offset = Element.viewportOffset(element);
			var width = $(element).getWidth();
			
			var bottom = offset.top + 10 + this.keyPad.getHeight();
			var right = offset.left + width + 40 + this.keyPad.getWidth();
			
			var screenWidth = ViewPort.getWidth();
			var screenHeight = ViewPort.getHeight();
			
			var topOffset = 10;
			var leftOffset = width + 40;
			
			if(right > screenWidth){
				leftOffset = -40 - this.keyPad.getWidth();
			}
			
			if(bottom > screenHeight){
				topOffset = bottom - screenHeight - this.keyPad.getHeight();
			}			
			
			this.keyPad.style.top = (offset.top + topOffset) + 'px';
			this.keyPad.style.left = (offset.left + leftOffset) + 'px';
			this.keyPad.style.display='block';
			
			this.deleteAll = true;
			
			var inputs = $$('input.numeric');
			
			for(var i=0; i<inputs.length; i++)
			{
				if (inputs[i].getAttribute('isTextBoxSelected') == 'true' || inputs[i].getAttribute('isTextBoxSelected') == null)
				{
					if (inputs[i].getAttribute('isActiveTextBox') != 'true')
					{
						inputs[i].setAttribute('isTextBoxSelected', 'false');
					}
				}
			}
			
			if (this.textField.getAttribute('isTextBoxSelected') == 'false' || this.textField.getAttribute('isTextBoxSelected') == null)
			{
				this.textField.focus();
				this.textField.select();
				this.textField.setAttribute('isTextBoxSelected', 'true');
				
				for(var i=0; i<inputs.length; i++)
				{
					if (inputs[i].getAttribute('isActiveTextBox') == 'true')
					{
						inputs[i].setAttribute('isActiveTextBox', 'false');
						break;
					}
				}
				
				this.textField.setAttribute('isActiveTextBox', 'true');
			}
		},
		
		hide:function(){
			this.textField = null;
			var keyPad = this.getKeyPad();
			if(keyPad != null) keyPad.hide();
		},
		
		keyHandler:function(e){
			var element = Event.element(e);	
			
			/*Bug fix for chrome*/
			if (element.id == '' || element.id == null)
			{
				element = element.parentElement;
			}
			/*******************/
			
			var value = this.textField.value;
			
			if (this.textField.selectionStart != this.textField.selectionEnd)
			{
				this.textField.value = '';
				value = '';
			}
			
			switch(element){
				case this.keyClose : 
					this.hide();
					return;
					break;
					
				case this.keyDelete :
					if(this.deleteAll == true){
						value = '';
						this.textField.value = value;
						this.deleteAll = false;
						return;
					}
					
					if(value.length > 0)
						value = value.substring(0,value.length-1);
					break;
					
				case this.keyDot :
					value = value + '.';
					break;
				
				case this.key9 :
					value = value + '9';
					break;
					
				case this.key8 : 
					value = value + '8';
					break;
					
				case this.key7 : 
					value = value + '7';
					break;
					
				case this.key6 : 
					value = value + '6';
					break;
					
				case this.key5 : 
					value = value + '5';
					break;
					
				case this.key4 : 
					value = value + '4';
					break;
					
				case this.key3 : 
					value = value + '3';
					break;
					
				case this.key2 : 
					value = value + '2';
					break;
					
				case this.key1 : 
					value = value + '1';
					break;
					
				case this.key0 : 
					value = value + '0';
					break;
					
				case this.keyEnter :
					
					/*
					var evt = document.createEvent("KeyboardEvent");
					evt.initKeyEvent ("keyup", true, true, window, 0, 0, 0, 0,13, 13);					
					this.textField.dispatchEvent(evt);	
					*/
					
					switch (this.textField) {
					case $('quantity-texfield'):
						if(!ShoppingCartManager.isShoppingCartEmpty()){
							var qty = parseFloat(this.textField.value);							
							if(isNaN(qty)){
								alert(Translation.translate("invalid.qty","Invalid Qty!"));
								this.textField.selectAll();								
								return;					
							}
							ShoppingCartManager.updateQty(qty);							
						}
						break;

					default:
						break;
					}
										
					this.hide();
					return;
		
				case this.keyMinus : 
					if(value.indexOf('-') == -1){
						value = '-' + value;
					}
					else{
						value = value.substring(1);
					}
					
					break;
					
				default: break;					
			}
			
			this.deleteAll = false;
						
			this.textField.value = value;
			var onkeyup = this.textField.onkeyup;
			if(onkeyup){
				var event = {keyCode:65};
				onkeyup.call(this.textField,event);
			}
		}
};

/* Product Info Panel */
var ProductInfoPanel =  Class.create(PopUpBase, {
	  
	initialize: function() {
	    this.createPopUp($('productInfo.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('productInfo.popup.okButton');
	    
	    /*labels*/
	    this.barcodeLabel = $('productInfo.popup.barcodeLabel');
	    this.nameLabel = $('productInfo.popup.nameLabel');
	    this.descriptionLabel = $('productInfo.popup.descriptionLabel');
	    this.standardPriceLabel = $('productInfo.popup.standardPriceLabel');
	    this.listPriceLabel = $('productInfo.popup.listPriceLabel');
	    this.limitPriceLabel = $('productInfo.popup.limitPriceLabel');
	    this.unitOfMeasureLabel = $('productInfo.popup.unitOfMeasureLabel');
	    this.productTypeLabel = $('productInfo.popup.productTypeLabel');
	    this.orgLabel = $('productInfo.popup.orgLabel');
	    this.includesTaxLabel = $('productInfo.popup.includesTaxLabel');
	    this.taxLabel = $('productInfo.popup.taxLabel');
	    this.image = $('productInfo.popup.image');
	    this.imageLink = $('productInfo.popup.image.link');
	    this.productGroups = $('product.groups');
	    this.productStocks = $('product.stocks');
	    this.closeButton = $('productInfo.popup.close.button');
	    this.viewTransactionDetailsBtn = $('productInfo.popup.viewTransactionDetails');
	    this.currencySymbol = '';
	    this.mappingLabel = $('productInfo.popup.mappingLabel');
	    
	    /*events*/
	    this.okBtn.onclick = this.hide.bind(this); 
	    this.okBtn.onkeyup = this.closePanel.bind(this);
	    this.closeButton.onclick = this.hide.bind(this);
	    this.viewTransactionDetailsBtn.onclick = this.viewTransactionDetails.bind(this);
	  },
	  
	  viewTransactionDetails : function()
	  {
		  /*getReport('RV_TransactionDetails', '{M_Product_ID :'+this.productInfo.basicInfo.M_Product_ID.value+'}');*/
		  getURL('report-view-transaction-details.do?#m_product_id=' + this.productInfo.basicInfo.M_Product_ID.value + '&product=' + encodeURI(this.productInfo.basicInfo.Name.value));
	  },
	  
	  getInfo:function(productId){
		  var url = "ProductAction.do?action=getProductInfo&productId=" + productId;
		  new Ajax.Request( url,{ 
				onSuccess: this.getInfoSuccess.bind(this)	  					  			
	  		});	
	  },
	  
	  getInfoSuccess:function(request){
		  var json = eval('('+ request.responseText +')');
		  this.productInfo = json;
		  this.show();
	  },
	  
	  onShow:function(){
		  /*set photobooth parameters*/
		  PhotoBooth.parameters = "?table=M_Product&id=" + this.productInfo.basicInfo.M_Product_ID.value;
		  
	  	/*display info*/
		  
		

	    this.nameLabel.innerHTML = this.productInfo.basicInfo.Name.value;
	    this.barcodeLabel.innerHTML = this.productInfo.basicInfo.UPC.value;
	    this.descriptionLabel.innerHTML = this.productInfo.basicInfo.Description.value;
	    this.unitOfMeasureLabel.innerHTML = this.productInfo.basicInfo.C_UOM_ID.value;
	    this.productTypeLabel.innerHTML = this.productInfo.basicInfo.ProductType.value;
	    this.orgLabel.innerHTML = this.productInfo.basicInfo.AD_Org_ID.value;
	    
	    var mappings = this.productInfo.mappings;
	    
	    if( mappings != undefined)
	    {
	    	this.mappingLabel.innerHTML = mappings;
	    }
	    else
	    {
	    	this.mappingLabel.innerHTML = '';
	    }
    
	    var hasImageAttachment = this.productInfo.hasImageAttachment;
	    /*var imageURL = this.productInfo.imageURL != null ? this.productInfo.imageURL: "";*/
	    var imageURL = this.productInfo.imageURL;
	    /*var imageURL = "images/v13/logo2.png";*/
	    /*var imageURL = null;*/
	    if(imageURL!=null){
	    	this.imageLink.innerHTML = "<img id='productInfo.popup.image' class='frame-image' alt=''/>";
	    	this.image = $('productInfo.popup.image');
	    	this.image.src = imageURL;
	    }
	    else{
	    	this.imageLink.innerHTML = "<i class='glyphicons product-image object-image no-padding'></i>";
	    }
	    /*this.image.src = imageURL;*/
	    this.imageLink.href = imageURL;
	    
		
	    var includesTax = this.productInfo.includesTax;
	    var tax = this.productInfo.tax;
	    
    	this.includesTaxLabel.innerHTML = (includesTax) ? 'Price Includes Tax' : 'Price Excludes Tax';
    	this.taxLabel.innerHTML = tax;
	    
    	this.currencySymbol = this.productInfo.basicInfo.CurrencySymbol;
    	
	    /*this.standardPriceLabel.innerHTML = this.currencySymbol + new Number(this.productInfo.priceStd).toFixed(2);
	    this.listPriceLabel.innerHTML = this.currencySymbol + new Number(this.productInfo.priceList).toFixed(2);
	    this.limitPriceLabel.innerHTML = this.currencySymbol + new Number(this.productInfo.priceLimit).toFixed(2);*/
	    this.standardPriceLabel.innerHTML = new Number(this.productInfo.priceStd).toFixed(2);
	    this.listPriceLabel.innerHTML = new Number(this.productInfo.priceList).toFixed(2);
	    this.limitPriceLabel.innerHTML = new Number(this.productInfo.priceLimit).toFixed(2);
	    
	    var groups = this.productInfo.groups;
	    var groups_html = '';
	    for (var i=0; i<groups.length; i++)
	    {
	    	var group = groups[i];
	    	for (var name in group)
	    	{
	    		var gp = group[name];
	    		var label = gp.label;
	    		var value = gp.value;
		    	if (value != '' && value != 'null')
		    	{
		    		/*groups_html += "<span>" + value + ",&nbsp;</span>";*/
		    		groups_html += "<div class='row'>";
		    		groups_html += "<div class='col-xs-12 col-md-12'>" + label + ": " +value + "</div>";
		    		groups_html += "</div>";
		    	}
	    	}
	    }
	    
    	this.productGroups.innerHTML = groups_html;
	    
	    var stocks = this.productInfo.stocks;
	    var stocks_html = '';
	    for (var warehouse in stocks)
	    {
	    	if (stocks.hasOwnProperty(warehouse))
	    	{
		    	var name = warehouse;
		    	var qty = stocks[warehouse];
		    	
		    	/*stocks_html += "<tr class='stock'><td>"+name+":</td><td>"+qty+"</td></tr>";*/
		    	stocks_html += "<div class='row' style='background-color: #e8e8e8; border-bottom:3px solid white;'>";
		    	stocks_html += "<div class='col-xs-10 col-md-10' style='font-family:arial; font-size:12pt; color:#1d1d1d;'>"+name+"</div>";
		    	stocks_html += "<div class='col-xs-2 col-md-2' style='font-family:arial; font-size:12pt; color:#1d1d1d;'><div class='pull-right'>"+qty+"</div></div>";
		    	stocks_html += "</div>";
	    	}
	    }
	    
    	this.productStocks.innerHTML = stocks_html;
	   // this.limitPricelLabel.innerHTML = currencySymbol + new Number(currentLine.priceLimit).toFixed(2);
    	
    	if (this.productInfo.imageURL != null)
		{
			jQuery.noConflict();
			try
			{
				jQuery(document).ready(function(){
					/* THEMES : default, dark_rounded, dark_square, facebook, light_rounded, light_square */
					jQuery("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
				});
			}
			catch (e)
			{
				alert(e.message);
			}
		}
    	
    	/* discount codes */
    	if(this.productInfo.discountCodes){  
    		
    		var discountCodes = this.productInfo.discountCodes;
    		var discountCode, description, price;
    		
    		var html = "";
    		
    		for(var i=0; i<discountCodes.length; i++){
    			
    			discountCode = discountCodes[i];
    			
    			description = discountCode.name + " - " + discountCode.percentage + "%";
    			price = new Number(( ( this.productInfo.priceStd * ( 100 - discountCode.percentage ) ) / 100 ) ).toFixed(2);
    			
    			html = html + '<div class="row header-3" style="font-family:arial;color:#8e8e93 !important;">' +
					'<div class="col-xs-8 col-md-8">' +
						'<span>' + description + '</span>' +
					'</div>' +
					'<div class="col-xs-4 col-md-4">' +
						'<span class="pull-right" style="padding-right: 36px">' + price + '</span>' +
					'</div>' +
				'</div>';
    		}    		
    		
    		$('productInfo.popup.discountCodes').innerHTML = html;
    		
    		$("productInfo.popup.discountCode.container").show();    		
    	}
    	else
    	{
    		$("productInfo.popup.discountCode.container").hide();
    	}
		
		/* Wholesale Price */
		if(this.productInfo.wholesalePrice)
		{
			$("productInfo.popup.WholesalePriceContainer").show();
			$("productInfo.popup.WholesalePriceLabel").innerHTML = new Number(this.productInfo.wholesalePrice).toFixed(2);
		}
		else
		{
			$("productInfo.popup.WholesalePriceContainer").hide(); 
		}
		
    	
    	this.okBtn.focus();
	  },
	  
	  closePanel : function(e)
	  {
		  if (e.keyCode == Event.KEY_RETURN)
		 {
			  this.hide();
		 }
	  }
});

var OrderInfoPanel = Class.create(PopUpBase,{
	initialize : function(orderId)
	{
		this.orderId = orderId;
		this.createPopUp($('orderInfo_popup_panel'));
		
		this.cancelBtn = $('orderInfo.popup.close.button');
		this.cancelBtn.onclick = this.hide.bind(this);
		
		this.okBtn = $('orderInfo.popup.ok.button');
		this.okBtn.onclick = this.hide.bind(this);
	},
	
	getInfo : function()
	{
		var url = "OrderAction.do?action=getOrderInfo&orderId=" + this.orderId;
		new Ajax.Request( url,{ 
			onSuccess: this.getInfoSuccess.bind(this)	  					  			
  		});	
	},
	
	getInfoSuccess : function(response)
	{
		var json = eval('('+response.responseText + ')');
		$('orderInfo.logo').innerHTML = '<img src="images/v13/logo2.png" alt="logo" />';
		
		var orderInfo = eval('(' + json.orderInfo + ')');
		
		var orgAddress = orderInfo.header.org.address;
		var orgCity = orderInfo.header.org.city;
		var orgPostal = orderInfo.header.org.postal;
		
		orgAddress = (orgAddress != null) ? orgAddress : '';
		orgCity = (orgCity != null) ? orgCity : '';
		orgPostal = (orgPostal != null) ? orgPostal : '';
		
		$('orderInfo.top.address').innerHTML =orderInfo.header.org.name +'<br/>' + 
													orgAddress+ '<br/>' + 
													orgCity + '<br/>' + 
													orgPostal+ '<br/>' +
													Translation.translate('terminal') + ': '+orderInfo.header.terminal+'<br/>';
		
		$('orderInfo-title').innerHTML = orderInfo.header.title;
		
		$('orderInfo-right').innerHTML ='# '+orderInfo.header.documentNo;
		
		$('invoice-info').innerHTML = Translation.translate('invoice.no')+ ': '+orderInfo.header.invoiceNo;
				
		var creditCardDetails = orderInfo.header.creditCardDetails;
		
		
		creditCardDetails = (creditCardDetails != null) ? creditCardDetails: '';
		
		var card = Translation.translate('sold.by') + ': '+orderInfo.header.salesRep+'<br/>'
					+orderInfo.header.paymentRuleName;
		if(paymentRuleName == 'Credit Card'){
			if(creditCardDetails != null && creditCardDetails != ''){
				var details = 'Card details: ';
				card = card + details + creditCardDetails;
			}
			
		}
		
		
		$('orderInfo-left2').innerHTML = card;
											
		var paymentRuleName = orderInfo.header.paymentRuleName;
		$('orderInfo-right2').innerHTML = orderInfo.header.dateOrdered+'<br/>'+
											Translation.translate('status') + ': '+orderInfo.header.docStatusName+'<br/>'
											+orderInfo.header.orderType;
											/*'Order Type: '+'<strong>'+orderInfo.header.orderType+'</strong>'+'<br/>'+
											'Sales Rep: '+'<strong>'+orderInfo.header.salesRep+'</strong>'+'<br/>'+
											'Terminal: '+'<strong>'+orderInfo.header.terminal+'</strong>'+'<br/>';*/
		
		var customerAddress = orderInfo.header.bpName +
									orderInfo.header.bpName2 + 
								'<br/>';
		
		var bpAddress1= orderInfo.header.bpAddress1;
		var bpAddress2= orderInfo.header.bpAddress2;
		var bpAddress3= orderInfo.header.bpAddress3;
		var bpAddress4= orderInfo.header.bpAddress4;
		var bpCity = orderInfo.header.bpCity;
		var bpPostal = orderInfo.header.bpPostal;
		
		bpAddress1 = (bpAddress1 != null) ? bpAddress1 : '';
		bpAddress2 = (bpAddress2 != null) ? bpAddress2 : '';
		bpAddress3 = (bpAddress3 != null) ? bpAddress3 : '';
		bpAddress4 = (bpAddress4 != null) ? bpAddress4 : '';
		bpCity = (bpCity != null) ? bpCity : '';
		bpPostal = (bpPostal != null) ? bpPostal : '';
		
		var address = orderInfo.header.bpName +
		orderInfo.header.bpName2 + 
		'<br/>'; 
		
		if(bpAddress1 != null && bpAddress1 != '')
		{
			address = address + bpAddress1 + '<br/>';
		}
		
		if(bpAddress2 != null && bpAddress2 != '')
		{
			address = address + bpAddress2 + '<br/>';
		}
		
		if(bpAddress3 != null && bpAddress3 != '')
		{
			address = address + bpAddress3 + '<br/>';
		}
		
		if(bpAddress4 != null && bpAddress4 != '')
		{
			address = address + bpAddress4 + '<br/>';
		}
		
		if(bpCity != null && bpCity != '')
		{
			address = address + bpCity + '<br/>';
		}
		
		
		address = address + bpPostal;
		
		$('orderInfo.customer.address').innerHTML = address;
		
		
		
	
	

	

		
		var orderLines = '<table width="100%" border="0" cellspacing="0" cellpadding="0">' +
							'<thead>' +
								'<tr>' +
									'<th>&nbsp;</th>'+
									'<th>' + Translation.translate('name')+ '</th>'+
									'<th>' + Translation.translate('unit.price')+ '</th>' +
									'<th>' + Translation.translate('total')+ '</th>'+
								'</tr>'+
							'</thead>';
		
		var lineDetails = '<tbody>';
		for (var i=0; i<orderInfo.lines.length; i++)
		{
			var line = orderInfo.lines[i];
			var styleClass = (i%2 == 0) ? 'odd' : 'even';
			lineDetails += '<tr class="'+styleClass+'">' + 
								'<td>' + line.qtyEntered + 	'</td>' + 
								'<td>' + line.productName + '<br>' + line.description +	'</td>' +																	
								'<td>' + Number(line.priceEntered).toFixed(2) + '</td>' +								
								'<td>' + line.lineNetAmt + '</td>' + 
							'</tr>';
			var boms = line.boms;
			if (boms != null)
			{
				for (var j=0; j<boms.length; j++)
				{
					var bom = boms[j];
					var style = (j%2 == 0) ? 'odd' : 'even';
					lineDetails += '<tr class="'+style+'">'+
										'<td>&nbsp;</td>' + 
										'<td>' + bom.qtyEntered + 'x' + 
											bom.productName + '<br>' +
											bom.description + 
										'</td>' + 																	
										'<td>' + Number(bom.priceEntered).toFixed(2) + '</td>' + 								
										'<td>' + Number(bom.lineNetAmt).toFixed(2) + '</td>' +
									'</tr>';
				}
			}
		}
		
		var subTotalDetails = '<tr class="subtotal">' + 
									'<td><strong>'+ orderInfo.header.qtyTotal + 'x </strong></td>';
		subTotalDetails += '<td><strong>';  
		var discountAmt = parseFloat(orderInfo.header.discountAmt).toFixed(2);
		if (discountAmt > 0)
		{
			subTotalDetails += 'Discount <br/>';
		}
		
		subTotalDetails += Translation.translate('subtotal');
		
		var taxes = orderInfo.taxes;
		for (var i=0; i<taxes.length; i++)
		{
			var tax = taxes[i];
			subTotalDetails += '<br/> ' + Translation.translate('tax') + ' - ' + tax.name;
		}
									 
		subTotalDetails += '</strong></td><td>&nbsp;</td>';
		
		subTotalDetails += '<td><strong>';
		if (discountAmt > 0)
		{
			subTotalDetails += discountAmt + '<br/>';
		}
		
		subTotalDetails += orderInfo.header.totalLines ;
		for (var i=0; i<taxes.length; i++)
		{
			var tax = taxes[i];
			subTotalDetails += '<br/>' + parseFloat(tax.amt).toFixed(3);
		}
		
		subTotalDetails += '</strong></td></tr>';
		
		orderLines += lineDetails + subTotalDetails;
		orderLines += '</tbody></table>';
		
		orderLines += '<div class="grandtotal">'+
		'<div class="grandtotal-column1">'+
		Translation.translate('grand.total')+
		'</div>'+
		'<div class="grandtotal-column2">'+
			Number(orderInfo.header.grandTotal).toFixed(2) +
		'</div></div>';
		
		if (orderInfo.header.docStatus == 'CO')
		{
			orderLines += '<div class="docstatus" align="center">';
			if (orderInfo.header.paid)
			{
				orderLines += '<div class="paid"></div>';
			}
			else
			{
				orderLines += '<div class="outstanding"></div>';
			}
			
			if (orderInfo.header.shipped)
			{
				if (orderInfo.header.soTrx)
					orderLines += '<div class="delivered"></div>';
				else
					orderLines += '<div class="received-stamp"></div>';
			}
			orderLines += '</div>';
		}
		else if (orderInfo.header.docStatus == 'VO')
		{
			orderLines += '<div class="voided-stamp"></div>';
		}
		else if (orderInfo.header.docStatus == 'DR')
		{
			orderLines += '<div class="drafted-stamp"></div>';
		}
		
		$('orderInfo.lines').innerHTML = orderLines;
		
		var orderInfoComments = orderInfo.comments;
		
		var commentsHTML = '';
		
		if (orderInfoComments != null)
		{
			for (var i=0; i<orderInfoComments.length; i++)
			{
				var comment = orderInfoComments[i];
				var userName = comment.userName;
				var dateCommented = comment.dateCommented;
				var message = comment.message;
				
				commentsHTML += '<div class="comment">'+
									'<div class="user">' +
										'<div class="employee-comment"></div>' 
										+ userName + 
									'</div>' + 
									'<div class="date">'+
										dateCommented +
									'</div>' +
									'<div class="message">'+
										message + 
									'</div>'+
								'</div>';
			}
		}
		
		$('comments-div').innerHTML = commentsHTML;
		
		this.show();
	}
});


var ShopSavvyProductPanel = Class.create(PopUpBase, {
	initialize: function(shopSavvyProduct, taxCategories) 
	{
	    this.createPopUp($('shopSavvy.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('shopSavvy.popup.okButton');
	    
	    /*labels*/
	    this.barcodeLabel = $('shopSavvy.popup.barcodeLabel');
	    this.nameLabel = $('shopSavvy.popup.nameLabel');
	    this.descriptionLabel = $('shopSavvy.popup.descriptionLabel');
	    this.productPrice = $('shopSavvy.product.price');
	    
	    this.purchasePrice = $('shopSavvy.purchase.price');
	    this.primaryGroup = $('shopSavvy.primary.group');
	    
	    this.taxCategorySelect = $('shopSavvy.tax.category.id');
	    
	    this.group1 = $('shopSavvy.group1');
	    this.group2 = $('shopSavvy.group2');
	    this.group3 = $('shopSavvy.group3');
	    this.group4 = $('shopSavvy.group4');
	    this.group5 = $('shopSavvy.group5');
	    this.group6 = $('shopSavvy.group6');
	    this.group7 = $('shopSavvy.group7');
	    this.group8 = $('shopSavvy.group8');
	    
	    this.image = $('shopSavvy.popup.image');
	    this.closeButton = $('shopSavvy.popup.close.button');
	    /*this.cancelButton = $('shopSavvy.popup.cancelButton')*/
	    /*events*/
	    //this.okBtn.onclick = this.saveProduct.bind(this); 
	    
	    this.okBtn.onclick = this.showConfirmDialog.bind(this);
	    this.closeButton.onclick = this.hide.bind(this);
	    /*this.cancelButton.onclick = this.hide.bind(this);*/
	    
	    this.shopSavvyProduct = shopSavvyProduct;    
	    this.taxCategories = taxCategories;
	    
	    $('shopsavvy-confirm-panel-cancelButton').onclick = function(){jQuery('#shopsavvy-confirm-panel').dialog('close');}
		$('shopsavvy-confirm-panel-okButton').onclick = this.confirmSaveProduct.bind(this);
	}, 
	
	showConfirmDialog : function()
	{
		jQuery('#shopsavvy-confirm-panel-product-name').html(this.descriptionLabel);
		
		jQuery('#shopsavvy-confirm-panel').dialog({
			modal: true,
			title: 'Confirm save product',
			height: 200,
			width: 400,
			position: { my: "center", at: "center", of: window },
		});
		
		this.hide();
	},
	
	confirmSaveProduct : function()
	{
		jQuery('#shopsavvy-confirm-panel').dialog('close');
		this.saveProduct();
	},
	
	saveProduct : function()
	{
		this.hide();
		this.shopSavvyProduct.productPrice = this.productPrice.value;
		
		this.shopSavvyProduct.purchasePrice = this.purchasePrice.value;
		//this.shopSavvyProduct.taxCategory = this.taxCategory.value;		
		this.shopSavvyProduct.taxCategoryId = this.taxCategorySelect.value;
		this.shopSavvyProduct.primaryGroup = this.primaryGroup.value;
		
		this.shopSavvyProduct.group1 = this.group1.value;
		this.shopSavvyProduct.group2 = this.group2.value;
		this.shopSavvyProduct.group3 = this.group3.value;
		this.shopSavvyProduct.group4 = this.group4.value;
		this.shopSavvyProduct.group5 = this.group5.value;
		this.shopSavvyProduct.group6 = this.group6.value;
		this.shopSavvyProduct.group7 = this.group7.value;
		this.shopSavvyProduct.group8 = this.group8.value;		
		
		var params = 'action=saveShopSavvyProduct&shopSavvy=' + encodeURIComponent(Object.toJSON(this.shopSavvyProduct));
		try
		{	
			var myAjax = new Ajax.Request('ShopSavvyAction.do', 
			{ 
				method: 'post', 
				parameters: params,
				onSuccess: this.afterSave.bind(this)
			});	
			
		}
		catch(e)
		{} 
	},
	
	afterSave : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		
		if (json.error)
		{
			alert(json.message);
			return;
		}
		
		shoppingCart.addToCart(json.productId,1);
	},
	
	clear : function()
	{
		this.barcodeLabel.innerHTML = '';
		this.nameLabel.innetaxCategoriesrHTML = '';
		this.descriptionLabel.innerHTML = '';
		this.image.src = '';
	},
	
	onShow : function()
	{
		this.nameLabel.innerHTML = this.shopSavvyProduct.title;
		this.descriptionLabel.innerHTML = this.shopSavvyProduct.title;
		this.barcodeLabel.innerHTML = this.shopSavvyProduct.id;
	    this.image.src = this.shopSavvyProduct.thumbnailUrl;
	    
	    var option = '';
	    for(var i=0; i<this.taxCategories.length; i++)
	    {
	    	option = option + "<option value='" + this.taxCategories[i].c_taxcategory_id + "'>"+ this.taxCategories[i].name +"</option>";
	    }
	    
	    this.taxCategorySelect.innerHTML = option;
	    
	    this.productPrice.value='';
	    this.purchasePrice.value='';
	    this.primaryGroup.value = '';
	    this.group1.value = '';
		this.group2.value = '';
		this.group3.value = '';
		this.group4.value = '';
		this.group5.value = '';
		this.group6.value = '';
		this.group7.value = '';
		this.group8.value = '';
		
		$('shopSavvy.primary.group').onkeyup = function(e)
		{
			$('group1').style.display = 'block';
		}
		$('shopSavvy.group1').onkeyup = function(e)
		{
			$('group2').style.display = 'block';
		}
		$('shopSavvy.group2').onkeyup = function(e)
		{
			$('group3').style.display = 'block';
		}
		$('shopSavvy.group3').onkeyup = function(e)
		{
			$('group4').style.display = 'block';
		}
		$('shopSavvy.group4').onkeyup = function(e)
		{
			$('group5').style.display = 'block';
		}
		$('shopSavvy.group5').onkeyup = function(e)
		{
			$('group6').style.display = 'block';
		}
		$('shopSavvy.group6').onkeyup = function(e)
		{
			$('group7').style.display = 'block';
		}
		$('shopSavvy.group7').onkeyup = function(e)
		{
			$('group8').style.display = 'block';
		}
	}
});


var ErrorPopUpPanel = Class.create(PopUpBase, {
	initialize: function(error) 
	{
	    this.createPopUp($('error.popup'));
	    
	    /*buttons*/
	    this.okBtn = $('error.popup.okBtn');
	    this.cancelButton = $('error.popup.cancelBtn')
	    /*events*/
	    this.okBtn.onclick = this.hide.bind(this); 
	    this.cancelButton.onclick = this.hide.bind(this);
	    
	    this.error = error;  
	    this.errorMsgContainer = $('error.popup.msg');
	},
	
	init : function()
	{
		var errorMessage = '';
		for (var key in this.error)
		{
			var msg = this.error[key];
			errorMessage += key + ' : ' + msg + '<br>';
		}
		
		if (errorMessage != '')
		{
			this.errorMsgContainer.innerHTML = errorMessage;
			
			this.show();
		}
	},
	
	onShow : function()
	{
		jQuery('#error-content').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	}
});

var AlertPopUpPanel = Class.create(PopUpBase, {
	initialize: function(error) 
	{
	    this.createPopUp($('alert.popup'));
	    
	    /*buttons*/
	    this.okBtn = $('alert.popup.okBtn');
	    this.cancelButton = $('alert.popup.cancelBtn')
	    /*events*/
	    this.okBtn.onclick = this.hide.bind(this); 
	    this.cancelButton.onclick = this.hide.bind(this);
	    
	    this.error = error;  
	    this.errorMsgContainer = $('alert.popup.msg');
	    this.errorMsgContainer.innerHTML = error;
	},
		
	onShow : function()
	{
		
	}
});

/*Overriding window.alert*/
/*var alert = function(msg){
	new AlertPopUpPanel(msg).show();
};*/

/* PhotoBooth Panel */
var PhotoBoothPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('photoBooth.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('photoBooth.popup.okButton');
	    	    
	    /*events*/
	    this.okBtn.onclick = this.hide.bind(this); 
	    
	    this.frame = $('photo-booth-frame');
	    	    
	  }, 
	 	 
	  onShow:function(){
	  	
	  }
});

/* Terminal Panel */
var TerminalInfoPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('terminal-popup-panel'));
	    	    
	    /*buttons*/
	    this.okBtn = $('terminal-popup-okButton');
	    	    
	    /*events*/
	    this.okBtn.onclick = this.hide.bind(this);
	    	    
	  }, 
	 	 
	  onShow:function(){

		  var terminal = TerminalManager.terminal;
		  
		  if(terminal == null) {
			  alert(Translation.translate('please.choose.a.terminal','Please choose a terminal'));
			  this.hide();
			  return;
		  }

		  $('terminal-popup-organisation-label').innerHTML = terminal.org_name;
		  $('terminal-popup-name-label').innerHTML = terminal.terminal_name;
		  $('terminal-popup-client-label').innerHTML = terminal.client_name;
		  $('terminal-popup-default-customer-label').innerHTML = terminal.bp_name;
		  $('terminal-popup-sales-price-list-label').innerHTML = terminal.so_pricelist;
		  $('terminal-popup-purchase-price-list-label').innerHTML = terminal.po_pricelist;
		  $('terminal-popup-warehouse-label').innerHTML = terminal.warehouse_name;	
		  
	  },

	  onHide:function(){
		  
	  }
});

/* Clock In Out Panel */
var ClockInOutPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('clock-in-out-panel'));
	    
	    /*inputs*/
	    this.usernameTextfield = $('clock-in-out-panel-username-textfield');
	    this.passwordTextfield = $('clock-in-out-panel-password-textfield');
	    	    
	    /*buttons*/
	    this.clockBtn = $('clock-in-out-panel-clock-button');
	    //this.keyboardBtn = $('clock-in-out-panel-keyboard-button');
	    this.closeBtn = $('clock-in-out-panel-close-button');
	    
	    	    
	    /*events*/
	   // this.keyboardBtn.onclick = Keyboard.toggle.bind(Keyboard);
	    this.clockBtn.onclick = this.clockInOutUser.bind(this);
	    this.closeBtn.onclick = this.hide.bind(this);
	    
	    this.passwordTextfield.panel = this;
	    this.passwordTextfield.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.clockInOutUser();			
			}
		};
	    
	    /*register*/
	    ClockInOutManager.listeners.push(this); 
	       	    
	  }, 
	 	 
	  onShow:function(){
		  /*render clocked in user list*/	
		 // this.renderClockedInUsersList();
		  if(this.usernameTextfield.value == '')
		  {
			  this.usernameTextfield.focus();
		  }
		  else
		  {
			  this.passwordTextfield.focus();
		  }
		  
	  },

	  onHide:function(){
		  ClockInOut.renderList();
		  ClockInOutManager.listeners.pop(this); 
	  },
	  
	  resetHandler:function(){
		  this.usernameTextfield.value = '';
		  this.passwordTextfield.value = '';		  
	  },
	  
	  /*renderClockedInUsersList:function(){
		  var table = this.clockedInUsersTable;
		  
		  remove rows
		  var rowsLength = table.rows.length;
		  for(var i=0; i<rowsLength; i++){
			  table.deleteRow(0);
		  }
		  
		  var salesRepsList = ClockInOutManager.clockedInUsers;
		  
		  for(var i=0; i<salesRepsList.length; i++){
			  var salesRep = salesRepsList[i];
			  var username = salesRep.username;
			  var clockInTime = salesRep.clockInTime;
			  
			  var row = table.insertRow(table.rows.length);
			  
			  var cell = row.insertCell(0);
			  cell.innerHTML = username;
			  
			  cell.username = username;
			  cell.onclick = function(e){
				  var username = this.username;
				  $('clock-in-out-panel-username-textfield').value = username;
				  $('clock-in-out-panel-password-textfield').value = '';
				  $('clock-in-out-panel-password-textfield').focus();
			  };
			  
			  cell = row.insertCell(1);
			  cell.innerHTML = clockInTime;			  
		  }
	  },*/
	  
	  renderClockedInUsersList : function()
	  {
		  ClockInOut.renderList ();
	  },
	  
	  setUser : function(username)
	  {
		  this.username = username;
		  $('clock-in-out-panel-username-textfield').value = username;
		  $('clock-in-out-panel-password-textfield').value = '';
		  $('clock-in-out-panel-password-textfield').focus();
	  },
	  
	  clockInOutUser:function(){
		  var username = this.usernameTextfield.value;
		  var password = this.passwordTextfield.value;
		  var date = DateUtil.getCurrentDate();
		  
		  ClockInOutManager.clockInOutUser(username, password, date);
		 /* this.hide();*/
	  },
	  
	  clockInOutUserHandler : function(message, clockedInUsersList, clockIn){
		  
		  /*this.hide();*/
		  
		  if("OK" != message){
			  alert(message);
			  return;
		  }
		  
		  if(clockIn){
			  alert(Translation.translate('user.successfully.clocked.in','User successfully clocked in'));
		  }
		  else
		  {
			  alert(Translation.translate('user.successfully.clocked.out','User successfully clocked out'));
		  }
		  
		  
		  this.hide();
		  
		  /*render clocked in user list*/	
		  this.renderClockedInUsersList();
	  }
	  
});

/* Information popup */
var InfoPanel = Class.create(PopUpBase, {
	initialize: function() {
    	this.createPopUp($('info-popup-panel'));
    	
    	/*buttons*/
	    this.okBtn = $('info-popup-panel-ok-button');
	    this.changeTerminalBtn = $('info-popup-panel-change-terminal-button');
	    this.closeBtn = $('info-popup-panel-close-button');
	    /*events*/
	    this.okBtn.onclick = this.hide.bind(this);
	    this.closeBtn.onclick = this.hide.bind(this);
	    if(this.changeTerminalBtn){
	    	this.changeTerminalBtn.onclick = this.changeTerminal.bind(this);
	    }	    	
    },
    
    changeTerminal:function(){
    	this.showChangeTerminalPopUp = true;
    	this.hide();
    },
    
    onHide:function(){
    	if(this.showChangeTerminalPopUp)
    	{
    		new ChangeTerminalPopUp().show();
    	}
    }
});

/*Notifications*/
var MessagePopUp = Class.create(PopUpBase, {
	initialize: function() {
    	this.createPopUp($('message-popup'));
	    
	    /*events*/
	    
	    var btn = $('message-popup-ok-btn');
	    btn.onclick = this.hide.bind(this);
    }
});

var ErrorPopUp = Class.create(PopUpBase, {
	initialize: function() {
    	this.createPopUp($('error-popup'));
	    
	    /*events*/
	    this.container.onclick = this.hide.bind(this);	    	
    }
});

var CommentPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('comment-panel'));
	    
	    /*inputs*/
	    this.commentTextfield = $('comment-panel-comments-textfield');
	    	    
	    /*buttons*/
	    this.postBtn = $('comment-panel-post-button');
	    this.closeBtn = $('comment-panel-close-button');
	    	    
	    /*events*/	    
	    this.closeBtn.onclick = this.hide.bind(this);
	  }, 
	 	 
	  onShow:function(){
		  
	  },

	  onHide:function(){
		  
	  },
	  
	  resetHandler:function(){
		  this.commentTextfield.value = '';	  
	  },
	  
	  postComment : function(){
	  
	  },
	  
	  renderComments : function(request){
		  
	  }
	  
});

/* Org Access Panel */
var OrgAccessPanel =  Class.create(PopUpBase, {
	  
	initialize: function() {
	    this.createPopUp($('role.org.access.panel'));
	    this.roleName = $('role.org.access.role.name');
	    this.roleId = $('role.org.access.role.id');
	    this.availableOrgsPanel = $('availableOrgs');
	    this.chosenOrgsPanel = $('chosenOrgs');
	    this.roleId = $('roleId').value;
	    
	    /*buttons*/
	    this.saveBtn = $('role.org.access.save.button');
	    
	    this.closeButton = $('role.org.access.close.button');
	    
	    /*events*/
	    this.saveBtn.onclick = this.hidePanel.bind(this); 
	    this.closeButton.onclick = this.hidePanel.bind(this);
	    	    
	  }, 
	  
	  getInfo:function(){
		  
		  url = 'RoleAction.do?action=initEditRoleOrgAccess&roleId=' + this.roleId;
			var myAjax = new Ajax.Request( url, 
				{ 
					method: 'POST', 
					onSuccess: this.getInfoSuccess.bind(this), 
					onFailure: ''
				});
	  },
	  
	  getInfoSuccess:function(request){
		  var list = eval('('+request.responseText+')');

			var availableOrgsList = list.availableOrgs;

			var availableOrgsSelect = document.createElement('select');
			availableOrgsSelect.setAttribute('id', 'availableOrgsSelect');
			availableOrgsSelect.setAttribute('multiple', 'true');

			for(var i=0; i<availableOrgsList.length; i++)
			{
				var option = document.createElement('option');
				option.setAttribute('value', availableOrgsList[i].orgId);
				option.innerHTML = availableOrgsList[i].orgName;
				availableOrgsSelect.appendChild(option);
			}

			
			this.availableOrgsPanel.appendChild(availableOrgsSelect);


			var chosenOrgsList = list.chosenOrgs;
			this.roleName.innerHTML = chosenOrgsList[0].roleName;
			this.roleId.value = chosenOrgsList[0].roleId;
			
			var chosenOrgsSelect = document.createElement('select');
			chosenOrgsSelect.setAttribute('id', 'chosenOrgsSelect');
			chosenOrgsSelect.setAttribute('multiple', 'true');

			for(var i=0; i<chosenOrgsList.length; i++)
			{
				var option = document.createElement('option');
				option.setAttribute('value', chosenOrgsList[i].orgId);
				option.innerHTML = chosenOrgsList[i].orgName;
				chosenOrgsSelect.appendChild(option);
			}

			this.chosenOrgsPanel.appendChild(chosenOrgsSelect);

			var transfer = new Transfer('availableOrgsSelect', 'chosenOrgsSelect', 'availableOrgsInput','chosenOrgsInput');
			transfer.init();
		    transfer.updateInputs();
		    this.show();
	  },
	 	 
	  onShow:function(){
	  	/*display info*/
		
	    },
	    
	  hidePanel : function(){
	    	this.availableOrgsPanel.innerHTML = '';
	    	this.chosenOrgsPanel.innerHTML = '';
	    	this.hide();
	    }
	    	
});
    
var ProductSearchErrorPanel = Class.create(PopUpBase, {
	initialize : function(barcode)
	{
		this.createPopUp($('product.search.error.panel'));
		$('product.search.error.barcode').innerHTML = barcode;
		this.okBtn = $('product.search.error.ok');
		this.cancelBtn = $('product.search.error.cancel');
		
		this.okBtn.onclick = this.okHandler.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	onShow : function()
	{
		SearchProduct.disableTextFieldFocus();
		$('product.search.error.panel').focus();
	},
	
	okHandler : function()
	{
		this.hide();
		SearchProduct.clearTextField();
		SearchProduct.highlightTextField();
	}
});

/* Invoke Order Panel */
var InvokeOrderPanel =  Class.create(PopUpBase, {
	
	  appendHTML : function() {
		  
		  var html = ' <div class="modal" style="display: block; visibility: visible; left: 0px; top: 10px; z-index: 100000;" id="invokeOrder-popup-panel" tabindex="-1" role="dialog" aria-labelledby="invokeOrder-popup-panel" aria-hidden="true"> ' +
			' 	<div class="modal-dialog modal-sm"> ' +
			' 		<div class="modal-content"> ' +
			' 			<div class="modal-header"> ' +
			' 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="invokeOrder-popup-cancelButton">×</button> ' +
			' 				<h4 class="modal-title"> ' +
			' 					<div class="glyphicon glyphicon-folder-open"></div> ' +
			' 					&nbsp;' + Translation.translate('load.order') +
			' 				</h4> ' +
			' 			</div> ' +
			' 			<div class="modal-body"> ' +
			' 				<div class="row"> ' +
			' 					<div class="col-xs-12 col-md-12 popup-label"> ' + Translation.translate('store') + 
			' 					</div> ' +
			' 				</div> ' +
			' 				<div class="row"> ' +
			' 					<div class="col-xs-12 col-md-12"> ' +
			' 						<select class="input-sm" name="ad_org_id" id="invokeOrder-popup-organisation-dropdown"> ' +
			' 							<option value="0">*</option> ' +
			' 						</select> ' +
			' 					</div> ' +
			' 				</div> ' +
			' 				<div class="row"> ' +
			' 					<div class="float-left popup-label">' + Translation.translate('order.no') + ':</div> ' +
			' 				</div> ' +
			' 				<div class="row"> ' +
			' 					<div class="input-group float-left input-sm"> ' +
			' 						<input type="text" class="form-control textField input-sm" placeholder="" id="invokeOrder-popup-documentNumberTextField"> ' +
			' 					</div> ' +
			' 				</div> ' +
			
			' 				<div class="row"> ' +
			' 					<div class="float-left popup-label">' + Translation.translate('order.ref.no') + ':</div> ' +
			' 				</div> ' +
			' 				<div class="row"> ' +
			' 					<div class="input-group float-left input-sm"> ' +
			' 						<input type="text" class="form-control textField input-sm" placeholder="" id="invokeOrder-popup-referenceTextField"> ' +
			' 					</div> ' +
			' 				</div> ' +				
			
			' 			</div> ' +
			' 			<div class="modal-footer"> ' +
			' 				<div class="row"> ' +
			' 					<button type="button" class="btn btn-xs btn-success" style="width:20%;" id="invokeOrder-popup-okButton"> ' +
			' 						<div class="glyphicons glyphicons-pro ok_2"></div> ' +
			' 						<div>' + Translation.translate('ok') + '</div> ' +
			' 					</button> ' +
			' 				</div> ' +
			' 			</div> ' +
			' 		</div> ' +
			' 	</div> ' +
			' </div> ';
		  
		jQuery(document.body).append(html);
		
		this.organisationSelectList = $('invokeOrder-popup-organisation-dropdown');	 
		
		var terminals = TerminalManager.terminalList;
		var organisations = new Array();
  
		/* this.organisationSelectList.options.length = 0; */
  
		for(var i=0; i<terminals.length; i++)
		{
			var terminal = terminals[i];
			var orgName = terminal.org_name;
			var orgId = terminal.org_id;
		
			if(organisations.indexOf(orgId) == -1){
				organisations.push(orgId);
				this.organisationSelectList.options[this.organisationSelectList.options.length] = new Option(orgName, orgId, false, false)					
			}   	   			
		}
	  
		var orgId = TerminalManager.terminal.orgId;
		this.organisationSelectList.setValue(orgId);
	  },
	
	  initialize: function() {
		
		if(jQuery('#invokeOrder-popup-panel').length == 0) {
			
			this.appendHTML();
		}
		
		this.organisationSelectList = $('invokeOrder-popup-organisation-dropdown');
		
	    this.createPopUp($('invokeOrder-popup-panel'));
	    
	    /*buttons*/
	    this.okBtn = $('invokeOrder-popup-okButton');
	    this.cancelBtn = $('invokeOrder-popup-cancelButton');
	    
	    /*textfields*/
	    this.documentNumberTextField = $('invokeOrder-popup-documentNumberTextField');	
	    this.referenceTextField = $('invokeOrder-popup-referenceTextField');
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.documentNumberTextField.panel = this;
	    this.documentNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};
		
		this.referenceTextField.panel = this;
	    this.referenceTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};
	    
	  }, 
	  validate:function(){
	  	if(this.documentNumberTextField.value == null || this.documentNumberTextField.value == ''){
	  		if(this.referenceTextField.value == null || this.referenceTextField.value == ''){
	  			this.onError('invalid.documentno');
				this.documentNumberTextField.focus();
				return;
	  		}
			
		}
		
	  	var url = 'OrderAction.do?action=invoke&documentNo=' + encodeURIComponent(this.documentNumberTextField.value) + '&referenceNo=' + encodeURIComponent(this.referenceTextField.value) + '&ad_org_id=' + this.organisationSelectList.options[this.organisationSelectList.selectedIndex].value;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});	
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.documentNumberTextField.value = '';
	  },
	  onShow:function(){
	  	this.documentNumberTextField.focus();
	  },
	  /* ajax callbacks */
	  onComplete:function(request){		  
	  },
	  onSuccess:function(request){	  		
  		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
  		
  		if(result.error){
  			/* this.hide(); */
  			alert(result.error);
  			return;
  		}  		
		
  		/*if (result.editable)
  			get('OrderAction.do?action=loadOrderScreen&orderType=' + result.orderType);
  		else
  			getURL('OrderAction.do?action=view&orderId=' + result.orderId);*/
  		
  		getURL('OrderAction.do?action=view&orderId=' + result.orderId);
	  	
		this.hide();
	  },
	  onFailure:function(request){	
		   /* this.hide(); */
		  alert("failed.to.process.request");
	  }
});

/* Invoke Quotation Panel */
var InvokeQuotationPanel =  Class.create(PopUpBase, {
	
	  appendHTML : function() {
		  
		  var html = ' <div class="modal" style="display: block; visibility: visible; left: 0px; top: 10px; z-index: 100000;" id="invokeQuotation-popup-panel" tabindex="-1" role="dialog" aria-labelledby="invokeQuotation-popup-panel" aria-hidden="true"> ' +
			' 	<div class="modal-dialog modal-sm"> ' +
			' 		<div class="modal-content"> ' +
			' 			<div class="modal-header"> ' +
			' 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="invokeQuotation-popup-cancelButton">×</button> ' +
			' 				<h4 class="modal-title"> ' +
			' 					<div class="glyphicon glyphicon-folder-open"></div> ' +
			' 					&nbsp;' + Translation.translate('load.quotation') +
			' 				</h4> ' +
			' 			</div> ' +
			' 			<div class="modal-body"> ' +							
			' 				<div class="row"> ' +
			' 					<div class="float-left popup-label">' + Translation.translate('quotation.no') + ':</div> ' +
			' 				</div> ' +
			' 				<div class="row"> ' +
			' 					<div class="input-group float-left input-sm"> ' +
			' 						<input type="text" class="form-control textField input-sm" placeholder="" id="invokeQuotation-popup-documentNumberTextField"> ' +
			' 					</div> ' +
			' 				</div> ' +			
			' 			</div> ' +
			' 			<div class="modal-footer"> ' +
			' 				<div class="row"> ' +
			' 					<button type="button" class="btn btn-xs btn-success" style="width:20%;" id="invokeQuotation-popup-okButton"> ' +
			' 						<div class="glyphicons glyphicons-pro ok_2"></div> ' +
			' 						<div>' + Translation.translate('ok') + '</div> ' +
			' 					</button> ' +
			' 				</div> ' +
			' 			</div> ' +
			' 		</div> ' +
			' 	</div> ' +
			' </div> ';
		  
		jQuery(document.body).append(html);		
		
	  },
	
	  initialize: function() {
		
		if(jQuery('#invokeQuotation-popup-panel').length == 0) {
			
			this.appendHTML();
		}
		
		this.createPopUp($('invokeQuotation-popup-panel'));
	    
	    /*buttons*/
	    this.okBtn = $('invokeQuotation-popup-okButton');
	    this.cancelBtn = $('invokeQuotation-popup-cancelButton');
	    
	    /*textfields*/
	    this.documentNumberTextField = $('invokeQuotation-popup-documentNumberTextField');	
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.documentNumberTextField.panel = this;
	    this.documentNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};
	    
	  }, 
	  validate:function(){
	  	if(this.documentNumberTextField.value == null || this.documentNumberTextField.value == ''){
	  		this.onError('invalid.quotation.no');
			this.documentNumberTextField.focus();
			return;			
		}
		
	  	var url = 'OrderAction.do?action=invokeQuotation&documentNo=' + this.documentNumberTextField.value;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});	
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.documentNumberTextField.value = '';
	  },
	  onShow:function(){
	  	this.documentNumberTextField.focus();
	  },
	  /* ajax callbacks */
	  onComplete:function(request){		  
	  },
	  onSuccess:function(request){	  		
  		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
  		
  		if(result.error){
  			/* this.hide(); */
  			alert(result.error);
  			return;
  		}  		
		
  		/*if (result.editable)
  			get('OrderAction.do?action=loadOrderScreen&orderType=' + result.orderType);
  		else
  			getURL('OrderAction.do?action=view&orderId=' + result.orderId);*/
  		
  		getURL('OrderAction.do?action=view&orderId=' + result.orderId);
	  	
		this.hide();
	  },
	  onFailure:function(request){	
		   /* this.hide(); */
		  alert("failed.to.process.request");
	  }
});


var CreateVendorPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
		this.createPopUp($('create.vendor.popup.panel'));
		this.cancelBtn = $('create.vendor.popup.close.button');
		
		this.cancelBtn.onclick = this.hide.bind(this);
		
		this.vendor = null;
	},
	
	onShow : function()
	{
		$('vendor.fields').innerHTML = VendorFields.fields;
		
		this.initFields();
	},
	
	initFields : function()
	{
		var params = new Hash();
		params.set('controller', 'Vendor');
		params.set('id', 0);
		try
		{	
			var myAjax = new Ajax.Request('ModelAction.do?action=loadPanel',
			{
				method: 'POST',
				parameters: params,
				onSuccess: this.displayFields.bind(this)				
			});
		}
		catch(e)
		{}
	},
	
	displayFields : function(response)
	{
		var json = eval('('+response.responseText+')');
		
		this.vendor = new VendorController(json);
		this.vendor.setHelpComp('new-vendor-help');
		this.vendor.init();
		
		
		this.saveBtn = $('create.vendor.popup.saveBtn');
		this.saveBtn.onclick = this.vendor.save.bind(this.vendor);
		this.vendor.addOnSaveListener(this);
	},
	
	controllerSaved : function(vendorId)
	{
		var hash = new Hash();
		hash.set('c_bpartner_id', vendorId);
		hash.set('name', this.vendor.getField('vendor_partner.Name').getValue());
		hash.set('email', this.vendor.getField('vendor_user.EMail').getValue());
		hash.set('address1', this.vendor.getField('vendor_location.Address1').getValue());
		hash.set('city', this.vendor.getField('vendor_location.City').getValue());
		hash.set('phone', this.vendor.getField('vendor_user.Phone').getValue());
		
		var json = eval('(' + hash.toJSON() + ')');
		var bp = new BP(json);
		BPManager.setBP(bp);
		
		$('search-bp-textfield').value = bp.getName();
		OrderScreen.setBPartnerId(bp.getId());
		
		this.hide();
	}
		
});

var CreateCustomerPanel2 = Class.create(PopUpBase, {
	
	initialize : function()
	{
		this.createPopUp($('create.customer.popup.panel'));
		this.cancelBtn = $('create.customer.popup.close.button');
		
		this.cancelBtn.onclick = this.hide.bind(this);
		
		this.customer = null;
		
		this.cancelBtn.onkeyup = this.closePopUp.bind(this);
	},
	
	onShow : function()
	{
		$('customer.fields').innerHTML = CustomerFields.fields;
		
		this.initFields();
	},
	
	initFields : function()
	{
		var params = new Hash();
		params.set('controller', 'Customer');
		params.set('id', 0);
		try
		{	
			var myAjax = new Ajax.Request('ModelAction.do?action=loadPanel',
			{
				method: 'POST',
				parameters: params,
				onSuccess: this.displayFields.bind(this),
				on401: this.accessDenied.bind(this)				
			});
		}
		catch(e)
		{}
	},
	
	displayFields : function(response)
	{
		var json = eval('('+response.responseText+')');
		
		this.customer = new PartnerController(json);
		this.customer.setHelpComp('new-customer-help');
		this.customer.init();
		
		this.nameField = $('partner.Name');
		this.saveBtn = $('create.customer.popup.saveBtn');
		this.customer.addOnSaveListener(this);
		this.saveBtn.onclick = this.saveCustomer.bind(this);
		
		this.cancelBtn.focus();
		$('search-product-textfield').blur();
		this.nameField.focus();
		this.emailReceipt = $('partner.EmailReceipt');
		this.emailReceipt.checked = true;
	},
	
	saveCustomer : function()
	{
		this.customer.save();
	},
	
	controllerSaved : function(customerId)
	{
		var hash = new Hash();
		hash.set('c_bpartner_id', customerId);
		hash.set('name', this.customer.getField('partner.Name').getValue());
		hash.set('email', this.customer.getField('user.EMail').getValue());
		hash.set('address1', this.customer.getField('location.Address1').getValue());
		hash.set('city', this.customer.getField('location.City').getValue());
		hash.set('phone', this.customer.getField('user.Phone').getValue());
		
		var json = eval('(' + hash.toJSON() + ')');
		var bp = new BP(json);
		BPManager.setBP(bp);
		
		$('search-bp-textfield').value = bp.getName();
		OrderScreen.setBPartnerId(bp.getId());
		
		this.hide();
	},
	
	 accessDenied : function(request){
		this.hide();
		request.responseText;
	  },
	  
	closePopUp : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN)
		{
			this.hide();
			$('search-product-textfield').focus();
			return false;
		}
	}
		
});

var CreateProductPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
		this.createPopUp($('create.product.popup.panel'));
		this.cancelBtn = $('create.product.popup.close.button');
		
		this.cancelBtn.onclick = this.hide.bind(this);
		this.cancelBtn.onkeyup = this.closePanel.bind(this);
		
		this.product = null;
	},
	
	onShow : function()
	{
		$('items.fields').innerHTML = ItemFields.fields;
		
		this.initFields();
		
		this.cancelBtn.focus();
	},
	
	initFields : function()
	{
		var params = new Hash();
		params.set('controller', 'Product');
		params.set('id', 0);
		try
		{	
			var myAjax = new Ajax.Request('ModelAction.do?action=loadPanel',
			{
				method: 'POST',
				parameters: params,
				onSuccess: this.displayFields.bind(this)				
			});
		}
		catch(e)
		{}
	},
	
	displayFields : function(response)
	{
		var json = eval('('+response.responseText+')');
		
		this.product = new ProductController(json);
		this.product.setHelpComp('new-item-help');
		this.product.init();
		
		
		this.saveBtn = $('create.product.popup.saveBtn');
		this.product.addOnSaveListener(this);
		this.saveBtn.onclick = this.saveProduct.bind(this);
		
	},
	
	saveProduct : function()
	{
		this.product.save();
	},
	
	controllerSaved : function(productId)
	{
		shoppingCart.addToCart(productId, 1);
		this.hide();
	},
	
	closePanel : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN)
		{
			this.hide();
		}
	}
});

/*Sign Up with Code Panel*/
var SignUpPopUpPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('signup.code.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('signup.popup.code.okbutton');
	    this.cancelBtn = $('signup.popup.code.cancelButton');
	    
	    /*textfields*/
	    this.signUpCodeTextField = $('signup.popup.code.text.field');	    
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.signUpCodeTextField.panel = this;
	    this.signUpCodeTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};
	    
	  }, 
	  validate:function(){
	  	if(this.signUpCodeTextField.value == null || this.signUpCodeTextField.value == ''){
			this.onError('Invalid Code');
			this.signUpCodeTextField.focus();
			return;
		}
		
	  	var url = 'CreateOrganizationBusinessAction.do?action=unlockRestrictedCountries&code=' + this.signUpCodeTextField.value;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});	
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.signUpCodeTextField.value = '';
	  },
	  onShow:function(){
	  	this.signUpCodeTextField.focus();
	  },
	  /* ajax callbacks */
	  onComplete:function(request){		  
	  },
	  onSuccess:function(request){	  		
  		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
  		
  		 
  		if(result.error){
  			this.onError(result.errorMessage);
  			this.hide();
  			return;
  		}
		
  		$('country').innerHTML = result.countryList;
  		$('currency').innerHTML = result.currencyList;
  		$('is-sign-up-code-valid').value = true;
  		this.hide();
	  },
	  onFailure:function(request){	
		  this.onError("failed.to.process.request");
	  }
});

/*Translation Panel*/
var TranslationPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('translation-panel'));
	    
     	    this.okBtn = $('translation.panel.okbutton');
	    this.cancelBtn = $('translation.panel.cancelButton');

	    this.cancelBtn.onclick = this.hide.bind(this);
	  },
	  
	  show: function()
		{
			PopUpBase.show.call(this);
			
			if (window.top != window.self)
			{
				var div = $(parent.document.getElementById('tree-mask'));
				
				if (div != null)
				{
					div.style.display = 'block';
				}
			}
		},
		
		hide : function()
		{
			PopUpBase.hide.call(this);
			
			if (window.top != window.self)
			{
				var div = $(parent.document.getElementById('tree-mask'));
				
				if (div != null)
				{
					div.style.display = 'none';
				}
			}
		}
});

/*Statement of Account*/
var StatementOfAccountPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('statement-of-account-popup-panel'));
	    
	    /*buttons*/
	    this.okBtn = $('statement.of.account.okbutton');
	    this.cancelBtn = $('statement.of.account.cancel.button');
	    this.sendMailBtn = $('send-mail-btn');
	    this.exportBtn = $('statement.of.account.export-button');
	    
	    /*events*/
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    this.cancelBtn.onclick = this.cancelHandler.bind(this);
	    this.sendMailBtn.onclick = this.sendMail.bind(this);
	    this.exportBtn.onclick = this.exportPDF.bind(this);

	    this.panel = $('statement.of.account.content');
	    this.panel.innerHTML = '';

	    this.bpName = $('bp-name');
	    this.bpAddress = $('bp-address');
	    this.bpAddress2 = $('bp-address2');
	    this.bpAddress3 = $('bp-address3');
	    this.bpAddress4 = $('bp-address4');
	    this.bpCity = $('bp-city');
	    this.bpState = $('bp-state');
	    this.bpPostal = $('bp-postal');
	    this.bpPostalAdd = $('bp-postal-add');
	    this.bpCountry = $('bp-country');
	    
	    this.orgName = $('org-name');
	    this.orgAddress = $('org-address');
	    this.orgAddress2 = $('org-address2');
	    this.orgAddress3 = $('org-address3');
	    this.orgAddress4 = $('org-address4');
	    this.orgCity = $('org-city');
	    this.orgState = $('org-state');
	    this.orgPostal = $('org-postal');
	    this.orgPostalAdd = $('org-postal-add');
	    this.orgCountry = $('org-country');
	    this.orgCity = $('org-city');
	    
        this.bpartnerId = null;
	    this.dateFrom = null;
	    this.dateTo = null;
	    this.statusWindow = null;
	  }, 

	  init : function(bpartnerId, dateFrom, dateTo){
	  	this.bpartnerId = bpartnerId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
 	  },

	  okHandler:function(){	
		this.hide();  
	  },

	  cancelHandler:function(){	
		this.hide();  
	  },

	  sendMail:function(){
		var url = 'BPartnerAction.do?action=sendStatementOfAccount&customerId=' + this.bpartnerId + '&dateFrom=' + this.dateFrom + '&dateTo=' +this.dateTo;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSendMailSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});
		
		this.statusWindow = new ModalWindow('', false);
		this.statusWindow.div.style.zIndex = '99999999';
		this.statusWindow.show();
	  },

	  resetHandler:function(){
	  },

	  onShow:function(){

	  	var url = 'BPartnerAction.do?action=getStatementOfAccount&customerId=' + this.bpartnerId + '&dateFrom=' + this.dateFrom + '&dateTo=' +this.dateTo;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});
	  },

	  /* ajax callbacks */
	  onComplete:function(request){		  
	  },

	  onSuccess:function(request){	  		
  		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');

		var statementOfAccountLines = result.statementOfAccountLines;
		var bp = result.bp;
		var org = result.org;
		var openBalance = result.openingBalance;

		this.bpName.innerHTML = bp.name + bp.name2;
    	this.bpAddress.innerHTML = this.validateField(bp.address);
    	this.bpAddress2.innerHTML = this.validateField(bp.address2);
	    this.bpAddress3.innerHTML = this.validateField(bp.address3);
	    this.bpAddress4.innerHTML = this.validateField(bp.address4);
	    this.bpCity.innerHTML = this.validateField(bp.city);
	    
	    var state = this.validateField(bp.state);
	    if (state != null || state != 'null' || state != '')
	    {
	    	this.bpState.innerHTML = ', ' + state;
	    }
	    
	    this.bpPostal.innerHTML = this.validateField(bp.postal);
	    
	    if (!this.validateField(bp.postalAdd) == this.validateField(bp.postal))
	    {
	    	this.bpPostalAdd.innerHTML = this.validateField(bp.postalAdd);
	    }
	    
	    this.bpCountry.innerHTML = this.validateField(bp.country);
	    
	    this.orgName.innerHTML = this.validateField(org.name);
	    this.orgAddress.innerHTML = this.validateField(org.address);
	    this.orgAddress2.innerHTML = this.validateField(org.address2);
	    this.orgAddress3.innerHTML = this.validateField(org.address3);
	    this.orgAddress4.innerHTML = this.validateField(org.address4);
	    this.orgCity.innerHTML = this.validateField(org.city);
	    
	    state = this.validateField(org.state);
	    if (state != null || state != 'null' || state != '')
	    {
	    	this.orgState.innerHTML = ', ' + state;
	    }
	    
	    this.orgPostal.innerHTML = this.validateField(org.postal);
	    
	    if (!this.validateField(org.postalAdd) == this.validateField(org.postal))
	    {
	    	this.orgPostalAdd.innerHTML = this.validateField(org.postalAdd);
	    }
	    
	    this.orgCountry.innerHTML = this.validateField(org.country);
	    	

		var balance = new Number(0);

		/*var header = document.createElement('tr');
		var th1 = document.createElement('td');
		var th2 = document.createElement('td');
		var th3 = document.createElement('td');
		var th4 = document.createElement('td');
		var th5 = document.createElement('td');
		var th6 = document.createElement('td');*/
		
		var header = document.createElement('div');
		header.setAttribute('class', 'row dark-grey-header');
	

		var th1 = document.createElement('div');
		th1.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th2 = document.createElement('div');
		th2.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th3 = document.createElement('div');
		th3.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th4 = document.createElement('div');
		th4.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th5 = document.createElement('div');
		th5.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th6 = document.createElement('div');
		th6.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		

		/*th1.style.borderBottomWidth='0px';
		th1.style.borderBottomColor='#CCCCCC';
		th2.style.borderBottomWidth='0px';
		th2.style.borderBottomColor='#CCCCCC';
		th3.style.borderBottomWidth='0px';
		th3.style.borderBottomColor='#CCCCCC';
		th4.style.borderBottomWidth='0px';
		th4.style.borderBottomColor='#CCCCCC';
		th5.style.borderBottomWidth='0px';
		th5.style.borderBottomColor='#CCCCCC';
		th6.style.borderBottomWidth='0px';
		th6.style.borderBottomColor='#CCCCCC';*/

		th1.innerHTML = Translation.translate('date');
		th2.innerHTML = Translation.translate('doc.no');
		th3.innerHTML = Translation.translate('doc.type');
		th4.innerHTML = Translation.translate('debit');
		th5.innerHTML = Translation.translate('credit');
		th6.innerHTML = Translation.translate('balance');
		
		header.appendChild(th1);
		header.appendChild(th2);
		header.appendChild(th3);
		header.appendChild(th4);
		header.appendChild(th5);
		header.appendChild(th6);

		//header.className = 'dark-grey-header';

		this.panel.appendChild(header);

		/*Open Balance*/
	/*	var openBalanceTr = document.createElement('tr');
		var openBalanceLabel = document.createElement('td');
		openBalanceLabel.setAttribute('colspan', '3');
		openBalanceTr.className = 'open-balance';
		openBalanceLabel.innerHTML = 'Open Balance';
		openBalanceTr.appendChild(openBalanceLabel);*/
		
	
		
		
		var openBalanceTr = document.createElement('div');
		openBalanceTr.setAttribute('class','row statement-of-account-odd-row');
		
		var openBalanceLabel = document.createElement('div');
		openBalanceLabel.setAttribute('class', 'col-xs-6 col-md-6 no-padding' );
		openBalanceLabel.innerHTML = Translation.translate('open.balance');
		openBalanceTr.appendChild(openBalanceLabel);
		
		
		var invoiceOpenBalance = new Number(openBalance.Invoice);
		var paymentOpenBalance = new Number(openBalance.Payment);
		
		var openDebitAmt = new Number(openBalance.debit);
		var openCreditAmt = new Number(openBalance.credit);
		
		var diff = invoiceOpenBalance - paymentOpenBalance;

		if(parseFloat(diff) != 0)
		{
			//var invoiceOpenBalanceTd = document.createElement('td');
			//var paymentOpenBalanceTd = document.createElement('td');
			
			var invoiceOpenBalanceTd = document.createElement('div');
			invoiceOpenBalanceTd.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
			
			var paymentOpenBalanceTd = document.createElement('div');
			paymentOpenBalanceTd.setAttribute('class','col-xs-2 col-md-2 no-padding');		
			
			invoiceOpenBalanceTd.innerHTML = openDebitAmt;
			paymentOpenBalanceTd.innerHTML = openCreditAmt;

			//var openBalanceTd = document.createElement('td');
			
			var openBalanceTd = document.createElement('div');
			openBalanceTd.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			
			balance = openDebitAmt - openCreditAmt;
			balance = Math.round(balance*100)/100;
			openBalanceTd.innerHTML = balance;
		
			openBalanceTr.appendChild(invoiceOpenBalanceTd);
			openBalanceTr.appendChild(paymentOpenBalanceTd);
			openBalanceTr.appendChild(openBalanceTd);

			this.panel.appendChild(openBalanceTr);
		}

		for(var i=0; i<statementOfAccountLines.length; i++)
		{	
			var statementOfAccountLine = statementOfAccountLines[i];
			/*var tr = document.createElement('tr');

			var td1 = document.createElement('td');
			var td2 = document.createElement('td');
			var td3 = document.createElement('td');
			var td4 = document.createElement('td');
			var td5 = document.createElement('td');
			var td6 = document.createElement('td');*/
			
			var tr = document.createElement('div');
			//tr.setAttribute('class', 'row');
			
			var td1 = document.createElement('div');
			td1.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td2 = document.createElement('div');
			td2.setAttribute('class','col-xs-2 col-md-2 no-padding');

			var td3 = document.createElement('div');
			td3.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td4 = document.createElement('div');
			td4.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td5 = document.createElement('div');
			td5.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td6 = document.createElement('div');
			td6.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			/*td1.style.borderBottomStyle='none';
			td1.style.borderTopStyle='none';
			td2.style.borderBottomStyle='none';
			td2.style.borderTopStyle='none';
			td3.style.borderBottomStyle='none';
			td3.style.borderTopStyle='none';
			td4.style.borderBottomStyle='none';
			td4.style.borderTopStyle='none';
			td5.style.borderBottomStyle='none';
			td5.style.borderTopStyle='none';
			td6.style.borderBottomStyle='none';
			td6.style.borderTopStyle='none';*/


			td1.innerHTML = statementOfAccountLine.datetrx;
			td2.innerHTML = statementOfAccountLine.documentno;
			td3.innerHTML = statementOfAccountLine.documenttype;

			var docAmt = new Number(statementOfAccountLine.amt);

			if (statementOfAccountLine.documenttype == 'AR Invoice')
			{
				td4.innerHTML = statementOfAccountLine.amt;
				td5.innerHTML = '&nbsp;';
			}
			
			if (statementOfAccountLine.documenttype == 'AP Invoice')
			{
				td4.innerHTML = '&nbsp;';
				td5.innerHTML = statementOfAccountLine.amt;
				docAmt = new Number(0) - docAmt;
				docAmt = Math.round(docAmt*100)/100
			}
			
			if (statementOfAccountLine.documenttype == 'AR Credit Memo')
			{
				td4.innerHTML = '&nbsp;';
				td5.innerHTML = statementOfAccountLine.amt;
				docAmt = new Number(0) - docAmt;
				docAmt = Math.round(docAmt*100)/100
			}
			
			if (statementOfAccountLine.documenttype == 'AR Receipt')
			{
				td4.innerHTML = '&nbsp;';
				td5.innerHTML = statementOfAccountLine.amt;
				docAmt = new Number(0) - docAmt;
				docAmt = Math.round(docAmt*100)/100
			}
			
			if (statementOfAccountLine.documenttype == 'AP Payment')
			{
				td4.innerHTML = statementOfAccountLine.amt;
				td5.innerHTML = '&nbsp;';
			}
			
			if (statementOfAccountLine.documenttype == 'AP CreditMemo')
			{
				td4.innerHTML = statementOfAccountLine.amt;
				td5.innerHTML = '&nbsp;';
			}
			
			balance = balance + docAmt;

			balance = Math.round(balance*100)/100

			
			td6.innerHTML = balance;

			tr.appendChild(td1);
			tr.appendChild(td2);
			tr.appendChild(td3);
			tr.appendChild(td4);
			tr.appendChild(td5);
			tr.appendChild(td6);

			if (i%2 == 0)
			{
				tr.className = 'row statement-of-account-even-row';
			}
			else
			{
				tr.className = 'row statement-of-account-odd-row';
			}
			
			this.panel.appendChild(tr);

		}

		//var totalBalanceTr = document.createElement('tr');
		//var totalBalanceTd = document.createElement('td');
		
		//totalBalanceTd.setAttribute('colspan', '5');
		
		var totalBalanceTr = document.createElement('div');
		//totalBalanceTr.setAttribute('class','row');
		//totalBalanceTr.setAttribute('style','text-align:center;background-color:#d9d9d9;font-size:14px;');
		
		var totalBalanceTd = document.createElement('div');
		totalBalanceTd.setAttribute('class','col-xs-10 col-md-10 no-padding ');

		totalBalanceTd.innerHTML = Translation.translate('balance');
		totalBalanceTr.appendChild(totalBalanceTd);

		//var totalBalanceTdValue = document.createElement('td');
		var totalBalanceTdValue = document.createElement('div');
		totalBalanceTdValue.setAttribute('class','col-xs-2 col-md-2 no-padding');
		
		totalBalanceTdValue.innerHTML = balance;	
		totalBalanceTr.appendChild(totalBalanceTdValue);

		totalBalanceTr.className = 'row balance statement-of-account-even-row';

		this.panel.appendChild(totalBalanceTr);
  		
  		 
  		if(result.error){
  			this.onError(result.errorMessage);
  			this.hide();
  			return;
  		}
		/*alert(result.bp.address);*/
	  },

	  onFailure:function(request){	
	  	this.onError("failed.to.process.request");
		/*this.hide();
		request.responseText;*/
	  },

	  onSendMailSuccess:function(request){
		this.statusWindow.dispose();
		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
		alert(result.status);
	  },

	  exportPDF:function(){
		var url = 'BPartnerAction.do?action=exportStatementOfAccount&customerId=' + this.bpartnerId + '&dateFrom=' + this.dateFrom + '&dateTo=' +this.dateTo;
		window.location = url;
	  },

	  clear:function(){
		  	this.panel.innerHTML = '';
	    	this.bpName.innerHTML = '';
	    	this.bpAddress.innerHTML = '';
	    	this.bpAddress2.innerHTML = '';
		    this.bpAddress3.innerHTML = '';
		    this.bpAddress4.innerHTML = '';
		    this.bpCity.innerHTML = '';
		    this.bpState.innerHTML = '';
		    this.bpPostal.innerHTML = '';
		    this.bpPostalAdd.innerHTML = '';
		    this.bpCountry.innerHTML = '';
		    
		    this.orgName.innerHTML = '';
		    this.orgAddress.innerHTML = '';
		    this.orgAddress2.innerHTML = '';
		    this.orgAddress3.innerHTML = '';
		    this.orgAddress4.innerHTML = '';
		    this.orgCity.innerHTML = '';
		    this.orgState.innerHTML = '';
		    this.orgPostal.innerHTML = '';
		    this.orgPostalAdd.innerHTML = '';
		    this.orgCountry.innerHTML = '';
		    this.orgCity.innerHTML = '';
	},
	
	validateField : function(field){
		if (field == null || field == '' || field == 'null')
		{
			return '';
		}
		
		return field;
	}
});

/*Upgrade POS Panel*/
var UpgradePOSPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('upgrade-pos-panel'));
	    this.cancelBtn = $('upgrade-pos-panel-cancelBtn');
	    this.subscribeProfessionalBtn = $('subscribe-professional-button');
	    this.subscribeEnterpriseBtn = $('subscribe-enterprise-button');

	    this.cancelBtn.onclick = this.cancelHandler.bind(this);

	    /*this.cancelBtn.onclick = function(e){
		this.hide();
		};

	    this.okBtn.onclick = function(e){
		this.hide();
		};*/
	},

    cancelHandler : function()
   	{
		this.hide();
    },
	
	onShow : function()
	{
		var url = 'SignUpAction.do?action=getUpgradeAccountLink';
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});
	},

	onSuccess: function(request)
	{
		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
		this.subscribeProfessionalBtn.href = result.subscribeProfessionalLink;
		this.subscribeEnterpriseBtn.href = result.subcribeEnterpriseLink;
	},
	
	onFailure : function(request)
	{
	},
	
	show: function()
	{
		PopUpBase.show.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'block';
			}
		}
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'none';
			}
		}
	}

});

/*Close Account Panel*/
var AccountClosedPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('account-closed'));
	    this.cancelBtn = $('account-closed-panel-cancelBtn');
	    this.okBtn = $('account-closed-panel-okBtn');
	     
            this.cancelBtn.onclick = this.cancelHandler.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this);
	},
	
	cancelHandler : function()
   	{
		this.hide();
    	},

	okHandler : function()
   	{
		if (window.top != window.self)
		{
			window.top.location='ModelAction.do?action=load&controller=Subscription&processKey=RV_POS_Subscription';
		}
		else
		{
			window.location='ModelAction.do?action=load&controller=Subscription&processKey=RV_POS_Subscription';
		}
   	},
   	
   	show: function()
	{
		PopUpBase.show.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'block';
			}
		}
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'none';
			}
		}
	}

});

/*Expired Account Panel*/
var AccountExpiredPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('account-expired'));
	    this.cancelBtn = $('account-expired-panel-cancelBtn');
	    this.okBtn = $('account-expired-panel-okBtn');
	     
        this.cancelBtn.onclick = this.cancelHandler.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this);
	},
	
	cancelHandler : function()
   	{
		this.hide();
    },

	okHandler : function()
   	{
		if (window.top != window.self)
		{
			window.top.location='activation.do';
		}
		else
		{
			window.location='activation.do';
		}
   	},
   	
   	show: function()
	{
		PopUpBase.show.call(this);
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
	}

});

var ChangePasswordPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('change-password'));
	    this.cancelBtn = $('change-password-panel-cancelBtn');
	    this.okBtn = $('change-password-panel-okBtn');
	    this.oldPassword = $('old-password');
	    this.newPassword = $('new-password');
	    this.confirmNewPassword = $('confirm-new-password');
	     
        this.cancelBtn.onclick = this.cancelHandler.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this);
	},
	
	cancelHandler : function()
 	{
		this.hide();
  	},

	okHandler : function()
 	{
 	}

});

var GenerateQRPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('generate-qr-panel'));
	    this.cancelBtn = $('generate-qr-panel-cancelBtn');
	    this.okBtn = $('generate-qr-panel-okBtn');
	    this.qr = $('qr-data');
	    this.qr.innerHTML = '';
	     
	    this.cancelBtn.onclick = this.cancelHandler.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this);
	},
	
	init : function (productId)
	{
		this.productId = productId;
		this.qr.innerHTML = '';
		this.show();
	},
	
	cancelHandler : function()
	{
		this.hide();
	},

	okHandler : function()
	{
		this.hide();
	},
	
	onShow:function(){
		var url = 'ProductAction.do';
		var pars = 'action=generateQR&productId=' + this.productId;
		new Ajax.Request(url, {
			method:'post',
			parameters: pars,
			onSuccess: this.onSuccess.bind(this)
		});
	},
	
	onSuccess:function(response){
			this.qr.innerHTML = response.responseText;
	}

});

/*Date Picker Panel*/
/*var DatePickerPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('date-picker-popup-panel'));
	    
     	    this.fromDateTextField = $('date-from-textfield');
	    this.fromDateButton = $('date-from-button');

	    this.toDateTextField = $('date-to-textfield');
	    this.toDateButton = $('date-to-button');
	
	    this.okButton = $('date-picker-popup-ok-button');
	    this.cancelButton = $('date-picker-popup-cancel-button');

	    this.cancelButton.onclick = this.cancelHandler.bind(this); 
	    this.okButton.onclick = this.okHandler.bind(this);
	
	    this.recordId = null;

		Calendar.setup({ 
   	   			inputField : 'date-from-textfield', 
   	   			ifFormat : '%Y-%m-%d',
   	   			showTime : true,
   	   			button : 'date-from-button', 
   	   			onUpdate : this.setDate });

		Calendar.setup({ 
   			inputField : 'date-to-textfield', 
   			ifFormat : '%Y-%m-%d',
   			showTime : true,
   			button : 'date-to-button', 
   			onUpdate :  this.setDate});
	},
	
	init : function(recordId)
	{
		this.recordId = recordId;
	},

	setDate :  function(calendar){
	},

	cancelHandler : function(){
		this.hide();
	},
	
	okHandler : function(){
		this.hide();
		var statementOfAccount = new StatementOfAccountPanel();
		statementOfAccount.init(10004220, this.fromDateTextField.value, this.toDateTextField.value);
		statementOfAccount.show();
	}

});*/

var WatchVideosPanel = Class.create(PopUpBase, {
	initialize : function()
	{
	 	this.createPopUp($('watch-videos-panel'));
	 	this.cancelBtn = $('watch-videos-panel-cancelBtn');
	 	this.videoFrame = $('video-frame');
	 	
	 	this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	show: function()
	{
		PopUpBase.show.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'block';
			}
		}
		
		this.videoFrame.style.display = 'block';
		this.videoFrame.src = 'www.youtube.com/embed/QpAfFaunSaU';
		
		
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'none';
			}
		}
		
		this.videoFrame.style.display = 'none';
	}
});

/*Create Instance Product Popup Panel*/
var CreateInstanceProductsPanel =  Class.create(PopUpBase, {
	  initialize : function()
	  {
		this.createPopUp($('create-instance-products-popup'));
		
		this.numberOfSerialNoTextField = $('create-instance-products-popup-NumberOfSerialNos-textField');
	    this.saveBtn = $('create-instance-products-popup-save-button');
	    this.closeBtn = $('create-instance-products-popup-close-button');
	    this.serialNosListContainer = $('serialNosListContainer');
	    this.label = $('label');
	    this.parentListSelect = $('parentList');
	    this.movementId = $('movementId').value;
	    this.saveBtn.onclick = this.save.bind(this);
	    this.closeBtn.onclick = this.hide.bind(this);
	    this.numberOfSerialNoTextField.onkeyup = this.renderList.bind(this);
	    this.parentListSelect.onchange = this.onParentChange.bind(this);
	    //$('parents').value = '[{"productId":10162893,"qty":4,"name":"dvd"}, {"productId":10162894,"qty":5,"name":"refrigerator"}]';
	    this.parents = eval('('+ $('parents').value.unescapeHTML() +')');
	    this.selectedParentId = null;
	    this.params = null;
	    this.hash = new Hash();
	    this.currentParent = null;
	    this.generateSerialNosButton = $('generate-serial-nos');
	    this.dialog = null;
	    
	    this.init();
	  },
	  
	  init : function()
	  {
		  this.resetPanel();
		  this.parentListSelect.innerHTML = '';
		  
		  for(var i=0; i<this.parents.length; i++)
		  {
			 var option = document.createElement('option');
			 option.setAttribute('value', this.parents[i].productId);
			 option.innerHTML = this.parents[i].name;
			 
			 this.parentListSelect.appendChild(option);
			 this.parentListSelect.options[0].selected = true;
			 
			 var productId = this.parentListSelect.options[this.parentListSelect.options.selectedIndex].value;
			 
			  if (this.parents[i].productId == productId)
			  {
				  this.currentParent = this.parents[i];
				  this.hash.set(productId, new ArrayList());
			  }
		  }
	  },
	  
	  save : function()
	  { 
		  this.onParentChange();
		  var isValid = false;
		  
		  for(var i=0; i<this.hash.keys().length; i++)
		  {
			  var arrayList = this.hash.get(this.hash.keys()[i]);
			  var flag = true;
			  
			  if (arrayList.size() == 0)
			  {
				  flag = false;
			  }
			  
			  isValid = isValid || flag;
		  }
		  
		  if (!isValid)
		  {
			  alert(Translation.translate('please.enter.the.number.of.serial.no.to.generate','Please enter the number of serial no to generate.'));
			  return;
		  }
		  
		  this.params = this.hash.toJSON();
		  
		  //alert(this.params);
		  
		  
		  var url = 'StockTransferAction.do';
		  var params = 'action=createInstanceProducts&serialNos=' + this.params + '&movementId=' + this.movementId;
		  	new Ajax.Request( url,{ 
		  		ethod: 'post', 
				parameters: params,
				onSuccess: this.onSuccess.bind(this), 
				onFailure: this.onFailure.bind(this)
			});	
		  	
		  	this.hide();
		  	this.dialog = new Dialog();
		  	this.dialog.show();
	  },
	  
	  onSuccess : function(request)
	  {
		  this.dialog.hide();
		  
		  var response = eval('('+request.responseText+')');
		  
		  if (response.success)
		  {
			  $('parents').value = response.parents;
			  
			  if (!response.isGenerateSerialNo)
			  {
				  this.generateSerialNosButton.style.display = 'none';
			  } 
			  else
			  {
				  this.generateSerialNosButton.style.display = 'block';
			  }
			  
			  alert(Translation.translate('serial.no.generated','Serial No generated'));
		  }
		  else
		  {
			  alert(response.error);
		  }
	  },
	  
	  onFailure : function()
	  {
		  
	  },
	  
	  renderList : function()
	  {
		  var noOfSerialNos = this.numberOfSerialNoTextField.value;
		  var parentQty = this.currentParent.qty;
		  
		  if (noOfSerialNos > parentQty)
		  {
			  alert(Translation.translate('invalid.amount.entered','Invalid Amount Entered'));
			  return;
		  }
		  
		  this.label.innerHTML = 'Enter Serial No:';
		  this.serialNosListContainer.innerHTML = '';
		  
		  for(var i=1; i<=noOfSerialNos; i++)
		  {
			  var div = document.createElement('div');
			  div.setAttribute('style', 'padding-bottom:10px');
			  var input = document.createElement('input');
			  input.setAttribute('id', i);
			  input.setAttribute('name', 'serialNoTextField');
			  
			  div.appendChild(input);
			  
			  this.serialNosListContainer.appendChild(div);
		  }
	  },
	  
	  onParentChange : function()
	  {
		  //save current parent state and then load new parent
		  //Each time a parent is loaded again, all serial no are reset again
		  var serialNoTextFields = document.getElementsByName('serialNoTextField');
		  
		  if (serialNoTextFields.length == 0 && this.numberOfSerialNoTextField.value == '')
		  {
			  var productId = this.parentListSelect.options[this.parentListSelect.options.selectedIndex].value;
			  
			  for(var i=0; i<this.parents.length; i++)
			  {
				  if (productId == this.parents[i].productId)
				  {
					  this.currentParent = this.parents[i];
					  break;
				  }
			  }
			  return;
		  }
		  
		  for(var i=0; i<serialNoTextFields.length; i++)
		  {
			  if (serialNoTextFields[i].value == '')
			  {
				  alert(Translation.translate('please.fill.in.all.serial.no','Please fill in all serial no'));
				  return;
			  }
		  }
		  
		  for(var i=0; i<serialNoTextFields.length; i++)
		  {
			  for(var j=0; j<serialNoTextFields.length; j++)
			  {
				 if (i != j)
				 {
					 if (serialNoTextFields[i].value == serialNoTextFields[j].value)
					 {
						 alert(Translation.translate('duplicate.serial.no','Duplicate Serial No!'));
						 return;
					 }
				 }
			  }
		  }
		  
		  var productId = this.currentParent.productId;
		  var arrayList = this.hash.get(productId);
		  
		  if (arrayList == null)
		  {
			  arrayList = new ArrayList();
		  }
		  else
		  {
			  arrayList.clear();
		  }
		  
		  for(var i=0; i<serialNoTextFields.length; i++)
		  {
			  var hash = new Hash()
			  hash.set('serialNo', serialNoTextFields[i].value);
			  hash.set('locatorId', this.currentParent.locatorId);
			  arrayList.add(hash);
		  }
		  
		  this.hash.set(productId, arrayList);
		  
		  //load new Parent
		  this.resetPanel();
		  var productId = this.parentListSelect.options[this.parentListSelect.options.selectedIndex].value;

		  for(var i=0; i<this.parents.length; i++)
		  {
			  if (this.parents[i].productId == productId)
			  {
				  this.currentParent = this.parents[i];
				  break;
			  }
		  }
	  },
	  
	  resetPanel : function()
	  {
		  this.numberOfSerialNoTextField.value = '';
		  this.serialNosListContainer.innerHTML = '';
		  this.label.innerHTML = '';
	  },
	  
	  show : function()
	  {
		  
		  PopUpBase.show.call(this);
	  }
});

var CommentsPanel = Class.create(PopUpBase, {
	initialize : function()
	{
	 	this.createPopUp($('comments.popup.panel'));
	 	this.cancelBtn = $('comments.popup.cancelButton');
	 	this.okBtn = $('comments.popup.okButton');
	 	this.comments = $('order-comment');
	 	this.orderComments = $('comment');
	 	
	 	this.commentBadge = $('comment-badge');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);
	 	this.okBtn.onclick = this.okHandler.bind(this);
	},
	
	okHandler : function()
	{
		this.orderComments.value = this.comments.value;
		this.changeCommentBadge();
		this.hide();
	},
	
	changeCommentBadge: function()
	{
		if (this.comments.value == '' || this.comments.value == null)
		{
		 this.commentBadge.innerHTML = '';
		}
		else
		{
		 this.commentBadge.innerHTML = '1';
		};
	},
	
	cancelHandler: function()
	{
		this.changeCommentBadge();
		this.hide();
	}
});

var ItemQuantityPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('itemQuantityPanel'));
	    
	    this.applyBtn = $('item-quantity-panel-apply-button');
	    this.cancelBtn = $('item-quantity-panel-cancel-button');
	    
	    this.quantityTextField = $('quantity-texfield');
	    
	    this.addButton = $('add-button');
	    this.decreaseButton = $('decrease-button');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.applyBtn.onclick = this.applyHandler.bind(this); 
	    this.addButton.onclick = this.increment.bind(this);
	    this.decreaseButton.onclick = this.decrement.bind(this);
	  },
	    
	  applyHandler:function(){
		  
		  this.validate();
		  
		  var quantity = parseFloat(this.quantityTextField.value);
		  ShoppingCartManager.updateQty(quantity);
		  this.hide();
	  },
	  
	  onShow:function(){
	  },
	  
	  validate:function(){	
		  var quantity = parseFloat(this.quantityTextField.value);
		  if(isNaN(quantity) || (quantity < 0.0)){
			  alert(Translation.translate('invalid.amt','Invalid amount'));
			  return false;
		  }
	  },
	  
	  increment : function(){
		  ShoppingCartManager.incrementQty();
		  
		 /* var cart = ShoppingCartManager.getCart();
		  var currentLine  = cart.lines[cart.selectedIndex];
		  
		  this.quantityTextField.value = currentLine.qty;*/
	  },
	  
	  decrement : function(){
		  ShoppingCartManager.decrementQty();
		  
		  /*var cart = ShoppingCartManager.getCart();
		  var currentLine  = cart.lines[cart.selectedIndex];
		  
		  this.quantityTextField.value = currentLine.qty;*/
	  }
		  
	});

var LineItemPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('itemLinePanel'));
	    
	    this.applyBtn = $('line-item-panel-apply-button');
	    this.cancelBtn = $('line-item-panel-cancel-button');
	    
	    this.quantityTextField = $('quantity-texfield');
	    
	    this.decreaseButton = $('decrease-button');
	    this.increaseButton = $('add-button');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.applyBtn.onclick = this.applyHandler.bind(this); 
	    
	    this.decreaseButton.onclick = this.decreaseQty.bind(this);
	    this.increaseButton.onclick = this.increaseQty.bind(this);
	    
	    var keypadButtons = $$('#itemLinePanel .keypad-button');
      for(var i=0; i<keypadButtons.length; i++){
          var btn = keypadButtons[i];
          btn.onclick = this.keypadHandler.bind(this);
      }
      
      this.quantityTextField.onkeyup = function(e)
	    {
			if (isNaN(this.value))
			{
				this.value = '';
				return;
			}
      }
	  },
	    
	    
	    
	  applyHandler:function(){
		  
		  this.validate();
		  
		  var quantity = parseFloat(this.quantityTextField.value);
		  ShoppingCartManager.updateQty(quantity);
		  this.hide();
	  },
	  
	  onShow:function(){
		  this.quantityTextField.focus();
		  this.quantityTextField.select();
	  },
	  
	  validate:function(){	
		  var quantity = parseFloat(this.quantityTextField.value);
		  if(isNaN(quantity) || (quantity < 0.0)){
			  alert(Translation.translate('invalid.amt','Invalid amount'));
			  return false;
		  }
	  },
	  
	  keypadHandler:function(e){
        var button = Event.element(e); 
        var element = button.getElementsByClassName('keypad-value')[0];
        
        /*Fix for google chrome*/
        if (element == null)
        {
        		element = button;  
        }
        /**********************/
	          
        var value = element.innerHTML;
       // var value = element.innerHTML;
                     
        var quantity = this.quantityTextField.value;
        
        	if (this.quantityTextField.selectionStart != this.quantityTextField.selectionEnd)
			{	
        		quantity = '';
				this.quantityTextField.value = '';
				this.quantityTextField.focus();
			}
        
        
			if('Del' == element.innerHTML.strip()){
				
				if(quantity.length > 0)
	        		  quantity = quantity.substring(0,quantity.length-1);
        }
			else if('-' == element.innerHTML.strip()){
				
				if(quantity.indexOf('-') == -1){
					quantity = '-' + quantity;
				}
				else{
					quantity = quantity.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(quantity.indexOf('.') == -1)
				{
					quantity = quantity + value;
				}
			}
			else{
				quantity = quantity + value;
			}
			
			this.quantityTextField.value = quantity;
			
         
          if(isNaN(quantity) || (quantity < 0.0)){           
              return;
          }
         
          if(quantity != 0.0 && !isNaN(quantity)){
          	quantity = parseFloat(quantity);
          }
    	},
    	
    	increaseQty : function()
    	{
    		var quantity = this.quantityTextField.value;
				
			if (!isNaN(quantity))
			{
				quantity = parseFloat(quantity) + 1;
				this.quantityTextField.value = quantity;
			}
			
			if(isNaN(quantity) || (quantity < 0.0)){           
              return;
          }
         
          if(quantity != 0.0 && !isNaN(quantity)){
          	quantity = parseFloat(quantity);
          }
    	},
    	
    	decreaseQty : function()
    	{
    		var quantity = this.quantityTextField.value;
				
			if (!isNaN(quantity) && quantity > 0)
			{
				quantity = parseFloat(quantity) - 1;
				this.quantityTextField.value = quantity;
			}
			
			if(isNaN(quantity) || (quantity < 0)){ 
				this.quantityTextField.value = 0;
              return;
          }
         
          if(quantity != 0.0 && !isNaN(quantity)){
          	quantity = parseFloat(quantity);
          }
    	}
	});


/*Date Picker Panel*/
var DatePickerPanel =  Class.create(PopUpBase, {
	initialize: function(url) 
	{
	    this.createPopUp($('date-picker-popup-panel'));
	    
     	this.fromDateTextField = $('date-from-textfield');
	    this.fromDateButton = $('date-from-button');

	    this.toDateTextField = $('date-to-textfield');
	    this.toDateButton = $('date-to-button');
	
	    this.okBtn = $('date-picker-popup-ok-button');
	    this.cancelBtn = $('date-picker-popup-cancel-button');

	    this.cancelBtn.onclick = this.cancelHandler.bind(this); 
	    this.okBtn.onclick = this.okHandler.bind(this);
	    

		Calendar.setup({ 
   	   			inputField : 'date-from-textfield', 
   	   			ifFormat : '%Y-%m-%d',
   	   			showTime : true,
   	   			button : 'date-from-button', 
   	   			onUpdate : this.setDate.bind(this) });

		Calendar.setup({ 
   			inputField : 'date-to-textfield', 
   			ifFormat : '%Y-%m-%d',
   			showTime : true,
   			button : 'date-to-button', 
   			onUpdate :  this.setDate.bind(this)});
	},
	
	setDate :  function(){

	},

	cancelHandler : function(){
		this.hide();
	},
	
	okHandler : function(){
		this.hide();
		var statementOfAccount = new StatementOfAccountPanel();
		statementOfAccount.init(this.recordId, this.fromDateTextField.value, this.toDateTextField.value);
		statementOfAccount.show();
	},
	
	setRecordId : function(recordId)
	{
		this.recordId = recordId;
	}

});

/*ExportStatmentOfAccount Panel*/
var ExportStatementOfAccountPopUp =  Class.create(PopUpBase, {
	initialize: function(url) 
	{
	    this.createPopUp($('stmt-date-picker-popup-panel'));
	    
     	this.fromDateTextField = $('stmt-date-from-textfield');
	    this.fromDateButton = $('stmt-date-from-button');

	    this.toDateTextField = $('stmt-date-to-textfield');
	    this.toDateButton = $('stmt-date-to-button');
	
	    this.okBtn = $('stmt-date-picker-popup-ok-button');
	    this.cancelBtn = $('stmt-date-picker-popup-cancel-button');

	    this.cancelBtn.onclick = this.cancelHandler.bind(this); 
	    this.okBtn.onclick = this.okHandler.bind(this);
	    

		Calendar.setup({ 
   	   			inputField : 'stmt-date-from-textfield', 
   	   			ifFormat : '%Y-%m-%d',
   	   			showTime : true,
   	   			button : 'stmt-date-from-button', 
   	   			onUpdate : this.setDate.bind(this) });

		Calendar.setup({ 
   			inputField : 'stmt-date-to-textfield', 
   			ifFormat : '%Y-%m-%d',
   			showTime : true,
   			button : 'stmt-date-to-button', 
   			onUpdate :  this.setDate.bind(this)});
	},
	
	setDate :  function(){

	},

	cancelHandler : function(){
		this.hide();
	},
	
	okHandler : function(){
		this.hide();
		var url = 'BPartnerAction.do?action=exportStatementOfAccount&customerId=' + this.recordId + '&dateFrom=' + this.fromDateTextField.value + '&dateTo=' +this.toDateTextField.value;
		window.location = url;
	}

});

/*Statement of Account*/
var StatementOfAccount =  Class.create({
	  initialize: function() {
	    /*buttons*/

	    this.panel = $('statement.of.account.content');
	    this.panel.innerHTML = '';

	    this.bpName = $('bp-name');
	    this.bpAddress = $('bp-address');
	    this.bpAddress2 = $('bp-address2');
	    this.bpAddress3 = $('bp-address3');
	    this.bpAddress4 = $('bp-address4');
	    this.bpCity = $('bp-city');
	    this.bpState = $('bp-state');
	    this.bpPostal = $('bp-postal');
	    this.bpPostalAdd = $('bp-postal-add');
	    this.bpCountry = $('bp-country');
	    
	    this.orgName = $('org-name');
	    this.orgAddress = $('org-address');
	    this.orgAddress2 = $('org-address2');
	    this.orgAddress3 = $('org-address3');
	    this.orgAddress4 = $('org-address4');
	    this.orgCity = $('org-city');
	    this.orgState = $('org-state');
	    this.orgPostal = $('org-postal');
	    this.orgPostalAdd = $('org-postal-add');
	    this.orgCountry = $('org-country');
	    this.orgCity = $('org-city');
	    
        this.bpartnerId = null;
	    this.dateFrom = null;
	    this.dateTo = null;
	    this.statusWindow = null;
	  }, 

	  init : function(bpartnerId, dateFrom, dateTo){
	  	this.bpartnerId = bpartnerId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        
        var url = 'BPartnerAction.do?action=getStatementOfAccount&customerId=' + this.bpartnerId + '&dateFrom=' + this.dateFrom + '&dateTo=' +this.dateTo;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});
 	  },

	  okHandler:function(){	
		this.hide();  
	  },

	  cancelHandler:function(){	
		this.hide();  
	  },

	  sendMail:function(){
		var url = 'BPartnerAction.do?action=sendStatementOfAccount&customerId=' + this.bpartnerId + '&dateFrom=' + this.dateFrom + '&dateTo=' +this.dateTo;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSendMailSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});
		
		this.statusWindow = new ModalWindow('', false);
		this.statusWindow.div.style.zIndex = '99999999';
		this.statusWindow.show();
	  },

	  resetHandler:function(){
	  },

	  /* ajax callbacks */
	  onComplete:function(request){		  
	  },

	  onSuccess:function(request){	  		
  		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');

		var statementOfAccountLines = result.statementOfAccountLines;
		var bp = result.bp;
		var org = result.org;
		var openBalance = result.openingBalance;

		this.bpName.innerHTML = bp.name + bp.name2;
    	this.bpAddress.innerHTML = this.validateField(bp.address);
    	this.bpAddress2.innerHTML = this.validateField(bp.address2);
	    this.bpAddress3.innerHTML = this.validateField(bp.address3);
	    this.bpAddress4.innerHTML = this.validateField(bp.address4);
	    this.bpCity.innerHTML = this.validateField(bp.city);
	    
	    var state = this.validateField(bp.state);
	    if (state != null || state != 'null' || state != '')
	    {
	    	this.bpState.innerHTML = ', ' + state;
	    }
	    
	    this.bpPostal.innerHTML = this.validateField(bp.postal);
	    
	    if (!this.validateField(bp.postalAdd) == this.validateField(bp.postal))
	    {
	    	this.bpPostalAdd.innerHTML = this.validateField(bp.postalAdd);
	    }
	    
	    this.bpCountry.innerHTML = this.validateField(bp.country);
	    
	    this.orgName.innerHTML = this.validateField(org.name);
	    this.orgAddress.innerHTML = this.validateField(org.address);
	    this.orgAddress2.innerHTML = this.validateField(org.address2);
	    this.orgAddress3.innerHTML = this.validateField(org.address3);
	    this.orgAddress4.innerHTML = this.validateField(org.address4);
	    this.orgCity.innerHTML = this.validateField(org.city);
	    
	    state = this.validateField(org.state);
	    if (state != null || state != 'null' || state != '')
	    {
	    	this.orgState.innerHTML = ', ' + state;
	    }
	    
	    this.orgPostal.innerHTML = this.validateField(org.postal);
	    
	    if (!this.validateField(org.postalAdd) == this.validateField(org.postal))
	    {
	    	this.orgPostalAdd.innerHTML = this.validateField(org.postalAdd);
	    }
	    
	    this.orgCountry.innerHTML = this.validateField(org.country);
	    	

		var balance = new Number(0);

		/*var header = document.createElement('tr');
		var th1 = document.createElement('td');
		var th2 = document.createElement('td');
		var th3 = document.createElement('td');
		var th4 = document.createElement('td');
		var th5 = document.createElement('td');
		var th6 = document.createElement('td');*/
		
		var header = document.createElement('div');
		header.setAttribute('class', 'row dark-grey-header');
	

		var th1 = document.createElement('div');
		th1.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th2 = document.createElement('div');
		th2.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th3 = document.createElement('div');
		th3.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th4 = document.createElement('div');
		th4.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th5 = document.createElement('div');
		th5.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		var th6 = document.createElement('div');
		th6.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
		
		

		/*th1.style.borderBottomWidth='0px';
		th1.style.borderBottomColor='#CCCCCC';
		th2.style.borderBottomWidth='0px';
		th2.style.borderBottomColor='#CCCCCC';
		th3.style.borderBottomWidth='0px';
		th3.style.borderBottomColor='#CCCCCC';
		th4.style.borderBottomWidth='0px';
		th4.style.borderBottomColor='#CCCCCC';
		th5.style.borderBottomWidth='0px';
		th5.style.borderBottomColor='#CCCCCC';
		th6.style.borderBottomWidth='0px';
		th6.style.borderBottomColor='#CCCCCC';*/

		th1.innerHTML = Translation.translate('date');
		th2.innerHTML = Translation.translate('doc.no');
		th3.innerHTML = Translation.translate('doc.type');
		th4.innerHTML = Translation.translate('debit');
		th5.innerHTML = Translation.translate('credit');
		th6.innerHTML = Translation.translate('balance');
		
		header.appendChild(th1);
		header.appendChild(th2);
		header.appendChild(th3);
		header.appendChild(th4);
		header.appendChild(th5);
		header.appendChild(th6);

		//header.className = 'dark-grey-header';

		this.panel.appendChild(header);

		/*Open Balance*/
	/*	var openBalanceTr = document.createElement('tr');
		var openBalanceLabel = document.createElement('td');
		openBalanceLabel.setAttribute('colspan', '3');
		openBalanceTr.className = 'open-balance';
		openBalanceLabel.innerHTML = 'Open Balance';
		openBalanceTr.appendChild(openBalanceLabel);*/
		
	
		
		
		var openBalanceTr = document.createElement('div');
		openBalanceTr.setAttribute('class','row statement-of-account-odd-row');
		
		var openBalanceLabel = document.createElement('div');
		openBalanceLabel.setAttribute('class', 'col-xs-6 col-md-6 no-padding' );
		openBalanceLabel.innerHTML = Translation.translate('open.balance');
		openBalanceTr.appendChild(openBalanceLabel);
		
		
		var invoiceOpenBalance = new Number(openBalance.Invoice);
		var paymentOpenBalance = new Number(openBalance.Payment);
		
		var openDebitAmt = new Number(openBalance.debit);
		var openCreditAmt = new Number(openBalance.credit);
		
		var diff = invoiceOpenBalance - paymentOpenBalance;

		if(parseFloat(diff) != 0)
		{
			//var invoiceOpenBalanceTd = document.createElement('td');
			//var paymentOpenBalanceTd = document.createElement('td');
			
			var invoiceOpenBalanceTd = document.createElement('div');
			invoiceOpenBalanceTd.setAttribute('class', 'col-xs-2 col-md-2 no-padding');
			
			var paymentOpenBalanceTd = document.createElement('div');
			paymentOpenBalanceTd.setAttribute('class','col-xs-2 col-md-2 no-padding');		
			
			invoiceOpenBalanceTd.innerHTML = openDebitAmt;
			paymentOpenBalanceTd.innerHTML = openCreditAmt;

			//var openBalanceTd = document.createElement('td');
			
			var openBalanceTd = document.createElement('div');
			openBalanceTd.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			
			balance = openDebitAmt - openCreditAmt;
			balance = Math.round(balance*100)/100;
			openBalanceTd.innerHTML = balance;
		
			openBalanceTr.appendChild(invoiceOpenBalanceTd);
			openBalanceTr.appendChild(paymentOpenBalanceTd);
			openBalanceTr.appendChild(openBalanceTd);

			this.panel.appendChild(openBalanceTr);
		}

		for(var i=0; i<statementOfAccountLines.length; i++)
		{	
			var statementOfAccountLine = statementOfAccountLines[i];
			/*var tr = document.createElement('tr');

			var td1 = document.createElement('td');
			var td2 = document.createElement('td');
			var td3 = document.createElement('td');
			var td4 = document.createElement('td');
			var td5 = document.createElement('td');
			var td6 = document.createElement('td');*/
			
			var tr = document.createElement('div');
			//tr.setAttribute('class', 'row');
			
			var td1 = document.createElement('div');
			td1.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td2 = document.createElement('div');
			td2.setAttribute('class','col-xs-2 col-md-2 no-padding');

			var td3 = document.createElement('div');
			td3.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td4 = document.createElement('div');
			td4.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td5 = document.createElement('div');
			td5.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			var td6 = document.createElement('div');
			td6.setAttribute('class','col-xs-2 col-md-2 no-padding');
			
			/*td1.style.borderBottomStyle='none';
			td1.style.borderTopStyle='none';
			td2.style.borderBottomStyle='none';
			td2.style.borderTopStyle='none';
			td3.style.borderBottomStyle='none';
			td3.style.borderTopStyle='none';
			td4.style.borderBottomStyle='none';
			td4.style.borderTopStyle='none';
			td5.style.borderBottomStyle='none';
			td5.style.borderTopStyle='none';
			td6.style.borderBottomStyle='none';
			td6.style.borderTopStyle='none';*/


			td1.innerHTML = statementOfAccountLine.datetrx;
			td2.innerHTML = statementOfAccountLine.documentno;
			td3.innerHTML = statementOfAccountLine.documenttype;

			var docAmt = new Number(statementOfAccountLine.amt);

			if (statementOfAccountLine.documenttype == 'AR Invoice')
			{
				td4.innerHTML = statementOfAccountLine.amt;
				td5.innerHTML = '&nbsp;';
			}
			
			if (statementOfAccountLine.documenttype == 'AP Invoice')
			{
				td4.innerHTML = '&nbsp;';
				td5.innerHTML = statementOfAccountLine.amt;
				docAmt = new Number(0) - docAmt;
				docAmt = Math.round(docAmt*100)/100
			}
			
			if (statementOfAccountLine.documenttype == 'AR Credit Memo')
			{
				td4.innerHTML = '&nbsp;';
				td5.innerHTML = statementOfAccountLine.amt;
				docAmt = new Number(0) - docAmt;
				docAmt = Math.round(docAmt*100)/100
			}
			
			if (statementOfAccountLine.documenttype == 'AR Receipt')
			{
				td4.innerHTML = '&nbsp;';
				td5.innerHTML = statementOfAccountLine.amt;
				docAmt = new Number(0) - docAmt;
				docAmt = Math.round(docAmt*100)/100
			}
			
			if (statementOfAccountLine.documenttype == 'AP Payment')
			{
				td4.innerHTML = statementOfAccountLine.amt;
				td5.innerHTML = '&nbsp;';
			}
			
			if (statementOfAccountLine.documenttype == 'AP CreditMemo')
			{
				td4.innerHTML = statementOfAccountLine.amt;
				td5.innerHTML = '&nbsp;';
			}
			
			balance = balance + docAmt;

			balance = Math.round(balance*100)/100

			
			td6.innerHTML = balance;

			tr.appendChild(td1);
			tr.appendChild(td2);
			tr.appendChild(td3);
			tr.appendChild(td4);
			tr.appendChild(td5);
			tr.appendChild(td6);

			if (i%2 == 0)
			{
				tr.className = 'row statement-of-account-even-row';
			}
			else
			{
				tr.className = 'row statement-of-account-odd-row';
			}
			
			this.panel.appendChild(tr);

		}

		//var totalBalanceTr = document.createElement('tr');
		//var totalBalanceTd = document.createElement('td');
		
		//totalBalanceTd.setAttribute('colspan', '5');
		
		var totalBalanceTr = document.createElement('div');
		//totalBalanceTr.setAttribute('class','row');
		//totalBalanceTr.setAttribute('style','text-align:center;background-color:#d9d9d9;font-size:14px;');
		
		var totalBalanceTd = document.createElement('div');
		totalBalanceTd.setAttribute('class','col-xs-10 col-md-10 no-padding ');

		totalBalanceTd.innerHTML = Translation.translate('balance');
		totalBalanceTr.appendChild(totalBalanceTd);

		//var totalBalanceTdValue = document.createElement('td');
		var totalBalanceTdValue = document.createElement('div');
		totalBalanceTdValue.setAttribute('class','col-xs-2 col-md-2 no-padding');
		
		totalBalanceTdValue.innerHTML = balance;	
		totalBalanceTr.appendChild(totalBalanceTdValue);

		totalBalanceTr.className = 'row balance statement-of-account-even-row';

		this.panel.appendChild(totalBalanceTr);
  		
  		 
  		if(result.error){
  			this.onError(result.errorMessage);
  			this.hide();
  			return;
  		}
		/*alert(result.bp.address);*/
	  },

	  onFailure:function(request){	
	  	this.onError("failed.to.process.request");
		/*this.hide();
		request.responseText;*/
	  },

	  onSendMailSuccess:function(request){
		this.statusWindow.dispose();
		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
		alert(result.status);
	  },

	  exportPDF:function(){
		var url = 'BPartnerAction.do?action=exportStatementOfAccount&customerId=' + this.bpartnerId + '&dateFrom=' + this.dateFrom + '&dateTo=' +this.dateTo;
		window.location = url;
	  },

	  clear:function(){
		  	this.panel.innerHTML = '';
	    	this.bpName.innerHTML = '';
	    	this.bpAddress.innerHTML = '';
	    	this.bpAddress2.innerHTML = '';
		    this.bpAddress3.innerHTML = '';
		    this.bpAddress4.innerHTML = '';
		    this.bpCity.innerHTML = '';
		    this.bpState.innerHTML = '';
		    this.bpPostal.innerHTML = '';
		    this.bpPostalAdd.innerHTML = '';
		    this.bpCountry.innerHTML = '';
		    
		    this.orgName.innerHTML = '';
		    this.orgAddress.innerHTML = '';
		    this.orgAddress2.innerHTML = '';
		    this.orgAddress3.innerHTML = '';
		    this.orgAddress4.innerHTML = '';
		    this.orgCity.innerHTML = '';
		    this.orgState.innerHTML = '';
		    this.orgPostal.innerHTML = '';
		    this.orgPostalAdd.innerHTML = '';
		    this.orgCountry.innerHTML = '';
		    this.orgCity.innerHTML = '';
	},
	
	validateField : function(field){
		if (field == null || field == '' || field == 'null')
		{
			return '';
		}
		
		return field;
	}
});

var TENDER_TYPE = {
		CARD : 'Card',
		CASH : 'Cash',
		CHEQUE : 'Cheque',
		CREDIT : 'Credit',
		MIXED : 'Mixed',
		VOUCHER : 'Voucher',
		EXTERNAL_CARD : 'Ext Card',
		GIFT_CARD : 'Gift Card',
		SK_WALLET : 'SK Wallet',
		ZAPPER : 'Zapper',
		LOYALTY : 'Loyalty',
		MCB_JUICE : 'MCB Juice',
		MY_T_MONEY : 'MY.T Money',
		EMTEL_MONEY : 'Emtel Money',
		MIPS : 'MIPS',
		GIFTS_MU : 'Gifts.mu'
	};

window.onload = (event) => {

	Keypad.init();
	
	if($('message-popup')){
		new MessagePopUp().show();
	}	
	
	if($('error-popup')){
		new AlertPopUpPanel($('error-popup').innerHTML).show();
	}
	

};

window.onresize = (event) => {
	PopUpManager.centerPopUp.bind(PopUpManager);
};

/*
Event.observe(window,'resize',PopUpManager.centerPopUp.bind(PopUpManager),false);
Event.observe(window,'load',function(){
	Keypad.init();
	
	if($('message-popup')){
		new MessagePopUp().show();
	}	
	
	if($('error-popup')){
		new AlertPopUpPanel($('error-popup').innerHTML).show();
	}
	
},false);
*/


var ViewOrderMoreOptionsPanels = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('view-order-more-option-popup-panel'));
	 	this.cancelBtn = $('view-order-more-option-popup-cancelButton');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);	 	 	
	 	
	},
	
	onShow : function()
	{
		
	},
	
	cancelHandler: function()
	{
		this.hide();
	}
});

var SearchProductPanel =  Class.create(PopUpBase, {
	
	  appendHTML : function() {
		  
		  var html = ' <div class="modal" style="display: block; visibility: visible; left: 0px; top: 10px; z-index: 100000;" id="search-product-popup-panel" tabindex="-1" role="dialog" aria-labelledby="invokeOrder-popup-panel" aria-hidden="true"> ' +
			' 	<div class="modal-dialog"> ' +
			' 		<div class="modal-content"> ' +
			' 			<div class="modal-header"> ' +
			' 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="search-product-popup-cancelButton">×</button> ' +
			' 				<h4 class="modal-title"> ' +
			' 					<div class="glyphicon glyphicon-search"></div> ' +
			' 					&nbsp;' + 'Search Item' +
			' 				</h4> ' +
			' 			</div> ' +
			' 			<div class="modal-body"> ' +
			' 				<div class="row"> ' +
			' 					<div class="float-left popup-label">' + 'Search Item' + ':</div> ' +
			' 				</div> ' +
			' 				<div class="row"> ' +
			' 					<div class="input-group float-left input-sm"> ' +
			' 						<input type="text" class="form-control textField input-lg" placeholder="Barcode, Name, Description, Groups" id="search-product-popup-textfield"> ' +
			' 					</div> ' +
			' 				</div> ' +
			' 			</div> ' +
			' 			<div class="modal-footer"> ' +
			' 				<div class="row"> ' +
			' 					<button type="button" class="btn btn-lg btn-success" style="width:100px;" id="search-product-popup-okButton"> ' +
			' 						<div class="glyphicons glyphicons-pro"></div> ' +
			' 						<div>' + 'Search' + '</div> ' +
			' 					</button> ' +
			' 				</div> ' +
			' 			</div> ' +
			' 		</div> ' +
			' 	</div> ' +
			' </div> ';
		  
		jQuery(document.body).append(html);		
		
	  },
	
	  initialize: function() {
		
		if(jQuery('#search-product-popup-panel').length == 0) {
			
			this.appendHTML();
		}
		
		this.createPopUp($('search-product-popup-panel'));
	    
	    /*buttons*/
	    this.okBtn = $('search-product-popup-okButton');
	    this.cancelBtn = $('search-product-popup-cancelButton');
	    
	    /*textfields*/
	    this.searchTextField = $('search-product-popup-textfield');		    
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.searchTextField.panel = this;
	    this.searchTextField.onkeyup = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){
				e.stopPropagation();
				this.panel.validate();					
			}
		};		
	    
	  }, 
	  validate:function(){
		  var searchTerm = this.searchTextField.value;
		  SearchProduct.search({ 'searchTerm' : searchTerm });
		  this.hide();
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.searchTextField.value = '';
	  },
	  onShow:function(){
	  	this.searchTextField.focus();
	  }
});

var CreateStorePanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('create-store-panel'));
	    
	    /*inputs*/
	    this.passwordTextfield = $('create-store-panel-password-textfield');
	    	    
	    /*buttons*/
	    this.okBtn = $('create-store-panel-ok-button');
	    this.closeBtn = $('create-store-panel-close-button');
	    
	    this.operation = null;
	    	    
	    /*events*/
	    this.okBtn.onclick = this.validatePassword.bind(this);
	    this.closeBtn.onclick = this.hide.bind(this);
	    
	    this.passwordTextfield.panel = this;
	    this.passwordTextfield.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				//this.panel.clockInOutUser();			
			}
		};
	    
	       	    
	  }, 
	 	 
	  validatePassword:function()
	  {
		  var url = 'DataTableAction.do?action=validatePasswordToCreateStore&operation=' + this.operation + '&password=' + this.passwordTextfield.value;
		  jQuery.post(url,
					'',
		    		function(json, textStatus, jqXHR){
		    			
		    			if(json == null || jqXHR.status != 200){
		    				console.error('autentication failed');
		    				BackOffice.displayError('Failed to save: session timed out!');
		    				return;
		    			}
		    			
		    			if(json.error){
		    				console.error(json.error);
		    				BackOffice.displayError(json.error);
		    				return;
		    			}
		    			else
		    			{
		    				var isValid = json.valid;	
		    				
		    				if (!isValid)
		    				{
		    					BackOffice.displayError('Invalid Password!');
		    				}
		    				else
		    				{
		    					//Create store or terminal
		    					
		    					if (json.operation == 'create')
		    					{
		    						BackOffice.save(form);
		    					}
		    					
		    					
		    					//Activate store or terminal
		    					
		    					if (json.operation == 'activate')
		    					{
		    						BackOffice.activate(true);
		    					}
		    					
		    					
		    				}

		    			}
		    			
		    			
		    		},
					"json").fail(function(json, textStatus, jqXHR){
						
						if (json.status == 401)
						{
							BackOffice.preloader.hide();
							jQuery.globalEval(json.responseText);
						}
						else
						{
							console.error('request failed');
							BackOffice.displayError("Failed to process request");
						}
						
					})
					
					this.hide();
		  
	  },
	  
	  onShow:function(){
		  if(this.passwordTextfield.value == '')
		  {
			  this.passwordTextfield.focus();
		  }
		  
	  },

	  onHide:function(){
	  },
	  
	  resetHandler:function(){
		  this.passwordTextfield.value = '';		  
	  }
	  
});
