var LeadManager = {
		claimLead:function(leadId){
			/*show claim lead popup*/
			var popUp = new ClaimLeadPopUp();
			popUp.leadId = leadId;
			popUp.show();
			
		},
		
		assignLead:function(leadId){
			/*request resellers*/
			
			var url = 'ResellerAction.do?action=getResellerJSON';
			new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						var popUp = new AssignResellerPopUp();
						popUp.resellerList = json.resellerList;
						popUp.leadId = leadId;
						popUp.show();
					}
					else{
						alert("Failed to request resellers! Reason:" + json.error);
					}						
				}					
			});
		},
		
		rejectLead:function(leadId){
			/*show reject lead popup*/
			var popUp = new RejectLeadPopUp();
			popUp.leadId = leadId;
			popUp.show();			
		},
		
		closeLead:function(leadId){
			/*show close lead popup*/
			var popUp = new CloseLeadPopUp();
			popUp.leadId = leadId;
			popUp.show();
					
		},
		
		reOpenLead:function(leadId){
			/*show reOpen lead popup*/
			var popUp = new ReOpenLeadPopUp();
			popUp.leadId = leadId;
			popUp.show();
				
		},
		
		openLead:function(leadId){
			/*show open lead popup*/
			var popUp = new OpenLeadPopUp();
			popUp.leadId = leadId;
			popUp.show();
				
		},
		
		commentLead:function(leadId){
			/*request comments*/
			
			var url = 'LeadAction.do?action=getCommentsJSON&leadId=' + leadId;
			new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						var popUp = new LeadCommentPopUp();
						popUp.commentList = json.commentList;
						popUp.leadId = leadId;
						popUp.show();
					}
					else{
						alert("Failed to load comments! Reason:" + json.error);
					}						
				}					
			});
		}
};

/*popups*/
/* Assign Reseller  Popup*/
var AssignResellerPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('assignReseller.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('assignReseller.popup.okButton');
	    this.cancelBtn = $('assignReseller.popup.cancelButton');     
	    
	    /*selects*/
	    this.resellerSelectList = $('assignReseller.popup.resellerSelectList');
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
				    
	  }, 
	  validate:function(){
		  var selectedResellerId = this.resellerSelectList.value;
		  if(selectedResellerId == null || selectedResellerId == ''){
			  alert('Please choose a reseller');
			  return;
		  }
		  
		  var url = 'LeadAction.do?action=assign&leadId=' + this.leadId + "&resellerId=" + selectedResellerId;
			
		  new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						alert("Lead has been successfully assigned.");
						PopUpManager.activePopUp.hide();
						reportManager.reportAjax.reloadReport();
					}
					else{
						alert("Failed to assign lead! Reason:" + json.error);
					}
						
				}					
			});		  
		  
	  },  
	  okHandler:function(){
		  this.validate();
	  },
	  resetHandler:function(){	  	
	  },
	  onShow:function(){		  
		  var count = 0;
		  this.resellerSelectList.options.length = 0;
		  if(this.resellerList)
		  {
			  for(var i=0; i<this.resellerList.length; i++){
				  var reseller = this.resellerList[i];
				  this.resellerSelectList.options[count++] = new Option(reseller.name, reseller.id, false, false);			  
			  }
		  }
	  } 	  
});

/* Lead Comments  Popup*/
var LeadCommentPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('leadComment.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('leadComment.popup.okButton');
	    this.cancelBtn = $('leadComment.popup.cancelButton'); 
	    
	    /*textarea*/
	    this.commentTextArea = $('leadComment.popup.commentTextArea');
	    
	    /*panels*/
	    this.commentsPanel = $('leadComment.popup.commentsPanel');
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
				    
	  }, 
	  validate:function(){
		  var comment = this.commentTextArea.value;
		  if(comment == null || comment == ''){
			  alert('Please enter a comment');
			  return;
		  }
		  
		  var url = 'LeadAction.do?action=comment&leadId=' + this.leadId + "&comment=" + comment;
			
		  new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						PopUpManager.activePopUp.commentList = json.commentList;
						PopUpManager.activePopUp.resetHandler();
						PopUpManager.activePopUp.render();
					}
					else{
						alert("Failed to send comments! Reason:" + json.error);
					}
						
				}					
			});		  
		  
	  },  
	  okHandler:function(){
		  this.validate();
	  },
	  resetHandler:function(){	 
		  this.commentTextArea.value = "";
		  this.commentsPanel.innerHTML = "";
	  },
	  onShow:function(){	
		 this.render();
	  },
	  
	  render:function(){
		 var html = "";
		 for(var i=0; i<this.commentList.length; i++){
			 var comment = this.commentList[i];
			 
			 html = html + "<div class='comment " + ((i%2==0)?"odd":"even") + "'>" +
			 		"<div class='header'>" +
				 		"<div class='left'>" +
				 		comment.userName + 
				 		"</div>" +
				 		"<div class='right'>" +
				 		comment.dateCommented +
				 		"</div>" +
			 		"</div>" +
			 		"<div class='message'>" +
			 		comment.message +
			 		"</div>" +
			 	"</div>";
		 }
		 
		 this.commentsPanel.innerHTML = html;
	  }
});


/* Claim Lead  Popup*/
var ClaimLeadPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('claimLead.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('claimLead.popup.okButton');
	    this.cancelBtn = $('claimLead.popup.cancelButton');     
	    	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
				    
	  }, 
	  validate:function(){
		 		  
		  var url = 'LeadAction.do?action=claim&leadId=' + this.leadId;
			
			new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						alert("Lead has been successfully claimed.");						
						reportManager.reportAjax.reloadReport();
					}
					else{
						alert("Failed to claim lead! Reason:" + json.error);
					}
					
					PopUpManager.activePopUp.hide();						
				}					
			});		  
		  
	  },  
	  okHandler:function(){
		  this.validate();
	  },
	  resetHandler:function(){	  	
	  },
	  onShow:function(){			 
	  } 	  
});

/* Open Lead  Popup*/
var OpenLeadPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('openLead.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('openLead.popup.okButton');
	    this.cancelBtn = $('openLead.popup.cancelButton');     
	    	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
				    
	  }, 
	  validate:function(){
		 		  
		  var url = 'LeadAction.do?action=open&leadId=' + this.leadId;
			
			new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						alert("Lead has been successfully opened.");						
						reportManager.reportAjax.reloadReport();
					}
					else{
						alert("Failed to open lead! Reason:" + json.error);
					}	
					
					PopUpManager.activePopUp.hide();
				}					
			});		  
		  
	  },  
	  okHandler:function(){
		  this.validate();
	  },
	  resetHandler:function(){	  	
	  },
	  onShow:function(){			 
	  } 	  
});

/* Re-Open Lead  Popup*/
var ReOpenLeadPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('reOpenLead.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('reOpenLead.popup.okButton');
	    this.cancelBtn = $('reOpenLead.popup.cancelButton');     
	    	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
				    
	  }, 
	  validate:function(){
		 		  
		  var url = 'LeadAction.do?action=reOpen&leadId=' + this.leadId;
			
			new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						alert("Lead has been successfully re-opened.");						
						reportManager.reportAjax.reloadReport();
					}
					else{
						alert("Failed to re-open lead! Reason:" + json.error);
					}	
					
					PopUpManager.activePopUp.hide();
				}					
			});		  
		  
	  },  
	  okHandler:function(){
		  this.validate();
	  },
	  resetHandler:function(){	  	
	  },
	  onShow:function(){			 
	  } 	  
});

/* Close Lead  Popup*/
var CloseLeadPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('closeLead.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('closeLead.popup.okButton');
	    this.cancelBtn = $('closeLead.popup.cancelButton');  
	    
	    /*textfields*/
	    this.reasonTextArea = $("closeLead.popup.reasonTextArea");
	    	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
				    
	  }, 
	  validate:function(){
		  
		  if(this.reasonTextArea.value == null || this.reasonTextArea.value.length == 0){
			  alert('Please enter a reason');
			  return;
		  }
		 		  
		  var url = 'LeadAction.do?action=close&leadId=' + this.leadId + "&comment=" + this.reasonTextArea.value;
			
			new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						alert("Lead has been successfully closed.");						
						reportManager.reportAjax.reloadReport();
					}
					else{
						alert("Failed to close lead! Reason:" + json.error);
					}
					
					PopUpManager.activePopUp.hide();
				}					
			});		  
		  
	  },  
	  okHandler:function(){
		  this.validate();
	  },
	  resetHandler:function(){	 
		  this.reasonTextArea.value = "";
	  },
	  onShow:function(){			 
	  } 	  
});


/* Reject Lead  Popup*/
var RejectLeadPopUp =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('rejectLead.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('rejectLead.popup.okButton');
	    this.cancelBtn = $('rejectLead.popup.cancelButton');  
	    
	    /*textfields*/
	    this.reasonTextArea = $("rejectLead.popup.reasonTextArea");
	    	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
				    
	  }, 
	  validate:function(){
		 
		  if(this.reasonTextArea.value == null || this.reasonTextArea.value.length == 0){
			  alert('Please enter a reason');
			  return;
		  }
		  
		  var url = 'LeadAction.do?action=reject&leadId=' + this.leadId + "&comment=" + this.reasonTextArea.value;
			
			new Ajax.Request(url, {
				method:'post',
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true){
						alert("Lead has been successfully rejected.");
						reportManager.reportAjax.reloadReport();
					}
					else{
						alert("Failed to reject lead! Reason:" + json.error);
					}
					
					PopUpManager.activePopUp.hide();
				}					
			});		  
		  
	  },  
	  okHandler:function(){
		  this.validate();
	  },
	  resetHandler:function(){	 
		  this.reasonTextArea.value = "";
	  },
	  onShow:function(){			 
	  } 	  
});

