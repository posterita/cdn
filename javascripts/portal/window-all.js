/* Generated on Wed Jul 18 13:59:16 MUT 2012 
/* The file contains the following scripts: 
	- controller.js
	- controller-impl.js
	- controller-summary.js
	- window-popups.js
	- displayLogic.js
	- editor.js
*/

/*--------- start of : controller.js ---------*/
var Callout = Class.create({
	
	initialize : function(methodName, id, prefix)
	{
		this.prefix = prefix;
		var params = new Hash();
		params.set('methodName', methodName);
		params.set('id', id);
		try
		{	
			var myAjax = new Ajax.Request('PortalModelAction.do?action=callout',
			{
				method: 'POST',
				parameters: params,
				onSuccess: this.displayData.bind(this)				
			});
		}
		catch(e)
		{}
	},
	
	displayData : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		for (var i=0; i<json.list.length; i++)
		{
			var j = json.list[i];
			
			var column = $(this.prefix + '.' + j.columnName);
			if (column != null)
			{
				var value = j.value;
				if (value != null)
					column.value = j.value;
			}
			
			var blockElement = $(this.prefix + "_" + j.columnName);
			if (blockElement != null)
			{
				blockElement.style.display = 'block';
			}
		}
	}
});

var Attributes = {
		globalHash : new Hash(),
		
		store : function(elementId, attribute, value)
		{
			var localHash = Attributes.globalHash.get(elementId);
			localHash = (localHash == null) ? new Hash() : localHash;
			
			localHash.set(attribute, value);
			
			Attributes.globalHash.set(elementId, localHash);
		},
		
		retrieve : function(elementId, attribute)
		{
			var localHash = Attributes.globalHash.get(elementId);
			return localHash.get(attribute);
		},
		
		getAll : function(elementId)
		{
			return Attributes.globalHash.get(elementId);
		}
}; 

var Field = Class.create({
		initialize : function(json)
		{
			if (json != null)
			{
				this.columnName = json.columnName;
				this.field = json.field;
				this.help = null;
				this.value = json.value;
				this.value_bak = this.value;
				this.displayValue = json.displayValue;
				this.displayId = json.displayId; //needed for search box
				this.label = json.label;
				this.isDate = json.isDate;
				this.isSearch = json.isSearch;
				
				this.callout = null;
				this.columnId = json.columnId;
				this.prefix = json.prefix;
				
				this.mandatory = false;
				this.unique = false;
				this.encrypted = false;
				this.error = null;
				this.logicEventListeners = new ArrayList();
				this.isDisplayed = true;
				this.readOnly = false;
				this.textOnly = false;
				this.styleClass = null;
				this.updateable = true;
				this.defaultValue = null;
				
				this.helpComp = $('help');
			}
		},
		
		revert : function()
		{
			this.value = this.value_bak;
		},
		
		doCallout : function(text, li)
		{
			$(this.columnName).value = li.getAttribute('id');
			new Callout(this.callout, li.getAttribute('id'), this.prefix);			
		},
		
		initReplace : function()
		{
			Element.replace(this.columnName, this.field);
			
			if (this.encrypted)
			{
				var name = $(this.columnName).getAttribute('name');
				var id = $(this.columnName).getAttribute('id');
				var value = $(this.columnName).getAttribute('value');
				
				var enc = document.createElement('input');
				enc.setAttribute('type', 'password');
				enc.setAttribute('name', name);
				enc.setAttribute('id', id);
				enc.setAttribute('value', value);
				
				Element.replace(this.columnName, enc);
			}
			
			$(this.columnName).setAttribute('class', this.styleClass);
			$(this.columnName).removeAttribute('title');
			
			if (this.readOnly || this.textOnly)
			{
				if ($(this.columnName) != null)
				{
					if (this.readOnly)
						$(this.columnName).disabled = 'disabled';
					
					if (this.textOnly)
					{
						var div = document.createElement('div');
						div.setAttribute('id', this.columnName);
						div.innerHTML = (this.displayValue == null) ? "" : this.displayValue;
						
						Element.replace(this.displayId, div);
					}
				}
			}
			else
			{
				$(this.displayId).onclick = this.onClick.bind(this);
			}
			
			if (this.isDate)
			{
				Calendar.setup({
					inputField: this.displayId, 
					ifFormat : '%Y-%m-%d',
					showTime : false, 
					button : this.displayId + 'Btn'
				});
			}
			
			if (this.callout != null && !this.textOnly)
			{
				$(this.columnName+'Query').Autocompleter=new Ajax.Autocompleter(this.columnName+'Query',
						this.columnName + 'QuerySearchResult','PortalModelAction.do', {
					'paramName': 'Name',
					'parameters': 'action=search&columnId=' + this.columnId,
					'afterUpdateElement': this.doCallout.bind(this),
					'minChars': 0
				});
			}
			
			if (this.isSearch && !this.textOnly)
			{
				$(this.columnName+'Query').Autocompleter=new Ajax.Autocompleter(this.columnName+'Query',
						this.columnName + 'QuerySearchResult','PortalModelAction.do', {
								'paramName': 'Name',
								'parameters': 'action=search&columnId='+this.columnId,
								'afterUpdateElement': this.search.bind(this),
								'minChars': 0
				});
			}
		},
		
		search : function(text, li){
			$(this.columnName).value = li.getAttribute('id');
		},
	
		init : function()
		{
			if ($(this.columnName) != null)
			{
				var info = eval('(' + $(this.columnName).innerHTML + ')');
				if (info != null)
				{
					this.unique = info.unique;
					this.mandatory = info.mandatory;
					this.encrypted = info.encrypted;
					this.readOnly = info.readOnly;
					this.textOnly = info.textOnly;
					this.styleClass = info.styleClass;
					this.help = info.help;
					this.defaultValue = info.defaultValue;
					this.callout = info.callout;
					
					if (info.updateable != null)
						this.updateable = info.updateable;
				}
				
				this.initReplace();
			}
			else
				this.isDisplayed = false;
			
			if (this.disabled)
				this.disable();
			else
				this.enable();
		},
		
		initDynUpdate : function()
		{
			if ($(this.columnName) != null)
			{
				Element.replace(this.columnName, this.field);
				$(this.columnName).setAttribute('class', this.styleClass);
				$(this.columnName).removeAttribute('title');
				
				if (this.readOnly || this.textOnly)
				{
						if (this.readOnly)
							$(this.columnName).disabled = 'disabled';
						
						if (this.textOnly)
							Element.replace(this.columnName, this.value);
				}
				else
				{
					$(this.displayId).onclick = this.onClick.bind(this);
				}
			}
		},
		
		setHelpComp : function(id)
		{
			if ($(id) != null)
				this.helpComp = $(id);
		},
		
		isUpdateable : function()
		{
			return this.updateable;
		},
		
		disable : function()
		{
			var displayEl = $(this.displayId);
			if (displayEl != null)
				displayEl.setAttribute('disabled', 'disabled');
		},
		
		enable : function()
		{
			var displayEl = $(this.displayId);
			if (displayEl != null && !this.readOnly)
				displayEl.removeAttribute('disabled');
		},
		
		addLogicEventListener : function(listener)
		{
			this.logicEventListeners.add(listener);
		},
		
		onClick : function()
		{
			this.showHelp();
			this.fireLogicEvent();			
		},
		
		fireLogicEvent : function()
		{
			for (var i=0; i<this.logicEventListeners.size(); i++)
			{
				var listener = this.logicEventListeners.get(i);
				listener.logicEvent(this.getColumnName());
			}
		},
		
		setError : function(error)
		{
			this.error = error;
			$(this.displayId).setAttribute('style', 'border:2px solid #CC0000;');
		},
		
		removeError : function()
		{
			var comp = $(this.displayId);
			if (comp != null)
				comp.removeAttribute('style');
		},
		
		initDisplayError : function()
		{
			$(this.displayId).onclick = this.errorHandle.bind(this);
		},
		
		errorHandle : function()
		{
			this.showError();
			this.fireLogicEvent();
		},
		
		showError : function()
		{
			this.helpComp.innerHTML = this.error;
		},
		
		showHelp : function()
		{
			this.helpComp.innerHTML = this.help;
		},
	
		getColumnName : function()
		{
			return this.columnName;
		},
		
		getDisplayId : function()
		{
			return this.displayId;
		},
		
		getMandatory : function()
		{
			return this.mandatory;
		},
		
		getUnique : function()
		{
			return this.unique;
		},
		
		isDisplayed : function()
		{
			return this.isDisplayed;
		},
		
		setValue : function(value)
		{
			this.value = value;
			var _element = $(this.columnName);
			if (_element != null)
			{
				_element.value = value;
			}
		},
		
		getValue : function()
		{
			var _element = $(this.columnName);
			if (_element != null)
			{
				if (_element.type == 'checkbox')
					this.value = _element.checked;
				else
					this.value = _element.value;
			}
			return this.value;
		},
	
		getField : function()
		{
			return this.field;
		},
		
		getElement : function()
		{
			return $(this.columnName);
		},
	
		getHelp : function()
		{
			return this.help;
		},
	
		equals : function(element)
		{
			return (this.columnName == element.getColumnName());
		}
	
	});
	
var Model = Class.create({
	
	initialize : function(json)
	{
		if (json != null)
		{
			this.id = json.id;
			this.prefix = json.prefix;
			this.tableId = json.tableId;
			this.className = json.className;
			this.image = json.image;
			
			this.img = $(this.prefix + ".image");
			this.imgLink = $(this.prefix + ".image.link");
			
			if (this.img != null)
			{
				if (this.image != null)
					this.img.src = this.image;
			}
			
			if (this.imgLink != null)
			{
				if (this.image != null)
					this.imgLink.href = this.image;
			}
			
			if (this.image != null)
			{
				jQuery(document).ready(function(){
					/* THEMES : default, dark_rounded, dark_square, facebook, light_rounded, light_square */
					jQuery("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
				});
			}
			this.fields = new ArrayList(new Field());
			
			this.fieldMap = new Hash();
			for (var i in json)
			{
				var column = json[i];
				var field = new Field(column);
				this.fields.add(field);
				this.fieldMap.set(column.displayId, field);
			}
		}
	},
	
	setHelpComp : function(id)
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.setHelpComp(id);
		}
	},
	
	getFields : function()
	{
		return this.fields;
	},
	
	getFieldMap : function()
	{
		return this.fieldMap;
	},
	
	init : function()
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.init();
		}
	},
	
	disableFields : function()
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.disable();
		}
	},
	
	enableFields : function()
	{
		for (var i=0; i<this.fields.size(); i++)
		{
			var field = this.fields.get(i);
			field.enable();
		}
	},

	getColumnValue : function(columnName)
	{
		var field = this.fieldMap.get(columnName);
		return field.getValue();
	},
	
	setColumnValue : function(columnName, value)
	{
		var field = this.fieldMap.get(columnName);
		field.setValue(value);
	},
	
	setId : function(id)
	{
		this.id = id;
	},
	
	getId : function()
	{
		return this.id;
	},
	
	getTableId : function()
	{
		return this.tableId;
	},
	
	getClassName : function()
	{
		return this.className;
	},
	
	setActive : function(active)
	{
		var isActiveField = this.fieldMap.get(this.prefix + '.IsActive');
		isActiveField.value = active;
	},
	
	isActive : function()
	{
		var isActiveField = this.fieldMap.get(this.prefix + '.IsActive');
		return isActiveField.value;
	}
});

var Controller = Class.create({
	
	initialize : function(controller, root)
	{
		this.className = controller.name;
		this.controllerId = controller.id;
		this.processKey = controller.processKey;
		this.windowNo = controller.windowNo;
		this.root = root;
		this.interactives = new ArrayList(new Model());
		this.informatives = new ArrayList(new Model())
		this.parameters = '';
		this.parametersMap = new Hash();
		this.monitorDisplayLogic = new Hash();
		this.watchFieldMonitors = new Hash();
		
		this.deactivateListeners = new ArrayList();
		this.activateListeners = new ArrayList();
		this.deleteListeners = new ArrayList();
		this.onSaveListeners = new ArrayList();
		
		this.inactiveSticker = $('inactive_sticker');
	},
	
	isActive : function()
	{
		return this.root.isActive();
	},
	
	addDeactivateListener : function(listener)
	{
		this.deactivateListeners.add(listener);
	},
	
	addActivateListener : function(listener)
	{
		this.activateListeners.add(listener);
	},
	
	addDeleteListener : function(listener)
	{
		this.deleteListeners.add(listener);
	},
	
	addOnSaveListener : function(listener)
	{
		this.onSaveListeners.add(listener);
	},
	
	fireOnSaveEvent : function()
	{
		for (var i=0; i<this.onSaveListeners.size(); i++)
		{
			var listener = this.onSaveListeners.get(i);
			listener.controllerSaved(this.controllerId);
		}
	},
	
	fireDeleteEvent : function()
	{
		for (var i=0; i<this.deleteListeners.size(); i++)
		{
			var listener = this.deleteListeners.get(i);
			listener.deletePerformed();
		}
	},
	
	fireDeactivateEvent : function()
	{
		for (var i=0; i<this.deactivateListeners.size(); i++)
		{
			var listener = this.deactivateListeners.get(i);
			listener.deactivatePerformed();
		}
	},
	
	fireActivateEvent : function()
	{
		for (var i=0; i<this.activateListeners.size(); i++)
		{
			var listener = this.activateListeners.get(i);
			listener.activatePerformed();
		}
	},
	
	disable : function()
	{
		this.applyInactiveSticker();
		this.root.setActive(false);
		this.root.disableFields();
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.disableFields();
		}
		
		this.fireDeleteEvent();
	},
	
	enable : function()
	{
		this.removeInactiveSticker();
		this.root.setActive(true);
		this.root.enableFields();
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.enableFields();
		}
	},
	
	applyInactiveSticker : function()
	{
		if (this.inactiveSticker != null)
			this.inactiveSticker.style.display = 'inline';
	},
	
	removeInactiveSticker : function()
	{
		if (this.inactiveSticker != null)
			this.inactiveSticker.style.display = 'none';
	},
	
	getClassName : function()
	{
		return this.className;
	},
	
	getControllerId : function()
	{
		return this.controllerId;
	},
	
	getRoot : function()
	{
		return this.root;
	},
	
	setHelpComp : function(id)
	{
		this.root.setHelpComp(id);
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.setHelpComp(id);
		}
	},
	
	init : function()
	{
		this.root.init();
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			interactive.init();
		}
		
		for (var i=0; i<this.informatives.size(); i++)
		{
			var informative = this.informatives.get(i);
			informative.init();
		}
		
		this.displayLogic();
		
		var save = $('save');
		if (save != null)
		{
			save.onclick = this.save.bind(this);
		}
		var newEl = $('new');
		if (newEl != null)
		{
			newEl.href = 'PortalModelAction.do?action=load&controller=' + this.className + '&processKey=' + this.processKey+ '&id=0';
		}
		
		var cancel = $('cancel');
		if (cancel != null)
		{
			cancel.href = 'PortalModelAction.do?action=load&controller=' + this.className + '&processKey=' + this.processKey+ '&id='+this.controllerId + '&windowNo=' + this.windowNo;
		}
		
		var backreport = $('back-report');
		if (backreport != null)
		{
			/*
			backreport.href = 'ReportAction.do?action=generateReportInputForProcess&processKey=' + this.processKey;
			*/
			/*use window.history.back*/
			backreport.href = 'javascript:history.back();';
		}
		
		var activate = $('activate');
		if (activate != null)
		{
			activate.onclick = this.activate.bind(this);
		}
		
		var deactivate = $('deactivate');
		if (deactivate != null)
		{
			deactivate.onclick = this.deactivate.bind(this);
		}
		
		var del = $('delete');
		if (del != null)
			del.onclick = this.deleteAction.bind(this);
		
		var help = $('general-help');
		if (help != null)
			help.onclick = this.showHelp.bind(this);
		
		if (!this.isActive())
		{
			this.disable();
		}
		else
		{
			this.enable();
		}
		
		this.initNonUpdateableFields();
		
		this.initAjaxResponder();
		Keypad.init();
		
		this.onInit();
	},
	
	onInit : function()
	{
		
	},
	
	showHelp : function()
	{
		var helpPanel = new HelpPanel();
		helpPanel.show();
	},
	
	initNonUpdateableFields : function()
	{
		if (this.controllerId != 0)
		{
			var fields = this.root.getFields();
			
			for (var i=0; i<fields.size(); i++)
			{
				var field = fields.get(i);
				if (!field.isUpdateable())
				{
					field.disable();
				}
			}
			
			for (var i=0; i<this.interactives.size(); i++)
			{
				var interactive = this.interactives.get(i);
				var fields = interactive.getFields();
				for (var i=0; i<fields.size(); i++)
				{
					var field = fields.get(i);
					if (!field.isUpdateable())
					{
						field.disable();
					}
				}
			}
		}
	},
	
	deleteAction : function()
	{
		if (confirm('Are you sure to delete?'))
		{
			var url = 'PortalModelAction.do';
			var pars = 'action=delete&controller=' + this.className + '&id=' + this.controllerId;
			
			var ajax = new Ajax.Request(url,
			{ 
						method: 'POST', 
						parameters: pars, 
						onComplete: this.afterDelete.bind(this)
			});	
		}
	},
	
	afterDelete : function(response)
	{
		var json = eval('('+response.responseText + ')');
		var errorMsg = json.errorMsg;
		if (errorMsg != null)
		{
			alert(errorMsg);
		}
		else
		{
			$('message-display-container').innerHTML = 'DELETED';
			this.disable();
		}
	},
	
	activate : function()
	{
		var url = 'PortalModelAction.do';
		var pars = 'action=activate&controller=' + this.className + '&id=' + this.controllerId;
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.afterActivate.bind(this)
		});	
	},
	
	deactivate : function()
	{
		var url = 'PortalModelAction.do';
		var pars = 'action=deactivate&controller=' + this.className + '&id=' + this.controllerId;
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.afterDeactivate.bind(this)
		});	
	},
	
	afterActivate : function()
	{
		this.enable();
		this.initNonUpdateableFields();
		this.fireActivateEvent();
	},
	
	afterDeactivate : function(response)
	{
		try
		{
			var json = eval('(' + response.responseText + ')');
			var errorMsg = json.errorMsg;
			if (errorMsg != null)
			{
				alert(errorMsg);
			}
			else
			{
				this.disable();
				this.fireDeactivateEvent();
			}
		}
		catch(e)
		{
			this.disable();
			this.fireDeactivateEvent();
		}
	},
	
	addDependent : function(interactive)
	{
		this.interactives.add(interactive);
	},
	
	addDisplayOnly : function(informative)
	{
		this.informatives.add(informative);
	},
	
	
	addParameters : function(fields)
	{
		for (var i=0; i<fields.size(); i++)
		{
			var field = fields.get(i);
			var columnName = field.getColumnName();
			var value = field.getValue();
			
			var mandatory = field.getMandatory();
			var unique = field.getUnique();
			
			this.parameters += '&' + columnName + '=' + value;
			
			this.parameters += '&' + columnName + '.mandatory=' + mandatory;
			
			this.parameters += '&' + columnName +'.unique=' + unique;
				
		}
	},
	
	addParametersMap : function(fields)
	{
		for (var i=0; i<fields.size(); i++)
		{
			var field = fields.get(i);
			var columnName = field.getColumnName();
			var value = field.getValue();
			
			var mandatory = field.getMandatory();
			var unique = field.getUnique();
			
			this.parametersMap.set(columnName, value);
			this.parametersMap.set(columnName + '.mandatory', mandatory);
			this.parametersMap.set(columnName +'.unique', unique);
		}
	},
	
	getParametersMap : function()
	{
		this.parametersMap = new Hash();
		
		var fields = this.root.getFields();
		
		this.addParametersMap(fields);
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			this.addParametersMap(fields);
		}
		
		return this.parametersMap;
	},
	
	getParameters : function()
	{
		this.initParameters();
		return this.parameters;
	},
	
	initParameters : function()
	{
		this.parameters = '';
		var fields = this.root.getFields();
		
		this.addParametersMap(fields);
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			this.addParametersMap(fields);
		}
		
		this.parametersMap.set('controllerId', this.controllerId);
	},
	
	initAjaxResponder : function()
	{
		Ajax.Responders.register({
		  onCreate: function() {
		    $('indicator').style.display = 'block';
		  },
		  onComplete: function() {
		    $('indicator').style.display = 'none';
		  }
		});
	},
	
	beforeSave : function()
	{
		return true;
	},
	
	save : function()
	{
		if (!this.beforeSave())
			return;
		
		this.initParameters();
		
		var url = 'PortalModelAction.do';
		var pars = 'action=save&controller=' + this.className  + '&windowNo=' + 
					this.windowNo + '&id=' + this.controllerId + '&' + this.parametersMap.toQueryString();
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.showStatus.bind(this)
		});	
	},
	
	showStatusLog : function(log)
	{
		var localLogs = log.local;
		var globalLogs = log.global;
		
		var errorFields = new ArrayList(new Field());
		if (localLogs != null || globalLogs != null)
		{	
			$('message-display-container').className = 'message-display-container-error';
			$('message-display-container').innerHTML = 'ERROR';
			
			for (var displayId in localLogs)
			{
				var errorMsg = localLogs[displayId];
					
				var field = this.getField(displayId);
				if (field != null)
				{
					errorFields.add(field);
					field.setError(errorMsg);
					field.initDisplayError();
				}
			}
			
			this.clearOtherErrors(errorFields);
			
			var errorPopUpPanel = new ErrorPopUpPanel(globalLogs);
			errorPopUpPanel.init();
		}
		else
		{
			$('message-display-container').className = 'message-display-container-saved';
			$('message-display-container').innerHTML = 'SAVED';
			this.clearAllErrors();
			var state = log.state;
			if (state != null)
			{
				this.controllerId = state.controllerId;
				this.initNonUpdateableFields();
				this.updateState(state);
				this.fireOnSaveEvent();
			}
		}
	},
	
	updateFields : function()
	{
		
	},
	
	showStatus : function(response)
	{
		var log = eval('(' + response.responseText + ')');
		this.showStatusLog(log);
	},
	
	clearAllErrors : function()
	{
		var rootFields = this.root.getFields();
		for (var i=0; i<rootFields.size(); i++)
		{
			var field = rootFields.get(i);
			field.removeError();
		}
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			
			for (var i=0; i<fields.size(); i++)
			{
				var interactiveField = fields.get(i);
				interactiveField.removeError();
			}
		}
	},
	
	clearOtherErrors : function(errorFields)
	{
		if (errorFields == null || errorFields.size() == 0)
		{
			this.clearAllErrors();
			return;
		}
		
		var rootFields = this.root.getFields();
		for (var i=0; i<rootFields.size(); i++)
		{
			var field = rootFields.get(i);

			if (!errorFields.contains(field))
			{
				field.removeError();
			}
		}
		
		for (var i=0; i<this.interactives.size(); i++)
		{
			var interactive = this.interactives.get(i);
			var fields = interactive.getFields();
			
			for (var i=0; i<fields.size(); i++)
			{
				var interactiveField = fields.get(i);
				if (!errorFields.contains(interactiveField))
				{
					interactiveField.removeError();
				}
			}
		}
		
	},
	
	getField : function(displayId)
	{
		var mainMap = this.root.getFieldMap();
		var field = mainMap.get(displayId);
		if (field != null)
			return field;
		else
		{
			for (var i=0; i<this.interactives.size(); i++)
			{
				var interactive = this.interactives.get(i);
				var fieldMap = interactive.getFieldMap();
				field = fieldMap.get(displayId);
				if (field != null)
					return field;
			}
		}
		return null;
	},
	
	updateState : function(state)
	{},
	
	displayLogic : function()
	{},
	
	addDisplayLogic : function(monitor, displayLogic)
	{
		this.monitorDisplayLogic.set(monitor, displayLogic);
		
		var watchFields = this.getWatchFields(displayLogic);
		for (var i=0; i<watchFields.size(); i++)
		{
			var watchField = watchFields.get(i);
			watchField.addLogicEventListener(this);
			
			var monitors = this.watchFieldMonitors.get(watchField);
			
			if (monitors == null)
			{
				monitors = new ArrayList();
				this.watchFieldMonitors.set(watchField, monitors);
			}
			
			monitors.add(monitor);
			watchField.fireLogicEvent();
		}
	},
	
	getWatchFields : function(displayLogic)
	{
		var watchFields = new ArrayList();
		var splits = displayLogic.match(/\@.*?\@/g);
		for (var i=0; i<splits.length; i++)
		{
			var split = splits[i];
			
			var watchColumnName = split.substring(1, (split.length-1));
			var watchField = this.getField(watchColumnName);
			if (watchField != null)
			{
				watchFields.add(watchField);
			}
		}
		return watchFields;
	},
	
	logicEvent : function(watchColumnName)
	{
		var watchField = this.getField(watchColumnName);
		if (watchField != null)
		{
			var watchValue = watchField.getValue();
			var monitors = this.watchFieldMonitors.get(watchField);
			if (monitors != null)
			{
				for (var i=0; i<monitors.size(); i++)
				{
					var monitor = monitors.get(i);
					var displayLogic = this.monitorDisplayLogic.get(monitor);
					var watchFields = this.getWatchFields(displayLogic);
					var display = this.evaluateLogic(displayLogic, watchFields);
					var monitorElement = $(monitor);
					if (monitorElement != null)
					{
						var styleDisplay = (display)? 'block' : 'none';
						monitorElement.style.display = styleDisplay;
					}
				}
			}
		}
	},
	
	evaluateLogic : function(displayLogic, watchFields)
	{
		displayLogic = displayLogic.replace(/\@/g, '');
		for (var i=0; i<watchFields.size(); i++)
		{
			var watchField = watchFields.get(i);
			var columnName = watchField.getColumnName();
			var value = watchField.getValue();
			
			if (value == null || value == '')
				return false;
			
			var regExp = new RegExp(columnName, 'g');
			displayLogic = displayLogic.replace(regExp, "'" + value + "'");
		}
		
		return eval('('+displayLogic+')')
	},
	
	getProcessKey : function()
	{
		return this.processKey;
	},
	
	getId : function()
	{
		return this.controllerId;
	}
});
/*--------- end of : controller.js ---------*/
/*--------- start of : controller-impl.js ---------*/
var ProductController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json.product);
		var priceListSO = new Model(json.priceListSO);
		var priceListPO = new Model(json.priceListPO);
		var priceSO = new Model(json.priceSO);
		var pricePO = new Model(json.pricePO);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
		this.addDependent(pricePO);
		
		this.addDisplayOnly(priceListSO);
		this.addDisplayOnly(priceListPO);
	},
	
	onInit : function()
	{
		var options = $('product.ProductType').options;
		for (var i=0; i<options.length; i++)
		{
			var option = options[i];
			if (option.value == 'I' || option.value == 'S')
			{
				option.style.display = 'block';
			}
			else
			{
				option.style.display = 'none';
			}
		}

		var uomOptions = $('product.C_UOM_ID').options;
		if (this.getId() == 0)
		{
			for (var i=0; i<uomOptions.length; i++)
			{
				var option = uomOptions[i];
				if (option.value == '100')
				{
					option.selected = 'selected';
				}
				else
				{
					option.removeAttribute('selected');
				}
			}
		}
	},
	
	updateState : function(state)
	{
		if ($('product.summary.newPrice') != null)
		{
			$('product.summary.newPrice').innerHTML = state.PriceStd;
		}
		
		if ($('product.summary.oldPrice') != null)
		{
			$('product.summary.oldPrice').innerHTML = state.PriceList;
		}
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('product_Group' + i);
			var input = $('product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
					element.style.display = 'block';
				else
					element.style.display = 'none';
			}
		}
		
		$('product.PrimaryGroup').onkeyup = function(e)
		{
			$('product_Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('product.Group' + i);
			var group_next = this.getField('product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					
					var nextElement = $("product_" + name + (no+1));
					
					if (nextElement != null)
						nextElement.style.display = 'block';
				}
			}
		}
	}
});


var ProductTemplateController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json.product);
		var priceSO = new Model(json.priceSO);
		var pricePO = new Model(json.pricePO);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
		this.addDependent(pricePO);
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('product_Group' + i);
			var input = $('product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
					element.style.display = 'block';
				else
					element.style.display = 'none';
			}
		}
		
		$('product.PrimaryGroup').onkeyup = function(e)
		{
			$('product_Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('product.Group' + i);
			var group_next = this.getField('product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					var nextElement = $("product_" + name + (no+1));
					if (nextElement != null)
						nextElement.style.display = 'block';
				}
			}
		}
	}
});


var ProductLeftLookupController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json['left.product']);
		var priceSO = new Model(json['left.priceSO']);
		var pricePO = new Model(json['left.pricePO']);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
		this.addDependent(pricePO);
		
		this.setHelpComp('lookup-item-help');
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('left.product_Group' + i);
			var input = $('left.product.Group' + i);
			var copyLeftDiv = $('product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
				{
					element.style.display = 'block';
					copyLeftDiv.style.display = 'block';
				}
				else
				{
					element.style.display = 'none';
					copyLeftDiv.style.display = 'none';
				}
			}
		}
		
		$('left.product.PrimaryGroup').onkeyup = function(e)
		{
			$('left.product_Group1').style.display = 'block';
			$('product.Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('left.product.Group' + i);
			var group_next = this.getField('left.product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('left.product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					var nextElement = $('left.product_'+name + (no+1));
					if (nextElement != null)
					{
						nextElement.style.display = 'block';
						$('product.Group' + (no +1)).style.display = 'block';
					}
				}
			}
		}
	}
});

var ProductRightLookupController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var product = new Model(json['right.product']);
		var priceSO = new Model(json['right.priceSO']);
		var pricePO = new Model(json['right.pricePO']);
		
		$super(controller, product);
		
		this.addDependent(priceSO);
		this.addDependent(pricePO);
		
		this.setHelpComp('lookup-item-help');
	},
	
	displayLogic : function()
	{
		for (var i=1; i<=8; i++)
		{
			var element = $('right.product_Group' + i);
			var input = $('right.product.Group' + i);
			if (input != null)
			{
				var value = input.value;
				if (value != null && value.strip().length != 0)
					element.style.display = 'block';
				else
					element.style.display = 'none';
			}
		}
		
		$('right.product.PrimaryGroup').onkeyup = function(e)
		{
			$('right.product_Group1').style.display = 'block';
		}
		
		for (var i=1; i<8; i++)
		{
			var group_current = this.getField('right.product.Group' + i);
			var group_next = this.getField('right.product.Group' + (i+1));
			
			var name =  "Group";
			
			var element = $('right.product.' + name + i);
			if (element != null)
			{
				element.onkeyup = function(e){
					
					var id = this.getAttribute('id');
					var no = new Number(id.substring((id.length-1), id.length));
					var nextElement = $('right.product_'+name + (no+1));
					if (nextElement != null)
						nextElement.style.display = 'block';
				}
			}
		}
	}
});

var OrgController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		
		var organisation = new Model(json.organisation);		
		var partner = new Model(json.partner);
		var user = new Model(json.user);
		var location = new Model(json.location);
		var orgInfo = new Model(json.orgInfo);
		
		$super(controller, organisation);
		
		this.addDependent(partner);
		this.addDependent(user);
		this.addDependent(location);
		this.addDependent(orgInfo);
	},
	
	displayLogic : function()
	{
		var displayLogic = '(@location.C_Country_ID@ == "100")';
		this.addDisplayLogic('State', displayLogic);
	}
});

var PartnerController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var partner = new Model(json.partner);
		var user = new Model(json.user);
		var location = new Model(json.location);
		
		$super(controller, partner);
		
		this.addDependent(user);
		this.addDependent(location);
	}
});

var VendorController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var partner = new Model(json.vendor_partner);
		var user = new Model(json.vendor_user);
		var location = new Model(json.vendor_location);
		
		$super(controller, partner);
		
		this.addDependent(user);
		this.addDependent(location);
	}
});

var UserController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.partner = new Model(json.partner);
		this.user = new Model(json.user);
		this.location = new Model(json.location);
		this.userRoles = new Model(json.userRoles);
		
		$super(controller, this.user);
		this.addDependent(this.partner);
		this.addDependent(this.location);
		this.addDependent(this.userRoles);
	},
	
	isActive : function()
	{
		return this.user.isActive();
	},
	
	disable : function($super)
	{
		$super();
		$('user.ConfirmPassword').disable();
	},
	
	enable : function($super)
	{
		$super();
		$('user.ConfirmPassword').enable();
	},
	
	init : function($super)
	{
		$super();
		var password = $('user.Password').value;
		$('user.ConfirmPassword').value = password;
	},
	
	beforeSave : function()
	{
		return this.confirmPassword();
	},
	
	confirmPassword : function()
	{
		var password = $('user.Password').value;
		var confirm = $('user.ConfirmPassword').value;
		
		if (password != confirm)
		{
			alert('passwords do not match');
			return false;
		}
		return true;
	}
});

var PaymentTermController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var paymentTerm = new Model(json.paymentTerm);
		
		$super(controller, paymentTerm);
	},
	
	displayLogic : function()
	{
		var displayLogic = '(@paymentTerm.IsDueFixed@ == true)';
		this.addDisplayLogic('FixMonthDay', displayLogic);
		this.addDisplayLogic('FixMonthOffset', displayLogic);
		this.addDisplayLogic('FixMonthCutoff', displayLogic);
	}
	
});


var PaymentController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var payment = new Model(json.payment);
		
		$super(controller, payment);
	},
	
	displayLogic : function()
	{
		var checkDL = "(@payment.TenderType@ == 'K')";
		this.addDisplayLogic('CheckNo', checkDL);
		
		var creditCardDL = "(@payment.TenderType@ == 'C')";
		this.addDisplayLogic('Swipe', creditCardDL);
		this.addDisplayLogic('CreditCardType', creditCardDL);
		this.addDisplayLogic('CreditCardNumber', creditCardDL);
		this.addDisplayLogic('CreditCardExpMM', creditCardDL);
		this.addDisplayLogic('CreditCardExpYY', creditCardDL);
		this.addDisplayLogic('CreditCardVV', creditCardDL);
		this.addDisplayLogic('AccountName', creditCardDL);
		this.addDisplayLogic('AccountStreet', creditCardDL);
		this.addDisplayLogic('AccountZip', creditCardDL);
	}
	
});

var PaymentProcessorController = Class.create(Controller, {
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.paymentProcessor = new Model(json.paymentProcessor);
		
		$super(controller, this.paymentProcessor);
		
		this.dynamicValidationJSON = new Hash();
		
		this.dynamic = new Hash();
		this.dynamic.set('columnName', 'AD_Org_ID');
		
		var bankAccount = new Hash();
		bankAccount.set('columnName', 'C_BankAccount_ID');
		bankAccount.set('prefix', this.paymentProcessor.prefix);
		bankAccount.set('tableId', this.paymentProcessor.tableId);
		
		var dependents = new ArrayList();
		dependents.add(bankAccount);
		
		this.dynamicValidationJSON.set('dependents', dependents);
	},
	
	init : function($super)
	{
		$super();
		$('paymentProcessor.PayProcessorClass').onchange = this.initCallout.bind(this);
		this.initDynamicValidation();
	},
	
	initCallout : function()
	{
		var payProcessorClass = $('paymentProcessor.PayProcessorClass').value;
		if (payProcessorClass == null) return; 
		
		if (payProcessorClass == 'org.compiere.model.PP_XWeb')
		{
			$('paymentProcessor.HostAddress').value = 'https://gw.t3secure.net/x-chargeweb.dll';
			$('paymentProcessor.HostPort').value = '0';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Authorize')
		{
			$('paymentProcessor.HostAddress').value = 'https://secure.authorize.net/gateway/transact.dll';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_ElementPS')
		{
			$('paymentProcessor.HostAddress').value = 'https://transaction.elementexpress.com';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Mercury_HF')
		{
			$('paymentProcessor.HostAddress').value = 'https://hc.mercurypay.com/hcws/hcservice.asmx';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Mercury_EE')
		{
			$('paymentProcessor.HostAddress').value = 'https://w2.backuppay.com/ws/ws.asmx';
			$('paymentProcessor.HostPort').value = '443';
		}
		else if (payProcessorClass == 'org.compiere.model.PP_Century')
		{
			$('paymentProcessor.HostAddress').value = 'https://secure.nmi.com/api/v2/three-step';
			$('paymentProcessor.HostPort').value = '443';
		}
		else
		{
			$('paymentProcessor.HostAddress').value = '';
			$('paymentProcessor.HostPort').value = '';
		}
	},
	
	dynamicValidation : function()
	{
		$('paymentProcessor.AD_Org_ID').onchange = this.initDynamicValidation.bind(this);
	},
	
	initDynamicValidation : function()
	{
		this.dynamic.set('value', this.paymentProcessor.getColumnValue('paymentProcessor.AD_Org_ID'));
		this.dynamicValidationJSON.set('dynamic', this.dynamic);
		
		var url = 'PortalModelAction.do';
		var pars = 'action=dynamicValidation&controller=' + this.className + '&id=' + 
		this.controllerId + '&windowNo=' + this.windowNo + '&dynamicValidation=' + this.dynamicValidationJSON.toJSON();
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.updateDynamicFields.bind(this)
		});	
	},
	
	updateDynamicFields : function(response)
	{
		var fieldMap = this.paymentProcessor.getFieldMap();
		var json = eval('(' + response.responseText + ')');
		for (var j in json)
		{
			var column = json[j];
			var f = fieldMap.get(column.displayId);
			f.field = column.field;
			
			f.initDynUpdate();
		}
		
		this.initNonUpdateableFields();
	}
});

var TerminalController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.terminal = new Model(json.terminal);
		
		$super(controller, this.terminal);		
		
		this.dynamicValidationJSON = new Hash();
		
		this.dynamic = new Hash();
		this.dynamic.set('columnName', 'AD_Org_ID');
		
		var dependents = new ArrayList();
		var cardBA = new Hash();
		var cashBook = new Hash();
		var checkBA = new Hash();
		var warehouse = new Hash();
		var voucherBA = new Hash();
		
		cardBA.set('columnName', 'Card_BankAccount_ID');
		cashBook.set('columnName', 'C_CashBook_ID');
		checkBA.set('columnName', 'Check_BankAccount_ID');
		warehouse.set('columnName', 'M_Warehouse_ID');
		voucherBA.set('columnName', 'voucher_bankaccount_id');
		
		cardBA.set('prefix', this.terminal.prefix);
		cashBook.set('prefix', this.terminal.prefix);
		checkBA.set('prefix', this.terminal.prefix);
		warehouse.set('prefix', this.terminal.prefix);
		voucherBA.set('prefix', this.terminal.prefix);
		
		cardBA.set('tableId', this.terminal.tableId);
		cashBook.set('tableId', this.terminal.tableId);
		checkBA.set('tableId', this.terminal.tableId);
		warehouse.set('tableId', this.terminal.tableId);
		voucherBA.set('tableId', this.terminal.tableId);
		
		dependents.add(cardBA);
		dependents.add(cashBook);
		dependents.add(checkBA);
		dependents.add(warehouse);
		dependents.add(voucherBA);
		
		this.dynamicValidationJSON.set('dependents', dependents);
	},
	
	dynamicValidation : function()
	{
		$('terminal.AD_Org_ID').onchange = this.initDynamicValidation.bind(this);
	},
	
	initDynamicValidation : function()
	{
		this.dynamic.set('value', this.terminal.getColumnValue('terminal.AD_Org_ID'));
		this.dynamicValidationJSON.set('dynamic', this.dynamic);
		
		var url = 'PortalModelAction.do';
		var pars = 'action=dynamicValidation&controller=' + this.className + '&id=' + 
		this.controllerId + '&windowNo=' + this.windowNo + '&dynamicValidation=' + this.dynamicValidationJSON.toJSON();
		
		var ajax = new Ajax.Request(url,
		{ 
					method: 'POST', 
					parameters: pars, 
					onComplete: this.updateDynamicFields.bind(this)
		});	
	},
	
	updateDynamicFields : function(response)
	{
		var fieldMap = this.terminal.getFieldMap();
		var json = eval('(' + response.responseText + ')');
		for (var j in json)
		{
			var column = json[j];
			var field = fieldMap.get(column.displayId);
			field.field = column.field;
			
			field.initReplace();
		}
	}
	
});

var ResellerController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.reseller = new Model(json.reseller);
		
		$super(controller, this.reseller);
	},
	
	isActive : function()
	{
		return this.reseller.isActive();
	},
	
	disable : function($super)
	{
		$super();
		$('reseller.ConfirmPassword').disable();
	},
	
	enable : function($super)
	{
		$super();
		$('reseller.ConfirmPassword').enable();
	},
	
	init : function($super)
	{
		$super();
		var password = $('reseller.Password').value;
		$('reseller.ConfirmPassword').value = password;
	},
	
	beforeSave : function()
	{
		return this.confirmPassword();
	},
	
	confirmPassword : function()
	{
		var password = $('reseller.Password').value;
		var confirm = $('reseller.ConfirmPassword').value;
		
		if (password != confirm)
		{
			alert('passwords do not match');
			return false;
		}
		return true;
	}
});

var WarehouseController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		var warehouse = new Model(json.warehouse);
		var location = new Model(json.location);
		
		$super(controller, warehouse);
		
		this.addDependent(location);
	}	
});

var CloseTillController = Class.create(Controller, {
	
	initialize : function($super, json)
	{
		var controller = json.controller;
		this.closeTill = new Model(json.closeTill);
		
		$super(controller, this.closeTill);
		
	},
	
	initKeyLogic : function()
	{
		$('closeTill.TotalCashAmount').onkeyup = this.computeTotalAmt.bind(this);
		$('closeTill.VoucherAmtEntered').onkeyup = this.computeTotalAmt.bind(this);		
		
		var beginningBalance = this.closeTill.getColumnValue('closeTill.BeginningBalance');
		var floatAmt = this.closeTill.getColumnValue('closeTill.FloatAmount');
		
		var totalCash = parseFloat($('closeTill.TotalCashAmount').value);
		
		if (isNaN(totalCash) || totalCash == 0)
		{
			$('CashSales').innerHTML = 0;
			$('TotalCashAmount').innerHTML = beginningBalance;
			$('TransferAmount').innerHTML = beginningBalance - floatAmt;
			$('EndingBalance').innerHTML = floatAmt;
		}
	},
	
	computeTotalAmt : function()
	{
		var totalCash = parseFloat($('closeTill.TotalCashAmount').value);
		if (isNaN(totalCash))
		{
			totalCash = 0;
		}
		
		var voucherAmt = parseFloat($('closeTill.VoucherAmtEntered').value);
		if (isNaN(voucherAmt))
		{
			voucherAmt = 0;
		}
		
		var cardAmt = parseFloat($('closeTill.CardAmount').value);
		var chequeAmt = parseFloat($('closeTill.ChequeAmount').value);
		var extCardAmt = parseFloat($('closeTill.ExternalCreditCardAmt').value);
				
		var totalAmt = totalCash + voucherAmt + cardAmt + chequeAmt + extCardAmt;
		totalAmt = (new Number(totalAmt)).toFixed(2);
		$('TotalAmount').innerHTML = totalAmt;
		
		
		var beginningBalance = this.closeTill.getColumnValue('closeTill.BeginningBalance');
		var floatAmt = this.closeTill.getColumnValue('closeTill.FloatAmount');
		
		var cashSales = parseFloat(totalCash - beginningBalance).toFixed(2);
		$('CashSales').innerHTML = cashSales;
		$('TotalCashAmount').innerHTML = totalCash;
		
		var transferAmt = parseFloat(totalCash - floatAmt).toFixed(2);
		$('TransferAmount').innerHTML = transferAmt;
		
		$('EndingBalance').innerHTML = floatAmt;
	},	
	
	
	showCloseTillErrors : function()
	{
		$('closeTill.TotalCashAmount').value = 'ERROR';
	}
});
/*--------- end of : controller-impl.js ---------*/
/*--------- start of : controller-summary.js ---------*/
var SummaryInfo = Class.create({
	initialize : function(summaryInfo)
	{
		this.summaryInfo = summaryInfo;
	},
	
	init : function()
	{
		for (var field in this.summaryInfo)
		{
			var summaryField = $(field);
			
			if (summaryField != null)
			{
				summaryField.style.display = 'block';
				summaryField.innerHTML = this.summaryInfo[field];
			}
		}
	},
	
	getSummaryInfo : function()
	{
		return this.summaryInfo;
	}
});

var ProductSummary = Class.create(SummaryInfo, {
	
	initialize : function($super, summary)
	{
		$super(summary);
	},
	
	init : function($super)
	{
		$super();
		
		var warehouses = this.summaryInfo['product.summary.allStock'];
		
		var totalQty = 0;
		
		for (var i=0; i<warehouses.length; i++)
		{
			var wh = warehouses[i];
			var name = wh.name;
			var id = wh.id;
			var qty = wh.qty;
			
			totalQty += qty;
		}
		
		$('product.summary.allStock').innerHTML = totalQty;
		
		var productPairing = this.summaryInfo['product.summary.productPairing'];
		if (productPairing != null)
		{
			var boms = productPairing.boms;
			
			var t = '<table>';
			for (var i=0; i<boms.length; i++)
			{
				var bom = boms[i];
				var name = bom.name;
				var qty = bom.qty;
				
				t += '<tr><td>' + qty + 'x </td><td>' + name + '</td></tr>';
			}
			
			t+= '</table>';
			
			$('product.pairing').innerHTML = t;
		}
	}
});

var TaxSummary = Class.create({
	
	initialize : function(summaryInfo)
	{
		this.summaryInfo = summaryInfo;
	},
	
	init : function()
	{
		var subTaxes = this.summaryInfo.subTaxes;
		
		var t = '<table>';
		
		for (var i=0; i<subTaxes.length; i++)
		{
			var subTax = subTaxes[i];
			var name = subTax.name;
			var rate = subTax.rate;
			
			t += '<tr><td>' + name + '</td><td>' + rate + '%</td></tr>';
		}
		
		t+= '</table>';
		
		$('sub.taxes').innerHTML = t;
	}
});
/*--------- end of : controller-summary.js ---------*/
/*--------- start of : window-popups.js ---------*/
var PopUpPanel = Class.create(PopUpBase, {
	
	initialize : function(popUpPanel, trigger)
	{
		this.popUpPanelId = popUpPanel;
		this.triggerBtn = null;
		this.popUp = null;
		this.className = trigger;
		this.controllerClass = null;
		this.processKey = null;
		this.tableId = 0;
		this.modelClass = null;
		this.modelId = 0;
		this.okBtn = null;
		this.cancelBtn = null;
	},
	
	setProcessKey : function(processKey)
	{
		this.processKey = processKey;
	},
	
	setControllerClass : function(controllerClass)
	{
		this.controllerClass = controllerClass;
	},
	
	setTableId : function(tableId)
	{
		this.tableId = tableId;
	},
	
	setModelId : function(modelId)
	{
		this.modelId = modelId;
	},
	
	setModelClass : function(modelClass)
	{
		this.modelClass = modelClass;
	},
	
	init : function()
	{
		this.triggerBtn = $(this.className);
		this.popUp = $(this.popUpPanelId);
		
		if (this.popUp != null)
		{
			this.createPopUp(this.popUp);
			this.initBtns();
			this.initPanelActions();
		}
		
		Keypad.init();
	},
	
	initPanelActions : function()
	{
		this.triggerBtn.onclick = this.initOnCall.bind(this);
		this.okBtn.onclick = this.initOnOk.bind(this);
		this.cancelBtn.onclick = this.initOnCancel.bind(this);
	},
	
	hideBtn : function()
	{
		this.triggerBtn.style.display = 'none';
	},
	
	showBtn : function()
	{
		this.triggerBtn.style.display = 'block';
	},
	
	disable : function()
	{
		this.triggerBtn.onclick = function(e){};
		this.hideBtn();
	},
	
	enable : function()
	{
		this.triggerBtn.onclick = this.initOnCall.bind(this);
		this.showBtn();
	},
	
	initOnCall : function()
	{
		if (this.modelId != 0)
		{
			var url = 'PortalModelAction.do';
			var pars = 'action=popup&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId;
			
			var ajax = new Ajax.Request(url,
			{ 
				method: 'POST', 
				parameters: pars, 
				onComplete: this.callHandler.bind(this)
			});
		}
	},
	
	initOnOk : function()
	{
		this.hide();
		var url = 'PortalModelAction.do';
		var pars = 'action=popupOk&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.okHandler.bind(this)
		});	
	},
	
	initOnCancel : function()
	{
		this.afterCancel();
	},
	
	afterCancel : function()
	{
		this.hide();
	},
	
	callHandler : function(request)
	{
		var json = eval('('+request.responseText+')');
		this.afterCall(json);
		this.show();
		
	},
	
	okHandler : function(request)
	{
		this.hide();
		var response = request.responseText;
		var json = eval('('+request.responseText+')');
		this.afterOk(json);
	},
	
	initBtns : function()
	{},
	
	afterCall : function(json)
	{},
	
	afterOk : function(json)
	{},
	
	getParameters : function()
	{}	
});

var OrgPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger, message)
	{
		$super('organisation', trigger);
		this.selectComp = null;
		this.message = message;
	},
	
	initBtns : function()
	{
		this.okBtn = $('organisation.okBtn');
		this.cancelBtn = $('organisation.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.selectComp = document.createElement('select');
		var orgs = json.orgs;
		if (orgs != null)
		{
			for (var i=0; i<orgs.length; i++)
			{
				var org = orgs[i];
				var name = org.name;
				var id = org.id;
				var selected = org.selected;
				
				var option = document.createElement('option');
				option.setAttribute('value', id);
				option.innerHTML = name;
				if (selected)
				{
					option.setAttribute('selected', true);
				}
				
				this.selectComp.appendChild(option);
			}
		}
		$('organisation.component').innerHTML = '';
		$('organisation.component').appendChild(this.selectComp);
		
		var orgModels = $$('span.organisation-model');
		for (var i=0; i<orgModels.length; i++)
		{
			var orgModel = orgModels[i];
			orgModel.innerHTML = this.message;
		}
	},
	
	getParameters : function()
	{
		var orgId = this.selectComp.value;
		var parameters = "{'orgId':"+ orgId +"}";
		return parameters;
	},
	
	afterOk : function(json)
	{}
});

var ProductOrgPanel = Class.create(OrgPanel, {
	
	initialize : function($super, trigger, message)
	{
		$super(trigger, message);
	},
	
	afterOk : function(json)
	{
		$('product.summary.orgName').innerHTML = json.orgName;
	}
});

var  PartnerOrgPanel = Class.create(OrgPanel, {
	
	initialize : function($super, trigger, message)
	{
		$super(trigger, message);
	},
	
	afterOk : function(json)
	{
		$('partner.summary.orgName').innerHTML = json.orgName;
	}
});

var ImagePanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('imagePanel', trigger);
	},
	
	initBtns : function()
	{
		this.okBtn = $('image.okBtn');
		this.cancelBtn = $('image.cancelBtn');
		this.takePictureBtn = $('image.takePictureBtn');
		
		this.takePictureBtn.onclick = this.takePicture.bind(this);
	},
	
	takePicture:function()
	{
		  this.hide();
		  PopUpManager.removeActivePopUp(this);
		  new PhotoBoothPanel().show();
	},
	
	initOnCall : function()
	{
		if (this.modelId != 0)
		{
			$('image.panel.frame').src = "AttachmentServlet?tableId="+ this.tableId + "&id=" + this.modelId;
			this.show();
		}
	},
	
	initOnOk : function()
	{
		this.uploadImage();
	},
	
	uploadImage : function()
	{
		var imageForm = document.forms.ImageForm;
		imageForm.action.value = 'uploadImagePortal';
		imageForm.tableId.value = this.tableId;
		imageForm.recordId.value = this.modelId;
		imageForm.controller.value = this.controllerClass;
		imageForm.processKey.value = this.processKey;
		imageForm.submit();
	}
});

var ProductPrice = Class.create({
	
	initialize : function(json)
	{
		if (json!= null)
		{
			this.isTaxIncluded = json.isTaxIncluded;
			this.name = json.name;
			this.priceListId = json.priceListId;
			this.productId = json.productId;
			this.priceListVersionId = json.priceListVersionId;
			this.priceStd = json.priceStd;
			this.priceList = json.priceList;
			this.priceLimit = json.priceLimit;
			this.currentPrice = json.currentPrice;
			
			this.initComponent();
		}
	},
	
	initComponent : function()
	{
		this.std_box = this.createBox(this.priceStd);
		this.list_box = this.createBox(this.priceList);
		this.limit_box = this.createBox(this.priceLimit);
		
		this.std_box.onchange = this.updatePriceStd.bind(this);
		this.list_box.onchange = this.updatePriceList.bind(this);
		this.limit_box.onchange = this.updatePriceLimit.bind(this);
	},
	
	getCurrentPrice : function()
	{
		return this.currentPrice;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getTaxIncluded : function()
	{
		return this.isTaxIncluded;
	},
	
	getPriceStdComp : function()
	{
		return this.std_box;
	},
	
	getPriceListComp : function()
	{
		return this.list_box;
	},
	
	getPriceLimitComp : function()
	{
		return this.limit_box;
	},
	
	updatePriceStd : function()
	{
		this.priceStd = this.std_box.value;
	},
	
	updatePriceList : function()
	{
		this.priceList = this.list_box.value;
	},
	
	updatePriceLimit : function()
	{
		this.priceLimit = this.limit_box.value;
	},
	
	createBox : function(value)
	{
		var box = document.createElement('input');
		box.setAttribute('type', 'text');
		box.setAttribute('value', value);
		box.setAttribute('class', 'pricelistinput');
		return box;
	}
});

var ProductSettings = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('product.settings', trigger);
		this._editPriceOnFly = $('EditPriceOnFly');
		this._editDescOnFly = $('EditDescriptionOnFly');
	},
	
	initBtns : function()
	{
		this.okBtn = $('product.settings.okBtn');
		this.cancelBtn = $('product.settings.cancelBtn');
	},
	
	
	afterCall : function(json)
	{
		this._editDescOnFly.checked = json.editDescOnFly;
		this._editPriceOnFly.checked = json.editPriceOnFly;
		this.show();
	},
	
	afterOk : function(json)
	{
		var editDescOnFly = json.editDescOnFly;
		var editPriceOnFly = json.editPriceOnFly;
		
		$('product.summary.editPrice').innerHTML = (editPriceOnFly) ? "Yes" : "No";
		$('product.summary.editDesc').innerHTML = (editDescOnFly) ? "Yes" : "No";
	},
	
	getParameters : function()
	{
		var editDescOnFly = this._editDescOnFly.checked;
		var editPriceOnFly = this._editPriceOnFly.checked;
		
		var h = new Hash();
		h.set('editDescOnFly', editDescOnFly);
		h.set('editPriceOnFly', editPriceOnFly);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});

var ProductPairing = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger, product)
	{
		$super('product.pairing.panel', trigger);
		this.product = product;
		this.productSearch = null;
		this.productBOMGrid = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('product.pairing.okBtn');
		this.cancelBtn = $('product.pairing.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.productBOMGrid = new ProductBOMGrid(this.product, json.boms)
		this.productSearch = new ProductSearch(this.productBOMGrid);
		
		this.productBOMGrid.buildGrid();
		this.productSearch.initializeComponents();
		
		this.show();
	},
	
	afterOk : function(productPairing)
	{
		if (productPairing != null)
		{
			var boms = productPairing.boms;
			
			var t = '<table>';
			for (var i=0; i<boms.length; i++)
			{
				var bom = boms[i];
				var name = bom.name;
				var qty = bom.qty;
				
				t += '<tr><td>' + qty + 'x </td><td>' + name + '</td></tr>';
			}
			
			t+= '</table>';
			
			$('product.pairing').innerHTML = t;
		}
	},
	
	onShow : function()
	{
		jQuery('#bom-grid').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	},
	
	getParameters : function()
	{
		var parameters = new Hash();
		
		var params = new ArrayList();
		
		var productBOMs =this.productBOMGrid.getProductBOMs();
		for (var i=0; i<productBOMs.size(); i++)
		{
			var productBOM = productBOMs.get(i);
			
			var h = new Hash();
			h.set('bomId', productBOM.getBOMId());
			h.set('productId', productBOM.getProductId());
			h.set('qty', productBOM.getQty());
			h.set('line', productBOM.getLine());
			
			params.add(h);
		}
		
		parameters.set('boms', params)
		return parameters.toJSON();
	}
});

var ProductPricesPanel = Class.create(PopUpPanel, {
	initialize : function($super, trigger)
	{
		$super('product.prices', trigger);
		this.soProductPrices = new ArrayList(new ProductPrice());
		this.poProductPrices = new ArrayList(new ProductPrice());
	},
	
	clear : function()
	{
		this.soProductPrices.clear();
		this.poProductPrices.clear();
	},
	
	afterCall : function(json)
	{
		this.clear();
		var soPrices = json.soPrices;
		var poPrices = json.poPrices;
		
		this.addProductPrices(soPrices, this.soProductPrices);
		this.addProductPrices(poPrices, this.poProductPrices);
		this.initPopup();
		this.show();
	},
	
	getParameters : function()
	{
		var parameters = '{ "soPrices":' + this.soProductPrices.toJSON();
		parameters += ', "poPrices":' + this.poProductPrices.toJSON();
		parameters += '}';
		
		return parameters;
	},
	
	initPopup : function()
	{
		this.flush();
		
		for (var i=0; i<this.soProductPrices.size(); i++)
		{
			var productPrice = this.soProductPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			var tdOriginal = document.createElement('td');
			var tdLimit = document.createElement('td');
			
			tdCurrent.setAttribute('class', 'pricelistbg');
			tdOriginal.setAttribute('class', 'pricelistbg');
			tdLimit.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			tdOriginal.appendChild(productPrice.getPriceListComp());
			tdLimit.appendChild(productPrice.getPriceLimitComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			tr.appendChild(tdOriginal);
			tr.appendChild(tdLimit);
			
			Element.insert($('product.prices.so.header'), {after:tr});
		}
		
		for (var i=0; i<this.poProductPrices.size(); i++)
		{
			var productPrice = this.poProductPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			
			tdCurrent.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			
			Element.insert($('product.prices.po.header'), {after:tr});
		}
	},
	
	flush : function()
	{
		var soTable = $('product.prices.so.table');
		var soHeader = $('product.prices.so.header');
		
		soTable.innerHTML = '';
		soTable.appendChild(soHeader);
		
		var poTable = $('product.prices.po.table');
		var poHeader = $('product.prices.po.header');
		
		poTable.innerHTML = '';
		poTable.appendChild(poHeader);
	},
	
	addProductPrices : function(jsonPrices, productPrices)
	{
		for (var i=0; i<jsonPrices.length; i++)
		{
			var json = jsonPrices[i];
			var productPrice = new ProductPrice(json);
			productPrices.add(productPrice);
		}
	},
	
	afterOk : function(json)
	{
		/*var soPrice = json.currentPriceSO;
		var poPrice = json.currentPricePO;
		
		$('priceSO.PriceStd').value = soPrice.priceStd;
		$('priceSO.PriceList').value = soPrice.priceList;
		$('priceSO.PriceLimit').value = soPrice.priceLimit;
		
		$('product.summary.newPrice').innerHTML = soPrice.priceStd;
		$('product.summary.oldPrice').innerHTML = soPrice.priceList;
		
		$('pricePO.PriceStd').value = poPrice.priceStd;*/
	},
	
	initBtns : function()
	{
		this.okBtn = $('product.prices.okBtn');
		this.cancelBtn = $('product.prices.cancelBtn');
	}
});

var TaxCategoryPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('tax.category', trigger);
		this.selectComp = null;
		this.hash = new Hash();
	},
	
	initBtns : function()
	{
		this.okBtn = $('tax.category.okBtn');
		this.cancelBtn = $('tax.category.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.selectComp = document.createElement('select');
		this.selectComp.setAttribute('id', 'tax.category.select');
		
		var taxCategories = json.taxCategories;
		for (var i=0; i<taxCategories.length; i++)
		{
			var taxCategory = taxCategories[i];
			var name = taxCategory.name;
			var id = taxCategory.id;
			var selected = taxCategory.selected;
			var details = taxCategory.details;
			
			this.hash.set(id, details);
			
			var option = document.createElement('option');
			option.setAttribute('value', id);
			option.innerHTML = name;
			option.onclick = this.displayDetails.bind(this, option.value);
			if (selected)
			{
				option.setAttribute('selected', true);
				this.displayDetails(option.value);
			}
			
			this.selectComp.appendChild(option);
		}
		$('taxCategoryId').innerHTML = '';
		$('taxCategoryId').appendChild(this.selectComp);
	},
	
	displayDetails : function(id)
	{
		this.flush();
		var details = this.hash.get(id);
		for (var i=0; i<details.length; i++)
		{
			var detail = details[i];
			var orgName = detail.orgName == null? "" : detail.orgName;
			var taxRate = detail.taxRate == null ? "" : detail.taxRate;
			var terminalName = detail.terminalName == null ? "" : detail.terminalName;
			
			var tr = document.createElement('tr');
			var tdOrg = document.createElement('td');
			var tdRate = document.createElement('td');
			var tdTerminal = document.createElement('td');
			
			tdOrg.innerHTML = orgName;
			tdRate.innerHTML = taxRate;
			tdTerminal.innerHTML = terminalName;
			
			tr.appendChild(tdOrg);
			tr.appendChild(tdTerminal);
			tr.appendChild(tdRate);
			
			Element.insert($('tax.category.header'), {after:tr});
		}
	},
	
	flush : function()
	{
		var table = $('tax.category.table');
		var header = $('tax.category.header');
		
		table.innerHTML = '';
		table.appendChild(header);
	},
	
	afterOk : function(json)
	{
		var taxCat = json['product.summary.taxCategoryName'];
		$('product.summary.taxCategoryName').innerHTML = taxCat;
	},
	
	getParameters : function()
	{
		var taxCategoryId = this.selectComp.value;
		var parameters = "{'taxCategoryId':" + taxCategoryId + "}";
		return parameters;
	}
});

/* Product Info Panel */
var ProductInfo =  Class.create(ProductInfoPanel, {
	
	initialize: function($super, product, summary) 
	{
		$super();
		this.product = product;
		this.summary = summary;
	}, 
	
	getInfo:function()
	{
		var summaryInfoJSON = this.summary.getSummaryInfo();
		
		var columnNames = new ArrayList();
		columnNames.add('Description');
		columnNames.add('Name');
		columnNames.add('UPC');
		
		var root = this.product.getRoot();
		var map = root.getFieldMap();
		var prefix = root.prefix;
		
		var orgName = summaryInfoJSON['product.summary.orgName'];
		var productType = $('product.ProductType');
		var uom = $('product.C_UOM_ID');
		var productId = root.getId();
		
		var productTypeVal = productType.options[productType.selectedIndex].label;
		var uomValue = uom.options[uom.selectedIndex].label;
		
		var basicInfo = '{"AD_Org_ID": {"value" : "' + orgName + '", "label" : "' + orgName + '"}, ';
		basicInfo += '"ProductType": {"value" : "' + productTypeVal + '", "label" : "' + productTypeVal + '"}, ';
		basicInfo += '"C_UOM_ID": {"value" : "' + uomValue + '", "label" : "' + uomValue + '"}, ';
		basicInfo += '"M_Product_ID": {"value" : "' + productId + '", "label" : "' + productId + '"}, ';
		basicInfo += '"CurrencySymbol":"' + summaryInfoJSON['product.summary.currencySymbol'] + '",';
		for (var i=0; i<columnNames.size(); i++)
		{
			var columnName = columnNames.get(i);
			var field = map.get(prefix + "." + columnName);
			var value = field.value;
			var label = field.label;
			var json = '{"value" : "' + value + '", "label" : "' + label + '"}';
			
			basicInfo += '"' + columnName + '":' + json;
			
			if (i < (columnNames.size() - 1))
					basicInfo += ',';
		}
		
		basicInfo += '}';
		
		var groupCols = new ArrayList();
		groupCols.add('PrimaryGroup');
		for (var i=1; i<=8; i++)
		{
			var columnName = 'Group' + i;
			groupCols.add(columnName);
		}
		
		var groups = '[';
		for (var i=0; i<groupCols.size(); i++)
		{
			var columnName = groupCols.get(i);
			var group = prefix + '.' + columnName;
			
			var field = map.get(group);
			var value = field.value;
			var label = field.label;
			
			groups += '{"' + columnName + '" : {"value": "' + value + '", "label" : "' + label + '"}}';
			if (i < (groupCols.size() - 1))
			{
				groups += ','
			}
		}
		groups += ']';
		
		var stocksList = summaryInfoJSON['product.summary.allStock'];
		
		var stocks = new Hash();
		for (var i=0; i<stocksList.length; i++)
		{
			var stock = stocksList[i];
			stocks.set(stock.name, stock.qty);
	    }
		
		var priceStd = summaryInfoJSON['product.summary.newPrice'];
		var priceList = summaryInfoJSON['product.summary.oldPrice'];
		var isTaxIncluded = summaryInfoJSON['product.summary.isTaxIncluded'];
		
		var taxName = summaryInfoJSON['product.summary.taxName'];
		
		this.productInfo = {'basicInfo': eval('(' + basicInfo + ')'),
					'groups': eval('(' +groups + ')'),
					'imageURL': $('product.image').src,
					'hasImageAttachment' : true,
					'isTaxInclusive' : isTaxIncluded,
					'priceList': priceList,
					'tax' : taxName,
					'priceStd': priceStd,
					'stocks': eval('(' +stocks.toJSON() + ')')};
		
		this.show();
	}	
});

var Warehouse = Class.create({
	
	initialize : function(name, id, qty)
	{
		this.name = name;
		this.id = id;
		this.qty = qty;
		
		this.initComponent();
	},
	
	initComponent : function()
	{
		this.component = document.createElement('input');
		this.component.setAttribute('type', 'text');
		this.component.setAttribute('value', this.qty);
		this.component.onchange = this.updateQty.bind(this);
	},
	
	updateQty : function()
	{
		this.qty = this.component.value;
	},
	
	getComponent : function()
	{
		return this.component;
	},
	
	getWarehouseId : function()
	{
		return this.warehouseId;
	},
	
	getQty : function()
	{
		return this.component.value;
	},
	
	equals : function (warehouse)
	{
		return (warehouse.getWarehouseId() == this.warehouseId);
	}
});

var StockPanel = Class.create(PopUpPanel, {
	initialize : function($super, trigger)
	{
		$super('update.stock', trigger);
		this.warehouseList = new ArrayList(new Warehouse());
	},
	
	initBtns : function()
	{
		this.okBtn = $('update.stock.okBtn');
		this.cancelBtn = $('update.stock.cancelBtn');
	},
	
	afterCall : function(json)
	{
		var warehouses = json.warehouses;
		var stockHeader = $('stock.header');
		var stockTable = $('stock.table');
		stockTable.innerHTML = '';
		
		stockTable.appendChild(stockHeader);
		
		for (var i=0; i<warehouses.length; i++)
		{
			var tr = document.createElement('tr');
			var wh = warehouses[i];
			var name = wh.name;
			var id = wh.id;
			var qty = wh.qty;
			
			var warehouse = new Warehouse(name, id, qty);
			this.warehouseList.add(warehouse);
			
			var style = 'text-align:left;padding-left:50px';
			
			var tdName = document.createElement('td');
			var tdComp = document.createElement('td');
			
			tdName.setAttribute('style', style);
			tdComp.setAttribute('style', style);
			
			tdName.innerHTML = name;
			tdComp.appendChild(warehouse.getComponent());
			
			tr.appendChild(tdName);
			tr.appendChild(tdComp);
			
			Element.insert('stock.header', {after : tr});
		}
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error != null)
		{
			alert(error);
		}
		else
		{
			var stock = json.stock;
			var allStock = json.allStock;
			
			$('product.summary.stock').innerHTML = stock;
			$('product.summary.allStock').innerHTML = allStock;
		}
	},
	
	getParameters : function()
	{
		var parameters = "{'warehouses' : " + this.warehouseList.toJSON() + "}";
		return parameters;
	}
});


var PriceListPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('price.list.panel', trigger);
		this.selectComp = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('price.list.panel.okBtn');
		this.cancelBtn = $('price.list.panel.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.selectComp = document.createElement('select');
		this.selectComp.setAttribute('class','price-list-select-dimen');
		var priceLists = json.priceLists;
		for (var i=0; i<priceLists.length; i++)
		{
			var priceList = priceLists[i];
			var id = priceList.id;
			var name = priceList.name;
			
			var selected = priceList.selected;
			
			var option = document.createElement('option');
			option.setAttribute('value', id);
			option.innerHTML = name;
			if (selected)
			{
				option.setAttribute('selected', true);
			}
			
			this.selectComp.appendChild(option);
		}
		
		$('price.list.component').innerHTML = '';
		$('price.list.component').appendChild(this.selectComp);
	},
	
	afterOk : function(json)
	{
		$('partner.summary.priceListName').innerHTML = json.priceListName;
	},
	
	getParameters : function()
	{
		var priceListId = this.selectComp.value;
		var parameters = "{'priceListId':"+ priceListId +"}";
		return parameters;
	}
});

var CreditManagementPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('credit.management.panel', trigger);
		
		this.creditStatusComp = document.createElement('select');
		this.creditStatusComp.setAttribute('class','credit-status-select-dimen');
	},
	
	initBtns : function()
	{
		this.okBtn = $('credit.management.okBtn');
		this.cancelBtn = $('credit.management.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.creditStatusComp.innerHTML = '';
		var cs = json.creditStatus;
		for (var i=0; i<cs.length; i++)
		{
			var creditS = cs[i];
			var value = creditS.value;
			var display = creditS.display;
			var selected = creditS.selected;
			
			var option = document.createElement('option');
			option.setAttribute('value', value);
			option.innerHTML = display;
			if (selected)
			{
				option.setAttribute('selected', true);
			}
			
			this.creditStatusComp.appendChild(option);
		}
		
		$('credit.status').innerHTML = '';
		$('credit.status').appendChild(this.creditStatusComp);
		
		$('credit.limit').value = json.creditLimit;
		$('credit.watch').value = json.creditWatch;
	},
	
	afterOk : function(json)
	{
		$('partner.summary.creditStatus').innerHTML = json.creditStatus;
		$('partner.summary.creditLimit').innerHTML = json.creditLimit;
		$('partner.summary.creditWatch').innerHTML = json.creditWatch;
	},
	
	getParameters : function()
	{
		var creditStatus = this.creditStatusComp.value;
		var creditLimit = $('credit.limit').value
		var creditWatch = $('credit.watch').value
		
		var h = new Hash();
		h.set('creditStatus', creditStatus);
		h.set('creditLimit', creditLimit);
		h.set('creditWatch', creditWatch);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});

var CloseTillPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('close.till.panel', trigger);
		
		this.totalCashAmount = $('closeTill.TotalCashAmount');
		this.voucherAmountEntered = $('closeTill.VoucherAmtEntered');
		this.closeTillController = null;
	},
	
	setCloseTillController: function(controller)
	{
		this.closeTillController = controller;
	},
	
	initBtns : function()
	{
		this.okBtn = $('close.till.okBtn');
		this.cancelBtn = $('close.till.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	afterOk : function(json)
	{
		var success = json.success;
		
		if (success)
		{
			window.location = 'TillAction.do?action=viewCloseTill&id=' + json.cashJournalId + '&print=true';
		}
		else
		{
			var errorType = json.errorType;
			if (errorType == 'saveError')
			{
				this.closeTillController.showStatusLog(json.errorLog);
			}
			else
			{
				alert(json.errorMsg);
			}
		}
	},
	
	getParameters : function()
	{
		var paramMap = this.closeTillController.getParametersMap();
		var d = new Date();
		var clientTime = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
		
		paramMap.set('clientTime', clientTime);
						
		return paramMap.toJSON();
	}
});

var PopUpGrid = Class.create({
	
	initialize : function(controller)
	{
		this.controller = controller;
		this.panels = new ArrayList();
		this.root = this.controller.getRoot();
		this.activateBtn = $('activate');
		this.deactivateBtn = $('deactivate');
	},
	
	addPanel : function(panel)
	{
		this.panels.add(panel);
	},
	
	init : function()
	{
		var controllerClass = this.controller.getClassName()
		this.controller.addOnSaveListener(this);
		this.controller.addDeactivateListener(this);
		this.controller.addActivateListener(this);
		this.controller.addDeleteListener(this);
		
		var controllerId = this.controller.getControllerId();
		
		var processKey = this.controller.getProcessKey();
		
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			
			var className = this.root.getClassName();
			var tableId = this.root.getTableId();
			var id = this.root.getId();
			
			panel.setControllerClass(controllerClass);
			panel.setModelClass(className);
			panel.setModelId(id);
			panel.setTableId(tableId);
			panel.setProcessKey(processKey);
			panel.init();
		}
		
		if (controllerId != 0)
		{
			if (!this.controller.isActive())
				this.deactivatePerformed();
			else
				this.activatePerformed();
		}
		else
		{
			this.disableAllPanels();
			this.deactivateBtn.style.display = 'none';
			this.activateBtn.style.display = 'none';
			$('save').show();
		}
	},
	
	controllerSaved : function(controllerId)
	{
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			panel.setModelId(controllerId);
			if (controllerId != 0)
			{
				panel.enable();
			}
		}
		
		if (!this.controller.isActive())
			this.deactivatePerformed();
		else
			this.activatePerformed();
	},
	
	deactivatePerformed : function()
	{
		this.disableAllPanels();
		$('delete').show();
		
		this.toggleActivateBtn();
	},
	
	activatePerformed : function()
	{
		this.enableAllPanels();
		
		this.toggleDeactivateBtn();
	},
	
	deletePerformed : function()
	{
		this.disableAllPanels();
		this.deactivateBtn.hide();
		this.activateBtn.hide();
	},
	
	disableAllPanels : function()
	{
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			panel.disable();
		}
		
		$('save').hide();
		$('cancel').hide();
		$('delete').hide();
	},
	
	enableAllPanels : function()
	{
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			panel.enable();
		}
		$('save').show();
		$('cancel').show();
		$('delete').show();
	},
	
	toggleActivateBtn : function()
	{
		if (this.activateBtn != null)
			this.activateBtn.style.display = 'block';
		if (this.deactivateBtn != null)
			this.deactivateBtn.style.display = 'none';
	},
	
	toggleDeactivateBtn : function()
	{
		if (this.deactivateBtn != null)
			this.deactivateBtn.style.display = 'block';
		if (this.activateBtn != null)
			this.activateBtn.style.display = 'none';
	}
});

var CreateAccountPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('lead.create.account.confirm.panel', trigger);
		
		this.controller = null;
	},
	
	setController: function(controller)
	{
		this.controller = controller;
	},
	
	initBtns : function()
	{
		this.okBtn = $('lead.create.account.okBtn');
		this.cancelBtn = $('lead.create.account.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	afterOk : function(json)
	{
		var success = json.success;
		
		if (success)
		{
			$('message-display-container').innerHTML = 'Account Created';
			$('createLeadAccount').style.display = 'none';
		}
		else
		{
			alert(json.errorMsg);
		}
	},
	
	getParameters : function()
	{
		var h = new Hash();
		h.set('leadId', this.controller.getControllerId());
		
		return h.toJSON();
	}
});

var CompletePaymentPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('complete.payment.panel', trigger);
		
		this.c_bpartner_id = $('payment.C_BPartner_ID');
		this.payamt = $('payment.PayAmt');
		this.tenderType = $('payment.TenderType');
		this.completePaymentController = null;
	},
	
	setCompletePaymentController: function(controller)
	{
		this.completePaymentController = controller;
	},
	
	initBtns : function()
	{
		this.okBtn = $('complete.payment.okBtn');
		this.cancelBtn = $('complete.payment.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	disable : function()
	{},
	
	afterOk : function(json)
	{
		var success = json.success;
		
		if (success)
		{
			window.location = 'PaymentAction.do?action=view&id=' + json.paymentId;
		}
		else
		{
			var errorType = json.errorType;
			if (errorType == 'completeError')
			{
				this.completePaymentController.showStatusLog(json.errorLog);
			}
			else
			{
				alert(json.errorMsg);
			}
		}
	},
	
	getParameters : function()
	{
		var c_bpartner_id = this.c_bpartner_id.value;
		var payamt = this.payamt.value;
		var tenderType = this.tenderType.value;
		
		var h = new Hash();
		h.set('c_bpartner_id', c_bpartner_id);
		h.set('payamt', payamt);
		h.set('tenderType', tenderType);
		
		var parameters = h.toJSON();
						
		return parameters;
	}
});

var HelpPanel = Class.create(PopUpBase, {
	initialize : function()
	{
	 	this.createPopUp($('help.panel'));
	 	this.okBtn = $('help.panel.okBtn');
	 	this.cancelBtn = $('help.panel.cancelBtn');
	 	
	 	this.cancelBtn.onclick = this.hide.bind(this);
	 	this.okBtn.onclick = this.hide.bind(this);
	}
});

var TaxSub = Class.create({
	
	initialize : function(taxSubPanel, json)
	{
		this.taxSubPanel = taxSubPanel;
		if (json != null)
		{
			this.name = json.name;
			this.rate = json.rate;
			this.id = json.id;
			this.comp = null;
			
			this.buildComp();
		}
	},
	
	getId : function()
	{
		return this.id;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getRate : function()
	{
		return this.rate;
	},
	
	getComponent : function()
	{
		return this.comp;
	},
	
	buildComp : function()
	{
		this.comp = document.createElement('div');
		this.comp.setAttribute('class', 'tax-sub-container');
		
		this.nameDiv = document.createElement('div');
		this.nameDiv.setAttribute('class', 'tax-sub-name');
		
		this.rateDiv = document.createElement('div');
		this.rateDiv.setAttribute('class', 'tax-sub-rate');
		
		this.deleteDiv = document.createElement('div');
		this.deleteDiv.setAttribute('class', 'tax-sub-delete');
		
		this.nameDiv.innerHTML = this.name;
		this.rateDiv.innerHTML = this.rate + ' %';
		
		this.deleteBtn = document.createElement('a');
		this.deleteBtn.setAttribute('class', 'delete-small');
		this.deleteBtn.setAttribute('href', 'javascript:void()');
		
		this.deleteDiv.appendChild(this.deleteBtn);
		
		this.comp.appendChild(this.nameDiv);
		this.comp.appendChild(this.rateDiv);
		this.comp.appendChild(this.deleteDiv);
		
		this.deleteBtn.onclick = this.remove.bind(this);
	},
	
	toJSON : function()
	{
		var hash = new Hash();
		hash.set('name', this.name);
		hash.set('id', this.id);
		hash.set('rate', this.rate);
		
		return hash.toJSON();
	},
	
	remove : function()
	{
		this.taxSubPanel.removeTaxSub(this);
	},
	
	equals : function(taxSub)
	{
		return (this.getId() == taxSub.getId());
	}
});

var TaxSubPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('tax.sub.panel', trigger);
		
		this.contentPanel = $('tax.sub.content');
		
		this.taxSubs = new ArrayList(new TaxSub());
		
		this.addBtn = null;
		
		this.nameInput = $('tax.sub.name.input');
		this.rateInput = $('tax.sub.rate.input');
	},
	
	initBtns : function()
	{
		this.okBtn = $('tax.sub.okBtn');
		this.cancelBtn = $('tax.sub.cancelBtn');
		
		this.addBtn = $('tax.sub.addBtn');
		this.addBtn.onclick = this.add.bind(this);
	},
	
	add : function()
	{
		var name = this.nameInput.value;
		var rate = this.rateInput.value;
		
		if (name == null || name.trim().length == 0)
		{
			return;
		}
		
		if (rate == null || rate.trim().length == 0)
		{
			rate = 0;
		}
		
		var json = {'name' : escape(name), 'rate' : rate, 'id' : 0};
		var taxSub = new TaxSub(this, json);
		this.addTaxSub(taxSub);
	},
	
	afterCall : function(json)
	{
		this.taxSubs.clear();
		
		var subTaxes = json.subTaxes;
		this.contentPanel.innerHTML = '';
		if (subTaxes != null)
		{
			for (var i=0; i<subTaxes.length; i++)
			{
				var tax = subTaxes[i];
				var taxSub = new TaxSub(this, tax);
				
				this.addTaxSub(taxSub);
			}
		}		
	},
	
	addTaxSub : function(taxSub)
	{
		this.taxSubs.add(taxSub);
		
		var comp = taxSub.getComponent();
		
		this.contentPanel.appendChild(comp);
	},
	
	removeTaxSub : function(taxSub)
	{
		if (this.taxSubs.contains(taxSub))
		{
			var comp = taxSub.getComponent();
			this.contentPanel.removeChild(comp);
			
			this.taxSubs.remove(taxSub);
		}
	},
	
	getParameters : function()
	{
		var parameters = "{'subTaxes':" + this.taxSubs.toJSON() + "}";
		
		return parameters;
	},
	
	afterOk : function(json)
	{
		var summaryInfo = new TaxSummary(json);
		summaryInfo.init();
	}
});
/*--------- end of : window-popups.js ---------*/
/*--------- start of : displayLogic.js ---------*/
var DisplayLogic = Class.create({
	initialize : function()
	{
		this.monitorToDisplayFunction = new Hash();
		this.monitoredToMonitors = new Hash();
		this.monitoredList = new ArrayList();		
	},
	
	setMonitorDisplayLogic : function(monitor, displayLogicFunction)
	{
		this.monitorToDisplayFunction.set(monitor, displayLogicFunction)
	},
	
	addMonitor : function(monitor, listOfItemsMonitored)
	{
		for (var i=0;i<listOfItemsMonitored.length;i++)
		{
			var monitored = listOfItemsMonitored[i];
			var isElementPresentInList = this.monitoredList.contains(monitored);
			if (!isElementPresentInList)
			{
				this.monitoredList.add(monitored);
			}
			var monitors = this.monitoredToMonitors.get(monitored);
			if (monitors == null)
			{
				monitors = new ArrayList();
			}
			monitors.add(monitor);
			this.monitoredToMonitors.set(monitored, monitors);
		}
	},
	
	run : function()
	{
		var disp = new Display(this.monitorToDisplayFunction, this.monitoredToMonitors);
		for (var i=0; i<this.monitoredList.size(); i++)
		{
			var monitored = this.monitoredList.get(i);
			var monitoredEditor = new Editor(monitored);
			var monitoredTag = monitoredEditor.getColumnTag();
			
			disp.display(monitored);
			monitoredTag.onchange = function(e)
			{
				disp.display(this.id);
				
			};
		}		
	}
});

var Display = Class.create({
	initialize : function (monitorToDisplayFunction, monitoredToMonitors)
	{
		this.monitorToDisplayFunction = monitorToDisplayFunction;
		this.monitoredToMonitors = monitoredToMonitors;
	},
	
	isMonitorDisplayed : function(monitor)
	{
		var displayLogicFunction = this.monitorToDisplayFunction.get(monitor);
		return displayLogicFunction();
	},
	
	display : function(monitored)
	{
		var monitors = this.monitoredToMonitors.get(monitored);			
		for (var i=0; i<monitors.size(); i++)
		{
			var monitor = monitors.get(i);
			var monitorEditor = new Editor(monitor);
			var isDisplay = this.isMonitorDisplayed(monitor);
			
			var textOnly = $(monitorEditor.getColumnName() + 'TextOnly');
			if (textOnly != null)
			{
				var textOnlyEditor = new Editor(monitorEditor.getColumnName() + 'TextOnly');
				textOnlyEditor.display(isDisplay);
			}
			else
			{
				monitorEditor.display(isDisplay);
			}
		}
	}	
});

/*--------- end of : displayLogic.js ---------*/
/*--------- start of : editor.js ---------*/
var Editor = Class.create({
	initialize: function(colName)
	{
		this.columnName = colName;
		this.columnTag = $(colName);
		this.tagName = this.columnTag.tagName;
		this.columnTagLabel = $(colName + 'Label');
		this.value = null;
	},
	
	getValue : function()
	{
		if (this.tagName == 'INPUT')
		{
			if (this.columnTag.type == 'checkbox')
			{
				if (this.columnTag.checked == true)
				{
					this.value = 'Y';
				}
				else
				{
					this.value = 'N';
				}
			}	
			else
			{
				this.value = this.columnTag.value;
			}
		}
		else if (this.tagName == 'SPAN')
		{
			this.value = this.columnTag.innerHTML;
		}
		else
		{
			this.value = this.columnTag.value;
		}
		
		return this.value;
	},

	setValue : function(val)
	{
		if (this.tagName == 'INPUT')
		{
			if (this.columnTag.type == 'checkbox')
			{
				this.columnTag.checked = val;
			}			
		}
		else if (this.tagName == 'SPAN')
		{
			this.columnTag.innerHTML = val; 
		}
		
		this.columnTag.value = val;
		this.value = val;
		var textOnly = $(this.columnName + 'TextOnly');
		if (textOnly != null)
			textOnly.innerHTML = this.value;
	},

	
	display : function(isDisplay)
	{
		if (isDisplay == true)
		{
			this.columnTag.style.display = 'inline';
			this.columnTagLabel.style.display = 'inline';
		}
		else
		{
			this.columnTag.style.display = 'none';
			this.columnTagLabel.style.display = 'none';
		}
	},
	
	getColumnTag : function()
	{
		return this.columnTag;
	},
	
	getColumnName : function()
	{
		return this.columnName;
	}
});

var NumberEditor = Class.create({
	initialize: function(colName)
	{
		this.editor = new Editor(colName);
	},
	
	setValue : function (val)
	{
		var value = this.getNumber(val);
		this.editor.setValue(value);
	},
	
	getValue : function()
	{
		var value = this.editor.getValue();
		return this.getNumber(value);
	},
	
	getNumber :  function(val)
	{
		if (val != null)
		{
			var value = String(val);
			var valNo = value.replace(/\,/g,'');
			var num = new Number(valNo).toFixed(2);
			return parseFloat(num);
		}
		else
			return null;
	},
});
/*--------- end of : editor.js ---------*/
