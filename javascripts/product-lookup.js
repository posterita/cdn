var Product = Class.create({
	
	initialize : function()
	{
		this.upc = null;
		this.name = null;
		this.description = null;
		this.extendedDescription = null;
		this.extendedDescriptionTrunc = null;
		this.productId = 0;
		this.imageURL = null;
		this.primaryGroup = null;
		this.type = null;
		
		this._parentContainer = null;
		this._imageContainer = null;
		this._descriptionContainer = null;
		
		this.productController = null;

		this.listener = null;
		
		this.initComponents();
	},
	
	initComponents : function()
	{
		this._parentContainer = document.createElement('tr');
		this._imageContainer = document.createElement('td');
		this._descriptionContainer = document.createElement('td');
		
		this._imageContainer.setAttribute('class', 'lookup-image-box');
		this._descriptionContainer.setAttribute('class', 'lookup-description-box');
		
		this._parentContainer.appendChild(this._imageContainer);
		this._parentContainer.appendChild(this._descriptionContainer);
	},
	
	initFields : function(json)
	{
		this.upc = json.upc;
		this.name = json.name;
		this.description = json.description;
	},
	
	initController : function(controller)
	{
		$('lookup.fields').innerHTML = LookupFields.fields;
		
		this.productController = controller;
		this.productController.init();
		
		var product = this.productController.getRoot();
		
		this.productId = product.getId();
		
		var prefix = product.prefix;
		
		this.upc = product.getColumnValue(prefix + '.UPC');
		this.name = product.getColumnValue(prefix + '.Name');
		this.description = product.getColumnValue(prefix + '.Description');
		this.extendedDescription = product.getColumnValue(prefix + '.ExtendedDescription');
		this.primaryGroup = product.getColumnValue(prefix + '.PrimaryGroup');
		
		this.extendedDescriptionTrunc = (this.extendedDescription != null) ? 
				this.extendedDescription.substring(0, 100) : this.description;
	},
	
	initLoader : function(text)
	{
		this._imageContainer.innerHTML = '<div class="indicator sub-indicator"></div>';
		this._descriptionContainer.innerHTML = '<div class="loading">'+text+'</div>';
	},
	
	updateLoader : function()
	{
		this._imageContainer.innerHTML = '<img src= "'+this.imageURL+'"/>';
		this._descriptionContainer.innerHTML = '<div class="loading">' + this.name +
												'<br>' + this.extendedDescriptionTrunc + '</div>';
		
		this._parentContainer.onclick = this.onSelect.bind(this);
	},
	
	onSelect : function()
	{
		if (this.listener != null)
			this.listener.productSelected(this);
	},
	
	initLookup : function(queryText)
	{
		var param = new Hash();
		param.set('action', 'productLookup');
		param.set('searchTerm', queryText);
		
		new Ajax.Request('ProductLookupAction.do', {
			method:'get',
			onSuccess: this.fireLookupEvent.bind(this),
			parameters:param
		});
	},
	
	fireLookupEvent : function(response)
	{
		var json = response.responseText.evalJSON(true);
		
		if (this.listener != null)
			this.listener.productLookedUp(this, json);
	},
	
	setListener : function(listener)
	{
		this.listener = listener;
	},
	
	highlight : function()
	{
		this._parentContainer.addClassName('search-product-highlight');
	},
	
	removeHighlight : function()
	{
		this._parentContainer.removeClassName('search-product-highlight');
	},
	
	getComponent : function()
	{
		return this._parentContainer;
	},
	
	getUpc : function()
	{
		return this.upc;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getDescription : function()
	{
		return this.description;
	},
	
	getProductId : function()
	{
		return this.productId;
	},
	
	setType : function(type)
	{
		this.type = type;
	},
	
	getType : function()
	{
		return this.type;
	},

	setImageURL : function(imageURL)
	{
		this.imageURL = imageURL;
	},
	
	getImageURL : function()
	{
		return this.imageURL;
	},
	
	getListener : function()
	{
		return this.listener;
	},
	
	paramJSON : function()
	{
		var hash = new Hash();
		hash.set('name', this.name);
		hash.set('description', this.description);
		hash.set('extendedDescription', this.extendedDescription);
		hash.set('productId', this.productId);
		hash.set('upc', this.upc);
		
		if (this.primaryGroup != null)
			hash.set('primaryGroup', this.primaryGroup);
		
		return hash.toJSON();
	},
	
	toJSON : function()
	{
		var divs = $$('div.move-left');
		for (var i=0; i<divs.length; i++)
		{
			var div = divs[i];
			var id = div.id;
			var fieldId = 'left.' + id;
			var field = this.productController.getField(fieldId);
			if (field != null)
				field.setValue(field.value);
		}
		
		var imageURLField = this.productController.getField('left.product.ImageURL');
		imageURLField.setValue(imageURLField.value);
		
		this.productController.initParameters();
		return this.productController.parametersMap.toJSON();
	}
});

var ProductWrapper = Class.create(Product, {
	initialize : function(product)
	{
		this.upc = product.getUpc();
		this.name = product.getName();
		this.description = product.getDescription();
		this.productId = product.getProductId();
		this.imageURL = product.getImageURL();
		this.type = product.getType();
		
		this.extendedDescription = product.extendedDescription;
		this.productController = product.productController;
		this._parentContainer = product._parentContainer;
		this._imageContainer = product._imageContainer;
		this._descriptionContainer = product._descriptionContainer;
		
		this.listener = product.getListener();
	}
});

var POSProduct = Class.create(ProductWrapper, {
	
	initialize : function($super, product)
	{
		if (product != null)
		{
			$super(product);
		}
	},
	
	equals : function(product)
	{
		return (this.getProductId() == product.getProductId());
	}
});

var ShopSavvyProduct = Class.create(ProductWrapper, {
	
	initialize : function($super, product)
	{
		if (product != null)
		{
			$super(product);
		}
	},

	equals : function(product)
	{
		return (this.getUpc() == product.getUpc());
	}
});

var MiloProduct = Class.create(ProductWrapper, {
	
	initialize : function($super, product)
	{
		if (product != null)
		{
			$super(product);
		}	
	},
	
	initFields : function(json)
	{
		this.productId = 0;
		
		this.name = json.name;
		this.description = json.description;		
		this.extendedDescription = json.extendedDescription;
		this.primaryGroup = json.primaryGroup;
		this.imageURL = json.imageURL;
		this.extendedDescriptionTrunc = (this.extendedDescription != null) ? 
				this.extendedDescription.substring(0, 100) + '...' : this.description;
	},	
	
	equals : function(product)
	{
		return (this.getName() == product.getName());
	}
});

var UPCLookupManager = Class.create({
	
	initialize : function()
	{
		this.posProducts = new ArrayList(new POSProduct());
		this.shopSavvyProducts = new ArrayList(new ShopSavvyProduct());
		this.miloProducts = new ArrayList(new MiloProduct());
		
		this.textLookupManager = new TextLookupManager(); 
		
		this._searchInput = null;
		this._searchBtn = null;
		this._result = null;
		
		this.itemSelected = null;
	},
	
	initializeComponents : function()
	{
		this._searchInput = $('add-product-textfield');
		this._searchBtn = $('add-product-button');
		this._result = $('add-product-result-list');
		
		if (this._result != null)
		{
			this._result.innerHTML = '';
		}
		
		this._searchBtn.onclick = this.search.bind(this);
		this._searchInput.onkeyup = this.wait.bind(this);
		this._searchInput.onclick = this.focus.bind(this);
	},
	
	focus : function()
	{
		this._searchInput.select();
	},
	
	wait : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN){
			this.search();
		}
	},
	
	search : function()
	{
		if (this._searchInput == null)	return;
		
		var upc = this._searchInput.value;
		
		if (upc == null || upc.trim().length == 0) return;
		
		var json = {'upc': upc, 'name': upc, 'description': upc};
		
		var product = new Product();
		product.initFields(json);
		
		if (!this.shopSavvyProducts.contains(product))
		{
			this.addProduct(product);
		}
	},
	
	addProduct : function(product)
	{
		product.initLoader(product.getUpc());
		
		var firstRow = this._result.firstDescendant();
		
		if (firstRow == null)
			this._result.appendChild(product.getComponent());
		else
		{
			Element.insert(firstRow, {before : product.getComponent()});
		}
		
		this.refreshScroll();
		
		product.setListener(this);
		product.initLookup(product.getUpc());
	},
	
	productLookedUp : function(product, json)
	{
		var controller = new ProductLeftLookupController(json);
		
		product.initController(controller);
		product.setType(json.type);
		product.setImageURL(json.imageURL);
		
		product.updateLoader();
		
		this.refreshScroll();
		
		var type = product.getType();
		if (type == 'pos')
		{
			var posProduct = new POSProduct(product);
			this.posProducts.add(posProduct);
		}
		else if (type == 'shopSavvy')
		{
			var shopSavvyProduct = new ShopSavvyProduct(product);
			this.shopSavvyProducts.add(shopSavvyProduct);
		}
		else if (type == 'milo')
		{
			var miloProduct = new MiloProduct(product);
			this.miloProducts.add(miloProduct);
		}
	},
	
	productSelected : function(product)
	{
		for (var i=0; i<this.posProducts.size(); i++)
		{
			var prod = this.posProducts.get(i);
			prod.removeHighlight();
		}
		
		for (var i=0; i<this.shopSavvyProducts.size(); i++)
		{
			var prod = this.shopSavvyProducts.get(i);
			prod.removeHighlight();
		}
		
		for (var i=0; i<this.miloProducts.size(); i++)
		{
			var prod = this.miloProducts.get(i);
			prod.removeHighlight();
		}
		
		product.highlight();
		
		this.textLookupManager._searchInput.value = product.getName();
		this.textLookupManager.setProductLeft(product);
		this.textLookupManager.search();
		
		this.itemSelected = product;
	},
	
	refreshScroll : function()
	{
		jQuery(document).ready(function(){

			jQuery('#add-product-pane').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		});
	},
	
	clear : function()
	{
		this.posProducts.clear();
		this.shopSavvyProducts.clear();
		this.miloProducts.clear();
		this._result.innerHTML = '';
		$('add-product-pane').removeAttribute('style');
	}
});

var TextLookupManager = Class.create(
{
	initialize : function()
	{
		this.posProducts = new ArrayList(new POSProduct());
		this.shopSavvyProducts = new ArrayList(new ShopSavvyProduct());
		this.miloProducts = new ArrayList(new MiloProduct());
		this.productLeft = null;
		
		this._searchInput = null;
		this._searchBtn = null;
		this._result = null;
		
		this.initializeComponents();
	},
	
	initializeComponents : function()
	{
		this._searchInput = $('compare-product-textfield');
		this._searchBtn = $('compare-product-button');
		this._result = $('compare-product-result-list');
		
		if (this._result != null)
		{
			this._result.innerHTML = '';
		}
		
		this._searchBtn.onclick = this.search.bind(this);
		this._searchInput.onkeyup = this.wait.bind(this);		
		this._searchInput.onclick = this.focus.bind(this);
	},
	
	focus : function()
	{
		this._searchInput.select();
	},
	
	wait : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN){
			this.search();
		}
	},
	
	search : function()
	{
		if (this._searchInput == null)	return;
		
		var text = this._searchInput.value;
		
		if (text == null || text.trim().length == 0) return;
		
		var json = {'upc': text, 'name': text, 'description': text};
		var product = new Product(json);
		
		var product = new Product();
		product.initFields(json);
		
		if (!this.miloProducts.contains(product))
		{
			this.addProduct(product);
		}
	},
	
	addProduct : function(product)
	{
		this.clear();
		product.initLoader(product.getName());
		this._result.appendChild(product.getComponent());
		
		this.refreshScroll();
		
		product.setListener(this);
		product.initLookup(product.getName());
	},
	
	productLookedUp : function(product, json)
	{
		this.clear();
		
		var products = json.products;
						
		var type = json.type;
		
		if (type == 'pos')
		{
			var posProduct = new POSProduct(product);
			this.posProducts.add(posProduct);
		}
		else if (type == 'shopSavvy')
		{
			var shopSavvyProduct = new ShopSavvyProduct(product);
			this.shopSavvyProducts.add(shopSavvyProduct);
		}
		else if (type == 'milo')
		{
			var div = document.createElement('div');
			
			for (var i=0; i<products.length; i++)
			{
				var miloJSON = products[i];
				div.innerHTML = miloJSON.description;
				var primaryGroup = miloJSON.category_name;
				
				var extendedDescription = div.textContent;
				
				primaryGroup = (primaryGroup == null) ? '' : primaryGroup;
				
				var json = {'name': miloJSON.name,
							'imageURL' : miloJSON.image_220,
							'description' : miloJSON.name,
							'extendedDescription' : extendedDescription,
							'type' : type,
							'primaryGroup' : primaryGroup};
				
				var prod = new Product();
				
				var miloProduct = new MiloProduct(prod);
				miloProduct.initFields(json);
				miloProduct.setListener(this);
				
				this._result.appendChild(miloProduct.getComponent());
				miloProduct.updateLoader();
				
				this.miloProducts.add(miloProduct);
			}
		}
		
		this.refreshScroll();
	},
	
	setProductLeft : function(product)
	{
		this.productLeft = product;
	},
	
	productSelected : function(product)
	{
		for (var i=0; i<this.posProducts.size(); i++)
		{
			var prod = this.posProducts.get(i);
			prod.removeHighlight();
		}
		
		for (var i=0; i<this.shopSavvyProducts.size(); i++)
		{
			var prod = this.shopSavvyProducts.get(i);
			prod.removeHighlight();
		}
		
		for (var i=0; i<this.miloProducts.size(); i++)
		{
			var prod = this.miloProducts.get(i);
			prod.removeHighlight();
		}
		
		product.highlight();
		
		var comparePanel = new ComparePanel(this.productLeft, product);
		comparePanel.show();
		
	},
	
	refreshScroll : function()
	{
		jQuery(document).ready(function(){

			jQuery('#compare-product-pane').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		});
	},
	
	clear : function()
	{
		this.posProducts.clear();
		this.shopSavvyProducts.clear();
		this.miloProducts.clear();
		this._result.innerHTML = '';
		$('compare-product-pane').removeAttribute('style');
	}
});

var ComparePanel = Class.create(PopUpBase, {
	
	initialize : function(leftProduct, rightProduct)
	{
		this.createPopUp($('product.lookup.popup.panel'));
		this.cancelBtn = $('product.lookup.popup.close.button');
		this.saveBtn = $('product.lookup.popup.saveBtn');
		
		this.cancelBtn.onclick = this.cancel.bind(this);
		this.saveBtn.onclick = this.save.bind(this);
		
		this.leftProduct = leftProduct;
		this.rightProduct = rightProduct;
	},
	
	onShow : function()
	{
		$('lookup.fields').innerHTML = LookupFields.fields;
		
		this.initFields();
	},
	
	initFields : function()
	{
		try
		{	
			var myAjax = new Ajax.Request('ProductLookupAction.do?action=getCompareProducts&productLeft='+
					escape(this.leftProduct.paramJSON())+'&productRight='+ escape(this.rightProduct.paramJSON()),
			{
				method: 'POST',
				onSuccess: this.displayFields.bind(this)				
			});
		}
		catch(e)
		{}
	},
	
	displayFields : function(response)
	{
		var json = eval('('+response.responseText+')');
		
		var right = json.right;
		
		var leftController = this.leftProduct.productController;
		leftController.init();
		
		var divs = $$('div.move-left');
		for (var i=0; i<divs.length; i++)
		{
			var div = divs[i];
			var id = div.id;
			var fieldId = 'left.' + id;
			var field = this.leftProduct.productController.getField(fieldId);
			if (field != null)
				field.setValue(field.value);
		}
		
		var rightController = new ProductRightLookupController(right);
		rightController.init();
		
		$('left.product.Image').src = this.leftProduct.imageURL;
		$('right.product.Image').src = this.rightProduct.imageURL;
		
		this.initCopyLeft();
	},
	
	initCopyLeft : function()
	{
		var divs = $$('div.move-left');
		for (var i=0; i<divs.length; i++)
		{
			var div = divs[i];
			div.onclick = this.copyLeft.bind(this, div.id); 
		}
	},
	
	copyLeft : function(id)
	{
		var element = $('left.' + id);				
		var type = element.tagName;
		
		var rightValue = $('right.' + id).innerHTML;
		
		if (type != 'SELECT')
			$('left.' + id).value = rightValue;
		
		if (type == 'IMG')
		{
			$('left.' + id).src = $('right.' + id).src;
			$('left.' + id + 'URL').value = $('left.' + id).src;
		}
		
		if (type == 'SELECT')
		{
			for (var i=0; i<element.length; i++)
			{
				var option = element.options[i];
				var label = option.label;
				if (rightValue == label)
				{
					option.selected = true;
				}
			}
		}		
	},
	
	save : function()
	{
		this.hide();
		this.leftProduct.name = $('right.product.Name').innerHTML;
		this.leftProduct.description = $('right.product.Description').innerHTML;
		this.leftProduct.imageURL = $('left.product.Image').src;
		
		
		this.leftProduct.updateLoader();
		
		var divs = $$('div.move-left');
		for (var i=0; i<divs.length; i++)
		{
			var div = divs[i];
			var id = div.id;
			var fieldId = 'left.' + id;
			var field = this.leftProduct.productController.getField(fieldId);
			if (field != null)
				field.setValue(field.getValue());
		}
		
		var imageURLField = this.leftProduct.productController.getField('left.product.ImageURL');
		imageURLField.setValue(imageURLField.getValue());
	},
	
	cancel : function()
	{
		this.hide();
		$('left.product.Image').src = $('left.product.ImageURL').value;
	}
});

var AdminPanel = Class.create({
	
	initialize : function(upcLookupManager)
	{
		this.upcLookupManager = upcLookupManager;
		this.textLookupManager = upcLookupManager.textLookupManager;
		
		this.clearBtn = $('clear-button');
		this.saveBtn = $('save-button');	
		this.productInfoBtn = $('product-info-button');
	},
	
	initBtns : function()
	{
		this.clearBtn.onclick = this.clear.bind(this);
		this.saveBtn.onclick = this.save.bind(this);
		this.productInfoBtn.onclick = this.showProductInfo.bind(this);
	},
	
	clear : function()
	{
		this.upcLookupManager.clear();
		this.textLookupManager.clear();
	},
	
	save : function()
	{
		var products = this.upcLookupManager.shopSavvyProducts;
		var posProducts = this.upcLookupManager.posProducts;
		
		for (var i=0; i<posProducts.size(); i++)
		{
			var posProduct = posProducts.get(i);
			products.add(posProduct);
		}
		
		var h = new Hash();
		h.set('products', eval('('+products.toJSON() +')'));
		$('product-lookup-form').products.value = h.toJSON();
		$('product-lookup-form').request({
			onComplete : function(){alert(Translation.translate('success','success'))}
		});
	},
	
	afterSave : function(){},
	
	showProductInfo : function()
	{
		var panel = new ProductInfoPanel();
		var itemSelected = this.upcLookupManager.itemSelected;
		
		panel.getInfo(itemSelected.getProductId());
	}
});

var LookupFields = 
{
		fields : null,
		
		init : function()
		{
			LookupFields.fields = $('lookup.fields').innerHTML;
		}
}
