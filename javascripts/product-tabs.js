var ProductPricesTab = Class.create(Tab, {
	initialize : function($super, tabId)
	{
		$super(tabId, 'PricePanel');
		this.soProductPrices = new ArrayList(new ProductPrice());
		this.poProductPrices = new ArrayList(new ProductPrice());
		
		this.currentSOPrices = new ArrayList(new ProductPrice());
		this.currentPOPrices = new ArrayList(new ProductPrice());
		
		this.isUpdateVariantPrice = false;
	},
	
	clear : function()
	{
		this.soProductPrices.clear();
		this.poProductPrices.clear();
		
		this.currentSOPrices.clear();
		this.currentPOPrices.clear();
	},
	
	afterCall : function(json)
	{
		this.clear();
		var soPrices = json.soPrices;
		var poPrices = json.poPrices;
		this.productParentId = json.productParentId;
		this.isParent = json.isParent;
		
		this.generalHelp = json['product.prices.general.help'];
		
		this.addProductPrices(soPrices, this.soProductPrices, this.currentSOPrices);
		this.addProductPrices(poPrices, this.poProductPrices, this.currentPOPrices);
		this.initTab();
	},
	
	getParameters : function()
	{
		var visible = $('other.pricelists').style.display == 'block';
		
		var soPrices = new ArrayList(new ProductPrice());
		var poPrices = new ArrayList(new ProductPrice());
		
		soPrices.addAll(this.currentSOPrices);
		poPrices.addAll(this.currentPOPrices);
		
		if (visible)
		{
			soPrices.addAll(this.soProductPrices);
			poPrices.addAll(this.poProductPrices);
		}
		
		var parameters = '{"soPrices":' + soPrices.toJSON();
		parameters += ', "poPrices":' + poPrices.toJSON();
		parameters += ', "isUpdateVariantPrice":' + this.isUpdateVariantPrice;
		parameters += '}';
		
		
		return parameters;
	},
	
	initTab : function()
	{
		this.flush();
		
		for (var i=0; i<this.soProductPrices.size(); i++)
		{
			var productPrice = this.soProductPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			var tdOriginal = document.createElement('td');
			var tdLimit = document.createElement('td');
			
			tdName.setAttribute('class', 'pricelistname-so');
			tdCurrent.setAttribute('class', 'pricelistbg');
			tdOriginal.setAttribute('class', 'pricelistbg');
			tdLimit.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			tdOriginal.appendChild(productPrice.getPriceListComp());
			tdLimit.appendChild(productPrice.getPriceLimitComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			tr.appendChild(tdOriginal);
			tr.appendChild(tdLimit);
			
			Element.insert($('product.prices.so.header'), {after:tr});
		}
		
		for (var i=0; i<this.currentSOPrices.size(); i++)
		{
			var productPrice = this.currentSOPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			var tdOriginal = document.createElement('td');
			var tdLimit = document.createElement('td');
			
			tdName.setAttribute('class', 'pricelistname-so');
			tdCurrent.setAttribute('class', 'pricelistbg');
			tdOriginal.setAttribute('class', 'pricelistbg');
			tdLimit.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			tdOriginal.appendChild(productPrice.getPriceListComp());
			tdLimit.appendChild(productPrice.getPriceLimitComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			tr.appendChild(tdOriginal);
			tr.appendChild(tdLimit);
			
			Element.insert($('product.prices.so.current.header'), {after:tr});
		}
		
		for (var i=0; i<this.poProductPrices.size(); i++)
		{
			var productPrice = this.poProductPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			
			tdName.setAttribute('class', 'pricelistname-po');
			tdCurrent.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			
			Element.insert($('product.prices.po.header'), {after:tr});
		}
		
		for (var i=0; i<this.currentPOPrices.size(); i++)
		{
			var productPrice = this.currentPOPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			
			tdName.setAttribute('class', 'pricelistname-po');
			tdCurrent.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			
			Element.insert($('product.prices.po.current.header'), {after:tr});
		}
	},
	
	flush : function()
	{
		var soTable = $('product.prices.so.table');
		var soHeader = $('product.prices.so.header');
		
		var soCurrentTable = $('product.prices.so.current.table');
		var soCurrentHeader = $('product.prices.so.current.header');
		
		soTable.innerHTML = '';
		soTable.appendChild(soHeader);
		
		soCurrentTable.innerHTML = '';
		soCurrentTable.appendChild(soCurrentHeader);
		
		var poTable = $('product.prices.po.table');
		var poHeader = $('product.prices.po.header');
		
		var poCurrentTable = $('product.prices.po.current.table');
		var poCurrentHeader = $('product.prices.po.current.header');
		
		poTable.innerHTML = '';
		poTable.appendChild(poHeader);
		
		poCurrentTable.innerHTML = '';
		poCurrentTable.appendChild(poCurrentHeader);
	},
	
	addProductPrices : function(jsonPrices, productPrices, currentProductPrices)
	{
		for (var i=0; i<jsonPrices.length; i++)
		{
			var json = jsonPrices[i];
			var productPrice = new ProductPrice(json);
			
			if (productPrice.isCurrentPrice())
				currentProductPrices.add(productPrice);
			else
				productPrices.add(productPrice);
		}
	},
	
	afterOk : function(json)
	{
		var soPrice = json.currentPriceSO;
		var poPrice = json.currentPricePO;
		
		$('product.summary.newPrice').innerHTML = soPrice.priceStd;
		$('product.summary.oldPrice').innerHTML = soPrice.priceList;
	},
	
	beforeOk : function()
	{
		if (this.productParentId <=0 && this.isParent)
		{
			var updateVariantPricePanel = new UpdateVariantsPricesPanel();
			updateVariantPricePanel.setTab(this);
			updateVariantPricePanel.show();
		}
	},
	
	onOk : function()
	{
		if (this.productParentId <=0 && this.isParent)
		{
			var updateVariantPricePanel = new UpdateVariantsPricesPanel();
			updateVariantPricePanel.setTab(this);
			updateVariantPricePanel.show();
		}
		else
		{
			this.ok();
		}
	},
	
	ok : function()
	{
		var url = 'ModelAction.do';
		var pars = 'action=popupOk&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.okHandler.bind(this)
		});	
	}
});

var ProductPrice = Class.create({
	
	initialize : function(json)
	{
		if (json!= null)
		{
			this.isTaxIncluded = json.isTaxIncluded;
			this.name = json.name;
			this.priceListId = json.priceListId;
			this.productId = json.productId;
			this.priceListVersionId = json.priceListVersionId;
			this.priceStd = json.priceStd;
			this.priceList = json.priceList;
			this.priceLimit = json.priceLimit;
			this.currentPrice = json.isCurrentPrice;
			
			this.initComponent();
		}
	},
	
	initComponent : function()
	{
		this.std_box = this.createBox(this.priceStd);
		this.list_box = this.createBox(this.priceList);
		this.limit_box = this.createBox(this.priceLimit);
		
		this.std_box.onchange = this.updatePriceStd.bind(this);
		this.list_box.onchange = this.updatePriceList.bind(this);
		this.limit_box.onchange = this.updatePriceLimit.bind(this);
	},
	
	isCurrentPrice : function()
	{
		return this.currentPrice;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getTaxIncluded : function()
	{
		return this.isTaxIncluded;
	},
	
	getPriceStdComp : function()
	{
		return this.std_box;
	},
	
	getPriceListComp : function()
	{
		return this.list_box;
	},
	
	getPriceLimitComp : function()
	{
		return this.limit_box;
	},
	
	updatePriceStd : function()
	{
		this.priceStd = this.std_box.value;
	},
	
	updatePriceList : function()
	{
		this.priceList = this.list_box.value;
	},
	
	updatePriceLimit : function()
	{
		this.priceLimit = this.limit_box.value;
	},
	
	createBox : function(value)
	{
		var box = document.createElement('input');
		box.setAttribute('type', 'text');
		box.setAttribute('value', value);
		box.setAttribute('class', 'pricelistinput');
		box.setAttribute('style', 'text-align:left');
		return box;
	}
});

var InventoryTab = Class.create(Tab, {
	initialize : function($super, tabId)
	{
		$super(tabId, 'StockPanel');
		this.warehouseList = new ArrayList(new Warehouse());
	},
	
	afterCall : function(json)
	{
		var warehouses = json.warehouses;
		
		this.generalHelp = json['product.inventory.general.help'];
		
		var stockHeader = $('stock.header');
		var stockTable = $('stock.table');
		stockTable.innerHTML = '';
		
		stockTable.appendChild(stockHeader);
		
		for (var i=0; i<warehouses.length; i++)
		{
			var div = document.createElement('div');
			div.setAttribute('class', 'row');
			var wh = warehouses[i];
			var name = wh.name;
			var id = wh.id;
			var qty = wh.qty;
			
			var warehouse = new Warehouse(name, id, qty);
			this.warehouseList.add(warehouse);
			
			var col1 = document.createElement('div');
			col1.setAttribute('class', 'col-md-6 col-xs-6');
			
			var label = document.createElement('label');
			label.innerHTML = name;
			col1.appendChild(label);
			
			var col2 = document.createElement('div');
			col2.setAttribute('class', 'col-md-6 col-xs-6 padding-top-4');
			col2.setAttribute('style', 'text-align:right');
			col2.appendChild(warehouse.getComponent());
			
			//div.appendChild(label);
			//div.appendChild(warehouse.getComponent());
			div.appendChild(col1);
			div.appendChild(col2);
			
			Element.insert('stock.header', {after : div});
		}
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error != null)
		{
			alert(error);
		}
		else
		{
			var stock = json.stock;
			var allStock = json.allStock;
			
			$('product.summary.stock').innerHTML = stock;
			$('product.summary.allStock').innerHTML = allStock;
		}
	},
	
	getParameters : function()
	{
		var d = new Date();
		var movementDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() 
							+ ' ' + d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
		
		var parameters = "{'warehouses' : " + this.warehouseList.toJSON() + ", 'movementDate' : '"+ movementDate+"'}";
		
		
		return parameters;
	}
});

var ProductPairingTab = Class.create(Tab, {
	
	initialize : function($super, tabId, product)
	{
		$super(tabId, 'ProductPairing');
		this.product = product;
		this.productSearch = null;
		this.productBOMGrid = null;
	},
		
	afterCall : function(json)
	{
		this.generalHelp = json['product.pairing.general.help'];
		
		this.productBOMGrid = new ProductBOMGrid(this.product, json.boms)
		this.productSearch = new ProductSearch(this.productBOMGrid);
		
		this.productBOMGrid.buildGrid();
		this.productSearch.initializeComponents();
	},
	
	afterOk : function(productPairing)
	{
		if (productPairing != null && productPairing.boms.length > 0)
		{
			var boms = productPairing.boms;
			var t = '<div class="row"><div class="col-xs-12 col-md-12"><div class="summary-block">Paired Item(s)</div></div></div><div class="row" style="height:90px;overflow:auto;"><table>';
			for (var i=0; i<boms.length; i++)
			{
				var bom = boms[i];
				var name = bom.name;
				var qty = bom.qty;
				
				t += '<tr><td>' + qty + 'x </td><td> ' + name + '</td></tr>';
			}
			
			t+= '</table></div>';
			
			$('product.pairing').innerHTML = t;
		}
	},
	
	/*onShow : function()
	{
		jQuery('#bom-grid').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	},*/
	
	getParameters : function()
	{
		var parameters = new Hash();
		
		var params = new ArrayList();
		
		var productBOMs =this.productBOMGrid.getProductBOMs();
		for (var i=0; i<productBOMs.size(); i++)
		{
			var productBOM = productBOMs.get(i);
			
			var h = new Hash();
			h.set('bomId', productBOM.getBOMId());
			h.set('productId', productBOM.getProductId());
			h.set('qty', productBOM.getQty());
			h.set('line', productBOM.getLine());
			
			params.add(h);
		}
		
		parameters.set('boms', params)
		return parameters.toJSON();
	}
});

var ProductSettingsTab = Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'ProductSettings');
		this._editPriceOnFly = $('EditPriceOnFly');
		this._editDescOnFly = $('EditDescriptionOnFly');
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['product.settings.general.help'];
		
		this._editDescOnFly.checked = json.editDescOnFly;
		this._editPriceOnFly.checked = json.editPriceOnFly;
	},
	
	afterOk : function(json)
	{
		var editDescOnFly = json.editDescOnFly;
		var editPriceOnFly = json.editPriceOnFly;
		var isModifier = json.isModifier;
		
		if($('product.summary.editPrice')){
			$('product.summary.editPrice').innerHTML = (editPriceOnFly) ? "Yes" : "No";
			$('product.summary.editDesc').innerHTML = (editDescOnFly) ? "Yes" : "No";
		}
		
	},
	
	getParameters : function()
	{
		var editDescOnFly = this._editDescOnFly.checked;
		var editPriceOnFly = this._editPriceOnFly.checked;
		
		var h = new Hash();
		h.set('editDescOnFly', editDescOnFly);
		h.set('editPriceOnFly', editPriceOnFly);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});

function toggleOtherPriceLists()
{
	var visible = $('other.pricelists').style.display == 'block';
	$('other.pricelists').style.display = (visible)? 'none' : 'block';
	$('show.hide').innerHTML = (visible) ? 'Show' : 'Hide';
}

var TaxCategoryTab = Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'TaxCategoryPanel');
		this.selectComp = null;
		this.hash = new Hash();
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['product.tax.general.help'];
		
		this.selectComp = document.createElement('select');
		this.selectComp.setAttribute('id', 'tax.category.select');
		
		var taxCategories = json.taxCategories;
		for (var t in taxCategories)
		{
			var taxCat = taxCategories[t];
			var name = taxCat.taxCatName;			
			var id = taxCat.taxCatId;
			var selected = taxCat.selected;			
			var taxes = taxCat.taxes;
			
			this.hash.set(id, taxes);
			
			var option = document.createElement('option');
			option.setAttribute('value', id);
			option.innerHTML = name;
			option.onclick = this.displayDetails.bind(this, option.value);
			if (selected)
			{
				option.setAttribute('selected', true);
				this.displayDetails(option.value);
			}
			
			this.selectComp.appendChild(option);
		}
		$('taxCategoryId').innerHTML = '';
		$('taxCategoryId').appendChild(this.selectComp);
	},
	
	displayDetails : function(id)
	{
		this.flush();
		var taxes = this.hash.get(id);
		for (var i=0; i<taxes.length; i++)
		{
			var tax = taxes[i];
			var orgName = tax.orgName == null? "" : tax.orgName;
			var taxRate = tax.taxRate == null ? "" : tax.taxRate;
			
			var tr = document.createElement('div');
			tr.setAttribute('class', 'row');
			var tdOrg = document.createElement('div');
			tdOrg.setAttribute('class', 'col-xs-6 col-md-6');
			var tdRate = document.createElement('div');
			tdRate.setAttribute('class', 'col-xs-6 col-md-6');
			tdRate.setAttribute('style', 'text-align:right');
			
			tdOrg.innerHTML = orgName;
			tdRate.innerHTML = taxRate;
			
			tr.appendChild(tdOrg);
			tr.appendChild(tdRate);
			
			Element.insert($('tax.category.header'), {after:tr});
		}
	},
	
	flush : function()
	{
		var table = $('tax.category.table');
		var header = $('tax.category.header');
		
		table.innerHTML = '';
		table.appendChild(header);
	},
	
	afterOk : function(json)
	{
		var taxCatId = json.taxCategoryId;
		
		var options = $('product.C_TaxCategory_ID').options;
		for (var i=0; i<options.length; i++)
		{
			var option = options[i];
			var value = parseFloat(option.value);
			option.removeAttribute('selected');
			if (value == taxCatId)
			{
				$('product.C_TaxCategory_ID').selectedIndex = i;
			}
		}
	},
	
	getParameters : function()
	{
		var taxCategoryId = this.selectComp.value;
		var parameters = "{'taxCategoryId':" + taxCategoryId + "}";
		return parameters;
	}
});



var ProductModifierTab = Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'ProductModifiers');
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['product.modifiers.general.help'];
		this.availableModifierGroupsPanel = $('availableModifierGroups');
		this.chosenModifierGroupsPanel = $('chosenModifierGroups');
		this.availableModifierGroupsInput = $('availableModifierGroupsInput');
		this.chosenModifierGroupsInput = $('chosenModifierGroupsInput');
		
		var chosenSet = {};
		
		this.chosenModifierGroupsIds = json.chosenModifierGroupsIds;
		for(var i=0; i<this.chosenModifierGroupsIds.length; i++){
			var id = this.chosenModifierGroupsIds[i];
			chosenSet[id + ''] = id;
		}
		
		this.availableModifierGroupsList = [];
		this.chosenModifierGroupsList = [];
		
		var modifierGroupList = json.modifierGroups;
		
		for(var i=0; i<modifierGroupList.length; i++){
			var id = modifierGroupList[i].groupId;
			var modifier = {"name" : modifierGroupList[i].groupName, "id" : id};
			
			if(chosenSet[id + '']){
				this.chosenModifierGroupsList.push(modifier);
			}
			else{
				this.availableModifierGroupsList.push(modifier);
			}
		}

		this.availableModifierGroupsSelect = document.createElement('select');
		this.availableModifierGroupsSelect.setAttribute('id', 'availableModifierGroupsSelect');
		this.availableModifierGroupsSelect.setAttribute('style', 'width:100%;height:300px;');
		this.availableModifierGroupsSelect.setAttribute('multiple', 'true');

		for(var i=0; i<this.availableModifierGroupsList.length; i++)
		{
			var option = document.createElement('option');
			option.setAttribute('value', this.availableModifierGroupsList[i].id);
			option.innerHTML = this.availableModifierGroupsList[i].name;
			this.availableModifierGroupsSelect.appendChild(option);
		}
		this.availableModifierGroupsPanel.innerHTML = '';
		this.availableModifierGroupsPanel.appendChild(this.availableModifierGroupsSelect);


		this.chosenModifierGroupsList = this.chosenModifierGroupsList;
		this.chosenModifierGroupsSelect = document.createElement('select');
		this.chosenModifierGroupsSelect.setAttribute('id', 'chosenModifierGroupsSelect');
		this.chosenModifierGroupsSelect.setAttribute('style', 'width:100%;height:300px;');
		this.chosenModifierGroupsSelect.setAttribute('multiple', 'true');

		for(var i=0; i<this.chosenModifierGroupsList.length; i++)
		{
			var option = document.createElement('option');
			option.setAttribute('value', this.chosenModifierGroupsList[i].id);
			option.innerHTML = this.chosenModifierGroupsList[i].name;
			this.chosenModifierGroupsSelect.appendChild(option);
		}
		this.chosenModifierGroupsPanel.innerHTML = '';
		this.chosenModifierGroupsPanel.appendChild(this.chosenModifierGroupsSelect);

		var transfer = new Transfer(this.availableModifierGroupsSelect.id, this.chosenModifierGroupsSelect.id, this.availableModifierGroupsInput.id, this.chosenModifierGroupsInput.id);
		transfer.init();
	    transfer.updateInputs();
	},
	
	afterOk : function(json)
	{
		
	},
	
	getParameters : function()
	{		
		var h = new Hash();	
		h.set('modifiers', this.chosenModifierGroupsInput.value);
		
		var parameters = h.toJSON();		
		return parameters;
	}
});