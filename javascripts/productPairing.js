var Product = Class.create({
	
	initialize : function(productSearch, productJSON)
	{
		this.productSearch = productSearch;
		
		if (productJSON != null)
		{
			this.productId = productJSON.m_product_id;
			this.name = productJSON.name;
			this.description = productJSON.description;
			
			this._component = null;
			
			this.build();
		}
	},
	
	build : function()
	{
		this._component = document.createElement('tr');
		this._component.style.cursor = 'pointer';
		
		var td = document.createElement('td');
		td.innerHTML = this.name + '<br>' + this.description;
		
		this._component.appendChild(td);
		
		this._component.onclick = this.onSelect.bind(this);		
	},
	
	onSelect : function()
	{
		this.productSearch.onSelect(this);
	},
	
	highlight : function()
	{
		this._component.addClassName('search-product-highlight');
	},
	
	removeHighlight : function()
	{
		this._component.removeClassName('search-product-highlight');
	},
	
	getComponent : function()
	{
		return this._component;
	},
	
	getProductId : function()
	{
		return this.productId;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getDescription : function()
	{
		return this.description;
	},
	
	equals : function(product)
	{
		return (this.productId == product.getProductId());
	}
});

var ProductSearch = Class.create({
	
	initialize : function(productBOMGrid)
	{
		this.productBOMGrid = productBOMGrid;
		this.isSOTrx = true;
		this.active = null;
		
		this._searchInput = null;
		this._searchBtn = null;
		
		this.products = new ArrayList(new Product());
	},
	
	initializeComponents : function()
	{
		this._searchInput = $('search-product-pairing-textfield');
		this._searchBtn = $('search-product-pairing-button');
		
		this._searchBtn.onclick = this.search.bind(this);
		this._searchInput.onkeyup = this.wait.bind(this);
		
		var resultDiv = $('searchResultContainer');
		if (resultDiv != null)
			resultDiv.innerHTML = '';
	},
	
	wait : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN){
			this.search();
		}
	},
	
	search : function()
	{
		if (this._searchInput == null)
			return;
		
		var param = new Hash();
		param.set('action', 'productPairingSearch');
		param.set('searchTerm', this._searchInput.value);
		param.set('isSoTrx', this.isSOTrx);
		param.set('productId', this.productBOMGrid.getParentProductId());
		
		new Ajax.Request('ProductAction.do', {
			method:'post',
			onSuccess: this.render.bind(this),
			parameters:param
		});
	},
	
	render : function(response)
	{
		var json = response.responseText.evalJSON(true);
		var container = $('searchResultContainer');
		container.innerHTML = '';
		
		var _searchResultTable = document.createElement('table');
		_searchResultTable.setAttribute('id', 'searchResultTable');
		_searchResultTable.setAttribute('border', 0);
		_searchResultTable.setAttribute('width', '100%');

		var products = json.products;
		
		if (!json.success)
		{
			var productSearchErrorPanel = new ProductSearchErrorPanel(this._searchInput.value);
			productSearchErrorPanel.show();
		}
		
		for (var i=0; i<products.length; i++)
		{
			var productJSON = products[i];
			var product = new Product(this, productJSON);
			this.products.add(product);
			
			var _productRow = product.getComponent();
			_productRow.setAttribute('class', (i%2==0)? "odd" : "even");
			
			_searchResultTable.appendChild(_productRow);
			
			if (products.length == 1)
			{
				this.onSelect(product);
			}
		}

		container.appendChild( _searchResultTable);
		
		jQuery(document).ready(function(){

			jQuery('#product-pairing-search-pane').jScrollPane({scrollbarWidth:30, scrollbarMargin:0});
		});
	},
	
	onSelect : function(product)
	{
		for (var i=0; i<this.products.size(); i++)
		{
			var prod = this.products.get(i);
			prod.removeHighlight();
		}
		
		product.highlight();
		this.checkCircular(product);
	},
	
	checkCircular : function(product)
	{
		var param = new Hash();
		param.set('action', 'checkCircular');
		param.set('productId', product.getProductId());
		param.set('parentProductId', this.productBOMGrid.getParentProductId());
		
		new Ajax.Request('ProductAction.do', {
			method:'post',
			onSuccess: this.addProduct.bind(this, product),
			parameters:param
		});
	},
	
	addProduct : function (product, response)
	{
		var json = eval('(' + response.responseText + ')');
		if (json.success)
			this.productBOMGrid.add(product);
		else
		{
			var parentProductName = this.productBOMGrid.product.fieldMap.get('product.Name').value;
			alert('Cannot add ' +product.name + ' because it has pairings which are linked to ' + parentProductName);
		}
	}
});

var ProductBOM = Class.create({
	
	initialize : function(productBOMGrid, json)
	{
		this.productBOMGrid = productBOMGrid;
		
		if (json != null)
		{
			this.bomId = json.bomId;
			this.productId = json.productId;
			this.name = json.name;
			this.qty = parseFloat(json.qty);
			this.line = parseFloat(json.line);
			
			this._component = null;
			
			this.initComponent();
		}
	},
	
	initComponent : function()
	{
		this._component = document.createElement('input');
		this._component.setAttribute('type', 'text');
		this._component.setAttribute('class', 'numeric qty-box');
		this._component.value = this.qty;
	},
	
	getComponent : function()
	{
		return this._component;
	},
	
	getBOMId : function()
	{
		return this.bomId;
	},
	
	setLine : function(line)
	{
		this.line = line;
	},
	
	getLine : function()
	{
		return this.line;
	},
	
	getProductId : function()
	{
		return this.productId;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	setQty : function(qty)
	{
		this.qty = qty;
		this._component.value = qty;
	},
	
	getQty : function()
	{
		var qty = parseFloat(this._component.value);
		return qty;
	},
	
	equals : function(productBOM)
	{
		return (this.productId == productBOM.getProductId());
	}
});


var ProductBOMGrid = Class.create({
	
	initialize : function(product, boms)
	{
		this.product = product;
		this.productBOMs = new ArrayList(new ProductBOM());
		this.boms = boms;
		this._grid = $('bom-grid');
	},
	
	getParentProductId : function()
	{
		return this.product.getId();
	},
	
	buildGrid : function()
	{
		for (var i=0; i<this.boms.length; i++)
		{
			var bom = this.boms[i];
			var productBOM = new ProductBOM(this, bom);
			this.productBOMs.add(productBOM);
		}
		
		this.updateGrid();
	},
	
	updateGrid : function()
	{
		var table = document.createElement('table');
		table.setAttribute('cellspacing', 0);
		table.setAttribute('style', 'width:100%;');
		
		for (var i=0; i<this.productBOMs.size(); i++)
		{
			var productBOM = this.productBOMs.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdComp = document.createElement('td');
			
			tr.setAttribute('class', (i%2==0)? "odd" : "even");
			
			tdName.innerHTML = productBOM.getName();
			tdComp.appendChild(productBOM.getComponent());
			
			tr.appendChild(tdComp);
			tr.appendChild(tdName);
			
			table.appendChild(tr);
		}
		
		this._grid.innerHTML = '';
		this._grid.appendChild(table);
	},
	
	add : function(product)
	{
		var bom = new Hash();
		bom.set('bomId', 0);
		bom.set('productId', product.getProductId());
		bom.set('name', product.getName());
		bom.set('qty', 1);
		bom.set('line', 0);
		
		var productBOM = new ProductBOM(this, eval('('+bom.toJSON()+')'));
		
		if (this.productBOMs.contains(productBOM))
		{
			var index = this.productBOMs.indexOf(productBOM);
			var bom = this.productBOMs.get(index);
			var qty = bom.getQty();
			
			qty += 1;
			
			bom.setQty(qty);
		}
		else
		{
			var line = 0;
			if (this.productBOMs.size() != 0)
			{
				var lastIndex = this.productBOMs.size() - 1;
				var last = this.productBOMs.get(lastIndex);
				line = last.getLine();
			}
			
			line = line += 10;
			productBOM.setLine(line);
			
			this.productBOMs.add(productBOM);
		}
		
		this.updateGrid();
		
		jQuery('#bom-grid').jScrollPane({scrollbarWidth:30, scrollbarMargin:0});
	},
	
	getProductBOMs : function()
	{
		return this.productBOMs;
	}
});