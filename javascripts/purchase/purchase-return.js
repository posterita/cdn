var app = angular.module("app",['ui.bootstrap']);

app.controller("PurchaseController", function($scope, $http, $timeout, user_id, draft){
	var ctrl = this;

	ctrl.showModal = false;			
	ctrl.user_id = user_id;			
	ctrl.draft = draft;

	ctrl.purchase_id = 0;
	ctrl.boxNumber = null;
	ctrl.suppliers = [];
	ctrl.warehouseList = [];
	ctrl.supplierInfo = null;
	ctrl.updateprice = false;

	// load draft from cache
	// draft is saved for import csv for example
	if(ctrl.draft == null && sessionStorage.purchaseReturnDraft){
		ctrl.draft = JSON.parse(sessionStorage.getItem("purchaseReturnDraft"));
	}

	// set draft info if any
	if(ctrl.draft != null){

		ctrl.purchase_id = ctrl.draft['purchase_id'];
		ctrl.referenceno = ctrl.draft['referenceno'];
		ctrl.paymentrule = ctrl.draft['paymentrule'];

		var promisedate = moment(ctrl.draft['promisedate'],'DD-MM-YYYY');

		ctrl.promisedate = promisedate.toDate();

		// ctrl.purchase_id = ctrl.draft['purchase_id'];
		// ctrl.purchase_id = ctrl.draft['purchase_id'];
	}

	// load
	$http.get('DataTableAction.do?action=getWarehousesList').then(function(response){

		ctrl.warehouseList = response.data;	

		if(ctrl.draft != null){
			var id = ctrl.draft['warehouse_id'];

			var warehouse;

			for(var i=0; i<ctrl.warehouseList.length; i++){
				warehouse = ctrl.warehouseList[i];

				if(warehouse.value == id){

					ctrl.warehouse = warehouse;
					break;

				}
			}
		}

	}, function(error){

	});

	$http.get('DataTableAction.do?action=getVendorList').then(function(response){

		ctrl.suppliers = response.data.vendor;	

		if(ctrl.draft != null){
			var id = ctrl.draft['supplier_id'];

			var supplier;

			for(var i=0; i<ctrl.suppliers.length; i++){
				supplier = ctrl.suppliers[i];

				if(supplier.value == id){

					ctrl.supplier = supplier;
					ctrl.updateSupplier();

					break;

				}
			}
		}

	}, function(error){

	});

	ctrl.updateSupplier = function(){

		var supplier_id = ctrl.supplier.value;

		$http.get('BPartnerAction.do?action=reloadPartner&id=' + supplier_id ).then(function(response){

			ctrl.supplierInfo = response.data;					

		}, function(error){

		});
	}


	ctrl.showHeader = true;

	var input = document.getElementById("search-textfield");

	ctrl.clearList = function(){

	};

	ctrl.getAddress = function(model) {

		if(model == null) return "";

		var fields = [
			"address1",
			"address2",
			"address3",
			"address4",
			"city",
			"postal",
			"phone1",
			"phone2"
			];

		var address = "";

		for (var i = 0; i < fields.length; i++) {

			var value = model[fields[i]] || '';

			if (value == null || value.length == 0) continue;

			if (address.length > 0) {
				address += ", ";
			}

			address += value;
		}

		return address;
	};

	ctrl.opened1 = false;
	ctrl.open1 = function(e) {
		e.stopPropagation ();
		this.opened1 = true;
	};

	ctrl.searchProduct = function(searchTerm){

		return $http.get('PurchaseAction.do', {
			params: {
				'action': 'searchProduct',
				'searchTerm': searchTerm
			}
		}).then(function(response){
			return response.data;
		});

	};

	ctrl.addProduct = function( product ){
		if(product == null) return;

		ctrl.showModal = true;

		console.info("Adding product .. " + product.name);

		$http.get('ShoppingCartAction.do?action=addToCart&format=json&orderType=PurchaseReturn&qty=1&productId=' + product.m_product_id).then(function(response){

			ctrl.shoppingCart = response.data;	

			ctrl.showModal = false;

		}, function(error){

			ctrl.showModal = false;
		});
	};

	ctrl.createPurchase = function( complete ){

		// validate form
		if(!ctrl.validate()){
			return;
		}

		var post = {};
		post.supplier = ctrl.supplier.value;				
		post.referenceno = ctrl.referenceno;
		post.paymentrule = ctrl.paymentrule;
		// post.invoicerule = 'D'; //after delivery
		// post.deliveryrule = 'M'; //manual
		post.promisedate = moment( ctrl.promisedate ).format('DD-MM-YYYY HH:mm:ss');
		post.dateordered = moment().format('YYYY-MM-DD HH:mm:ss');
		post.docaction = complete ? "CO" : "DR";
		post.discount = 0;
		post.warehouse_id = ctrl.warehouse.value;
		post.purchase_id = ctrl.shoppingCart.c_order_id;
		post.pricelist_id = ctrl.shoppingCart.m_pricelist_id;				
		post.user_id = ctrl.user_id;
		post.ordertype = ctrl.shoppingCart.order_type;

		post.updateprice = false;

		/*
		 * if( complete && ctrl.updateprice ){
		 * 
		 * if(window.confirm("The unit cost price of the items are different
		 * from the purchase price list. Do you want to update the prices on the
		 * price list?")){ post.updateprice = true; } }
		 */

		var lines = [];
		var line;

		for(var i=0; i< ctrl.shoppingCart.lines.length; i++){

			line = ctrl.shoppingCart.lines[i];

			lines.push({

				'description' : line.description,
				'boxNo' : line.box,
				'm_product_id' : line.m_product_id,
				'c_tax_id' : line.c_tax_id,
				'c_uom_id' : line.c_uom_id,
				'pricelist' : line.price_list,
				'pricestd' : line.price_std,
				'priceentered' : line.price_entered,
				'qty' : line.qty

			});
		}

		post.lines = lines;

		var data = JSON.stringify( post );
		$("#post_data").val( data );

		ctrl.showModal = true;

		sessionStorage.removeItem("purchaseReturnDraft")

		$("#purchase_form").submit();				

	};

	ctrl.validate = function(){

		// supplier
		if( StringUtils.isEmpty( ctrl.warehouse ) ){
			alert("Select a warehouse for delivery");					
			$("#purchase_warehouse").focus();					
			return false;
		}


		// reference
		if( StringUtils.isEmpty( ctrl.referenceno ) ){
			alert("Enter a reference number");					
			$("#purchase_referenceno").focus();					
			return false;
		}


		// payment rule
		if( StringUtils.isEmpty( ctrl.paymentrule ) ){
			alert("Select a payment type");
			$("#purchase_paymentrule").focus();					
			return false;
		}

		// promisedate
		if( StringUtils.isEmpty( ctrl.promisedate ) ){
			alert("Select a promise date");
			$("#purchase_promisedate").focus();					
			return false;
		}

		// lines
		if( ctrl.shoppingCart.lines.length == 0 ){
			alert("No line found! Add some products");
			$("#search-textfield").focus();					
			return false;
		}

		return true;
	};

	/* shopping cart related */
	ctrl.getShoppingCart = function(){

		ctrl.showModal = true;

		$http.get('ShoppingCartAction.do?action=getCart&orderType=PurchaseReturn&format=json').then(function(response){

			ctrl.shoppingCart = response.data;	

			ctrl.showModal = false;

		}, function(error){

			ctrl.showModal = false;
		});
	};

	/* others */
	ctrl.setBoxNo = function(){

		var boxNo = window.prompt("Enter Box/Bag No");

		if(boxNo != null){

			ctrl.showModal = true;

			ctrl.boxNumber = boxNo;

			$http.get('ShoppingCartAction.do?action=setCurrentBoxNumber&orderType=PurchaseReturn&format=json&currentBoxNumber=' + boxNo).then(function(response){

				ctrl.shoppingCart = response.data;	

				ctrl.showModal = false;

			}, function(error){

				ctrl.showModal = false;
			});

		}

	};

	ctrl.updateQty = function(line, index){

		/* compare qty */
		var previousQty = line.qty;
		var newQty = document.getElementById('qty-' + index).value;

		var lineId = line.line_id;

		if(previousQty == newQty) return; /* take no action */

		ctrl.showModal = true;

		$http.get('ShoppingCartAction.do?action=updateQty&orderType=PurchaseReturn&format=json&lineId=' + lineId + '&qty=' + newQty).then(function(response){

			ctrl.shoppingCart = response.data;

			ctrl.showModal = false;

		}, function(error){

			ctrl.showModal = false;

		});

	};

	ctrl.updatePrice = function(line, index){

		/* compare price */
		var previousPrice = line.price_entered;
		var newPrice = document.getElementById('price-' + index).value;

		var lineId = line.line_id;

		if(previousPrice == newPrice) return; /* take no action */

		ctrl.showModal = true;

		$http.get('ShoppingCartAction.do?action=updatePrice&orderType=PurchaseReturn&format=json&lineId=' + lineId + '&price=' + newPrice).then(function(response){

			ctrl.shoppingCart = response.data;
			ctrl.updateprice = true;

			ctrl.showModal = false;

		}, function(error){

			ctrl.showModal = false;

		});

	};

	ctrl.removeFromCart = function(line){

		var lineId = line.line_id;

		ctrl.showModal = true;

		$http.get('ShoppingCartAction.do?action=removeFromCart&orderType=PurchaseReturn&format=json&lineId=' + lineId).then(function(response){

			ctrl.shoppingCart = response.data;	

			ctrl.showModal = false;

		}, function(error){

			ctrl.showModal = false;
		});

	};

	ctrl.clearList = function(){

		if(window.confirm("Do you want to clear all the lines?"))
		{
			ctrl.showModal = true;

			$http.get('ShoppingCartAction.do?action=clearCart&orderType=PurchaseReturn&format=json').then(function(response){

				ctrl.shoppingCart = response.data;	
				ctrl.updateprice = false;

				ctrl.showModal = false;

			}, function(error){

				ctrl.showModal = false;

			});
		}

	};

	ctrl.cancel = function(){
		window.location = "backoffice-menus.do#pmenuId=10002150";
	};

	ctrl.importcsv = function(){

		// save state before page switch
		var draft = {};

		draft.purchase_id = ctrl.purchase_id
		draft.supplier_id = ctrl.supplier.value;				
		draft.referenceno = ctrl.referenceno;
		draft.paymentrule = ctrl.paymentrule;
		draft.promisedate = moment( ctrl.promisedate ).format('DD-MM-YYYY');
		draft.warehouse_id = ctrl.warehouse.value;

		sessionStorage.setItem("purchaseReturnDraft", JSON.stringify(draft));

		window.location = "importer.do?importer=OrderLineImporter&orderType=PurchaseReturn";
	}

	// load shopping cart first time
	ctrl.getShoppingCart();

});