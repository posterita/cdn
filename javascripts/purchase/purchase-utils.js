var filters = ["groupBy", "orderBy"];

for (var i = 0; i < filters.length; i++) {

    app.filter(filters[i], ["$parse", "$filter", function($parse, $filter) {
        return function(array, groupByField) {
            var result = [];
            var prev_item = null;
            var groupKey = false;
            var filteredData = array;
            /*$filter('orderBy')(array,groupByField);*/

            if (filteredData != null)
                for (var i = 0; i < filteredData.length; i++) {
                    groupKey = false;
                    if (prev_item !== null) {
                        if (prev_item[groupByField] !== filteredData[i][groupByField]) {
                            groupKey = true;
                        }
                    } else {
                        groupKey = true;
                    }
                    if (groupKey) {
                        filteredData[i]['group_by_key'] = true;
                    } else {
                        filteredData[i]['group_by_key'] = false;
                    }
                    result.push(filteredData[i]);
                    prev_item = filteredData[i];
                }
            return result;
        }
    }]);

}

angular.module('app').directive('selectOnFocus', function() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.on("focus", function() {
                element.select();
            });
        }
    };
});

angular.module('app').directive('focusMe', function($timeout) {
    return {
        link: function(scope, element, attr) {
            attr.$observe('focusMe', function(value) {
                if (value === "true") {
                    $timeout(function() {
                        element[0].focus();
                    });
                }
            });
        }
    };
});

angular.module('app').directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

/* utils */
var StringUtils = {
    isEmpty: function(str) {
        return str == null || str.length == 0;
    }
};

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}