var app = angular.module("app",['ui.bootstrap']);
app.controller("PurchaseController", function($scope, $http, $timeout, $modal,
		purchase_pricelist_id, is_discount_percentage_applied, show_price, user_id, 
		allow_update_price, draft, enable_batch_and_expiry, reload_partner, domain){
	
	var ctrl = this;

	ctrl.showModal = false;
	ctrl.showConfirmModal = false;

	ctrl.user_id = user_id;
	ctrl.allow_update_price = allow_update_price;
	ctrl.enable_batch_and_expiry = enable_batch_and_expiry
	ctrl.draft = draft;
	ctrl.reload_partner = reload_partner;
	ctrl.domain = domain;

	ctrl.purchase_id = 0;
	ctrl.boxNumber = null;
	ctrl.suppliers = [];
	ctrl.warehouseList = [];
	ctrl.taxList = [];
	ctrl.supplierInfo = null;
	ctrl.priceDifferent = false;
	ctrl.supplierPurchasePrice = {};
	ctrl.productNotOnPricelist = false;
	ctrl.forceResetLine = false;
	ctrl.discountAmt = 0;
	ctrl.previousListPrice = null;
	ctrl.isDiscountApplied = false;
	ctrl.newListPrice = null;
	ctrl.listPriceChanged = false;
	ctrl.boxSellingPrice = null;
	//ctrl.updateTax = false;
	ctrl.linesToDelete = [];
	ctrl.pricePrecision = 2;
	ctrl.linesToSplit = [];
	
	/*show hide columns*/
	ctrl.showBarcode = true;
	ctrl.showName = false;
	ctrl.showNoOfPacks = false;
	ctrl.showCurrentPrice = false;
	ctrl.showQtyOnHand = false;
	ctrl.showProfitMargin = false;
	ctrl.showBoxCurrentPrice = false;	
	
	
	ctrl.deliverydate = moment(new Date).format();
	ctrl.promisedate = moment(new Date).format();

	//load draft from cache
	//draft is saved for import csv for example
	if(ctrl.draft == null && sessionStorage.purchaseDraft){
		ctrl.draft = JSON.parse(sessionStorage.getItem("purchaseDraft"));
	}

	//set draft info if any
	if(ctrl.draft != null){

		ctrl.purchase_id = ctrl.draft['purchase_id'];
		ctrl.referenceno = ctrl.draft['referenceno'];
		ctrl.paymentrule = ctrl.draft['paymentrule'];
		ctrl.agent = ctrl.draft['agent'];
		ctrl.supplierPurchasePrice.value = ctrl.draft['pricelist_id'];
		ctrl.supplierPurchasePriceList = ctrl.draft['supplierPurchasePriceList'];

		var promisedate = moment(ctrl.draft['promisedate'],'DD-MM-YYYY');

		ctrl.promisedate = promisedate.toDate();

		var deliverydate = moment(ctrl.draft['deliverydate'],'DD-MM-YYYY');

		ctrl.deliverydate = deliverydate.toDate();
		ctrl.pricePrecision = ctrl.draft['priceprecision'];

		//ctrl.purchase_id = ctrl.draft['purchase_id'];
		//ctrl.purchase_id = ctrl.draft['purchase_id'];
	}

	//load 
	$http.get('DataTableAction.do?action=getRoleAccessWarehousesList').then(function(response){

		ctrl.warehouseList = response.data;	

		if(ctrl.draft != null){
			var id = ctrl.draft['warehouse_id'];

			var warehouse;

			for(var i=0; i<ctrl.warehouseList.length; i++){
				warehouse = ctrl.warehouseList[i];

				if(warehouse.value == id){

					ctrl.warehouse = warehouse;
					break;

				}
			}
		}

	}, function(error){

	});

	$http.get('DataTableAction.do?action=getVendorList').then(function(response){

		ctrl.suppliers = response.data.vendor;	

		if(ctrl.draft != null){
			var id = ctrl.draft['supplier_id'];

			var supplier;

			for(var i=0; i<ctrl.suppliers.length; i++){
				supplier = ctrl.suppliers[i];

				if(supplier.value == id){

					ctrl.supplier = supplier;
					ctrl.updateSupplier();

					break;

				}
			}
		}

	}, function(error){

	});

	$http.get('DataTableAction.do?action=getTaxList').then(function(response){

		ctrl.taxList = response.data.tax;					

	}, function(error){

	});

	ctrl.setDraftedPricelist = function(){

		if(ctrl.draft != null){
			var id = ctrl.draft['pricelist_id'];

			var pricelist;

			for(var i=0; i<ctrl.supplierPurchasePriceList.length; i++){
				pricelist = ctrl.supplierPurchasePriceList[i];

				if(pricelist.value == id){

					ctrl.supplierPurchasePrice = pricelist;
					break;

				}
			}
		}
	}

	ctrl.getPurchasePriceList = function( supplier_id ){	

		ctrl.supplierPurchasePriceList = [];

		$http.get('DataTableAction.do?action=getSupplierPurchasePriceList&c_bpartner_id=' + supplier_id).then(function(response){					

			var priceList = response.data.supplierPurchasePriceList;

			if( priceList.hasOwnProperty('name') )//if supplier has a pricelist assigned on him
			{
				ctrl.supplierPurchasePriceList.push( priceList );
				ctrl.supplierPurchasePrice = priceList;
				
				ctrl.updatePurchasePricelist();

				ctrl.setDraftedPricelist();
			}
			else//if supplier has no pricelist assigned on him. then get all purchase pricelists
			{
				$http.get('DataTableAction.do?action=getPurchasePriceList').then(function(response){

					priceList = response.data.purchasePrice;
					
					ctrl.supplierPurchasePriceList = priceList;
					
					//
					ctrl.supplierPurchasePrice = {};
					
					if(priceList.length == 1){
						
						ctrl.supplierPurchasePrice = priceList[0];
						
						ctrl.updatePurchasePricelist();
					}
					//

					ctrl.setDraftedPricelist();

				}, function(error){

				});
			}

		}, function(error){

		});							
	}

	ctrl.updateSupplier = function(){

		var supplier_id = ctrl.supplier.value;
		
		if(ctrl.reload_partner != null){
			
			if(!ctrl.reload_partner){
				
				reload_partner = true;
				return;
			}
		}
		
		$http.get('BPartnerAction.do?action=reloadSupplier&isSoTrx=false&id=' + supplier_id ).then(function(response){

			ctrl.supplierInfo = response.data;	
			ctrl.getPurchasePriceList( supplier_id );

		}, function(error){

		});		
		
	}			

	ctrl.showHeader = true;

	var input = document.getElementById("search-textfield");

	ctrl.clearList = function(){

	};

	ctrl.getAddress = function(model) {

		if(model == null) return "";

		var fields = [
			"address1",
			"address2",
			"address3",
			"address4",
			"city",
			"postal",
			"phone1",
			"phone2"
			];

		var address = "";

		for (var i = 0; i < fields.length; i++) {

			var value = model[fields[i]] || '';

			if (value == null || value.length == 0) continue;

			if (address.length > 0) {
				address += ", ";
			}

			address += value;
		}

		return address;
	};

	ctrl.opened1 = false;
	ctrl.open1 = function(e) {
		e.stopPropagation ();
		this.opened1 = true;
	};

	ctrl.opened2 = false;
	ctrl.open2 = function(e) {
		e.stopPropagation ();
		this.opened2 = true;
	};

	ctrl.copyDeliveryDate = function(date){				

		ctrl.promisedate = angular.copy(date);				
	}

	ctrl.searchProduct = function(searchTerm){

		return $http.get('PurchaseAction.do', {
			params: {
				'action': 'searchProduct',
				'searchTerm': searchTerm
			}
		}).then(function(response){
			return response.data;
		});

	};
	
	ctrl.addProduct = function( product ){
		if(product == null) return;
		
		if(ctrl.supplierPurchasePrice.value == null){
			
			alert('Select Pricelist');
			return;
		}				
		
		ctrl.showModal = true;
		
		console.info("Adding product .. " + product.name);
		
		if(product.isbatchandexpiry == 'Y'){
			
			//last lot no/expiry to be displayed first
			$http.get('PurchaseAction.do?action=getLastLotIdForProduct&orderType=Purchase&format=json&m_product_id=' + product.m_product_id,{
				
			}).success(function(id){  
				
				var lotId = id;
		              
				$http.get('ShoppingCartAction.do?action=addToCart&format=json&orderType=Purchase&qty=1&attributesetInstanceId=' + lotId + '&productId=' + product.m_product_id).then(function(response){
					
					if(response.data.error)
					{													
						if(window.confirm( response.data.error + " \nDo you want to add product on pricelist?") ){
							
							ctrl.showModal = false;
							
							ctrl.productNameNotOnPricelist = response.data.product_name_not_on_pricelist;
							ctrl.addProductToPricelistModal = true;	
							
							ctrl.add = function(){
								
								ctrl.addProductToPricelist( response, false );
								ctrl.productNotOnPricelist = false;
							}
							
							ctrl.close = function(){
								
								ctrl.addProductToPricelistModal = false;
								ctrl.setPreviousPricelist( true );
								ctrl.productNotOnPricelist = true;
								ctrl.reset();
							}							
						}
						else{
							//set pricelist to previous pricelist
												
							ctrl.setPreviousPricelist( true );
							ctrl.productNotOnPricelist = true;							
						}
						
					}
					else
					{
						ctrl.shoppingCart = response.data;
						ctrl.productNotOnPricelist = false;
					}										
					
					ctrl.showModal = false;
					
				}, function(error){
					
					ctrl.showModal = false;
				});				
			
			});					
		}
		
		else{
			
			$http.get('ShoppingCartAction.do?action=addToCart&format=json&orderType=Purchase&qty=1&attributesetInstanceId=0&productId=' + product.m_product_id).then(function(response){
				
				if(response.data.error)
				{													
					if(window.confirm( response.data.error + " \nDo you want to add product on pricelist?") ){
						
						ctrl.showModal = false;
						
						ctrl.productNameNotOnPricelist = response.data.product_name_not_on_pricelist;
						ctrl.addProductToPricelistModal = true;	
						
						ctrl.add = function(){
							
							ctrl.addProductToPricelist( response, false );
							ctrl.productNotOnPricelist = false;
						}
						
						ctrl.close = function(){
							
							ctrl.addProductToPricelistModal = false;
							ctrl.setPreviousPricelist( true );
							ctrl.productNotOnPricelist = true;
							ctrl.reset();
						}							
					}
					else{
						//set pricelist to previous pricelist
						
						/*reset previous line if product is not added to pricelist !!*/	
						
						ctrl.setPreviousPricelist( true );
						//ctrl.setPreviousPricelistSynchronously(true);
						ctrl.productNotOnPricelist = true;	
						
					}
					
				}
				else
				{
					ctrl.shoppingCart = response.data;
					ctrl.productNotOnPricelist = false;
				}										
				
				ctrl.showModal = false;
				
			}, function(error){
				
				ctrl.showModal = false;
			});					
		}
		
	};
	
	/*ctrl.addProduct = function( product ){
		if(product == null) return;

		if(ctrl.supplierPurchasePrice.value == null){

			alert('Select Pricelist');
			return;
		}				

		ctrl.showModal = true;

		console.info("Adding product .. " + product.name);

		$http.get('ShoppingCartAction.do?action=addToCart&format=json&orderType=Purchase&qty=1&attributesetInstanceId=0&productId=' + product.m_product_id).then(function(response){

			if(response.data.error)
			{													
				if(window.confirm( response.data.error + " \nDo you want to add product on pricelist?") ){

					ctrl.showModal = false;

					ctrl.productNameNotOnPricelist = response.data.product_name_not_on_pricelist;
					ctrl.addProductToPricelistModal = true;	

					ctrl.add = function(){

						ctrl.addProductToPricelist( response, false );
						ctrl.productNotOnPricelist = false;
					}

					ctrl.close = function(){

						ctrl.addProductToPricelistModal = false;
						ctrl.setPreviousPricelist( true );
						ctrl.productNotOnPricelist = true;
						ctrl.reset();
					}							
				}
				else{
					//set pricelist to previous pricelist

					ctrl.setPreviousPricelist( true );
					ctrl.productNotOnPricelist = true;							
				}

			}
			else
			{
				ctrl.shoppingCart = response.data;
				ctrl.productNotOnPricelist = false;
			}										

			ctrl.showModal = false;

		}, function(error){

			ctrl.showModal = false;
		});
	};*/

	ctrl.addProductToPricelist = function( response, addToCart ){

		ctrl.productNameNotOnPricelist = response.data.product_name_not_on_pricelist;

		var post = {};

		post["pricestd"] = ctrl.currentPrice;
		post["pricelimit"] = ctrl.currentPrice;
		post["pricelist"] = ctrl.currentPrice;
		post["m_pricelist_version_id"] = response.data.m_pricelist_version_id;
		post["m_product_id"] = response.data.m_product_id;

		var data = $.param({
			json: JSON.stringify(post)
		});

		var url = "DataTableAction.do?action=saveProductOnPricelist";

		var config = {
				headers : {
					'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
				}
		};

		$http.post(url, data, config).success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available

			if(data['success']){
				//alert('Successfully saved!');				     			        	
				ctrl.addProductToPricelistModal = false;

				ctrl.addProduct( response.data );

				ctrl.forceResetLine = true;     			        	
				ctrl.updatePurchasePricelist();
				ctrl.reset();
				ctrl.productNotOnPricelist = false;
			}
			else
			{
				alert('Failed to save! ' + data['error']);
			}        

		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.

			alert('Failed to save! ' + status);

		});								
	}

	ctrl.reset = function(){

		ctrl.currentPrice = null;
		ctrl.minimumPrice = null;
		ctrl.originalPrice = null;
	}

	ctrl.createPurchase = function( complete ){

		//validate form
		if(!ctrl.validate()){
			return;
		}

		if(ctrl.isDiscountApplied){
			
			ctrl.productNotOnPricelist = false;

			ctrl.checkout(complete);
			return;
		}

		ctrl.updatePurchasePricelist().then(function(){

			ctrl.checkout(complete);					
		});					
	};			

	ctrl.checkout = function( complete ){

		if(ctrl.productNotOnPricelist)
		{
			return;
		}

		//lines
		if( ctrl.shoppingCart.lines.length == 0 ){
			alert("No line found! Add some products");
			$("#search-textfield").focus();					
			return false;
		}

		var post = {};
		post.supplier = ctrl.supplier.value;				
		post.referenceno = ctrl.referenceno;
		post.paymentrule = ctrl.paymentrule;
		post.invoicerule = 'D'; //after delivery
		post.deliveryrule = 'M'; //manual
		post.promisedate = moment( ctrl.promisedate ).format('DD-MM-YYYY HH:mm:ss');
		post.deliverydate = moment( ctrl.deliverydate ).format('DD-MM-YYYY HH:mm:ss');
		post.dateordered = moment().format('YYYY-MM-DD HH:mm:ss');
		post.docaction = complete ? "CO" : "DR";
		post.discount = 0;
		post.warehouse_id = ctrl.warehouse.value;
		post.purchase_id = ctrl.shoppingCart.c_order_id;
		post.pricelist_id = ctrl.supplierPurchasePrice.value;			
		post.user_id = ctrl.user_id;
		post.ordertype = ctrl.shoppingCart.order_type;
		post.agent = ctrl.agent || '';

		var lines = [];
		var line;

		for(var i=0; i< ctrl.shoppingCart.lines.length; i++){

			line = ctrl.shoppingCart.lines[i];

			line.updateprice = false;

			if(line.qty < 0){
				alert("Qty cannot be negative!");
				document.getElementById("qty-" + i).select();
				return;
			}
			
			if(line.isbatchandexpiry){
				
				if(line.m_attributesetinstance_id == 0){
					
					alert("Lot No. and Expiry Date is mandatory for product[" + line.name + "]");
					return;
				}				
			}
			
			/*if(line.current_price < line.price_entered){
				
				alert("Selling Price of Product[" + line.name + "] cannot be less than Purchase Price");
				return;
			}*/
			
			//check for Livewell percentage margin. shoud be more than 20%.
			
			if(ctrl.domain == 'Livewell'){
				
				var gross_profit_margin = ctrl.getPercentageProfitMargin(line, i);
				
				if(gross_profit_margin < 20){
					
					if(window.confirm( "Product [ "+line.name+" ] has gross profit margin less than 20!\nDo you want to correct the gross profit margin?") ){
						
						return;
						
					}
				}
			}
						
			if((line.price_entered != line.price_list))
			{
				ctrl.priceDifferent = true;
			}

			if(ctrl.isDiscountApplied)
			{
				ctrl.priceDifferent = true;
			}

			if(ctrl.isDiscountApplied && !ctrl.listPriceChanged)
			{
				ctrl.priceDifferent = false;				
			}

			if(ctrl.isDiscountApplied && ctrl.listPriceChanged)
			{
				ctrl.priceDifferent = true;
			}

			//post.updateprice = false;

			if(line.is_list_price_changed){

				ctrl.priceDifferent = true;
			}

			//complete && ctrl.priceDifferent
			if( ctrl.priceDifferent ){

				//role validation

				if(!ctrl.isDiscountApplied && ctrl.listPriceChanged){
					var newPrice = document.getElementById('price-' + i).value;

					line.price_list = newPrice;
				}
				
				if(!ctrl.isDiscountApplied && (line.price_entered != line.price_list)){
					var newPrice = document.getElementById('price-' + i).value;

					line.price_list = newPrice;
				}

				/*if(window.confirm("The unit cost price of the items are different from the purchase price list. Do you want to update the prices on the price list?")){
					line.updateprice = true;
				}						
				else
				{							
					line.updateprice = false;
				}*/
				
				line.updateprice = true;
			}
			
			//for saved purchases
			var discount_percentage = document.getElementById('discount-percentage-' + i).value;
			if(discount_percentage > 0 && !ctrl.isDiscountApplied)
			{
				var newPrice = document.getElementById('price-' + i).value;

				line.price_entered = newPrice;
			}

			lines.push({

				'description' : line.description,
				'boxNo' : line.box,
				'm_product_id' : line.m_product_id,
				'c_tax_id' : line.c_tax_id,
				'c_uom_id' : line.c_uom_id,
				'pricelist' : line.price_list,
				'pricestd' : line.price_std,
				'priceentered' : line.price_entered,
				'qty' : line.qty,
				'updateprice' : line.updateprice,
				'm_attributesetinstance_id' : line.m_attributesetinstance_id,
				'grossprofitmargin' : ctrl.getPercentageProfitMargin(line, i),
				'c_orderline_id' : line.c_orderline_id
			});
			
		}

		post.lines = lines;		
		post.linesToDelete = ctrl.linesToDelete;
		post.linesToSplit = ctrl.linesToSplit;

		$timeout(function(){

			var data = JSON.stringify( post );
			$("#post_data").val( data );

			ctrl.showModal = true;

			sessionStorage.removeItem("purchaseDraft");

			$("#purchase_form").submit();

		});				

	};			

	ctrl.updatePurchasePricelist = function(){		

		var dfd = new jQuery.Deferred();

		var pricelist_id;				

		pricelist_id = ctrl.supplierPurchasePrice.value;
		
		var list = ctrl.supplierPurchasePriceList;	
		
		if(list != undefined){		

			for( var i=0; i<list.length; i++ )
			{
				if( list[i].value == pricelist_id )
				{
					pricelist = list[i];
					ctrl.supplierPurchasePrice = pricelist;
				}
			}	
		}

		$http.get( 'ShoppingCartAction.do?action=setPriceList&orderType=Purchase&format=json&forceResetLine=' + ctrl.forceResetLine + '&pricelistId=' + pricelist_id ).then(function(response){					

			if(response.data.error)
			{
				var m_pricelist_version_id = response.data.m_pricelist_version_id;
				var m_product_id = response.data.m_product_id;

				if(window.confirm( response.data.error + " \nDo you want to add product on pricelist?") ){

					//ctrl.productNotOnPricelist = false;
					ctrl.productNameNotOnPricelist = response.data.product_name_not_on_pricelist;
					ctrl.showModal = false;					
					ctrl.addProductToPricelistModal = true;	
					
					ctrl.add = function(){

						ctrl.addProductToPricelist( response, false );								
					}

					ctrl.close = function(){

						ctrl.addProductToPricelistModal = false;
						ctrl.setPreviousPricelist( true );
						//ctrl.setPreviousPricelistSynchronously(true);
						ctrl.reset();
					}							
				}
				else
				{
					//set pricelist to previous pricelist												
					ctrl.setPreviousPricelist( false );
					//ctrl.setPreviousPricelistSynchronously(false);
					ctrl.productNotOnPricelist = true;	
					ctrl.getPricePrecision(ctrl.supplierPurchasePrice.value);
				}						
			}
			else
			{
				ctrl.shoppingCart = response.data;	
				var line;
				
				/*for(var i=0;i<ctrl.shoppingCart.lines.length; i++){
					
					line = ctrl.shoppingCart.lines[i];
					
					var discount_percentage = document.getElementById('discount-percentage-' + i).value;
					
					if(discount_percentage > 0 && ctrl.supplierPurchasePriceList.length > 1){						
					
						ctrl.setPriceAfterPricelistChanged(line, i);						
					}
				}*/
				
				for(var i=0;i<ctrl.shoppingCart.lines.length; i++){
					
					line = ctrl.shoppingCart.lines[i];
					
					if(line.discount_percentage > 0){
					
						ctrl.isDiscountApplied = true;
						break;
					}
				}
				
				ctrl.productNotOnPricelist = false;
			}

			dfd.resolve( response );

		}, function(error){

			if( error.status == 500 ){
				
				alert('Session timed out! Log in again.');
			}
			else{
				
				alert('Error: ' + error.statusText);
			}
			
			dfd.reject( error );

		}).finally(function(){

			if(ctrl.supplierPurchasePrice.value != undefined){
				
				ctrl.getPricePrecision(ctrl.supplierPurchasePrice.value);
			}
			
			ctrl.showModal = false;
			textfield.select();
			textfield.focus();
			
		});		

		return dfd.promise();				
	}
	
	ctrl.getPricePrecision = function(m_pricelist_id){
		
		$http.get('PurchaseAction.do?action=getPricePrecision&orderType=Purchase&format=json&m_pricelist_id=' + m_pricelist_id,{
			
		}).success(function(pricePrecision){ 
			ctrl.pricePrecision = pricePrecision;
		});
	}

	ctrl.validate = function(){

		//supplier
		if( StringUtils.isEmpty( ctrl.supplier ) ){
			alert("Select a supplier");					
			$("#purchase_supplier").focus();					
			return false;
		}

		//warehouse
		if( StringUtils.isEmpty( ctrl.warehouse ) ){
			alert("Select a warehouse for delivery");					
			$("#purchase_warehouse").focus();					
			return false;
		}


		//reference
		if( StringUtils.isEmpty( ctrl.referenceno ) ){
			alert("Enter a reference number");					
			$("#purchase_referenceno").focus();					
			return false;
		}


		//payment rule
		if( StringUtils.isEmpty( ctrl.paymentrule ) ){
			alert("Select a payment type");
			$("#purchase_paymentrule").focus();					
			return false;
		}

		//deliverydate
		if( StringUtils.isEmpty( ctrl.deliverydate ) ){
			alert("Select a delivery date");
			$("#purchase_deliverydate").focus();					
			return false;
		}

		//promisedate
		if( StringUtils.isEmpty( ctrl.promisedate ) ){
			alert("Select an arrival date");
			$("#purchase_promisedate").focus();					
			return false;
		}

		//purchase price list
		if( StringUtils.isEmpty( ctrl.supplierPurchasePrice ) ){
			alert("Select a purchase prcelist");					
			$("#supplier_purchase_pricelist").focus();					
			return false;
		}

		return true;
	};

	var textfield = document.getElementById('search-textfield');

	/* util function to send shopping cart actions */
	ctrl.shoppingCartAction = function( action ){

		var dfd = new jQuery.Deferred();

		ctrl.showModal = true;

		$http.get( action ).then(function(response){

			ctrl.shoppingCart = response.data;
			
			if(response.data.error){
				dfd.reject(response.data.error);
				alert(response.data.error);
			}

			dfd.resolve(response);

		}, function(error){

			dfd.reject(error);
			alert('Error: ' + error);

		}).finally(function(){

			ctrl.showModal = false;
			textfield.select();
			textfield.focus();
		});

		return dfd.promise();				
	};

	/* shopping cart related */
	ctrl.getShoppingCart = function(){

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=getCart&orderType=Purchase&format=json');
	};

	/* others */
	ctrl.setBoxNo = function(){

		var boxNo = window.prompt("Enter Box/Bag No");

		if(boxNo != null){

			ctrl.showModal = true;

			ctrl.boxNumber = boxNo;

			ctrl.shoppingCartAction('ShoppingCartAction.do?action=setCurrentBoxNumber&orderType=Purchase&format=json&currentBoxNumber=' + boxNo);

		}

	};

	ctrl.updateQty = function(line, index){

		/* compare qty */
		var previousQty = line.qty;
		var newQty = document.getElementById('qty-' + index).value;

		var lineId = line.line_id;

		if(previousQty == newQty) return; /* take no action */

		if(newQty < 0){
			line.qty = newQty;
			alert("Qty cannot be less than 1!");
			document.getElementById('qty-' + index).select();
			return;
		}
		
		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updateQty&orderType=Purchase&format=json&lineId=' + lineId + '&qty=' + newQty).always(function(){

		});

	};

	ctrl.updatePack = function(line, index){

		/* compare qty */
		var previousQty = document.getElementById('qty-' + index).value;
		var noOfPacks = document.getElementById('no-of-packs-' + index).value || 1;

		var newQty = noOfPacks * line['unitsperpack'];

		var lineId = line.line_id;

		if(previousQty == newQty) return; /* take no action */

		if(newQty < 0){
			line.qty = newQty;
			alert("No of packs cannot be less than 1!");
			document.getElementById('no-of-packs-' + index).select();
			return;
		}

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updateQty&orderType=Purchase&format=json&lineId=' + lineId + '&qty=' + newQty);

	};

	ctrl.updatePrice = function(line, index){

		ctrl.forceResetLine = false;

		/* compare price */
		var previousPrice = line.price_entered;
		var newPrice = document.getElementById('price-' + index).value;

		var lineId = line.line_id;

		if(previousPrice == newPrice){

			return; /* take no action */
		} 
		else 
		{
			ctrl.listPriceChanged = true;
		}
		
		if(ctrl.enable_batch_and_expiry == true && line.isbatchandexpiry == true){
			
			lotId = document.getElementById('lot-' + index).value || 0;	
		}
		else 
		{
			lotId = 0;
		}

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updatePrice&orderType=Purchase&format=json&lineId=' + lineId + '&price=' + newPrice + '&attributesetInstanceId=' + lotId );

		//ctrl.shoppingCartAction('ShoppingCartAction.do?action=updatePrice&orderType=Purchase&format=json&lineId=' + lineId + '&price=' + newPrice);
	};

	//update total
	ctrl.updateLineTotal = function(line, index){
		
		ctrl.forceResetLine = false;
		
		/* compare total */
		var previousTotal;
		
		if(line.is_tax_included)
		{					
			previousTotal = line.line_net_amount;
		}
		
		else
		{					
			previousTotal = line.line_amount;
		}	
		
		var newTotal = document.getElementById('total-' + index).value;
		var lineId = line.line_id;
		
		if(previousTotal == newTotal) return; /* take no action */
		
		var qty = document.getElementById('qty-' + index).value;
		
		var newPrice = new BigNumber(newTotal).dividedBy(qty).toFixed(4);
		
		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updatePrice&orderType=Purchase&format=json&lineId=' + lineId + '&price=' + newPrice);
	}

	//for discount
	ctrl.updateListPrice = function(line, index){

		ctrl.forceResetLine = false;

		/* compare price */
		var previousPrice = line.price_list;
		var newPrice = document.getElementById('price-list-' + index).value;

		ctrl.previousListPrice = previousPrice;
		ctrl.newListPrice = newPrice;

		var discountPercentage = document.getElementById('discount-percentage-' + index).value;
		
		if(discountPercentage == ''){
			
			alert("Discount percentage cannot be null!");
			
			return;
		}

		if(discountPercentage !=null && discountPercentage > 0 )
		{
			ctrl.isDiscountApplied = true;
			ctrl.listPriceChanged = true;
		}
		else
		{
			ctrl.isDiscountApplied = false;
		}

		var lineId = line.line_id;
				
		//if(previousPrice == newPrice) return; /* take no action */

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updateListPrice&orderType=Purchase&format=json&lineId=' + lineId + '&price=' + newPrice + '&discountPercentage=' + discountPercentage +'&isListPriceChanged=' + ctrl.listPriceChanged);

	};		
	
	ctrl.setPriceAfterPricelistChanged = function(line, index){

		ctrl.forceResetLine = false;

		/* compare price */
		//var previousPrice = line.price_list;
		//var newPrice = document.getElementById('price-list-' + index).value;

		//ctrl.previousListPrice = previousPrice;
		//ctrl.newListPrice = newPrice;

		var newPrice = line.price_entered;
		
		var discountPercentage = document.getElementById('discount-percentage-' + index).value;
		
		if(discountPercentage == ''){
			
			alert("Discount percentage cannot be null!");
			
			return;
		}

		if(discountPercentage !=null && discountPercentage > 0 )
		{
			ctrl.isDiscountApplied = true;
			ctrl.listPriceChanged = true;
		}
		else
		{
			ctrl.isDiscountApplied = false;
		}

		var lineId = line.line_id;
				
		//if(previousPrice == newPrice) return; /* take no action */

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updateListPrice&orderType=Purchase&format=json&lineId=' + lineId + '&price=' + newPrice + '&discountPercentage=' + discountPercentage +'&isListPriceChanged=' + ctrl.listPriceChanged);

	};

	ctrl.updateUpc = function(line, index){

		ctrl.forceResetLine = false;

		/* compare upc */
		var previousUpc = line.upc;
		var newUpc = document.getElementById('upc-' + index).value;

		var lineId = line.line_id;
		var productId = line.m_product_id;

		if(previousUpc == newUpc) return; /* take no action */

		if(newUpc == null){

			alert("Upc cannot be null!");

			document.getElementById('upc-' + index).select();
			return;
		}

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updateUpc&orderType=Purchase&format=json&lineId=' + lineId + '&upc=' + newUpc +'&productId=' + productId);

	};
	
	ctrl.updateSellingPrice = function(line, index){
		
		ctrl.forceResetLine = false;
		
		/* compare upc */
		var previousSellingPrice = line.current_price;
		var newSellingPrice = document.getElementById('current-price-' + index).value;
		
		var lotId;
		
		if(ctrl.enable_batch_and_expiry == true && line.isbatchandexpiry == true){
			
			lotId = document.getElementById('lot-' + index).value || 0;	
		}
		else 
		{
			lotId = 0;
		}
		
		var lineId = line.line_id;
		var productId = line.m_product_id;
		
		if(previousSellingPrice == newSellingPrice) return; /* take no action */
		
		if(newSellingPrice == ''){
			
			alert("Current Price cannot be null!");
			
			//document.getElementById('current-price-' + index).select();
			return;
		}
		
		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updateSellingPrice&orderType=Purchase&format=json&lineId=' + lineId + '&sellingPrice=' + newSellingPrice +'&productId=' + productId + '&attributesetInstanceId=' + lotId );
						
	};
	
	ctrl.updateBoxSellingPrice = function(line, index){
		
		ctrl.forceResetLine = false;
		
		/* compare boxCurrentPrice */
		var previousBoxSellingPrice = ctrl.boxSellingPrice;
		var boxCurrentPrice = document.getElementById('box-current-price-' + index).value;
		
		if(boxCurrentPrice == ''){
			
			alert("Box Current Price cannot be null!");
			
			//document.getElementById('box-current-price-' + index).select();
			return;
		}
		
		var newBoxSellingPrice = (boxCurrentPrice * line.noofpacks) / line.qty;		
		
		var lotId;
		
		if(ctrl.enable_batch_and_expiry == true && line.isbatchandexpiry == true){
			
			lotId = document.getElementById('lot-' + index).value || 0;	
		}
		else 
		{
			lotId = 0;
		}
		
		var lineId = line.line_id;
		var productId = line.m_product_id;
		
		if(previousBoxSellingPrice == boxCurrentPrice) return; /* take no action */
		
		/*if(newBoxSellingPrice == null){
			
			alert("Box Current Price cannot be null!");
			
			document.getElementById('box-current-price-' + index).select();
			return;
		}*/
		
		ctrl.shoppingCartAction('ShoppingCartAction.do?action=updateSellingPrice&orderType=Purchase&format=json&lineId=' + lineId + '&sellingPrice=' + newBoxSellingPrice +'&productId=' + productId + '&attributesetInstanceId=' + lotId );
						
	};

	ctrl.getHighlighted = function(line_id){

		if(ctrl.shoppingCart.last_updated_line_id == line_id)
			return true;
	}
	
	ctrl.highlightPercentageProfitMargin = function(line, i){
		
		if(ctrl.domain == 'Livewell'){
			
			var gppm = ctrl.getPercentageProfitMargin(line, i);

			if(gppm < 20)

			return true;
		}
		
	}

	ctrl.validateQty = function(line, index){

		var newQty = document.getElementById('qty-' + index).value;
		if(newQty < 0){
			alert("Qty cannot be negative");
		}

	};	

	ctrl.changeTax = function(line, index){
		/* compare tax */
		var previousTaxId = line.c_tax_id;
		var newTaxId = document.getElementById('tax-' + index).value;

		var lineId = line.line_id;

		if(previousTaxId == newTaxId) return; /* take no action */
		
		//recalculate price for discounted price
		if(line.discount_percentage > 0){
			
			ctrl.updateListPrice(line, index);
		}
		
		var discountPercentage = document.getElementById('discount-percentage-' + index).value;
		var price = document.getElementById('price-list-' + index).value;

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=setTax&orderType=Purchase&format=json&lineId=' + lineId + '&taxId=' + newTaxId + "&discountPercentage=" + discountPercentage + "&price=" + price);
	}

	ctrl.changeLotExpiry = function(line, index){
		var lotId = document.getElementById('lot-' + index).value;				
		var lineId = line.line_id;
		
		ctrl.shoppingCartAction('ShoppingCartAction.do?action=setAttributeSetInstance&orderType=Purchase&format=json&attributesetInstanceId=' + lotId + '&lineId=' + lineId );
		
	}
	
	ctrl.createNewLotExpiry = function(line){

		$modal.open({

			templateUrl: 'create-lot-template.html',
			controllerAs : '$ctrl',
			resolve : {
				line : function(){
					return line;
				}
			},
			controller: function($scope, $modalInstance, line){

				var ctrl$ = this;

				$scope.lotNo = "";
				
				ctrl$.minimumDate = new Date(); 
				
				ctrl$.opened1 = false;
				ctrl$.open1 = function(e) {
					e.stopPropagation ();
					this.opened1 = true;
				};

				ctrl$.cancel = function(){
					$modalInstance.dismiss('cancel');
				};

				ctrl$.createLot = function(lot, date){			    		  
					var expiry = date == null ? "" : moment( date ).format('YYYY-MM-DD');

					if(expiry == "" && lot == ""){
						alert("Both Lot No. and Expiry Date cannot be empty!");
						return;
					}

					$modalInstance.dismiss('close');

					var lineId = line.line_id;

					ctrl.shoppingCartAction('ShoppingCartAction.do?action=createLotAndExpiry&orderType=Purchase&format=json&lot=' + lot + '&expiry=' + expiry + '&lineId=' + lineId );
				};	  

			},
			size: 'sm',
			scope : $scope
		});				

	}

	ctrl.setPreviousPricelist = function( forceResetLine ){

		var pricelist = {};

		var previousPriceListId = ctrl.shoppingCart.m_pricelist_id;

		var list = ctrl.supplierPurchasePriceList;						

		for( var i=0; i<list.length; i++ )
		{
			if( list[i].value == previousPriceListId )
			{
				pricelist = list[i];
				ctrl.supplierPurchasePrice = pricelist;
			}
		}	

		$http.get( 'ShoppingCartAction.do?action=setPriceList&orderType=Purchase&format=json&forceResetLine=' + forceResetLine + '&pricelistId=' + previousPriceListId ).then(function(response){					

			ctrl.shoppingCart = response.data;	

		}, function(error){

			alert('Error: ' + error);

		});
		
	}		
	
	//set back previous pricelist synchronously when cancel updating new pricelist. 
	ctrl.setPreviousPricelistSynchronously = function( forceResetLine ){

		var pricelist = {};

		var previousPriceListId = ctrl.shoppingCart.m_pricelist_id;

		var list = ctrl.supplierPurchasePriceList;						

		for( var i=0; i<list.length; i++ )
		{
			if( list[i].value == previousPriceListId )
			{
				pricelist = list[i];
				ctrl.supplierPurchasePrice = pricelist;
			}
		}	
		
		$.get( 'ShoppingCartAction.do?action=setPriceList&orderType=Purchase&format=json&forceResetLine=' + forceResetLine + '&pricelistId=' + previousPriceListId, function( response ){
			
			ctrl.shoppingCart = response;	
			
			var line;
			for(var i=0; i<ctrl.shoppingCart.lines.length; i++){
				
				line = ctrl.shoppingCart.lines[i];				

				var discountPercentage = document.getElementById('discount-percentage-' + i).value;
				
				var previousPrice = line.price_entered;
				var newPrice = document.getElementById('price-' + i).value;
				
				if((previousPrice != newPrice) && discountPercentage == 0){					
						
					ctrl.updatePriceSynchronously(line, i);										
				}
				
				if((previousPrice !=newPrice) && discountPercentage > 0){					
						
					ctrl.updateListPriceSynchronously(line, i);
				}
			}
		});
	}
	
	//update price synchronously. prices was being updated at last. causing amount to display wrong figures at times
	ctrl.updatePriceSynchronously = function( line, index ){
		
		ctrl.forceResetLine = false;

		/* compare price */
		var previousPrice = line.price_entered;
		var newPrice = document.getElementById('price-' + index).value;

		var lineId = line.line_id;

		if(previousPrice == newPrice){

			return; /* take no action */
		} 
		else 
		{
			ctrl.listPriceChanged = true;
		}
		
		if(ctrl.enable_batch_and_expiry == true && line.isbatchandexpiry == true){
			
			lotId = document.getElementById('lot-' + index).value || 0;	
		}
		else 
		{
			lotId = 0;
		}
		
		$.get( 'ShoppingCartAction.do?action=updatePrice&orderType=Purchase&format=json&lineId=' + lineId + '&price=' + newPrice + '&attributesetInstanceId=' + lotId );
		
		var dfd = new jQuery.Deferred();	        	        
        
        $.ajax({
            method: 'GET',
            url: url,
            dataType: 'json',
            async: false,
            success: function( response ){
            	
            	ctrl.shoppingCart = response;
                dfd.resolve( response );
            },
            error: function( jqXHR, textStatus, errorThrown ){
            	
            	dfd.reject( textStatus );
            }
        });
        
        return dfd.promise();
	}
	
	//update price synchronously. prices was being updated at last. causing amount to display wrong figures at times
	ctrl.updateListPriceSynchronously = function(line, index){		
		
		ctrl.forceResetLine = false;

		/* compare price */
		var previousPrice = line.price_list;
		var newPrice = document.getElementById('price-list-' + index).value;

		ctrl.previousListPrice = previousPrice;
		ctrl.newListPrice = newPrice;

		var discountPercentage = document.getElementById('discount-percentage-' + index).value;
		
		if(discountPercentage == ''){
			
			alert("Discount percentage cannot be null!");
			
			return;
		}

		if(discountPercentage !=null && discountPercentage > 0 )
		{
			ctrl.isDiscountApplied = true;
			ctrl.listPriceChanged = true;
		}
		else
		{
			ctrl.isDiscountApplied = false;
		}

		var lineId = line.line_id;
				
		//if(previousPrice == newPrice) return; /* take no action */

		var url = 'ShoppingCartAction.do?action=updateListPrice&orderType=Purchase&format=json&lineId=' + lineId + '&price=' + newPrice + '&discountPercentage=' + discountPercentage +'&isListPriceChanged=' + ctrl.listPriceChanged;
				
		var dfd = new jQuery.Deferred();	        	        
        
        $.ajax({
            method: 'GET',
            url: url,
            dataType: 'json',
            async: false,
            success: function( response ){
            	
            	ctrl.shoppingCart = response;
                dfd.resolve( response );
            },
            error: function( jqXHR, textStatus, errorThrown ){
            	
            	dfd.reject( textStatus );
            }
        });
        
        return dfd.promise();
	}
	
	
	//((current price - price entered) / price entered * 100)
	ctrl.getPercentageProfitMargin = function(line, index){
		
		
		/*var price_entered = line.price_entered;
		var current_price = line.current_price;
		
		if( current_price == 0 ) return 0.00;		
		
		var percentage_profit_margin = ((current_price - price_entered) / current_price) * 100;
		
		return percentage_profit_margin.toFixed(2);*/
		
		var price_entered = line.price_entered;
		var current_price = line.current_price;
		
		var percentage_profit_margin;
		
		var price = document.getElementById('price-' + index).value;
		
		var price_list = document.getElementById('price-list-' + index).value;
		
		if( isNaN(current_price) ) return 0.00;
		
		if(price != price_list){
			
			if( price == 0 ) return 0.00;
			
			percentage_profit_margin = ((current_price - price) / price) * 100;
		}
		else{
			
			if( price_list == 0 ) return 0.00;
		
			percentage_profit_margin = ((current_price - price_entered) / price_list) * 100;
		}
		
		return percentage_profit_margin.toFixed(2);
	}
	
	ctrl.getBoxSellingPrice = function(line, index){
		
		var current_price = line.current_price;
		var qty = line.qty;
		var noOfPacts = line.noofpacks;
		
		if( current_price == 0 ) return 0.00;
		
		if( isNaN(current_price) ) return 'N/A';
		
		var boxSellingPrice = (current_price * qty ) / noOfPacts ;
		
		ctrl.boxSellingPrice = boxSellingPrice;
		
		return boxSellingPrice.toFixed(ctrl.pricePrecision);
		
	};		
	
	ctrl.removeFromCart = function(line){

		var lineId = line.line_id;

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=removeFromCart&orderType=Purchase&format=json&lineId=' + lineId);
		
		ctrl.linesToDelete.push(line.m_product_id);		

	};

	ctrl.splitLine = function(line){

		if(line.qty == 1){
			alert("Cannot split line. Qty must be greater than 1");
			return;
		}

		var lineId = line.line_id;
		
		ctrl.shoppingCartAction('ShoppingCartAction.do?action=splitLines&orderType=Purchase&qty=1&format=json&lineId=' + lineId);
		
		if(ctrl.shoppingCart.c_order_id > 0){
			
			ctrl.linesToSplit.push(line.c_orderline_id);
		}
		
	};

	ctrl.clearList = function(){

		if(window.confirm("Do you want to clear all the lines?"))
		{
			ctrl.shoppingCartAction('ShoppingCartAction.do?action=clearCart&orderType=Purchase&format=json').then(function(){

				ctrl.forceResetLine = true;
				ctrl.updatePurchasePricelist();
				ctrl.setWarehouse();
			});
		}				
	};


	ctrl.setWarehouse = function(){				

		ctrl.shoppingCartAction('ShoppingCartAction.do?action=setWarehouse&orderType=Purchase&format=json&warehouseId=' + ctrl.warehouse.value);

	};

	ctrl.cancel = function(){
		window.location = "backoffice-menus.do#pmenuId=10002150";
	};

	ctrl.importcsv = function(){

		if(!ctrl.validate()){
			return;
		}

		if(ctrl.supplierPurchasePrice == null){

			alert('Select Pricelist');
			return;
		}

		//save state before page switch
		var draft = {};

		draft.purchase_id = ctrl.purchase_id
		draft.supplier_id = ctrl.supplier.value;				
		draft.referenceno = ctrl.referenceno;
		draft.paymentrule = ctrl.paymentrule;
		draft.promisedate = moment( ctrl.promisedate ).format('DD-MM-YYYY');
		draft.deliverydate = moment( ctrl.deliverydate ).format('DD-MM-YYYY');
		draft.warehouse_id = ctrl.warehouse.value;
		draft.pricelist_id = ctrl.supplierPurchasePrice.value;
		draft.supplierPurchasePriceList = ctrl.supplierPurchasePriceList;
		draft.priceprecision = ctrl.pricePrecision;

		sessionStorage.setItem("purchaseDraft", JSON.stringify(draft));

		window.location = "importer.do?importer=OrderLineImporter&orderType=Purchase&pricelistId=" + ctrl.supplierPurchasePrice.value;
	}

	//load shopping cart first time
	ctrl.getShoppingCart();

	if(purchase_pricelist_id > 0)
	{
		if(is_discount_percentage_applied)
		{
			ctrl.forceResetLine = false;
			ctrl.isDiscountApplied = true;
		}
		else
		{
			ctrl.forceResetLine = false;
			ctrl.isDiscountApplied = false;
			ctrl.updatePurchasePricelist();
		}		
	}
	else{
		ctrl.isDiscountApplied = false;
	}
	
});