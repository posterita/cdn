var app = angular.module("app", ['ui.bootstrap']);
app.controller("ViewPurchaseController", function($scope, $http, $timeout, doc) {
    var ctrl = this;

    ctrl.showHeader = true;
    ctrl.m_inout_id = 0;
    ctrl.boxNumber = "";

    ctrl.document = doc;

    ctrl.cancel = function() {
        window.location = "backoffice-menus.do#pmenuId=10002150";
    };

    ctrl.getTotalQtyReceived = function() {
        var line;
        var qtyRececived;
        var qtyTotal = 0;

        for (var i = 0; i < ctrl.document.lines.length; i++) {
            line = ctrl.document.lines[i];

            qtyRececived = line.qtyDelivered;
            qtyTotal = parseInt(qtyTotal) + parseInt(qtyRececived);
        }

        return qtyTotal;
    }

    ctrl.receiveItems = function(save) {

        var line;
        var qty;

        var receivedQtys = [];

        var lines = ctrl.shippingLines;

        for (var i = 0; i < lines.length; i++) {

            line = lines[i];

            qty = (line['qtyReceived'] || 0);

            if (qty < 0) {
                alert("Qty cannot be negative!");
                document.getElementById("qty-" + i).select();
                return;
            }

            if (qty == 0) continue;

            receivedQtys.push({
                'c_orderline_id': line['c_orderline_id'],
                'qtyReceived': qty,
                'box': line.box
            });
        }

        if (receivedQtys.length == 0) {
            alert("No lines found!");
            return;
        }

        //push data
        var post = {};
        post.purchase_id = ctrl.document.id;
        post.lines = receivedQtys;
        post.dateReceived = moment().format("DD-MM-YYYY");
        post.referenceNo = ctrl.referenceno;
        post.save = save;
        post.m_inout_id = ctrl.m_inout_id;

        var data = JSON.stringify(post);
        $("#post_data").val(data);

        ctrl.showModal = true;

        $("#purchase_form").submit();
    };

    ctrl.markAllAsReceived = function() {

        for (var i = 0; i < ctrl.document.lines.length; i++) {

            line = ctrl.document.lines[i];

            line['qtyReceived'] = (parseInt(line.qty) > parseInt(line.qtyDelivered)) ? (parseInt(line.qty) - parseInt(line.qtyDelivered)) : 0;

        }
    };
   
    var press_enter = false;
    
    $('#search-textfield').on('keypress',function(e) {
        if(e.which == 13) {
        	 $('#search-textfield').select();
        	 
        	 press_enter = true;
        }
    });
    
    ctrl.searchProduct = function(searchTerm) {
    	
		return $http.get('PurchaseAction.do', {
            params: {
                'action': 'searchProduct',
                'searchTerm': searchTerm
            }
        }).then(function(response) { 
        	        	
        	if(response.data.length == 1 && searchTerm == response.data[0].upc && press_enter){
        		
        		press_enter = false;
        		
        		$timeout(function(){
        			
        			$('.product-search-result').trigger('click');
        			
        		},100);
        		
        		console.log("exact match");        		
        	}   
        	
        	if(response.data.length == 0 && press_enter){
        		
        		press_enter = false;
        		
        		alert('No product found!');
        		$('#search-textfield').select();
        	}
        	
            return response.data;
        });
    };

    ctrl.orderedProducts = new HashMap();
    ctrl.initOrderedProducts = function() {

        var lines = ctrl.document.lines;
        var line, m_product_id;

        var map = ctrl.orderedProducts;
        
        for (var i = 0; i < lines.length; i++) {

            line = lines[i];
            m_product_id = line['m_product_id'];

            if (!map.hasItem(m_product_id)) {

                map.put(m_product_id, line);
            }
        }    

    };

    ctrl.initOrderedProducts();

    ctrl.shippingLineMap = new HashMap();
    ctrl.shippingLines = [];    
    
    
    var _addProduct = function(product, qtyReceived) {
    	
    	ctrl.showModal = true;
    	
        if (product == null) return;

        /* 1. check if product was ordered */
        var line = ctrl.orderedProducts.get(product['m_product_id']);

        if (line == null) {

            alert('Product - ' + product['name'] + ' was not ordered!');

            return;
        } 
        

        /* 2. check if shipping line for current box number exists */
        var indexId = ctrl.boxNumber + "-" + line['m_product_id'];
        var map = ctrl.shippingLineMap;

        var shippingLine = map.get(indexId);
        
        //highlight line
        $timeout(function() {
        	
        	ctrl.getHighlighted = function(id){	
        		
	    		if(indexId == id){
	    			
	    			return true;
	    			
	    			}
	    		}
        	});        
        
        if (shippingLine == null) {

            var qtyEnetered = 1;

            if (qtyReceived) {

                qtyEnetered = qtyReceived;
            }

            shippingLine = {

                id: indexId,
                box: ctrl.boxNumber,
                m_product_id: line['m_product_id'],
                upc: line['upc'],
                productName: line['productName'],
                description: line['description'],
                lot: line['lot'],
                qty: line['qty'],
                'qtyReceived': qtyEnetered,
                qtyDelivered: line['qtyDelivered'],
                c_orderline_id: line['c_orderline_id'],
                m_attributesetinstance_id: line['m_attributesetinstance_id']

            };

            map.put(indexId, shippingLine);
        } else {
            shippingLine.qtyReceived += 1;
        }

        $timeout(function() {

            ctrl.shippingLines = map.toArray();

        });

    };
    
    ctrl.addProduct = function(product, qtyReceived) {
    	
    	$timeout(function(){
    		ctrl.showModal = true;    	
        	_addProduct(product, qtyReceived);    	
        	ctrl.showModal = false;
    	});
    };

    /* others */
    ctrl.setBoxNo = function() {

        var boxNo = window.prompt("Enter Box/Bag No");

        if (boxNo != null) {

            ctrl.boxNumber = boxNo;
        }

    };

    ctrl.reset = function() {

        if (window.confirm("Do you want to reset received lines?")) {
            ctrl.shippingLineMap.clear();
            ctrl.shippingLines = [];
        };

    };

    ctrl.removeShippingLine = function(id) {

        var map = ctrl.shippingLineMap;
        map.remove(id);

        $timeout(function() {
            ctrl.shippingLines = map.toArray();
        });

    };
    
    ctrl.checkForDraftShipments = function() {

        //load draft shipment if any

        var c_order_id = getParameterByName("c_order_id");
        var m_inout_id = getParameterByName("m_inout_id");

        if (m_inout_id != null) {

            ctrl.loadDraftShipment(c_order_id, m_inout_id);
        }
    };

    ctrl.loadDraftShipment = function(c_order_id, m_inout_id) {

        ctrl.showModal = true;

        $http.get('PurchaseAction.do', {
            params: {
                'action': 'getDraftShipment',
                'm_inout_id': m_inout_id,
                'c_order_id': c_order_id,
            }
        }).then(function(response) {

            ctrl.showModal = false;

            var draft = response.data;
            var lines = draft.lines;

            ctrl.m_inout_id = draft.m_inout_id;
            ctrl.referenceno = draft.reference_no;

            console.log(lines);
            console.log(ctrl.boxNumber);

            var line;
            for (var i = 0; i < lines.length; i++) {

                line = lines[i];

                ctrl.boxNumber = line.box;
                ctrl.addProduct(line, line.qtyentered);
            }

        }, function(error) {

            alert(error);
            ctrl.showModal = false;

        });
    };

    ctrl.checkForDraftShipments();


});