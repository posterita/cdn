var app = angular.module("app", ['ui.bootstrap']);
app.controller("ViewPurchaseController", function($scope, $http, $timeout, doc) {
	var ctrl = this;

	ctrl.showHeader = true;
	ctrl.m_inout_id = 0;

	ctrl.document = doc;

	ctrl.cancel = function() {
		window.location = "backoffice-menus.do#pmenuId=10002150";
	};

	ctrl.receiveItems = function(save) {

		var line;
		var qty;

		var receivedQtys = [];

		for (var i = 0; i < ctrl.document.lines.length; i++) {
			line = ctrl.document.lines[i];

			qty = (line['qtyShipped'] || 0);

			if (qty < 0) {
				alert("Qty cannot be negative!");
				document.getElementById("qty-" + i).select();
				return;
			}

			if (qty == 0) continue;

			receivedQtys.push({
				'c_orderline_id': line['c_orderline_id'],
				'qtyReceived': qty
			});
		}

		if (receivedQtys.length == 0) {
			alert("No lines found!");
			return;
		}

		//push data
		var post = {};
		post.purchase_id = ctrl.document.id;
		post.lines = receivedQtys;
		post.dateReceived = moment().format("DD-MM-YYYY");
		post.referenceNo = ctrl.referenceno;
		post.save = save;
		post.m_inout_id = ctrl.m_inout_id;

		var data = JSON.stringify(post);
		$("#post_data").val(data);

		ctrl.showModal = true;

		$("#purchase_form").submit();
	};

	ctrl.markAllAsReceived = function() {

		for (var i = 0; i < ctrl.document.lines.length; i++) {

			line = ctrl.document.lines[i];

			line['qtyShipped'] = (parseFloat(line.qty) > parseFloat(line.qtyDelivered)) ? (parseFloat(line.qty) - parseFloat(line.qtyDelivered)) : 0;

		}
	};

	ctrl.checkForDraftShipments = function() {

		//load draft shipment if any

		var c_order_id = getParameterByName("c_order_id");
		var m_inout_id = getParameterByName("m_inout_id");

		if (m_inout_id != null) {

			ctrl.loadDraftShipment(c_order_id, m_inout_id);
		}
	};

	ctrl.loadDraftShipment = function(c_order_id, m_inout_id) {

		ctrl.showModal = true;

		$http.get('PurchaseAction.do', {
			params: {
				'action': 'getDraftShipment',
				'm_inout_id': m_inout_id,
				'c_order_id': c_order_id,
			}
		}).then(function(response) {

			ctrl.showModal = false;

			var draft = response.data;
			var lines = draft.lines;

			ctrl.m_inout_id = draft.m_inout_id;
			ctrl.referenceno = draft.reference_no;

			console.log(lines);
			console.log(ctrl.boxNumber);

			var draftline, documentline;
			for (var i = 0; i < lines.length; i++) {

				draftline = lines[i];

				for (var j = 0; j < ctrl.document.lines.length; j++) {

					documentline = ctrl.document.lines[j];

					if (draftline.m_product_id == documentline.m_product_id) {

						documentline['qtyShipped'] = draftline.qtyentered;
						break;
					}

				}
			}

		}, function(error) {

			alert(error);
			ctrl.showModal = false;

		});
	};

	ctrl.checkForDraftShipments();

});