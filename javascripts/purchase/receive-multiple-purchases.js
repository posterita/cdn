var app = angular.module("app", ['ui.bootstrap', 'ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state('page1', {
        url: '/page1',
        templateUrl: '/page1.html',
        controller: 'Page1Controller'
    });

    $stateProvider.state('page2', {
        url: '/page2',
        templateUrl: '/page2.html',
        controller: 'Page2Controller'
    });

    $stateProvider.state('page3', {
        url: '/page3',
        params: {
            shipments: null
        },
        templateUrl: '/page3.html',
        controller: 'Page3Controller'
    });

    $urlRouterProvider.otherwise('page1');

});

app.service("DocumentService", function() {

    this.reset = function() {

        this.warehouse = null;
        this.documents = [];
        this.undeliveredItems = [];

    };

    this.setWarehouse = function(warehouse) {
        this.warehouse = warehouse;
    };

    this.getWarehouse = function() {
        return this.warehouse;
    };

    this.setDocuments = function(documents) {
        this.documents = documents;
    };

    this.getDocuments = function() {
        return this.documents;
    };

    this.setUndeliveredItems = function(undeliveredItems) {
        this.undeliveredItems = undeliveredItems;
    };

    this.getUndeliveredItems = function() {
        return this.undeliveredItems;
    };

    this.reset();

});

app.controller("Page1Controller", function($scope, $http, $timeout, $modal, $state, DocumentService) {

    $scope.warehouseList = [];

    $('#modal').show();

    //load 
    $http.get('DataTableAction.do?action=getWarehousesList').then(function(response) {

        $('#modal').hide();

        $scope.warehouseList = response.data;
        $scope.warehouse = DocumentService.getWarehouse();
        $scope.documents = DocumentService.getDocuments();

    }, function(error) {
        $('#modal').hide();
    });

    $scope.cancel = function() {
        window.location = "backoffice-menus.do#pmenuId=10002150";
    };

    $scope.updateWarehouse = function(e) {

        $scope.documents = [];

        DocumentService.setWarehouse($scope.warehouse);
        DocumentService.setUndeliveredItems([]);
        DocumentService.setDocuments([]);
    };

    $scope.addDocument = function(documentno) {

        if (documentno == null || documentno.length == 0) {
            alert("Invalid document no!");
            return;
        }

        const found = $scope.documents.find(function(x) {
            return x.documentno == documentno;
        });

        if (found) {
            alert("Document already selected!");
            return;
        }

        console.log("Requesting document #" + documentno);

        $('#modal').show();

        $http.get('PurchaseAction.do?action=getPurchaseInfo&documentNo=' + encodeURIComponent(documentno)).then(function(response) {

            $('#modal').hide();

            var result = response.data;

            if (result.error) {
                alert("Failed to load purchase #" + documentno + ". Error: " + result.error);
                return;
            }

            //validate document
            if (result.m_warehouse_id != $scope.warehouse.value) {
                alert("Error: Purchase belongs to another warehouse!");
                return;
            }

            if ("COCL".indexOf(result.docstatus) == -1) {
                alert("Error: Purchase has invalid doc status: " + result.docstatus);
                return;
            }

            if (result.isdelivered == 'Y') {
                alert("Error: Purchase has already been received!");
                return;
            }

            $scope.documents.push(result);


        }, function(error) {

            $('#modal').hide();

            alert("Failed to load document #" + documentno + ". Error: " + error);

        });
    };

    $scope.next = function() {

        var order_ids = [];

        for (var i = 0; i < $scope.documents.length; i++) {

            order_ids.push($scope.documents[i]['c_order_id']);
        }

        var ids = order_ids.join(",");

        $('#modal').show();

        $http.get('PurchaseAction.do?action=getUndeliveredItemsForMultiplePurchases&order_ids=' + encodeURIComponent(ids)).then(function(response) {

            $('#modal').hide();

            var result = response.data;

            if (result.error) {
                alert("Failed to load request undelivered items! Error: " + result.error);
                return;
            }

            DocumentService.setDocuments($scope.documents);
            DocumentService.setUndeliveredItems(result.data);

            //move to new page
            $state.go('page2');

        }, function(error) {

            $('#modal').hide();

            alert("Failed to load request undelivered items! Error: " + error);

        });

    };

    $scope.documents = [];

});

app.controller("Page2Controller", function($scope, $http, $timeout, $modal, $state, DocumentService) {

    $scope.productList = DocumentService.getUndeliveredItems();

    $scope.back = function() {
        $state.go('page1');
    };

    $scope.markAllAsReceived = function() {

        var list = $scope.shippingLines;
        var line;

        for (var i = 0; i < list.length; i++) {

            line = list[i];

            line.qtyentered = line.qtyordered - line.qtyreceived;
        }

    };

    $scope.receiveItems = function() {

        var order_ids = [];

        var documents = DocumentService.getDocuments();

        for (var i = 0; i < documents.length; i++) {

            order_ids.push(documents[i]['c_order_id']);
        }

        var lines = [];
        var line;

        for (var j = 0; j < $scope.shippingLines.length; j++) {

            line = $scope.shippingLines[j];

            if (line["qtyentered"] < 0) {
                alert("Qty cannot be negative!");
                document.getElementById("qty-" + j).select();
                return;
            }

            lines.push({
                "m_product_id": line["m_product_id"],
                "qtyentered": line["qtyentered"],
            });
        }

        var post = {};
        post.orderIds = order_ids;
        post.dateReceived = moment().format("DD-MM-YYYY");
        post.lines = lines;

        console.log(post);

        post = JSON.stringify(post);

        $('#modal').show();


        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        };

        $http.post('PurchaseAction.do', 'action=receiveItemsForMultiplePurchases&post=' + encodeURIComponent(post), config).then(function(response) {
            $('#modal').hide();

            $state.go('page3', {
                'shipments': response.data.shipments
            });

        }, function(error) {

            $('#modal').hide();

            alert("Failed to load request undelivered items! Error: " + error);

        });

        //$("#post_data").val(JSON.stringify(post));
        //$("#purchase_form").submit();			

    };

    // receive manually
    $scope.boxNumber = "";

    $scope.searchProduct = function(searchTerm) {

        return $http.get('PurchaseAction.do', {
            params: {
                'action': 'searchProduct',
                'searchTerm': searchTerm
            }
        }).then(function(response) {
            return response.data;
        });

    };

    $scope.orderedProducts = new HashMap();
    $scope.initOrderedProducts = function() {

        var lines = $scope.productList;
        var line, m_product_id;

        var map = $scope.orderedProducts;

        for (var i = 0; i < lines.length; i++) {

            line = lines[i];
            m_product_id = line['m_product_id'];

            if (!map.hasItem(m_product_id)) {

                map.put(m_product_id, line);
            }
        }

    };

    $scope.initOrderedProducts();

    $scope.shippingLineMap = new HashMap();
    $scope.shippingLines = [];

    $scope.addProduct = function(product) {

        if (product == null) return;

        /* 1. check if product was ordered */
        var line = $scope.orderedProducts.get(product['m_product_id']);

        if (line == null) {

            alert('Product - ' + product['name'] + ' was not ordered!');

            return;
        }

        /* 2. check if shipping line for current box number exists */
        var indexId = $scope.boxNumber + "-" + line['m_product_id'];
        var map = $scope.shippingLineMap;

        var shippingLine = map.get(indexId);
        if (shippingLine == null) {

            shippingLine = {
                id: indexId,
                box: $scope.boxNumber,
                m_product_id: line['m_product_id'],
                upc: line['upc'],
                name: line['name'],
                description: line['description'],
                qtyordered: line['qtyordered'],
                qtyentered: 1,
                qtyreceived: line['qtyreceived']

            };

            map.put(indexId, shippingLine);
        } else {
            shippingLine.qtyentered += 1;
        }

        $timeout(function() {

            $scope.shippingLines = map.toArray();

        });

    };

});

app.controller("Page3Controller", function($scope, $http, $timeout, $modal, $state, $stateParams, $window, DocumentService) {

    $scope.shipments = $stateParams.shipments;

    console.log($scope.shipments);

    $scope.receiveNew = function() {
        DocumentService.reset();
        $state.go('page1');
    };

    $scope.viewShipmentHistory = function() {
        $window.location.href = "report-shipments.do";
    };
});