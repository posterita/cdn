var app = angular.module("app", ['ui.bootstrap']);
app.controller("ViewPurchaseController", function($scope, $http, $timeout, doc) {
    var ctrl = this;

    ctrl.showHeader = true;

    ctrl.document = doc;

    ctrl.cancel = function() {
        window.location = "backoffice-menus.do#pmenuId=10002150";
    };

    ctrl.edit = function() {
        window.location = "PurchaseAction.do?action=edit&c_order_id=" + ctrl.document.id;
    };

    ctrl.receiveItems = function() {
        window.location = "PurchaseAction.do?action=receiveItems&c_order_id=" + ctrl.document.id;
    };

    ctrl.exportPdf = function() {
        window.location = "PurchaseAction.do?action=exportPdf&c_order_id=" + ctrl.document.id;
    };

    ctrl.markAsPaid = function() {

        var post = {};
        post.purchase_id = ctrl.document.id;
        post.datePaid = moment().format('DD-MM-YYYY');

        post = JSON.stringify(post);
        post = encodeURI(post);

        ctrl.showModal = true;

        window.location = "PurchaseAction.do?action=markAsPaid&post=" + post;
    };

    //check if partially received
    ctrl.partiallyReceived = false;

    var line;

    if (ctrl.document.header.shipped) {

        for (var i = 0; i < ctrl.document.lines.length; i++) {

            line = ctrl.document.lines[i];

            if (line.qtyDelivered < line.qty) {
                ctrl.partiallyReceived = true;
                break;
            }
        }
    }

});