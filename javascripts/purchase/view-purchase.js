var app = angular.module("app", ['ui.bootstrap']);
app.controller("ViewPurchaseController", function($scope, $http, $timeout, allow_void_purchase, doc, canPostOrder, accountingSolutionName) {
    var ctrl = this;

    ctrl.allow_void_purchase = allow_void_purchase;
    ctrl.document = doc;
    ctrl.canPostOrder = canPostOrder;
    ctrl.accountingSolutionName = accountingSolutionName;
    
    ctrl.showHeader = true;
    ctrl.partiallyReceived = false;    

    ctrl.getTotalQtyReceived = function() {
        var line;
        var qtyRececived;
        var qtyTotal = 0;

        for (var i = 0; i < ctrl.document.lines.length; i++) {
            line = ctrl.document.lines[i];

            qtyRececived = line.qtyDelivered;
            qtyTotal = parseInt(qtyTotal) + parseInt(qtyRececived);
        }

        return qtyTotal;
    }

    ctrl.getDateFormat = function(date) {
        return new Date(date);
    }

    ctrl.cancel = function() {
        ctrl.showModal = true;
        window.location = "backoffice-menus.do#pmenuId=10002150";
    };

    ctrl.edit = function() {
        ctrl.showModal = true;
        window.location = "PurchaseAction.do?action=edit&c_order_id=" + ctrl.document.id;
    };

    ctrl.receiveItems = function() {

        ctrl.showModal = false;
        ctrl.showConfirmModal = true;

        ctrl.receiveLater = function() {

            ctrl.showConfirmModal = false;
            ctrl.showModal = true;
            window.location = "PurchaseAction.do?action=receiveItems&c_order_id=" + ctrl.document.id;
        }

        ctrl.receiveLaterManually = function() {

            ctrl.showConfirmModal = false;
            ctrl.showModal = true;
            window.location = "PurchaseAction.do?action=receiveItemsManually&c_order_id=" + ctrl.document.id;
        }

        ctrl.receiveNow = function() {
            //receive all items directly					
            var line;
            var qty;

            var receivedQtys = [];

            for (var i = 0; i < ctrl.document.lines.length; i++) {
                line = ctrl.document.lines[i];

                qty = (parseInt(line.qty) > parseInt(line.qtyDelivered)) ? (parseInt(line.qty) - parseInt(line.qtyDelivered)) : 0;

                if (qty == 0) continue;

                receivedQtys.push({
                    'c_orderline_id': line['c_orderline_id'],
                    'qtyReceived': qty
                });
            }

            if (receivedQtys.length == 0) {
                alert("No lines found!");
                return;
            }

            //push data
            var post = {};
            post.purchase_id = ctrl.document.id;
            post.lines = receivedQtys;
            post.dateReceived = moment().format("DD-MM-YYYY");
            post.referenceNo = ctrl.referenceno;

            post = JSON.stringify(post);

            ctrl.partiallyReceived = false;
            ctrl.showConfirmModal = false;
            ctrl.showModal = true;

            /* window.location = "PurchaseAction.do?action=createShipment&post=" + post; */
            $("#post_action").val("createShipment");
            $("#post_data").val(post);
            $("#purchase_form").submit();
        }

        ctrl.close = function() {
            ctrl.showConfirmModal = false;
        }
    };

    ctrl.exportPdf = function() {
    	
    	const link = document.createElement('a');
        link.href = "PurchaseAction.do?action=exportPdf&c_order_id=" + ctrl.document.id;
        link.download = "purchase.pdf";
        link.click();
        
        //window.location = "PurchaseAction.do?action=exportPdf&c_order_id=" + ctrl.document.id;
    };

    ctrl.voidPurchase = function() {
        if (window.confirm("Do you want to void purchase order?")) {
            ctrl.showModal = true;
            window.location = "PurchaseAction.do?action=voidPurchase&c_order_id=" + ctrl.document.id;
        };
    };

    ctrl.showPostOrder = function(){
        return ctrl.document.header.docStatus == 'CO' && ctrl.canPostOrder == true && ctrl.document.header.posted == false && ctrl.document.header.delivered == true;
    };

    ctrl.postOrder = function(){
        if (window.confirm("Do you want to post purchase order?")) {
            ctrl.showModal = true;

            jQuery.getJSON("PurchaseAction.do?action=postPurchase&c_order_id=" + ctrl.document.id).done(function(response){

                if(response.error){
                    alert("Failed to post purchase! Error:" + response.error);
                }
                else
                {
                    $timeout(function(){
                        ctrl.document.header.posted = true;
                        alert("Purchase was successfully posted.");
                    });
                }

            }).always(function(){
                $timeout(function(){
                    ctrl.showModal = false;
                });
            });
        };
    }

    ctrl.newPurchase = function() {
        window.location = "PurchaseAction.do?action=loadPurchase";
    };

    ctrl.markAsPaid = function() {

        var post = {};
        post.purchase_id = ctrl.document.id;
        post.datePaid = moment().format('DD-MM-YYYY');

        post = JSON.stringify(post);

        ctrl.showModal = true;

        /*window.location = "PurchaseAction.do?action=markAsPaid&post=" + post ;*/
        $("#post_action").val("markAsPaid");
        $("#post_data").val(post);
        $("#purchase_form").submit();
    };

    ctrl.exportCsv = function() {
    	
    	const link = document.createElement('a');
        link.href = "PurchaseAction.do?action=exportCsv&c_order_id=" + ctrl.document.id;
        link.download = "purchase.csv";
        link.click();
        
        //window.location = "PurchaseAction.do?action=exportCsv&c_order_id=" + ctrl.document.id;
    };

    ctrl.exportRecivedItemsCsv = function() {
        window.location = "PurchaseAction.do?action=exportReceivedItemsCsv&c_order_id=" + ctrl.document.id;
    };

    //check if partially received

    var line;

    if (ctrl.document.header.shipped) {

        if (!ctrl.document.header.delivered) {

            for (var i = 0; i < ctrl.document.lines.length; i++) {

                line = ctrl.document.lines[i];

                if (parseInt(line.qtyDelivered) < parseInt(line.qty)) {
                    ctrl.partiallyReceived = true;
                    break;
                }
            }
        } else {
            ctrl.partiallyReceived = false;
        }
    }

    ctrl.markAllAsReceived = function() {

        if (window.confirm("Are you sure you want to mark the order as received?")) {
            ctrl.showModal = true;

            //push data
            var post = {};
            post.purchase_id = ctrl.document.id;
            post.lines = [];
            post.dateReceived = moment().format("DD-MM-YYYY");
            post.referenceNo = ctrl.referenceno;

            post = JSON.stringify(post);

            ctrl.partiallyReceived = false;

            /*window.location = "PurchaseAction.do?action=markAsReceived&post=" + post;*/
            $("#post_action").val("markAsReceived");
            $("#post_data").val(post);
            $("#purchase_form").submit();
        };
    }

});