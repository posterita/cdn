var app = angular.module("app", ['ui.bootstrap']);
app.controller("ViewShipmentController", function($scope, $http, $timeout, allow_void_shipment, doc) {
    var ctrl = this;

    ctrl.showHeader = true;
    ctrl.showConfirmModal = false;

    ctrl.allow_void_shipment = allow_void_shipment;
    ctrl.document = doc;

    ctrl.editShipment = function() {
        ctrl.showConfirmModal = true;
    };

    ctrl.receiveLater = function() {

        ctrl.showConfirmModal = false;
        ctrl.showModal = true;
        window.location = "PurchaseAction.do?action=receiveItems&m_inout_id=" + ctrl.document.header.m_inout_id + "&c_order_id=" + ctrl.document.header.c_order_id;
    }

    ctrl.receiveLaterManually = function() {

        ctrl.showConfirmModal = false;
        ctrl.showModal = true;
        window.location = "PurchaseAction.do?action=receiveItemsManually&m_inout_id=" + ctrl.document.header.m_inout_id + "&c_order_id=" + ctrl.document.header.c_order_id;
    }

    ctrl.voidShipment = function() {

        if (window.confirm("Do you want to void shipment?")) {
            ctrl.showModal = true;
            window.location = "PurchaseAction.do?action=voidShipment&m_inout_id=" + ctrl.document.header.m_inout_id;
        }

    };

    ctrl.exportShipmentCSV = function() {

        window.location = "PurchaseAction.do?action=exportShipmentCsv&m_inout_id=" + ctrl.document.header.m_inout_id;
    };

    ctrl.newPurchase = function() {
        window.location = "PurchaseAction.do?action=loadPurchase";
    };


    ctrl.print = function() {

        var configuration = PrinterManager.getPrinterConfiguration();

        var LINE_WIDTH = configuration.LINE_WIDTH;
        var LINE_SEPARATOR = JSReceiptUtils.replicate('-', LINE_WIDTH);

        var date = moment().format('DD MMM YYYY, HH:mm');

        var printFormat = [
            ['CENTER'],
            ['N', LINE_SEPARATOR],
            ["H1", "Shipment"],
            ['N', LINE_SEPARATOR],
            ["H3", "Receiver: " + ctrl.document.header.store],
            ['N', LINE_SEPARATOR],
            ['N', JSReceiptUtils.format("Doc No:", LINE_WIDTH - 20) + JSReceiptUtils.format(ctrl.document.header.shipment, 20)],
            ['N', JSReceiptUtils.format("Purchase Doc No:", LINE_WIDTH - 20) + JSReceiptUtils.format(ctrl.document.header.purchase, 20)],
            ['N', JSReceiptUtils.format("Reference:", LINE_WIDTH - 20) + JSReceiptUtils.format(ctrl.document.header.reference, 20)],
            ["N", JSReceiptUtils.format("Supplier: ", LINE_WIDTH - 20) + JSReceiptUtils.format(ctrl.document.header.supplier, 20)],
            ["N", JSReceiptUtils.format("Date Received: ", LINE_WIDTH - 20) + JSReceiptUtils.format(ctrl.document.header.movementdate, 20)],
            ["N", JSReceiptUtils.format("Sales Rep: ", LINE_WIDTH - 20) + JSReceiptUtils.format(ctrl.document.header.salesrep, 20)],
            ["N", JSReceiptUtils.format("Doc Status: ", LINE_WIDTH - 20) + JSReceiptUtils.format(ctrl.document.header.docstatus_name, 20)],
            ['N', LINE_SEPARATOR],
            ["B", JSReceiptUtils.format("Name ", LINE_WIDTH - 20) + JSReceiptUtils.format("Qty", 20, true)],
            ['N', LINE_SEPARATOR]
        ];

        var line;
        var currentBoxNumber;
        var total = 0;

        var boxes = [];
        var boxIndex = -1;

        for (var i = 0; i < ctrl.document.lines.length; i++) {

            line = ctrl.document.lines[i];

            /* box feature */
            if (line.box != currentBoxNumber) {
                currentBoxNumber = line.box;
                boxIndex = boxIndex + 1;

                boxes[boxIndex] = {
                    name: line.box,
                    qty: 0,
                    lines: []
                };
            }

            boxes[boxIndex].lines.push(line);
            boxes[boxIndex].qty += line.movementqty;
        }

        var box, line;
        for (var i = 0; i < boxes.length; i++) {
            box = boxes[i];

            printFormat.push(["FEED"]);
            printFormat.push(["B", JSReceiptUtils.format("Box/Bag: " + box.name, LINE_WIDTH - 10) + JSReceiptUtils.format(box.qty, 10, true)]);
            printFormat.push(['B', LINE_SEPARATOR]);

            for (var j = 0; j < box.lines.length; j++) {
                line = box.lines[j];
                total = total + line.movementqty;

                printFormat.push(["N", JSReceiptUtils.format(line.name, LINE_WIDTH - 10) + JSReceiptUtils.format(line.movementqty, 10, true)]);
                printFormat.push(["N", JSReceiptUtils.format(line.description, LINE_WIDTH)]);
            }
        }
        
        var taxAmt = Number(ctrl.document.header.taxamt).toFixed(2);
        var valueReceived =  Number(ctrl.document.header.valuereceived).toFixed(2)
        var subTotal = valueReceived - taxAmt;
        
        printFormat.push(['FEED']);
        printFormat.push(['B', LINE_SEPARATOR]);
        printFormat.push(['B', JSReceiptUtils.format("Tax Amt", LINE_WIDTH - 10) + JSReceiptUtils.format( taxAmt, 10, true)]);
        
        printFormat.push(['B', JSReceiptUtils.format("Sub Total", LINE_WIDTH - 10) + JSReceiptUtils.format( Number(subTotal).toFixed(2), 10, true)]);
       
        printFormat.push(['B', LINE_SEPARATOR]);
        printFormat.push(['B', JSReceiptUtils.format("Value Received", LINE_WIDTH - 10) + JSReceiptUtils.format(valueReceived, 10, true)]);
        printFormat.push(['B', LINE_SEPARATOR]);

        printFormat.push(['FEED']);
        printFormat.push(['B', LINE_SEPARATOR]);
        printFormat.push(['B', JSReceiptUtils.format("Total", LINE_WIDTH - 10) + JSReceiptUtils.format(total, 10, true)]);
        printFormat.push(['B', LINE_SEPARATOR]);

        printFormat.push(['FEED']);
        printFormat.push(['N', JSReceiptUtils.format('Total No. of Boxes/Bags:........................................................', LINE_WIDTH)]);
        printFormat.push(['FEED']);
        printFormat.push(['N', JSReceiptUtils.format('Received By:........................................................................', LINE_WIDTH)]);
        printFormat.push(['FEED']);
        printFormat.push(['N', JSReceiptUtils.format('Date:........................................................................', LINE_WIDTH)]);
        
        printFormat.push(['FEED']);
        printFormat.push(['N', JSReceiptUtils.format('Authorized by:.....................................................................', LINE_WIDTH)]);

        printFormat.push(['FEED']);
        printFormat.push(['FEED']);

        printFormat.push(['PAPER_CUT']);

        PrinterManager.print(printFormat);
    }

    ctrl.cancel = function() {

        window.location = "report-shipments.do#shipment_no="+ctrl.document.header.shipment;
    }

});