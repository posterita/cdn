var RecordManager = {};
RecordManager.deleteRecord = function(table, recordId){
	var win = new ModalWindow('Do you want to delete this record?', true);
	var deleteCommFunc = function(e){
		var url = 'RecordAction.do?action=deleteRecord';
		var pars = 'table=' + table + '&recordId=' + recordId;
		
		var myAjax = new Ajax.Request(url, 
				{ 
					method: 'POST', 
					parameters: pars, 
					onSuccess: RecordManager.confirmDelete
		});
	};
	win.setClickOk(deleteCommFunc);
	win.show();
};



RecordManager.confirmDelete = function(request)
{
	var response = request.responseText;
	var json = eval('('+response+')');
	var success = json.success;
	var msg = json.msg;
	var recordId = json.recordId;
	if (!success)
	{
		alert(json.msg);
	}
	else
	{
		RecordManager.deleteConfirmed(json.msg);
	}
};

RecordManager.deleteSuccess = function(request)
{
	var response = request.responseText;
	var json = eval('('+response+')');
	var msg = json.msg;
	
	RecordManager.deleteConfirmed(json.msg);
};

RecordManager.deleteConfirmed = function(msg)
{
	alert(msg);
	
	/* code from genericReport.js line 21 */
	initSystemBtns();
	$('reportType').value = 'html';
	$('isAjaxReport').value = 'true';
	this.className = 'selected';
	$('GenericReportForm').request({
		onComplete: GenericReportAjax.displayData
	});
};