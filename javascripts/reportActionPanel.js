var ActionButton = Class.create({
	
	initialize : function(id, url)
	{
		this.id = id;
		this.url = url;
		this.popUpPanel = null;
		
		this._actionBtn = $(id);
		this.setAction(this.url);
	},
	
	getURL : function()
	{
		return this.url;
	},
	
	setAction : function(action)
	{
		if (action != null)
		{
			if (this._actionBtn != null)
				this._actionBtn.href = action;
		}
		else
		{
			this.hide();
		}
	},
	
	setPopUp : function(popUpPanel)
	{
		this.popUpPanel = popUpPanel
		if (this.popUpPanel != null)
		{
			this._actionBtn.removeAttribute('href');
			this._actionBtn.onclick = this.showPopUp.bind(this);
		}
	},
	
	showPopUp : function()
	{
		this.popUpPanel.show();
	},
	
	getPopUp : function()
	{
		return this.popUpPanel;
	},
	
	hide : function()
	{
		if (this._actionBtn != null)
			this._actionBtn.style.display = 'none';
	},
	
	show : function()
	{
		if (this._actionBtn != null)
			this._actionBtn.style.display = 'inline';
	},
	
	getId : function()
	{
		return this.id;
	},
	
	equals : function(actionBtn)
	{
		return (this.getId() == actionBtn.getId());
	}

});

var ActionPanel = Class.create({
	
	initialize : function()
	{
		this.defaultActionBtns = new ArrayList(new ActionButton());
		this.customActionBtns = new ArrayList(new ActionButton());
		this.jsCustomActionBtns = new ArrayList(new ActionButton());
		this.customPopUpActionBtns = new ArrayList(new ActionButton());
		
		this.onRefreshListeners = new ArrayList();
		this.onClickCSVListeners = new ArrayList();
		this.onClickPDFListeners = new ArrayList();
		
		
		this.csvBtn = $('csvBtn');
		this.pdfBtn = $('pdfBtn');
		
		this.hash = new Hash();
		
		this.init();
	},
	
	init : function()
	{
		this.refreshBtn = $('refreshBtn');
		if (this.refreshBtn != null)
			this.refreshBtn.onclick = this.fireOnRefresh.bind(this);
	},
	
	initCSVPDF : function()
	{
		this.csvBtn = $('csvBtn');
		this.pdfBtn = $('pdfBtn');
		
		this.csvBtn.onclick = this.fireOnClickCSV.bind(this);
		this.pdfBtn.onclick = this.fireOnClickPDF.bind(this);
	},
	
	fireOnRefresh : function()
	{
		for (var i=0; i<this.onRefreshListeners.size(); i++)
		{
			var listener = this.onRefreshListeners.get(i);
			listener.refreshBtnClicked();
		}
	},
	
	fireOnClickCSV : function()
	{
		for (var i=0; i<this.onClickCSVListeners.size(); i++)
		{
			var listener = this.onClickCSVListeners.get(i);
			listener.csvBtnClicked();
		}
	},
	
	fireOnClickPDF : function()
	{
		for (var i=0; i<this.onClickPDFListeners.size(); i++)
		{
			var listener = this.onClickPDFListeners.get(i);
			listener.pdfBtnClicked();
		}
	},
	
	addDefaultAction : function(id, url)
	{
		var actionButton = new ActionButton(id, url);
		this.defaultActionBtns.add(actionButton);
		
		this.addBtn(id, actionButton);
	},
	
	addBtn : function(id, actionButton)
	{
		this.hash.set(id, actionButton);
	},
	
	addJSCustomAction : function(id, url)
	{
		var actionButton = new ActionButton(id, url);
		actionButton.hide();
		this.jsCustomActionBtns.add(actionButton);
		
		this.addBtn(id, actionButton);
	},
	
	addCustomPopUpAction : function(id, popUpPanel)
	{
		var actionButton = new ActionButton(id, null);
		actionButton.hide();
		
		actionButton.setPopUp(popUpPanel);
		
		this.customPopUpActionBtns.add(actionButton);
		
		this.addBtn(id, actionButton);
	},
	
	addCustomAction : function(id, url)
	{
		var actionButton = new ActionButton(id, url);
		actionButton.hide();
		this.customActionBtns.add(actionButton);
		
		this.addBtn(id, actionButton);
	},
	
	addCommissionPopUp : function(id, commissionSelect, processId)
	{
		var commissionRunPopUp = new CommissionRunPopUp(this, commissionSelect, processId);
		
		var actionButton = new ActionButton(id, commissionSelect);
		actionButton.setPopUp(commissionRunPopUp);
		
		if (commissionSelect != null && commissionSelect != 'null' && commissionSelect != '')
		{
			actionButton.show();
		}
	},
	
	addOnRefreshListener : function(listener)
	{
		this.onRefreshListeners.add(listener);
	},
	
	addOnClickCSVListener : function(listener)
	{
		this.onClickCSVListeners.add(listener);
	},
	
	addOnClickPDFListener : function(listener)
	{
		this.onClickPDFListeners.add(listener);
	},
	
	updateCustomActions : function(recordId)
	{
		for (var i=0; i<this.customActionBtns.size(); i++)
		{
			var actionButton = this.customActionBtns.get(i);
			
			/*Hide edit button for old report*/
			if (actionButton.id == 'editItem')
			{
				actionButton.hide();
				continue;
			}
			
			var url = actionButton.getURL();
			if (url != null)
			{
				url += recordId;
				actionButton.setAction(url);
				actionButton.show();
			}
		}
		
		for (var i=0; i<this.jsCustomActionBtns.size(); i++)
		{
			var actionButton = this.jsCustomActionBtns.get(i);
			var url = actionButton.getURL();
			if (url != null)
			{
				var reg = new RegExp('RECORD_ID', 'g');
				
				url = url.replace(/\@/g, '');
				url = url.replace(reg, recordId);
				actionButton.setAction(url);
				actionButton.show();
			}
		}
		
		for (var i=0; i<this.customPopUpActionBtns.size(); i++)
		{
			var actionButton = this.customPopUpActionBtns.get(i);
			var popUp = actionButton.getPopUp();
			if (popUp != null && popUp.getURL() != null)
			{
				popUp.setRecordId(recordId);
				actionButton.show();
			}
		}
		
	},
	
	setDisplayLogic : function(displayLogic)
	{
		this.displayLogic = displayLogic; 
	},
	
	updateDisplay : function(gridRow)
	{
		for (var btnId in this.displayLogic)
		{
			if (btnId != 'binaries')
			{
				var acb = this.hash.get(btnId);
				acb.hide();
				var logic = false;
				
				var logicArray = this.displayLogic[btnId];
				for (var i=0; i<logicArray.length; i++)
				{
					var logicAnd = true;
					var json = logicArray[i];
					for (var columnName in json)
					{
						var valReport = gridRow.getValueOfColumn(columnName);
						logicAnd = logicAnd && (valReport == json[columnName]);
					}
					
					logic = logic || logicAnd;
				}
				
				if (logic)
				{
					/*Hide edit button for old report*/
					if (actionButton.id != 'editItem')
					{
						acb.show();
					}
					
				}
			}
			else
			{
				var binaries = this.displayLogic.binaries;
				
				for (var btn in binaries)
				{
					var logic = true;
					
					var logicArray = binaries[btn];
					for (var i=0; i<logicArray.length; i++)
					{
						var columnName = logicArray[i];
						var valReport = gridRow.getValueOfColumn(columnName);
						logic = logic && (valReport != null);
					}
					var acb = this.hash.get(btn);
					
					if (logic)
					{
						acb.show();
					}
					else
					{
						acb.hide();
					}
				}
			}
		}
				
	}
});

var CommissionRunPopUp = Class.create(PopUpBase, {
	initialize : function(actionPanel, commissionSelect, processId)
	{
		this.actionPanel = actionPanel;
		
		this.createPopUp($('commission.run.panel'));
		
		this.okBtn = $('commission.run.okBtn');
	    this.cancelBtn = $('commission.run.cancelBtn');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	   
	    $('selComm').innerHTML = commissionSelect;
	    $('GenerateCommissionForm').processId.value = processId;
	},
	
	okHandler : function()
	{
		this.hide();
		$('GenerateCommissionForm').request({onComplete:this.loadReport.bind(this)});
	},
	
	loadReport : function()
	{
		this.actionPanel.fireOnRefresh();
	}
});

var ReportActionPopUp = Class.create(PopUpBase, {
	
	initialize : function(url)
	{
		this.url = url;
		this.recordId = 0;
	},
	
	getURL : function()
	{
		return this.url;
	},
	
	setRecordId : function(recordId)
	{
		this.recordId = recordId;
	}
});


var DeleteEventNotifier = Class.create(ReportActionPopUp,{
	
	initialize : function($super, url)
	{
		$super(url);
		
		this.createPopUp($('delete.event.notifier.panel'));
		this.okBtn = $('delete.event.notifier.panel.okBtn');
		this.cancelBtn = $('delete.event.notifier.panel.cancelBtn');
		
		this.okBtn.onclick = this.okHandler.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	okHandler : function()
	{
		this.hide();
		var action = this.url + this.recordId;
		
		try
		{	
			var myAjax = new Ajax.Request(action, 
			{ 
				method: 'post', 
				onSuccess: this.afterDelete.bind(this)				
			});	
		}
		catch(e)
		{} 
	},
	
	afterDelete : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		alert(json.msg);
	}
});

var DeleteProduct = Class.create(ReportActionPopUp,{
	
	initialize : function($super, url)
	{
		$super(url);
		
		this.createPopUp($('delete.product.panel'));
		this.okBtn = $('delete.product.panel.okBtn');
		this.cancelBtn = $('delete.product.panel.cancelBtn');
		
		this.okBtn.onclick = this.okHandler.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	okHandler : function()
	{
		this.hide();
		var action = this.url + this.recordId;
		
		try
		{	
			var myAjax = new Ajax.Request(action, 
			{ 
				method: 'post', 
				onSuccess: this.afterDelete.bind(this)				
			});	
		}
		catch(e)
		{} 
	},
	
	afterDelete : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		if (json.productTransactionsFound)
		{
			alert("Can't delete product. This product has been used in transactions.");
		}
		else
		{
			alert('Product deleted');
		}
	}
});

/*Date Picker Panel*/
var DatePickerPanel =  Class.create(ReportActionPopUp, {
	initialize: function($super, url) 
	{
		$super(url);
	    this.createPopUp($('date-picker-popup-panel'));
	    
     	this.fromDateTextField = $('date-from-textfield');
	    this.fromDateButton = $('date-from-button');

	    this.toDateTextField = $('date-to-textfield');
	    this.toDateButton = $('date-to-button');
	
	    this.okBtn = $('date-picker-popup-ok-button');
	    this.cancelBtn = $('date-picker-popup-cancel-button');

	    this.cancelBtn.onclick = this.cancelHandler.bind(this); 
	    this.okBtn.onclick = this.okHandler.bind(this);
	    

		Calendar.setup({ 
   	   			inputField : 'date-from-textfield', 
   	   			ifFormat : '%Y-%m-%d',
   	   			showTime : true,
   	   			button : 'date-from-button', 
   	   			onUpdate : this.setDate.bind(this) });

		Calendar.setup({ 
   			inputField : 'date-to-textfield', 
   			ifFormat : '%Y-%m-%d',
   			showTime : true,
   			button : 'date-to-button', 
   			onUpdate :  this.setDate.bind(this)});
	},
	
	setDate :  function(){

	},

	cancelHandler : function(){
		this.hide();
	},
	
	okHandler : function(){
		this.hide();
		var statementOfAccount = new StatementOfAccountPanel();
		statementOfAccount.init(this.recordId, this.fromDateTextField.value, this.toDateTextField.value);
		statementOfAccount.show();
	},
	
	setRecordId : function(recordId)
	{
		this.recordId = recordId;
	}

});

/*ExportStatmentOfAccount Panel*/
var ExportStatementOfAccountPopUp =  Class.create(ReportActionPopUp, {
	initialize: function($super, url) 
	{
		$super(url);
	    this.createPopUp($('stmt-date-picker-popup-panel'));
	    
     	    this.fromDateTextField = $('stmt-date-from-textfield');
	    this.fromDateButton = $('stmt-date-from-button');

	    this.toDateTextField = $('stmt-date-to-textfield');
	    this.toDateButton = $('stmt-date-to-button');
	
	    this.okBtn = $('stmt-date-picker-popup-ok-button');
	    this.cancelBtn = $('stmt-date-picker-popup-cancel-button');

	    this.cancelBtn.onclick = this.cancelHandler.bind(this); 
	    this.okBtn.onclick = this.okHandler.bind(this);
	    

		Calendar.setup({ 
   	   			inputField : 'stmt-date-from-textfield', 
   	   			ifFormat : '%Y-%m-%d',
   	   			showTime : true,
   	   			button : 'stmt-date-from-button', 
   	   			onUpdate : this.setDate.bind(this) });

		Calendar.setup({ 
   			inputField : 'stmt-date-to-textfield', 
   			ifFormat : '%Y-%m-%d',
   			showTime : true,
   			button : 'stmt-date-to-button', 
   			onUpdate :  this.setDate.bind(this)});
	},
	
	setDate :  function(){

	},

	cancelHandler : function(){
		this.hide();
	},
	
	okHandler : function(){
		this.hide();
		var url = 'BPartnerAction.do?action=exportStatementOfAccount&customerId=' + this.recordId + '&dateFrom=' + this.fromDateTextField.value + '&dateTo=' +this.toDateTextField.value;
		window.location = url;
	}

});

var DeleteCommission = Class.create(ReportActionPopUp,{
	initialize : function($super, url)
	{
		$super(url);
		
		this.createPopUp($('delete.commission.settings.panel'));
		this.okBtn = $('delete.commission.settings.panel.okBtn');
		this.cancelBtn = $('delete.commission.settings.panel.cancelBtn');
		
		this.okBtn.onclick = this.okHandler.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	okHandler : function()
	{
		this.hide();
		var action = this.url + this.recordId + '&force=false';
		
		try
		{	
			var myAjax = new Ajax.Request(action, 
			{ 
				method: 'post', 
				onSuccess: this.afterDelete.bind(this)				
			});	
			
		}
		catch(e)
		{} 
	},
	
	afterDelete : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		if (json.commissionRunsFound)
		{
			if (!confirm('Pay slips have been generated for this commission. \nDeleting this commission will delete the pay slips as well. \nDelete anyway?')) return;
			
			var action = this.url + this.recordId + '&force=true';
			try
			{	
				var myAjax = new Ajax.Request(action, 
				{ 
					method: 'post', 
					onSuccess: this.afterDelete.bind(this)				
				});	
				
			}
			catch(e)
			{} 
		}
		else
		{
			alert('Commission deleted');
		}
	}
});

var DeleteCommRun = Class.create(ReportActionPopUp, {
	
	initialize : function($super, url)
	{
		$super(url);
		
		this.createPopUp($('delete.commission.run.panel'));
		this.okBtn = $('delete.commission.run.panel.okBtn');
		this.cancelBtn = $('delete.commission.run.panel.cancelBtn');
		
		this.okBtn.onclick = this.okHandler.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
	},
	
	okHandler : function()
	{
		this.hide();
		var action = this.url + this.recordId;
		
		try
		{	
			var myAjax = new Ajax.Request(action, 
			{ 
				method: 'post', 
				onSuccess: this.afterDelete.bind(this)				
			});	
			
		}
		catch(e)
		{} 
	},
	
	afterDelete : function(response)
	{
		var json = eval('(' + response.responseText + ')');
		alert(json.successMsg);
	}
});


function sendToImporter(importerName)
{
	$('reportType').value = 'Import';
	$('importerName').value = importerName;
	$('GenericReportForm').submit();
}
