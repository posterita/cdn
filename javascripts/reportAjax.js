/******************************************************************************
 *  Product: Posterita Web-Based POS and Adempiere Plugin                     *
 *  Copyright (C) 2008  Posterita Ltd                                         *
 *  This file is part of POSterita                                            *
 *                                                                            *
 *  POSterita is free software; you can redistribute it and/or modify         *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program; if not, write to the Free Software Foundation,   *
 *  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.         *
 *****************************************************************************/
 
/******************************************************************************
 * Generic Report Ajax (Function to Ajaxify the Generic Report  )              *
 * @author shameem  (Email: shameem.peerbuccoss@gmail.com)                    *
 *****************************************************************************/

var ReportAjax = Class.create({
	
		initialize : function()
		{
			this.onLoadListeners = new ArrayList();
		},
		
		addOnLoadListener : function(listener)
		{
			this.onLoadListeners.add(listener);
		},
		
		fireOnLoadEvent : function(reportHTML)
		{
			for (var i=0; i<this.onLoadListeners.size(); i++)
			{
				var listener = this.onLoadListeners.get(i);
				listener.reportLoaded(reportHTML);
			}
		},
		
		getReportData : function(params)
		{	
			/*push form state before submit*/
			HASH_CHANGE = false
			window.location.hash = params;
			
			try
			{	
				var myAjax = new Ajax.Request('ReportAction.do?action=generateReportOutput', 
				{ 
					method: 'post', 
					parameters: params,
					onSuccess: this.displayData.bind(this),
					onFailure: this.reportError.bind(this)
					
				});	
				
			}
			catch(e)
			{
				showErrorMessage(e, 'grid');
			} 
		},
		
		displayData : function(request)
		{
			var response = request.responseText;
			$('indicator').style.display = 'none'; 
			
			try
			{		
				var result = eval('(' + response + ')');
				
				if(result.logout)
				{
					window.location = result.logout;
				}
				else if(result.error)
				{
					if(typeof(result.error) == 'object' || typeof(result.error) == 'string')
					{
						showErrorMessage(result.error, 'grid');
					}
					else
					{				
						showErrorMessage(result, 'grid');
					}
				}
				else
				{ 		
		 			this.fireOnLoadEvent(reponse);
		 		}
			}
			catch(e) 
			{
				this.fireOnLoadEvent(response)
			}
		},
		
		reportError : function(request)
		{
			alert(messages.get('js.report.error.msg'));
			alert(request.responseText);
			var win = window.open();
			win.document.write(request.responseText);
			win.document.close();
		},
		
		reloadReport : function()
		{
			$('GenericReportForm').reportType.value = 'html';
			$('GenericReportForm').isAjaxReport.value='true';
			
			/*push form state before submit*/
			var params = $('GenericReportForm').serialize();
			
			var oldHash = window.location.hash;
			var p = oldHash.split('&');
			
			/*parse display-tag parameters*/
			var displaytag_params = '';			
			for(var i=0; i<p.length; i++)
			{
				if(p[i].startsWith('d-')){					
					displaytag_params += ('&' + p[i]);
				}
			}
			
			window.location.hash = params;
			
			params = params + displaytag_params;
			
			/*
			$('GenericReportForm').request({
				onComplete: this.displayData.bind(this)
			});
			*/
			
			try
			{	
				var myAjax = new Ajax.Request('ReportAction.do?action=generateReportOutput', 
				{ 
					method: 'post', 
					parameters: params,
					onSuccess: this.displayData.bind(this),
					onFailure: this.reportError.bind(this)
					
				});	
				
			}
			catch(e)
			{
				showErrorMessage(e, 'grid');
			} 
		},
		
		showReport : function(reportConfigId)
		{
			$('GenericReportForm').reportConfigId.value = reportConfigId;
			this.reloadReport();
		},
		
		exportCSV : function()
		{
			$('reportType').value = 'csv';
			$('GenericReportForm').submit();
		},
		
		exportPDF : function()
		{
			$('reportType').value = 'pdf';
			$('GenericReportForm').submit();
		},
		
		openPageNo : function(params)
		{
			this.getReportData(params);
		},
		
		sortHeader : function(params)
		{
			this.getReportData(params);
		}
});