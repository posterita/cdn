var PrintFormat = Class.create({
	
	initialize : function(json)
	{
		if (json != null)
		{
			this.name = json.name;
			this.id = json.id;
			this.availableColumnsSelectId = 'availableColumnsSelect' + this.id;
			this.displayedColumnsSelectId = 'displayedColumnsSelect' + this.id;
			this.availableColumnsSortId = 'availableColumnsSort' + this.id;
			this.selectedColumnsSortId = 'sortColumns' + this.id;
			
			this.columnsId = 'columns';
			
			if (json.availableItems != null && json.displayedItems != null)
			{
				this.availableItems = json.availableItems;
				this.displayedItems = json.displayedItems;
			
				this.availSelectComp = this.getAvailableColumns();
				this.dispSelectComp = this.getDisplayedColumns();
				
				this.printFormatItemSort = new PrintFormatItemSort(this.availableItems, this.displayedItems, 
				this.availableColumnsSortId, this.selectedColumnsSortId);
				
				this.availSortComp = this.printFormatItemSort.availElement;
				this.selectedSortComp = this.printFormatItemSort.sortElement;
				
				this.displayedColumnsInput = document.createElement('input');
				this.displayedColumnsInput.setAttribute('type', 'hidden');
				this.displayedColumnsInput.setAttribute('name', 'displayedColumnsInput' + this.id);
				this.displayedColumnsInput.setAttribute('id', 'displayedColumnsInput' + this.id);
				
				this.availableColumnsInput = document.createElement('input');
				this.availableColumnsInput.setAttribute('type', 'hidden');
				this.availableColumnsInput.setAttribute('name', 'availableColumnsInput' + this.id);
				this.availableColumnsInput.setAttribute('id', 'availableColumnsInput' + this.id);
				
				this.availableSortColumnsInput = document.createElement('input');
				this.availableSortColumnsInput.setAttribute('type', 'hidden');
				this.availableSortColumnsInput.setAttribute('name', 'availableSortColumnsInput' + this.id);
				this.availableSortColumnsInput.setAttribute('id', 'availableSortColumnsInput' + this.id);
				
				this.displayedSortColumnsInput = document.createElement('input');
				this.displayedSortColumnsInput.setAttribute('type', 'hidden');
				this.displayedSortColumnsInput.setAttribute('name', 'displayedSortColumnsInput' + this.id);
				this.displayedSortColumnsInput.setAttribute('id', 'displayedSortColumnsInput' + this.id);
				
				this.transfer = null;
				this.sortTransfer = null;
			}
		}
	},
	
	getAvailableColumns : function()
	{
		var availSelect = new PrintFormatItemSelect(this.availableItems, this.availableColumnsSelectId);
		return availSelect.getElement();
	},
	
	getDisplayedColumns : function()
	{
		var dispSelect = new PrintFormatItemSelect(this.displayedItems, this.displayedColumnsSelectId);
		return dispSelect.getElement();
	},
	
	getAvailableSortColumns : function()
	{
		return this.availSortComp;
	},
	
	getDisplayedSortColumns : function()
	{
		return this.selectedSortComp;
	},
	
	initTransfer : function()
	{
		this.transfer = new Transfer(this.availableColumnsSelectId, this.displayedColumnsSelectId, 
				this.availableColumnsInput.getAttribute('id'), this.displayedColumnsInput.getAttribute('id'));
	    this.transfer.init();
	    this.transfer.updateInputs();
	    
	    this.sortTransfer = new Transfer(this.availableColumnsSortId, this.selectedColumnsSortId,
	    		this.availableSortColumnsInput.getAttribute('id'), this.displayedSortColumnsInput.getAttribute('id'));
	    this.sortTransfer.initBtns('toRightEachSort', 'toLeftEachSort', 'toRightAllSort', 'toLeftAllSort', 'upSort', 'downSort');
	    this.sortTransfer.init();
	    this.sortTransfer.updateInputs();
	},
	
	setItems : function()
	{
		$(this.columnsId).innerHTML = '';
		$(this.columnsId).appendChild(this.availableColumnsInput);
		$(this.columnsId).appendChild(this.displayedColumnsInput);
		$(this.columnsId).appendChild(this.availableSortColumnsInput);
		$(this.columnsId).appendChild(this.displayedSortColumnsInput);
	}
});

var PrintFormatItemSort = Class.create({
	
	initialize : function(availableItemsJSON, displayedItemsJSON, availId, dispId)
	{
		this.availElement = null;
		this.sortElement = null;
		this.build(availableItemsJSON, displayedItemsJSON, availId, dispId);
	},
	
	build : function(availableItemsJSON, displayedItemsJSON, availId, dispId)
	{
		var unsortedList = new ArrayList();
		var sortedList = new ArrayList();
		for (var i=0; i<availableItemsJSON.length; i++)
		{
			var item = availableItemsJSON[i];
			var name = item.name;
			var id = item.id;
			var sortNo = item.sortNo;
			if (sortNo == 0)
			{
				unsortedList.add(item);
			}
			else
			{
				sortedList.add(item);
			}
		}
		for (var j=0; j<displayedItemsJSON.length; j++)
		{
			var item = displayedItemsJSON[j];
			if (item != null)
			{
				var name = item.name;
				var id = item.id;
				var sortNo = item.sortNo;
				if (sortNo == 0)
				{
					unsortedList.add(item);
				}
				else
				{
					sortedList.add(item);
				}
			}
		}
		sortedList.sort(function(a,b){return a.sortNo - b.sortNo});
		
		
		this.availElement = document.createElement("select");
		this.availElement.setAttribute("id",  availId);
		this.availElement.setAttribute("class", "printFormatItems");
		this.availElement.setAttribute("multiple", "multiple");
		this.availElement.setAttribute("size", 10);
		for (var i=0; i<unsortedList.size(); i++)
		{
			var item = unsortedList.get(i);
			var name = item.name;
			var id = item.id;
			var option = document.createElement('option');
			option.setAttribute('name', id);
			option.setAttribute('value', id);
			option.innerHTML = name;
			
			this.availElement.appendChild(option);
		}
		
		this.sortElement = document.createElement("select");
		this.sortElement.setAttribute("id",  dispId);
		this.sortElement.setAttribute("class", "printFormatItems");
		this.sortElement.setAttribute("multiple", "multiple");
		this.sortElement.setAttribute("size", 10);
		for (var i=0; i<sortedList.size(); i++)
		{
			var item = sortedList.get(i);
			var name = item.name;
			var id = item.id;
			var option = document.createElement('option');
			option.setAttribute('name', id);
			option.setAttribute('value', id);
			option.innerHTML = name;
			
			this.sortElement.appendChild(option);
		}
	}
});

var PrintFormatItemSelect = Class.create({
	
	initialize : function(itemsJSON, id)
	{
		this.element = this.build(itemsJSON, id);
	},
	
	build : function(itemsJSON, id)
	{
		var select = document.createElement("select");
		select.setAttribute("id", id);
		select.setAttribute("class", "printFormatItems");
		select.setAttribute("multiple", "multiple");
		select.setAttribute("size", 10);
		for (var i=0; i<itemsJSON.length; i++)
		{
			var item = itemsJSON[i];
			var name = item.name;
			var id = item.id;
			var option = document.createElement('option');
			option.setAttribute('name', id);
			option.setAttribute('value', id);
			option.innerHTML = name;
			
			select.appendChild(option);
		}
		return select;
	},
	
	getElement : function()
	{
		return this.element;
	}
});

var ReportConfig = Class.create({

	initialize : function(json)
	{
		if (json != null)
		{
			this.name = null;
			this.reportConfigId = null;
			this.parentReportConfigId = null;
			this.processId = null;
			this.system = null;
			this.isDefault = false;
			this.printFormat = null;
			this.parameters = null;
			this._element = null;
			this.reportConfigManager = null;
			this.reportConfigPanel = null;
			this.displayElementFactory = new DisplayElementFactory();
			this.parameterList = null;
			this.reset = false;
			this.init(json);
		}
	},
	
	init : function(json)
	{
		this.name = json.name;
		this.reportConfigId = json.reportConfigId;
		this.parentReportConfigId = json.parentReportConfigId;
		this.processId = json.processId;
		this.isDefault = json.isDefault;
		
		this.system = (this.parentReportConfigId != null && this.parentReportConfigId != 0);
		
		if (json.printFormat != null)
		{
			this.printFormat = new PrintFormat(json.printFormat);
		}
		
		this.parameterList = json.parameterList;
		
		this.parameters = null;
		this.initParameters();
		this._element = this.initElement();
		
		this.reportConfigPanel = new ReportConfigPanel(this);
	},
	
	initParameters : function()
	{
		this.parameters = new ArrayList();
		if (this.parameterList != null)
		{
			for (var i=0; i<this.parameterList.length; i++)
			{
				var reportParameterJSON = this.parameterList[i];
				var json = eval('('+reportParameterJSON.json+')');
				
				var wElement = this.displayElementFactory.getWElement(json);
				this.parameters.add(wElement);
			}
		}
	},

	initElement : function()
	{
		var element = document.createElement('option');
		if (this.isSystem())
		{
			element.setAttribute("class", "reportConfigOption system_report_view");
		}
		else
		{
			element.setAttribute("class", "reportConfigOption");
		}
		
		element.setAttribute('id', this.reportConfigId);
		element.setAttribute('value', this.reportConfigId);
		element.innerHTML = this.name;
		return element;
	},
	
	onSave : function(reportConfigJSON)
	{
		this.reset = true;
		this.init(reportConfigJSON);
		this.reportConfigManager.onSave(this);
	},
	
	getElement : function()
	{
		return this._element;
	},

	select : function()
	{
		this._element.setAttribute('selected', 'selected');
		
		if (this.isDefault)
		{
			this.showParamValues();
			this.showSortValues();
			
			if (this.reportConfigManager != null)
			{
				//this.reportConfigManager.dynatreeReset=true;
				this.reportConfigManager.reportConfigFetched(this);
			}
		}
		else
		{
			
			var url = "ReportConfigAction.do";
			
			var param = new Hash();
			param.set('action', 'loadDetails');			
			param.set('reportConfigId',  this.reportConfigId);
			param = param.toQueryString();
			
			var myAjax = new Ajax.Request(url, { 
					method: 'get', 
					parameters: param, 
					onSuccess: this.loadDetails.bind(this)
			});
		}
	},
	
	loadDetails : function(response)
	{
		var json = response.responseText.evalJSON(true);
		
		if (json.printFormat != null)
		{
			this.printFormat = new PrintFormat(json.printFormat);
		}
		
		this.parameterList = json.parameterList;
		this.initParameters();
		
		this.showParamValues();
		this.showSortValues();
		
		if (this.reportConfigManager != null)
		{
			//this.reportConfigManager.dynatreeReset=true;
			this.reportConfigManager.reportConfigFetched(this);
		}
		
		this.reset = false;
	},
	
	showParamValues : function()
	{
		$('param-values').innerHTML = '';
		
		var numberOfNonNulls = 0;
		for (var i=0; i<this.parameters.size(); i++)
		{
			var wElement = this.parameters.get(i);
			
			var displayValue = this.getDisplayValue(wElement);
			var displayValueTo = this.getDisplayValueTo(wElement);
			
			if ((displayValue != null && String(displayValue).trim().length > 0) ||
					(displayValue != null && String(displayValue).trim().length > 0))
				numberOfNonNulls++;
		}
		
		var count=0;
		for (var i=0; i<this.parameters.size(); i++)
		{
			var wElement = this.parameters.get(i);
			var label = wElement.label;
			var range = wElement.range;
			
			var displayValue = this.getDisplayValue(wElement);
			var displayValueTo = this.getDisplayValueTo(wElement);
			
			if ((displayValue != null && String(displayValue).trim().length > 0) ||
					(displayValue != null && String(displayValue).trim().length > 0))
			{
				count++;
				var divLabel = document.createElement('div')
				divLabel.setAttribute('class', 'parameter-label');
				divLabel.innerHTML = label + ":";
				
				var divValue = document.createElement('div');
				divValue.setAttribute('class', 'parameter-value');
				
				var text = "";
				
				if (range)
				{
					text += displayValue + " To ";
					if (displayValueTo != null)
						text += displayValueTo;
				}
				else
				{
					text += displayValue;
				}
				
				if (count < numberOfNonNulls)
					text += ",";
				
				divValue.innerHTML = text;
				/*$('param-values').appendChild(divLabel);*/
				$('param-values').appendChild(divValue);
			}
		}
	},
	
	getDisplayValue : function(wElement)
	{
		var displayValue = wElement.displayValue;
		
		if (this.reset)
			return displayValue;
		
		if (wElement.range)
		{
			var fromDispId = wElement.from.displayElementId;
			
			if ($(fromDispId) != null)
				displayValue = $(fromDispId).value;
		}
		else
		{
			if (wElement instanceof WSearch)
			{
				if ($(wElement._queryInput.id) != null)
					displayValue = $(wElement._queryInput.id).value;
			}
			else if (wElement instanceof WList)
			{
				if ($(wElement.displayElementId) != null)
					displayValue = $(wElement.displayElementId).options[$(wElement.displayElementId).selectedIndex].text;
			}
			else
			{
				if ($(wElement.displayElementId) != null)
					displayValue = $(wElement.displayElementId).value;
			}
		}
		return displayValue;
	},
	
	getDisplayValueTo : function(wElement)
	{
		var displayValueTo = wElement.displayValueTo;
		if (this.reset)
			return displayValueTo;
		
		if (wElement.range)
		{
			var toDispId = wElement.to.displayElementId;
			
			if ($(toDispId) != null)
				displayValueTo = $(toDispId).value;
		}
		
		return displayValueTo;
	},
	
	showSortValues : function()
	{
		$('sort-values').innerHTML = '';
		
		var displayedItems = this.printFormat.displayedItems;
		
		var sortItems = new ArrayList();
		
		for (var i=0; i<displayedItems.length; i++)
		{
			var item = displayedItems[i];
			if (item.sortNo > 0)
				sortItems.add(item);			
		}
		
		var availableItems = this.printFormat.availableItems;
		for (var i=0; i<availableItems.length; i++)
		{
			var item = availableItems[i];
			if (item.sortNo > 0)
				sortItems.add(item);
		}
		
		sortItems.sort(function compare(a, b){
			if (a.sortNo < b.sortNo)
				return -1;
			if (a.sortNo > b.sortNo)
				return 1;
			return 0;
		});
		
		if (sortItems.size() > 0)
		{
			var divSortLabel = document.createElement('div');
			divSortLabel.setAttribute('class', 'sort-label');
			divSortLabel.innerHTML = 'Sorted By:';
			$('sort-values').appendChild(divSortLabel);
		}
		
		for (var i=0; i<sortItems.size(); i++)
		{
			var item = sortItems.get(i);
			var text = item.name;
			
			if (i < sortItems.size() - 1)
				text += ",";
			
			var divSort = document.createElement('div');
			divSort.setAttribute('class', 'sort-by');
			divSort.innerHTML = text;
			
			$('sort-values').appendChild(divSort);
		}
	},
	
	setReportConfigManager : function(reportConfigManager)
	{
		this.reportConfigManager = reportConfigManager;
	},

	setName : function(name)
	{
		this.name = name;
	},
	
	getName : function()
	{
		return this.name;
	},

	getReportConfigId : function()
	{
		return this.reportConfigId;
	},
	
	isSystem : function()
	{
		return this.system;
	},
	
	getPrintFormat : function()
	{
		return this.printFormat;
	},
	
	getPrintFormatId : function()
	{
		return this.printFormat.id;
	},

	getProcessId : function()
	{
		return this.processId;
	},

	equals : function(reportConfig)
	{
		return (this.getReportConfigId() == reportConfig.getReportConfigId());
	},

	getParameters : function()
	{
		return this.parameters;
	},
	
	getPanel : function()
	{
		return this.reportConfigPanel;
	}

});

var ReportConfigPanel = Class.create(PopUpBase, {

	initialize: function(reportConfig) 
	{
		this.reportConfig = reportConfig;
		this.reportConfigName = this.reportConfig.name;
		this.reportParameters = this.reportConfig.parameters;
		this.printFormat = this.reportConfig.printFormat;
	
		this.createPopUp($('reportConfigPanel'));
		this.parameterPanel = $('parameterPanel');
		this.reportConfigNameInput = $('reportConfigLabel');
		this.availableColumnsPanel = $('availableColumnsPanel');
		this.displayedColumnsPanel = $('displayedColumnsPanel');
		this.availableSortColumnsPanel = $('availableSortColumnsPanel');
		this.displayedSortColumnsPanel = $('displayedSortColumnsPanel');
		
		$('parameters').className = 'reportview-tab-button active';
		$('display').className = 'reportview-tab-button border-middletab';
		$('sorting').className = 'reportview-tab-button border-righttab';
		
		$('reportConfig.ParameterPanel').style.display = 'block';
		$('reportConfig.DisplayPanel').style.display = 'none';
		$('reportConfig.SortPanel').style.display = 'none';
		
		$('parameters').onclick = function(e)
		{
			$('parameters').className = 'reportview-tab-button active';
			$('display').className = 'reportview-tab-button border-middletab';
			$('sorting').className = 'reportview-tab-button border-righttab';
			
			$('reportConfig.DisplayPanel').style.display = 'none';
			$('reportConfig.SortPanel').style.display = 'none';
			$('reportConfig.ParameterPanel').style.display = 'block';
			//Effect.Appear('reportConfig.ParameterPanel');
		}
		
		$('display').onclick = function(e)
		{
			$('parameters').className = 'reportview-tab-button border-lefttab';
			$('display').className = 'reportview-tab-button active';
			$('sorting').className = 'reportview-tab-button border-righttab';
			
			$('reportConfig.SortPanel').style.display = 'none';
			$('reportConfig.ParameterPanel').style.display = 'none';
			$('reportConfig.DisplayPanel').style.display = 'block';
			//Effect.Appear('reportConfig.DisplayPanel');
		}
		
		$('sorting').onclick = function(e)
		{
			$('parameters').className = 'reportview-tab-button border-lefttab';
			$('display').className = 'reportview-tab-button border-middletab';
			$('sorting').className = 'reportview-tab-button active';
			
			$('reportConfig.ParameterPanel').style.display = 'none';
			$('reportConfig.DisplayPanel').style.display = 'none';
			$('reportConfig.SortPanel').style.display = 'block';
			//Effect.Appear('reportConfig.SortPanel');
		}
		/*buttons*/
		this.saveBtn = $('saveBtn');
		this.cancelBtn = $('cancelBtn');
	}, 
	
	onShow : function()
	{
		this.clearPanels();
		this.populatePanel();
	},
	
	clearPanels : function()
	{
		this.parameterPanel.innerHTML = '';
		this.availableColumnsPanel.innerHTML = '';
		this.displayedColumnsPanel.innerHTML = '';
		this.availableSortColumnsPanel.innerHTML = '';
		this.displayedSortColumnsPanel.innerHTML = '';
	},
	
	populatePanel : function()
	{
		var compList = new ArrayList();
		for (var i=0; i<this.reportParameters.size(); i++)
		{
			var reportParameter = this.reportParameters.get(i);
			var label = reportParameter.getLabel();
			
			var wComponent = reportParameter.clone('param' + reportParameter.getColumnName());
			var component = wComponent.getElement();
			compList.add(wComponent);
			
			var div = document.createElement('div');
			div.setAttribute('class', 'create-inside');
			
			var divLabel = document.createElement('div');
			divLabel.setAttribute('class', 'create-inside-left');
			
			var divComponent = document.createElement('div');
			divComponent.setAttribute('class', 'create-inside-right');
			
			divLabel.innerHTML = label;
			divComponent.appendChild(component);
			
			div.appendChild(divLabel);
			div.appendChild(divComponent);
			
			this.parameterPanel.appendChild(div);
		}
		
		for (var i=0; i<compList.size(); i++)
		{
			var component = compList.get(i);
			component.init();
		}
		
		var availableColumns = this.printFormat.getAvailableColumns();
		var displayedColumns = this.printFormat.getDisplayedColumns();
		var availableSortColumns = this.printFormat.getAvailableSortColumns();
		var displayedSortColumns = this.printFormat.getDisplayedSortColumns();
		
		this.availableColumnsPanel.appendChild(availableColumns);
		this.displayedColumnsPanel.appendChild(displayedColumns);
		this.availableSortColumnsPanel.appendChild(availableSortColumns);
		this.displayedSortColumnsPanel.appendChild(displayedSortColumns);
		
		this.reportConfigNameInput.value = this.reportConfigName;
		this.reportConfigNameInput.select();
		this.printFormat.initTransfer();
		this.cancelBtn.onclick = this.hide.bind(this);
		this.saveBtn.onclick = this.saveHandler.bind(this);
	},
	
	saveHandler : function()
	{
		this.hide();
		$('ReportConfigForm').request({
			onComplete : this.saveSuccess.bind(this),
			onFailure : this.saveFailure.bind(this)
		});
	},
	
	saveSuccess : function(request)
	{
		var response = request.responseText;
		var reportConfigJSON = eval('('+response+')');
		this.reportConfig.onSave(reportConfigJSON);
	},
	
	saveFailure : function(request)
	{}
});

var ReportConfigManager = Class.create({

	initialize : function(reportConfigsJSON)
	{
		this.currentReportConfig = null;
		this.defaultReportConfig = null;
		this.defaultParameterValueList = null;
		
		this._viewingList = $('viewingList');
		this._params = $('params');
		this._editBtn = $('editReportConfig');
		this._delBtn = $('delReportConfig');
		this._newBtn = $('newReportConfig');
		
		this.mapping = new Hash();
		this.onSelectListeners = new ArrayList(); 
		this.reportConfigs = new ArrayList(new ReportConfig());
		this._select = null;
		
		this.addReportConfigs(reportConfigsJSON);
		
		this.buildSelect();
		this.initBtns();
		
		this.refreshDisplay();
		this.dynatreeInitialised = false;
		this.dynatreeReset = false;
	},

	addReportConfigs : function(reportConfigsJSON)
	{
		for (var i=0;i<reportConfigsJSON.length;i++)
		{
			var reportConfigJSON = reportConfigsJSON[i];
			var reportConfig = new ReportConfig(reportConfigJSON);
			
			reportConfig.setReportConfigManager(this);
			
			this.reportConfigs.add(reportConfig);
			
			this.mapping.set(reportConfig.getReportConfigId(), reportConfig);
			
		}
	},
	
	buildSelect : function()
	{
		var _reportConfigSelect = document.createElement('select');
		_reportConfigSelect.setAttribute('id', 'dropdown');
		
		var element = document.createElement('option');
		element.setAttribute("style","cursor:pointer");
		element.setAttribute("class", "reportConfigOption");
		element.innerHTML = '';
		
		_reportConfigSelect.appendChild(element);
		
		for (var i=0; i<this.reportConfigs.size(); i++)
		{
			var reportConfig = this.reportConfigs.get(i);
			var _reportConfigOption = reportConfig.getElement();
			_reportConfigSelect.appendChild(_reportConfigOption);
		}
		this._select = _reportConfigSelect;		
		this._select.onchange = this.reportConfigSelected.bind(this);
	},
	
	initBtns : function()
	{
		this._editBtn.onclick = this.viewCurrentReportConfig.bind(this);
		this._newBtn.onclick = this.newPanel.bind(this);
		this._delBtn.onclick = this.deleteReportConfig.bind(this);
	},
	
	refreshDisplay : function()
	{
		if (this._viewingList != null)
		{
			this._viewingList.innerHTML = '';
			this._viewingList.appendChild(this._select);
		}
	},
	
	reportConfigSelected : function()
	{
		var index = this._select.selectedIndex;
		var option = this._select.options[index];
		var id = option.getAttribute('id');
		
		var reportConfig = this.mapping.get(id);
		reportConfig.reset = true;
		this.dynatreeReset = true;
		this.select(reportConfig);
	},
	
	loadPanel : function()
	{
		var reportConfig = this.getCurrentReportConfig();
		if (reportConfig != null)
		{
			var panel = reportConfig.getPanel();
			panel.show();
		}
	},
	
	newPanel : function()
	{
		this.copyCurrentReportConfig();
	},
	
	deleteReportConfig : function()
	{
		var currentReportConfig = this.getCurrentReportConfig();
						
		if (currentReportConfig.isSystem())
		{
			alert('Cannot delete default views.');
			return;
		}
		
		if(!confirm('Delete View?')) return;
		
		var reportConfigId = currentReportConfig.getReportConfigId();
		var url = 'ReportConfigAction.do';
		var params = 'action=deleteReportConfig&reportConfigId=' + reportConfigId;
		try
		{	
			var myAjax = new Ajax.Request( url, 
			{
				method: 'post', 
				parameters: params,
				onComplete: this.afterDelete.bind(this)
			});	
		}
		catch(e)
		{} 
	},
	
	afterDelete : function(request)
	{
		var response = request.responseText;
		var json = eval('('+response+')');
		var success = json.success;
		if (success)
		{
			var reportConfigId = json.reportConfigId;
			var reportConfigToRemove = null;
			for (var i=0; i<this.reportConfigs.size(); i++)
			{
				var reportConfig = this.reportConfigs.get(i);
				var id = reportConfig.getReportConfigId();
				if (id == reportConfigId)
				{
					reportConfigToRemove = reportConfig;
				}
			}
			
			if (reportConfigToRemove != null)
			{
				this.reportConfigs.remove(reportConfigToRemove);
			}
			
			this.buildSelect();
			this.refreshDisplay();
			
			this.setCurrentReportConfig(null);
		}
	},
	
	viewCurrentReportConfig : function()
	{
		var reportConfig = this.getCurrentReportConfig();
		var url = 'ReportConfigAction.do';
		var params = 'action=viewReportConfig&reportConfigId=' + reportConfig.getReportConfigId();
		try
		{	
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'post', 
				parameters: params,
				onComplete: this.onSuccess.bind(this)
			});	
		}
		catch(e)
		{} 
	},
	
	copyCurrentReportConfig : function()
	{
		var reportConfig = this.getCurrentReportConfig();
		var url = 'ReportConfigAction.do';
		var params = 'action=copyReportConfig&reportConfigId=' + reportConfig.getReportConfigId();
		try
		{	
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'post', 
				parameters: params,
				onComplete: this.onSuccess.bind(this)
			});	
		}
		catch(e)
		{} 
	},
	
	onSuccess : function(request)
	{
		var response = request.responseText;
		var json = eval('('+response+')');
		var reportConfig = new ReportConfig(json);
		reportConfig.setReportConfigManager(this);
		
		if (!this.reportConfigs.contains(reportConfig))
		{
			this.reportConfigs.add(reportConfig);
			this.mapping.set(reportConfig.getReportConfigId(), reportConfig);
			this.buildSelect();
			this.refreshDisplay();
		}
		else
		{
			this.reportConfigs.replace(reportConfig);
		}
		this.setCurrentReportConfig(reportConfig);
		$('ReportConfigForm').reportConfigId.value = reportConfig.getReportConfigId();
		$('ReportConfigForm').processId.value = reportConfig.getProcessId();
		$('ReportConfigForm').printFormatId.value = reportConfig.getPrintFormatId();
		
		this.loadPanel();
	},
	
	setCurrentReportConfig : function(reportConfig)
	{
		this.currentReportConfig = reportConfig;
		if (this.currentReportConfig != null)
		{
			var printFormat = this.currentReportConfig.getPrintFormat();
			printFormat.setItems();
		}
	},
	
	getCurrentReportConfig : function()
	{
		return this.currentReportConfig;
	},

	onSave : function(reportConfig)
	{
		this.dynatreeReset = true;
		this.update(reportConfig);		
	},
	
	update : function(reportConfig)
	{
		this.buildSelect();
		this.refreshDisplay();
		this.select(reportConfig);
	},
	
	select : function(reportConfig)
	{
		this.currentReportConfig = reportConfig;
		this.currentReportConfig.select();
	},
	
	reportConfigFetched : function(reportConfig)
	{
		if (reportConfig.isSystem())
		{
			this._editBtn.hide();
			this._delBtn.hide();
		}
		else
		{
			this._editBtn.show();
			this._delBtn.show();
		}
		
		this.initDynaTree();
		
		$('ReportConfigForm').reportConfigId.value = reportConfig.getReportConfigId();
		$('ReportConfigForm').processId.value = reportConfig.getProcessId();
		$('GenericReportForm').processId.value = reportConfig.getProcessId();
		
		this.fireOnSelectEvent(reportConfig);
	},
	
	initDynaTree : function()
	{
		var parameters = this.currentReportConfig.getParameters();
		if (this.dynatreeInitialised)
		{
			var tree = jQuery("#params").dynatree("getTree");
			for (var i=0; i<parameters.size(); i++)
			{
				var reportParameter = parameters.get(i);
				
				var displayValue = null;
				var displayValueTo = null;
				
				if (this.dynatreeReset)
				{
					displayValue = reportParameter.displayValue;
					displayValueTo = reportParameter.displayValueTo;
				}
				else
				{
					var range = reportParameter.range;
					if (range)
					{
						var fromDispId = reportParameter.from.displayElementId;
						var toDispId = reportParameter.to.displayElementId;
						
						var oldFromVal = $(fromDispId).value;
						var oldToVal = $(toDispId).value;
						
						reportParameter.from.setValue(oldFromVal);
						reportParameter.to.setValue(oldToVal);
						
						displayValue = oldFromVal;
						displayValueTo = oldToVal;
					}
					else
					{
						var displayId = reportParameter.displayElementId;
						var oldDisplayValue = $(displayId).value;
						
						if (reportParameter instanceof WSearch)
						{
							var oldValue = $(reportParameter._hiddenInput.id).value;
							reportParameter.setValue(oldValue, oldDisplayValue);
						}
						else if (reportParameter instanceof WList)
						{
							var oldValue = $(reportParameter.displayElementId).value;
							reportParameter.setValue(oldValue);
						}
						
						displayValue = oldDisplayValue;
					}
				}
				
				if (displayValue == null || displayValue == 'null')
				{
					displayValue = '';
				}
				
				reportParameter.setValue(displayValue);
				//reportParameter.setValue(displayValueTo);
				
				var label = reportParameter.getLabel();
				var element = reportParameter.getElement();
				
				var childNode = tree.getNodeByKey(label);
				
				var node = tree.getNodeByKey("dynatree_parameter_" + childNode.data.key);
				 
				var divOuter = document.createElement('div');
				divOuter.appendChild(element);
				
				node.data.title = divOuter.innerHTML;
				
				if ((displayValue != null && String(displayValue).trim().length > 0) ||
						(displayValueTo != null && String(displayValue).trim().length > 0))
				{
					childNode.expand(true);
				}
			}
			
			jQuery("#params").dynatree("getTree").renderInvisibleNodes();
			
			this.initScript();
		}
		else
		{
			var parent = document.createElement('ul');
			for (var i=0; i<parameters.size(); i++)
			{
				var reportParameter = parameters.get(i);
				var label = reportParameter.getLabel();
				var element = reportParameter.getElement();
				
				var li = document.createElement('li');
				li.innerHTML = label;
				
				var childNode = document.createElement('ul');
				var childLi = document.createElement('li');
				childLi.setAttribute('id', "dynatree_parameter_" + label);
				childLi.appendChild(element);
				
				childNode.appendChild(childLi);
				li.setAttribute('id', label);
				li.appendChild(childNode);
				
				parent.appendChild(li);
				this.dynatreeInitialised = true;
			}
			
			var refreshBtn = document.createElement('div');
			refreshBtn.setAttribute('id', 'refreshBtn');
			refreshBtn.setAttribute('class', 'glyphicons glyphicons-pro refresh pull-right refreshBtn');
			refreshBtn.setAttribute('style', 'font-size: 12pt !important;padding-right:15px !important;padding-top:15px !important;');
			refreshBtn.innerHTML = 'Refresh';
			
			$('params').innerHTML = '';
			$('params').appendChild(refreshBtn);
			$('params').appendChild(parent);
		}
	},
	
	initScript : function()
	{
		var parameters = this.currentReportConfig.getParameters();
		for (var i=0; i<parameters.size(); i++)
		{
			var reportParameter = parameters.get(i);
			reportParameter.init();
			
			var elementId = reportParameter.getDisplayElementId();
			if ($(elementId) != null)
			{
				$(elementId).observe('keypress', this.keypress.bind(this));
			}
		}
	},
	
	keypress : function(event)
	{
		    if (event.keyCode == Event.KEY_RETURN) {
		        // stop processing the event
		        Event.stop(event);
		        this.fireOnSelectEvent(this.currentReportConfig);
		    }
	},
	
	addOnSelectListener : function(listener)
	{
		this.onSelectListeners.add(listener);
	},
	
	fireOnSelectEvent : function(reportConfig)
	{
		for (var i=0; i<this.onSelectListeners.size(); i++)
		{
			var listener = this.onSelectListeners.get(i);
			listener.reportConfigSelected(reportConfig);
		}
	},
	
	getElement : function()
	{
		return this._reportConfigSelect;
	},
	
	selectDefaultReportConfig : function()
	{
		this.defaultReportConfig = this.getDefaultReportConfig();
		
		if(this.defaultReportConfig != null){
			this.select(this.defaultReportConfig);
		}
	}, 
	
	/*Returns the default report configuration*/
	getDefaultReportConfig:function()
	{
		for(var i=1; i<this._select.options.length; i++)
		{
			var option = this._select.options[i];
			
			var id = option.getAttribute('id');
			var reportConfig = this.mapping.get(id);
			
			if (reportConfig.isDefault)
			{
				return reportConfig;
			}
		}
		return null;
	}
});
