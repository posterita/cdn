var GridRow = Class.create({
	initialize : function(_row)
	{
		if (_row != null)
		{
			this._row = _row;
			this.recordId = 0;
			this.onSelectListeners = new ArrayList();
			this.initRecordId();
			this.initRow();
		}
	},
	
	setId : function(id)
	{
		this._row.id = id; 
	},
	
	initRow : function()
	{
		this._row.style.cursor = 'pointer';
		this._row.onclick = this.fireOnSelectEvent.bind(this);
	},
	
	addOnSelectListener : function(listener)
	{
		this.onSelectListeners.add(listener);
	},
	
	fireOnSelectEvent : function()
	{
		for (var i=0; i<this.onSelectListeners.size(); i++)
		{
			var listener = this.onSelectListeners.get(i);
			listener.gridRowSelected(this);
		}
	},
	
	initRecordId : function()
	{
		var childNodes = this._row.childNodes;
		
		if (!childNodes || childNodes.length == 0)
		{
			this.recordId = null;
		}
		
		for (var i=0; i<childNodes.length; i++)
		{
			var _child = childNodes[i];
			if (_child.tagName == "TD")
			{
				var isRecordActionColumn = _child.getAttribute('IsRecordActionColumn');
				var recordId = _child.getAttribute('RecordId');
				
				if (isRecordActionColumn)
				{
					this.recordId = recordId;
				}
			}
		}
	},
	
	getValueOfColumn : function(columnName)
	{
		var childNodes = this._row.childNodes;
		
		if (!childNodes || childNodes.length == 0)
		{
			this.recordId = null;
		}
		
		for (var i=0; i<childNodes.length; i++)
		{
			var _child = childNodes[i];
			if (_child.tagName == "TD")
			{
				var colName = _child.getAttribute('columnName');
				
				if (colName == columnName)
				{
					var columnValue = _child.innerHTML;
				}
			}
		}
		
		return columnValue;
	},
	
	reset : function()
	{
		this._row.className = this._row.className.replace(/\bhighlight\b/,'');
	},
	
	highlight : function()
	{
		this._row.className += ' highlight';
	},
	
	getRecordId : function()
	{
		return this.recordId;
	}
	
});

var ReportGrid = Class.create({
	initialize : function()
	{
		this._grid = $('report-grid');
		
		this.gridRows = new ArrayList(new GridRow());
		
		this.onSelectGridRowListeners = new ArrayList();
		this.onClickPageNoListeners = new ArrayList();
		this.onClickHeaderListeners = new ArrayList();
		
		this.gridIndex = 0;
		this.maxRows = 5;
		this.itemId = 0;
	},
	
	initGrid : function()
	{
		this._reportData = $('reportData');
		
		if (this._reportData != null)
		{  	
			var _rows = this._reportData.getElementsByTagName('tr');
			
			for (var rowIndex = 1; rowIndex < _rows.length; rowIndex++) 
			{
				var _row = _rows[rowIndex];
				
				var gridRow = new GridRow(_row);
				gridRow.setId('row' + rowIndex);
				gridRow.addOnSelectListener(this);
				
		    	this.gridRows.add(gridRow);	
		   	}
		}
	},
	
	getGridRows : function()
	{
		return this.gridRows;
	},
	
	addOnSelectGridRowListener : function(listener)
	{
		this.onSelectGridRowListeners.add(listener);
	},
	
	gridRowSelected : function(gridRow)
	{
		for (var i=0; i<this.gridRows.size(); i++)
		{
			var gRow = this.gridRows.get(i);
			gRow.reset();
		}
		
		gridRow.highlight();
		
		this.fireOnSelectGridRow(gridRow);
	},
	
	fireOnSelectGridRow : function(gridRow)
	{
		for (var i=0; i<this.onSelectGridRowListeners.size(); i++)
		{
			var listener = this.onSelectGridRowListeners.get(i);
			listener.gridRowSelected(gridRow);
		}
	},
	
	setGridContent : function(gridContent)
	{
		this._grid.innerHTML  = gridContent;
		this.initGrid();
	},
	
	addOnClickPageNoListener : function(listener)
	{
		this.onClickPageNoListeners.add(listener);
	},
	
	addOnClickHeaderListener : function(listener)
	{
		this.onClickHeaderListeners.add(listener);
	},
	
	enablePagination : function()
	{
		var pageLinks = $('pageLinks');
		if (pageLinks != null)
		{
			var children = pageLinks.getElementsBySelector('a');
			for (var i=0; i<children.length; i++)
			{
				var child = children[i];
				child.onclick = this.firePageNoClicked.bind(this, child.getAttribute('params'));
			}
		}
	},
	
	enableHeaderSorting : function()
	{
		var reportData = $('reportData');
		if (reportData != null)
		{
			/*var headerLinks = reportData.firstElementChild.getElementsByTagName('A');*/
			var headerLinks = $$('#reportData th a');
			
			for (var i=0; i<headerLinks.length; i++)
			{
				var hlink = headerLinks[i];
				hlink.onclick = this.fireHeaderClicked.bind(this, hlink.getAttribute('params'));
			}
		}
	},
	
	firePageNoClicked : function(params)
	{
		for (var i=0; i<this.onClickPageNoListeners.size(); i++)
		{
			var listener = this.onClickPageNoListeners.get(i);
			listener.pageNoClicked(params);
		}
	},
	
	fireHeaderClicked : function(params)
	{
		for (var i=0; i<this.onClickHeaderListeners.size(); i++)
		{
			var listener = this.onClickHeaderListeners.get(i);
			listener.headerClicked(params);
		}
	}
});
