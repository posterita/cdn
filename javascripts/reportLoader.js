function getReport(processKey, defaultParameterValueListJSON, pmenuId, menuId)
{
	//var defaultParameterValueListJSON = "{'AD_Org_ID': 10000018, 'C_BPartner_ID': 10000300, 'TenderType': 'X'}";
	//var defaultParameterValueListJSON = '{DocStatus: DR, m_warehouseto_id: \'@#M_Warehouse_ID@\'}';
	//var defaultParameterValueListJSON = '{DocStatus: DR, m_warehouseto_id: "@#M_Warehouse_ID@"}';
	
	if (defaultParameterValueListJSON.indexOf('@#') != -1)
	{
		//defaultParameterValueListJSON = defaultParameterValueListJSON.replace(new RegExp('$', 'g') , '"');
		var complete = '';

		while(defaultParameterValueListJSON.indexOf('$') != -1)
		{
			var index = defaultParameterValueListJSON.indexOf('$');
			var part = defaultParameterValueListJSON.substring(0,index + 1);

			part = part.replace('$', '"');
			complete = complete + part;
			defaultParameterValueListJSON = defaultParameterValueListJSON.substring(index + 1);
		}
		complete = complete + defaultParameterValueListJSON;
		defaultParameterValueListJSON = complete;
	}
		
	$('processKey').value = processKey;
	$('defaultParameterValueList').value = defaultParameterValueListJSON;
	$('pmenuId').value = pmenuId;
	$('menuId').value = menuId;
	$('reportForm').submit();
	
	for(var i=0; i<menuTree.length; i++){
		var m = menuTree[i];
		for(var j=0; j<m.nodes.length; j++){
			var node = m.nodes[j];
			if(node.id == menuId){
				menu = node;
				pmenu = m;
				break;
			}
		}
	}						
					
	if(menu){
		currentPage = menu;
		//var breadcrumb = pmenu.name + " -> " + menu.name;
		//jQuery('#breadcrumb').html(breadcrumb);	
		
		jQuery('#parent-menu').html(pmenu.name);
		jQuery('#sub-menu').html(menu.name);
		jQuery('#menu-icon').attr('class', 'glyphicons glyphicons-pro ' + pmenu.styleId);
	}
}