var ReportManager = Class.create({
	
	initialize : function(report)
	{
		this.processId = report.processId;
		this.reportConfigs = report.reportConfigs;
		this.recordActions = report.recordActions;
		this.defaultParameterValueList = report.defaultParameterValueList;
		
		this.reportConfigManager = null;
		this.reportGrid = null;		
		this.reportAjax = null;
		this.actionPanel = null;
	},
	
	init : function()
	{
		this.initReportConfig();
		this.initReportGrid();
		this.initReportAjax();
		this.initActionPanel();
		this.initAjaxResponder();
		this.loadDefaultReportConfig();
	},
	
	initReportConfig : function()
	{
		this.reportConfigManager = new ReportConfigManager(this.reportConfigs);
		this.reportConfigManager.addOnSelectListener(this);
	},
	
	initReportGrid : function()
	{
		this.reportGrid = new ReportGrid();
		this.reportGrid.initGrid();
		
		this.reportGrid.addOnSelectGridRowListener(this);
		this.reportGrid.addOnClickPageNoListener(this);
		this.reportGrid.addOnClickHeaderListener(this);
	},
	
	initReportAjax : function()
	{
		this.reportAjax = new ReportAjax();
		this.reportAjax.addOnLoadListener(this);
	},
	
	initActionPanel : function()
	{
		this.actionPanel = new ActionPanel();
		
		/*this.actionPanel.addDefaultAction('newItem', this.recordActions.newRecordUrl);*/
		this.actionPanel.addDefaultAction('importItem', this.recordActions.importRecordUrl);
		this.actionPanel.addDefaultAction('reset', this.recordActions.resetUrl);
		this.actionPanel.addDefaultAction('importImages', this.recordActions.importImagesUrl);
		this.actionPanel.addDefaultAction('requestStockItem', this.recordActions.requestStockUrl);
		this.actionPanel.addDefaultAction('sendStockItem', this.recordActions.sendStockUrl);
		this.actionPanel.addDefaultAction('deleteBulkProduct', this.recordActions.deleteBulkUrl);
		
		this.actionPanel.addCustomAction('editItem', this.recordActions.editRecordUrl);
		this.actionPanel.addCustomAction('viewItem', this.recordActions.viewRecordUrl);
		this.actionPanel.addCustomAction('configureItem', this.recordActions.configureRecordUrl);
		this.actionPanel.addCustomAction('orgAccessItem', this.recordActions.orgAccessRecordUrl);
		this.actionPanel.addCustomAction('deletePrices', this.recordActions.deletePricesUrl);
		this.actionPanel.addCustomAction('changePaymentType', this.recordActions.changePaymentTypeUrl);
		/*this.actionPanel.addCustomAction('barcodeMatching', this.recordActions.barcodeMatchingUrl);*/
		this.actionPanel.addCustomAction('allocatePayment', this.recordActions.allocatePaymentUrl);
		/*this.actionPanel.addCustomAction('deleteItem', this.recordActions.deleteRecordUrl);*/
		
		this.actionPanel.addJSCustomAction('reset-password-btn', this.recordActions.resetUserPasswordUrl);
		/*this.actionPanel.addJSCustomAction('generate-qr-btn', this.recordActions.generateQRUrl);*/
		this.actionPanel.addJSCustomAction('claimLead', this.recordActions.claimLeadUrl);
		this.actionPanel.addJSCustomAction('assignLead', this.recordActions.assignLeadUrl);
		this.actionPanel.addJSCustomAction('rejectLead', this.recordActions.rejectLeadUrl);
		this.actionPanel.addJSCustomAction('closeLead', this.recordActions.closeLeadUrl);
		this.actionPanel.addJSCustomAction('reOpenLead', this.recordActions.reOpenLeadUrl);
		this.actionPanel.addJSCustomAction('openLead', this.recordActions.openLeadUrl);
		this.actionPanel.addJSCustomAction('commentLead', this.recordActions.commentLeadUrl);
		this.actionPanel.addJSCustomAction('transactionDetails', this.recordActions.transactionDetailsUrl);
		
		var delCommRunPopUp = new DeleteCommRun(this.recordActions.deleteCommissionRunUrl);
		this.actionPanel.addCustomPopUpAction('deleteCommissionRun', delCommRunPopUp);

		var datePickerPopUp = new DatePickerPanel(this.recordActions.showStatementUrl);
		this.actionPanel.addCustomPopUpAction('showStatementOfAccount', datePickerPopUp);

		var exportStatementOfAccountPopUp= new ExportStatementOfAccountPopUp(this.recordActions.showStatementUrl);
		this.actionPanel.addCustomPopUpAction('exportStatementOfAccountPopUp', exportStatementOfAccountPopUp);
		
		var deleteCommissionPopUp = new DeleteCommission(this.recordActions.deleteCommissionUrl);
		this.actionPanel.addCustomPopUpAction('deleteCommission', deleteCommissionPopUp);
		
		/*var deleteProductPopUp = new DeleteProduct(this.recordActions.deleteProductUrl);
		this.actionPanel.addCustomPopUpAction('deleteProduct', deleteProductPopUp);*/
		
		var deleteEventNotifierPopUp = new DeleteEventNotifier(this.recordActions.deleteEventNotifierUrl);
		this.actionPanel.addCustomPopUpAction('deleteEventNotifier', deleteEventNotifierPopUp);
		
		var displayLogic = eval('('+this.recordActions.displayLogic+')');
					
		this.actionPanel.setDisplayLogic(displayLogic); 
		
		this.actionPanel.addCommissionPopUp('generateCommission', this.recordActions.generateCommissionUrl, this.processId);
		
		this.actionPanel.addOnRefreshListener(this);
		this.actionPanel.addOnClickCSVListener(this);
		this.actionPanel.addOnClickPDFListener(this);
	},
	
	initAjaxResponder : function()
	{
		/*Ajax.Responders.register({
		  onCreate: function() {
		    $('indicator').style.display = 'block';
		  },
		  onComplete: function() {
		    $('indicator').style.display = 'none';
		  }
		});*/
	},
	
	reportLoaded : function(gridContent)
	{
		this.reportGrid.setGridContent(gridContent);
		this.reportGrid.enablePagination();
		this.reportGrid.enableHeaderSorting();
		this.actionPanel.initCSVPDF();
	},
	
	gridRowSelected : function(gridRow)
	{
		var recordId = gridRow.getRecordId();
		if (recordId != 0)
		{
			this.actionPanel.updateCustomActions(recordId);
			this.actionPanel.updateDisplay(gridRow); 
		}
	},
	
	pageNoClicked : function(params)
	{
		this.reportAjax.openPageNo(params);
	},
	
	headerClicked : function(params)
	{
		this.reportAjax.sortHeader(params);
	},
	
	reportConfigSelected : function(reportConfig)
	{
		this.reportAjax.showReport(reportConfig.getReportConfigId());
	},
	
	refreshBtnClicked : function()
	{
		var currentReportConfig = this.reportConfigManager.getCurrentReportConfig();
		if (currentReportConfig == null)
		{
			alert(Translation.translate('please.choose.a.view','Please choose a view'));
			return;
		}
		
		$('params').style.display = 'none';
		currentReportConfig.showParamValues();
		this.reportAjax.reloadReport();
	},
	
	csvBtnClicked : function()
	{
		var currentReportConfig = this.reportConfigManager.getCurrentReportConfig();
		if (currentReportConfig == null)
		{
			alert(Translation.translate('please.choose.a.view','Please choose a view'));
			return;
		}
		this.reportAjax.exportCSV();
	},
	
	pdfBtnClicked : function()
	{
		var currentReportConfig = this.reportConfigManager.getCurrentReportConfig();
		if (currentReportConfig == null)
		{
			alert(Translation.translate('please.choose.a.view','Please choose a view'));
			return;
		}
		this.reportAjax.exportPDF();
	},
	
	getDefaultParameterValueList : function()
	{
		return this.defaultParameterValueList;
	},
	
	loadDefaultReportConfig : function()
	{
		/*if (this.defaultParameterValueList != null && this.defaultParameterValueList != '')
		{
			this.reportConfigManager.selectDefaultReportConfig();
		}*/
		
		this.reportConfigManager.selectDefaultReportConfig();
	}
});
