var RoleMenuTab = Class.create(Tab, {
		
		initialize : function($super, tabId)
		{
			$super(tabId, 'RoleMenuPanel');
			this.treeManager = null;
			this.messagePanel = $('message-panel');
		},
		
		afterCall : function(json)
		{
			this.generalHelp = json['role.menu.general.help'];
			
			this.menuKeyValues = json.menuKeyValues;
			
			this.treeManager = new TreeManager('#role-menu-tree', 'UserMenuTree', this.modelId, null);
			this.tree = this.treeManager.getTree();
			this.tree.setIsCheckbox(true);
			this.tree.setSelectMode(3);
			this.tree.setnoLink(true);
			this.tree.setMinExpandLevel(1);
			this.treeManager.setLoadTreeUrl(null);
			this.tree.setChildren(json.tree);
			this.treeManager.init();
			
			this.addMenuMessages();
			
			//select nodes
			var menuIds = new Array();
			
			if (json.menuIds != null && json.menuIds != '')
			{
				menuIds = json.menuIds.split(",");
				this.treeManager.selectNodes(menuIds);
			}
			
			//deselect submenu not present in role. Added because by default dynatree 
			//selects all child nodes when parent node is selected 
			//this.treeManager.deSelectNodes(menuIds);
		},
		
		getParameters : function()
		{
			var parameters = "{'menuIds':\"" + this.treeManager.getSelectedNodesIds() + "\"}";
			
			return parameters;
		},
		
		afterOk : function(json)
		{
			if (json.error)
			{
				//$('msg-box').setAttribute('class', 'msg-box-error');
				$('success-msg-box').style.display = 'none';
				$('error-msg-box').style.display = 'block';
				$('ErrorMessages').innerHTML = json.message;
			}
			else
			{
				//$('msg-box').setAttribute('class', 'msg-box-save');
				$('error-msg-box').style.display = 'none';
				$('success-msg-box').style.display = 'block';
				$('SuccessMessages').innerHTML = json.message;
			}
		},
		
		addMenuMessages : function()
		{
			if (this.menuKeyValues != null && this.messagePanel != null)
			{
				for(var i=0; i<this.menuKeyValues.size(); i++)
				{
					var tr = document.createElement('tr');
					
					var messageKeytd = document.createElement('td');
					messageKeytd.setAttribute('width', '33%');
					
					var messageValuetd = document.createElement('td');
					messageValuetd.setAttribute('width', '33%');
					
					var messageNewValuetd = document.createElement('td');
					messageNewValuetd.setAttribute('width', '33%');
					
					messageKeytd.innerHTML = this.menuKeyValues[i].key;
					messageValuetd.innerHTML = this.menuKeyValues[i].value;
					
					var input = document.createElement('input');
					input.setAttribute('name', 'message-' + this.menuKeyValues[i].key);
					messageNewValuetd.appendChild(input);
					
					tr.appendChild(messageKeytd);
					tr.appendChild(messageValuetd);
					tr.appendChild(messageNewValuetd);
					
					this.messagePanel.appendChild(tr);
				}
			}
		}
	});

var RoleOrgAccessTab = Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'RoleOrgAccessPanel');
		this.availableOrgsPanel = $('availableOrgs');
		this.chosenOrgsPanel = $('chosenOrgs');
		this.availableOrgsInput = $('availableOrgsInput');
		this.chosenOrgsInput = $('chosenOrgsInput');
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['role.org.access.general.help'];
		
		this.availableOrgsList = json.availableOrgs;

		this.availableOrgsSelect = document.createElement('select');
		this.availableOrgsSelect.setAttribute('id', 'availableOrgsSelect');
		this.availableOrgsSelect.setAttribute('multiple', 'true');

		for(var i=0; i<this.availableOrgsList.length; i++)
		{
			var option = document.createElement('option');
			option.setAttribute('value', this.availableOrgsList[i].orgId);
			option.innerHTML = this.availableOrgsList[i].orgName;
			this.availableOrgsSelect.appendChild(option);
		}
		this.availableOrgsPanel.innerHTML = '';
		this.availableOrgsPanel.appendChild(this.availableOrgsSelect);


		this.chosenOrgsList = json.chosenOrgs;
		this.chosenOrgsSelect = document.createElement('select');
		this.chosenOrgsSelect.setAttribute('id', 'chosenOrgsSelect');
		this.chosenOrgsSelect.setAttribute('multiple', 'true');

		for(var i=0; i<this.chosenOrgsList.length; i++)
		{
			var option = document.createElement('option');
			option.setAttribute('value', this.chosenOrgsList[i].orgId);
			option.innerHTML = this.chosenOrgsList[i].orgName;
			this.chosenOrgsSelect.appendChild(option);
		}
		this.chosenOrgsPanel.innerHTML = '';
		this.chosenOrgsPanel.appendChild(this.chosenOrgsSelect);

		var transfer = new Transfer(this.availableOrgsSelect.id, this.chosenOrgsSelect.id, this.availableOrgsInput.id, this.chosenOrgsInput.id);
		transfer.init();
	    transfer.updateInputs();
	},
	
	getParameters : function()
	{
		var parameters = "{'availableOrgs':\"" + this.availableOrgsInput.value + "\"," +
				"'chosenOrgs':\"" + this.chosenOrgsInput.value + "\"}";
		
		return parameters;
	},
	
	afterOk : function(json)
	{
		if (json.error)
		{
			//$('msg-box').setAttribute('class', 'msg-box-error');
			$('success-msg-box').style.display = 'none';
			$('error-msg-box').style.display = 'block';
			$('ErrorMessages').innerHTML = json.message;
		}
		else
		{
			//$('msg-box').setAttribute('class', 'msg-box-save');
			$('error-msg-box').style.display = 'none';
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.message;
		}
	}
});
