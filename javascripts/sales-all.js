/** search product.js **/
var SearchProduct = {
		searchResultPlaceHolder:'productSearchPlaceHolder',
		isSoTrx:true,
		ignoreTerminalPricelist:false,
		input:null,
		active:null,
		filterQuery:null,
		param: new Hash(),
		createInstanceProductPanel: null,
		isChildSearch: false,
		
		search:function( criteria ){
			
			/*TODO: check network*/
			
			if(SearchProduct.input == null) SearchProduct.input = $('search-product-textfield');
			
			if (!SearchProduct.isChildSearch)
			{
				//reset search parameter
				//SearchProduct.filterQuery = 'm_product.m_product_parent_id is null';
				SearchProduct.filterQuery = '';
			}
			
			var searchTerm = null;
			
			if( criteria && SearchProduct.isSoTrx == true 
					&& SearchProduct.searchByBarcodeOnly 
					&& SearchProduct.searchByBarcodeOnly == true 
					&& criteria.barcode 
					&& criteria.barcode == true )
			{					
				SearchProduct.param.set('searchByBarcodeOnly', 'true');
				
			}else
			{
				SearchProduct.param.set('searchByBarcodeOnly', 'false');
				
			}
			
			if( criteria && criteria.searchTerm )
			{
				searchTerm = criteria.searchTerm
			}
			else
			{
				searchTerm = SearchProduct.input.value;
			}			
			
			//var param = new Hash();
			SearchProduct.param.set('action', 'search');
			SearchProduct.param.set('searchTerm', searchTerm );
			SearchProduct.param.set('isSoTrx', SearchProduct.isSoTrx);
			SearchProduct.param.set('bpPricelistVersionId', SearchProduct.bpPricelistVersionId);
			SearchProduct.param.set('warehouseId', SearchProduct.warehouseId);
			SearchProduct.param.set('ignoreTerminalPricelist', SearchProduct.ignoreTerminalPricelist);
			SearchProduct.param.set('filterQuery', SearchProduct.filterQuery);
			
			SearchProduct.highlightTextField();
			
			/*if (SearchProduct.input.value == '')
			{
				Ajax.Responders.register({
					  onCreate: function() {
						  jQuery('#indicator').css('display', 'block');
					  },
					  onComplete: function() {
						  jQuery('#indicator').css('display', 'none');
					  }
				});
			}*/
	
			new Ajax.Request('ProductAction.do', {
				method:'post',
				onSuccess: function(response){
					
					/*Ajax.Responders.register({
						  onCreate: function() {
							  jQuery('#indicator').css('display', 'none');
						  },
						  onComplete: function() {
							  jQuery('#indicator').css('display', 'none');
						  }
					});*/
					
					var json = response.responseText.evalJSON(true);					
					
					
					if (json.isSerialNoSearch)
					{
						SearchProduct.renderSerialNo(json);
					}
					else
					{
						SearchProduct.render(json);
					}
				},
				parameters: SearchProduct.param
			});
			
		},
		render:function(json){
			
			if(json.error && json.error == true){
				
				if(json.reason == 'session.timeout'){
					
					BootstrapDialog.show({
						closable: false,
						type: BootstrapDialog.TYPE_DANGER,
			            title: 'Session Expired',
			            message: 'Your session has expired. Please login again.',
			            buttons: [{
			                label: 'OK',
			                action: function(dialog) {
			                	window.location.href = "select-user.do";
			                }
			            }]
			        });
			        
				}
				else{
					alert("Failed to search product! Reason: " + json.reason);
				}
				
				return;
			}
			
			var container = $('searchResultContainer');
			container.innerHTML = '';
			
			var html = "<table id='searchResultTable' border='0' width='100%'>";

			var results = json.products;
			
			//filter modifiers
			var products = [];
			
			for(var i=0; i<results.length; i++){
				var record = results[i];
				if(record.ismodifier == 'Y') continue;
				
				products.push(record);
			}
			
			if (!json.success)
			{
				if (json.shopSavvy)
				{
					var shopSavvyPanel = new ShopSavvyProductPanel(json.shopSavvy, json.taxCategories);
					shopSavvyPanel.show();
				}
				else
				{
					var productSearchErrorPanel = new ProductSearchErrorPanel(SearchProduct.input.value);
					productSearchErrorPanel.show();
				}
			}
			
			var count = 0;
			
			for(var i=0; i<products.length; i++)
			{
				var product = products[i];
				
				/* lot and expiry check*/	
				/*
				if(product.isbatchandexpiry == 'Y'){
					
					// sum qty on hand
					while( i+1 <products.length && product.m_product_id == products[i+1].m_product_id ){
						product.qtyonhand += products[i+1].qtyonhand;
						i++;
					}
					 
				}
				*/
				
				var isParent = product.isparent == 'Y'? true : false;
				var isSerialNo = product.isserialno == 'Y' ? true : false;
				var m_product_parent_id = product.m_product_parent_id;
				
				count ++;
				
				var styleClass = 'even-row';
				
				if (count%2 != 0)
				{
					styleClass = 'odd-row';
				}
				
				if ((OrderScreen.orderType == 'POS Order' && isSerialNo && product.qtyonhand == 0 && !isParent) 
					|| (OrderScreen.isInventory() && isSerialNo && product.qtyonhand == 0 && !isParent)
					|| (OrderScreen.orderType == 'POS Goods Receive Note' && isSerialNo && product.qtyonhand == 1 && !isParent)
					/*|| (!SearchProduct.isChildSearch && m_product_parent_id != '')*/)
				{
					//If product is serial no and is not parent and has already been sold do not display in sales
					//Or if product is serial no and is not parent and qty is zero, we cannot transfer
					//Or if product is serial no and is not parent and qty is one, we cannot purchase
					
					products.splice(i, 1);
					i--;
				}
				else
				{
					html += "<tr data-test-id='product-search-result-product-info' class='" + (product.isparent == 'Y' ? " parentProduct" : "") + "' id='product_" + product.m_product_id + "'>";
					html += "<td class='"+ styleClass +"' style='width:80%;'><span style='padding-left:20px;'>";
					html += product.name;
					html += "<span><br><span style='padding-left:20px;'>";
					html += product.description;
					html += "</span></td>";
					if(product.producttype == 'I') {
						html += "<td class='"+ styleClass +"' style='width:20%;'><span style='padding-right:20px;' class='pull-right'>";
						html += product.qtyonhand;
						html += "x</span></td>";
					}else{
						html += "<td class='"+ styleClass +"' style='width:20%;'><span style='padding-right:20px;' class='pull-right'>";
						html += "&nbsp;</span></td>";
					}
					html += "</tr>";
				}
			}

			html += "</table>";
			
			container.innerHTML = html;
			
			//Reset
			SearchProduct.isChildSearch = false;
			
			/*add behaviour*/
			var table = $('searchResultTable');
			var rows = table.rows;
			var searchTerm = SearchProduct.param.get("searchTerm");
			
			if (products.length == 1)
			{
				var product = products[0];
				var isParent = product.isparent == 'Y'? true : false;
				var m_product_parent_id = product.m_product_parent_id;
				var isSerialNo = product.isserialno == 'Y' ? true : false;
				
				
				if (!isParent)
				{
					if(product.upc == searchTerm)
					{
						SearchProduct.onSelect(product, false);
						/*var highlightColor = 'search-product-highlight';
						
						if(SearchProduct.active){
							SearchProduct.active.removeClassName(highlightColor);						
						}*/
					}
					else
					{
						SearchProduct.onSelect(product, SearchProduct.trackManualProductEntry);
					}
					
				}
			}
			
			for(var i=0; i<rows.length; i++)
			{
				var row = rows[i];
				row.product = products[i];
				
				row.style.cursor = 'pointer';
				row.onclick = function(e){
					
					var isParent = this.product.isparent == 'Y'? true : false;
					var isSerialNo = this.product.isserialno == 'Y' ? true : false;
					
					//If product is parent and is serialno and all instances have already been created and sold
					if (isParent && isSerialNo && this.product.qtyonhand ==  0 && OrderScreen.orderType == 'POS Order')
					{
						new AlertPopUpPanel(Translation.translate('all.serial.no.have.already.been.created.and.sold','All serial no have already been created and sold.')).show();
						return;
					}
					
					//If product is parent and is serialno and qty is zero and we are sending stock
					//We cannot send parent with qty 0
					if (isParent && isSerialNo && this.product.qtyonhand ==  0 && OrderScreen.isInventory() && OrderScreen.orderType == 'stock-transfer-send')
					{
						new AlertPopUpPanel(Translation.translate('you.cannot.transfer.a.parent.with.quantity.zero','You cannot transfer a parent with quantity zero.')).show();
						return;
					}
					
					//If product is parent and is serialno and qty is zero and we are requesting stock
					if (isParent && isSerialNo && this.product.qtyonhand ==  0 && OrderScreen.isInventory() && OrderScreen.orderType == 'stock-transfer-request')
					{
						SearchProduct.onSelect(this.product);
						
						var highlightColor = 'search-product-highlight';
						
						if(SearchProduct.active){
							SearchProduct.active.removeClassName(highlightColor);						
						}
						
						SearchProduct.active = this;
						this.addClassName(highlightColor);
						return;
					}
					
					//If an existing(old) product or child, add to cart
					if (!isParent)
					{
						SearchProduct.onSelect(this.product, SearchProduct.trackManualProductEntry && searchTerm != this.product.upc);
						
						var highlightColor = 'search-product-highlight';
						
						if(SearchProduct.active){
							SearchProduct.active.removeClassName(highlightColor);						
						}
						
						SearchProduct.active = this;
						this.addClassName(highlightColor);
					}
					else
					{
						/*Is a parent product and may have children, query children*/
						SearchProduct.isChildSearch = true;
						
						SearchProduct.filterQuery = 'm_product.m_product_parent_id=' + this.product.m_product_id;
						
						/*Non Instance Variants will always exists with children, render children list
						If product is serialNo, it may not have instance. Prompt to create specific instance
						If instances exist and all instances has been created, render children list
						If not all instances have been created, render children list and prompt to create more instance
						*/
						
						//If Non instance variant, search children and render list
						if (!isSerialNo)
						{
							SearchProduct.search();
						}
						else
						{
							//SerialNo
							SearchProduct.param.set('isSerialNoSearch', true);
							
							/*if (OrderScreen.orderType == 'POS Goods Receive Note')
							{
								//Since we are purchasing serial no, we should purchase child with qty 0
								//SearchProduct.filterQuery = 'searchproduct.qtyonhand=0;searchproduct.hasbeensold=\'N\';searchproduct.m_product_parent_id=' + this.product.m_product_id;
								SearchProduct.filterQuery = 'm_product.m_product_parent_id=' + this.product.m_product_id;
							}
							else
							{
								//do not display child with qty 0 on sales and inventory
								//SearchProduct.filterQuery = 'm_product.qtyonhand<>0;m_product.m_product_parent_id=' + this.product.m_product_id;
								SearchProduct.filterQuery = 'm_product.m_product_parent_id=' + this.product.m_product_id;
							}*/
							
							/*SearchProduct.filterQuery += ' and stock.qtyonhand > 0';*/
							SearchProduct.active = this;
							SearchProduct.search();
						}
					}
					
					//resetFilter after serialno search
					//SearchProduct.filterQuery = 'm_product.m_product_parent_id is null';
					SearchProduct.param.set('isSerialNoSearch', false);
				};
			}
				
			SearchProduct.afterRender();
			SearchProduct.input.value = '';
		},
		afterRender:function(){
			jQuery('#pane3').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		},
		onSelect:function(product, manualEntry){
			shoppingCart.addToCart(product.m_product_id, 1, null, null, null, null, manualEntry);
			setShoppingCartHeight();
		},
		initializeComponents:function(){
			SearchProduct.input = $('search-product-textfield');
			$('search-product-button').onclick = function(e){
				SearchProduct.search({barcode : true});
			};

			$('search-product-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchProduct.search({barcode : true});
				}
			};
			
			$('search-product-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-product-textfield').onclick = function(e){
				this.select();
			};
		},
		highlightTextField : function()
		{
			$('search-product-textfield').select();
		},
		clearTextField : function()
		{
			$('search-product-textfield').value = '';
		},
		disableTextFieldFocus : function()
		{
			$('search-product-textfield').blur();
		},
		
		renderSerialNo : function(json)
		{
			//If no Instance exists
			if  (!json.success)
			{
				//Prompt to create new Instance
				SearchProduct.createInstanceProductPanel = new CreateInstanceProductPanel(SearchProduct.active.product);
				SearchProduct.createInstanceProductPanel.displayButtons();
				SearchProduct.createInstanceProductPanel.show();
			}
			else
			{
				//Render children List and Prompt to create new Instance if any
				//Check if we can create more instance
				
				if (SearchProduct.active.product.qtyonhand == 1 && SearchProduct.active.product.isparent == 'N' && OrderScreen.orderType == 'POS Order')
				{
					//add the only instance left to cart
					SearchProduct.onSelect(json.products[0]);
				}
				else
				{
					SearchProduct.createInstanceProductPanel = new CreateInstanceProductPanel(SearchProduct.active.product);
					SearchProduct.createInstanceProductPanel.productListJSON = json;
					SearchProduct.createInstanceProductPanel.displayButtons();
					SearchProduct.createInstanceProductPanel.show();
				}
			}
			
			/*if (SearchProduct.createInstanceProductPanel != null)
			{
				SearchProduct.createInstanceProductPanel.productListJSON = json;
				SearchProduct.createInstanceProductPanel.noOfChildren = json.products.length;
				SearchProduct.createInstanceProductPanel.parentQty = SearchProduct.active.product.qtyonhand;
				SearchProduct.createInstanceProductPanel.displayButtons();
			}*/
		}
};

var SearchProductFilter = {
		data : null,
		selection : null,
		query : null,		
		
		setSelectionQuery : function(query){
			this.query = query;
			this.applySelection();
		},
				
		applySelection : function(){
			
			/*TODO: check network*/			
			
			var filterQuery = this.query;
			
			if (filterQuery.indexOf('=') != -1)
			{
				var find = '=';
				var re = new RegExp(find, 'g');
				
				filterQuery = filterQuery.replace(re, '=\'');
			}
			
			if (filterQuery.indexOf(';') != -1)
			{
				var find = ';';
				var re = new RegExp(find, 'g');
				
				filterQuery = filterQuery.replace(re, '\';');
			}
			
			filterQuery = filterQuery + ';' + 'm_product.m_product_parent_id is null';
			
			var url = "ProductAction.do";
			
			/*
			var param = new Hash();
			
			param.set('action', 'search');			
			param.set('searchTerm', '');
			param.set('isSoTrx', SearchProduct.isSoTrx);
			param.set('bpPricelistVersionId', SearchProduct.bpPricelistVersionId);
			param.set('warehouseId', SearchProduct.warehouseId);
			param.set('filterQuery', filterQuery);
			param.set('ignoreTerminalPricelist', SearchProduct.ignoreTerminalPricelist);
			
			param = param.toQueryString();
			
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'get', 
				parameters: param, 
				onSuccess: function (response) {
					//update info
					var json = response.responseText.evalJSON(true);
					SearchProduct.render(json);
				}
			});
			*/
			var data = {
				'action' : 'search',		
				'searchTerm' : '',
				'isSoTrx' : SearchProduct.isSoTrx,
				'bpPricelistVersionId' : SearchProduct.bpPricelistVersionId,
				'warehouseId' : SearchProduct.warehouseId,
				'filterQuery' : filterQuery,
				'ignoreTerminalPricelist' : SearchProduct.ignoreTerminalPricelist
			};
			
			jQuery.ajax({
				
				type: "POST",
				url: url,
				data: data,
				success: (data, status, xhr) => {
					SearchProduct.render(data);
				},
				error: (xhr, data, status) => {
					
					if(xhr.status === 0){
						status = "Server is unreachable!";
					}

					BootstrapDialog.show({
						closable: true,
						type: BootstrapDialog.TYPE_DANGER,
						title: 'Error',
						message: status,
						buttons: [{
							label: 'OK',
							action: function(dialog) {
								dialog.close();
							}
						}]
					});
				}
			});			
			
		}
};
/**
 * Call ShoppingCartAction 
 * Available actions 
 * 1. getCart - no param 
 * 2. addToCart - param1 productId, param2 qty 
 * 3. clearCart 
 * 4. removeFromCart - param1 productId 
 * 5. incrementQty - param1 productId, param2 qty
 * 6. decrementQty - param1 productId, param2 qty
 */
function reportShoppingCartReferencingError()
{
	alert("Please use ShoppingCart Class instead!");
}

function reportShoppingCartError()
{
	console.warn('Operation on cart FAILED!');
}

var ShoppingCart = Class.create( {
	initialize : function(orderType) {
		this.orderType = orderType;
		this.isSoTrx = true;
		this.bpId = null;
		this.pricelistId = null;
		this.lastUpdatedProductId = 0;
		this.orderId = 0;
		this.lines = null;
		this.container = null;
		this.subTotal = 0;
		this.grandTotal = 0;
		this.taxTotal = 0;
		this.qtyTotal = 0;
		this.discountOnTotal = 0;
		this.upsellTotal = 0;
		this.selectedIndex = -1;
		this.active = false;
		this.currencySymbol = '$';
		this.URL = 'ShoppingCartAction.do';	
		this.requestCounter = 0;
		this.initializeShortcuts();
		
		this.callbacks = jQuery.Callbacks();
	},
	setContainer : function(container) {
		this.container = container;
	},
	updateCart : function(url){

		/*TODO: check network*/	

		this.dfd = jQuery.Deferred();

		LOGGER.info('Cart requesting --> ' + url);
		this.requestCounter ++;

		jQuery.ajax({
			type: "GET",
			url: url,
			success: (data, status, xhr) => {

				this.requestCounter --;
				
				let responseText = xhr.responseText;

				if(responseText.startsWith("{")){
					var json = JSON.parse(responseText);

					if(json.error && json.error == true){

						if(json.reason == 'session.timeout'){

							BootstrapDialog.show({
								closable: false,
								type: BootstrapDialog.TYPE_DANGER,
								title: 'Session Expired',
								message: 'Your session has expired. Please login again.',
								buttons: [{
									label: 'OK',
									action: function(dialog) {
										window.location.href = "select-user.do";
									}
								}]
							});

						}
						else {
							BootstrapDialog.show({
								closable: true,
								type: BootstrapDialog.TYPE_DANGER,
								title: 'Error',
								message: json.reason,
								buttons: [{
									label: 'OK',
									action: function(dialog) {
										dialog.close();
									}
								}]
							});
						}

						return;
					}
				}
				else {
					jQuery('#' + this.container).html(data);
				}

			},
			error: function(xhr, status, error) {
				// handle error
				
				if(xhr.status === 0){
					error = "Server is unreachable!";
				}
				
				BootstrapDialog.show({
					closable: true,
					type: BootstrapDialog.TYPE_DANGER,
					title: 'Error',
					message: error,
					buttons: [{
						label: 'OK',
						action: function(dialog) {
							dialog.close();
						}
					}]
				});
			}
		});



		/*
		new Ajax.Updater(
				this.container,
				url,
				{
					evalScripts:true,
					method: "post",
					onSuccess:	this.successNotifier.bind(this),
					onComplete:	this.completeNotifier.bind(this),
					onFailure:	this.failureNotifier.bind(this),
					onException: this.exceptionNotifier.bind(this)
				}
			);
		 */

		return this.dfd.promise();
	},
	successNotifier : function(response) {
		/**/ 
		console.info('Operation on cart succeded');
		this.requestCounter --;
		this.dfd.resolve(response);
		
		this.callbacks.fire("success-notifier");
	},
	completeNotifier : function(response) {
		/**/ 
		console.info('Operation on cart completed');
		$('shopping-cart-outer-container').style.display = 'block';
		this.requestCounter --;
		this.dfd.resolve(response);
	},
	failureNotifier : function(response) {
		/**/ 
		console.warn('Operation on cart FAILED!');
		this.requestCounter --;
		this.dfd.reject(response);
	},
	exceptionNotifier : function(response) {
		/**/ 
		console.error('Operation on cart FAILED TO REQUEST!');
		this.requestCounter --;
		this.dfd.reject(response);
	},
	getCart : function() {
		/*refresh cart, call cart renderer ShoppingCartAction-->getCart */
		var url = this.URL + "?action=getCart&orderType=" + this.orderType;
		return this.updateCart(url);
	},
	clearCart : function() {
		this.lastUpdatedProductId = 0;
		this.lastUpdatedLineId = 0;
		this.selectedIndex = -1;
		var url = this.URL + "?action=clearCart&orderType=" + this.orderType + "&date=" + DateUtil.getCurrentDate();
		return this.updateCart(url);	 
	},
	
	addToCart : function(productId, qty, description, price, modifiers, warehouseId, manualEntry, attributesetInstanceId ) {
		/*this.lastUpdatedLineId = productId;*/
		var url = this.URL + "?action=addToCart&orderType=" + this.orderType + "&productId=" + productId + "&qty=" + qty 
			+ "&date=" + DateUtil.getCurrentDate();
		
		if(description) url = url + '&description=' + description;
		if(price) url = url + '&price=' + price;
		
		if(attributesetInstanceId) url = url + '&attributesetInstanceId=' + attributesetInstanceId;
		
		/* see order-screen.jsp line 1239 */
		if(modifiers){
		
			url = url + '&modifiers=';
			for(var i=0; i<modifiers.length; i++){
				if(i>0) url = url + ',';
				url = url + modifiers[i].modifierId;
			}
		}
		
		if(warehouseId) url = url + '&warehouseId=' + warehouseId;
		
		if(manualEntry){
			url = url + '&manualEntry=' + manualEntry;
		}
		
		return this.updateCart(url);
		
	},
	removeFromCart : function(lineId) {
		var url = this.URL + "?action=removeFromCart&orderType=" + this.orderType + "&lineId=" + lineId + "&date=" + DateUtil.getCurrentDate();
		return this.updateCart(url);	 
	},
	incrementQty : function(lineId) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=incrementQty&orderType=" + this.orderType + "&lineId=" + lineId;
		return this.updateCart(url);	 
	},
	decrementQty : function(lineId) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=decrementQty&orderType=" + this.orderType + "&lineId=" + lineId;
		return this.updateCart(url);	 
	},
	updateQty : function(lineId, qty, warehouseId) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=updateQty&orderType=" + this.orderType + "&lineId=" + lineId + "&qty=" + qty + "&date=" + DateUtil.getCurrentDate();
		if (warehouseId) url = url + "&warehouseId=" + warehouseId;
		return this.updateCart(url);	 
	},
	splitLines : function(lineId, qty) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=splitLines&orderType=" + this.orderType + "&lineId=" + lineId + "&qty=" + qty;
		return this.updateCart(url);	 
	},
	setDiscountOnLine : function(lineId, amt){
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=setDiscountOnLine&orderType=" + this.orderType + "&lineId=" + lineId + "&discountAmt=" + amt;
		return this.updateCart(url);	
	},
	setDiscountOnTotal : function(amt){
		var url = this.URL + "?action=setDiscountOnTotal&orderType=" + this.orderType + "&discountAmt=" + amt;
		return this.updateCart(url);	
	},
	setTax : function(lineId, taxId){
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=setTax&orderType=" + this.orderType + "&lineId=" + lineId + "&taxId=" + taxId;
		return this.updateCart(url);	
	},
	setBp : function(bpId){
		this.bpId = bpId;
		var url = this.URL + "?action=setBPartner&orderType=" + this.orderType + "&bpartnerId=" + bpId;
		return this.updateCart(url);	
	},
	setPriceList : function(pricelistId){
		this.pricelistId = pricelistId;
		var url = this.URL + "?action=setPriceList&orderType=" + this.orderType + "&pricelistId=" + pricelistId;
		return this.updateCart(url);	
	},
	resetDiscounts : function(){
		var url = this.URL + "?action=resetDiscounts&orderType=" + this.orderType;
		return this.updateCart(url);	
	},
	applyDiscountCode : function(lineId, discountCodeId){
		var url = this.URL + "?action=applyDiscountCode&orderType=" + this.orderType + "&lineId=" + lineId + "&discountCodeId=" + discountCodeId;
		return this.updateCart(url);	
	},
	
	resetLineDiscountCode: function(lineId){
		var url = this.URL + "?action=resetLineDiscountCode&orderType=" + this.orderType + "&lineId=" + lineId;
		return this.updateCart(url);	
	},
	
	setCurrentBoxNumber : function( boxNumber ){
		
		var url = this.URL + "?action=setCurrentBoxNumber&orderType=" + this.orderType + "&currentBoxNumber=" + encodeURIComponent(boxNumber);
		return this.updateCart(url);
	},
	
	addBehaviourToLines : function(){
		/*custom parser*/
		LOGGER.info('Adding behaviour to cart lines');
		
		if (this.isEmpty())
		{
			jQuery('#order-actions-button-container').hide();
			jQuery('#shopping-cart-column').hide();
			jQuery('#more-button').hide();
			jQuery('#empty-cart-button-container').show();
			
			jQuery('.disabled-when-cart-empty').attr("disabled", "disabled");
		}
		else
		{
			jQuery('#empty-cart-button-container').hide();
			jQuery('#shopping-cart-column').show();
			jQuery('#order-actions-button-container').show();
			jQuery('#more-button').show();
			
			jQuery('.disabled-when-cart-empty').removeAttr("disabled");
		}
		
		if(this.isEmpty())
		{
			if(ShoppingCartManager.quantityTextfield){
				ShoppingCartManager.quantityTextfield.value = '';
			}
			
			this.onChange();
			return;
		}
		
		if(this.selectedIndex < 0 
				|| this.selectedIndex == this.lines.length
				|| this.lines[this.selectedIndex].lineId != this.lastUpdatedLineId){
			this.selectedIndex = (this.lines.length - 1);
		}
						
		
		for(var i=0; i<this.lines.length; i++){
			var line = this.lines[i];
			var row = $('row' + (i+1));
			
			if(line.lineId == this.lastUpdatedLineId){
				this.selectedIndex = i;
			}
			
			if(row){
				line.element = row;
				row.onclick = this.setSelectedIndex.bind(this,i);
			}			
		}	
		
		this.renderLines();
	},
	setSelectedIndex : function(){
		var index = arguments[0];		
		this.selectedIndex = index;
		
		/*update qty textfield*/
		var currentLine  = this.lines[this.selectedIndex];
		var qty = currentLine.qty;		
		ShoppingCartManager.quantityTextfield.value = qty;
		
		
		this.renderLines();	
	},
	
	renderLines : function(){
		/*highlight active row*/
		for(var i=0; i<this.lines.length; i++){
			var currentLine = this.lines[i];
			
			var className = ((i%2 == 0) ? 'row even' : 'row odd');
			
			if(this.selectedIndex == i){
				var highlightColor = 'row shopping-cart-highlight';
				className = highlightColor;	
			}	
			
			var element = $('row' + (i+1));			
			if(!element) continue;
			
			element.className = className;
			
		}/*for*/ 		
		
		/*update qty textfield*/
		var currentLine  = this.lines[this.selectedIndex];
		var qty = currentLine.qty;	
		if(ShoppingCartManager.quantityTextfield){
			ShoppingCartManager.quantityTextfield.value = qty;
			
			//shoppingCartChangeNotifier();		
			this.onChange();
			
			ShoppingCartManager.quantityTextfield.select();
		}
	},
	isEmpty : function(){
		return (this.lines == null || this.lines.length == 0);
	},
	initializeShortcuts : function(){
		/*Add shortcut keys to shopping cart*/
		/*
		 * CTRL+UP move up
		 * CTRL+DOWN move down
		*/
		if (typeof shortcut != "undefined") {
			shortcut.add("Ctrl+Up", this.moveUp.bind(this));
			shortcut.add("Ctrl+Down", this.moveDown.bind(this));
		}
		
	},
	moveDown : function(){		
		if(this.selectedIndex < (this.lines.length - 1))
		{
			this.selectedIndex ++;
			this.renderLines();
		}
	},
	moveUp : function(){
		if(this.selectedIndex > 0)
		{
			this.selectedIndex --;
			this.renderLines();
		}
	},
	onChange : function(){
		alert(Translation.translate('onchange.not.initialized','onChange not initialized'));
	},
	
	onUpdate : function(fn){
		this.callbacks.add(fn);
	}
});


var ShoppingCartManager = {
		/* loop lines for products that needs age verification */
		needAgeVerification : function(){
			var cart = this.getCart();
			var lines = cart.lines;
			
			if(lines && lines.length > 0){
				for(var i=0; i<lines.length; i++){
					var line = lines[i];
					if(line.isAgeverified == true){
						return true;
					}
				}
			}			
			
			return false;
		},
		
		getCart : function(){
			return shoppingCart;
		},
		
		refreshCart : function(){
			this.getCart().getCart();
		},
		
		clearCart : function(){
			this.getCart().clearCart();
		},
		
		getOrderId : function(){
			return this.getCart().orderId;
		},
		
		getOrderType : function(){
			return this.getCart().orderType;
		},
		
		getLineId : function(){
			var lineId = null;
			
			var cart = this.getCart();
			
			if(!this.isShoppingCartEmpty()){
				lineId = cart.lines[cart.selectedIndex].lineId;
			}
			
			return lineId;
		},
		
		scrollUp : function(){
			this.getCart().moveUp();
		},
		
		scrollDown : function(){
			this.getCart().moveDown();
		},
		
		addToCart : function(productId){
			if(productId == null) return;			
			this.getCart().addToCart(productId,1);
		},
		
		incrementQty : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			var currentLine  = cart.lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
			
			/* check for gift card */
			if( currentLine.isGift && cart.isSoTrx ){
				alert(Translation.translate('operation.not.allowed.cannot.change.gift.card.quantity','Operation not allowed! Cannot change gift card quantity!'));
				return;
			}
			
			this.getCart().incrementQty(lineId);
		},
		
		decrementQty : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			this.getCart().decrementQty(lineId);
		},
		
		updateQty : function(qty){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			
			var currentLine  = cart.lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
			
			/* check for gift card */
			if( currentLine.isGift && cart.isSoTrx ){
				if(qty > 1){
					alert(Translation.translate('operation.not.allowed.cannot.change.gift.card.quantity','Operation not allowed! Cannot change gift card quantity!'));
					return;
				}
			}
			
			if(currentLine.qty != qty){ /*avoid server hits*/
				
				var select = jQuery('#warehouse-dropdown')
				if (select.length == 0){
					cart.updateQty(lineId, qty);
				} else {
					cart.updateQty(lineId, qty, select.val());
				}
				
			}
			
			/*cart.updateQty(lineId, qty);*/
		},
		
		splitLines : function(qty){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			
			var currentLine  = cart.lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
						
			cart.splitLines(lineId, qty);
		},
		
		removeFromCart : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			this.getCart().removeFromCart(lineId);
		},
		
		deleteLine : function(lineId){
			if(lineId == null) return;
			this.getCart().removeFromCart(lineId);
		},
		
		setLineTax : function(taxId){
			var lineId = this.getLineId();
			if(lineId != null && taxId != null) this.getCart().setTax(lineId, taxId);
		},
		
		setBp : function(bpId){
			return this.getCart().setBp(bpId);
		},
		
		getGrandTotal : function(){
			return this.getCart().grandTotal;
		},
		
		getSubTotal : function(){
			return this.getCart().subTotal;
		},
		
		getDiscountOnTotal : function(){
			return this.getCart().discountOnTotal;
		},
		
		getTaxTotal : function(){
			return this.getCart().taxTotal;
		},
		
		getUpsellTotal:function(){
			return this.getCart().upsellTotal;
		},
		
		getLineNetTotal : function(){
			var lineNetTotal = ShoppingCartManager.getSubTotal() + ShoppingCartManager.getTaxTotal();
			return lineNetTotal;
		},
		
		getQtyTotal : function(){
			return this.getCart().qtyTotal;
		},
		
		getCurrencySymbol : function(){
			return this.getCart().currencySymbol;
		},
		
		isShoppingCartEmpty : function(){
			return this.getCart().isEmpty();
		},
		
		isShoppingCartReady : function(){
			return (this.getCart().requestCounter <= 0); /*negative count bug*/
		},
		
		setDiscountOnTotal : function(discountAmt){			
			this.getCart().setDiscountOnTotal(discountAmt);
		},
		
		setDiscountOnLine : function(discountAmt){
			var cart = this.getCart();
			
			if(cart.lines && cart.lines.length > 0){
				var lineId = cart.lines[cart.selectedIndex].lineId;
				cart.setDiscountOnLine(lineId,discountAmt);
			}
		},
		
		applyDiscountCode : function( discountCodeId ){
			
			var cart = this.getCart();
			
			if(cart.lines && cart.lines.length > 0){
				var lineId = cart.lines[cart.selectedIndex].lineId;
				cart.applyDiscountCode(lineId, discountCodeId);
			}
			
		},
		
		resetLineDiscountCode : function(){
			
			var cart = this.getCart();
			
			if(cart.lines && cart.lines.length > 0){
				var lineId = cart.lines[cart.selectedIndex].lineId;
				cart.resetLineDiscountCode(lineId);
			}
			
		},
		
		resetDiscounts : function(){
			var cart = this.getCart();
			cart.resetDiscounts();
		},
		
		initializeComponents : function(){
			
			/*this.scrollUpButton = $('scroll-up-button');
			this.scrollDownButton = $('scroll-down-button');*/
			this.clearButton = $('clear-button');
			/*this.addButton = $('add-button');
			this.decreaseButton = $('decrease-button');*/
			this.removeButton = $('remove-button');
			this.productInfoButton = $('product-info-button');
			this.splitLineButton = $('split-line-button');
			
			this.quantityTextfield = $('quantity-texfield');
			
			/* add behaviour */	
			/*this.scrollUpButton.onclick = function(e){
				ShoppingCartManager.scrollUp();
			};
			
			this.scrollDownButton.onclick = function(e){
				ShoppingCartManager.scrollDown();
			};*/
			
			this.clearButton.onclick = function(e){
				ShoppingCartManager.clearCart();
			};
			
			/*this.addButton.onclick = function(e){
				ShoppingCartManager.incrementQty();
			};*/
			
			/*this.decreaseButton.onclick = function(e){
				ShoppingCartManager.decrementQty();
			};*/
			
			if(this.removeButton){
				this.removeButton.onclick = function(e){
					ShoppingCartManager.removeFromCart();
				};
			}
			
			if(this.productInfoButton){				
				this.productInfoButton.onclick = function(e){
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert(Translation.translate('cart.is.empty','Cart is empty!'));
						return;
					}
					
					var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
					var productId = currentLine.productId;
					
					new ProductInfoPanel().getInfo(productId);
				};
			}
			
			if(this.splitLineButton){
				this.splitLineButton.onclick = function(e){
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert(Translation.translate('cart.is.empty','Cart is empty!'));
						return;
					}
						
					var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
					var currentLineQty = currentLine.qty;
					
					if(currentLineQty == 1) return;
						
					var qty = window.prompt('Enter qty', '1');
					qty = parseFloat(qty);
					
					if(isNaN(qty)){
						alert(Translation.translate('invalid.qty','Invalid Qty!'));
						return;					
					}
					
					if(qty >= currentLineQty){
						alert('Qty entered must be less than ' + currentLineQty + '!');						
						return;	
					}
					
					ShoppingCartManager.splitLines(qty);	
				};
			}
			
			/* add keypad to qty textfield */
			/*Event.observe(this.quantityTextfield,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			/* bug fix for updateQtyTextField */
			this.quantityTextfield.onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					if(!ShoppingCartManager.isShoppingCartEmpty()){
						var qty = parseFloat(this.value);
						
						if(isNaN(qty)){
							alert(Translation.translate('invalid.qty','Invalid Qty!'));
							this.selectAll();								
							return;					
						}
						ShoppingCartManager.updateQty(qty);							
					}
				}
			}
		}
		
};
/**
 * Small pricing model to calculate prices based on priceList, 
 * priceStd, priceLimit and priceEntered
 */
 var PPricing = Class.create({
	 initialize: function(priceEntered, priceStd, priceList, priceLimit, qty, taxRate, isTaxIncluded)
	 {
	 	this.pricePrecision = 2;
	 	this.priceEntered = priceEntered;
	 	this.priceStd = priceStd;
	 	this.priceList = priceList;
	 	this.priceLimit = priceLimit;	 	
	 	this.qty = qty;
	 	this.taxRate = taxRate;
	 	this.isTaxIncluded = isTaxIncluded;
	 	this.discount = 0; /*discount percentage*/
	 	
	 	/*Bug fix
	 	 * If user forgot to set list & limit prices set them to std*/
	 	if(this.priceList == 0.0) this.priceList = this.priceStd;
	 	if(this.priceLimit == 0.0) this.priceLimit = this.priceStd;
	 },
	 
	 setPrecision : function(price)
	 {
		 var p = new Number(price).toFixed(this.pricePrecision);
		 
		 return parseFloat(p);
	 },
	 
	 getPrice : function(price, taxIncluded)
	 {
		 var newPrice  = 0;
		 
		 if(this.isTaxIncluded == taxIncluded){
			 newPrice = price;
		 }
		 else{
			 /*Price without tax*/
			 if(this.isTaxIncluded && !taxIncluded)		
			 {
				 newPrice = this.calculatePrice(price, true);
			 }
			 else
			 {
				 newPrice = this.calculatePrice(price, false);
			 }
		 }
		 
		 newPrice = this.setPrecision(newPrice);		 
		 return newPrice;

	 },
	 
	 getDiscount : function()
	 {
		 var discount = this.priceList - this.priceEntered;
		 
		 if(this.priceList == 0) return 0.0; /*prevent divide by zero error*/
		 
		 discount = (discount/this.priceList)*100;		 
		 return this.setPrecision(discount);
	 },
	 
	 setDiscount : function(discount)
	 {		 
		 discount = parseFloat(discount);
		 
		 this.discount = discount;
		 
		 if(isNaN(discount)){
			 //alert(messages.get('js.invalid.discount'));
			 discount = 0.0;
		 }
		 
		 if(discount > 100.0)
		 {
			 //alert(messages.get('js.invalid.discount'));
			 discount = 100.0;
		 }
		 		 
		 var newPrice = (this.priceList*(100.00 - discount))/100.00;
		 newPrice = this.setPrecision(newPrice);
		 
		 //round price		 
		 this.priceEntered = newPrice;
	 },
	 
	 getTotal : function()
	 {
		 return this.priceEntered * this.qty;
	 },
	 
	 	 
	 calculatePrice : function(price, isTaxIncluded)
	 {
		 var newPrice = 0;
		 
		 //if price includes tax
		 if(isTaxIncluded)
		 {
			 newPrice = (price * 100)/(100 + this.taxRate);
		 }
		 else
		 {
			 newPrice = price + ((price*this.taxRate)/100);
		 }
		 
		 newPrice = this.setPrecision(newPrice);		 
		 return newPrice;
		 
	 },
	 
	 getDiscountAmt : function()
	 {
		 var discountAmt = (this.priceList - this.priceEntered) * this.qty;
		 
		 discountAmt = this.setPrecision(discountAmt);		 
		 return discountAmt;
	 },
	 
	 getPriceEntered : function()
	 {
		 return this.priceEntered;
	 }
	 
 });
 
 var DiscountPanel = Class.create(PopUpBase, {
	 
	 initialize:function(){
		 	this.createPopUp($('lineItemDiscountPanel'));
		 	this.unit_tf = $('unit');
		 	this.total_tf = $('total');
		 	this.discount_percentage_tf = $('discount.percentage');
		 	this.discount_resetBtn = $('discount.resetBtn');	
		 	this.discount_applyBtn = $('discount.applyBtn');
		 	
		 	
		 	
		 	this.discount_cancelBtn = $('discount.cancel.button');
		 	//this.discount_total_cancelBtn = $('discount.total.cancel.button');
		 	this.discount_cancelBtn.onclick = this.hide.bind(this);
		 	/*this.discount_total_cancelBtn.onclick = function(e){
		 		this.hide();
		 	}; */
		 	
		 	/* add keypad */
		 	/*Event.observe(this.unit_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		 	Event.observe(this.total_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		 	Event.observe(this.discount_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			this.discount_percentage_tf.panel = this;
			this.discount_percentage_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscount();
				}
				
				this.panel.pp.setDiscount(this.value);
				this.panel.render(this);
				this.focus();
				//this.select();
			};	
			
			
			this.unit_tf.panel = this;
			this.unit_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscount();
				}
				
				var priceList = parseFloat(this.panel.pp.priceList);
				var oldValue = this.panel.pp.getPrice(priceList, false);
				var newValue = parseFloat(this.value);
				
				var discount = ((oldValue-newValue)/oldValue)*100;
				
				this.panel.pp.setDiscount(discount);
				this.panel.render(this);
			};
			
			this.total_tf.panel = this;
			this.total_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscount();
				}
				
				var totalPrice = parseFloat(this.panel.pp.priceList * this.panel.pp.qty);
				var oldValue = this.panel.pp.getPrice(totalPrice, false);
				var newValue = parseFloat(this.value);
				
				var discount = ((oldValue-newValue)/oldValue)*100;
				
				this.panel.pp.setDiscount(discount);
				this.panel.render(this);
			};	
			
			this.discount_resetBtn.panel = this;
			this.discount_resetBtn.onclick = function(e){
				this.panel.reset();
			};		
			
			this.discount_applyBtn.panel = this;
			this.discount_applyBtn.onclick = function(e){
				this.panel.applyDiscount();
			};
			
			/* discount on total*/
			
			this.discountOnTotal_percentage_tf = $('discountOnTotal.percentageTextfield');
			this.discountOnTotal_amount_tf = $('discountOnTotal.amountTextfield');
			this.discountOnTotal_total_tf = $('discountOnTotal.totalTextfield');
			
			this.discountOnTotal_applyBtn = $('discountOnTotal.applyButton');	
			this.discountOnTotal_resetBtn = $('discountOnTotal.resetButton');
			
			/* add keypad */
			/*Event.observe(this.discountOnTotal_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
			Event.observe(this.discountOnTotal_amount_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
			Event.observe(this.discountOnTotal_total_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			
			this.discountOnTotal_percentage_tf.panel = this;
			this.discountOnTotal_percentage_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscountOnTotal();
				}
				
				var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
				
				var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
				baseAmt = baseAmt + discountOnTotal;
				
				var percentage = this.panel.discountOnTotal_percentage_tf.value;
				percentage = parseFloat(percentage);
				
				if(isNaN(percentage)){
					percentage = 0.0;
				}
				
				var discountAmt = (baseAmt * percentage)/100.0;
				discountAmt = PriceManager.setPrecision(discountAmt, 2);
				
				var baseAmt = baseAmt - discountAmt;
				baseAmt = PriceManager.setPrecision(baseAmt, 2);
				
				this.panel.discountOnTotal_amount_tf.value = discountAmt;
				this.panel.discountOnTotal_total_tf.value = baseAmt;
				
			};
			
			this.discountOnTotal_amount_tf.panel = this;
			this.discountOnTotal_amount_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscountOnTotal();
				}
				
				var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
				
							
				var discountAmt = this.panel.discountOnTotal_amount_tf.value;
				discountAmt = parseFloat(discountAmt);
				
				if(isNaN(discountAmt)){
					discountAmt = 0;
				}
				
				discountAmt = PriceManager.setPrecision(discountAmt, 2);
				
				var percentage = 0;
				
				if(baseAmt != 0){
					percentage = (discountAmt * 100)/baseAmt;
				}
				
				percentage = PriceManager.setPrecision(percentage, 2);
				
				baseAmt = baseAmt - discountAmt;
				baseAmt = PriceManager.setPrecision(baseAmt, 2);
				
				this.panel.discountOnTotal_percentage_tf.value = percentage;
				this.panel.discountOnTotal_total_tf.value = baseAmt;
			};
			
			this.discountOnTotal_total_tf.panel = this;
			this.discountOnTotal_total_tf.onkeyup = function(e){
				
				if(e.keyCode == 13){
					this.panel.applyDiscountOnTotal();
				}
				
				var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
				
				var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
				baseAmt = baseAmt + discountOnTotal;
				
				var amtEntered = this.panel.discountOnTotal_total_tf.value;
				
				amtEntered = parseFloat(amtEntered);
				
				if(isNaN(amtEntered)){
					amtEntered = 0;
				}
				
				amtEntered = PriceManager.setPrecision(amtEntered, 2);
				
				var discountAmt = baseAmt - amtEntered;
				var percentage = 0;
				
				if(baseAmt != 0){
					percentage = (discountAmt * 100)/baseAmt;
				}				
				
				discountAmt = PriceManager.setPrecision(discountAmt, 2);
				percentage = PriceManager.setPrecision(percentage, 2);
				
				this.panel.discountOnTotal_percentage_tf.value = percentage;
				this.panel.discountOnTotal_amount_tf.value = discountAmt;			
			};
			
			
			this.discountOnTotal_amount_tf.panel = this;
			this.discountOnTotal_total_tf.panel = this;
			
			this.discountOnTotal_applyBtn.panel = this;
			this.discountOnTotal_applyBtn.onclick = function(e){			
				this.panel.applyDiscountOnTotal();
			};
			
			this.discountOnTotal_resetBtn.panel = this;
			this.discountOnTotal_resetBtn.onclick = function(e){
				this.panel.resetDiscountOnTotal();
			};
			
			
			var keypadButtons = $$('#lineItemDiscountPanel .keypad-button');
            for(var i=0; i<keypadButtons.length; i++){
                var btn = keypadButtons[i];
                btn.onclick = this.keypadHandler.bind(this);
            }
            
            var inputs = $$('#lineItemDiscountPanel .numeric-input');
            for(var i=0; i<inputs.length; i++){
            	Event.observe(inputs[i],'click',this.clickHandler.bindAsEventListener(this),false);
            }           
            
	 	},
	 	
	 	clickHandler : function(e){
	 		var element = Event.element(e);
	 		this.textField = element;
	 		this.textField.focus();
	 		this.textField.select();
	 		
	 		/*this.textField.onkeyup = function(e){
	 			if (isNaN(this.value))
				{
	 				this.value = '';
					return;
				}
	 		}*/
	 	},
	 	
	 	keypadHandler:function(e){
	 		var button = Event.element(e); 
	        var element = button.getElementsByClassName('keypad-value')[0];
	        
	        /*Fix for google chrome*/
	          if (element == null)
	          {
	          		element = button;  
	          }
	          /**********************/
	          
	        var value = element.innerHTML;
                         
            var inputValue = this.textField.value;
            
            if (this.textField.selectionStart != this.textField.selectionEnd)
            {
            	inputValue = '';
				this.textField.value = '';
				this.textField.focus();
            }
            
            //if(inputValue == '') inputValue = 0;
            
			if('Del' == element.innerHTML.strip()){
				
				if(inputValue.length > 0)
				{
					inputValue = inputValue.substring(0,inputValue.length-1);
				}
            }
			else if('-' == element.innerHTML.strip()){
				
				if(inputValue.indexOf('-') == -1){
					inputValue = '-' + inputValue;
				}
				else{
					inputValue = inputValue.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(inputValue.indexOf('.') == -1)
				{
					inputValue = inputValue + value;
				}
			}
			
			else{
				inputValue = inputValue + value;
			}
				
			this.textField.value = inputValue;
				
             
              if(isNaN(inputValue) || (inputValue < 0.0)){           
                  return;
              }
             
              var onkeyup = this.textField.onkeyup;
	  			if(onkeyup){
	  				var event = {keyCode:65};
	  				onkeyup.call(this.textField,event);
	  			}
        },
	 
	 onShow:function(){		
	 
		 if(ShoppingCartManager.isShoppingCartEmpty()) return;
		 
		 if(!DiscountManager.isDiscountOnLineAllowed()) return;
		 
		 if(!DiscountManager.isDiscountOnTotalAllowed())
		 {
			/*hide discount on total panel*/
			DiscountManager.hideDiscountOnTotalPanel();
		 }
		 else
		 {
			DiscountManager.showDiscountOnTotalPanel();
		 }		
		
		this.loadLineDetails();
		this.renderDiscountRightsInfo();
		
		setTimeout(function(){
			$('unit').focus();
			$('unit').select();
		},500);
		
		this.textField = this.unit_tf;
		
		
	 },
	 
	 /*hide:function(){
		 $('discountContainer').hide();
		 $('discountRights').style.display = 'none';
		 $('item-details-panel').style.display = 'none';
		 $('discountOnTotalContainer').style.display = 'none';
		 $('shopping-cart-panel').show();	
		 $('glider-column').show();
		// $$('.sidebar-main-button')[0].show();
		 
		 this.visible = false;
	 },*/
 
	 
 	
 	applyDiscount:function(){
 		
 		//User discount is not allowed in case of customer discount code or product discount code
		var bp = BPManager.getBP();
		var line = shoppingCart.lines[shoppingCart.selectedIndex];
		 
		 if(bp != null)
		 {
			 var customerDiscountCodeId = bp['u_pos_discountcode_id'];
			 
			 if (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0 && line.isDiscountCodeApplied)
			 {
				 alert('Business partner has discount code. User discount is not possible.');
				 return;
			 }
		 }		 
		 
		 var discountCode = line.discountCode;
		 
        if( discountCode != null && line.isDiscountCodeApplied)
        {
        	alert('Product has discount code. User discount is not possible.');
			return;
        }
			 
 		/*get discount percentage*/
 		var discountPercentage = this.pp.getDiscount();
 		
 		var rights = DiscountManager.getDiscountRights();
 		var allowUpSell = rights.allowUpSell;
 		
 		if(!DiscountManager.isLineDiscountValid(discountPercentage))
 		{
 			return;
 		} 		
 		
 		if(allowUpSell){
 			//do nothing
 		}
 		else
 		{
 			if(!DiscountManager.overrideLimitPrice()){
 	 			var priceEntered = parseFloat(this.pp.priceEntered);
 	 	 		var priceLimit = parseFloat(this.pp.priceLimit);
 	 	 		
 	 	 		if(priceLimit > priceEntered){
 	 	 			alert('The Price Limit [' + priceLimit + '] has been exceeded by the Price Entered: ' + priceEntered);
 	 				return;
 	 	 		}
 	 		}
 		}
 		
 		
 		/*get discount amt*/
		var discountAmt = this.pp.getDiscountAmt();
		discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
		discountAmt = parseFloat(discountAmt);
		
		if(isNaN(discountAmt))
		{
			alert(Translation.translate('error.invalid.discount.amount') + ': ' + discountAmt);
			return;
		}
    		
		ShoppingCartManager.setDiscountOnLine(discountAmt);	
		this.hide();
 	},
 	
 	applyDiscountOnTotal:function(){
 		/*validate discount limit*/ 
		
		var discountPercentage = this.discountOnTotal_percentage_tf.value			
		if(!DiscountManager.isDiscountValid(discountPercentage)){
			return;
		}
		
		var discountAmt = this.discountOnTotal_amount_tf.value;
		discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
		
		if(discountAmt < 0.0){
			alert(Translation.translate('discount.amount.cannot.be.negative','Discount Amount cannot be negative!'));
			return;
		}
		
		var previousDiscountOnTotal = ShoppingCartManager.getDiscountOnTotal();
		if(previousDiscountOnTotal != 0){
			alert(Translation.translate("you.have.already.given.discount.on.total.please.clear.previous.discount.on.total.click.reset.to.clear.previous.discounts","You have already given discount on total! Please clear previous discount on total! Click reset to clear previous discounts!"));
			return;
		}
		
		ShoppingCartManager.setDiscountOnTotal(discountAmt);
		this.hide();
 	},
 	
 	loadLineDetails:function()
 	{
 		if(ShoppingCartManager.isShoppingCartEmpty()) return;
 		
 		var line = shoppingCart.lines[shoppingCart.selectedIndex];
		
 		var priceEntered = line.priceEntered;
		var priceList = line.priceList;
		var priceStd = line.priceStd;
		var priceLimit = line.priceLimit;
		var isTaxIncluded = line.isTaxIncluded;
		var taxRate = line.taxRate;
		var qty = line.qty;
		
		/*Parse values*/
		isTaxIncluded = new Boolean(isTaxIncluded);
		taxRate = parseFloat(taxRate);
		priceList = parseFloat(priceList);
		priceStd = parseFloat(priceStd);
		priceLimit = parseFloat(priceLimit);
		qty = parseFloat(qty);
		
		this.pp = new PPricing(priceEntered,priceStd,priceList,priceLimit,qty,taxRate,isTaxIncluded); 
		
		this.render(null);
		
		if(DiscountManager.isDiscountOnTotalAllowed())
		{
			/* render discount on total */
			this.renderDiscountOnTotal();
		}
		
		/* discount codes */
        var discountCodes = line.discountCodes;
        var ref = this;
        var discountCode;
        
        if( discountCodes.length > 0 ){
        	
        	jQuery('#discount-code-container').show();
        	
        	var html = "<option value=''></option>";
        	
        	for(var i=0; i<discountCodes.length; i++){
        		
        		discountCode = discountCodes[i];
        		
        		html += "<option value='" + discountCode['id'] + "'>" + discountCode['name']  + "</option>"
        	}
        	
        	jQuery('#discount-code-select').html(html);
        	
        	//populate discount code
        	if(line.isDiscountCodeApplied && line.discountCode ){
        		jQuery('#discount-code-select').val(line.discountCode.id);
        	}
        	
        	var btn = jQuery('#discount-code-apply-button')[0];
        	btn.onclick = function(){
        		
        		var id = jQuery('#discount-code-select').val();
        		
        		if(id == ''){
        			
        			alert('Please select a discount code');
        			return;
        			
        		}
        		
        		ShoppingCartManager.applyDiscountCode( id );
        		
        		ref.hide();
        		
        	};
        	
        	var btn = jQuery('#discount-code-reset-button')[0];
        	btn.onclick = function(){
        		
        		var bp = BPManager.getBP();
        		
        		if(bp != null)
	       		 {
	       			 var customerDiscountCodeId = bp['u_pos_discountcode_id'];
	       			 
	       			 if (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0 && line.isDiscountCodeApplied)
	       			 {
	       				 alert('Business partner has discount code. User discount is not possible.');
	       				 return;
	       			 }
	       		 }
        		
        		ShoppingCartManager.resetLineDiscountCode();
        		
        		ref.hide();
        		
        	};
        }
        else
        {
        	jQuery('#discount-code-container').hide();
        }
		
 	},
 	
 	isDiscountValid : function(discount){
 		return DiscountManager.isDiscountValid(discount);
 	},
 	
 	renderDiscountRightsInfo:function(){
 		
 		var rights = DiscountManager.getDiscountRights();
 		
 		//$('discountUpToLimitPriceLabel').innerHTML = rights.discountUpToLimitPrice ? 'Yes' : 'No';
 		$('overrideLimitPriceLabel').innerHTML = rights.overrideLimitPrice ? Translation.translate('yes') : Translation.translate('no');
 		$('allowDiscountOnTotalLabel').innerHTML = rights.allowDiscountOnTotal ? Translation.translate('yes') : Translation.translate('no');
 		$('discountLimitLabel').innerHTML = rights.discountLimit + '%';
 		
 		if(rights.allowOrderBackDate && OrderScreen.backDateButton){
 			OrderScreen.backDateButton.show();
		 }
 	},
 	
 	resetDiscountOnTotal:function(){
 		var subTotal = ShoppingCartManager.getSubTotal();
 		subTotal = parseFloat(subTotal.toFixed(2));
 		
 		subTotal = PriceManager.setPrecision(subTotal, 2);		
		this.discountOnTotal_amount_tf.value = 0;
		this.discountOnTotal_percentage_tf.value = 0;
		this.discountOnTotal_total_tf.value = subTotal;
		
		if(window.confirm(Translation.translate("confirmation.reset.discount"))){
			/* clear previous discounts */
			ShoppingCartManager.setDiscountOnTotal(0);
		}		
		
 	},
 	
 	renderDiscountOnTotal:function(){
 		
 		var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
		
		var discountAmt = ShoppingCartManager.getDiscountOnTotal();
 		var percentage = (discountAmt * 100)/(baseAmt + discountAmt);
 		
 		if(isNaN(percentage)) percentage = 0;
 		
		percentage = PriceManager.setPrecision(percentage, 2);
		discountAmt = PriceManager.setPrecision(discountAmt, 2);
		
		this.discountOnTotal_amount_tf.value = discountAmt;
		this.discountOnTotal_percentage_tf.value = percentage;
		this.discountOnTotal_total_tf.value = baseAmt;
 	},
 	
 	render:function(elementNotToRender)
 	{
 		var unitPrice = this.pp.priceEntered;
		var totalPrice = this.pp.getTotal();
		
		if(elementNotToRender != this.unit_tf)
		this.unit_tf.value = Number(this.pp.getPrice(unitPrice, false)).toFixed(2);
		
		/*if(elementNotToRender != this.unit_incl_tf)
		this.unit_incl_tf.value = Number(this.pp.getPrice(unitPrice, true)).toFixed(2);	*/
		
		if(elementNotToRender != this.total_tf)
		this.total_tf.value = Number(this.pp.getPrice(totalPrice, false)).toFixed(2);
		
		/*if(elementNotToRender != this.total_incl_tf)
		this.total_incl_tf.value = Number(this.pp.getPrice(totalPrice, true)).toFixed(2);*/
		
		if(elementNotToRender != this.discount_percentage_tf)
		this.discount_percentage_tf.value = Number(this.pp.getDiscount()).toFixed(2);
		
		
 	},
 	
 	reset:function(){
 		this.pp.priceEntered = this.pp.priceStd;
 		this.render(null);
 	}
 });
 
 

 
 var DiscountManager = {
 		
 		defaultDiscountRights:null,
 		discountRights:null,
 		
 		discountPanel:null,
 		discountOnTotalPanel:null,
 		
 		getDiscountPanel : function(){
 			
 			if (this.discountPanel == null)
 			{
 				alert(Translation.translate('discount.limit.has.not.been.set','Discount Limit has not been set!'));
 			}
 			else
			{
 				this.discountPanel.show();
			}
 		},
 		
 		getDiscountOnTotalPanel : function(){
 			
 			//Customer discount code overrides all other discounts
 			var bp = BPManager.getBP();
 			 
 			 if(bp != null)
 			 {
 				 var customerDiscountCodeId = bp['u_pos_discountcode_id'];
 				 
 				 if (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0)
 				 {
 					 alert('Business partner has discount code. All other discounts are not applicable.');
 					 return;
 				 }
 			 }
 			
 			if (this.discountOnTotalPanel == null)
 			{
 				alert(Translation.translate('discount.on.total.denied','Discount on total denied!'));
 			}
 			else
			{
 				if (ShoppingCartManager.isShoppingCartEmpty())
 				{
 					return;
 				}
 				this.discountOnTotalPanel.show();
			}
 		},
 		
		 /* set discount amt on cart total */
		 setDiscountOnTotal : function(discountAmt){
			 ShoppingCartManager.setDiscountOnTotal(discountAmt);
		 },
		 
		 isDiscountOnTotalAllowed : function(){
			 
			 var rights = DiscountManager.getDiscountRights();	
			 
			 return rights.allowDiscountOnTotal;
		 },
		 
		 isDiscountOnLineAllowed : function(){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
					 
			 if(rights.discountLimit > 0.0) return true;
			 
			 var msg = messages.get(Translation.translate('discount.limit.has.not.been.set','Discount Limit has not been set!')); 
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 overrideLimitPrice : function(){
			 
			 var rights = DiscountManager.getDiscountRights();	
			 
			 return rights.overrideLimitPrice;
		 },
		 
		 allowOrderBackDate : function(){
			 var rights = DiscountManager.getDiscountRights();				 
			 return rights.allowOrderBackDate;
		 },
		 
		 showDiscountOnTotalPanel : function(){
			 /*$('discountOnTotalDetails').show();*/
			/* $('discount-button').style.display = 'inline';*/
			 
			 
		 },
		 
		 hideDiscountOnTotalPanel : function(){
			 /*$('discountOnTotalDetails').hide();*/
			/* $('discount-button').style.display = 'none';*/
		 },
		 
		 isDiscountValid : function(discountPercentage){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
			 
			 /* user can give discount */
			 if(discountPercentage <= rights.discountLimit) return true;
			 
			 /* throw error message*/
			 var msg = messages.get('Discount is greater than discount limit!');
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 isLineDiscountValid : function(discountPercentage){
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 /* user can upsell */
			 if(rights.allowUpSell) return true;
			 
			 var discountLimit = rights.discountLimit;
			 
			 var pp = this.discountPanel.pp;
			 
			 var initialDiscount = 0;
			 
			 if( pp.priceList != 0.0 && pp.priceList != pp.priceStd )
			 {
				 initialDiscount = ((pp.priceList - pp.priceStd) * 100)/ pp.priceList;
			 }
			 
			 if(rights.discountOnCurrentPrice)
			 {
				 //calculate new discount limit
				 var d1 = 100 - initialDiscount;
				 var d2 = 100 - discountLimit;
					
				 var d3 = (d1 * d2) / (100);
				 var d4 = 100 - d3;
				 
				 discountLimit = parseFloat(new Number(d4).toFixed(2));
			 }
			 else
			 {
				 if( initialDiscount > discountLimit ){
					 discountLimit = initialDiscount;
				 }
			 }
			 
			 /* user can give discount */
			 if(discountPercentage <= discountLimit) return true;
			 
			 /* throw error message*/
			 var msg = messages.get('Discount is greater than discount limit!');
			 this.reportError(msg);
			 
			 return false;
		 },
		 
		 getDiscountRights : function(){
			 return this.discountRights;
		 },
		 
		 setDiscountRights : function(discountRights){
			 if(this.defaultDiscountRights == null) this.defaultDiscountRights = discountRights;
			 this.discountRights = discountRights;
			 
			 /* notifies changes in discount rights */
			 this.discountRightsChangeNotifier();
		 },
		 
		 resetDiscountRights:function(){
		 	this.setDiscountRights(this.defaultDiscountRights);
		 },
		 
		 discountRightsChangeNotifier : function(){
			 /* notifies changes in discount rights */
			 
			/* if(!DiscountPanel.visible) return;*/
			 
			 var rights = DiscountManager.getDiscountRights();
			 
			 if(rights.discountLimit > 0.0){
				 this.discountPanel = new DiscountPanel();
			 }
			 
			 if(rights.allowDiscountOnTotal){
				 this.discountOnTotalPanel = new DiscountOnTotalPanel();
			 }
			 
			 if (this.discountPanel != null)
			 {
				 this.discountPanel.renderDiscountRightsInfo();
			 }
		 },
		 
		 reportError:function(err){
			 alert(err);
		 }
 };
 
var PriceManager = {
		setPrecision : function(price, precision){
			return parseFloat(new Number(price).toFixed(precision));
		}
};
 
var DiscountRights =  Class.create({
	initialize : function(discountLimit, overrideLimitPrice, discountUpToLimitPrice, allowDiscountOnTotal, allowOrderBackDate, allowUpSell, discountOnCurrentPrice){
		
		if(ORDER_TYPES.POS_GOODS_RETURNED_NOTE == ShoppingCartManager.getOrderType()){
			
			this.discountLimit = 100;
		}
		else{
			
			this.discountLimit = discountLimit;
		}
		
		this.overrideLimitPrice = overrideLimitPrice;
		this.discountUpToLimitPrice = !overrideLimitPrice;
		this.allowDiscountOnTotal = allowDiscountOnTotal;
		this.allowOrderBackDate = allowOrderBackDate;
		this.allowUpSell = allowUpSell;
		this.discountOnCurrentPrice = discountOnCurrentPrice;
	}
});


var DiscountOnTotalPanel = Class.create(PopUpBase, {
		
	initialize:function(){
		/* discount on total*/
		
		this.createPopUp($('discountOnTotalContainer'));
		this.cancelBtn = $('create.customer.popup.close.button');
		
		
		this.initialized = false;	
		 
		this.visible = false;
		
		this.discountOnTotal_percentage_tf = $('discountOnTotal.percentageTextfield');
		this.discountOnTotal_amount_tf = $('discountOnTotal.amountTextfield');
		this.discountOnTotal_total_tf = $('discountOnTotal.totalTextfield');
		
		this.discountOnTotal_applyBtn = $('discountOnTotal.applyButton');	
		this.discountOnTotal_resetBtn = $('discountOnTotal.resetButton');
		this.discount_total_cancelBtn = $('discount.total.cancel.button');
		
	 	this.discount_total_cancelBtn.onclick = this.hide.bind(this);
		
		/* add keypad */
		/*Event.observe(this.discountOnTotal_percentage_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(this.discountOnTotal_amount_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		Event.observe(this.discountOnTotal_total_tf,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
		
		
		this.discountOnTotal_percentage_tf.panel = this;
		this.discountOnTotal_percentage_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
			baseAmt = baseAmt + discountOnTotal;
			
			var percentage = this.panel.discountOnTotal_percentage_tf.value;
			percentage = parseFloat(percentage);
			
			if(isNaN(percentage)){
				percentage = 0.0;
			}
			
			var discountAmt = (baseAmt * percentage)/100.0;
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			
			var baseAmt = baseAmt - discountAmt;
			baseAmt = PriceManager.setPrecision(baseAmt, 2);
			
			this.panel.discountOnTotal_amount_tf.value = discountAmt;
			this.panel.discountOnTotal_total_tf.value = baseAmt;
			
		};
		
		this.discountOnTotal_amount_tf.panel = this;
		this.discountOnTotal_amount_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
						
			var discountAmt = this.panel.discountOnTotal_amount_tf.value;
			discountAmt = parseFloat(discountAmt);
			
			if(isNaN(discountAmt)){
				discountAmt = 0;
			}
			
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			
			var percentage = 0;
			
			if(baseAmt != 0){
				percentage = (discountAmt * 100)/baseAmt;
			}
			
			percentage = PriceManager.setPrecision(percentage, 2);
			
			baseAmt = baseAmt - discountAmt;
			baseAmt = PriceManager.setPrecision(baseAmt, 2);
			
			this.panel.discountOnTotal_percentage_tf.value = percentage;
			this.panel.discountOnTotal_total_tf.value = baseAmt;
		};
		
		this.discountOnTotal_total_tf.panel = this;
		this.discountOnTotal_total_tf.onkeyup = function(e){
			
			if(e.keyCode == 13){
				this.panel.applyDiscountOnTotal();
			}
			
			var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountOnTotal = ShoppingCartManager.getDiscountOnTotal();			
			baseAmt = baseAmt + discountOnTotal;
			
			var amtEntered = this.panel.discountOnTotal_total_tf.value;
			
			amtEntered = parseFloat(amtEntered);
			
			if(isNaN(amtEntered)){
				amtEntered = 0;
			}
			
			amtEntered = PriceManager.setPrecision(amtEntered, 2);
			
			var discountAmt = baseAmt - amtEntered;
			var percentage = 0;
			
			if(baseAmt != 0){
				percentage = (discountAmt * 100)/baseAmt;
			}				
			
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			percentage = PriceManager.setPrecision(percentage, 2);
			
			this.panel.discountOnTotal_percentage_tf.value = percentage;
			this.panel.discountOnTotal_amount_tf.value = discountAmt;			
		};
		
		
		this.discountOnTotal_amount_tf.panel = this;
		this.discountOnTotal_total_tf.panel = this;
		
		this.discountOnTotal_applyBtn.panel = this;
		this.discountOnTotal_applyBtn.onclick = function(e){			
			this.panel.applyDiscountOnTotal();
		};
		
		this.discountOnTotal_resetBtn.panel = this;
		this.discountOnTotal_resetBtn.onclick = function(e){
			this.panel.resetDiscountOnTotal();
		};
		
		this.initialized = true;
		
		var keypadButtons = $$('#discountOnTotalContainer .keypad-button');
        for(var i=0; i<keypadButtons.length; i++){
            var btn = keypadButtons[i];
            btn.onclick = this.keypadHandler.bind(this);
        }
        
        
        var inputs = $$('#discountOnTotalContainer .numeric-input');
        for(var i=0; i<inputs.length; i++){
        	Event.observe(inputs[i],'click',this.clickHandler.bindAsEventListener(this),false);
        }
 	},
		 
 	toggle:function(){		
	 	 this.visible = !this.visible;	
	 
		 if(!this.initialized) this.initialize();
		 
		 if(ShoppingCartManager.isShoppingCartEmpty()) return;
		 
		 if(!DiscountManager.isDiscountOnLineAllowed()) return;
		 		 
		 if(!this.visible){			
			 $('discountOnTotalContainer').hide();
			 $('discountContainer').style.display = 'none';
			 $('discountRights').style.display = 'none';
			 $('item-details-panel').style.display = 'none';
			 $('glider-column').show();
			 //$$('.sidebar-main-button')[0].show();
			 
			 return;
		 }
		 else
		 {
			 $('glider-column').hide();
			 //$$('.sidebar-main-button')[0].hide();
			 $('discountContainer').style.display = 'none';
			 $('item-details-panel').style.display = 'block';
			 $('discountOnTotalContainer').show();	
			 $('discountRights').style.display = 'block';
		 }
		 
		 if(!DiscountManager.isDiscountOnTotalAllowed())
		 {
			/*hide discount on total panel*/
			DiscountManager.hideDiscountOnTotalPanel();
		 }
		 else
		 {
			DiscountManager.showDiscountOnTotalPanel();
		 }		
		
		this.loadLineDetails();
		this.renderDiscountRightsInfo();	
	 },
		 
		/* hide:function(){
			 $('discountOnTotalContainer').hide();
			 $('discountContainer').style.display = 'none';
			 $('discountRights').style.display = 'none';
			 $('item-details-panel').style.display = 'none';
			 $('glider-column').show();	
			 //$$('.sidebar-main-button')[0].show();
			 
			 this.visible = false;
		 },*/
	 
		 
	 	
	 	applyDiscountOnTotal:function(){
	 		/*validate discount limit*/ 
			
			var discountPercentage = this.discountOnTotal_percentage_tf.value			
			if(!DiscountManager.isDiscountValid(discountPercentage)){
				return;
			}
			
			var discountAmt = this.discountOnTotal_amount_tf.value;
			discountAmt = new Number(discountAmt).toFixed(2); /* use 2 d.p for rounding */
			
			if(discountAmt < 0.0){
				alert(Translation.translate('discount.amount.cannot.be.negative','Discount Amount cannot be negative!'));
				return;
			}
			
			var previousDiscountOnTotal = ShoppingCartManager.getDiscountOnTotal();
			if(previousDiscountOnTotal != 0){
				alert(Translation.translate("you.have.already.given.discount.on.total.please.clear.previous.discount.on.total.click.reset.to.clear.previous.discounts","You have already given discount on total! Please clear previous discount on total! Click reset to clear previous discounts!"));
				return;
			}
			
			ShoppingCartManager.setDiscountOnTotal(discountAmt);
			//DiscountPanel.hide();
			this.hide();
	 	},
	 	
	 	loadLineDetails:function()
	 	{
	 		if(!this.visible) return;
	 		
	 		if(ShoppingCartManager.isShoppingCartEmpty()) return;
	 		
	 		var line = shoppingCart.lines[shoppingCart.selectedIndex];
			
	 		var priceEntered = line.priceEntered;
			var priceList = line.priceList;
			var priceStd = line.priceStd;
			var priceLimit = line.priceLimit;
			var isTaxIncluded = line.isTaxIncluded;
			var taxRate = line.taxRate;
			var qty = line.qty;
			
			/*Parse values*/
			isTaxIncluded = new Boolean(isTaxIncluded);
			taxRate = parseFloat(taxRate);
			priceList = parseFloat(priceList);
			priceStd = parseFloat(priceStd);
			priceLimit = parseFloat(priceLimit);
			qty = parseFloat(qty);
			
			this.pp = new PPricing(priceEntered,priceStd,priceList,priceLimit,qty,taxRate,isTaxIncluded); 
			
			this.render(null);
			
			if(DiscountManager.isDiscountOnTotalAllowed())
			{
				/* render discount on total */
				this.renderDiscountOnTotal();
			}
			
	 	},
	 	
	 	isDiscountValid : function(discount){
	 		return DiscountManager.isDiscountValid(discount);
	 	},
	 	
	 	renderDiscountRightsInfo:function(){
	 		
	 		var rights = DiscountManager.getDiscountRights();
	 		
	 		//$('discountUpToLimitPriceLabel').innerHTML = rights.discountUpToLimitPrice ? 'Yes' : 'No';
	 		$('overrideLimitPriceLabel').innerHTML = rights.overrideLimitPrice ? 'Yes' : 'No';
	 		$('allowDiscountOnTotalLabel').innerHTML = rights.allowDiscountOnTotal ? 'Yes' : 'No';
	 		$('discountLimitLabel').innerHTML = rights.discountLimit + '%';
	 		
	 		if(rights.allowOrderBackDate && OrderScreen.backDateButton){
	 			OrderScreen.backDateButton.show();
			 }
	 	},
	 	
	 	resetDiscountOnTotal:function(){
	 		var subTotal = ShoppingCartManager.getSubTotal();
	 		subTotal = parseFloat(subTotal.toFixed(2));
	 		
	 		subTotal = PriceManager.setPrecision(subTotal, 2);		
			this.discountOnTotal_amount_tf.value = 0;
			this.discountOnTotal_percentage_tf.value = 0;
			this.discountOnTotal_total_tf.value = subTotal;
			
			if(window.confirm(Translation.translate("confirmation.reset.discount"))){
				/* clear previous discounts */
				ShoppingCartManager.setDiscountOnTotal(0);
			}		
			
	 	},
	 	
	 	renderDiscountOnTotal:function(){
	 		
	 		var baseAmt = (TerminalManager.terminal.isSOPriceListTaxIncluded) ? ShoppingCartManager.getGrandTotal() : ShoppingCartManager.getSubTotal();
			
			var discountAmt = ShoppingCartManager.getDiscountOnTotal();
	 		var percentage = (discountAmt * 100)/(baseAmt + discountAmt);
	 		
	 		if(isNaN(percentage)) percentage = 0;
	 		
			percentage = PriceManager.setPrecision(percentage, 2);
			discountAmt = PriceManager.setPrecision(discountAmt, 2);
			
			this.discountOnTotal_amount_tf.value = discountAmt;
			this.discountOnTotal_percentage_tf.value = percentage;
			this.discountOnTotal_total_tf.value = baseAmt;
	 	},
	 	
	 	render:function(elementNotToRender)
	 	{
			
	 	},
	 	
	 	reset:function(){
	 		this.pp.priceEntered = this.pp.priceStd;
	 		this.render(null);
	 	},
	 	
	 	clickHandler : function(e){
	 		var element = Event.element(e);
	 		this.textField = element;
	 		this.textField.focus();
	 		this.textField.select();
	 		
	 		/*this.textField.onkeyup = function(e){
	 			if (isNaN(this.value))
				{
	 				this.value = '';
					return;
				}
	 		}*/
	 	},
	 	
	 	keypadHandler:function(e){
	 		var button = Event.element(e); 
	        var element = button.getElementsByClassName('keypad-value')[0];
	        
	        /*Fix for google chrome*/
	          if (element == null)
	          {
	          		element = button;  
	          }
	          /**********************/
	          
	        var value = element.innerHTML;
                         
            var inputValue = this.textField.value;
            
            if (this.textField.selectionStart != this.textField.selectionEnd)
			{
				inputValue = '';
				this.textField.value = '';
				this.textField.focus();
			}
            
			if('Del' == element.innerHTML.strip()){
          	  
				if(inputValue.length > 0)
				{
					inputValue = inputValue.substring(0,inputValue.length-1);
				}
            }
			else if('-' == element.innerHTML.strip()){
				
				if(inputValue.indexOf('-') == -1){
					inputValue = '-' + inputValue;
				}
				else{
					inputValue = inputValue.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(inputValue.indexOf('.') == -1)
				{
					inputValue = inputValue + value;
				}
			}
			
			else{
				inputValue = inputValue + value;
			}
				
			this.textField.value = inputValue;
				
             
              if(isNaN(inputValue) || (inputValue < 0.0)){           
                  return;
              }
             
              var onkeyup = this.textField.onkeyup;
	  			if(onkeyup){
	  				var event = {keyCode:65};
	  				onkeyup.call(this.textField,event);
	  			}
        },
        
        onShow:function(){		
        	var subTotal = ShoppingCartManager.getSubTotal();
     		subTotal = parseFloat(subTotal.toFixed(2));
     		
     		subTotal = PriceManager.setPrecision(subTotal, 2);		
    		this.discountOnTotal_amount_tf.value = 0;
    		this.discountOnTotal_percentage_tf.value = 0;
    		this.discountOnTotal_total_tf.value = subTotal;
    		
    		setTimeout(function(){
    			$('discountOnTotal.percentageTextfield').focus();
    			$('discountOnTotal.percentageTextfield').select();
    		},500);
    		
    		this.textField = this.discountOnTotal_percentage_tf;
   	 }
        
	 });
var BP_Constants = { 
			TYPE : {
				CUSTOMER	:'Customer',
				VENDOR		:'Vendor'
			},
			
			SO_CREDIT_STATUS : {
				CREDIT_STOP		: "S",
				CREDIT_HOLD		: "H",
				CREDIT_WATCH 	: "W",
				NO_CREDIT_CHECK : "X",
				CREDIT_OK 		: "O"
			}
};

var BP = new Class.create({
	initialize: function(record){
		this.id = record.c_bpartner_id || 0;
		this.code = record.identifier || '';
		this.name = unescape(record.name || '') ;
		this.name2 = unescape(record.name2 || '');
		this.openBalance = record.totalopenbalance || 0;
		this.creditStatus = record.socreditstatus || BP_Constants.SO_CREDIT_STATUS.NO_CREDIT_CHECK;
		this.creditLimit = record.so_creditlimit || 0;
		this.creditUsed = record.so_creditused || 0;
		this.isTaxExempt = record.istaxexempt;
		this.taxId = record.taxid;
		this.priceListId = record.m_pricelist_id;
		this.paymentTermId = record.c_paymentterm_id;
		this.actualLifetimeValue = record.actuallifetimevalue || '';
		this.flatDiscount = record.flatdiscount || '';
		this.firstSale = record.firstsale || '';
		this.imageURL = record.imageURL;
		this.customerImageUrl = record.customerImageUrl || '';
		this.email = record.email || '';
		this.birthday = record.birthday || '';
		this.phone = record.phone || '';
		this.fax = record.fax || '';
		this.address1 = record.address1 || '';
		this.city = record.city || '';
		this.searchKey = record.identifier || '';
		this.creditStatusName = 'No Credit Check';
		this.description = record.description || '';
		this.mobile = record.phone2 || '';
		this.custom1 = record.custom1 || '';
		this.custom2 = record.custom2 || '';
		this.custom3 = record.custom3 || '';
		this.custom4 = record.custom4 || '';
		this.emailReceipt = record.emailReceipt || '';
		
		this.enableLoyalty = record.enableloyalty || 'N';
		this.loyaltyPoints = record.loyaltypoints;
		this.loyaltyPointsEarned = record.loyaltypointsearned;
		this.loyaltyPointsSpent = record.loyaltypointsspent;
		this.loyaltyStartingPoints = record.loyaltystartingpoints;
		
		this.u_pos_discountcode_id = record.u_pos_discountcode_id || 0;
		this.discountcode_expiry = record.discountcode_expiry || '';
		
		this.setCreditStatusName();
		
		this.lastDateRedeemed = record.lastdateredeemed || '';
	},
	getId:function(){
		return this.id;
	},
	getCode:function(){
		return this.code;
	},
	getName:function(){
		return this.name;
	},
	getName2:function(){
		return this.name2;
	},
	getFullName:function(){
		var name1 = this.getName();
		var name2 = this.getName2();
		
		var fullName = name1;
		if(name2 != null && name2.length > 0){
			fullName = fullName + ' ' + name2;
		}
		
		return fullName;
	},
	getCreditAvailable:function(){
		var openBalance = this.getOpenBalance();
		var creditLimit = this.getCreditLimit();
		
		var creditAvailable = parseFloat(new Number(creditLimit - openBalance).toFixed(2));
		
		return creditAvailable;		
	},
	getCreditLimit:function(){
		return this.creditLimit;
	},
	getCreditUsed:function(){
		return this.creditUsed;
	},
	getCreditStatus:function(){
		return this.creditStatus;
	},
	getOpenBalance:function(){
		return this.openBalance;
	},
	getLifetimeValue:function(){
		return this.actualLifetimeValue;
	},
	getFirstSale:function(){
		return this.firstSale;
	},
	
	getImageURL : function(){
		return this.imageURL;
	},
	
	getCustomerImageUrl : function()
	{
		return this.customerImageUrl;
	},
	
	getEmail : function()
	{
		return this.email;
	},
	
	getBirthday : function()
	{
		return this.birthday;
	},
	
	getPhone : function()
	{
		return this.phone;
	},
	
	getFax : function()
	{
		return this.fax;
	},
	
	getMobile : function()
	{
		return this.mobile;
	},
	
	getAddress1 : function()
	{
		return this.address1;
	},
	
	getCity : function()
	{
		return this.city;
	},
	
	getSearchKey : function()
	{
		return this.searchKey;
	},
	
	getDescription : function()
	{
		return this.description;
	},
	getCustom1 : function()
	{
		return this.custom1;
	},
	getCustom2 : function()
	{
		return this.custom2;
	},
	getCustom3 : function()
	{
		return this.custom3;
	},
	getCustom4 : function()
	{
		return this.custom4;
	},
	isEmailReceipt : function()
	{
		return this.emailReceipt;
	},
	getLoyaltyStartingPoints : function()
	{
		return this.loyaltyStartingPoints;
	},
			
	setCreditStatusName : function(){
		switch(this.creditStatus)
		{
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_STOP :
			this.creditStatusName = 'Credit Stop';
			break;
			
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_HOLD :
			this.creditStatusName = 'Credit Hold';
			break;
			
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_WATCH :
			this.creditStatusName = 'Credit Watch';
			break;
			
			case BP_Constants.SO_CREDIT_STATUS.NO_CREDIT_CHECK :
				this.creditStatusName = 'No Credit Check';
				break;
				
			case BP_Constants.SO_CREDIT_STATUS.CREDIT_OK :
				this.creditStatusName = 'Credit OK';
				break;
			
			default: break;
		}
	},
	
	getCreditStatusName : function(){
		return this.creditStatusName;
	},
	
	getPaymentTermId : function(){
		return this.paymentTermId;
	},
	
	getLastDateRedeemed : function(){
		return this.lastDateRedeemed;
	}
});

var BP_Autocompleter = Class.create({
	 initialize: function(textfield, resultContainer, isSoTrx) {
	    this.textfield  = textfield;
	    this.resultContainer = resultContainer;
	    this.isSoTrx = isSoTrx;
	    
	    this.url = "BPartnerAction.do";
	    
	    this.options = {
	    		'frequency' : 1.0,
	    		'minChars' : 1,
	    		'paramName' : "searchTerm",
	    		'parameters' : ('action=search&isCustomer=' + (this.isSoTrx == null || this.isSoTrx))
	    };
	    	    
	    this.options.afterUpdateElement = this.afterUpdateElement.bind(this);	    	    
	    
	    new Ajax.Autocompleter(this.textfield, this.resultContainer, this.url, this.options);
	    
	    this.textfield = $(this.textfield);
	    
	    this.textfield.onclick = function(e){
	    	this.select();
	    };
	 },
	 
	 afterUpdateElement:function(text, li){
		 var json = li.getAttribute('json');		 
		 if(json == null) return;
		 
		 json = eval('(' + json + ')');
		 
		 
	 
	 
	 	if (json.c_bpartner_id == 0)
	 	{			
			//alert($input.val());
			//dlg.close();
			
			jQuery.post("BPartnerAction.do",
			  {
			    action : "getCMSCustomer",
			    "identifier" : json.identifier
			  },
			  function(data,status,xhr){
				  
				data = JSON.parse(data);
				  
				if(data.error){
					
					alert(data.error);
					return;										
				}
				
				//dlg.close();
				
				/*var bp = new BP({
					'c_bpartner_id': data['c_bpartner_id'], 
					'name' : data['name']
				});*/
				
				var bp = new BP(data);
				
				BPManager.setBP(bp);
				$('search-bp-textfield').value = data['name'];
				$('bp-name').innerHTML = data['name'];
				
				OrderScreen.setBPartnerId(data['c_bpartner_id']).done(function(){
					
					$('search-bp-textfield').value = data['name'];
					$('bp-name').innerHTML = data['name'];
					
					BPManager.setBP(bp);
					
					alert('Imported CMS Customer sucessfully.');
					
				});				
			    
			  });
			
			return;
		
	 	}
		 
		 
		 /* encapsulate bp data in a BP object */
		 
		 var bp = new BP(json);
		 
		 //Validation for Customer discount code
		 if(!ShoppingCartManager.isShoppingCartEmpty())
		 {
			var customerDiscountCodeId = bp['u_pos_discountcode_id'];
			var oldBpName = $('bp-name').innerHTML;
			
			if ((oldBpName != bp.getFullName()) && (customerDiscountCodeId && parseInt(customerDiscountCodeId) > 0))
			{
				//check for discount code expiry
				if( bp.discountcode_expiry && bp.discountcode_expiry.length > 0 )
				{
					var expired = false;
					
					expired = moment().startOf('day').isAfter(moment(bp.discountcode_expiry,'MM-DD-YYYY'))
					
					console.log("Discount code expired -> " + expired);
					
					if( ! expired )
					{
						if(!window.confirm(bp.getFullName() + ' has discount code. Any discount will be reset on ' + oldBpName + '. Do you want to proceed?')) return;
					}
				}
				
				
			}
		 }
		 
		 BPManager.setBP(bp);
		 
		 /* clear textbox */
		 this.textfield.value = "";
		 
		 this.afterUpdateBP(bp);
		 
		 var bpInfoPanel = new BPInfoPanel();
		 bpInfoPanel.show();
	 },
	 
	 afterUpdateBP:function(bp){}
	 
});

var BPManager = {
		defaultBP:null,
		bp:null,
		dialog:null,
		setDefaultBP:function(bp){
			this.defaultBP = bp;
			if(this.bp == null){
				this.bp = this.defaultBP;
			}
		},
		getDefaultBP:function(){
			return this.defaultBP;
		},
		setBP:function(bp){
			this.bp = bp;
		},
		getBP:function(){
			return this.bp;
		},
		isBPDefault:function(bp){
			return (bp.getId() == this.defaultBP.getId());
		},
		
		validateCreditStatus:function(bp, amt){
			/*
			if(BPManager.isBPDefault(bp)){
				//need to choose a bp
				alert('Choose a customer. Credit transactions are not allowed on default customer');
				return;
			}
			*/
			
			/*validate credit status*/
			if(BP_Constants.SO_CREDIT_STATUS.NO_CREDIT_CHECK == bp.getCreditStatus())
			{
				/*var errmsg = messages.get("js.set.customer.credit.status");*/
				alert(Translation.translate("invalid.credit.status.reason.no.credit.check","Invalid credit status! Reason: No Credit Check"));
				return false;
			}
			
			if(bp.getCreditLimit() == 0)
			{
				/*var errmsg = messages.get("js.set.customer.credit.limit");*/
				alert(Translation.translate("invalid.credit.status.reason.credit.limit.not.set","Invalid credit status! Reason: Credit limit not set"));
				return false;
			}
			
			/*
			if((bp.getOpenBalance() + amt) > bp.getCreditLimit())	
			{				
				alert("Invalid credit status! Reason: Credit limit exceeded");			
				return false;
			}
			
			if(bp.getCreditStatus() != BP_Constants.SO_CREDIT_STATUS.CREDIT_OK)
			{
				
				var errmsg = "Invalid credit status! Reason: ";
				
				var status = bp.getCreditStatus();
				
				if(status == BP_Constants.SO_CREDIT_STATUS.CREDIT_HOLD)
				{
					errmsg += ' Credit Hold';
				}
				
				if(status == BP_Constants.SO_CREDIT_STATUS.CREDIT_STOP)
				{
					errmsg += ' Credit Stop';
				}
				
				if(status == BP_Constants.SO_CREDIT_STATUS.CREDIT_WATCH)
				{
					errmsg += ' Credit Watch';
				}
				
				alert(errmsg);
				return false;
			}
			*/
			
			return true;			
		},
		
		emailReceipt : function(orderId,emailAdd)
		{
			this.dialog = new Dialog();
			this.dialog .show();
						
			
			var url = 'OrderAction.do';
			var pars = 'action=emailReceipt&orderId=' + orderId; 
			new Ajax.Request(url, {
				method:'post',
				parameters: pars,
				onSuccess: this.onSuccess.bind(this)					
			});
		},
		
		onSuccess : function(response)
		{
			this.dialog.hide();
			var json = response.responseText.evalJSON(true);
			alert(json.message);					
		}
};

var UserManager = {
		initResetPassword:function(userId){
			var url = 'UserAction.do';
			var pars = 'action=initResetPassword&userId=' + userId;
			new Ajax.Request(url, {
				method:'post',
				parameters: pars,
				onSuccess: function(response){
					var json = response.responseText.evalJSON(true);
					if(json.success == true || json.success == 'true'){
						alert(Translation.translate('please.check.your.mail.to.reset.your.password','Please check your mail to reset your password'));
					}
					else{
						alert(json.error);
					}						
				}					
			});
		}
		
};

var SearchBP = {
		
		isSoTrx:true,
		input:null,
		param: new Hash(),
		bpJSON: new Hash(),
		
		initializeComponents:function(){
			SearchBP.input = $('search-bp-textfield');
			
			$('bp-search-button').onclick = function(e){
				SearchBP.search();
			};

			$('search-bp-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchBP.search();
				}
			};
			
			$('search-bp-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-bp-textfield').onclick = function(e){
				this.select();
			};
		},
		
		search : function()
		{
			SearchBP.param.set('action', 'searchBP');
			SearchBP.param.set('searchTerm', SearchBP.input.value);
			SearchBP.param.set('isCustomer', (OrderScreen.isSoTrx == null || OrderScreen.isSoTrx));
			
			new Ajax.Request('BPartnerAction.do', {
				method:'post',
				onSuccess: SearchBP.render,
				parameters: SearchBP.param
			});
		},
		
		render : function(response){
			var jsonArray = response.responseText.evalJSON(true);
			
			$('bp-list').innerHTML =  '<div class="col-md-12 col-xs-12 no-padding"></div>';
			
			if (jsonArray.length == 0)
			{
				alert(Translation.translate('no.results.found','No Results Found'));
				return;
			}
			
			for (var i=0; i<jsonArray.length; i++)
			{
				var json = jsonArray[i];
				var phone = json.phone;
				var name = json.name;
				var name2 = json.name2;
				
				
				SearchBP.bpJSON.set(json.c_bpartner_id, json);
				
				var styleClass = 'even-row';
				
				if (i%2 == 0)
				{
					styleClass = 'odd-row';
				}
				
				var div = document.createElement('div');
				div.setAttribute('class', 'row ' + styleClass);
				div.setAttribute('style', 'padding: 4px 0px;');
				
				var div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.updateBP('+ json.c_bpartner_id +');');
				
				var div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name2;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.updateBP('+ json.c_bpartner_id +');');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-3 col-xs-3 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.updateBP('+ json.c_bpartner_id +');');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = phone;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-1 col-xs-1 no-padding');
				div2.setAttribute('onclick', 'javascript:SearchBP.displayInfo('+ json.c_bpartner_id +');');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-right');
				div3.setAttribute('style', 'padding-right:5px;');
				
				var iconDiv = document.createElement('div')
				iconDiv.setAttribute('class', 'glyphicons glyphicons-pro circle_info');
				div3.appendChild(iconDiv);
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				$('bp-list').appendChild(div);
			}
		},
		
		updateBP : function(bPartnerId)
		{
			var json = SearchBP.bpJSON.get(bPartnerId);
			var bp = new BP(json);
			BPManager.setBP(bp);
			
			OrderScreen.setBPartnerId(bp.getId());
			
			//$('search-bp-textfield').value = bp.getFullName();
			$('search-bp-textfield').value = '';
			$('bp-name').innerHTML = bp.getFullName();
		},
		
		displayInfo : function(bPartnerId)
		{
			var infoPanel = new BPInfoPanel();
			infoPanel.setId(bPartnerId);
			infoPanel.show();
		}
};


function updateBPName(url){
	
var bpartnerId;

var dlg = jQuery("<div title='"+ Translation.translate("change.customer") +"'><label>" +Translation.translate("select.customer")+"</label><br>" +
		"<input type='text' id='change-customer-textfield' style='margin-top:10px;font-size:20px;width:90%;' >" +
		"</div>");

var upbpdlg = jQuery(dlg).dialog({
    width: "450",
    height: "350",
    modal: true,
    autoOpen: true,
    open: function(event) {
        
    },
    beforeclose: function(){ 
    	return true;
    },
    close: function() {
    	jQuery(dlg).dialog('destroy');
    },
    position:['middle',40],
    buttons: [			 
          {
        	text: Translation.translate("apply"),
            click: function() {
            	//var bpartnerId = jQuery('#change-customer-textfield').val();
            	
            	if (bpartnerId == undefined || bpartnerId == null || bpartnerId == '')
            	{
            		alert(Translation.translate("please.select.customer"));
            		return;
            	}
            	
            	window.location = url + "&bpartnerId=" + bpartnerId;
            }
          }
    ]});


jQuery("#change-customer-textfield").keyup(function () {
 	
	var searchterm = jQuery(this).val();

    if (searchterm.length >= 2 ) {
    	
    	jQuery.post("BPartnerAction.do", {
            action: "searchCustomer",
            searchTerm : searchterm
        },
        
        function(json, textStatus, jqXHR) {

            if (json == null || jqXHR.status != 200) {
                alert(Translation.translate("failed.to.load.customers","Failed to load customers!"));
                return;
            }
            
            var customers = json.customers;	 
            
            customers = jQuery.map(customers, function(item){
        		return {
        			label: item.name,
        			value: item.c_bpartner_id
        		};
        	});
            
            jQuery('#change-customer-textfield').autocomplete({
            	source: customers,
            	
            	focus: function(event, ui){
            		event.preventDefault();
            	},
            	
            	select: function(event, ui){
            		event.preventDefault();
            		jQuery('#change-customer-textfield').val(ui.item.label);
            		bpartnerId = ui.item.value;
            	}
            	
            });       
            
        },
        "json").fail(function() {
        alert(Translation.translate("failed.to.query.customers","Failed to query customers!"));
        });   	 
    }	
	
});

};
var OrderTax = {
		 defaultTaxId : 0,
		 isTaxIncluded : false,
		 		 
		 taxes:[],
		
		getTax:function(taxid){
	 		for(var i=0; i<this.taxes.length; i++){
	 			var tax = this.taxes[i];
	 			if(tax.id == taxid){
	 				return tax;
	 			}
	 		}
	 		
	 		return null;
 		},
 		
 		getTaxesForCategory:function(taxCategoryId){
 			var taxes = new Array();
 			
 			for(var i=0; i<this.taxes.length; i++){
	 			var tax = this.taxes[i];
	 			if(tax.categoryid == taxCategoryId){
	 				taxes.push(tax);
	 			}
	 		}
 			
 			return taxes;
 		},
 		
 		renderComponents:function(){
 			 			
 			if(this.defaultTaxId == 0){
 				/*take default tax*/
 				if(this.taxes == null || this.taxes.length == 0)
 				{
 					alert(Translation.translate("error.no.tax.found","Error! No tax found!"));
 					return;
 				}
 				
 				this.defaultTaxId = this.taxes[0].id;
 				for(var i=0; i<this.taxes.length; i++)
 				{
 					if(this.taxes[i].isdefault)
 					{
 						this.defaultTaxId = this.taxes[i].id;
 						break;
 					}
 				}
 				
 				TAX_ID = this.defaultTaxId;
 				
 			}
 			
 			
 			var defaultTax = OrderTax.getTax(this.defaultTaxId);
 			$('orderScreen.taxLabel').innerHTML = 'Tax:' + defaultTax.name + '&nbsp;&nbsp;&nbsp;Rate:' + defaultTax.rate;
 			
 			/* if price includes tax then disable tax dropdown*/
 			if(this.isTaxIncluded)
 			{
 				$('orderScreen.taxSelectList').hide();
 				return;
 			}
 			
 			var html = '';
 			for(var i=0; i<this.taxes.length; i++){
	 			var tax = this.taxes[i];
				html = html + "<option value='"+ tax.id  + ((tax.id == defaultTax.id) ? "' selected='selected'" : "'")+">" + tax.name + ' - ' + tax.rate + "%</option>";				 			
	 		} 			
 			
 			$('orderScreen.taxSelectList').innerHTML = html;
 		},
 		
 		initializeTaxes:function(){
 			var url = 'TaxAction.do';
			var pars = 'action=getAllTaxes';
			
 			var myAjax = new Ajax.Request( url, 
 			{ 
				method: 'get',
				parameters: pars,
				onSuccess: OrderTax.loadTaxes, 
				onFailure: function(request){alert(Translation.translate("failed.to.get.available.taxes","Failed to get available taxes!"));}
 			});
 		},
 		
 		loadTaxes:function(request){
 			var response = request.responseText;
 			var taxes = eval('(' + response + ')');
 			
 			OrderTax.taxes = taxes;
 			OrderTax.renderComponents();
 		},
 		
 		/* show the tax for the shopping cart line*/
 		showTax:function(taxId){
 			var options = $('orderScreen.taxSelectList').options;
 			
 			for(var i=0; i<options.length; i++)
 			{
 				var option = options[i];
 				option.selected = (option.value == taxId);
 			}
 		}
 	
 };
 
 /*Event.observe(window,'load',OrderTax.initializeTaxes.bind(OrderTax),false);*/
var OrderScreen = {					
		setTenderType : function(tenderType){
			this.tenderType = tenderType;
		},
		
		setDeliveryRule : function(deliveryRule){
			this.deliveryRule = deliveryRule;
		},
		
		setPaymentTermId : function(paymentTermId){
			this.paymentTermId = paymentTermId;
		},
		
		getPaymentTermId : function(){
			return this.paymentTermId;
		},
		
		setPaymentRule : function(paymentRule){
			
			switch (paymentRule) {
			
			case PAYMENT_RULE.CASH:	
				this.setTenderType(TENDER_TYPE.CASH);
				break;
			
			case PAYMENT_RULE.CARD:	
				this.setTenderType(TENDER_TYPE.CARD);
				break;
				
			case PAYMENT_RULE.CHEQUE:	
				this.setTenderType(TENDER_TYPE.CHEQUE);
				break;
				
			case PAYMENT_RULE.MIXED:	
				this.setTenderType(TENDER_TYPE.MIXED);
				break;
				
			case PAYMENT_RULE.CREDIT:	
				this.setTenderType(TENDER_TYPE.CREDIT);
				break;
			
			case PAYMENT_RULE.VOUCHER:
				this.setTenderType(TENDER_TYPE.VOUCHER);
				break;
				
			case PAYMENT_RULE.EXTERNAL_CARD:
				this.setTenderType(TENDER_TYPE.EXTERNAL_CARD);
				break;
				
			case PAYMENT_RULE.GIFT_CARD:
				this.setTenderType(TENDER_TYPE.GIFT_CARD);
				break;
				
			case PAYMENT_RULE.LOYALTY:
				this.setTenderType(TENDER_TYPE.LOYALTY);
				break;
				
			case PAYMENT_RULE.SK_WALLET:
				this.setTenderType(TENDER_TYPE.SK_WALLET);
				break;
				
			case PAYMENT_RULE.ZAPPER:
				this.setTenderType(TENDER_TYPE.ZAPPER);
				break;
									
			case PAYMENT_RULE.MCB_JUICE:
				this.setTenderType(TENDER_TYPE.MCB_JUICE);
				break;
			
			case PAYMENT_RULE.MY_T_MONEY:
				this.setTenderType(TENDER_TYPE.MY_T_MONEY);
				break;
				
			case PAYMENT_RULE.EMTEL_MONEY:
				this.setTenderType(TENDER_TYPE.EMTEL_MONEY);
				break;
				
			case PAYMENT_RULE.GIFTS_MU:
				this.setTenderType(TENDER_TYPE.GIFTS_MU);
				break;
				
			case PAYMENT_RULE.MIPS:
				this.setTenderType(TENDER_TYPE.MIPS);
				break;
				
			default:
				break;
			}
		},
		
		setPayment : function(payment){
			this.payment = payment;			
			this.checkout();
		},
		
		setIsPaymentOnline : function(isOnline){
			this.isPaymentOnline = isOnline;
		},
		
		getPaymentDetails : function(){
			
			/*validate bp*/
			if(BPManager.getBP() == null){
				alert(((OrderScreen.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');
				return;
			}
			
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			
			if(!ShoppingCartManager.isShoppingCartReady()){
				alert(Translation.translate('cart.is.not.ready','Cart is not ready!'));
				return;
			}
			
			var isRefund = (this.getCartTotal() < 0.0) || (this.orderType == ORDER_TYPES.CUSTOMER_RETURNED_ORDER) || (this.orderType == ORDER_TYPES.POS_GOODS_RETURNED_NOTE);
						
			switch (this.tenderType) {			
				
				case TENDER_TYPE.CASH:	
					if(isRefund){
						var payment = new Hash();
					  	payment.set('amountTendered', this.getCartTotal());
					  	payment.set('amountRefunded', 0.0);  	
					  	this.payment = payment;
					  	this.checkout();
						break;
					}
					
					var panel = new CashPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.MCB_JUICE:
				case TENDER_TYPE.MY_T_MONEY:
				case TENDER_TYPE.EMTEL_MONEY:
				case TENDER_TYPE.MIPS:
				case TENDER_TYPE.GIFTS_MU:
				case TENDER_TYPE.EXTERNAL_CARD:
					/* manual creditcard processing */	
					
					if((OrderScreen.skipExternalCardPopUp && OrderScreen.skipExternalCardPopUp == true) 
							|| (countryId != '100' && countryId != '109'))
					{
						/*this.setExternalCardPayment(); //this method is called on checkout. See OrderScreen->checkout*/
						this.checkout();
						break;
					}
					
					if ((countryId == '100' || countryId == '109') && OrderScreen.paymentProcessor == '')
					{
						panel = new ExtCreditCardPanel();
						panel.paymentHandler = this.paymentHandler.bind(this);
						panel.show();
						break;
					}
					
					this.checkout();
					break;
				
				case TENDER_TYPE.CARD:					
					var panel = null;
					
					if(OrderScreen.paymentProcessor == '' && (countryId == '100' || countryId == '109')){
						//alert("Payment processor not configured!");
						//return;	
						panel = new ApplyPaymentProcessorPanel();
						panel.show();
					}
					
					
					
					if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_XWeb"){
						panel = new XWebPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_HF")
					{						
						if (ORDER_TYPES.CUSTOMER_RETURNED_ORDER == ShoppingCartManager.getOrderType())
						{
							var payment = new Hash();
							payment.set('cardType', 'M');
							this.payment = payment;
							this.checkout();
							
							return;
						}
						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new MercuryPanel();
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_ElementPS")
					{
						if (ORDER_TYPES.CUSTOMER_RETURNED_ORDER == ShoppingCartManager.getOrderType())
						{
							var payment = new Hash();
							payment.set('cardType', 'M');
							this.payment = payment;
							this.checkout();
							
							return;
						}
						
						if(ORDER_TYPES.POS_ORDER == ShoppingCartManager.getOrderType())
						{
							panel = new ElementPSPanel();	
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0)
						{
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Mercury_EE")
					{
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						panel = new E2EPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Century")
					{
						panel = new CenturyPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Norse")
					{						
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new NorseCardPanel();					
					}
					else if(TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_USAEpay")
					{
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new UsaEpayPanel();					
					}
					
					else if((TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Merchant_Warehouse") && (PreferenceManager.getPreference(PreferenceManager.CONSTANTS.ENABLE_GENIUS_PAYMENT) == 'false'))
					{
						if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
							alert(Translation.translate('the.configured.payment.processor.only.supports.sales.transactions','The configured payment processor only supports sales transactions'));
							return;						
						}
						
						/*sales transactions can be negative*/
						if(ShoppingCartManager.getGrandTotal() < 0){
							alert(Translation.translate("the.configured.payment.processor.does.not.support.negative.sales.transactions","The configured payment processor does not support negative sales transactions"));
							return;	
						}
						
						panel = new MerchantWarehousePanel();					
					}
					
					else if((TerminalManager.terminal.paymentProcessor == "org.compiere.model.PP_Merchant_Warehouse") && (PreferenceManager.getPreference(PreferenceManager.CONSTANTS.ENABLE_GENIUS_PAYMENT) == 'true'))
					{
						panel = new MerchantWarehouseGeniusPanel();
					}
					
					else{
						panel = new CardPanel();
					}
					
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
				case TENDER_TYPE.CHEQUE:
					var panel = new ChequePanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
				
				case TENDER_TYPE.MIXED:	
					if(isRefund){
						alert(Translation.translate("mix.tender.type.is.not.supported.for.refunds","Mix tender type is not supported for refunds"));
						return;
					}
					
					/*
					var panel = new DeliveryPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					*/
					
					var preference = TerminalManager.terminal.preference;
					var showDeliveryOption = preference.showDeliveryOption;
					
					if( showDeliveryOption ){
						
						var panel = new DeliveryPanel();
						panel.paymentHandler = this.paymentHandler.bind(this);
						panel.show();
					}
					else
					{
						var payment = new Hash();
						payment.set('deliveryRule','O');
						payment.set('deliveryDate', null);
						payment.set('paymentTermId', 0);	
					  	this.payment = payment;
					  	this.checkout();
					}					
					
					break;
					
				case TENDER_TYPE.CREDIT:
					//validate BP
					var bp = BPManager.getBP();
					var amt = ShoppingCartManager.getGrandTotal();
					
					var isValid = false;
					
					if(this.isSoTrx == true){
						isValid = BPManager.validateCreditStatus(bp, amt);
					}
					else{
						/*don't check credit status for vendor*/
						isValid = true;
					}
					
					if(!isValid){
						return;
					}
					
					/*var paymentTermPanel = new PaymentTermPanel();
					paymentTermPanel.setPaymentTermId = this.setPaymentTermId.bind(this);
					paymentTermPanel.show();*/
					var panel = new DeliveryPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();	
					break;
					
				case TENDER_TYPE.VOUCHER:	
					if(isRefund){
						var payment = new Hash();
					  	payment.set('voucherAmt', this.getCartTotal());
					  	payment.set('voucherNo', null);
					  	payment.set('voucherOrgId', TerminalManager.terminal.orgId);					  	
					  	payment.set('amountRefunded', 0.0);  	
					  	this.payment = payment;
					  	this.checkout();
						break;
					}
					var panel = new VoucherPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
					
				case TENDER_TYPE.GIFT_CARD:
					
					var screen = this;
					
					function showGiftCardPanel()
					{
						var panel = new GiftCardPanel();
						panel.paymentHandler = screen.paymentHandler.bind(screen);
						panel.show();
					}
					
					var terminal = TerminalManager.terminal;
					var paymentProcessor = terminal.paymentProcessor;
					
					if(paymentProcessor != null 
							&& paymentProcessor.indexOf('Mercury') > -1
							&& terminal.preference.enableGift == true){
						
						OrderScreen.setIsPaymentOnline(true);
		    			showGiftCardPanel();
					}
					
					/*function showGiftCardPanel()
					{
						var panel = new GiftCardPanel();
						panel.paymentHandler = screen.paymentHandler.bind(screen);
						panel.show();
					}
					
					var terminal = TerminalManager.terminal;
					var paymentProcessor = terminal.paymentProcessor;
					
					if(paymentProcessor != null 
							&& paymentProcessor.indexOf('Mercury') > -1
							&& terminal.preference.enableGift == true){
						
						var dlg = jQuery("<div/>",{title:Translation.translate("choose.gift.card.processor","Choose Gift Card Processor"), "class":"gift-card-popup-transactions"});
						var configuredBtn = jQuery("<input>",{type:"button", value:Translation.translate("mercury.gift","Mercury Gift"), "class":""});
			    		var posteritaBtn = jQuery("<input>",{type:"button", value:Translation.translate("posterita.gift","Posterita Gift"), "class":""});
			    		
			    		configuredBtn.on("click", function(){
			    			jQuery(dlg).dialog("close");
			    			console.info('Configured :' + paymentProcessor);
			    			
			    			OrderScreen.setIsPaymentOnline(true);
			    			showGiftCardPanel();
			    		});
			    		
			    		posteritaBtn.on("click", function(){
			    			jQuery(dlg).dialog("close");
			    			console.info('Posterita Gift card processor');
			    			
			    			OrderScreen.setIsPaymentOnline(false);
			    			showGiftCardPanel();
			    		});
			    		
			    		var row = jQuery("<div/>", {html:Translation.translate("more.than.one.gift.card.processor.is.configured.please.choose.one","More than one gift card processor is configured! Please choose one")});
			    		dlg.append(row);		    		
			    		
			    		row = jQuery("<div/>", {html:"&nbsp;"});
			    		dlg.append(row);
			    		
			    		row = jQuery("<div/>", {"class":"row"});
			    		row.append(configuredBtn);
			    		dlg.append(row);
			    		
			    		row = jQuery("<div/>", {"class":"row"});
			    		row.append(posteritaBtn);
			    		dlg.append(row);
			    		
			    		jQuery(dlg).dialog({modal:true, width:460});
					}
					else
					{
						showGiftCardPanel();
					}*/
					
					/*
					var panel = new GiftCardPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();
					*/
									
					break;
					
				
				case TENDER_TYPE.SK_WALLET:
					SKWallet.showCheckInCustomersDialog();				
					break;
					
				case TENDER_TYPE.ZAPPER:
					
					if(ORDER_TYPES.POS_ORDER != ShoppingCartManager.getOrderType()){
						alert(Translation.translate("zapper only supports sales transactions","zapper.only.supports.sales.transactions."));
						return;						
					}
					
					/*sales transactions can be negative*/
					if(ShoppingCartManager.getGrandTotal() < 0){
						alert(Translation.translate("zapper.does.not.support.negative.sales.transactions","Zapper does not support negative sales transactions."));
						return;	
					}
					
					Zapper.showQRCodeDialog();				
					break;
					
				default:
					break;
				
				case TENDER_TYPE.LOYALTY:
					
					var panel = new LoyaltyPanel();
					panel.paymentHandler = this.paymentHandler.bind(this);
					panel.show();				
					break;
			}
		},
		
		paymentHandler:function(payment){
			this.payment = payment;			
			this.checkout();
		},
		
		setDateOrdered:function(){
			
			if(this.dateOrdered){
				$("dateOrdered").value = this.dateOrdered;
			}
			else{
				var today = new Date();
					
				var year = today.getFullYear();
				var month = today.getMonth() + 1;					
				var date = today.getDate();
				var hours = today.getHours();
				var minutes = today.getMinutes();
				var seconds = today.getSeconds();
				
				if(month < 10) month = '0' + month;
				if(date < 10) date = '0' + date;
				if(hours < 10) hours = '0' + hours;
				if(minutes < 10) minutes = '0' + minutes;
				if(seconds < 10) seconds = '0' + seconds;
				
				var dateOrdered = year + "-" + month + "-" + date +
					" " + hours + ":" + minutes + ":" + seconds;
				
				$("dateOrdered").value = dateOrdered;
			}
		},
		
		checkout : function(){
			/* signature capture */	
			var captureSignaturePreference = TerminalManager.terminal.preference.captureSignature;
			var paymentRule = this.getPaymentRuleForTenderType(this.tenderType);
			
			if(captureSignaturePreference == null || captureSignaturePreference.indexOf(paymentRule) < 0){
				OrderScreen.postData();
				return;
			}
			
			if(!OrderScreen.isSoTrx){
				OrderScreen.postData();
				return;
			}
			
			var signaturePopup = jQuery("<div class='signature-popup' title='"+Translation.translate("signature")+"'><canvas id='signature-canvas'></canvas><div class='sign-above'>"+Translation.translate("sign.above")+"</div></div>").dialog({width:"auto", height:"auto", modal: true, autoOpen: true, close: function( event, ui ) {
								
					}, 
					
					dialogClass : 'signature-popup',
					
					buttons: [{
				        id:"signature-popup-btn-skip",
				        text: Translation.translate("skip"),
				        click: function() {
				        	if(confirm(Translation.translate('you.want.to.skip.signature.capture','Do you want to skip signature capture?')))
							{
								signaturePopup.dialog( "close" );				        	
					        	OrderScreen.postData();
							}else
							{
								
							}	
				        }
				    },
				    {
				        id:"signature-popup-btn-clear",
				        text:Translation.translate("clear","Clear"),
				        click: function() {
				        	signaturePad.clear();
				        }
				    }, 
				    {
				        id:"signature-popup-btn-save",
				        text:Translation.translate("save","Save"),
				        click: function() {
				        	if(signaturePad.isEmpty()){
				        		alert(Translation.translate('signature.is.required','Signature is required!'));
				        		return;
				        	}
				        	
				        	var dataURL = signaturePad.toDataURL();
				        	jQuery('#signature').val(dataURL);
				        	
				        	signaturePopup.dialog( "close" );
				        	
				        	OrderScreen.postData();
				        }
				    }]					
			});
			
			var canvas = document.querySelector("canvas");
			var ratio =  window.devicePixelRatio || 1;
		    canvas.width = canvas.offsetWidth * ratio;
		    canvas.height = canvas.offsetHeight * ratio;
		    canvas.getContext("2d").scale(ratio, ratio);
		    
			var signaturePad = new SignaturePad(canvas);
		},
		
		postData : function(){
			/* do normal submit */ 
			/* TODO: check network*/			
			
			//1. reset form
			$("orderId").value = "0";
			$("bpartnerId").value = "0";
			$("prepareOrder").value="false";
			$("orderType").value="POS Order";
			$("tenderType").value="Cash";	
			$("isPaymentOnline").value="true";
			
			$("cashAmt").value="0";
			$("cardAmt").value="0";
			$("chequeAmt").value="0";
			$("externalCardAmt").value="0";
			
			$("mcbJuiceAmt").value="0";
			$("mytMoneyAmt").value="0";
			$("emtelMoneyAmt").value="0";
			$("mipsAmt").value="0";
			$("giftsMuAmt").value="0";
			
			
			$("voucherAmt").value="0";
			$("voucherOrgId").value="";
			$("voucherNo").value="";
			
			$("amountTendered").value="0";
			$("amountRefunded").value="0";
			
			$("chequeNo").value="";
			
			$("cardTrackData").value="";
			$("cardTrackDataEncrypted").value="";
			$("cardType").value="";
			$("cardholderName").value="";
			$("cardExpDate").value="";
			$("cardCVV").value="";
			$("cardStreetAddress").value="";
			$("cardZipCode").value="";
			$("cardNo").value="";
			
			$("documentNo").value = "";
			$("referenceNo").value = "";
			$("dateOrdered").value = "";
			
			$("giftCardAmt").value = "0";
			$("giftCardTrackData").value = "";
			$("giftCardNo").value = "";
			$("giftCardCVV").value = "";
			
			//$("skwalletAmt").value = "0";
			//$("zapperAmt").value = "0";
			
			//2. populate form
			$("orderType").value = this.orderType;
			$("deliveryRule").value = this.deliveryRule;
			$("paymentTermId").value = 0;
			
			var bp = BPManager.getBP();
			if(bp == null)
			{
				alert(((this.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');				
				
				return;
			}
			
			$("bpartnerId").value = bp.getId();
			$('orderId').value = ShoppingCartManager.getOrderId();
			
			if (OrderScreen.splitManager != null)
			{
				var splitSalesReps = OrderScreen.splitManager.getSplitSalesReps()
				var refSalesRep = OrderScreen.splitManager.getRefSalesRep();
				
				if (splitSalesReps != null)
				{
					if (refSalesRep != null)
						splitSalesReps.add(refSalesRep);
					$('splitSalesReps').value =  splitSalesReps.toJSON();
				}
				else
					$('splitSalesReps').value = "";
			}
			
			if(this.documentNumber) $("documentNo").value = this.documentNumber;			
			if(this.referenceNumber) $("referenceNo").value = this.referenceNumber;
			
			//set date ordered
			this.setDateOrdered();
			
			switch (this.tenderType) {
				case TENDER_TYPE.CASH :	
					this.setCashPayment();			
					break;
					
				case TENDER_TYPE.CHEQUE :
					this.setChequePayment();
					break;
					
				case TENDER_TYPE.CARD :
					this.setCardPayment();			  
					break;
					
				case TENDER_TYPE.EXTERNAL_CARD :
					this.setExternalCardPayment();			  
					break;
					
				case TENDER_TYPE.MCB_JUICE :
					this.setMCBJuicePayment();			  
					break;
				
				case TENDER_TYPE.MY_T_MONEY :
					this.setMyTMoneyPayment();			  
					break;
					
				case TENDER_TYPE.EMTEL_MONEY :
					this.setEmtelMoneyPayment();			  
					break;
				
				case TENDER_TYPE.MIPS :
					this.setMipsPayment();			  
					break;
					
				case TENDER_TYPE.GIFTS_MU :
					this.setGiftsMuPayment();			  
					break;
					
				case TENDER_TYPE.MIXED :
					this.setMixPayment();					
					break;
					
				case TENDER_TYPE.CREDIT :
					this.setOnCreditPayment();					
					break;
					
				case TENDER_TYPE.VOUCHER :
					this.setVoucherPayment();					
					break;
					
				case TENDER_TYPE.GIFT_CARD :
					this.setGiftCardPayment();					
					break;
					
				case TENDER_TYPE.SK_WALLET :
					this.setSKWalletPayment();					
					break;
					
				case TENDER_TYPE.ZAPPER :
					this.setZapperPayment();
					break;
					
				case TENDER_TYPE.LOYALTY :
					this.setLoyaltyPayment();					
					break;

				default:
					break;
			}				
			
			$('tenderType').value = this.tenderType;			
			
			$('order-form').submit();	
			this.processDialog.show();
		},
		
		setCashPayment : function(){
			$('amountTendered').value = this.payment.get('amountTendered');
			$('amountRefunded').value = this.payment.get('amountRefunded');	
			$('cashAmt').value = this.getCartTotal();
		},
		
		setCardPayment : function(){			
			$('cardAmt').value = this.getCartTotal();
			
			if(!this.isPaymentOnline)
			{
				$('isPaymentOnline').value = this.isPaymentOnline;
				return;
			}
			
			if(this.payment.get('cardTrackDataEncrypted'))
			{
				$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
				return;
			}			
			
			var isCardPresent = this.payment.get('isCardPresent');
			
			if(isCardPresent == true)
			{
				$('cardTrackData').value = this.payment.get('trackData');
			}
			else
			{
				$('cardTrackData').value = "";
				$('cardTrackDataEncrypted').value = "";
				$('cardNo').value = this.payment.get('cardNumber');
				$('cardExpDate').value = this.payment.get('cardExpiryDate');
				$('cardCVV').value = this.payment.get('cardCvv');
				$('cardholderName').value = this.payment.get('cardHolderName');
				$('cardStreetAddress').value = this.payment.get('streetAddress');
				$('cardZipCode').value = this.payment.get('zipCode');
			}			
		},
		
		setChequePayment : function(){
			$('chequeAmt').value = this.getCartTotal();
			$('chequeNo').value = this.payment.get('chequeNumber');
		},
		
		setMixPayment : function(){
			$('deliveryRule').value = this.payment.get('deliveryRule');
			$('deliveryDate').value = this.payment.get('deliveryDate');
			$('paymentTermId').value = this.payment.get('paymentTermId');
		},
		
		setOnCreditPayment : function(){
			$('deliveryRule').value = this.payment.get('deliveryRule');
			$('deliveryDate').value = this.payment.get('deliveryDate');
			$('paymentTermId').value = this.payment.get('paymentTermId');
		},
				
		setExternalCardPayment:function(){
			$('externalCardAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},	
		
		setMCBJuicePayment:function(){
			$('mcbJuiceAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setMyTMoneyPayment:function(){
			$('mytMoneyAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setEmtelMoneyPayment:function(){
			$('emtelMoneyAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setMipsPayment:function(){
			$('mipsAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setGiftsMuPayment:function(){
			$('giftsMuAmt').value = this.getCartTotal();
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setVoucherPayment : function(){
			$('voucherAmt').value = this.payment.get('voucherAmt');
			$('voucherNo').value = this.payment.get('voucherNo');
			$('voucherOrgId').value = this.payment.get('voucherOrgId');
		},
		
		setEECardPayment : function(){
			$('cardAmt').value = this.payment.get('cardAmt');
			$('cardTrackDataEncrypted').value = this.payment.get('cardTrackDataEncrypted');
		},
		
		setOtk : function(otk){
			$('otk').value = otk;
		},
		
		setGiftCardPayment : function(){
			$('giftCardAmt').value = this.getCartTotal();
			$('giftCardTrackData').value = this.payment.get('giftCardTrackData');
			$('giftCardNo').value = this.payment.get('giftCardNo');
			$('giftCardCVV').value = this.payment.get('giftCardCVV');
			$('isPaymentOnline').value = this.isPaymentOnline;
		},
		
		setSKWalletPayment : function(){
			$('skwalletAmt').value = this.getCartTotal();
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'true';
		},
		
		setZapperPayment : function(){
			$('zapperAmt').value = this.getCartTotal();
			$('otk').value = this.payment.get('otk');
			$('isPaymentOnline').value = 'true';
		},
		
		setLoyaltyPayment : function(){
			$('loyaltyAmt').value = this.getCartTotal();
		},
				
		saveOrder : function(){
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			
			var bp = BPManager.getBP();
			if(bp == null)
			{
				alert(((this.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');
				
				jQuery("#search-bp-textfield").focus();
				
				return;
			}
			
			$('orderId').value = ShoppingCartManager.getOrderId();
			$("orderType").value = this.orderType;
			$('prepareOrder').value = 'true';
			$('bpartnerId').value = bp.id;
			
			//set date ordered
			this.setDateOrdered();
			
			$('order-form').submit();
			this.processDialog.show();
		},
		
		setBPartnerId : function(bpartnerId){
			if(this.bpartnerId == bpartnerId){
				
				return jQuery.Deferred().resolve();
			}
			
			this.bpartnerId = bpartnerId;			
			$('bpartnerId').value = bpartnerId;			
			return ShoppingCartManager.setBp(bpartnerId);
		},
		
		setOrderId : function(orderId){
			$('orderId').value = orderId;				
		},
		
		initialize: function(){						
						
			/* fields declaration */
			this.tenderType = TENDER_TYPE.CASH;
			this.payment = null;
			this.splitManager = null;
			
			this.initializeDiscount();				
			this.initializeComponents();			
			this.initAutoCompletes();
			ClockedInSalesRepManager.addOnLoadClockedInSalesRepsListener(this);
			
			shortcut.add("Up", function(){
				SmartProductSearch.moveUp();
			});
			
			shortcut.add("Down", function(){
				SmartProductSearch.moveDown();
			});
			
			/* render search product filter 
			SearchProductFilter.initialize();*/
			
			ItemFields.init();
			CustomerFields.init();
			VendorFields.init();
			
			SearchBP.initializeComponents();
			SearchSalesRep.initializeComponents();
			//ClockInOut.init();
		},
		
		clockedInSalesRepsLoaded : function()
		{
			this.initSplitOrder();
		},
		
		initSplitOrder : function()
		{
			var splitOrderDetails = this.splitOrderDetails;
			var type = splitOrderDetails.type;
			var orderSplitSalesReps = eval('('+splitOrderDetails.salesReps+')');
			
			var orderDetailsManager = new SplitOrderDetailsManager(type, orderSplitSalesReps);
			this.splitManager = orderDetailsManager.getSplitManager();
			this.splitManager.updateSalesRepInfo();
			ClockedInSalesRepManager.addCurrentSalesRepChangeEventListener(this);
			ClockedInSalesRepManager.addClockInOutEventListener(this);
		},
		
		currentSalesRepChange : function (salesRepChangeEvent)
		{
			this.splitManager.currentSalesRepChange(salesRepChangeEvent);
		},
		
		clockInOut : function()
		{
			this.splitManager.clockInOut();
		},
		
		resetSplitAmounts : function()
		{
			if (this.splitManager != null)
				this.splitManager.resetAmounts();
		},
		
		updateSplitAmounts : function()
		{
			if (this.splitManager != null)
				this.splitManager.updateAmounts();
		},
		
		initializeComponents:function(){
			
			this.processDialog = new Dialog();
			
			/* search button & textfield */
			this.searchProductTextField = $('search-product-textfield');
			this.searchProductButton = $('search-product-button');
			
			/* header buttons */
			this.clockInOutButton = $('orderScreen.clockInOutButton');
			this.homeButton = $('orderScreen.homeButton');
			this.keyboardButton = $('orderScreen.keyboardButton');
			this.infoButton = $('orderScreen.infoButton');
			
			/* cart buttons & textfield */
			this.discountButton = $('discount-button');
			this.changePinButton = $('change-user-button');
			/*this.openCashDrawerButton = $('open-drawer-button');*/
			/*this.saveOrderButton = $('save-order-button');*/
			//this.commentsButton = $('comments-button');
			this.invokeOrderButton = $('load-button');
			this.clearCartButton = $('clear-button');
			/*this.decrementQtyButton = $('decrease-button');*/
			/*this.incrementQtyButton = $('add-button');*/			
			this.updateQtyTextField = $('quantity-texfield');	
			
			/* payment buttons */
			this.cashPaymentButton = $('orderScreen.cashPaymentButton');
			this.cardPaymentButton = $('orderScreen.cardPaymentButton');
			this.chequePaymentButton = $('orderScreen.chequePaymentButton');
			this.mixPaymentButton = $('orderScreen.mixPaymentButton');			
			this.onCreditPaymentButton = $('orderScreen.onCreditPaymentButton');			
			this.voucherPaymentButton = $('orderScreen.voucherPaymentButton');
			this.giftCardPaymentButton = $('orderScreen.giftCardPaymentButton');
			this.SKWalletPaymentButton = $('orderScreen.SKWalletPaymentButton');
			
			/* other buttons */
			this.bpInfoButton = $('bp-info-button');
			this.bpCreateButton = $('bp-create-button');
			/*this.checkoutButton = $('checkout-button');*/
			//this.newItemButton = $('create-item-button');			
			//this.giftCardButton = $('gift-card-button');
			//this.couponButton = $('redeem-coupon-button');
			
			this.backDateButton = $('back-date-button');
			if(DiscountManager.allowOrderBackDate()){
				if(this.backDateButton){
					this.backDateButton.show();
				}
			}
			
			
			
			this.referenceButton = $('reference-button');
			this.splitOrderButton = $('split-order-button');			
			//this.productInfoButton = $('product-info-button');
			
			/*Action buttons*/
			this.orderButton = $('order-button');
			this.terminalInfoButton = $('terminal-info-button');
			this.orderButtonContainer = $('order-actions-button-container');
			this.emptyCartButtonContainer = $('empty-cart-button-container');
			/*this.moreButton = $('more-button');*/
			
			/* drop downs */
			/*this.taxSelectList  = $('tax-dropdown');
			if(this.taxSelectList){
				this.taxSelectList.value = OrderTax.defaultTaxId;
			}*/
			
			/* set smart product search */
			SearchProduct.isSoTrx = this.isSoTrx;		
			
			this.selectQueryBox();
			 
			/* add behaviour */	
			for(var field in this){
				var fieldContents = this[field];
				
				if (fieldContents == null || typeof(fieldContents) == "function") {
					continue;
			    }
				
				if(fieldContents.type == 'button'){
			    	//register click handler
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.type == 'text'){
			    	//register key handler
			    	fieldContents.onkeyup = this.keyHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.type == 'select-one'){
			    	//register key handler
			    	fieldContents.onchange = this.changeHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.tagName == 'A'){
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			    else if(fieldContents.tagName == 'IMG'){
			    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
			    }
			}
			
			/* add keypad to qty textfield */
			/*Event.observe(this.updateQtyTextField,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			/* bug fix for updateQtyTextField */
			this.updateQtyTextField.onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					if(!ShoppingCartManager.isShoppingCartEmpty()){
						var qty = parseFloat(this.value);
						
						if(isNaN(qty)){
							alert(Translation.translate('invalid.qty','Invalid Qty!'));
							this.selectAll();								
							return;					
						}
						ShoppingCartManager.updateQty(qty);							
					}
				}
			}
			
			/*tax rate*/
			var cart = ShoppingCartManager.getCart();
			cart.addBehaviourToLines();
			
			//Action button behaviour
			if(ShoppingCartManager.isShoppingCartEmpty())
			{
				this.orderButtonContainer.style.display = 'none';
				this.emptyCartButtonContainer.style.display = 'block';
			}
			else
			{
				this.emptyCartButtonContainer.style.display = 'none';
				this.orderButtonContainer.style.display = 'block';
			}
			
		},
		
		clickHandler : function(e){
			var element = Event.element(e);
			
			/*bug fix for buttons*/
			if(Prototype.Browser.WebKit)
			if(element instanceof HTMLButtonElement 
					|| element instanceof HTMLLinkElement 
					|| (element instanceof HTMLImageElement && !(element.parentNode instanceof HTMLButtonElement)))
			{
				
			}
			else
			{
				element = element.parentNode;
			}
			
			switch(element){
				
				case this.searchProductButton : 
					SearchProduct.search( {barcode : true} );
					break;
				
				case this.clockInOutButton : break;
				case this.homeButton : break;
				case this.keyboardButton : break;
				case this.infoButton : break;
				
				/*case this.discountButton : 
					new DiscountOnTotalPanel().show();
					break;*/
				
				case this.changePinButton : 
					new PINPanel().show();
					break;
				
				/*
				case this.openCashDrawerButton : 
					OrderScreen.openCashDrawer();
					break;*/
				/*
				case this.saveOrderButton : 
					//this.moreButtonContainer.style.display='none';
					OrderScreen.saveOrder();
					break;*/
					
				case this.commentsButton :					
					//OrderScreen.setComment();
					//this.moreButtonContainer.style.display='none';
					new CommentsPanel().show();
					break;
				
				/*
				case this.invokeOrderButton : 
					new InvokeOrderPanel().show();
					break;*/
				
				/*
				case this.clearCartButton :
					//this.moreButtonContainer.style.display='none';
					ShoppingCartManager.clearCart();
					OrderScreen.resetSplitAmounts();
					break;*/
				
				/*case this.decrementQtyButton : 
					ShoppingCartManager.decrementQty();
					break;
				
				case this.incrementQtyButton : 
					ShoppingCartManager.incrementQty();
					break;	*/
					
				/* Payment buttons */
				case this.cashPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CASH);
					break;
					
				case this.cardPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CARD);
					break;
					
				case this.chequePaymentButton :
					OrderScreen.setTenderType(TENDER_TYPE.CHEQUE); 
					break;
					
				case this.mixPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.MIXED);
					break;		
						
				case this.onCreditPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.CREDIT);
					break;	
							
				case this.voucherPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.VOUCHER);
					break;
					
				case this.giftCardPaymentButton : 
					OrderScreen.setTenderType(TENDER_TYPE.GIFT_CARD);
					break;
					
				case this.SKWalletPaymentButton :
					OrderScreen.setTenderType(TENDER_TYPE.SK_WALLET);
					break;
					
				case this.ZapperPaymentButton :
					OrderScreen.setTenderType(TENDER_TYPE.ZAPPER);
					break;
					
				/* Checkout button */
				case this.checkoutButton :
					/*OrderScreen.getPaymentDetails();*/
					
					
					
				case this.bpInfoButton :
					new BPInfoPanel().show();
					break;
				
				case this.bpCreateButton : 
					var panel = null;
					
					if (this.isSoTrx == true)
						panel = new CreateCustomerPanel();
					else
						panel = new CreateVendorPanel();
					
					panel.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					panel.show();
					break;
					
				case this.backDateButton :
					var panel = new BackDatePanel();
					panel.updateElements = this.setBackDateValues.bind(this);
					panel.show();
					break;
					
				/*case this.productInfoButton : 
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert('Cart is empty!');
						return;
					}
					var panel = new ProductInfoPanel();
					panel.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					
					 var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
					 var productId = currentLine.productId;
					
					panel.getInfo(productId);
					break;*/
					
				case this.splitOrderButton : 
					OrderScreen.splitManager.showPanel();
					break;
					
				case this.referenceButton : 
					
					OrderScreen.referencePopup = jQuery("<div class='reference-popup' title='Reference'><label>Reference No. <input type='text' id='reference-popup-reference-textfield'/></div>").dialog({width:"auto", height:"auto", modal: true, autoOpen: true, close: function( event, ui ) {
						
					 }, buttons: {
				        "Ok": function() {
				        	OrderScreen.referenceNumber = jQuery('#reference-popup-reference-textfield').val();
				        	OrderScreen.referencePopup.dialog( "close" );
				          },
				        "Cancel": function() {
				        	OrderScreen.referencePopup.dialog( "close" );
				         }
				       }
					});
					
					break;
					
				case this.giftCardButton :
					selectGiftCardTransaction();
					break;
					
				case this.couponButton :
					redeemCoupon();
					break;
					
				case this.newItemButton : 
					/*
					var p = new CreateProductPanel();
					p.onHide = function(){
						OrderScreen.selectQueryBox();
					};
					p.show();*/
					getURL('backoffice-products.do?#0')
					break;
				
				/*
				case this.moreButton :
					new MoreOptionsPanels().show();
					break;*/
				
							
				default:break;
			}		
		},
		
		test : function(){ alert("test");},
		
		xxx : function(){		

			//check age verification
			if(ShoppingCartManager.needAgeVerification()){
				/* show popup */	
				
				var customerName = BPManager.getBP().name;
				var customerDOB = BPManager.getBP().birthday;
				var today = moment();						
				var birthday = moment(customerDOB, 'MM/DD/YYYY');
				
				var age = today.diff(birthday, 'years');
				var minimumAge = TerminalManager.terminal.preference.age;
				
				var divProceed = jQuery('<div title="Customer Age">' +
						'<div>Customer Age: '+age+' yrs old</div>' +
						'<div>Accepted</div>' +
						'</div>');
				
				var divVerifyAge = jQuery('<div title="Verify Customer Age">' +
						'<div><label>D.O.B</label></div>' +
						'<div><input type="text" value="'+customerDOB+'"></div>' +
						'</div>');						
				
				if(age >= minimumAge){
					
					var dialogProceed = jQuery(divProceed).dialog({
			    		position: ['center',40],
			    		modal:true,
			    		open:function(){					    		
			    		},
			    		close:function(){
			    		},
			    		buttons : {
			    			"OK" : function(){
			    				OrderScreen.showPaymentOptions();			    				
			    				dialogProceed.dialog( "close" );
			    			}
			    		}
			    	});	    					
				}
				
				else
				{
					if((customerDOB !="") && (age < minimumAge))
					{
						alert("The Customer age is "+age+"yrs old.\nThe authorized age to buy this item is "+minimumAge+" yrs old")
					}
					
					if(customerDOB =="" || customerName =="Walk-in Customer")
					{
						var cal = divVerifyAge.find("input[type|='text']").datepicker({
							format: "mm/dd/yyyy",
							orientation: "top left",
							endDate: '+0d',
							autoclose: true
						}).on('hide', function (e) { e.preventDefault(); });
						
						var dialogVerifyAge = jQuery(divVerifyAge).dialog({
				    		position: ['center',40],
				    		modal:true,
				    		open:function(){					    		
				    		},
				    		close:function(){
				    		},
				    		buttons : {
				    			"OK" : function(){	   				    								    			
				    				
				    				var birthday = moment(cal.val(), 'MM/DD/YYYY');				    				
				    				var age = today.diff(birthday, 'years');					    				
				    				var minimumAge = TerminalManager.terminal.preference.age;
				    				
				    				var divProceed = jQuery('<div title="Customer Age">' +
											'<div>Customer Age: '+age+' yrs old</div>' +
											'<div>Accepted</div>' +
											'</div>');	
				    				
				    				if(age >= minimumAge){
				    					dialogVerifyAge.dialog( "close" );
				    					//OrderScreen.showPaymentOptions();
				    					
				    					var dialogProceed = jQuery(divProceed).dialog({
								    		position: ['center',40],
								    		modal:true,
								    		open:function(){					    		
								    		},
								    		close:function(){
								    		},
								    		buttons : {
								    			"OK" : function(){
								    				OrderScreen.showPaymentOptions();			    				
								    				dialogProceed.dialog( "close" );
								    			}
								    		}
								    	});					    					
				    				}
				    				else
				    				{
				    					alert("The Customer age is "+age+"yrs old.\nThe authorized age to buy this item is "+minimumAge+" yrs old")
				    				}
				    			},
				    			"Cancel" : function(){
				    				dialogVerifyAge.dialog( "close" );
				    			}
				    		}
				    	});								
					}
				}
				
				
				
				/*var customerDOB = BPManager.getBP().birthday;
				
				var div = jQuery('<div title="Verify Customer Age">' +
						'<div><label>D.O.B</label></div>' +
						'<div><input type="text" value="'+customerDOB+'"></div>' +
						'</div>');
				
				var cal = div.find("input[type|='text']").datepicker({
					format: "mm/dd/yyyy",
					orientation: "top left",
					endDate: '+0d',
					autoclose: true
				}).on('hide', function (e) { e.preventDefault(); });
				
				var dialog = jQuery(div).dialog({
		    		position: ['center',40],
		    		modal:true,
		    		open:function(){					    		
		    		},
		    		close:function(){
		    		},
		    		buttons : {
		    			"OK" : function(){
		    				if(customerDOB == "")
		    				{
		    					alert("Enter Age");
		    					return;
		    				}
		    				
		    				var birthday = moment(customerDOB, 'MM/DD/YYYY');
		    				
		    				var today = moment();
		    				
		    				var age = today.diff(birthday, 'years');
		    				
		    				var minimumAge = TerminalManager.terminal.preference.age;
		    				if(age >= minimumAge){
		    					dialog.dialog( "close" );
		    					OrderScreen.showPaymentOptions();
		    				}
		    				else{
		    					alert( age + " is less than authorized age [" + minimumAge + "]!");
		    				}
		    			},
		    			"Cancel" : function(){
		    				dialog.dialog( "close" );
		    			}
		    		}
		    	});*/
			}
			else 
			{
				OrderScreen.showPaymentOptions();
			}
		},
		
		showPaymentOptions : function(){
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			
			if(!ShoppingCartManager.isShoppingCartReady()){
				alert(Translation.translate('cart.is.not.ready','Cart is not ready!'));
				return;
			}
			
			var salesReps = ClockedInSalesRepManager.getSalesReps();
			
			if(salesReps.size() == 0){						
				alert(Translation.translate("no.user.has.been.clocked.in","No user has been clocked in"));
				return;
			}
			
			if(this.allowUpSell){
				if(ShoppingCartManager.getUpsellTotal() < 0.0){
					alert(Translation.translate("the.upsell.buffer.cannot.be.negative","The upsell buffer cannot be negative!"));
					return;
				}
			}
			
			if(OrderScreen.orderType == 'Quotation'){
				this.tenderType = TENDER_TYPE.CASH;						
				var payment = new Hash();
			  	payment.set('amountTendered', 0.0);
			  	payment.set('amountRefunded', 0.0);			  	
			  	this.payment = payment;
			  	this.checkout();
			  	return;
			}
			
			var cartTotal = this.getCartTotal();
			if(cartTotal == 0.0){
				this.tenderType = TENDER_TYPE.CASH;						
				var payment = new Hash();
			  	payment.set('amountTendered', cartTotal);
			  	payment.set('amountRefunded', 0.0);			  	
			  	this.payment = payment;
			  	this.checkout();
			  	return;
			}
			
			new PaymentPopUp().show();
		},
		
		backDate : function()
		{
			var panel = new BackDatePanel();
			panel.updateElements = this.setBackDateValues.bind(this);
			panel.show();
		},
		
		getProductInfo : function(productId)
		{
			if(ShoppingCartManager.isShoppingCartEmpty()){
				alert(Translation.translate('cart.is.empty','Cart is empty!'));
				return;
			}
			var panel = new ProductInfoPanel();
			panel.onHide = function(){
				OrderScreen.selectQueryBox();
			};
			
			 /*var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
			 var productId = currentLine.productId;*/
			
			panel.getInfo(productId);
		},
		
		toggleMoreButtonContainer : function(activeContainer)
		{
			this.toggleElement(activeContainer);
		},
		
		toggleElement : function(element){
			
			if (element != null)
			{
				if (element.style.display == 'none')
				{
					element.style.display = 'block';
				}
				else
				{
					element.style.display = 'none';
				}
			}
		},
		
		
		keyHandler : function(e){
			
			var element = Event.element(e);
			
			switch(element){
				case this.searchProductTextField : 
					if(e.keyCode == Event.KEY_RETURN){
						SearchProduct.search({barcode:true});
					}
					break;	
							
				case this.updateQtyTextField : 					
					break;
							
				default:break;
			}
		},
		
		changeHandler : function(e){
			var element = Event.element(e);
			
			switch(element){
				/*case this.taxSelectList : 
					var taxId = this.taxSelectList.value;
					ShoppingCartManager.setLineTax(taxId);
					break;	*/
					
				default:break;
			}
		},
				
		render : function(){
			/* call this to redraw screen */ 
			/*
			$('orderGrandTotal').innerHTML = ShoppingCartManager.getCurrencySymbol() + " " + new Number(ShoppingCartManager.getGrandTotal()).toFixed(2);
			
			*/
			
			PoleDisplayManager.display('Grand Total',ShoppingCartManager.getCurrencySymbol() + new Number(ShoppingCartManager.getGrandTotal()).toFixed(2));
			
			if(this.allowUpSell){
				$('orderScreen-upsellAmountContainer').innerHTML = new Number(ShoppingCartManager.getUpsellTotal()).toFixed(2);
			}
			else{
				$('orderScreen-upsellAmountContainer').innerHTML = '';
			}
			
		},
		
		selectQueryBox : function (){
			if(navigator.userAgent.match(/iPad/i)){
				/*prevent ipad keyboard from popping each time*/
				return;
			}
			
			var textfield = $('search-product-textfield');
			if( textfield != null){
				if(textfield.value.length > 0)
					textfield.select();
				else
					textfield.focus();
			}			
		},
		
		getCartTotal : function(){
			var amt = ShoppingCartManager.getGrandTotal();	
			return parseFloat(amt);
		},
		
		getCurrencySymbol : function(){
			return ShoppingCartManager.getCurrencySymbol();
		},		
		
		initializeDiscount:function(){
			/* see orderScreen.jsp line 163*/
			var discountLimit = this.discountLimit;
			var overrideLimitPrice = this.overrideLimitPrice;
			var discountUpToLimitPrice = this.discountUpToLimitPrice;
			var allowDiscountOnTotal = this.allowDiscountOnTotal;
			var allowOrderBackDate = this.allowOrderBackDate;
			var allowUpSell = this.allowUpSell;
			var discountOnCurrentPrice = this.discountOnCurrentPrice;
	
			var discountRights = new DiscountRights(discountLimit, overrideLimitPrice, discountUpToLimitPrice, allowDiscountOnTotal, allowOrderBackDate, allowUpSell, discountOnCurrentPrice);
			DiscountManager.setDiscountRights(discountRights);
			
			if($('orderScreen.backDateButton'))
			{
				if(!this.allowOrderBackDate){
					$('orderScreen.backDateButton').hide();
				}
				else
				{
					$('orderScreen.backDateButton').show();
				}
			}
			
		},
		
		openCashDrawer : function(){
			/*PrinterManager.showOpenDrawerDialog();*/
			PrinterManager.print([['OPEN_DRAWER']]);
		},
		
		initAutoCompletes : function(){
			var autocompleter = new BP_Autocompleter('search-bp-textfield','bp-autocomplete-choices-container', this.isSoTrx);
			
			//register updateBP function
			autocompleter.afterUpdateBP = this.afterUpdateBP.bind(this);
			
			var autocompleter = new SalesRep_Autocompleter('search-sales-rep-textfield','sales-rep-autocomplete-choices-container');
			
			jQuery("#search-bp-textfield").off("blur").on("blur", function(){
				
								
				var name = jQuery(this).val();
				
				if(name == ""){
					
					var terminal = TerminalManager.terminal;
					var defaultBpId = terminal['bpId'];
					
					if(defaultBpId <= 0){
						// no default customer												
						$('search-bp-textfield').value = "";
						$('bp-name').innerHTML = "";
						BPManager.setBP(null);
					}
					else
					{
						var defaultBpName = terminal['bpName'];
						
						var defaultBp = new BP({
							'c_bpartner_id': defaultBpId, 
							'name' : defaultBpName
						});
						
						
						$('search-bp-textfield').value = defaultBpName;
						$('bp-name').innerHTML = defaultBpName;					
						BPManager.setBP(defaultBp);	
					}
					
					OrderScreen.setBPartnerId(defaultBpId);				
					
				}
				else
				{
					if(BPManager.bp){
						$('search-bp-textfield').value = BPManager.bp.getFullName();
						$('bp-name').innerHTML = BPManager.bp.getFullName();
					}
					else
					{
						$('search-bp-textfield').value = "";
						$('bp-name').innerHTML = "";
					}
					
				}
				
			});
		},
		
		afterUpdateBP : function(bp){
			var id = bp.getId();
			this.setBPartnerId(id);
			
			$('search-bp-textfield').value = bp.getFullName();
			$('bp-name').innerHTML = bp.getFullName();
		},
		
		setBackDateValues : function(values){
			this.referenceNumber = values.referenceNumber;
			this.documentNumber = values.documentNumber;
			this.dateOrdered = values.dateOrdered;
		},
		
		isInventory : function()
		{
			if (OrderScreen.orderType == 'stock-transfer-send' 
				  || OrderScreen.orderType == 'stock-transfer-request')
			{
				return true;
			}
			
			return false;
		},
		
		/*setComment : function()
		{
			if(OrderScreen.commentDialog == null)
			{
				OrderScreen.commentDialog = jQuery("<div title='Enter Comment'><textarea id='order-comment'></textarea></div>").dialog(
					{   width: "auto",
						height : "auto",
						modal: true,
						buttons: {
							"Save": function() {
								var comment = jQuery("#order-comment").val();
								jQuery("#comment").val(comment);
								OrderScreen.commentDialog.dialog( "close" );
							},
							Cancel: function() {
								OrderScreen.commentDialog.dialog( "close" );
							}
						}
					}
				);
			}
			else
			{
				OrderScreen.commentDialog.dialog("open");
			}
			
		},*/
		
		getPaymentRuleForTenderType : function(tenderType){
			var map = [];
			map[TENDER_TYPE.CASH] = PAYMENT_RULE.CASH;
			map[TENDER_TYPE.CARD] = PAYMENT_RULE.CARD;
			map[TENDER_TYPE.CHEQUE] = PAYMENT_RULE.CHEQUE;
			map[TENDER_TYPE.MIXED] = PAYMENT_RULE.MIXED;
			map[TENDER_TYPE.VOUCHER] = PAYMENT_RULE.VOUCHER;
			map[TENDER_TYPE.EXTERNAL_CARD] = PAYMENT_RULE.EXTERNAL_CARD;
			map[TENDER_TYPE.GIFT_CARD] = PAYMENT_RULE.GIFT_CARD;
			map[TENDER_TYPE.CREDIT] = PAYMENT_RULE.CREDIT;
			map[TENDER_TYPE.SK_WALLET] = PAYMENT_RULE.SK_WALLET;
			
			return map[tenderType];
		},
		
		setMerchantWarehousePaymentDetails : function(details)
		{
			$('merchant-warehouse-payment-details').value = details;
		}
};


var ItemFields = {
		
		fields : null,
		
		init : function()
		{
			ItemFields.fields = $('items.fields').innerHTML;
		}
}

var CustomerFields = {
		
		fields : null,
		
		init : function()
		{
			CustomerFields.fields = $('customer.fields').innerHTML;
		}
}

var VendorFields = {
		
		fields : null,
		
		init : function()
		{
			VendorFields.fields = $('vendor.fields').innerHTML;
		}
}

var ClockInClockOut = {
		
		/*init: function()
		{
			$('sales-rep-list').innerHTML = '';
			ClockInClockOut.render();
		},
		
		render : function()
		{
			var salesRepsList = ClockInOutManager.clockedInUsers;
			
			for(var i=0; i<salesRepsList.length; i++){
				  var salesRep = salesRepsList[i];
				  var username = salesRep.username;
				  var clockInTime = salesRep.clockInTime;
				  var clockOutTime = salesRep.clockOutTime;
				  
				  var div = document.createElement('div');
					div.setAttribute('class', 'row ');
					div.setAttribute('style', 'padding: 4px 0px;');
					
					var div2 = document.createElement('div');
					div2.setAttribute('class', 'col-md-6 col-xs-6 no-padding');
					
					var div3 = document.createElement('div');
					div3.setAttribute('class', 'pull-left');
					div3.innerHTML = username;
					
					div2.appendChild(div3);
					div.appendChild(div2);
					
					div2 = document.createElement('div');
					div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
					
					div3 = document.createElement('div');
					div3.setAttribute('class', 'pull-left');
					div3.innerHTML = clockInTime;
					
					div2.appendChild(div3);
					div.appendChild(div2);
					
					div2 = document.createElement('div');
					div2.setAttribute('class', 'col-md-2 col-xs-2 no-padding');
					div2.setAttribute('onclick', 'javascript: ClockInClockOut.clockInOrClockOut(\''+username+'\')');
					
					div3 = document.createElement('div');
					div3.setAttribute('class', 'pull-right');
					
					
					var iconDiv = document.createElement('div')
					iconDiv.setAttribute('class', 'glyphicons glyphicons-pro clock');
					div3.appendChild(iconDiv);
					
					div2.appendChild(div3);
					div.appendChild(div2);
					
					$('sales-rep-list').appendChild(div);
			  }
		},
		
		clockInOrClockOut : function(username)
		{
			var clockInOutPanel =  new ClockInOutPanel();
			clockInOutPanel.setUser(username);
			clockInOutPanel.show();
		}*/
};


var SearchSalesRep = {
		
		input:null,
		param: new Hash(),
		
		initializeComponents:function(){
			SearchSalesRep.input = $('search-sales-rep-textfield');
			
			$('sales-rep-search-button').onclick = function(e){
				SearchSalesRep.search();
			};

			$('search-sales-rep-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchSalesRep.search();
				}
			};
			
			$('search-sales-rep-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-sales-rep-textfield').onclick = function(e){
				this.select();
			};
		},
		
		search : function()
		{
			SearchSalesRep.param.set('action', 'loadSalesReps');
			SearchSalesRep.param.set('searchTerm', SearchSalesRep.input.value);
			
			new Ajax.Request('UserAction.do', {
				method:'post',
				onSuccess: SearchSalesRep.render,
				parameters: SearchSalesRep.param
			});
		},
		
		render : function(response){
			//var jsonArray = response.responseText.evalJSON(true);
			
			var jsonArray = eval('(' + response.responseText + ')');
			
			$('sales-rep-list').innerHTML =  '<div class="col-md-12 col-xs-12 no-padding"></div>';
			
			if (jsonArray.length == 0)
			{
				
				alert(Translation.translate('no.results.found','No Results Found'));
				return;
			}
			
			for (var i=0; i<jsonArray.length; i++)
			{
				var json = jsonArray[i];
				var id = json.id;
				var name = json.name;
				
				
				//SearchSalesRep.bpJSON.set(json.id, json);
				
				var styleClass = 'even-row';
				
				if (i%2 == 0)
				{
					styleClass = 'odd-row';
				}
				
				var div = document.createElement('div');
				div.setAttribute('class', 'row ' + styleClass);
				div.setAttribute('style', 'padding: 4px 0px;');
				div.setAttribute('onclick', 'javascript:SearchSalesRep.add("'+ json.name +'");');
				
				var div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				
				var div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name;
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				/*div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-4 col-xs-4 no-padding');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = name;
				
				div2.appendChild(div3);
				div.appendChild(div2);*/
				
				/*div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-3 col-xs-3 no-padding');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-left');
				div3.innerHTML = phone;
				
				div2.appendChild(div3);
				div.appendChild(div2);*/
				
				div2 = document.createElement('div');
				div2.setAttribute('class', 'col-md-1 col-xs-1 no-padding');
				
				div3 = document.createElement('div');
				div3.setAttribute('class', 'pull-right');
				
				
				var iconDiv = document.createElement('div')
				div3.appendChild(iconDiv);
				
				div2.appendChild(div3);
				div.appendChild(div2);
				
				$('sales-rep-list').appendChild(div);
			}
		},
		
		add : function(name)
		{
			 var clockInOutPanel =  new ClockInOutPanel();
			clockInOutPanel.setUser(name);
			clockInOutPanel.show();
			 
			 /* clear textbox */
			 $('search-sales-rep-textfield').value = "";
		}
};


var SalesRep_Autocompleter = Class.create({
	 initialize: function(textfield, resultContainer) {
	    this.textfield  = textfield;
	    this.resultContainer = resultContainer;
	    
	    this.url = "UserAction.do";
	    
	    this.options = {
	    		'frequency' : 1.0,
	    		'minChars' : 1,
	    		'paramName' : "searchTerm",
	    		'parameters' : ('action=getSalesReps')
	    };
	    	    
	    this.options.afterUpdateElement = this.afterUpdateElement.bind(this);	    	    
	    
	    new Ajax.Autocompleter(this.textfield, this.resultContainer, this.url, this.options);
	    
	    this.textfield = $(this.textfield);
	    
	    this.textfield.onclick = function(e){
	    	this.select();
	    };
	 },
	 
	 afterUpdateElement:function(text, li){
		 
		 var json = li.getAttribute('json');
		 
		 if(json == null) return;
		 
		 json = eval('(' + json + ')');
		 
		 
		 var clockInOutPanel =  new ClockInOutPanel();
		clockInOutPanel.setUser(json.name);
		clockInOutPanel.show();
		 
		 /* clear textbox */
		 this.textfield.value = "";
	 }
	 
	 
});


var ORDER_TYPES = { 
	POS_ORDER : 'POS Order',
	CUSTOMER_RETURNED_ORDER : 'Customer Returned Order',
	POS_GOODS_RECEIVE_NOTE : 'POS Goods Receive Note',		
	POS_GOODS_RETURNED_NOTE : 'POS Goods Returned Note'
};
	
var TENDER_TYPE = {
		CARD : 'Card',
		CASH : 'Cash',
		CHEQUE : 'Cheque',
		CREDIT : 'Credit',
		MIXED : 'Mixed',
		VOUCHER : 'Voucher',
		EXTERNAL_CARD : 'Ext Card',
		GIFT_CARD : 'Gift Card',
		SK_WALLET : 'SK Wallet',
		ZAPPER : 'Zapper',
		LOYALTY : 'Loyalty',
		MCB_JUICE : 'MCB Juice',
		MY_T_MONEY : 'MY.T Money',
		EMTEL_MONEY : 'Emtel Money',
		GIFTS_MU : 'Gifts.mu',
		MIPS : 'MIPS'
	};

var PAYMENT_RULE = {
		CARD : 'K',
		CASH : 'B',
		CHEQUE : 'S',
		CREDIT : 'P',
		MIXED : 'M',
		VOUCHER : 'V',
		EXTERNAL_CARD : 'E',
		GIFT_CARD : 'G',
		SK_WALLET : 'W',
		ZAPPER : 'Z',
		LOYALTY : 'L',
		MCB_JUICE : 'J',
		MY_T_MONEY : 'Y',
		EMTEL_MONEY : 'T',
		MIPS : 'I',
		GIFTS_MU : 'U'
		
};

var SO_CREDIT_STATUS = {
	CreditStop 		: "S",
	CreditHold 		: "H",
	CreditWatch 	: "W",
	NoCreditCheck 	: "X",
	CreditOK 		: "O"
};

var DELIVERY_RULE = {
		AFTER_RECEIPT : 'R',
		COMPLETE_ORDER : 'O',
		MANUAL : 'M'
};

/* define some variables */
OrderScreen.commentDialog = null;

/*
window.onerror=function(){
	OrderScreen.processDialog.hide();
};
*/

jQuery(function(){
	/*more options*/
	jQuery("#redeem-promotion-button").off('click').on('click', function(e){
		PopUpManager.clear();
		new RedeemPromotionPanel().show();
		
	});

	jQuery("#redeem-coupon-button").off('click').on('click', function(e){
		PopUpManager.clear();
		redeemCoupon();
	});

	jQuery(".gift-card-button").off('click').on('click', function(e){
		PopUpManager.clear();
		selectGiftCardTransaction();
	});
	
	jQuery(".deposit-button").off('click').on('click', function(e){
		PopUpManager.clear();
		selectDepositTransaction();
	});

	jQuery(".create-item-button").off('click').on('click', function(e){
		PopUpManager.clear();
		getURL('backoffice-products.do?#0');
	});

	jQuery("#split-order-button").off('click').on('click', function(e){
		PopUpManager.clear();
		OrderScreen.splitManager.showPanel();
	});

	jQuery("#discount-on-total-button").off('click').on('click', function(e){
		PopUpManager.clear();
		DiscountManager.getDiscountPanel();
	});

	jQuery("#change-tax-button").off('click').on('click', function(e){
		PopUpManager.clear();
		changeTaxPanel.show();			
	});

	jQuery("#comments-button").off('click').on('click', function(e){
		PopUpManager.clear();
		new CommentsPanel().show();		
	});
	
	jQuery("button.box-no-button").off('click').on('click', function(e){
		PopUpManager.clear();
		var box = window.prompt("Enter box/bag number");
		if(box){
			shoppingCart.setCurrentBoxNumber(box);
		}		
	});
	
	jQuery("button.search-by-barcode-only-button").off('click').on('click', function(e){
		PopUpManager.clear();
		/*
		var searchTerm = window.prompt("Search by SKU, Name, Description, Groups");
		if(searchTerm){
			SearchProduct.input.value = searchTerm;
			SearchProduct.search();
		}	
		*/	
		new SearchProductPanel().show();
	});
	
	/* transition from prototype.js to jquery */
	jQuery('#load-button').off('click').on('click', function(e){
		new InvokeOrderPanel().show();
	});
	
	jQuery('#import-csv-button').off('click').on('click', function(e){
		window.location = 'importer.do?importer=OrderLineImporter&orderType=' + shoppingCart.orderType;
	});
	
	jQuery('#open-drawer-button').off('click').on('click', function(e){
		OrderScreen.openCashDrawer();
	});
	
	jQuery('#checkout-button').off('click').on('click', function(e){
		
		var bp = BPManager.getBP();
		
		if(bp == null)
		{
			alert(((OrderScreen.isSoTrx == true) ? 'Customer' : 'Vendor!') + ' is required!');
			
			jQuery("#search-bp-textfield").focus();
			
			return;
		}
		
		//check default bp
		if(OrderScreen.isSoTrx == true 
				&& (TerminalManager.terminal.bpId == bp.id) 
				&& TerminalManager.terminal.preference.cms == true)
		{
			
			var getPosteritaCardNo = function(){
				
				var $message = jQuery('<div></div>');
				$message.append('Enter Posterita Loyalty Card Number<br />');
		        
		        var $input = jQuery('<input type="text" class="form-control form-control-lg">');
		        
		        $input.css({
		        	'padding': '.5rem 1rem',
				  	'font-size': '1.25rem',
				  	'line-height': '1.5',
		        	'border-radius': '.3rem'
		        });
		        
		        $message.append($input);
				
				var dlg = BootstrapDialog.show({
					size: BootstrapDialog.SIZE_SMALL,
		            title: 'Posterita Loyalty',
		            message: $message,
		            buttons: [{
		                label: 'Ok',
		                cssClass: 'btn-primary',
		                action: function(dialog) {			                    
		                    
		                    //validate card number
		                    validateID();
		                    
		                    //OrderScreen.performAgeVerification();
		                }
		            }, {
		                label: 'Cancel',
		                action: function(dialog) {
		                	dialog.close();
		                }
		            }],
		            
		            onshown : function(){
		            	
		            	$input.focus();
		            }
		        });
				
				//enter listner
				$input.on('keyup', function (e) {
				    if (e.keyCode == 13) {
				        // validate
				    	validateID();
				    }
				});
				
				var validateID = function(){
					
					//alert($input.val());
					//dlg.close();
					
					jQuery.post("BPartnerAction.do",
					  {
					    action : "getCMSCustomer",
					    "identifier" : $input.val()
					  },
					  function(data,status,xhr){
						  
						data = JSON.parse(data);
						  
						if(data.error){
							
							alert(data.error);
							return;										
						}
						
						dlg.close();
						
						var bp = new BP({
							'c_bpartner_id': data['c_bpartner_id'], 
							'name' : data['name']
						});
						
						OrderScreen.setBPartnerId(data['c_bpartner_id']).done(function(){
							
							$('search-bp-textfield').value = data['name'];
							$('bp-name').innerHTML = data['name'];
							
							BPManager.setBP(bp);
							
							OrderScreen.xxx();
							
						});
						
					    
					  });
				};
				
			};
			
			//posterita card
			BootstrapDialog.show({
				size: BootstrapDialog.SIZE_SMALL,
	            title: 'Posterita Loyalty',
	            message: 'Does customer have a Posterita Loyalty Card?',
	            buttons: [{
	            	id: 'posterita_loyalty_card_popup_yes_button',
	                label: 'Yes',
	                cssClass: 'btn-primary',
	                action: function(dialog) {
	                    dialog.close();
	                    
	                    getPosteritaCardNo();
	                }
	            }, {
	            	id: 'posterita_loyalty_card_popup_no_button',
	                label: 'No',
	                action: function(dialog) {
	                	dialog.close();
	                	
	                	OrderScreen.xxx();
	                }
	            }]
	        });
		}
		else
		{
			OrderScreen.xxx();
		}	
	});
	
	jQuery('#clear-button').off('click').on('click', function(e){
		ShoppingCartManager.clearCart();
		OrderScreen.resetSplitAmounts();
	});
	
	jQuery('#save-order-button').off('click').on('click', function(e){
		OrderScreen.saveOrder();
	});
	
	jQuery('#more-button').off('click').on('click', function(e){
		new MoreOptionsPanels().show();
	});
	
	jQuery('#load-quotation-button').click(function(){
    	PopUpManager.clear();
    	new InvokeQuotationPanel().show();
    });
	
	/* apply preference  */
	
	if(OrderScreen.isSoTrx === true){
		
		var preference = TerminalManager.terminal.preference;
		
		if( preference.searchByBarcodeOnly === true ){
			
			SearchProduct.searchByBarcodeOnly = true;
			
			var tf = document.getElementById("search-product-textfield");
			tf.placeholder = "Barcode";
		}
		
		if( preference.trackManualProductEntry === true ){
			SearchProduct.trackManualProductEntry = true;
		}
	}

});

/* load orderscreen glider and other defaults like modifier groups and mappings */
jQuery(document).ready(function(){
	
	var gliderHTML = sessionStorage.getItem('GLIDER');
	
	if(gliderHTML == null){
		
		jQuery.getJSON("OrderAction.do?action=loadOrderScreenDefaults", function(data){
			
			var defaults = data;
			
			gliderHTML = defaults.productSearchFilter;
			sessionStorage.setItem('GLIDER', gliderHTML);
			
			//set other defaults
			sessionStorage.setItem('MODIFIER_GROUPS', Object.toJSON(defaults["modifierGroups"]));
			sessionStorage.setItem('MODIFIER_MAPPINGS', Object.toJSON(defaults["modifierMappings"]));
			
			gliderReady(gliderHTML);
		});
		
	}
	else
	{
		gliderReady(gliderHTML);
	}
	
});
var CheckoutPanel = Class.create(PopUpBase, {
	initialize : function(panelId)
	{
		/*If current user is not a sales rep, return*/
		
		if (!ClockedInSalesRepManager.getIsCurrentUserSalesRep())
		{
			alert("Current user " + ClockedInSalesRepManager.getCurrentSalesRep +" is not a sales rep and will not be able to make orders!");
			return;
		}
		
		
		
		
		var table = $('checkoutPanel' + panelId);
		if (table == null)
		{
			var table = document.createElement('table');
			table.setAttribute('id', 'checkoutPanel' + panelId);
		}
		else
		{
			table.innerHTML = '';
		}
		var tr = document.createElement('tr');
		table.appendChild(tr);
		
		var referenceSalesRep = null;		
		if(OrderScreen.splitManager != null) referenceSalesRep = OrderScreen.splitManager.getRefSalesRep();
		
		if (referenceSalesRep != null && referenceSalesRep.getAmount != 0)
		{
			var divImage = document.createElement('div');
			var divName = document.createElement('div');
			var divAmount = document.createElement('div');
			
			var span = document.createElement('span');
			span.setAttribute('class', 'employee');
			
			//divImage.innerHTML = '<div class="butn-user01"></div>';
			divName.innerHTML = referenceSalesRep.getName();
			divAmount.innerHTML = referenceSalesRep.getAmount();
			
			//divImage.setAttribute('class', 'salesRepInfoImage');
			divName.setAttribute('class', 'salesRepInfo');
			divAmount.setAttribute('class', 'salesRepInfo');
			
			span.appendChild(divName);
			span.appendChild(divAmount);
			
			var td = document.createElement('td');
			//divImage.appendChild(divName);
			//divImage.appendChild(divAmount);
			td.appendChild(span);
			tr.appendChild(td);
		}
		
		var splitSalesReps = null;
		if(OrderScreen.splitManager != null) splitSalesReps = OrderScreen.splitManager.getSplitSalesReps();
		if (splitSalesReps != null)
		{
			for (var i=0; i<splitSalesReps.size(); i++)
			{
				var splitSalesRep = splitSalesReps.get(i);
				var name = splitSalesRep.getName();
				var amount = splitSalesRep.getAmount();
				
				if (amount != 0)
				{
					//var divImage = document.createElement('div');
					var divName = document.createElement('div');
					var divAmount = document.createElement('div');
					
					var span = document.createElement('span');
					span.setAttribute('class', 'employee');
					
					//divImage.innerHTML = '<div class="butn-user02-off"></div>';
					divName.innerHTML = name;
					divAmount.innerHTML = amount;
					
					//divImage.setAttribute('class', 'salesRepInfoImage');
					divName.setAttribute('class', 'salesRepInfo');
					divAmount.setAttribute('class', 'salesRepInfo');
					
					var td = document.createElement('td');
					//divImage.appendChild(divName);
					//divImage.appendChild(divAmount);
					
					span.appendChild(divName);
					span.appendChild(divAmount);
					
					td.appendChild(span);
					
					tr.appendChild(td);
				}
			}
			
			new Insertion.Top(panelId + '.split', table);
			
		}
		this.createPopUp($(panelId));
	}
});

var CashPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('cash.popup.panel');
	    
	    this.amountRefunded = 0.0;
	    
	    /*buttons*/
	    this.okBtn = $('cash.popup.okButton');
	    this.cancelBtn = $('cash.popup.cancelButton');
	    
	    /*textfields*/
	    this.amountTenderedTextField = $('cash.popup.amountTenderedTextField');   
	    
	    /*labels*/
	    this.totalLabel = $('cash.popup.totalLabel');
	    //this.writeOffCashLabel = $('cash.popup.writeOffCashLabel');
	    //this.paymentAmtLabel = $('cash.popup.paymentAmtLabel');
	    this.amountRefundedLabel = $('cash.popup.amountRefundedLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    //this.paymentAmtLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*others*/
	    this.amountTenderedTextField.panel = this;
	    this.amountTenderedTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('invalid.amt');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}
	    	
	    	var value = this.value;
	    	if(value == null || value == '') value = 0.0;
	    	
	    	var amountTendered = parseFloat(value);
	    	var paymentAmt = this.panel.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){
	    		this.panel.onError('invalid.amt');
	    		return;
	    	}
	    };
	    
	    this.amountTenderedTextField.onkeyup = function(e){
	    	
	    	var value = this.value;
	    	if(value == null || value == '') value = 0.0;
	    	
	    	var amountTendered = parseFloat(value);
	    	var paymentAmt = this.panel.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){    		
	    		return;
	    	}
	    	
	    	var change = 0;
	    	if(amountTendered != 0.0){
	    		change = amountTendered - paymentAmt; 
	    	}
	    	   
	    	change =  new Number(change).toFixed(2);	
	    	
	    	this.panel.amountRefundedLabel.innerHTML = '' + change;    	
	    	this.panel.amountRefunded = change;
	    };
	    
	    /*cash-out panel*/
	    var cashOutButtons = $$('.cash-out-panel button');
	    for(var i=0; i<cashOutButtons.length; i++){
	    	var btn = cashOutButtons[i];
	    	btn.onclick = this.cashOutHandler.bind(this);
	    }
	    
	    /*generic keypad panel*/
	    var keypadButtons = $$('.key-pad-button');
	    
	    for(var j=0; j<keypadButtons.length; j++)
	    {
	    	var btn = keypadButtons[i];
	    	btn.onclick = this.keypadHandler.bind(this);
	    }
	    
	  },
	  
	  cashOutHandler:function(e){
		  var element = Event.element(e);
		  
		  var amt = 0;
		  
		  if(element.className == 'exact-amount'){
			  amt = this.cartTotalAmount;
			  
			  this.amountTenderedTextField.value = new Number(amt).toFixed(2);
			  this.amountRefundedLabel.innerHTML = '0.00';    	
		      this.amountRefunded = 0.00;
		      
		      return;
		  }
		  else
		  {
			  amt = parseFloat(element.innerHTML);
		  }
		  
		  			  
		  var amountTendered = this.amountTenderedTextField.value;
		  if(amountTendered == '') amountTendered = 0;
		  
		  if('CLR' == element.innerHTML.strip()){
			  amt = 0;
			  amountTendered = 0;
		  }
		  
		  amountTendered = parseFloat(amountTendered) + amt;
		  this.amountTenderedTextField.value = new Number(amountTendered).toFixed(2);
		  
		  /*code from this->amountTenderedTextField->onkeyup*/
		  var paymentAmt = this.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){    		
	    		return;
	    	}
	    	
	    	var change = 0;
	    	if(amountTendered != 0.0){
	    		change = amountTendered - paymentAmt; 
	    	}
	    	   
	    	change =  new Number(change).toFixed(2);	
	    	
	    	this.amountRefundedLabel.innerHTML = '' + change;    	
	    	this.amountRefunded = change;
	  },
	  
	  keypadHandler:function(e){
		  var element = Event.element(e);			  
		  var amt = parseFloat(element.innerHTML);
		  			  
		  var amountTendered = this.amountTenderedTextField.value;
		  if(amountTendered == '') amountTendered = 0;
		  
		  if('&larr;' == element.innerHTML.strip()){
			  amt = 0;
			  amountTendered = 0;
		  }
		  
		  amountTendered = amt;
		  this.amountTenderedTextField.value = new Number(amountTendered).toFixed(2);
		  
		  /*code from this->amountTenderedTextField->onkeyup*/
		  var paymentAmt = this.cartTotalAmount;
	    	
	    	if(isNaN(amountTendered) || (amountTendered < 0.0)){    		
	    		return;
	    	}
	    	
	    	var change = 0;
	    	if(amountTendered != 0.0){
	    		change = amountTendered - paymentAmt; 
	    	}
	    	   
	    	change =  new Number(change).toFixed(2);	
	    	
	    	this.amountRefundedLabel.innerHTML = '' + change;    	
	    	this.amountRefunded = change;
	  },
	  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.amountTenderedTextField.value = '';
	  	this.amountRefundedLabel.innerHTML = '0.00';  
	  	
	  	this.amountRefunded = 0.0;
	  },
	  onShow:function(){
	  	this.amountTenderedTextField.focus();
	  	this.amountRefundedLabel.innerHTML = '0.00';  
	  },
	  validate:function(){
		  
		  var amountTendered = parseFloat(this.amountTenderedTextField.value);
		  if(isNaN(amountTendered) || (amountTendered < 0.0)){
			  this.onError('invalid.amt');
			  return false;
		  }
		  
		  var paymentAmt = this.cartTotalAmount;
		  if(amountTendered < paymentAmt){
			  this.onError('invalid.amt');
			  return false;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('amountTendered',this.amountTenderedTextField.value);
	  	payment.set('amountRefunded', this.amountRefunded);  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});

/* Card Panel */
var CardPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('card.popup.panel');
	    
	    /*panes*/
	    this.trackDataDetailsPane = $('card.popup.trackDataDetailsPane');
	    this.cardDetailsPane = $('card.popup.cardDetailsPane');
	    
	    /*labels*/
	    this.totalLabel = $('card.popup.totalLabel');	 	    
	    
	    /*textfields*/
	    /*this.cardSelectList = $('card.popup.cardSelectList');*/
	    this.swipeCardCheckBox = $('card.popup.swipeCardCheckBox');
	    
	    this.trackDataTextField = $('card.popup.trackDataTextField');
	    this.cardNumberTextField = $('card.popup.cardNumberTextField');
	    this.cardExpiryDateTextField = $('card.popup.cardExpiryDateTextField');
	    this.cardCvvTextField = $('card.popup.cardCvvTextField');
	    this.cardHolderNameTextField = $('card.popup.cardHolderNameTextField');
	    this.streetAddressTextField = $('card.popup.streetAddressTextField');
	    this.zipCodeTextField = $('card.popup.zipCodeTextField');
	    
	    /*buttons*/
	    this.okBtn = $('card.popup.okButton');
	    this.cancelBtn = $('card.popup.cancelButton');	   
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*show details panes*/	    
	    this.cardDetailsPane.style.display = this.swipeCardCheckBox.checked ? 'none' : '';
	    this.trackDataDetailsPane.style.display = this.swipeCardCheckBox.checked ? '' : 'none';
			    
	    this.swipeCardCheckBox.panel = this;
	    this.swipeCardCheckBox.onchange = function(e){
	    	this.panel.cardDetailsPane.style.display = this.checked ? 'none' : '';
	 	    this.panel.trackDataDetailsPane.style.display = this.checked ? '' : 'none';	 	   
		    
		 	this.panel.trackDataTextField.value = '';
		 	this.panel.cardNumberTextField.value = '';
		 	this.panel.cardExpiryDateTextField.value = '';
		 	this.panel.cardCvvTextField.value = '';
		 	this.panel.cardHolderNameTextField.value = '';
		 	this.panel.streetAddressTextField.value = '';
		 	this.panel.zipCodeTextField.value = '';
		};
		
		/*track data*/
		this.trackDataTextField.panel = this;
		this.trackDataTextField.onkeypress = function(e){			
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.credit.card');
					return;
				}
						
				this.panel.okHandler();		
			}
		};
		
		/*card number*/
		this.cardNumberTextField.panel = this;
		this.cardNumberTextField.onkeypress = function(e){			
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('enter.credit.card.number');
					return;
				}
						
				this.panel.okHandler();		
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.swipeCardCheckBox.checked = true;
	  	/*this.cardSelectList.options[0].selected = true;*/
	    
	    this.trackDataTextField.value = '';
	    this.cardNumberTextField.value = '';
	    this.cardExpiryDateTextField.value = '';
	    this.cardCvvTextField.value = '';
	    this.cardHolderNameTextField.value = '';
	    this.streetAddressTextField.value = '';
	    this.zipCodeTextField.value = '';
	  },
	  onShow:function(){
	  	this.trackDataTextField.focus();
	  },
	  
	  validate:function(){
		  
		  if(this.swipeCardCheckBox.checked){
			  /*card was swipped*/ 
			  if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
				  this.onError('swipe.credit.card');
				  return false;
			  }
		  }
		  else{
			  /*no card environment*/ 
			  if(this.cardNumberTextField.value == null || this.cardNumberTextField.value == ''){
				  this.onError('credit.card.number.is.required');
				  return false;
			  }
			  
			  if(this.cardExpiryDateTextField.value == null || this.cardExpiryDateTextField.value == ''){
				  this.onError('credit.card.has.expired');
				  return false;
			  }
				
			  
		  }
		  
		  this.setPayment();
		  		  
		  return true;
	  },
	  
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('isCardPresent', this.swipeCardCheckBox.checked);
	  	payment.set('cardType',"M");
	  	payment.set('cardTrackDataEncrypted',null);
	  	
	  	if(this.swipeCardCheckBox.checked == true){
	  		payment.set('trackData',this.trackDataTextField.value);		  		
	  	}
	  	else{	  		
	  		payment.set('cardNumber',this.cardNumberTextField.value);
	  		payment.set('cardExpiryDate',this.cardExpiryDateTextField.value);
	  		payment.set('cardCvv',this.cardCvvTextField.value);
	  		payment.set('cardHolderName',this.cardHolderNameTextField.value);
	  		payment.set('streetAddress',this.streetAddressTextField.value);
	  		payment.set('zipCode',this.zipCodeTextField.value);
	  	}	  	
	  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* Cheque Panel */
var ChequePanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('cheque.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('cheque.popup.okButton');
	    this.cancelBtn = $('cheque.popup.cancelButton');
	    
	    /*labels*/
	    this.totalLabel = $('cheque.popup.totalLabel');
	    
	    /*textfields*/
	    this.chequeNumberTextField = $('cheque.popup.chequeNumberTextField');    
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);

	    this.chequeNumberTextField.panel = this;
	    this.chequeNumberTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){	
				if(this.value == null || this.value == ''){
					this.panel.onError('enter.check.number');
					return;
				}
						
				this.panel.validate();			
			}
		};
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.chequeNumberTextField.value = '';
	  },
	  onShow:function(){
	  	this.chequeNumberTextField.focus();
	  },
	  validate:function(){
		  if(this.chequeNumberTextField.value == null || this.chequeNumberTextField.value == ''){
			  this.onError('invalid.cheque.number');
			  return false;
		  }
		  
		  this.setPayment();
		  
		  return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('chequeNumber', this.chequeNumberTextField.value);	  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* Mix Panel */
var MixPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('mix.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('mix.popup.okButton');
	    this.cancelBtn = $('mix.popup.cancelButton');
	    
	    /*textfields*/
	    this.cashAmountTextField = $('mix.popup.cashAmountTextField');
	    this.cardAmountTextField = $('mix.popup.cardAmountTextField');	    
	    this.chequeAmountTextField = $('mix.popup.chequeAmountTextField');
	    this.voucherAmountTextField = $('mix.popup.voucherAmountTextField');
	    this.voucherNoTextField = $('mix.popup.voucherNoTextField');
	    
	    this.chequeNumberTextField = $('mix.popup.chequeNumberTextField');	    
	    
	    /*this.cardSelectList = $('mix.popup.cardSelectList');*/
	    this.swipeCardCheckBox = $('mix.popup.swipeCardCheckBox');	    
	    this.trackDataTextField = $('mix.popup.trackDataTextField');
	    this.cardNumberTextField = $('mix.popup.cardNumberTextField');
	    this.cardExpiryDateTextField = $('mix.popup.cardExpiryDateTextField');
	    this.cardCvvTextField = $('mix.popup.cardCvvTextField');
	    this.cardHolderNameTextField = $('mix.popup.cardHolderNameTextField');
	    this.streetAddressTextField = $('mix.popup.streetAddressTextField');
	    this.zipCodeTextField = $('mix.popup.zipCodeTextField');
	    
	    this.externalCardAmountTextField = $('mix.popup.externalCardAmountTextField');
	    
	    /*labels*/
	    this.totalLabel = $('mix.popup.totalLabel');
	    
	    /*panes*/
	    this.trackDataDetailsPane = $('mix.popup.trackDataDetailsPane');
	    this.cardDetailsPane = $('mix.popup.cardDetailsPane');
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /** adding behaviours **/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*show details panes*/	    
	    this.cardDetailsPane.style.display = this.swipeCardCheckBox.checked ? 'none' : '';
	    this.trackDataDetailsPane.style.display = this.swipeCardCheckBox.checked ? '' : 'none';
			    
	    this.swipeCardCheckBox.panel = this;
	    this.swipeCardCheckBox.onchange = function(e){
	    	this.panel.cardDetailsPane.style.display = this.checked ? 'none' : '';
	 	    this.panel.trackDataDetailsPane.style.display = this.checked ? '' : 'none';			
		};

	    this.cashAmountTextField.panel = this;
	    this.cashAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	cashAmount = parseFloat(totalAmount);
	    	
	    	var cashAmount = this.value;
	    	cashAmount = parseFloat(cashAmount);
	    	
	    	if(isNaN(cashAmount)) return;
	    	
	    	if(cashAmount < 0.0 || cashAmount > totalAmount){
	    		this.panel.onError('invalid.cash.amt');
	    		return;
	    	}
	    };
	    
	    this.cashAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	cashAmount = parseFloat(totalAmount);
	    	
	    	var cashAmount = this.value;
	    	cashAmount = parseFloat(cashAmount);
	    	
	    	if(isNaN(cashAmount)) return;
	    	
	    	if(cashAmount < 0.0 || cashAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cardAmount = parseFloat(this.panel.cardAmountTextField.value);
	    	if(isNaN(cardAmount)) cardAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    	
	    	this.panel.cardAmountTextField.value = new Number(cardAmount).toFixed(2);
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
			
			/*update card text field*/
			var sum = chequeAmount + voucherAmount + externalCardAmount + cashAmount;
			var remainder = totalAmount - sum;
			this.panel.cardAmountTextField.value = new Number(remainder).toFixed(2);
	    };
	    
	    this.cardAmountTextField.panel = this; 
	    this.cardAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		this.panel.onError('invalid.card.amt');
	    		return;
	    	}
	    };
	    
	    this.cardAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    				
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
	    };
	    
	    this.chequeAmountTextField.panel = this;
	    this.chequeAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var chequeAmount = this.value;
	    	chequeAmount = parseFloat(chequeAmount);
	    	
	    	if(isNaN(chequeAmount)) return;
	    	
	    	if(chequeAmount < 0.0 || chequeAmount > totalAmount){
	    		this.panel.onError('invalid.cheque.amt');
	    		return;
	    	}
	    };
	    
	    this.chequeAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var chequeAmount = this.value;
	    	chequeAmount = parseFloat(chequeAmount);
	    	
	    	if(isNaN(chequeAmount)) return;
	    	
	    	if(chequeAmount < 0.0 || chequeAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    	var cardAmount = parseFloat(this.panel.cardAmountTextField.value);
	    	if(isNaN(cardAmount)) cardAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    		    	
	    	
	    	this.panel.cardAmountTextField.value = new Number(cardAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
	    };
	    
	    this.voucherAmountTextField.panel = this;
	    this.voucherAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var voucherAmount = this.value;
	    	voucherAmount = parseFloat(voucherAmount);
	    	
	    	if(isNaN(voucherAmount)) return;
	    	
	    	if(voucherAmount < 0.0 || voucherAmount > totalAmount){
	    		this.panel.onError('invalid.voucher.amount.');
	    		return;
	    	}
	    };
	    
	    this.voucherAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var voucherAmount = this.value;
	    	voucherAmount = parseFloat(voucherAmount);
	    	
	    	if(isNaN(voucherAmount)) return;
	    	
	    	if(voucherAmount < 0.0 || voucherAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cardAmount = parseFloat(this.panel.cardAmountTextField.value);
	    	if(isNaN(cardAmount)) cardAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    				
			this.panel.cardAmountTextField.value = new Number(cardAmount).toFixed(2);
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			
			var externalCardAmount = parseFloat(this.panel.externalCardAmountTextField.value);
	    	if(isNaN(externalCardAmount)) externalCardAmount = 0.0;
			this.panel.externalCardAmountTextField.value = new Number(externalCardAmount).toFixed(2);
	    };
	    
	    this.externalCardAmountTextField.panel = this;
	    this.externalCardAmountTextField.onkeypress = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		this.panel.onError('invalid.card.amt');
	    		return;
	    	}
	    };
	    
	    this.externalCardAmountTextField.onkeyup = function(e){
	    	var totalAmount = this.panel.cartTotalAmount;
	    	
	    	var cardAmount = this.value;
	    	cardAmount = parseFloat(cardAmount);
	    	
	    	if(isNaN(cardAmount)) return;
	    	
	    	if(cardAmount < 0.0 || cardAmount > totalAmount){
	    		return;
	    	}
	    	
	    	var cashAmount = parseFloat(this.panel.cashAmountTextField.value);
	    	if(isNaN(cashAmount)) cashAmount = 0.0;
	    	
	    	var chequeAmount = parseFloat(this.panel.chequeAmountTextField.value);
	    	if(isNaN(chequeAmount)) chequeAmount = 0.0;
	    	
	    	var voucherAmount = parseFloat(this.panel.voucherAmountTextField.value);
	    	if(isNaN(voucherAmount)) voucherAmount = 0.0;
	    				
			this.panel.chequeAmountTextField.value = new Number(chequeAmount).toFixed(2);
			this.panel.cashAmountTextField.value = new Number(cashAmount).toFixed(2);
			this.panel.voucherAmountTextField.value = new Number(voucherAmount).toFixed(2);
	    };
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	/*amounts*/
	  	this.cashAmountTextField.value = '';
	    this.cardAmountTextField.value = '';   
	    this.chequeAmountTextField.value = '';
	    this.voucherAmountTextField.value = '';
	    this.externalCardAmountTextField.value = '';   
	    
	    /*cheque*/
	    this.chequeNumberTextField.value = '';
	    
	  	/*creditcard*/
	  	this.swipeCardCheckBox.checked = true;
	  	/*this.cardSelectList.options[0].selected = true;*/
	  		    
	    this.trackDataTextField.value = '';
	    this.cardNumberTextField.value = '';
	    this.cardExpiryDateTextField.value = '';
	    this.cardCvvTextField.value = '';
	    this.cardHolderNameTextField.value = '';
	    this.streetAddressTextField.value = '';
	    this.zipCodeTextField.value = '';
	    
	    /*voucher*/
	    this.voucherNoTextField.value = '';
	    
	  },
	  onShow:function(){
		var totalAmount = this.cartTotalAmount;
		this.cashAmountTextField.value = new Number(totalAmount).toFixed(2);
	  	this.cashAmountTextField.focus();
	  },
	  validate:function(){
		  var totalAmount = this.cartTotalAmount;
		  
		  var cashAmount = parseFloat(this.cashAmountTextField.value);
		  if(isNaN(cashAmount)){
		  	cashAmount = 0.0;
		  	this.cashAmountTextField.value = '0';
		  }
		  
		  var cardAmount = parseFloat(this.cardAmountTextField.value);
		  if(isNaN(cardAmount)){
		  	cardAmount = 0.0;
		  	this.cardAmountTextField.value = '0';
		  }
		  
		  var externalCardAmount = parseFloat(this.externalCardAmountTextField.value);
		  if(isNaN(externalCardAmount)){
			  externalCardAmount = 0.0;
		  	this.externalCardAmountTextField.value = '0';
		  }
		  
		  var chequeAmount = parseFloat(this.chequeAmountTextField.value);
		  if(isNaN(chequeAmount)){
		  	chequeAmount = 0.0;
		  	this.chequeAmountTextField.value = '0';
		  }
		  
		  var voucherAmount = parseFloat(this.voucherAmountTextField.value);
		  if(isNaN(voucherAmount)){
		  	voucherAmount = 0.0;
		  	this.voucherAmountTextField.value = '0';
		  }
		  
		  var voucherNo = this.voucherNoTextField.value;
		  
		  if (voucherAmount != new Number(0) && voucherNo == '')
		  {
			  this.onError('enter.order.no.for.voucher');
			  return false;
		  }
		  
		  var paymentAmt = cashAmount + cardAmount + chequeAmount + voucherAmount + externalCardAmount;
		  var difference = totalAmount - paymentAmt;
		  
		  difference = new Number(difference).toFixed(2)
		  difference = parseFloat(difference);
		  
		  if(difference > 0.0){
			  this.onError('amount.tendered.is.less');
			  return false;
		  }
		  
		
		  if(difference < 0.0){
			  this.onError('amount.tendered.is.more');
			  return false;
		  }
		  
		  if(paymentAmt == 0.0){
			  this.onError('please.enter.amt');
			  return false;
		  }
		  
		  if(chequeAmount != 0.0 || this.chequeNumberTextField.value != ''){
			/* cheque validation */
			if(chequeAmount == 0.0){
				this.onError('enter.cheque.amt');
				return false;
			}
			
			if(chequeAmount < 0.0){
				this.onError('invalid.cheque.amt');
				return false;
			}
			
			if(this.chequeNumberTextField.value == ''){
				this.onError('invalid.cheque.number');
				return false;
			}				
		  }				  
		  
		  
		  if(cardAmount > 0.0 || this.trackDataTextField.value != '' || this.cardNumberTextField.value != ''){
			/* card validation */ 
			if(cardAmount == 0.0){
				this.onError('enter.card.amt');
				return false;
			}
			
			if(cardAmount < 0.0){
				this.onError('invalid.card.amt');
				return false;
			}
			
			if(this.swipeCardCheckBox.checked){
				/*card was swipped*/ 
				if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
					this.onError('card.not.swiped');
					return false;
				}
			  }
			  else{
				  /*no card environment*/ 
				  if(this.cardNumberTextField.value == null || this.cardNumberTextField.value == ''){
					  this.onError('js.enter.card.no');
					  return false;
				  }
				  
				  if(this.cardExpiryDateTextField.value == null || this.cardExpiryDateTextField.value == ''){
					  this.onError('js.enter.card.expiry');
					  return false;
				  }
					
				  
			  }
			
		  }	
		  
		  if(voucherAmount != 0.0 || this.voucherNoTextField.value != ''){
				/* voucher validation */
				if(voucherAmount == 0.0){
					this.onError('enter.voucher.amt');
					return false;
				}
				
				if(voucherAmount < 0.0){
					this.onError('invalid.voucher.amt');
					return false;
				}
				
				if(this.voucherNoTextField.value == ''){
					this.onError('enter.order.no');
					return false;
				}				
			  }	
		  if (cardAmount > 0.0 && externalCardAmount > 0.0)
		  {
			  this.onError("can't.use.both.credit.card.and.external.card");
			  return false;
		  }
		  
		  this.setPayment();
		  
		  return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	
	  	payment.set('cashAmount', this.cashAmountTextField.value);
	  	payment.set('cardAmount', this.cardAmountTextField.value);
	  	payment.set('externalCardAmount', this.externalCardAmountTextField.value);
	  	payment.set('chequeAmount', this.chequeAmountTextField.value);
	  	
	  	payment.set('chequeNumber', this.chequeNumberTextField.value);
	  	
	  	payment.set('isCardPresent', this.swipeCardCheckBox.checked);
	  	/*payment.set('cardType',this.cardSelectList.value);*/
	  	
	  	if(this.swipeCardCheckBox.checked){
	  		payment.set('trackData',this.trackDataTextField.value);		  		
	  	}
	  	else{	  		
	  		payment.set('cardNumber',this.cardNumberTextField.value);
	  		payment.set('cardExpiryDate',this.cardExpiryDateTextField.value);
	  		payment.set('cardCvv',this.cardCvvTextField.value);
	  		payment.set('cardHolderName',this.cardHolderNameTextField.value);
	  		payment.set('streetAddress',this.streetAddressTextField.value);
	  		payment.set('zipCode',this.zipCodeTextField.value);
	  	}	  
	  	
	  	payment.set('voucherAmt', this.voucherAmountTextField.value);
	  	payment.set('voucherNo', this.voucherNoTextField.value);
	  	
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
});

/* OnCredit Panel */
var OnCreditPanel =  Class.create(CheckoutPanel, {
	  initialize: function() {
		$super('onCredit.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('onCredit.popup.okButton');
	    this.cancelBtn = $('onCredit.popup.cancelButton');
	    
	    /*textfields*/
	    
	    
	    /*labels*/
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	  },  
	  okHandler:function(){	  
		  this.hide();
	  },
	  resetHandler:function(){
	  },
	  onShow:function(){
	  },
	  setPayment:function(){
	  	var payment = new Hash();	  	
		  	this.paymentHandler(payment);
	  },
	  paymentHandler:function(payment){}
});

/* PIN Panel */
var PINPanel =  Class.create(PopUpBase, {
	  initialize: function() {
	    this.createPopUp($('pin.popup.panel'));
	    
	    /*buttons*/
	    this.okBtn = $('pin.popup.okButton');
	    this.cancelBtn = $('pin.popup.cancelButton');
	    
	    /*textfields*/
	    this.pinTextField = $('pin.popup.pinTextField');	    
	    Event.observe(this.pinTextField,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);
	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.pinTextField.panel = this;
	    this.pinTextField.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};
	    
	  }, 
	  validate:function(){
	  	if(this.pinTextField.value == null || this.pinTextField.value == ''){
			this.onError('invalid.pin');
			this.pinTextField.focus();
			return;
		}
		
	  	var url = 'OrderAction.do?action=validatePIN&pin=' + this.pinTextField.value;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});	
	  },
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.pinTextField.value = '';
	  },
	  onShow:function(){
	  	this.pinTextField.focus();
	  },
	  onComplete:function(request){		  
	  },
	  onSuccess:function(request){	  		
  		var responseText = request.responseText;
  		var result = eval('(' + responseText + ')');
  		
  		if(result.error){
  			this.onError(result.error);
  			this.pinTextField.focus();
  			return;
  		}
  		
  		var roleId = result.roleId;
		var discountLimit = result.discountLimit;
		var overrideLimit = result.overrideLimit;
		var discountOnTotal = result.discountOnTotal;
		var discountUptoPriceLimit = result.discountUptoPriceLimit;	
		var allowOrderBackDate = result.allowOrderBackDate;	
		var allowUpSell = result.allowUpSell;
	  	
	  	var discountRights = new DiscountRights(discountLimit, overrideLimit, discountUptoPriceLimit, discountOnTotal, allowOrderBackDate, allowUpSell);
	  	
	  	/* notify discount manager */
	  	DiscountManager.setDiscountRights(discountRights);
	  	
		this.hide();
	  },
	  onFailure:function(request){	
		  this.onError("failed.to.process.request");
		  this.pinTextField.focus();
	  }
});


/*Delivery Panel*/
var DeliveryPanel =  Class.create(CheckoutPanel, {
  initialize: function() {
	this.createPopUp($('delivery.popup.panel'));
    
    /*buttons*/
    this.nowBtn = $('delivery.popup.now.button');    
    this.nowBtn.onclick = this.setDeliveryNow.bind(this);
    
    this.laterBtn = $('delivery.popup.later.button');    
    this.laterBtn.onclick = this.setDeliveryLater.bind(this);
    
    this.closeBtn = $('delivery.popup.close.button');
    this.closeBtn.onclick = this.hide.bind(this);
    
    this.okBtn = $('delivery.popup.ok.button');
    this.okBtn.onclick = this.okHandler.bind(this);
    
    /*panels*/
    this.confimationPanel = $('delivery.popup.confirmation.panel');
    this.datePanel = $('delivery.popup.date.panel');
    
    this.dateTextField = $('delivery.popup.date.textfield');
    this.dateButton = $('delivery.popup.date.button');
    this.dateButton.onclick = this.schedule.bind(this);
    
    this.paymentTermDropDown = $('payment-term-dropdown');
    this.paymentTermId = this.paymentTermDropDown.options[this.paymentTermDropDown.selectedIndex].value;
    this.paymentTermDropDown.onchange = this.setPaymentTerm.bind(this); 
    
    this.calendar = Calendar.setup({"inputField":this.dateTextField, 
    	"ifFormat":"%Y-%m-%d",
    	"daFormat":"%Y-%m-%d",
    	"button":this.dateButton});
  }, 
  
  resetHandler:function(){
	  this.deliveryRule = 'O';
	  this.confimationPanel.style.display = 'block';
	  this.datePanel.style.display = 'none';
	  this.dateTextField.value = "";
	  this.nowBtn.checked = false;
	  this.laterBtn.checked = false;
  },
  
  onShow:function(){
	  this.deliveryRule = 'O';
	  
	  var bpPaymentTermId = BPManager.getBP().getPaymentTermId();
	  
	  var options = this.paymentTermDropDown.options;
	  
	  for(var i=0; i<options.length; i++)
	  {
		  if (options[i].value == bpPaymentTermId)
		  {
			  options[i].selected = "selected";
			  break;
		  }
	  }
  },
  
  setDeliveryNow:function(){
	  if (this.nowBtn.checked)
	  {
		  this.deliveryRule = 'O';
		  this.laterBtn.checked = false;
	  }
	  else
	  {
		  this.laterBtn.checked = true;
		  this.setDeliveryLater();
	  }
  },
  
  setDeliveryLater:function(){
	  if (this.laterBtn.checked)
	  {
		  this.deliveryRule = 'M';
		  this.nowBtn.checked = false;
		  this.confimationPanel.style.display = 'none';
		  this.datePanel.style.display = 'block';
	  }
	  else
	  {
		  this.nowBtn.checked = true;
		  this.setDeliveryNow();
	  }
  },
  
  schedule:function(){
	  this.deliveryRule = 'M';
	  this.deliveryDate = this.dateTextField.value + ' 00:00:00';
  },
  
  setPayment:function(){
  	/*set payment*/
	var payment = new Hash();
	payment.set('deliveryRule',this.deliveryRule);
	
	if (this.deliveryRule == 'M')
	{
		this.deliveryDate = this.dateTextField.value + ' 00:00:00';
	}
	
	payment.set('deliveryDate',this.deliveryDate);
	payment.set('paymentTermId', this.paymentTermId);
	this.payment = payment;
	
  	this.hide();
	this.paymentHandler(this.payment);
  },
  
  paymentHandler:function(payment){},
  
  okHandler:function(){
	  this.setPayment();
  },
  
  setPaymentTerm:function(){
	  this.paymentTermId = this.paymentTermDropDown.options[this.paymentTermDropDown.selectedIndex].value;
  }
  
});

/*XWeb Panel*/
var XWebPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('xweb.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('xweb.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('xweb.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getOTK&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*Mercury Panel*/
var MercuryPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('mercury.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('mercury.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('mercury.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getMercuryOTK&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*ElementPS Panel*/
var ElementPSPanel =  Class.create(PopUpBase, {
  initialize: function() {
	this.createPopUp($('elementps.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('elementps.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('elementps.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getElementPSTransactionId&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*Merchant Warehouse Genius Panel*/
var MerchantWarehouseGeniusPanel =  Class.create(PopUpBase, {
  initialize: function() {
	this.createPopUp($('merchant.warehouse.genius.popup.panel'));
    
    /*buttons*/
    this.cancelBtn = $('merchant.warehouse.genius.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('merchant.warehouse.genius.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getTransactionKeyGeniusDevice&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});


/*MerchantWarehouse Panel*/
var MerchantWarehousePanel =  Class.create(CheckoutPanel, {
	initialize: function($super) {
	$super('merchant.warehouse.popup.panel');
    
	/*buttons*/
    this.cancelBtn = $('merchant.warehouse.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('merchant.warehouse.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getTransactionKey&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/* Payment panel */
var PaymentPopUp = Class.create(PopUpBase, {
	initialize: function() {
	    this.createPopUp($('payment.popup.panel'));
	    
	    /*buttons*/
	   // this.okBtn = $('payment.popup.okButton');
	    this.cancelBtn = $('payment.popup.cancelButton');
	    
	    this.cashBtn = $('payment.popup.cashPaymentButton');
	    this.cardBtn = $('payment.popup.cardPaymentButton');
	    this.chequeBtn = $('payment.popup.chequePaymentButton');
	    this.mixBtn = $('payment.popup.mixPaymentButton');
	    this.onCreditBtn = $('payment.popup.onCreditPaymentButton');
	    
	    /* external credit card processing */
	    this.externalCreditCardMachine = $('payment.popup.externalCreditCardMachineButton');
	    
	    /* voucher */
	    this.voucherBtn = $('payment.popup.voucherPaymentButton');
	    
	    /* e2e */
	    this.e2eCardBtn = $('payment.popup.e2eCardPaymentButton');
	    
	    /* gift card */
	    this.giftCardBtn = $('payment.popup.giftCardPaymentButton');
	    
	    /* SK wallet */
	    this.SKWalletBtn = $('payment.popup.SKWalletPaymentButton');
	    
	    /* Zapper */
	    this.ZapperBtn = $('payment.popup.ZapperPaymentButton');
	    
	    /* Loyalty */
	    this.LoyaltyBtn = $('payment.popup.loyaltyPaymentButton');
	    
	    /* MCB Juice */
	    this.MCBJuiceBtn = $('payment.popup.MCBJuicePaymentButton');
	    
	    /* MY.T Money */
	    this.MYTMoneyBtn = $('payment.popup.MYTMoneyPaymentButton');
	    
	    /* Emtel Money */
	    this.EmtelMoney = $('payment.popup.EmtelMoneyPaymentButton');
	    
	    /* Gifts.mu */
	    this.GiftsMuBtn = $('payment.popup.GiftsMuPaymentButton');
	    
	    /* MIPS */
	    this.MipsBtn = $('payment.popup.MipsPaymentButton');
	    	    
	    /* add behaviour */	
		for(var field in this){
			var fieldContents = this[field];
			
			if (fieldContents == null || typeof(fieldContents) == "function") {
				continue;
		    }
		    
		    if(fieldContents.type == 'button'){
		    	//register click handler
		    	fieldContents.onclick = this.clickHandler.bindAsEventListener(this);
		    }
		}
		
		 /*override events*/
	    this.cancelBtn.onclick = this.cancelHandler.bind(this);
	   // this.okBtn.onclick = this.okHandler.bind(this); 
	},
	okHandler:function(){	  
		if(this.tenderType == null) this.tenderType = TENDER_TYPE.CASH;		
		this.hide();
	},
	cancelHandler:function(){
		this.tenderType = null;
		this.isPaymentOnline = true;
		this.hide();
	},
	onShow:function(){
		
		this.tenderType = TENDER_TYPE.CASH;
		
		if(this.cashBtn){
			this.cashBtn.focus();
		}		
		
		if(this.LoyaltyBtn){
			if(BPManager.getBP().enableLoyalty == 'N'){
				this.LoyaltyBtn.style.display = "none";
			}
			else{
				this.LoyaltyBtn.style.display = "block";
			}
		}
		
	},
	onHide:function(){
		if(this.tenderType == null) return;	
		
		OrderScreen.setTenderType(this.tenderType);
		OrderScreen.getPaymentDetails();
	},
	clickHandler:function(e){
		var element = Event.element(e);
		
		var count = 0;
		while(!(element instanceof HTMLButtonElement)){
			count++;
			if(count > 10){
				alert(Translation.translate('fail.to.get.event.source','Fail to get event source!'));
				break;
			} 
			element = element.parentNode;
		}	
		
		switch(element){
			case this.cashBtn:
				this.tenderType = TENDER_TYPE.CASH;
				break;
			
			case this.cardBtn:
				this.tenderType = TENDER_TYPE.CARD;
				OrderScreen.setIsPaymentOnline(true);
				break;
				
			case this.chequeBtn:
				this.tenderType = TENDER_TYPE.CHEQUE;
				break;
				
			case this.mixBtn:
				if(!confirm('Do you want to proceed with Mix Payment?')) return;
				this.tenderType = TENDER_TYPE.MIXED;
				break;
				
			case this.onCreditBtn:
				this.tenderType = TENDER_TYPE.CREDIT;
				break;
		
			case this.MCBJuiceBtn:
				if(!confirm('Do you want to proceed with MCB Juice?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.MCB_JUICE;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.MYTMoneyBtn:
				if(!confirm('Do you want to proceed with MY.T Money?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.MY_T_MONEY;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.EmtelMoney:
				if(!confirm('Do you want to proceed with Blink?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.EMTEL_MONEY;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.GiftsMuBtn:
				if(!confirm('Do you want to proceed with Gifts.mu?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.GIFTS_MU;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.MipsBtn:
				if(!confirm('Do you want to proceed with MIPS?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.MIPS;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.externalCreditCardMachine:
				if(!confirm('Do you want to proceed with External Card Machine?')) return;
				if(!confirm('Has the payment gone through?')) return;
				this.tenderType = TENDER_TYPE.EXTERNAL_CARD;
				OrderScreen.setIsPaymentOnline(false);				
				break;
				
			case this.voucherBtn:
				this.tenderType = TENDER_TYPE.VOUCHER;
				break;
				
			case this.e2eCardBtn:
				this.tenderType = TENDER_TYPE.E2E_CARD;
				break;
				
			case this.giftCardBtn:
				this.tenderType = TENDER_TYPE.GIFT_CARD;
				break;
				
			case this.SKWalletBtn:
				this.tenderType = TENDER_TYPE.SK_WALLET;
				break;
				
			case this.ZapperBtn:
				this.tenderType = TENDER_TYPE.ZAPPER;
				break;
				
			case this.LoyaltyBtn:
				this.tenderType = TENDER_TYPE.LOYALTY;
				break;
				
			/**/
			case this.MCBJuiceBtn:
				this.tenderType = TENDER_TYPE.MCB_JUICE;
				break;
			
			case this.MYTMoneyBtn:
				this.tenderType = TENDER_TYPE.MY_T_MONEY;
				break;
				
			case this.EmtelMoneyBtn:
				this.tenderType = TENDER_TYPE.EMTEL_MONEY;
				break;
			
			case this.MipsBtn:
				this.tenderType = TENDER_TYPE.MIPS;
				break;
			
			case this.GiftsMuBtn:
				this.tenderType = TENDER_TYPE.GIFTS_MU;
				break;
				
			default:break;
		}
		
		this.okHandler();
	}
});


/*Voucher Panel*/
var VoucherPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('voucher.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('voucher.popup.okButton');
	    this.cancelBtn = $('voucher.popup.cancelButton');
	    
	    /*textfields*/
	    this.voucherNoTextField = $('voucher.popup.voucherNoTextField');
	    
	    /*labels*/
	    this.totalLabel = $('voucher.popup.totalLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    /*others*/
	    this.voucherNoTextField.panel = this;
	    this.voucherNoTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('invalid.voucher.number');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    
	    this.organisationSelectList = $('voucher-popup-dropdown');	 
		
		var terminals = TerminalManager.terminalList;
		var organisations = new Array();
  
		/* this.organisationSelectList.options.length = 0; */
  
		for(var i=0; i<terminals.length; i++)
		{
			var terminal = terminals[i];
			var orgName = terminal.org_name;
			var orgId = terminal.org_id;
		
			if(organisations.indexOf(orgId) == -1){
				organisations.push(orgId);
				this.organisationSelectList.options[this.organisationSelectList.options.length] = new Option(orgName, orgId, false, false)					
			}   	   			
		}
	  
		var orgId = TerminalManager.terminal.orgId;
		this.organisationSelectList.setValue(orgId);
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.voucherNoTextField.value = '';
	  },
	  onShow:function(){
	  	this.voucherNoTextField.focus();
	  },
	  validate:function(){		  
		  
		  if(this.voucherNoTextField.value == null || this.voucherNoTextField.value == ''){
			  this.onError('invalid.voucher.number');
			  this.voucherNoTextField.focus();
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('voucherAmt', this.cartTotalAmount);
	  	payment.set('voucherNo', this.voucherNoTextField.value);
	  	payment.set('voucherOrgId', this.organisationSelectList.value);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});

/*Loyalty Panel*/
var LoyaltyPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('loyalty.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('loyalty.popup.okButton');
	    this.cancelBtn = $('loyalty.popup.cancelButton');	    
	    
	    /*labels*/
	    this.totalLabel = $('loyalty.popup.totalLabel');
	    this.loyaltyAvailableLabel = $('loyalty.popup.loyaltyAvailableLabel');
	    this.loyaltyRemainingLabel = $('loyalty.popup.loyaltyRemainingLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    var cart = ShoppingCartManager.getCart();	
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.loyaltyPoints = BPManager.bp.loyaltyPoints - cart.promotionPointsTotal;
	    this.loyaltyStartingPoints = BPManager.bp.loyaltyStartingPoints;
	    
	    this.loyaltyAvailableLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.loyaltyPoints).toFixed(2);
	    this.loyaltyRemainingLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.loyaltyPoints - this.cartTotalAmount).toFixed(2);
	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	/*nothing*/
	  },
	  onShow:function(){
	  	/*nothing*/
	  },
	  validate:function(){	
		  
		  var diff = parseFloat(this.loyaltyPoints) - parseFloat(this.cartTotalAmount);
		  
		  if(diff < 0.0){
			  this.onError('Loyalty points is less than order total! Loyalty points:' 
					  + new Number(this.loyaltyPoints).toFixed(2) + ' Order total:' + new Number(this.cartTotalAmount).toFixed(2));			  
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('loyaltyAmt', this.cartTotalAmount);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});


var EditOnFlyPanel = Class.create(PopUpBase, {
	initialize: function() {
    this.createPopUp($('edit-on-fly-panel'));
    
    this.product = null;
    
    /*buttons*/
    this.okBtn = $('edit-on-fly-ok-button');
    this.cancelBtn = $('edit-on-fly-cancel-button');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    this.okBtn.onclick = this.okHandler.bind(this); 
    
    /*textfields*/
    this.priceTextfield = $('edit-on-fly-price-textfield');
    this.descriptionTextfield = $('edit-on-fly-description-textfield');    
	},

	resetHandler:function(){
	  	this.priceTextfield.value = '';
	  	this.descriptionTextfield.value = '';
	  	
	  	this.priceTextfield.disabled = false;
	  	this.descriptionTextfield.disabled = false;
	},
	
	onShow:function(){		
		
		if(this.product.editpriceonfly == 'Y'){
			this.priceTextfield.value = this.product.pricestd;
			this.priceTextfield.disabled = false;
			//$('edit-on-fly-price-row').show();
		}
		else{
			this.priceTextfield.disabled = true;
			//$('edit-on-fly-price-row').hide();
		}
			
		if(this.product.editdesconfly == 'Y'){
			this.descriptionTextfield.value = '';
			this.descriptionTextfield.disabled =false;
			//$('edit-on-fly-description-row').show();
		}
		else{
			this.descriptionTextfield.disabled =true;
			//$('edit-on-fly-description-row').hide();
		}
		
	},
	
	okHandler:function(){
		var price = this.priceTextfield.value;
		var description = this.descriptionTextfield.value;		
		shoppingCart.addToCart(this.product.m_product_id,1, description, price);
		this.hide();
	},
	
	
});


/*e2e Panel*/
var E2EPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('e2e.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('e2e.popup.okButton');
	    this.cancelBtn = $('e2e.popup.cancelButton');
	    
	    /*textfields*/ 
	    this.trackDataTextField = $('e2e.popup.trackDataTextField');
	    
	    /*labels*/
	    this.totalLabel = $('e2e.popup.totalLabel');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    /*fields*/
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.trackDataTextField.onkeypress = function(e){
	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.card');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    	    
	  },  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  resetHandler:function(){
	  	this.trackDataTextField.value = '';
	  },
	  onShow:function(){
	  	this.trackDataTextField.focus();
	  },
	  validate:function(){
		  
		  if(this.trackDataTextField.value == null || this.trackDataTextField.value == ''){
			  this.onError('card.not.swiped');
			  this.trackDataTextField.focus();
			  return;
		  }
		  
		  this.setPayment();
		  
		return true;
	  },
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('cardAmt', this.cartTotalAmount);
	  	payment.set('cardTrackDataEncrypted', this.trackDataTextField.value);
	  	this.payment = payment;
	  },
	  paymentHandler:function(payment){}
	});


/*Gift Card Panel*/
var GiftCardPanel =  Class.create(CheckoutPanel, {
	  initialize: function($super) {
		$super('gift.popup.panel');
	    
	    /*buttons*/
	    this.okBtn = $('gift.popup.okButton');
	    this.cancelBtn = $('gift.popup.cancelButton');
	    
	    /*textfields*/ 
	    this.trackDataTextField = $('gift.popup.trackDataTextField');
	    this.numberTextField = $('gift.popup.numberTextField');
	    /*this.cvvTextField = $('gift.popup.cvvTextField');*/
	    
	    /*labels*/
	    this.totalLabel = $('gift.popup.totalLabel');
	    
	    /*radios*/
	    this.swipeRadio = $('gift.popup.swipeRadio');
	    this.manualRadio = $('gift.popup.manualRadio');
	    
	    /*panels*/
	    this.swipePanel = $('gift.popup.swipePanel');
	    this.manualPanel = $('gift.popup.manualPanel');
	    
	    /*add behaviour to components*/
	    this.swipeRadio.onclick = this.radioHandler.bind(this);
	    this.manualRadio.onclick = this.radioHandler.bind(this);
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.trackDataTextField.onkeypress = function(e){	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.card');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    
	    this.numberTextField.onkeypress = function(e){	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('card.number.required');
					return;
				}
				
				this.panel.okHandler();	
				return;
			}	    	
	    };
	    	    
	  }, 
	  
	  radioHandler:function(e){
		  var element = Event.element(e);
		  
		  this.trackDataTextField.value = '';
		  this.numberTextField.value = '';
		  /*this.cvvTextField.value = '';*/
		  
		  switch (element) {
		  
			case this.swipeRadio:	
				this.manualPanel.style.display = 'none';
				this.swipePanel.style.display = '';
				break;
				
			case this.manualRadio:	
				this.swipePanel.style.display = 'none';
				this.manualPanel.style.display = '';				
				break;
	
			default:
				break;
		  }
	  },
	  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  
	  resetHandler:function(){
	  	this.trackDataTextField.value = '';
	  	this.numberTextField.value = '';
	  	/*this.cvvTextField.value = "";*/
	  	
	  	this.swipeRadio.checked = true;
	  	
	  	this.manualPanel.style.display = 'none';
		this.swipePanel.style.display = '';
	  },
	  
	  onShow:function(){	
		  if(this.swipeRadio.checked)
		  {
			  this.trackDataTextField.focus();
		  }
		  else
		  {
			  this.numberTextField.focus();
		  }	  	
	  },
	  
	  validate:function(){
		  
		  if(this.swipeRadio.checked == true)
		  {
			  if(this.trackDataTextField.value == null || this.trackDataTextField.value.trim() == ""){
				  this.onError('card.not.swiped');
				  this.trackDataTextField.focus();
				  return;
			  }
		  }
		  else
		  {
			  if(this.numberTextField.value == null  || this.numberTextField.value.trim() == ""){
				  this.onError('card.number.required');
				  this.numberTextField.focus();
				  return;
			  }
			  
			  if (isNaN(this.numberTextField.value))
			  {
				  this.onError('card.number.should.contain.only.numeric.values');
				  this.numberTextField.focus();
				  return;
			  }
		  }		  
		  
		  this.setPayment();
		  
		return true;
	  },
	  
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('giftCardAmt', this.cartTotalAmount);
	  	payment.set('giftCardTrackData', this.trackDataTextField.value);
	  	payment.set('giftCardNo', this.numberTextField.value);
	  	/*payment.set('giftCardCVV', this.cvvTextField.value);*/	  	
	  	this.payment = payment;
	  },
	  
	  paymentHandler:function(payment){}
	});

/*Century Card Panel*/
var CenturyPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('century.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('century.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('century.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getCenturyPaymentForm&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/*UsaEpay Card Panel*/
var UsaEpayPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('usaepay.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('usaepay.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('usaepay.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getUsaEpayPaymentForm&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

/* apply payment processor popup*/
var ApplyPaymentProcessorPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('apply.payment.processor.popup'));
	    
	    /*buttons*/
	    this.applyPaymentProcessorBtn = $('apply.payment.processor.Btn');
	    this.applyPaymentProcessorCloseBtn = $('apply.payment.processor.popup.close.button');
	    this.applyPaymentProcessorCloseBtn.onclick = this.hide.bind(this);
	  
	    this.applyPaymentProcessorBtn.onclick = function(e){	    	
	    	window.location = 'PaymentProcessorAction.do?action=initPaymentRequestSupport'
	    };
	  
	  }  
	  
	});

/* external credit card popup*/
var ExtCreditCardPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		this.createPopUp($('external-credit-card-popup'));
	    /*buttons*/
	    this.applyPaymentProcessorBtn = $('external-credit-card-apply-payment-processor-btn');
	    this.proceedBtn = $('external-credit-card-proceed-btn');
	    this.applyPaymentProcessorCloseBtn = $('external-credit-card-popup-close-button');
	    this.applyPaymentProcessorCloseBtn.onclick = this.hide.bind(this);
	    this.proceedBtn.onclick = this.okHandler.bind(this);
	    
	    this.applyPaymentProcessorBtn.onclick = function(e){	    	
	    	window.location = 'PaymentProcessorAction.do?action=initFormPaymentProcessor'
	    };
	        
	  },
	  
	  paymentHandler:function(payment){
		  
	  },
	  
	  okHandler:function(){ 
		  this.hide();
		  this.paymentHandler(this.payment);
	  }
	});

/*Create Instance Product Popup Panel*/
var CreateInstanceProductPanel =  Class.create(PopUpBase, {
	  initialize : function(parentProduct)
	  {
		this.createPopUp($('create-instance-product-popup'));
		
	    /*buttons*/
		this.chooseExistingBtn = $('create-instance-product-popup-choose-existing-btn');
	    this.saveBtn = $('create-instance-product-popup-save-button');
	    this.closeBtn = $('create-instance-product-popup-close-button');
	    this.addParentToCartBtn = $('create-instance-product-popup-add-parent-to-cart-button');
	    this.saveBtn.onclick = this.save.bind(this);
	    this.closeBtn.onclick = this.hide.bind(this);
	    this.chooseExistingBtn.onclick = this.chooseExistingSerialNo.bind(this);
	    this.parentProduct = parentProduct;
	    this.m_product_parent_id = parentProduct.m_product_id;
	    this.serialNoTextField = $('create-instance-product-popup-serialNo-textField');
	    this.serialNoTextFieldContainer = $('serial-no-textField-container');
	    this.serialNoPopUpContainer =  $('serial-no-popup-container');
	    this.serialNoMessage = $('serial-no-message');
	    this.noOfChildren = this.parentProduct.childrenQty;
	    this.parentQty = this.parentProduct.parentQty;
	    this.qtyOnHand = this.parentProduct.qtyOnHand;
	    this.productListJSON = null;
	    
	    this.resetValues();
	  },
	  
	  save : function()
	  { 
		  if (this.serialNoTextField.value == '' || this.serialNoTextField.value == null)
		  {
			  new AlertPopUpPanel(Translation.translate('serial.no.is.mandatory','Serial No is mandatory!')).show();
			  return;
		  }
		  
		  var url = 'ProductAction.do';
		  var params = 'action=createInstanceProduct&m_product_parent_id=' + this.m_product_parent_id + '&serialNo=' + this.serialNoTextField.value
		  				+ '&orderType=' + OrderScreen.orderType;
		  
		  	new Ajax.Request( url,{ 
		  		method: 'post', 
				parameters: params,
				onSuccess: this.onSuccess.bind(this), 
				onFailure: this.onFailure.bind(this)
			});	
		  	
		  	this.hide();

	  },
	  
	  onSuccess : function(request)
	  {
		  var response = eval('('+request.responseText+')');
		  
		  if (response.success)
		  {
			  SearchProduct.onSelect(response.product);
			  SearchProduct.filterQuery = 'm_product.m_product_id=' + this.m_product_parent_id;
			  SearchProduct.param.set('isSerialNoSearch', false);
			  SearchProduct.search();
		  }
		  else
		  {
			  new AlertPopUpPanel(response.error).show();
		  }
	  },
	  
	  onFailure : function()
	  {
		  
	  },
	  
	  chooseExistingSerialNo : function()
	  {
		  this.hide();
		  SearchProduct.render(this.productListJSON);
	  },
	  
	  addParentToCart : function()
	  {
		  this.hide();
		  SearchProduct.onSelect(this.parentProduct);
	  },
	  
	  displayButtons : function()
	  {
		  this.resetValues();
		  
		  if (((OrderScreen.orderType == 'stock-transfer-send' 
			  || OrderScreen.orderType == 'stock-transfer-request') && this.parentQty > this.noOfChildren) 
			  || OrderScreen.orderType == 'POS Goods Receive Note')
	      {
		    	this.addParentToCartBtn.style.display='block';
		    	this.addParentToCartBtn.onclick = this.addParentToCart.bind(this);
	      }
		  
		  //For purchase we can generate serial no even if parent qty is zero
		  
		  if (OrderScreen.orderType == 'POS Goods Receive Note')
		  {
			  this.saveBtn.style.display = 'block';
			  this.serialNoTextFieldContainer.style.display = 'block';
		  }
		  
		  if (this.parentQty > 0)
		  {
			//Parent with no child created yet
    		this.saveBtn.style.display = 'block';
    		this.serialNoTextFieldContainer.style.display = 'block';
		  }
		  
		  if (this.noOfChildren == this.qtyOnHand)
		  {
			  this.serialNoMessage.innerHTML = 'All Serial Nos have already been generated.';
			  this.serialNoMessage.style.display = 'block';
			  
			  if (this.noOfChildren > 0)
              {
                  this.chooseExistingBtn.style.display = 'block';
              }		  
		  }
		  
		  if (this.noOfChildren > 0)
		  {
			  this.chooseExistingBtn.style.display = 'block';
		  }
		  
		  if (this.parentQty == 0 && this.noOfChildren == 0 && OrderScreen.orderType != 'POS Goods Receive Note')
		  {
			  this.serialNoMessage.innerHTML = 'All Serial Nos have already been generated and sold.';
			  this.serialNoMessage.style.display = 'block';
		  }
		  
		  
		  /*if (this.noOfChildren == 0)
	      {
		    	//this.chooseExistingBtn.style.display = 'none';
		    	
		    	if (this.parentProduct.qtyonhand > 0)
		    	{
		    		//Parent with no child created yet
		    		this.saveBtn.style.display = 'block';
		    		this.serialNoTextFieldContainer.style.display = 'block';
		    	}
	      }
		  else
	      {
		    	this.chooseExistingBtn.style.display = 'block';
		    	
		    	if (this.noOfChildren == this.parentProduct.qtyonhand && OrderScreen.orderType != 'POS Goods Receive Note')
		    	{
		    		//We cannot create more child
		    		//this.saveBtn.style.display = 'none';
		    		this.serialNoMessage.innerHTML = 'All Serial Nos have already been generated';
		    		this.serialNoMessage.style.display = 'block';
		    		//this.serialNoTextFieldContainer.style.display = 'none';
		    	}
		    	else
	    		{
		    		//We can create more child
		    		this.saveBtn.style.display = 'block';
		    		this.serialNoTextFieldContainer.style.display = 'block';
		    	}
	      }*/
		  
		  this.serialNoPopUpContainer.style.display = 'block';
	  },
	  
	  resetValues : function()
	  {
		  this.saveBtn.style.display = 'none';
		  this.serialNoTextFieldContainer.style.display = 'none';
		  this.serialNoMessage.innerHTML = '';
		  this.chooseExistingBtn.style.display = 'none';
		  this.serialNoMessage.style.display = 'none';
		  this.addParentToCartBtn.style.display = 'none';
		  this.serialNoTextField.value = '';
	  }
});


/*Norse Card Panel*/

var NorseCardPanel1 =  Class.create(PopUpBase, {
	  initialize: function() {
		    
		this.createPopUp($('norse-popup-panel'));	    
	    /*buttons*/
	    this.okBtn = $('norse-popup-process-button');
	    this.cancelBtn = $('norse-popup-cancel-button');
	    
	    /*textfields*/ 	
	    this.cardnumberTextField = $('norse-popup-cardnumber-textfield');
	    this.cardholderTextField = $('norse-popup-cardholder-textfield');
	    this.cvvTextField = $('norse-popup-cvv-textfield');
	    
	    /*select*/
	    this.expmonthSelect = $('norse-popup-month-select');
	    this.expyearSelect = $('norse-popup-year-select');
	    
	    /*labels*/
	    this.titleLabel = $('norse-popup-title');
	    
	    /*radios*/
	    this.swipeRadio = $('norse-popup-swipe-radio');
	    this.manualRadio = $('norse-popup-manual-radio');	    
	    
	    /*add behaviour to components*/
	    this.swipeRadio.onclick = this.radioHandler.bind(this);
	    this.manualRadio.onclick = this.radioHandler.bind(this);
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.cartTotalAmount = OrderScreen.getCartTotal();
	    this.currencySymbol = OrderScreen.getCurrencySymbol();
	    
	    this.titleLabel.innerHTML = (OrderScreen.isSoTrx ? 'Sales ' : 'Refund ') + this.currencySymbol + new Number(this.cartTotalAmount).toFixed(2);
	    
	    this.cardnumberTextField.panel = this;
	    this.cardnumberTextField.onkeypress = function(e){	    	
	    	if(e.keyCode == Event.KEY_RETURN)
			{
				if(this.value == null || this.value == ''){
					this.panel.onError('swipe.card');
					return;
				}
				
				/* parse data */
				  var swipe = this.value;
				  this.value = "";
				  
				  if(swipe.length > 16){
					  var swipeData = new SwipeParserObj(swipe);					  
					  
					  if(!swipeData.hasTrack1){
						  alert(Translation.translate('Difficulty Reading Card Information! Please swipe.card Again!','Difficulty Reading Card Information! Please Swipe Card Again!'));
						  this.panel.cardnumberTextField.focus();
						  return;
					  }
					  
					  this.panel.cardnumberTextField.value = swipeData.account;
					  this.panel.cardholderTextField.value = swipeData.account_name
						  
					  Form.Element.setValue(this.panel.expmonthSelect, swipeData.exp_month);
					  Form.Element.setValue(this.panel.expyearSelect, swipeData.exp_year.substr(2)); 				  
					  			  
				  }
				  
				  this.panel.okHandler();
			}	    	
	    }
	    
	  }, 
	  
	  radioHandler:function(e){
		  var element = Event.element(e);
		  
		  this.cardnumberTextField.value = '';
		  this.cardholderTextField.value = '';
		  this.cvvTextField.value = '';
		  
		  this.expmonthSelect.options[0].selected = true;
		  this.expyearSelect.options[0].selected = true;
		  
		  switch (element) {
		  
			case this.swipeRadio:
				this.cardholderTextField.disabled = true;
				this.expmonthSelect.disabled = true;
				this.expyearSelect.disabled = true;
				this.cvvTextField.disabled = true;
				
				this.cardnumberTextField.type = 'password';
				
				break;
				
			case this.manualRadio:	
				this.cardholderTextField.disabled = false;
				this.expmonthSelect.disabled = false;
				this.expyearSelect.disabled = false;
				this.cvvTextField.disabled = false;
				
				this.cardnumberTextField.type = 'text';
				
				break;
	
			default:
				break;
		  }
		  
		  this.cardnumberTextField.focus();
	  },
	  
	  okHandler:function(){
		  if(!this.validate()) return;	  
		  this.hide();
		  this.paymentHandler(this.payment);
	  },
	  
	  resetHandler:function(){
		  this.cardnumberTextField.value = '';
		  this.cardholderTextField.value = '';
		  this.cvvTextField.value = '';
		  
		  this.expmonthSelect.options[0].selected = true;
		  this.expyearSelect.options[0].selected = true;
		  
		  this.swipeRadio.checked = true;
		  
		  this.cardholderTextField.disabled = true;
		  this.expmonthSelect.disabled = true;
		  this.expyearSelect.disabled = true;
		  this.cvvTextField.disabled = true;
	  },
	  
	  onShow:function(){
		this.resetHandler();
	  	this.cardnumberTextField.focus();
	  },
	  
	  validate:function(){
		  
		  if(this.cardnumberTextField.value == null || this.cardnumberTextField.value == ''){
			  
			  if(this.swipeRadio.checked){
				  alert(Translation.translate('card.not.swiped','Card not swiped!'));
			  }
			  else
			  {
				  alert(Translation.translate('enter.card.number','Enter Card Number!'));
			  }
			 
			  this.cardnumberTextField.focus();
			  return false;
		  }	
		  
		  if(this.cardholderTextField.value  == null || this.cardholderTextField.value == ''){			  
			  alert(Translation.translate('Enter name on Card!','Enter Name on Card!'));
			  this.cardholderTextField.focus();
			  return false;
		  }
		  
		  /* validate expiry */
		  var currentYear = new Date().getFullYear();
		  var currentMonth = new Date().getMonth() + 1;
		  
		  var month = this.expmonthSelect.value;
		  month = parseInt(month);
		  
		  var year = this.expyearSelect.value; 
		  
		  if(year === currentYear && month < currentMonth){
			  alert(Translation.translate('CreditCard has expired!','CreditCard has expired!'));
			  this.expmonthSelect.focus();
			  return false;
		  }		  
		  
		  this.setPayment();
		  
		return true;
	  },
	  
	  setPayment:function(){
	  	var payment = new Hash();
	  	payment.set('isCardPresent', this.swipeRadio.checked);
	  	payment.set('cardAmt', this.cartTotalAmount);
	  	payment.set('cardNumber',this.cardnumberTextField.value);
	  	
	  	var expiryDate = this.expmonthSelect.value + this.expyearSelect.value; 
  		payment.set('cardExpiryDate',  expiryDate);
  		
  		payment.set('cardCvv',this.cvvTextField.value);
  		payment.set('cardHolderName',this.cardholderTextField.value);
  		
  		payment.set('cardType', 'M');
	  	
	  	this.payment = payment;
	  },
	  
	  paymentHandler:function(payment){}
	});


/*NorseCardPanel Panel*/
var NorseCardPanel =  Class.create(CheckoutPanel, {
  initialize: function($super) {
	$super('norse.popup.panel');
    
    /*buttons*/
    this.cancelBtn = $('norse.popup.cancelButton');
    
    this.cancelBtn.onclick = this.hide.bind(this);
    
    /*fields*/
    this.cartTotalAmount = OrderScreen.getCartTotal();
    this.orderType = OrderScreen.orderType;
    
    /*others*/
    this.frame = $('norse.popup.frame');
    
  },  
  okHandler:function(){
	  /*ok handler*/
	  alert(Translation.translate('ok','OK'));
  },
  resetHandler:function(){
	  this.frame.src = "";
  },
  onShow:function(){
	  this.frame.src = ("OrderAction.do?action=getNorsePaymentForm&orderType=" + OrderScreen.orderType);
  },
  
  setPayment:function(){
  	/*set payment*/
  },
  
  paymentHandler:function(payment){}
});

var LineItemPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('itemLinePanel'));
	    
	    this.applyBtn = $('line-item-panel-apply-button');
	    this.cancelBtn = $('line-item-panel-cancel-button');
	    
	    this.quantityTextField = $('quantity-texfield');
	    
	    this.decreaseButton = $('decrease-button');
	    this.increaseButton = $('add-button');
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.applyBtn.onclick = this.applyHandler.bind(this); 
	    
	    this.decreaseButton.onclick = this.decreaseQty.bind(this);
	    this.increaseButton.onclick = this.increaseQty.bind(this);
	    
	    var keypadButtons = $$('#itemLinePanel .keypad-button');
        for(var i=0; i<keypadButtons.length; i++){
            var btn = keypadButtons[i];
            btn.onclick = this.keypadHandler.bind(this);
        }
        
        this.quantityTextField.onkeyup = function(e)
	    {
 			if (isNaN(this.value))
			{
 				this.value = '';
				return;
			}
        }
	  },
	    
	    
	    
	  applyHandler:function(){
		  
		  this.validate();
		  
		  var quantity = parseFloat(this.quantityTextField.value);
		  ShoppingCartManager.updateQty(quantity);
		  this.hide();
	  },
	  
	  onShow:function(){
		  this.quantityTextField.focus();
		  this.quantityTextField.select();
	  },
	  
	  validate:function(){	
		  var quantity = parseFloat(this.quantityTextField.value);
		  if(isNaN(quantity) || (quantity < 0.0)){
			  alert(Translation.translate('invalid.amt','Invalid amount'));
			  return false;
		  }
	  },
	  
	  keypadHandler:function(e){
          var button = Event.element(e); 
          var element = button.getElementsByClassName('keypad-value')[0];
          
          /*Fix for google chrome*/
          if (element == null)
          {
          		element = button;  
          }
          /**********************/
	          
          var value = element.innerHTML;
         // var value = element.innerHTML;
                       
          var quantity = this.quantityTextField.value;
          
          	if (this.quantityTextField.selectionStart != this.quantityTextField.selectionEnd)
			{	
          		quantity = '';
				this.quantityTextField.value = '';
				this.quantityTextField.focus();
			}
          
          
			if('Del' == element.innerHTML.strip()){
				
				if(quantity.length > 0)
	        		  quantity = quantity.substring(0,quantity.length-1);
          }
			else if('-' == element.innerHTML.strip()){
				
				if(quantity.indexOf('-') == -1){
					quantity = '-' + quantity;
				}
				else{
					quantity = quantity.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(quantity.indexOf('.') == -1)
				{
					quantity = quantity + value;
				}
			}
			else{
				quantity = quantity + value;
			}
			
			this.quantityTextField.value = quantity;
			
           
            if(isNaN(quantity) || (quantity < 0.0)){           
                return;
            }
           
            if(quantity != 0.0 && !isNaN(quantity)){
            	quantity = parseFloat(quantity);
            }
      	},
      	
      	increaseQty : function()
      	{
      		var quantity = this.quantityTextField.value;
				
			if (!isNaN(quantity))
			{
				quantity = parseFloat(quantity) + 1;
				this.quantityTextField.value = quantity;
			}
			
			if(isNaN(quantity) || (quantity < 0.0)){           
                return;
            }
           
            if(quantity != 0.0 && !isNaN(quantity)){
            	quantity = parseFloat(quantity);
            }
      	},
      	
      	decreaseQty : function()
      	{
      		var quantity = this.quantityTextField.value;
				
			if (!isNaN(quantity))
			{
				quantity = parseFloat(quantity) - 1;
				this.quantityTextField.value = quantity;
			}
			
			if(isNaN(quantity) || (quantity < 0.0)){  
				this.quantityTextField.value = 0;
                return;
            }
           
            if(quantity != 0.0 && !isNaN(quantity)){
            	quantity = parseFloat(quantity);
            }
      	}
	});

var ChangeTaxPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('change-tax-pop-up'));
		this.taxDropdown = $('tax-dropdown');
		this.shoppingCart = null;
		
		this.okBtn = $('change-tax-pop-up-ok-button');
		this.cancelBtn = $('change-tax-pop-up-cancel-button');
		
        /*this.taxDropdown.onselect = this.onChangeHandler.bind(this);*/
        this.okBtn.onclick = this.okHandler.bind(this);
        this.taxDropdown.onkeyup = this.closePanel.bind(this);
        this.cancelBtn.onclick = this.hide.bind(this);
        this.taxDropdown.focus();
        
	  },
	  
	  onChangeHandler : function()
	  {
		  /*var taxId = this.taxDropdown.value;
		  ShoppingCartManager.setLineTax(taxId);
		  this.taxDropdown.focus();*/
	  },
	  
	  okHandler : function()
	  {
		  this.setTax();
		  this.hide();
	  },
	  
	  setTax : function()
	  {
		  var cart = ShoppingCartManager.getCart();
		  var lines = cart.lines;
		  
		  if(lines.length > 0)
		  {
		        var currentLine  = lines[cart.selectedIndex];
		        var taxId = currentLine.taxId;
		        var newTaxId = this.taxDropdown.value;
		        ShoppingCartManager.setLineTax(newTaxId);
	      }		 
	  },
	  
	  setShoppingCart : function(shoppingCart)
	  {
		  this.shoppingCart = shoppingCart;
		  this.taxDropdown.focus();
	  },
	  
	  closePanel : function(e)
	  {
		  if (e.keyCode == Event.KEY_RETURN)
		 {
			  this.hide();
		 }
	  },
	  
	  onShow : function(){
		  var cart = ShoppingCartManager.getCart();
		  var lines = cart.lines;
		  if(lines.length > 0)
		  {
		        var currentLine  = lines[cart.selectedIndex];
		        var taxId = currentLine.taxId;
		        jQuery('#tax-dropdown').val(taxId);     
	      }
	  }
	  
});

/* modifier popup */
var SelectModifierPanel = Class.create(PopUpBase, {
    initialize: function() {
    this.createPopUp($('select-modifier-panel'));
   
    this.product = null;
   
    /*buttons*/
    this.okBtn = $('select-modifier-ok-button');
    this.cancelBtn = $('select-modifier-cancel-button');
   
    this.cancelBtn.onclick = this.hide.bind(this);
    this.okBtn.onclick = this.okHandler.bind(this);
    },

    resetHandler:function(){
         
    },
   
    renderSelectModifierList : function(){
       
        var display = $('select-modifier-panel-modifiers');
        display.innerHTML = "";
       
        var groupsAvailable = this.groupsAvailable;
       
        for(var i=0; i<groupsAvailable.length; i++){
            var groupId = groupsAvailable[i].groupId;               
            var btns = jQuery("div#" + groupId + " > button");
           
            for(var j=0; j<btns.length; j++){
                var btn = btns[j];
               
                if(btn.selected){
                    display.innerHTML += "<div>" + jQuery(btn).attr('name') + "</div>";
                }
            }
        }
    },
   
    loadModifiers : function(product){
        var product_id = product.m_product_id;
        this.product_id = product_id;
       
        /*
        var MODIFIER_GROUPS = window.parent.frames[1].MODIFIER_GROUPS;
        var MODIFIER_MAPPINGS = window.parent.frames[1].MODIFIER_MAPPINGS;
        */
        
        var mg = sessionStorage.getItem('MODIFIER_GROUPS');
		var mm = sessionStorage.getItem('MODIFIER_MAPPINGS');
		
		var MODIFIER_GROUPS = JSON.parse(mg);
		var MODIFIER_MAPPINGS = JSON.parse(mm);
       
        var groups = {};
        for(var j=0; j<MODIFIER_GROUPS.length; j++){
            var group = MODIFIER_GROUPS[j];
            groups[group.groupId] = group;
        }
       
        this.groups = groups;
        var groupsAvailable = new Array();
       
        $('select-modifier-panel-product').innerHTML = product.name;
       
        var modifierGroupsContainer  = $('select-modifier-panel-modifier-groups-container');
        modifierGroupsContainer.innerHTML = "";
       
        var html = "";
       
        for(var i=0; i< MODIFIER_MAPPINGS.length; i++){
            var mapping = MODIFIER_MAPPINGS[i];
           
            if(mapping.product_id == product_id){
                var group_id = mapping.group_id;
                var group = groups[group_id];
                
                if(group == null) continue;
               
                groupsAvailable.push(group);
               
                html += "<div class='modifier-group'>";
                /* html += "<div class='modifier-group-header'>" + group.groupName + "</div>"  */
                html += "<div class='modifier-group-header'> Choose below: </div>"                 
               
                html += "<div class='modifier-buttons' id='" + group_id + "'>";
               
                var groupLines = group.lines;
                for(var j=0; j<groupLines.length; j++){
                    var groupLine = groupLines[j];
                                   
                    html += "<button class='modifier-button btn btn-lg btn-primary' group_id='" + group_id + "' groupline_id='" + groupLine.groupLineId + "' name='" + groupLine.name + "' product_id='" + groupLine.productId + "' price='" + groupLine.price + "'>" + groupLine.name + "</button>";
                }
               
                html += "</div>";
                html += "</div>";
            }
        }
       
        this.groupsAvailable = groupsAvailable;
       
        modifierGroupsContainer.innerHTML = html;
       
        var btns = $$('button.modifier-button');
       
        for(var i=0; i<btns.length; i++){
            var btn = btns[i];
            btn.panel = this;
           
            btn.onclick = function(){
                var modifier = jQuery(this);
                var groupId = modifier.attr('group_id');
                var groupLineId = modifier.attr('groupline_id');
                var name = modifier.attr('name');
                var productId = modifier.attr('product_id');
                var price = modifier.attr('price');
               
                var groups = this.panel.groups;
                var group = groups[groupId];
               
                if(group.isExclusive == 'Y'){
                    /* select only one */
                    /* reset all buttons */
                    var btns = jQuery("div#" + groupId + " > button");
                    for(var i=0; i<btns.length; i++){
                        jQuery(btns[i]).removeClass('selected');
                        btns[i].selected = false;
                    }
                }
               
                if(this.selected){
                    this.selected = false;
                    modifier.removeClass('selected');
                }
                else{
                    this.selected = true;
                    modifier.addClass('selected');
                }
               
                this.panel.renderSelectModifierList();                   
            };
        }
    },
   
    onShow:function(){           
        var display = $('select-modifier-panel-modifiers');
        display.innerHTML = "";
    },
   
    okHandler:function(){
        /*
        var price = this.priceTextfield.value;
        var description = this.descriptionTextfield.value;       
        shoppingCart.addToCart(this.product.m_product_id,1, description, price);       
        */
       
        var groupsAvailable = this.groupsAvailable;
        var modifiers = new Array();
       
        for(var i=0; i<groupsAvailable.length; i++){
           
            var groupId = groupsAvailable[i].groupId;
            var isMandatory = groupsAvailable[i].isMandatory;
           
            var btns = jQuery("div#" + groupId + " > button");
           
            var isOk = false;
           
            for(var j=0; j<btns.length; j++){
                var btn = btns[j];
               
                if(btn.selected){
                    isOk = true;
                   
                    var modifier = jQuery(btn);
                    var groupId = modifier.attr('group_id');
                    var groupLineId = modifier.attr('groupline_id');
                    var name = modifier.attr('name');
                    var productId = modifier.attr('product_id');
                    var price = modifier.attr('price');
                   
                    var obj = {};
                    obj.modifierGroupId = groupId;
                    obj.modifierId = groupLineId;
                    obj.name = name;
                    obj.productId = productId;
                    obj.price = price;
                   
                    modifiers.push(obj);
                }
            }
           
            if(isMandatory == 'Y' && !isOk){
                alert(groupsAvailable[i].groupName + ' option is required!');
                return;
            }
        }
       
        shoppingCart.addToCart(this.product_id,1, null, null, modifiers);
                   
        this.hide();
    }      
   
});

var CreateCustomerPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('create-customer-popup-panel'));
	 	
	 	var ref = this;
	 	
	 	var form = jQuery("#create-customer-popup-panel").find("#form")[0];
	 	form.reset();
	 	
	 	jQuery("#create-customer-popup-panel").find(".address").hide();
	 	jQuery("#create-customer-popup-panel").find("#add-address-btn").off('click').on("click", function(){
	 		
	 		var divs = jQuery("#create-customer-popup-panel").find(".address");
	 		
	 		if(divs[0].style.display == 'block') return;
	 		
	 		divs.show();
	 		
	 		//build country list
	 		var countryList = jQuery("#create-customer-popup-panel").find("#country")[0];
	 		countryList.onchange = function(){
	 			
	 			var option = this.options[this.selectedIndex];
	 			
	 			var regions = jQuery(option).attr('regions');
	 			regions = JSON.parse(regions);
	 			
	 			var regionList = jQuery("#create-customer-popup-panel").find("#state")[0];
	 			regionList.options.length = 0;
	 			
	 			var html = "<option value='0'></option>";
	 			var region;
	 			
	 			for(var i=0; i<regions.length; i++){
	 				
	 				region = regions[i];
	 				
	 				if(region['c_region_id'] == 0) continue;
	 				
	 				html += "<option value='" + region['c_region_id'] + "'>" + region['c_region_name'] + "</option>";
	 				
	 			}
	 			
	 			regionList.innerHTML = html;
	 			
	 		};
	 		
	 		var select = jQuery(countryList);
	 		
	 		if(countryList.options.length == 0){
	 			
	 			console.log("Customer Popup: Building country dropdown ..");	 			
	 			
	 			jQuery.getJSON( "DataTableAction.do?action=getCountries", function( data ) {
	 				
	 				var country;
	 				var option;
	 				
	 				for( var i=0; i<data.length; i++) {
	 					
	 					country = data[i];
	 					option = jQuery("<option value='" + country['c_country_id'] +"'>" + country['c_country_name'] + "</option>");
	 					
	 					option.attr('regions', Object.toJSON(country['regions']));
	 					
	 					select.append(option);
	 				}
	 				
	 				select.val(TerminalManager.terminal.orgInfo.countryId);
	 				
	 			});
	 			
	 			
	 			
	 		}
	 		
	 		select.val(TerminalManager.terminal.orgInfo.countryId);
	 		
	 	});
	 	
	 	
	 	jQuery("#create-customer-popup-panel").find("#close-button").off('click').on("click", function(){
	 		
	 		ref.hide();
	 		
	 	});
	 	
	 	jQuery("#create-customer-popup-panel").find("#save-button").off('click').on("click", function(){
	 		
	 		var form = jQuery("#create-customer-popup-panel").find("#form")[0];
	 		
	 		var name = form.name.value;
	 		var identifier = form.identifier.value;
	 		var email = form.email.value;
	 		var emailReceipt = form.emailReceipt.checked;
	 		
	 		var phone = form.phone.value;
	 		var address = form.address.value;
	 		var city = form.city.value;
	 		var postalcode = form.postalcode.value;
	 		var country = 0;
	 		var state = 0;
	 		
	 		if( form.country.options.length > 0 )
	 		{
	 			country = form.country.options[form.country.selectedIndex].value;
	 		}
	 		
	 		if( form.state.options.length > 0 )
	 		{
	 			state = form.state.options[form.state.selectedIndex].value;
	 		}
	 		
	 		if(identifier == ""){
	 			
	 			alert("Customer ID is required!");
	 			form.identifier.focus();
	 			
	 			return;
	 		}
	 		
	 		if(name == ""){
	 			
	 			alert("Name is required!");
	 			form.name.focus();
	 			
	 			return;
	 		}
	 		
	 		if(name == "" && identifier == "")
	 		{
	 			alert("Name / Customer ID is required!");
	 			form.name.focus();
	 		}
	 		else
	 		{
	 			if(name == "")
	 			{
	 				//use identifier as name
	 				name = identifier;
	 			}
	 			
	 			var model = {
	 			"value" : '',
				"title" : 'Mr',
				"name" : '',
				"email" : '',
				"phoneNo" : '',
				"address" : '',
				"city" : '',
				"postal" : '',
				"mobileNo" : '',
				"countryId" : '',
				"regionId" : 0,
				"gender" : 'Male',
				"dob" : '',
				"custom1" : '',
				"custom2" : '',
				"custom3" : '',
				"custom4" : '',
				"notes" : '',
				"emailReceipt" : false
	 			};
				
	 			
	 			model.name = name || '';
	 			model.identifier = identifier || '';
	 			model.email = email || '';
	 			model.phoneNo = phone || '';
	 			model.countryId = country || TerminalManager.terminal.orgInfo.countryId;
	 			model.regionId = state || 0;
	 			model.value = model.identifier;
	 			
	 			model.address = address || '';
	 			model.city = city || '';
	 			model.postal = postalcode || '';
	 			model.emailReceipt = emailReceipt || false;
	 			
	 			var postData = {'json' : Object.toJSON(model)};
	 			var url = "BPartnerAction.do?action=createCustomer";
	 			
	 			jQuery.post(url,
	 					postData,
	 		    		function(json, textStatus, jqXHR){
	 		    			
	 		    			if(json == null || jqXHR.status != 200){
	 		    				alert('Failed to save: session timed out!');
	 		    				return;
	 		    			}
	 		    			
	 		    			if(json.error){
	 		    				alert(json.error);
	 		    				return;
	 		    			}
	 		    			else
	 		    			{
	 		    				var id = json['c_bpartner_id'];	
	 		    				console.log( json );
	 		    				
	 		    				var bp = new BP(json);
	 		    				BPManager.setBP(bp);
	 		    				
	 		    				jQuery('#search-bp-textfield').val(bp.getName());
	 		    				OrderScreen.setBPartnerId(bp.getId());
	 		    				
	 		    				ref.hide();
	 		    			}
	 		    			
	 		    		},
	 					"json").fail(function(json, textStatus, jqXHR){
	 						
	 						if (json.status == 401)
	 						{
	 							jQuery.globalEval(json.responseText);
	 						}
	 						else
	 						{
	 							alert("Failed to process request");
	 						}
	 						
	 					}).done(function() {
	 							 						
	 		    			
	 					}).always(function() {
	 						 						
	 						
	 					});
	 			
	 			
	 		}	 		
	 		
	 	});
	},
	
	onShow : function(){
		
		var form = jQuery("#create-customer-popup-panel").find("#form")[0];
		form.name.focus();
	}
});

var MoreOptionsPanels = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('more-option-popup-panel'));
	 	this.cancelBtn = $('more-option-popup-cancelButton');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);	 	 	
	 	
	},
	
	onShow : function()
	{
		
	},
	
	cancelHandler: function()
	{
		this.hide();
	}
});

var RedeemPromotionPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('redeem.promotion.popup.panel'));
	 	this.cancelBtn = $('redeem.promotion.popup.cancelButton');
	 	this.closeBtn = $('redeem.promotion.popup.closeButton');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);
	 	this.closeBtn.onclick = this.cancelHandler.bind(this);
	},
	
	onShow : function()
	{
		 var currencySymbol = OrderScreen.getCurrencySymbol();
		 var cart = ShoppingCartManager.getCart();
		    
		 var points = BPManager.bp.loyaltyPoints - cart.promotionPointsTotal;
		 var customer = BPManager.bp.name;
		 		    
		jQuery("#redeem-promotion-popup span.customer").html(customer);
		jQuery("#redeem-promotion-popup span.points").html(points);
		
		var html = "";
		var promotion = null;
		
		var popup = this;
		
		jQuery.getJSON("DataTableAction.do?action=getPromotions&date=" + DateUtil.getCurrentDate(), function(json){
			
			var promotions = json['promotions'];
			var disabled = false;
			
			var today = moment().hour(0).minute(0).second(0);			
			
       	  	var expiry = null;
       	  	var isExpired = false;
       	  	
			for( var i=0; i<promotions.length; i++ )
			{				
				promotion = promotions[i];
				
				expiry = promotion['expirydate'];
				expiry = moment(expiry, "DD-MM-YYYY");
				
				isExpired = today.unix() > expiry.unix();
				
				if(isExpired) continue; // do not show
				
				disabled = promotion['points'] > points || isExpired;
				
				//console.log("promotion['points'] > points " + promotion['points'] + " > " + points + " ==> " + (promotion['points'] > points));
				//console.log("today.unix() > expiry.unix() " + today.unix() + " > " + expiry.unix() + " ==>" + (today.unix() > expiry.unix()));
				//console.log("disabled " + disabled);
				
				html += "<tr>" 
					+ "<td>" + promotion['description'] + "</td>"
					+ "<td>" + promotion['amount'] + "</td>"
					+ "<td>" + promotion['points'] + " points</td>"
					+ "<td>" + expiry.format("DD-MMM-YYYY") + "</td>"
					+ "<td><button style='width:100%;' class='btn " + ((disabled) ? "" : "btn-success") + "' " + ((disabled) ? "disabled='disabled'" : "") + " id='" + promotion['u_promotion_id'] + "'>Redeem</button></td>"
				+ "</tr>";
			}
			
			jQuery("#redeem-promotion-popup tbody.promotions").html(html);
			
			var buttons = jQuery("#redeem-promotion-popup tbody.promotions button");
			var button = null;
			
			for( var j=0; j<buttons.length; j++)
			{
				button = jQuery(buttons[j]);
				button.on('click', function(){
					
					var promotion_id = jQuery(this).attr('id');
					
					jQuery.post("PromotionAction.do",
						  {
						    action : "redeemPromotion",
						    "promotion_id" : promotion_id,
						    "date": DateUtil.getCurrentDate()
						  },
						  function(data,status,xhr){
							  
							popup.hide();
							 
						    if(status != "success"){
						    	alert(Translation.translate("failed.to.redeem.promotion","Failed to redeem points!"));
						    }
						    else
						    {
						    	jQuery('#shopping-cart-container').html(data);	    	
						    }					     
						    
						  });					
					
				});
				
			}
		});
		
		
		
		
	},
	
	cancelHandler: function()
	{
		this.hide();
	}	
	
});

var BatchAndExpiryPanel = Class.create(PopUpBase, {
	
	initialize : function()
	{
	 	this.createPopUp($('batch.expiry.popup.panel'));
	 	this.cancelBtn = $('batch.expiry.popup.cancelButton');
	 	this.closeBtn = $('batch.expiry.popup.closeButton');
	 	
	 	this.cancelBtn.onclick = this.cancelHandler.bind(this);
	 	this.closeBtn.onclick = this.cancelHandler.bind(this);
	},
	
	onShow : function()
	{	
		var html = "";
		var promotion = null;
		
		var popup = this;
		var product = this.product;
		
		var m_product_id = product["m_product_id"];
		var m_warehouse_id = TerminalManager.terminal.warehouseId;
		
		jQuery.getJSON(`ProductAction.do?action=getLotAndExpiry&m_product_id=${m_product_id}&m_warehouse_id=${m_warehouse_id}`, function(json){
			
			var lots = json['data'];
			var disabled = false;
			
			var today = moment().hour(0).minute(0).second(0);			
			
       	  	var expiry = null;
       	  	var isExpired = false;
       	  	
       	  	var lot;
			for( var i=0; i<lots.length; i++ )
			{				
				lot = lots[i];
				
				expiry = lot['expirydate'];
				expiry = moment(expiry, "YYYY-MM-DD HH:mm:ss");
				
				isExpired = today.unix() > expiry.unix();
				
				if(isExpired) continue; // do not show
				
				//console.log("promotion['points'] > points " + promotion['points'] + " > " + points + " ==> " + (promotion['points'] > points));
				//console.log("today.unix() > expiry.unix() " + today.unix() + " > " + expiry.unix() + " ==>" + (today.unix() > expiry.unix()));
				//console.log("disabled " + disabled);
				
				html += "<tr>" 
					+ "<td>" + lot['lot'] + "</td>"
					+ "<td>" + expiry.format("DD-MMM-YYYY") + "</td>"
					+ "<td>" + lot['qty'] + "</td>"
					+ "<td><button style='width:100%;' class='btn btn-success' id='" + lot['m_attributesetinstance_id'] + "'>Add</button></td>"
				+ "</tr>";
			}
			
			jQuery("#batch-expiry-popup tbody.promotions").html(html);
			
			var buttons = jQuery("#batch-expiry-popup tbody.promotions button");
			var button = null;
			
			for( var j=0; j<buttons.length; j++)
			{
				button = jQuery(buttons[j]);
				button.on('click', function(){
					
					var m_attributesetinstance_id = jQuery(this).attr('id');
					
					popup.hide();
					
					//add to shoppingcart
					console.log(`add product ${m_product_id} m_attributesetinstance_id ${m_attributesetinstance_id}`);
					
					shoppingCart.addToCart(m_product_id, 1, null, null, null, null, true, m_attributesetinstance_id);
					
				});
				
			}
		});
		
		
		
		
	},
	
	cancelHandler: function()
	{
		this.hide();
	}	
	
});



var OfflineMarketingPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('offline-marketing-pop-up'));
		
		this.tryNowBtn = $('offline-marketing-pop-up-try-now-button');
		this.cancelBtn = $('offline-marketing-pop-up-cancel-button');

	    this.doNotShowCheckbox = $('offline-marketing-pop-up-check-box');
	    
		this.tryNowBtn.onclick = this.tryNowHandler.bind(this);
		this.cancelBtn.onclick = this.hide.bind(this);
      
	  },
	  
	  tryNowHandler : function()
	  {
		  //save oprion donotshow into cache if checked
		  if (this.doNotShowCheckbox.checked) {
			
			  localStorage.setItem('POSTERITA_MARKETING', true);
		  }
		  else
		  {
			  sessionStorage.setItem('POSTERITA_MARKETING', true);
		  }

		  //redirect the user to the page to try the posterita offline
		  getURL('offline-instructions.do');
		  
		  this.hide();
	  },
	  
	  closePanel : function(e)
	  {
		  if (e.keyCode == Event.KEY_RETURN)
		 {
			//save oprion donotshow into cache if checked
			  if (this.doNotShowCheckbox.checked) {
					
				  localStorage.setItem('POSTERITA_MARKETING', true);
			  }
			  else
			  {
				  sessionStorage.setItem('POSTERITA_MARKETING', true);
			  }
			  
			  this.hide();
		 }
	  },
	  
	  onShow : function(){
		  this.tryNowBtn.focus();
	  },
	  
	  onHide : function(){

		if (this.doNotShowCheckbox.checked) {
					
		  localStorage.setItem('POSTERITA_MARKETING', true);
	    }
		else
		{
			sessionStorage.setItem('POSTERITA_MARKETING', true);
		}

	  }
	  
});

/* Product Info Popup Panel */
var ProductInfoPopup =  Class.create(PopUpBase, {
	
	  appendHTML : function() {
		  
		  var html = ' <div class="modal" style="display: block; visibility: visible; left: 0px; top: 10px; z-index: 100000;" id="product-info-popup-panel" tabindex="-1" role="dialog" aria-labelledby="product-info-popup-panel" aria-hidden="true"> ' +
			' 	<div class="modal-dialog modal-md"> ' +
			' 		<div class="modal-content"> ' +
			' 			<div class="modal-header"> ' +
			' 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="product-info-popup-cancelButton">×</button> ' +
			' 				<h4 class="modal-title"> ' +
			' 					<div class="glyphicons glyphicons-pro search"></div> ' +
			' 					&nbsp;' + Translation.translate('product.info') +
			' 				</h4> ' +
			' 			</div> ' +
			' 			<div class="modal-body"> ' +
			' 				<div class="row"> ' +
			' 					<div class="input-group float-left input-sm"> ' +
			//' 						<input type="text" class="form-control textField input-sm" placeholder="Enter barcode or name" id="product-info-popup-textfield"> ' +
			'<div class="input-group"><input autocomplete="off" type="text" class="form-control" id="product-info-popup-textfield" placeholder="Enter barcode or name"><span class="input-group-btn"><button class="btn btn-success" type="button" id="product-info-popup-okButton">Search</button></span></div>' +
			
			
			' 					</div> ' +
			' 				</div> ' +
			'				<div class="row" id="product-info-popup-panel-result"></div>' +
			' 			</div> ' +
			' 			<div class="modal-footer"> ' +
			' 				' +
			' 			</div> ' +
			' 		</div> ' +
			' 	</div> ' +
			' </div> ';
		  
		jQuery(document.body).append(html);
	  },
	
	  initialize: function() {
		
		if(jQuery('#product-info-popup-panel').length == 0) {
			
			this.appendHTML();
		}
		
		
	    this.createPopUp($('product-info-popup-panel'));
	    
	    /*buttons*/
	    this.okBtn = $('product-info-popup-okButton');
	    this.cancelBtn = $('product-info-popup-cancelButton');
	    
	    /*textfields*/
	    this.searchTermtextfield = $('product-info-popup-textfield');		    
	    	   
	    /*events*/
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.okBtn.onclick = this.okHandler.bind(this); 
	    
	    this.searchTermtextfield.panel = this;
	    this.searchTermtextfield.onkeypress = function(e){	    	
			if(e.keyCode == Event.KEY_RETURN){										
				this.panel.validate();			
			}
		};		
	    
	  }, 
	  validate:function(){
	  	if(this.searchTermtextfield.value == null || this.searchTermtextfield.value == ''){
			this.onError('please.enter.search.criteria');
			this.searchTermtextfield.focus();
			return;
		}
		
	  	var url = 'ProductAction.do?action=search&searchTerm=' + this.searchTermtextfield.value + '&isSoTrx=' + OrderScreen.isSoTrx + '&bpPricelistVersionId=&warehouseId=' + SearchProduct.warehouseId + '&ignoreTerminalPricelist=false&filterQuery=' ;
	  	new Ajax.Request( url,{ 
			onSuccess: this.onSuccess.bind(this), 
			onFailure: this.onFailure.bind(this)
		});	
	  },  
	  okHandler:function(){	  
		  this.validate();
	  },
	  resetHandler:function(){
	  	this.searchTermtextfield.value = '';
	  },
	  onShow:function(){
	  	this.searchTermtextfield.focus();
	  },
	  /* ajax callbacks */
	  onComplete:function(request){		  
	  },
	  onSuccess:function(request){	  	
  
  			var responseText = request.responseText.evalJSON(true);
			var results = responseText.products;
			
			//filter modifiers
			var products = [];
			
			for(var i=0; i<results.length; i++){
				var record = results[i];
				if(record.ismodifier == 'Y') continue;
				
				products.push(record);
			}
			
			
			if (products.length == 0)
			{
				this.onError('no.product.found');
			}
			else 
			{
				var container = jQuery("#product-info-popup-panel-result");
				
				var html = "<table class='table table-striped table-scroll'><thead><tr><th>Name/Description</th><th style='text-align: right;'>Stock in Hand</th></tr></thead><tbody>";
				
				for(var i=0; i<products.length; i++)
				{
					var product = products[i];
					
					html = html + "<tr><td><a href='javascript:void(0);' onclick='ProductInfoPopup_ProductHandler(" + product.m_product_id + ")'>" + product.name + "<br>" + product.description + "</a></td><td style='text-align: right; width:20%;'>" + product.qtyonhand + "</td></tr>"
					
				}
				
				html = html + "</tbody></table>";
				
				container.html( html );
			
			}
			
			if(!responseText.success){
				/* this.hide(); */
				alert(result.error);
				return;
			}  		
	  },
	  onFailure:function(request){	
		  alert("failed.to.process.request");
	  }
});

var ProductInfoPopup_ProductHandler = function(product_id){
	PopUpManager.activePopUp.hide();
	var panel = new ProductInfoPanel();
	panel.onHide = function(){
		new ProductInfoPopup().show();
	};
	panel.getInfo(product_id);
};
var SalesRepCheck = Class.create({
	
		initialize : function(salesRep)
		{
			this.salesRep = salesRep;
			this.checkBox = document.createElement('input');
			this.checkBox.setAttribute('type', 'checkbox');
		},
		
		isChecked : function()
		{
			return this.checkBox.checked;
		},
		
		getSalesRep : function()
		{
			return this.salesRep;
		},
		
		getCheckBox : function()
		{
			return this.checkBox;
		}
});

var SalesRepSelectPanelManager = Class.create(PopUpBase, {
	
		initialize : function()
		{
			this.panel = $('split.order.select.salesrep.panel');
			this.salesRepList = new ArrayList();
			this.clickAddEventListeners = new ArrayList();
			this.clickRemoveEventListeners = new ArrayList();
			
			this.EVENT_ADD = 'ADD';
			this.EVENT_REMOVE = 'REMOVE';			
			
			this.addBtn = document.createElement('input');
			this.addBtn.setAttribute('type', 'button');
			this.addBtn.setAttribute('value', Translation.translate("add"));
			this.addBtn.setAttribute('id', 'addSalesRepsBtn');
			this.addBtn.onclick = this.addHandler.bind(this);
			
			this.removeBtn = document.createElement('input');
			this.removeBtn.setAttribute('type', 'button');
			this.removeBtn.setAttribute('value', Translation.translate("remove"));
			this.removeBtn.setAttribute('id', 'removeSalesRepsBtn');
			this.removeBtn.onclick = this.removeHandler.bind(this);
		},
		
		initSalesReps : function(salesReps)
		{
			for (var i=0; i<salesReps.size(); i++)
			{
				var salesRep = salesReps.get(i);
				var salesRepCheck = new SalesRepCheck(salesRep);
				this.salesRepList.add(salesRepCheck);
			}
			this.initPanel();
		},
		
		initPanel : function()
		{
			this.createPopUp($(this.panel.getAttribute('id')));
			var table = document.createElement('table');
			table.setAttribute('id', 'salesreps_table');
			for (var i=0; i < this.salesRepList.size(); i++)
			{
				var salesRepCheck = this.salesRepList.get(i);
				var checkBox = salesRepCheck.getCheckBox();
				var salesRep = salesRepCheck.getSalesRep();
				checkBox.setAttribute('id', salesRep.getId() + 'check');
				
				
				var tdCheck = document.createElement('td');
				tdCheck.appendChild(checkBox);
				
				var tdLabel = document.createElement('td');
				tdLabel.innerHTML = salesRep.getName();
				tdLabel.setAttribute('id', salesRep.getId());
				
				var tr = document.createElement('tr');	
				if (i%2 == 0)
				{
					tr.setAttribute('class', 'ev');
				}
				else
				{
					tr.setAttribute('class', 'od');
				}
				
				tdLabel.onclick = function(e)
				{
					var id = this.getAttribute('id');
					var chbox = $(id + 'check');
					chbox.checked = !(chbox.checked);
				};
				
				tr.appendChild(tdCheck);
				tr.appendChild(tdLabel);
				table.appendChild(tr);
			}
			var trBtn = document.createElement('tr');
			var tdBtn = document.createElement('td');
			tdBtn.setAttribute('colspan', 2);
			tdBtn.appendChild(this.removeBtn);
			tdBtn.appendChild(this.addBtn);
			trBtn.appendChild(tdBtn);
			table.appendChild(trBtn);
			this.panel.appendChild(table);
		},
		
		addHandler : function()
		{
			this.fireAddEvent();
		},
		
		removeHandler : function()
		{
			this.fireRemoveEvent();
		},
		
		addEventListener : function(type, listener)
		{
			if (type == null || listener == null)
				return;
			
			if (type == this.EVENT_ADD)
			{
				this.clickAddEventListeners.add(listener);
			}
			
			if (type == this.EVENT_REMOVE)
			{
				this.clickRemoveEventListeners.add(listener);
			}
		},
		
		fireAddEvent : function()
		{
			for (var i=0; i<this.clickAddEventListeners.size(); i++)
			{
				var selectedList = new ArrayList(); 
				for (var j=0; j<this.salesRepList.size(); j++)
				{
					var salesRepCheck = this.salesRepList.get(j);
					if (salesRepCheck.isChecked())
					{
						selectedList.add(salesRepCheck.getSalesRep());
					}
				}
				
				var listener = this.clickAddEventListeners.get(i);
				listener.addHandler(selectedList);
			}
		},
		
		fireRemoveEvent : function()
		{
			for (var i=0; i<this.clickRemoveEventListeners.size(); i++)
			{
				var selectedList = new ArrayList(); 
				for (var j=0; j<this.salesRepList.size(); j++)
				{
					var salesRepCheck = this.salesRepList.get(j);
					if (salesRepCheck.isChecked())
					{
						selectedList.add(salesRepCheck.getSalesRep());
					}
				}
				
				var listener = this.clickRemoveEventListeners.get(i);
				listener.removeHandler(selectedList);
			}
		}
});


var SalesRepSelectManager = Class.create({
		
		initialize : function()
		{
			this.salesReps = new ArrayList(new SalesRep());
			this.orderSalesRep = null;
			this.panelManager = new SalesRepSelectPanelManager();
			this.panelManager.addEventListener(this.panelManager.EVENT_ADD, this);
			this.panelManager.addEventListener(this.panelManager.EVENT_REMOVE, this);
			
			this.EVENT_ADD = this.panelManager.EVENT_ADD;
			this.EVENT_REMOVE = this.panelManager.EVENT_REMOVE;			
			
			this.clickAddEventListeners = new ArrayList();
			this.clickRemoveEventListeners = new ArrayList();
			
			this.salesRepsLoadedListeners = new ArrayList();
		},
		
		init : function(orderId)
		{
			var action = 'UserAction.do?action=loadSalesReps';
			var params = 'orderId='+orderId;
			
			var myAjax = new Ajax.Request(action, 
			{
				method : 'POST',
				parameters : params,
				onSuccess : this.loadSalesReps.bind(this),
				onFailure : this.reportFailure.bind(this)
			});
		},
		
		addEventListener : function(type, listener)
		{
			if (type == null || listener == null)
				return;
			
			if (type == this.EVENT_ADD)
			{
				this.clickAddEventListeners.add(listener);
			}
			
			if (type == this.EVENT_REMOVE)
			{
				this.clickRemoveEventListeners.add(listener);
			}
		},
		
		addHandler : function(salesReps)
		{
			for (var i=0; i<this.clickAddEventListeners.size(); i++)
			{
				var listener = this.clickAddEventListeners.get(i);
				listener.addHandler(salesReps);
			}
		},
		
		removeHandler : function(salesReps)
		{
			for (var i=0; i<this.clickRemoveEventListeners.size(); i++)
			{
				var listener = this.clickRemoveEventListeners.get(i);
				listener.removeHandler(salesReps);
			}
		},
		
		loadSalesReps : function(request)
		{
			var response  = request.responseText;
			var salesRepsJSON = eval('(' + response + ')');
			
			for (var i=0; i<salesRepsJSON.length; i++)
			{
				var _salesRep = salesRepsJSON[i];
				var salesRep = new SalesRep(_salesRep.name, _salesRep.id);
				salesRep.setCurrent(_salesRep.isCurrent);
				
				if (_salesRep.isCurrent)
				{
					this.orderSalesRep = salesRep;
				}
				else
				{
					this.salesReps.add(salesRep);
				}
			}
			
			this.panelManager.initSalesReps(this.salesReps);
			this.fireSalesRepsLoadedEvent();
		},
		
		reportFailure : function(request)
		{
			
		},
		
		addSalesRepsLoadedListener : function(listener)
		{
			this.salesRepsLoadedListeners.add(listener);
		},
		
		fireSalesRepsLoadedEvent : function()
		{
			for (var i=0; i<this.salesRepsLoadedListeners.size(); i++)
			{
				var listener = this.salesRepsLoadedListeners.get(i);
				listener.salesRepsLoaded();
			}
		},
		
		getSalesReps : function()
		{
			return this.salesReps;
		},
		
		getPanel : function()
		{
			return this.panelManager.getPanel();
		},
		
		showPanel : function()
		{
			this.panelManager.show();
		}
				
});var SplitSalesRep = Class.create(SalesRep, {
	initialize : function($super, salesRep)
	{
		if (salesRep != null)
		{
			$super(salesRep.getName(), salesRep.getId());
			this.current = salesRep.isCurrent();
		}
		this.amount = 0;
	},
		
	setAmount : function (amount)
	{
		this.amount = amount;
	},
	
	getAmount : function()
	{
		return this.amount;
	},
	
	clone : function()
	{
		var clone = new SplitSalesRep();
		clone.id = this.id;
		clone.name = this.name;
		clone.amount = this.amount;
		clone.current = this.current;
		
		return clone;
	}
});

var SalesRepAmountChangeEvent = Class.create({
	initialize : function(salesRepEditor)
	{
		this.source = salesRepEditor;
		this.value = salesRepEditor.getValue();
	},
	
	getSource : function()
	{
		return this.source;
	},
	
	getValue : function()
	{
		return this.value;
	}
});

var SalesRepEditor = Class.create({
	
	initialize : function(splitSalesRep)
	{
		if (splitSalesRep != null)
		{		
			this.splitSalesRep = splitSalesRep;
			this.component = null;
			this.amountEditor = null;
			// listeners
			this.salesRepAmountChangeListeners = new ArrayList();
			this.onSelectSalesRepListeners = new ArrayList();
			
			this.initComponent();
		}
	},
	
	getSalesRep : function()
	{
		return this.splitSalesRep;
	},
	
	initComponent : function()
	{
		var name = this.splitSalesRep.getName();
		var amount = this.splitSalesRep.getAmount();
		
		var labelCell = document.createElement('td');
		var textCell = document.createElement('td');
		
		var labelElement = null;
		
		if (this.isReference())
		{
			labelElement = document.createElement('span');
			labelElement.innerHTML = name;
			
			this.amountEditor = document.createElement('span');
			this.amountEditor.innerHTML = amount;
		}
		else
		{
			labelElement = document.createElement('button');
			labelElement.setAttribute('type', 'button');
			labelElement.setAttribute('class', 'big-button')
			labelElement.innerHTML = name;
			labelElement.onclick = this.fireOnSelectSalesRepEvent.bind(this);
			
			this.amountEditor = document.createElement('input');
			this.amountEditor.setAttribute('type', 'text');
			this.amountEditor.setAttribute('class', 'salesRepInput');
			this.amountEditor.setAttribute('style', 'font-size:20px; height:40px;');
			this.amountEditor.value = amount;
			this.amountEditor.onkeyup = this.fireSalesRepAmountChangeEvent.bind(this);
			this.amountEditor.onclick = this.fireOnSelectSalesRepEvent.bind(this);
			
			Event.observe(this.amountEditor,'focus',Keypad.clickHandler.bindAsEventListener(Keypad),false);
		}
		
		labelCell.appendChild(labelElement);
		textCell.appendChild(this.amountEditor);
		this.component = document.createElement('tr');
		this.component.appendChild(labelCell);
		this.component.appendChild(textCell);
	},
	
	fireSalesRepAmountChangeEvent : function()
	{
		var amtEntered = this.getValue();
		if (amtEntered > OrderAmount.getSubTotal())
		{
			this.setValue(this.splitSalesRep.getAmount());
			return;
		}
		
		this.splitSalesRep.setAmount(amtEntered);
		var salesRepAmountChangeEvent = new SalesRepAmountChangeEvent(this);
		for (var i=0; i<this.salesRepAmountChangeListeners.size(); i++)
		{
			var salesRepAmountChangeListener = this.salesRepAmountChangeListeners.get(i);
			salesRepAmountChangeListener.salesRepAmountChange(salesRepAmountChangeEvent);
		}
	},
	
	fireOnSelectSalesRepEvent : function()
	{
		for (var i=0; i<this.onSelectSalesRepListeners.size(); i++)
		{
			var onSelectSalesRepListener = this.onSelectSalesRepListeners.get(i);
			onSelectSalesRepListener.onSelectSalesRep(this);
		}
	},

	addSalesRepAmountChangeListener : function(listener)
	{
		this.salesRepAmountChangeListeners.add(listener);
	},
	
	addOnSelectSalesRepListener : function(listener)
	{
		this.onSelectSalesRepListeners.add(listener);
	},
	
	setFocus : function()
	{
		this.amountEditor.setAttribute('style', 'background-color:#9ACF30; font-weight:bold; font-size:20px; height:40px;');
	},
	
	resetFocus : function()
	{
		this.amountEditor.removeAttribute('style');
	},
	
	setStateChanged : function()
	{
		this.fireSalesRepAmountChangeEvent();
	},
	
	setValue : function(value)
	{
		this.splitSalesRep.setAmount(value);
		if (this.isReference())
		{
			this.amountEditor.innerHTML = value;
		}
		else
		{
			this.amountEditor.value = value;
		}
	},
	
	getValue : function()
	{
		if (this.isReference())
		{
			return parseFloat(this.amountEditor.innerHTML);
		}
		else
		{
			return parseFloat(this.amountEditor.value);
		}
	},
	
	isReference : function()
	{
		return this.splitSalesRep.isCurrent();
	},
	
	getId : function()
	{
		return this.splitSalesRep.getId();
	},
	
	getComponent : function()
	{
		return this.component;
	},
	
	equals : function (salesRepRowEditor)
	{
		return (this.getId() == salesRepRowEditor.getId());
	}
});

var SplitPanelManager = Class.create(PopUpBase, {
	
	initialize : function(refSalesRep, splitSalesRepList)
	{
		this.panel = $('split.order.popup.panel');
		this.innerPanel = $('split.order.popup.inner.panel');
		this.clear();
		//panel components
		this.subPanel = null;
		this.infoTotal = null;
		this.splitPanelToolBarManager = null;
		// editors
		this.refSalesRepEditor = null;
		this.salesRepEditors = new ArrayList(new SalesRepEditor());
		// event listeners
		this.salesRepAmountChangeListeners = new ArrayList();
		this.splitPanelClickOkListeners = new ArrayList();
		this.splitPanelClickCancelListeners = new ArrayList();
		// initialise components 
		this.initComponents(refSalesRep, splitSalesRepList);
		this.initPanel();
	},
	
	updateInfoTotal : function()
	{
		this.infoTotal.innerHTML = OrderAmount.getSubTotal();
	},
	
	initComponents : function(refSalesRep, splitSalesRepList)
	{
		this.refSalesRepEditor = new SalesRepEditor(refSalesRep);
		
		for (var i=0; i<splitSalesRepList.size(); i++)
		{
			var splitSalesRep = splitSalesRepList.get(i);
			
			var salesRepEditor = new SalesRepEditor(splitSalesRep);
			
			salesRepEditor.addSalesRepAmountChangeListener(this);
			salesRepEditor.addOnSelectSalesRepListener(this);
			
			this.salesRepEditors.add(salesRepEditor);
		}
	},
	
	initPanel : function()
	{
		this.createPopUp($(this.panel.getAttribute('id')));
	    
		this.subPanel =  document.createElement('table');
		this.subPanel.setAttribute('style', 'float:left');
		var infoTotalAmtTr = document.createElement('tr');
		var infoTotalTd = document.createElement('td');
		infoTotalTd.setAttribute('colspan', 2);
		infoTotalTd.setAttribute('align', 'right');
		
		infoTotalTd.setAttribute('style', 'font-weight:bold; font-size:20px');
		
		var labelTotal = document.createElement('div');
		labelTotal.innerHTML = Translation.translate('net.total','Net Total');
		
		this.infoTotal = document.createElement('div');
		this.infoTotal.innerHTML = OrderAmount.getSubTotal();
		
		infoTotalTd.appendChild(labelTotal);
		infoTotalTd.appendChild(this.infoTotal);
		
		infoTotalAmtTr.appendChild(infoTotalTd);
		
		this.subPanel.appendChild(infoTotalAmtTr);
		
		this.splitPanelToolBarManager = new SplitPanelToolBarManager(this.salesRepEditors, this.refSalesRepEditor);
		
		var toolbar = this.splitPanelToolBarManager.getToolBar();
		this.subPanel.appendChild(toolbar);
		
		this.subPanel.appendChild(this.refSalesRepEditor.getComponent());
		
		for (var i=0; i<this.salesRepEditors.size(); i++)
		{
			var salesRepEditor = this.salesRepEditors.get(i);
			var component = salesRepEditor.getComponent();
			this.subPanel.appendChild(component);
		}
		
		this.okBtn = $('split.order.popup.okButton');
	    this.okBtn.onclick = this.splitPanelClickOk.bind(this);
		
		this.cancelBtn = $('split.order.popup.cancelButton');
	    this.cancelBtn.onclick = this.splitPanelClickCancel.bind(this);
	    
		this.innerPanel.appendChild(this.subPanel);
		
		this.okBtn.focus();
	},
	
	onSelectSalesRep : function(salesRepEditor)
	{
		this.splitPanelToolBarManager.setSelectedSalesRepEditor(salesRepEditor);
		
		for (var i=0; i<this.salesRepEditors.size(); i++)
		{
			var sRepEditor = this.salesRepEditors.get(i);
			if (sRepEditor.equals(salesRepEditor))
			{
				sRepEditor.setFocus();
			}
			else
			{
				sRepEditor.resetFocus();
			}
		}
	},
	
	splitPanelClickOk : function()
	{
		this.hide();
		for (var i=0; i<this.splitPanelClickOkListeners.size(); i++)
		{
			var listener = this.splitPanelClickOkListeners.get(i);
			listener.splitPanelClickOk();
		}
	},
	
	splitPanelClickCancel : function()
	{
		this.hide();
		/*
		for (var i=0; i<this.splitPanelClickCancelListeners.size(); i++)
		{
			var listener = this.splitPanelClickCancelListeners.get(i);
			listener.splitPanelClickCancel();
		}
		*/
	},
	
	getRefSalesRepEditor : function()
	{
		return this.refSalesRepEditor;
	},
	
	setRefSalesRepEditor : function(salesRepEditor)
	{
		this.refSalesRepEditor = salesRepEditor;
	},
	
	addSalesRepAmountChangeListener : function(listener)
	{
		this.salesRepAmountChangeListeners.add(listener);
	},
	
	fireSalesRepAmountChangeEvent : function(salesRepAmountChangeEvent)
	{
		for (var i=0; i<this.salesRepAmountChangeListeners.size(); i++)
		{
			var salesRepAmountChangeListener = this.salesRepAmountChangeListeners.get(i);
			salesRepAmountChangeListener.salesRepAmountChange(salesRepAmountChangeEvent);
		}
	},
	
	addSplitPanelClickOkListener : function(listener)
	{
		this.splitPanelClickOkListeners.add(listener);
	},
	
	addSplitPanelClickCancelListener : function(listener)
	{
		this.splitPanelClickCancelListeners.add(listener);
	},
	
	clear : function()
	{
		this.innerPanel.innerHTML = '';
	},
	
	salesRepAmountChange : function(salesRepAmountChangeEvent)
	{
		this.fireSalesRepAmountChangeEvent(salesRepAmountChangeEvent);
	},
	
	getSalesRepEditors : function()
	{
		return this.salesRepEditors;
	},
	
	getPanel : function()
	{
		return this.innerPanel;
	}
});


var SplitPanelToolBarManager = Class.create({
		
		initialize : function(salesRepEditors, refSalesRepEditor)
		{
			this.salesRepEditors = salesRepEditors;	
			this.selectedSalesRepEditor = null;
			this.refSalesRepEditor = refSalesRepEditor;
			this.toolBar = null;
			
			this.initToolBar();
		},
		
		initToolBar : function()
		{
			var tr = document.createElement('tr');
			var td = document.createElement('td');
			td.setAttribute('style', 'text-align:right');
			td.setAttribute('colspan', 2);
			
			var size = this.salesRepEditors.size() + 1;
			
			var resetBtn = document.createElement('button');
			resetBtn.setAttribute('type', 'button');
			resetBtn.setAttribute('class', 'ok-button');
			resetBtn.innerHTML = Translation.translate('reset','RESET');
			resetBtn.onclick = this.reset.bind(this);
			
			for (var i=1; i<=size; i++)
			{
				var btn = document.createElement('button');
				btn.setAttribute('type', 'button');
				btn.setAttribute('class', 'ok-button')
				btn.innerHTML = "1/" + (i);
				btn.setAttribute('id', (i));
				btn.onclick =  this.calculateAmt.bind(this, btn.getAttribute('id'));
				
				td.appendChild(btn);
				td.appendChild(resetBtn);
				tr.appendChild(td);
			}
			this.toolBar = tr;
		},
		
		reset : function()
		{
			if (this.salesRepEditors.size() == 0)
			{
				return;
			}
			
			for (var i=0; i<this.salesRepEditors.size(); i++)
			{
				var salesRepditor = this.salesRepEditors.get(i);
				salesRepditor.setValue(0);
				salesRepditor.setStateChanged();
			}
		},
		
		calculateAmt : function(id)
		{
			var divisor = new Number(id);
			var selectedSalesRepEditor = this.selectedSalesRepEditor;
			if (selectedSalesRepEditor != null)
			{
				var refValue = OrderAmount.getSubTotal();
				var calculatedAmt = refValue / divisor;
				calculatedAmt = new Number(calculatedAmt).toFixed(2);
				
				selectedSalesRepEditor.setValue(calculatedAmt);
				selectedSalesRepEditor.setStateChanged();
			}
		},
		
		setSelectedSalesRepEditor : function(salesRepEditor)
		{
			this.selectedSalesRepEditor = salesRepEditor;
		},
		
		getToolBar : function()
		{
			return this.toolBar;
		}
});

var SplitManager = Class.create({
	
	initialize : function(splitSReps)
	{
		this.splitPanelManager = null;
		this.splitSalesReps = null;
		this.refSalesRep = null;
		this.salesRepInfoUpdater = null;
		
		this.refSalesRep_bak = null;
		this.splitSalesReps_bak = null;
		
		this.initSalesReps(splitSReps);
		this.initPanelManager();
		
		this.salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		this.refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		this.initSalesRepInfoUpdater();
	},
	
	saveState : function()
	{
		this.refSalesRep_bak = this.refSalesRep.clone();
		this.splitSalesReps_bak = new ArrayList(new SplitSalesRep());
		for (var i=0; i<this.splitSalesReps.size(); i++)
		{
			var splitSalesRep = this.splitSalesReps.get(i);
			var splitSalesRepClone = splitSalesRep.clone();
			this.splitSalesReps_bak.add(splitSalesRepClone); 
		}
	},
	
	rollBack : function()
	{
		this.refSalesRep = this.refSalesRep_bak;
		this.splitSalesReps = this.splitSalesReps_bak;
		this.initPanelManager();
		this.initSalesRepInfoUpdater();
		this.updateSalesRepInfo();
	},
	
	initSalesReps : function(splitSReps)
	{
		this.splitSalesReps = new ArrayList(new SplitSalesRep())
		
		
		for (var i=0; i<splitSReps.size(); i++)
		{
			var splitSalesRep = splitSReps.get(i);
			
			if (splitSalesRep.isCurrent())
			{
				this.refSalesRep = splitSalesRep;
			}
			else
			{
				this.splitSalesReps.add(splitSalesRep);
			}
		}
	},
	
	initPanelManager : function()
	{
		var refSalesRep = this.refSalesRep;
		var splitSalesReps = this.splitSalesReps;
		
		this.splitPanelManager = new SplitPanelManager(refSalesRep, splitSalesReps);
		this.splitPanelManager.addSalesRepAmountChangeListener(this);
		this.splitPanelManager.addSplitPanelClickOkListener(this);
		this.splitPanelManager.addSplitPanelClickCancelListener(this);
		
	},
	
	initSalesRepInfoUpdater : function()
	{
		this.salesRepInfoUpdater = new SalesRepInfoUpdater(this.refSalesRep, this.splitSalesReps);
		this.splitPanelManager.addSplitPanelClickOkListener(this.salesRepInfoUpdater);
	},
	
	resetAmounts : function()
	{
		this.initSalesRepAmounts();
		this.updateSalesRepInfo();
	},
	
	splitPanelClickOk : function()
	{
		this.saveState();
	},
	
	splitPanelClickCancel : function()
	{
		this.rollBack();
	},
	
	initSalesRepAmounts : function()
	{
	},
	
	updateSalesRepInfo : function()
	{
		this.salesRepInfoUpdater.update();
	},
	
	updateInfoTotal : function()
	{
		this.splitPanelManager.updateInfoTotal();
	},
	
	updateAmounts : function()
	{
	},
	
	salesRepAmountChange : function(salesRepAmountChangeEvent)
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var editor = salesRepAmountChangeEvent.getSource();
		var amt = editor.getValue();
		
		var refAmt = refSalesRepEditor.getValue();
		
		var totalAmt = refAmt;
		
		for (var i=0; i<salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);
			totalAmt += salesRepEditor.getValue();			
		}
		
		var orderAmt = OrderAmount.getSubTotal();
		var diff = totalAmt - orderAmt;
		
		if (diff < 0)
		{
			refAmt -= diff;
			refAmt = new Number(refAmt).toFixed(2);
			refAmt = parseFloat(refAmt);
			refSalesRepEditor.setValue(refAmt);
		}
		else if (diff > 0)
		{
			diff = refAmt - diff;
			if (diff >= 0)
			{
				diff = new Number(diff).toFixed(2);
				diff = parseFloat(diff);
				refSalesRepEditor.setValue(diff);
			}
			else
			{
				refSalesRepEditor.setValue(0);
				
				for (var i=0; i<salesRepEditors.size(); i++)
				{
					var salesRepEditor = salesRepEditors.get(i);
					if (!salesRepEditor.equals(editor))
					{
						diff += salesRepEditor.getValue();
						if (diff >= 0)
						{
							diff = new Number(diff).toFixed(2);
							diff = parseFloat(diff);
							salesRepEditor.setValue(diff);
							return;
						}
						else
						{
							salesRepEditor.setValue(0);
						}
					}
				}
			}
		}
	},
	
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
	},
	
	clockInOut : function()
	{
	},
	
	showPanel : function()
	{
		this.saveState();
		this.splitPanelManager.show();
		$('split.order.popup.okButton').focus();
	},
	
	hidePanel : function()
	{
		this.splitPanelManager.hide();
	},
	
	getRefSalesRep : function()
	{
		return this.refSalesRep;
	},
	
	getSplitSalesReps : function()
	{
		return this.splitSalesReps;
	},
	
	addSplitPanelClickOkListener : function(listener)
	{
		this.splitPanelManager.addSplitPanelClickOkListener(listener);
	},
	
	addSplitPanelClickCancelListener : function(listener)
	{
		this.splitPanelManager.addSplitPanelClickCancelListener(listener);
	},
	
	getPanel : function()
	{
		return this.splitPanelManager.getPanel();
	}
});

var SplitOrderManager = Class.create(SplitManager, {
	
	initialize : function($super, splitSalesReps)
	{
		$super(splitSalesReps);
		
		this.initSalesRepAmounts();
	},
	
	initSalesRepAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		if (refSalesRepEditor != null)
			refSalesRepEditor.setValue(OrderAmount.getSubTotal());
				
		if (salesRepEditors != null)
		{
			for (var i=0; i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				salesRepEditor.setValue(0);
			}
		}
	},
	
	updateAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var refAmt = refSalesRepEditor.getValue();
		var sum = refAmt;
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			sum += salesRepEditor.getValue();
		}
		
		var diff = OrderAmount.getSubTotal() - sum;
		diff += refAmt;
		diff = new Number(diff).toFixed(2);
		diff = parseFloat(diff);
		
		if (diff >= 0)
		{
			refSalesRepEditor.setValue(diff);
		}
		else
		{
			refSalesRepEditor.setValue(0);
			for (var i=0; i< salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);				
				diff += salesRepEditor.getValue();
				diff = new Number(diff).toFixed(2);
				diff = parseFloat(diff);
				
				if (diff >= 0)
				{
					salesRepEditor.setValue(diff);
					this.updateInfo();
					return;
				}
				else
				{
					salesRepEditor.setValue(0);
				}
			}
		}
		this.updateInfo();
	},
	
	updateInfo : function()
	{
		this.updateSalesRepInfo();
		this.updateInfoTotal();
	},
			
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
		this.updateSalesRepInfo();
	},
	
	clockInOut : function()
	{
	}
});

var SplitReturnManager = Class.create(SplitManager, {
	
	initialize : function($super, salesReps)
	{
		$super(salesReps);
		
		this.salesRepRatios = null;
		this.initRatios();
	},
	
	initRatios : function()
	{
		this.salesRepRatios = new Hash();
		
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var totalAmt = refSalesRepEditor.getValue();
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			totalAmt += salesRepEditor.getValue();
		}
		
		var refRatio = new Number(refSalesRepEditor.getValue()/totalAmt);
		this.salesRepRatios.set(refSalesRepEditor.getId(), refRatio);
		
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);
			var value = salesRepEditor.getValue();
			
			var ratio = new Number(value/totalAmt);
			this.salesRepRatios.set(salesRepEditor.getId(), ratio);
		}
	},
	
	initSalesRepAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		if (refSalesRepEditor != null)
			refSalesRepEditor.setValue(OrderAmount.getSubTotal());
		if (salesRepEditors != null)
		{
			for (var i=0; i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				salesRepEditor.setValue(0);
			}
		}
	},
	
	updateAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var total = OrderAmount.getSubTotal();
		var sum = 0;
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			var ratio = this.salesRepRatios.get(salesRepEditor.getId());
			var value = new Number(ratio * total).toFixed(2);
			sum += parseFloat(value);
			salesRepEditor.setValue(value);
		}
		
		var refUpdatedValue = new Number(total - sum).toFixed(2);
		refSalesRepEditor.setValue(refUpdatedValue);
		
		this.updateSalesRepInfo();
		this.updateInfoTotal();
	},
	
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
		
	},
	
	clockInOut : function()
	{
		
	}
});

var SplitExchangeManager = Class.create(SplitManager, {
	
	initialize : function($super, salesReps)
	{
		$super(salesReps);
		this.currentUserEditor = null;
		this.salesRepRatios = null;
		this.initRatios();
	},
	
	initRatios : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		this.salesRepRatios = new Hash();
		
		var totalAmt = refSalesRepEditor.getValue();
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);				
			totalAmt += salesRepEditor.getValue();
		}
		
		var refRatio = new Number(refSalesRepEditor.getValue()/totalAmt);
		this.salesRepRatios.set(refSalesRepEditor.getId(), refRatio);
		
		for (var i=0; i< salesRepEditors.size(); i++)
		{
			var salesRepEditor = salesRepEditors.get(i);
			var value = salesRepEditor.getValue();
			
			var ratio = new Number(value/totalAmt);
			this.salesRepRatios.set(salesRepEditor.getId(), ratio);
		}
	},
	
	initSalesRepAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		if (refSalesRepEditor != null)
			refSalesRepEditor.setValue(OrderAmount.getSubTotal());
		if (salesRepEditors != null)
		{
			for (var i=0; i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				salesRepEditor.setValue(0);
			}
		}
	},
	
	setCurrentUser : function(salesRep)
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var splitSalesRep = new SplitSalesRep(salesRep);
		var currentUserEditor = new SalesRepEditor(splitSalesRep);
		
		if (currentUserEditor.equals(refSalesRepEditor))
		{
			this.currentUserEditor = refSalesRepEditor;
		}
		else
		{
			for (var i=0 ;i<salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);
				if (salesRepEditor.equals(currentUserEditor))
				{
					this.currentUserEditor = salesRepEditor;
				}
			}
		}
	},
	
	updateAmounts : function()
	{
		var salesRepEditors = this.splitPanelManager.getSalesRepEditors();
		var refSalesRepEditor = this.splitPanelManager.getRefSalesRepEditor();
		
		var total = OrderAmount.getSubTotal();
		if (total > 0)
		{
			var sum = 0;
			for (var i=0; i< salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);	
				var value = salesRepEditor.getValue();
				if (value < 0)
				{
					salesRepEditor.setValue(0);
				}
				sum += salesRepEditor.getValue();
			}
			var refValue = refSalesRepEditor.getValue();
			if (refValue < 0)
			{
				refSalesRepEditor.setValue(0);
			}
			
			var updatedValue = OrderAmount.getSubTotal() - sum;
			updatedValue = new Number(updatedValue).toFixed(2);
			this.currentUserEditor.setValue(updatedValue);
		}
		else
		{
			var sum = 0;
			for (var i=0; i< salesRepEditors.size(); i++)
			{
				var salesRepEditor = salesRepEditors.get(i);				
				var ratio = this.salesRepRatios.get(salesRepEditor.getId());
				var value = new Number(ratio * total).toFixed(2);
				sum += parseFloat(value);
				salesRepEditor.setValue(value);
			}
			
			var updatedValue = new Number(total - sum).toFixed(2);
			refSalesRepEditor.setValue(updatedValue);
		}		
		this.updateSalesRepInfo();
		this.updateInfoTotal();
	},
	
	currentSalesRepChange : function(currentSalesRepChangeEvent)
	{
	},
	
	clockInOut : function()
	{
	}
});

var SalesRepInfoUpdater = Class.create({
		
		initialize : function(refSalesRep, splitSalesReps)
		{
			this.refSalesRep = refSalesRep;
			this.splitSalesReps = splitSalesReps;
			this.currencySymbol = OrderAmount.getCurrencySymbol();
		},
		
		splitPanelClickOk : function()
		{
			this.update();
		},
		
		update : function()
		{
			var salesRepAmount = $('salesRepAmount'+this.refSalesRep.getId());
			if (salesRepAmount != null)
				salesRepAmount.innerHTML = this.currencySymbol + ' ' + new Number(this.refSalesRep.getAmount()).toFixed(2);
			
			for (var i=0; i<this.splitSalesReps.size(); i++)
			{
				var splitSalesRep = this.splitSalesReps.get(i);
				salesRepAmount = $('salesRepAmount' + splitSalesRep.getId());
				if (salesRepAmount != null)
					salesRepAmount.innerHTML = this.currencySymbol + ' ' + new Number(splitSalesRep.getAmount()).toFixed(2);
			}
		}
});

var SplitOrderDetailsManager = Class.create({
	
	initialize : function(type, orderSplitSalesReps)
	{
		this.splitManager = null;
		
		if (type == 'sales')	
		{
			var clockedInSalesReps = ClockedInSalesRepManager.getSalesReps();
			var splitSalesReps = new ArrayList(new SplitSalesRep());
			
			for (var i=0; i<clockedInSalesReps.size(); i++)
			{
				var salesRep = clockedInSalesReps.get(i);
				var splitSalesRep = new SplitSalesRep(salesRep);
				splitSalesReps.add(splitSalesRep);
			}
			
			this.splitManager = new SplitOrderManager(splitSalesReps);
		}
		else if (type == 'return')
		{
			var clockedInSalesReps = ClockedInSalesRepManager.getSalesReps();
			var previousSalesRep = ClockedInSalesRepManager.getCurrentSalesRep();
			var currentSalesRep = previousSalesRep;
			var completeSalesReps = clockedInSalesReps.clone();
			
			var splitSalesReps = new ArrayList(new SplitSalesRep());
			for (var i=0; i<orderSplitSalesReps.size(); i++)
			{
				var orderSplitSalesRep = orderSplitSalesReps[i];
				var name = orderSplitSalesRep.name;
				var salesRepId = orderSplitSalesRep.salesRepId;
				var amount = orderSplitSalesRep.amount;
				var current = orderSplitSalesRep.isCurrent;
				
				var salesRep = new SalesRep(name, salesRepId);
				salesRep.setCurrent(current);
				
				if (salesRep.isCurrent())
				{
					currentSalesRep = salesRep;
				}
				
				if (!completeSalesReps.contains(salesRep))
				{
					completeSalesReps.add(salesRep);
				}
				
				var splitSalesRep = new SplitSalesRep(salesRep);
				splitSalesRep.setAmount(amount);
				splitSalesReps.add(splitSalesRep);
			}
			ClockInOut.render(completeSalesReps, currentSalesRep, previousSalesRep);				
			this.splitManager = new SplitReturnManager(splitSalesReps);
		}
		else if (type == 'exchange')
		{
			var clockedInSalesReps = ClockedInSalesRepManager.getSalesReps();
			var previousSalesRep = ClockedInSalesRepManager.getCurrentSalesRep();
			var currentSalesRep = previousSalesRep;
			var completeSalesReps = clockedInSalesReps.clone();
			
			var splitSalesReps = new ArrayList(new SplitSalesRep());
			for (var i=0; i<orderSplitSalesReps.size(); i++)
			{
				var orderSplitSalesRep = orderSplitSalesReps[i];
				var name = orderSplitSalesRep.name;
				var salesRepId = orderSplitSalesRep.salesRepId;
				var amount = orderSplitSalesRep.amount;
				var current = orderSplitSalesRep.isCurrent;
				
				var salesRep = new SalesRep(name, salesRepId);
				salesRep.setCurrent(current);
				
				if (salesRep.isCurrent())
				{
					currentSalesRep = salesRep;
				}
				
				if (!completeSalesReps.contains(salesRep))
				{
					completeSalesReps.add(salesRep);
				}
				
				var splitSalesRep = new SplitSalesRep(salesRep);
				splitSalesRep.setAmount(amount);
				splitSalesReps.add(splitSalesRep);
			}
			
			var isOrderSplit = (orderSplitSalesReps.size() > 0);
			for (var i=0; i<clockedInSalesReps.size(); i++)
			{
				var clockedInSalesRep = clockedInSalesReps.get(i);
				var splitSalesRep = new SplitSalesRep(clockedInSalesRep);
				splitSalesRep.setAmount(0);
				if (isOrderSplit)
				{
					splitSalesRep.setCurrent(false);
				}
				if (!splitSalesReps.contains(splitSalesRep))
				{
					splitSalesReps.add(splitSalesRep);
				}
			}
			
			ClockInOut.render(completeSalesReps, currentSalesRep, previousSalesRep);				
			this.splitManager = new SplitExchangeManager(splitSalesReps);
			this.splitManager.setCurrentUser(previousSalesRep);
		}
	},
	
	getSplitManager : function()
	{
		return this.splitManager;
	}
});

var OrderAmount = {
		
		isDraftedOrder : true,
		
		getSubTotal : function()
		{
			if (OrderAmount.isDraftedOrder)
			{
				return ShoppingCartManager.getSubTotal();
			}
			else
			{
				var orderHeader = receiptJSON.header;
				var isTaxIncluded = orderHeader.taxIncluded;
				var totalLines = orderHeader.totalLines;
				var taxTotal = orderHeader.taxTotal;
				var subTotal = isTaxIncluded ? (totalLines - taxTotal) : totalLines;
				
				return subTotal;
			}
		},
		
		getCurrencySymbol : function()
		{
			if (OrderAmount.isDraftedOrder)
			{
				return ShoppingCartManager.getCurrencySymbol();
			}
			else
			{
				return receiptJSON.header.currencySymbol;
			}
		}

}
