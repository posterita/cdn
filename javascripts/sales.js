/********/
var CashPanel =  Class.create(CheckoutPanel, {
      initialize: function($super) {
        $super('cash.popup.panel');
       
        this.amountRefunded = 0.0;
       
        /*buttons*/
        this.okBtn = $('cash.popup.okButton');
        this.cancelBtn = $('cash.popup.cancelButton');
       
        /*textfields*/
        this.amountTenderedTextField = $('cash.popup.amountTenderedTextField');  
       
        /*labels*/
        this.totalLabel = $('cash.popup.totalLabel');
        //this.writeOffCashLabel = $('cash.popup.writeOffCashLabel');
        //this.paymentAmtLabel = $('cash.popup.paymentAmtLabel');
        this.amountRefundedLabel = $('cash.popup.amountRefundedLabel');
        
        this.keypadPanel = $('keypad-panel');
        this.cashKeypadPanel = $('cash-keypad-panel');
        this.toggleKeypadButton = $('toggle-keypad-button');
        
        
       	this.toggleKeypadButton.onclick = this.toggleKeypad.bind(this);
        this.cancelBtn.onclick = this.hide.bind(this);
        this.okBtn.onclick = this.okHandler.bind(this);
       
        /*fields*/
        this.cartTotalAmount = OrderScreen.getCartTotal();
        this.currencySymbol = OrderScreen.getCurrencySymbol();
       
        this.totalLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
        //this.paymentAmtLabel.innerHTML = this.currencySymbol + ' ' + new Number(this.cartTotalAmount).toFixed(2);
       
        /*others*/
        this.amountTenderedTextField.panel = this;
        this.amountTenderedTextField.onkeypress = function(e){
           
            if(e.keyCode == Event.KEY_RETURN)
            {
                if(this.value == null || this.value == ''){
                    this.panel.onError('Invalid amount');
                    return;
                }
               
                this.panel.okHandler();   
                return;
            }
           
            var value = this.value;
            if(value == null || value == '') value = 0.0;
           
            var amountTendered = parseFloat(value);
            var paymentAmt = this.panel.cartTotalAmount;
           
            if(isNaN(amountTendered) || (amountTendered < 0.0)){
                this.panel.onError('Invalid amount');
                return;
            }
        };
       
        this.amountTenderedTextField.onkeyup = function(e){
           
            var value = this.value;
            if(value == null || value == '') value = 0.0;
           
            var amountTendered = parseFloat(value);
            var paymentAmt = this.panel.cartTotalAmount;
           
            if(isNaN(amountTendered) || (amountTendered < 0.0)){           
                return;
            }
           
            var change = 0;
            if(amountTendered != 0.0){
                change = amountTendered - paymentAmt;
            }
              
            change =  new Number(change).toFixed(2);   
           
            this.panel.amountRefundedLabel.innerHTML = '' + change;       
            this.panel.amountRefunded = change;
        };
       
        /*cash-out panel*/
        var cashOutButtons = $$('.cash-out-panel button');
        for(var i=0; i<cashOutButtons.length; i++){
            var btn = cashOutButtons[i];
            btn.onclick = this.cashOutHandler.bind(this);
        }
        
        var keypadButtons = $$('.keypad-button');
        for(var i=0; i<keypadButtons.length; i++){
            var btn = keypadButtons[i];
            btn.onclick = this.keypadHandler.bind(this);
        }
        
      },
     
      cashOutHandler:function(e){
          var element = Event.element(e);             
          var amt = parseFloat(element.innerHTML);
                       
          var amountTendered = this.amountTenderedTextField.value;
          if(amountTendered == '') amountTendered = 0;
         
          if('CLR' == element.innerHTML.strip()){
              amt = 0;
              amountTendered = 0;
          }
         
          amountTendered = parseFloat(amountTendered) + amt;
          this.amountTenderedTextField.value = new Number(amountTendered).toFixed(2);
         
          /*code from this->amountTenderedTextField->onkeyup*/
          var paymentAmt = this.cartTotalAmount;
           
            if(isNaN(amountTendered) || (amountTendered < 0.0)){           
                return;
            }
           
            var change = 0;
            if(amountTendered != 0.0){
                change = amountTendered - paymentAmt;
            }
              
            change =  new Number(change).toFixed(2);   
           
            this.amountRefundedLabel.innerHTML = '' + change;       
            this.amountRefunded = change;
          
            if ('CLR' == element.innerHTML.strip())
            {
            	this.amountTenderedTextField.focus();
        		this.amountTenderedTextField.select();
            }
      },
      
      keypadHandler:function(e){
    	  var button = Event.element(e); 
          var element = button.getElementsByClassName('keypad-value')[0];   
          
          /*Fix for google chrome*/
          if (element == null)
          {
          		element = button;  
          }
          /**********************/
          
          var value = element.innerHTML;
                       
          var amountTendered = this.amountTenderedTextField.value;
          
          //if(amountTendered == '') amountTendered = 0;
          
			if('Del' == element.innerHTML.strip()){
				
				if (this.amountTenderedTextField.selectionStart != this.amountTenderedTextField.selectionEnd)
				{
					amountTendered = '';
					this.amountTenderedTextField.value = '';
					this.amountTenderedTextField.focus();
				}
				else
				{
					if(amountTendered.length > 0)
						amountTendered = amountTendered.substring(0,amountTendered.length-1);
				}
          }
			else if('-' == element.innerHTML.strip()){
				
				if(amountTendered.indexOf('-') == -1){
					amountTendered = '-' + amountTendered;
				}
				else{
					amountTendered = amountTendered.substring(1);
				}
			}
			else if('.' == element.innerHTML.strip()){
				if(amountTendered.indexOf('.') == -1)
				{
					amountTendered = amountTendered + value;
				}
			}
			
			else{
				amountTendered = amountTendered + value;
			}
			
			this.amountTenderedTextField.value = amountTendered;
			
          /*code from this->amountTenderedTextField->onkeyup*/
          var paymentAmt = this.cartTotalAmount;
           
            if(isNaN(amountTendered) || (amountTendered < 0.0)){           
                return;
            }
           
            var change = 0;
            if(amountTendered != 0.0 && !isNaN(amountTendered)){
            	amountTendered = parseFloat(amountTendered);
                change = amountTendered - paymentAmt;
            }
              
            change =  new Number(change).toFixed(2);   
           
            this.amountRefundedLabel.innerHTML = '' + change;       
            this.amountRefunded = change;
      },
     
      okHandler:function(){
          if(!this.validate()) return;     
          this.hide();
          this.paymentHandler(this.payment);
      },
      resetHandler:function(){
          this.amountTenderedTextField.value = '';
          this.amountRefundedLabel.innerHTML = '0.00'; 
         
          this.amountRefunded = 0.0;
      },
      onShow:function(){
          this.amountTenderedTextField.focus();
          this.amountRefundedLabel.innerHTML = '0.00'; 
      },
      validate:function(){
         
          var amountTendered = parseFloat(this.amountTenderedTextField.value);
          if(isNaN(amountTendered) || (amountTendered < 0.0)){
              this.onError('Invalid amount');
              return false;
          }
         
          var paymentAmt = this.cartTotalAmount;
          if(amountTendered < paymentAmt){
              this.onError('Invalid amount');
              return false;
          }
         
          this.setPayment();
         
        return true;
      },
      setPayment:function(){
          var payment = new Hash();
          payment.set('amountTendered',this.amountTenderedTextField.value);
          payment.set('amountRefunded', this.amountRefunded);     
          this.payment = payment;
      },
      
      toggleKeypad : function(){
    	  
    	  this.amountTenderedTextField.value = '';
    	  this.amountTenderedTextField.focus();
    	  
    	  if (this.cashKeypadPanel.style.display == 'none')
    	  {
    		  this.cashKeypadPanel.style.display = 'block';
    		  this.keypadPanel.style.display = 'none';
    	  }
    	  else
    	  {
    		  this.cashKeypadPanel.style.display = 'none';
    		  this.keypadPanel.style.display = 'block';
    	  }
      },
      
      paymentHandler:function(payment){}
    });

SearchProduct.onSelect = function(product){
    if(product.editpriceonfly == 'Y' || product.editdesconfly == 'Y'){
        var panel = new EditOnFlyPanel();
        panel.product = product;
        panel.show();
    }
    else if(product.ismodifiable == 'Y'){
        var panel = new SelectModifierPanel();
        panel.loadModifiers(product);
        panel.show();
    }
    else{
        shoppingCart.addToCart(product.m_product_id,1);
    }
};   

SearchProduct.afterRender = function(){
    jQuery('#pane3').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
    OrderScreen.selectQueryBox();
};

/* set highlight color */
if(OrderScreen.orderType == ORDER_TYPES.CUSTOMER_RETURNED_ORDER
        || OrderScreen.orderType == ORDER_TYPES.POS_GOODS_RETURNED_NOTE){
    Global.highlightStyleClass = 'highlightOrange';
}

if(!shoppingCart.isSoTrx){
    /*disable upsell for purchase*/
    OrderScreen.allowUpSell = false;
}


 function setE2EData(data){
     if($('e2e.popup.panel').style.display == 'block'){
             $('e2e.popup.trackDataTextField').value = data;
     }
    
     }
 
 function showCustomerTab()
 {
	 jQuery('#tabs a[href="#customer"]').tab('show');
 }
/********/

