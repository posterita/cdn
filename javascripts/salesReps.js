var SalesRepCheck = Class.create({
	
		initialize : function(salesRep)
		{
			this.salesRep = salesRep;
			this.checkBox = document.createElement('input');
			this.checkBox.setAttribute('type', 'checkbox');
		},
		
		isChecked : function()
		{
			return this.checkBox.checked;
		},
		
		getSalesRep : function()
		{
			return this.salesRep;
		},
		
		getCheckBox : function()
		{
			return this.checkBox;
		}
});

var SalesRepSelectPanelManager = Class.create(PopUpBase, {
	
		initialize : function()
		{
			this.panel = $('split.order.select.salesrep.panel');
			this.salesRepList = new ArrayList();
			this.clickAddEventListeners = new ArrayList();
			this.clickRemoveEventListeners = new ArrayList();
			
			this.EVENT_ADD = 'ADD';
			this.EVENT_REMOVE = 'REMOVE';			
			
			this.addBtn = document.createElement('input');
			this.addBtn.setAttribute('type', 'button');
			this.addBtn.setAttribute('value', Translation.translate("add"));
			this.addBtn.setAttribute('id', 'addSalesRepsBtn');
			this.addBtn.onclick = this.addHandler.bind(this);
			
			this.removeBtn = document.createElement('input');
			this.removeBtn.setAttribute('type', 'button');
			this.removeBtn.setAttribute('value', Translation.translate("remove"));
			this.removeBtn.setAttribute('id', 'removeSalesRepsBtn');
			this.removeBtn.onclick = this.removeHandler.bind(this);
		},
		
		initSalesReps : function(salesReps)
		{
			for (var i=0; i<salesReps.size(); i++)
			{
				var salesRep = salesReps.get(i);
				var salesRepCheck = new SalesRepCheck(salesRep);
				this.salesRepList.add(salesRepCheck);
			}
			this.initPanel();
		},
		
		initPanel : function()
		{
			this.createPopUp($(this.panel.getAttribute('id')));
			var table = document.createElement('table');
			table.setAttribute('id', 'salesreps_table');
			for (var i=0; i < this.salesRepList.size(); i++)
			{
				var salesRepCheck = this.salesRepList.get(i);
				var checkBox = salesRepCheck.getCheckBox();
				var salesRep = salesRepCheck.getSalesRep();
				checkBox.setAttribute('id', salesRep.getId() + 'check');
				
				
				var tdCheck = document.createElement('td');
				tdCheck.appendChild(checkBox);
				
				var tdLabel = document.createElement('td');
				tdLabel.innerHTML = salesRep.getName();
				tdLabel.setAttribute('id', salesRep.getId());
				
				var tr = document.createElement('tr');	
				if (i%2 == 0)
				{
					tr.setAttribute('class', 'ev');
				}
				else
				{
					tr.setAttribute('class', 'od');
				}
				
				tdLabel.onclick = function(e)
				{
					var id = this.getAttribute('id');
					var chbox = $(id + 'check');
					chbox.checked = !(chbox.checked);
				};
				
				tr.appendChild(tdCheck);
				tr.appendChild(tdLabel);
				table.appendChild(tr);
			}
			var trBtn = document.createElement('tr');
			var tdBtn = document.createElement('td');
			tdBtn.setAttribute('colspan', 2);
			tdBtn.appendChild(this.removeBtn);
			tdBtn.appendChild(this.addBtn);
			trBtn.appendChild(tdBtn);
			table.appendChild(trBtn);
			this.panel.appendChild(table);
		},
		
		addHandler : function()
		{
			this.fireAddEvent();
		},
		
		removeHandler : function()
		{
			this.fireRemoveEvent();
		},
		
		addEventListener : function(type, listener)
		{
			if (type == null || listener == null)
				return;
			
			if (type == this.EVENT_ADD)
			{
				this.clickAddEventListeners.add(listener);
			}
			
			if (type == this.EVENT_REMOVE)
			{
				this.clickRemoveEventListeners.add(listener);
			}
		},
		
		fireAddEvent : function()
		{
			for (var i=0; i<this.clickAddEventListeners.size(); i++)
			{
				var selectedList = new ArrayList(); 
				for (var j=0; j<this.salesRepList.size(); j++)
				{
					var salesRepCheck = this.salesRepList.get(j);
					if (salesRepCheck.isChecked())
					{
						selectedList.add(salesRepCheck.getSalesRep());
					}
				}
				
				var listener = this.clickAddEventListeners.get(i);
				listener.addHandler(selectedList);
			}
		},
		
		fireRemoveEvent : function()
		{
			for (var i=0; i<this.clickRemoveEventListeners.size(); i++)
			{
				var selectedList = new ArrayList(); 
				for (var j=0; j<this.salesRepList.size(); j++)
				{
					var salesRepCheck = this.salesRepList.get(j);
					if (salesRepCheck.isChecked())
					{
						selectedList.add(salesRepCheck.getSalesRep());
					}
				}
				
				var listener = this.clickRemoveEventListeners.get(i);
				listener.removeHandler(selectedList);
			}
		}
});


var SalesRepSelectManager = Class.create({
		
		initialize : function()
		{
			this.salesReps = new ArrayList(new SalesRep());
			this.orderSalesRep = null;
			this.panelManager = new SalesRepSelectPanelManager();
			this.panelManager.addEventListener(this.panelManager.EVENT_ADD, this);
			this.panelManager.addEventListener(this.panelManager.EVENT_REMOVE, this);
			
			this.EVENT_ADD = this.panelManager.EVENT_ADD;
			this.EVENT_REMOVE = this.panelManager.EVENT_REMOVE;			
			
			this.clickAddEventListeners = new ArrayList();
			this.clickRemoveEventListeners = new ArrayList();
			
			this.salesRepsLoadedListeners = new ArrayList();
		},
		
		init : function(orderId)
		{
			var action = 'UserAction.do?action=loadSalesReps';
			var params = 'orderId='+orderId;
			
			var myAjax = new Ajax.Request(action, 
			{
				method : 'POST',
				parameters : params,
				onSuccess : this.loadSalesReps.bind(this),
				onFailure : this.reportFailure.bind(this)
			});
		},
		
		addEventListener : function(type, listener)
		{
			if (type == null || listener == null)
				return;
			
			if (type == this.EVENT_ADD)
			{
				this.clickAddEventListeners.add(listener);
			}
			
			if (type == this.EVENT_REMOVE)
			{
				this.clickRemoveEventListeners.add(listener);
			}
		},
		
		addHandler : function(salesReps)
		{
			for (var i=0; i<this.clickAddEventListeners.size(); i++)
			{
				var listener = this.clickAddEventListeners.get(i);
				listener.addHandler(salesReps);
			}
		},
		
		removeHandler : function(salesReps)
		{
			for (var i=0; i<this.clickRemoveEventListeners.size(); i++)
			{
				var listener = this.clickRemoveEventListeners.get(i);
				listener.removeHandler(salesReps);
			}
		},
		
		loadSalesReps : function(request)
		{
			var response  = request.responseText;
			var salesRepsJSON = eval('(' + response + ')');
			
			for (var i=0; i<salesRepsJSON.length; i++)
			{
				var _salesRep = salesRepsJSON[i];
				var salesRep = new SalesRep(_salesRep.name, _salesRep.id);
				salesRep.setCurrent(_salesRep.isCurrent);
				
				if (_salesRep.isCurrent)
				{
					this.orderSalesRep = salesRep;
				}
				else
				{
					this.salesReps.add(salesRep);
				}
			}
			
			this.panelManager.initSalesReps(this.salesReps);
			this.fireSalesRepsLoadedEvent();
		},
		
		reportFailure : function(request)
		{
			
		},
		
		addSalesRepsLoadedListener : function(listener)
		{
			this.salesRepsLoadedListeners.add(listener);
		},
		
		fireSalesRepsLoadedEvent : function()
		{
			for (var i=0; i<this.salesRepsLoadedListeners.size(); i++)
			{
				var listener = this.salesRepsLoadedListeners.get(i);
				listener.salesRepsLoaded();
			}
		},
		
		getSalesReps : function()
		{
			return this.salesReps;
		},
		
		getPanel : function()
		{
			return this.panelManager.getPanel();
		},
		
		showPanel : function()
		{
			this.panelManager.show();
		}
				
});