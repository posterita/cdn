/**
 * Search Object needs the following fields from the server json:
 * id, name and description
 * 
 * **/
var SearchObject = Class.create({
	
	initialize : function(searchManager, objectJSON)
	{
		this.searchManager = searchManager;
		
		if (objectJSON != null)
		{
			this.id = objectJSON.id;
			this.name = objectJSON.name;
			this.description = objectJSON.description;
			this.m_product_parent_id = objectJSON.m_product_parent_id;
			this.isSerialNo = objectJSON.isserialno == 'Y' ? true : false;
			this.isParent = objectJSON.isparent == 'Y' ? true : false;
			this.parentName = objectJSON.parentname;
			
			this._component = null;
			
			this.build();
		}
	},
	
	build : function()
	{
		this._component = document.createElement('tr');
		this._component.style.cursor = 'pointer';
		
		var td = document.createElement('td');
		
		if (this.isParent)
		{
			td.style.color = '#346AA7';
		}
			
		td.innerHTML = this.name + '<br>' + this.description;
		
		this._component.appendChild(td);
		
		this._component.onclick = this.onSelect.bind(this);		
	},
	
	onSelect : function()
	{
		if (this.isParent)
		{
			/*this.searchManager.params.set('filterQuery', 'p.m_product_parent_id =' + this.id);
			this.searchManager.search();*/
			var editParentProductPanel = new EditParentProductPanel(this);
			editParentProductPanel.show();
		}
		else
		{
			this.searchManager.onSelect(this);
		}
	},
	
	highlight : function()
	{
		this._component.addClassName('search-highlight');
	},
	
	removeHighlight : function()
	{
		this._component.removeClassName('search-highlight');
	},
	
	getComponent : function()
	{
		return this._component;
	},
	
	getId : function()
	{
		return this.id;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getDescription : function()
	{
		return this.description;
	},
	
	equals : function(searchObject)
	{
		return (this.id == searchObject.getId());
	}
});

var SearchManager = Class.create({
	
	initialize : function()
	{
		this._searchInput = $('search-textfield');
		this._searchBtn = $('search-button');
		this._result = $('search-result');
		this.jqueryScrollPane = 'search-pane';
		
		this.params = new Hash();
		this.searchObjectList = new ArrayList(new SearchObject());
		this.actionUrl = null;
		this.selectListener = null;
		this.activeSearchObject = null;
		
		this.setURLParams();
		this.init();
	},
	
	setURLParams : function()
	{},
	
	init : function()
	{
		this._searchBtn.onclick = this.search.bind(this);
		this._searchInput.onkeyup = this.wait.bind(this);
		this._result.innerHTML = '';
		this.params.set('filterQuery', 'p.m_product_parent_id is null');
	},
	
	wait : function(e)
	{
		if (e.keyCode == Event.KEY_RETURN){
			this.search();
		}
	},

	search : function()
	{
		if (this._searchInput == null)
			return;
		
		this.params.set('searchTerm', this._searchInput.value);
		
		new Ajax.Request(this.actionUrl, {
			method:'post',
			onSuccess: this.render.bind(this),
			parameters:this.params
		});
		
		this.resetFilterQuery();
	}, 
	
	render : function(response)
	{
		var json = response.responseText.evalJSON(true);
		this._result.innerHTML = '';
		
		var _searchResultTable = document.createElement('table');
		_searchResultTable.setAttribute('id', 'searchResultTable');
		_searchResultTable.setAttribute('border', 0);
		_searchResultTable.setAttribute('width', '100%');

		var list = json.list;
		
		for (var i=0; i<list.length; i++)
		{
			var objectJSON = list[i];
			var searchObject = new SearchObject(this, objectJSON);
			this.searchObjectList.add(searchObject);
			
			var _row = searchObject.getComponent();
			//_row.setAttribute('class', (i%2==0)? "odd" : "even");
			
			_row.setAttribute('class', 'searchResultRow');
			
			if (searchObject)
			
			_searchResultTable.appendChild(_row);
			
			if (this.searchObjectList.length == 1)
			{
				this.onSelect(searchObject);
			}
		}

		this._result.appendChild( _searchResultTable);
		
		var scrollPaneVar = this.jqueryScrollPane;
		
		jQuery(document).ready(function(){

			jQuery('#'+scrollPaneVar).jScrollPane({scrollbarWidth:42, scrollbarMargin:0, className:'scroll-container'});
		});
		
		//$('search-pane').style.width = '100%';
	},
	
	onSelect : function(searchObject)
	{
		for (var i=0; i<this.searchObjectList.size(); i++)
		{
			var obj = this.searchObjectList.get(i);
			obj.removeHighlight();
		}
		
		searchObject.highlight();
		
		this.selectListener.searchObjectSelected(searchObject.getId());
		this.activeSearchObject = searchObject;
	},
	
	setSelectListener : function(listener)
	{
		this.selectListener = listener;
	},
	
	resetFilterQuery : function()
	{
		this.params.set('filterQuery', 'p.m_product_parent_id is null');
	},
	
	getActiveSearchObject : function()
	{
		return this.activeSearchObject;
	}
	
});
