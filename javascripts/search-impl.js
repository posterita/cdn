var ProductSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'ProductAction.do?action=productSearch';
	}
});

var ProductTemplateSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'ProductAction.do?action=productTemplateSearch';
	}
});

var CustomerSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'BPartnerAction.do?action=partnerSearch';
		this.params.set('isCustomer', true);
		this.params.set('isVendor', false);
	}
});

var VendorSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'BPartnerAction.do?action=partnerSearch';
		this.params.set('isVendor', true);
		this.params.set('isCustomer', false);
	}
});

var WarehouseSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'WarehouseAction.do?action=warehouseSearch';
	}
});

var PriceListSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'PriceListAction.do?action=priceListSearch';
	}
});

var EventNotifierSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'EventNotifierAction.do?action=eventNotifierSearch';
	}
});

var PaymentTermSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'PaymentTermAction.do?action=paymentTermSearch';
	}
});

var ModifierGroupSearchManager = Class.create(SearchManager, {
	
	initialize : function($super)
	{
		$super();
	},
	
	setURLParams : function()
	{
		this.actionUrl = 'ModifierGroupAction.do?action=ModifierGroupSearch';
	}
});