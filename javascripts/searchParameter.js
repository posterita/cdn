var showResult = function(element, update)
{ 
    if(!update.style.position || update.style.position=='absolute') 
    {
    	update.style.position = 'absolute';
    	Position.clone(element, update, {setHeight: false, offsetTop: element.offsetHeight});
    }      
    
    update.style.display = 'block';
    update.style.opacity = '1.0';
    
  };
   
var afterUpdateParameter = function(e1, e2)
{
	var paramTagName = e1.name.substring(0, e1.name.length - 5);
	var id = e2.getAttribute('id');
	
	$(paramTagName).value = id;
};
