/** search product.js **/
var SearchProduct = {
		searchResultPlaceHolder:'productSearchPlaceHolder',
		isSoTrx:true,
		ignoreTerminalPricelist:false,
		input:null,
		active:null,
		filterQuery:null,
		param: new Hash(),
		createInstanceProductPanel: null,
		isChildSearch: false,
		
		search:function( criteria ){
			
			/*TODO: check network*/
			
			if(SearchProduct.input == null) SearchProduct.input = $('search-product-textfield');
			
			if (!SearchProduct.isChildSearch)
			{
				//reset search parameter
				//SearchProduct.filterQuery = 'm_product.m_product_parent_id is null';
				SearchProduct.filterQuery = '';
			}
			
			var searchTerm = null;
			
			if( criteria && SearchProduct.isSoTrx == true 
					&& SearchProduct.searchByBarcodeOnly 
					&& SearchProduct.searchByBarcodeOnly == true 
					&& criteria.barcode 
					&& criteria.barcode == true )
			{					
				SearchProduct.param.set('searchByBarcodeOnly', 'true');
				
			}else
			{
				SearchProduct.param.set('searchByBarcodeOnly', 'false');
				
			}
			
			if( criteria && criteria.searchTerm )
			{
				searchTerm = criteria.searchTerm
			}
			else
			{
				searchTerm = SearchProduct.input.value;
			}			
			
			//var param = new Hash();
			SearchProduct.param.set('action', 'search');
			SearchProduct.param.set('searchTerm', searchTerm );
			SearchProduct.param.set('isSoTrx', SearchProduct.isSoTrx);
			SearchProduct.param.set('bpPricelistVersionId', SearchProduct.bpPricelistVersionId);
			SearchProduct.param.set('warehouseId', SearchProduct.warehouseId);
			SearchProduct.param.set('ignoreTerminalPricelist', SearchProduct.ignoreTerminalPricelist);
			SearchProduct.param.set('filterQuery', SearchProduct.filterQuery);
			
			SearchProduct.highlightTextField();
			
			/*if (SearchProduct.input.value == '')
			{
				Ajax.Responders.register({
					  onCreate: function() {
						  jQuery('#indicator').css('display', 'block');
					  },
					  onComplete: function() {
						  jQuery('#indicator').css('display', 'none');
					  }
				});
			}*/
	
			new Ajax.Request('ProductAction.do', {
				method:'post',
				onSuccess: function(response){
					
					/*Ajax.Responders.register({
						  onCreate: function() {
							  jQuery('#indicator').css('display', 'none');
						  },
						  onComplete: function() {
							  jQuery('#indicator').css('display', 'none');
						  }
					});*/
					
					var json = response.responseText.evalJSON(true);					
					
					
					if (json.isSerialNoSearch)
					{
						SearchProduct.renderSerialNo(json);
					}
					else
					{
						SearchProduct.render(json);
					}
				},
				parameters: SearchProduct.param
			});
			
		},
		render:function(json){
			
			if(json.error && json.error == true){
				
				if(json.reason == 'session.timeout'){
					
					BootstrapDialog.show({
						closable: false,
						type: BootstrapDialog.TYPE_DANGER,
			            title: 'Session Expired',
			            message: 'Your session has expired. Please login again.',
			            buttons: [{
			                label: 'OK',
			                action: function(dialog) {
			                	window.location.href = "select-user.do";
			                }
			            }]
			        });
			        
				}
				else{
					alert("Failed to search product! Reason: " + json.reason);
				}
				
				return;
			}
			
			var container = $('searchResultContainer');
			container.innerHTML = '';
			
			var html = "<table id='searchResultTable' border='0' width='100%'>";

			var results = json.products;
			
			//filter modifiers
			var products = [];
			
			for(var i=0; i<results.length; i++){
				var record = results[i];
				if(record.ismodifier == 'Y') continue;
				
				products.push(record);
			}
			
			if (!json.success)
			{
				if (json.shopSavvy)
				{
					var shopSavvyPanel = new ShopSavvyProductPanel(json.shopSavvy, json.taxCategories);
					shopSavvyPanel.show();
				}
				else
				{
					var productSearchErrorPanel = new ProductSearchErrorPanel(SearchProduct.input.value);
					productSearchErrorPanel.show();
				}
			}
			
			var count = 0;
			
			for(var i=0; i<products.length; i++)
			{
				var product = products[i];
				
				/* lot and expiry check*/	
				/*
				if(product.isbatchandexpiry == 'Y'){
					
					// sum qty on hand
					while( i+1 <products.length && product.m_product_id == products[i+1].m_product_id ){
						product.qtyonhand += products[i+1].qtyonhand;
						i++;
					}
					 
				}
				*/
				
				var isParent = product.isparent == 'Y'? true : false;
				var isSerialNo = product.isserialno == 'Y' ? true : false;
				var m_product_parent_id = product.m_product_parent_id;
				
				count ++;
				
				var styleClass = 'even-row';
				
				if (count%2 != 0)
				{
					styleClass = 'odd-row';
				}
				
				if ((OrderScreen.orderType == 'POS Order' && isSerialNo && product.qtyonhand == 0 && !isParent) 
					|| (OrderScreen.isInventory() && isSerialNo && product.qtyonhand == 0 && !isParent)
					|| (OrderScreen.orderType == 'POS Goods Receive Note' && isSerialNo && product.qtyonhand == 1 && !isParent)
					/*|| (!SearchProduct.isChildSearch && m_product_parent_id != '')*/)
				{
					//If product is serial no and is not parent and has already been sold do not display in sales
					//Or if product is serial no and is not parent and qty is zero, we cannot transfer
					//Or if product is serial no and is not parent and qty is one, we cannot purchase
					
					products.splice(i, 1);
					i--;
				}
				else
				{
					html += "<tr data-test-id='product-search-result-product-info' class='" + (product.isparent == 'Y' ? " parentProduct" : "") + "' id='product_" + product.m_product_id + "'>";
					html += "<td class='"+ styleClass +"' style='width:80%;'><span style='padding-left:20px;'>";
					html += product.name;
					html += "<span><br><span style='padding-left:20px;'>";
					html += product.description;
					html += "</span></td>";
					if(product.producttype == 'I') {
						html += "<td class='"+ styleClass +"' style='width:20%;'><span style='padding-right:20px;' class='pull-right'>";
						html += product.qtyonhand;
						html += "x</span></td>";
					}else{
						html += "<td class='"+ styleClass +"' style='width:20%;'><span style='padding-right:20px;' class='pull-right'>";
						html += "&nbsp;</span></td>";
					}
					html += "</tr>";
				}
			}

			html += "</table>";
			
			container.innerHTML = html;
			
			//Reset
			SearchProduct.isChildSearch = false;
			
			/*add behaviour*/
			var table = $('searchResultTable');
			var rows = table.rows;
			var searchTerm = SearchProduct.param.get("searchTerm");
			
			if (products.length == 1)
			{
				var product = products[0];
				var isParent = product.isparent == 'Y'? true : false;
				var m_product_parent_id = product.m_product_parent_id;
				var isSerialNo = product.isserialno == 'Y' ? true : false;
				
				
				if (!isParent)
				{
					if(product.upc == searchTerm)
					{
						SearchProduct.onSelect(product, false);
						/*var highlightColor = 'search-product-highlight';
						
						if(SearchProduct.active){
							SearchProduct.active.removeClassName(highlightColor);						
						}*/
					}
					else
					{
						SearchProduct.onSelect(product, SearchProduct.trackManualProductEntry);
					}
					
				}
			}
			
			for(var i=0; i<rows.length; i++)
			{
				var row = rows[i];
				row.product = products[i];
				
				row.style.cursor = 'pointer';
				row.onclick = function(e){
					
					var isParent = this.product.isparent == 'Y'? true : false;
					var isSerialNo = this.product.isserialno == 'Y' ? true : false;
					
					//If product is parent and is serialno and all instances have already been created and sold
					if (isParent && isSerialNo && this.product.qtyonhand ==  0 && OrderScreen.orderType == 'POS Order')
					{
						new AlertPopUpPanel(Translation.translate('all.serial.no.have.already.been.created.and.sold','All serial no have already been created and sold.')).show();
						return;
					}
					
					//If product is parent and is serialno and qty is zero and we are sending stock
					//We cannot send parent with qty 0
					if (isParent && isSerialNo && this.product.qtyonhand ==  0 && OrderScreen.isInventory() && OrderScreen.orderType == 'stock-transfer-send')
					{
						new AlertPopUpPanel(Translation.translate('you.cannot.transfer.a.parent.with.quantity.zero','You cannot transfer a parent with quantity zero.')).show();
						return;
					}
					
					//If product is parent and is serialno and qty is zero and we are requesting stock
					if (isParent && isSerialNo && this.product.qtyonhand ==  0 && OrderScreen.isInventory() && OrderScreen.orderType == 'stock-transfer-request')
					{
						SearchProduct.onSelect(this.product);
						
						var highlightColor = 'search-product-highlight';
						
						if(SearchProduct.active){
							SearchProduct.active.removeClassName(highlightColor);						
						}
						
						SearchProduct.active = this;
						this.addClassName(highlightColor);
						return;
					}
					
					//If an existing(old) product or child, add to cart
					if (!isParent)
					{
						SearchProduct.onSelect(this.product, SearchProduct.trackManualProductEntry && searchTerm != this.product.upc);
						
						var highlightColor = 'search-product-highlight';
						
						if(SearchProduct.active){
							SearchProduct.active.removeClassName(highlightColor);						
						}
						
						SearchProduct.active = this;
						this.addClassName(highlightColor);
					}
					else
					{
						/*Is a parent product and may have children, query children*/
						SearchProduct.isChildSearch = true;
						
						SearchProduct.filterQuery = 'm_product.m_product_parent_id=' + this.product.m_product_id;
						
						/*Non Instance Variants will always exists with children, render children list
						If product is serialNo, it may not have instance. Prompt to create specific instance
						If instances exist and all instances has been created, render children list
						If not all instances have been created, render children list and prompt to create more instance
						*/
						
						//If Non instance variant, search children and render list
						if (!isSerialNo)
						{
							SearchProduct.search();
						}
						else
						{
							//SerialNo
							SearchProduct.param.set('isSerialNoSearch', true);
							
							/*if (OrderScreen.orderType == 'POS Goods Receive Note')
							{
								//Since we are purchasing serial no, we should purchase child with qty 0
								//SearchProduct.filterQuery = 'searchproduct.qtyonhand=0;searchproduct.hasbeensold=\'N\';searchproduct.m_product_parent_id=' + this.product.m_product_id;
								SearchProduct.filterQuery = 'm_product.m_product_parent_id=' + this.product.m_product_id;
							}
							else
							{
								//do not display child with qty 0 on sales and inventory
								//SearchProduct.filterQuery = 'm_product.qtyonhand<>0;m_product.m_product_parent_id=' + this.product.m_product_id;
								SearchProduct.filterQuery = 'm_product.m_product_parent_id=' + this.product.m_product_id;
							}*/
							
							/*SearchProduct.filterQuery += ' and stock.qtyonhand > 0';*/
							SearchProduct.active = this;
							SearchProduct.search();
						}
					}
					
					//resetFilter after serialno search
					//SearchProduct.filterQuery = 'm_product.m_product_parent_id is null';
					SearchProduct.param.set('isSerialNoSearch', false);
				};
			}
				
			SearchProduct.afterRender();
			SearchProduct.input.value = '';
		},
		afterRender:function(){
			jQuery('#pane3').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
		},
		onSelect:function(product, manualEntry){
			shoppingCart.addToCart(product.m_product_id, 1, null, null, null, null, manualEntry);
			setShoppingCartHeight();
		},
		initializeComponents:function(){
			SearchProduct.input = $('search-product-textfield');
			$('search-product-button').onclick = function(e){
				SearchProduct.search({barcode : true});
			};

			$('search-product-textfield').onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					SearchProduct.search({barcode : true});
				}
			};
			
			$('search-product-textfield').onfocus = function(e){
				this.select();
			};
			
			$('search-product-textfield').onclick = function(e){
				this.select();
			};
		},
		highlightTextField : function()
		{
			$('search-product-textfield').select();
		},
		clearTextField : function()
		{
			$('search-product-textfield').value = '';
		},
		disableTextFieldFocus : function()
		{
			$('search-product-textfield').blur();
		},
		
		renderSerialNo : function(json)
		{
			//If no Instance exists
			if  (!json.success)
			{
				//Prompt to create new Instance
				SearchProduct.createInstanceProductPanel = new CreateInstanceProductPanel(SearchProduct.active.product);
				SearchProduct.createInstanceProductPanel.displayButtons();
				SearchProduct.createInstanceProductPanel.show();
			}
			else
			{
				//Render children List and Prompt to create new Instance if any
				//Check if we can create more instance
				
				if (SearchProduct.active.product.qtyonhand == 1 && SearchProduct.active.product.isparent == 'N' && OrderScreen.orderType == 'POS Order')
				{
					//add the only instance left to cart
					SearchProduct.onSelect(json.products[0]);
				}
				else
				{
					SearchProduct.createInstanceProductPanel = new CreateInstanceProductPanel(SearchProduct.active.product);
					SearchProduct.createInstanceProductPanel.productListJSON = json;
					SearchProduct.createInstanceProductPanel.displayButtons();
					SearchProduct.createInstanceProductPanel.show();
				}
			}
			
			/*if (SearchProduct.createInstanceProductPanel != null)
			{
				SearchProduct.createInstanceProductPanel.productListJSON = json;
				SearchProduct.createInstanceProductPanel.noOfChildren = json.products.length;
				SearchProduct.createInstanceProductPanel.parentQty = SearchProduct.active.product.qtyonhand;
				SearchProduct.createInstanceProductPanel.displayButtons();
			}*/
		}
};

var SearchProductFilter = {
		data : null,
		selection : null,
		query : null,		
		
		setSelectionQuery : function(query){
			this.query = query;
			this.applySelection();
		},
				
		applySelection : function(){
			
			/*TODO: check network*/			
			
			var filterQuery = this.query;
			
			if (filterQuery.indexOf('=') != -1)
			{
				var find = '=';
				var re = new RegExp(find, 'g');
				
				filterQuery = filterQuery.replace(re, '=\'');
			}
			
			if (filterQuery.indexOf(';') != -1)
			{
				var find = ';';
				var re = new RegExp(find, 'g');
				
				filterQuery = filterQuery.replace(re, '\';');
			}
			
			filterQuery = filterQuery + ';' + 'm_product.m_product_parent_id is null';
			
			var url = "ProductAction.do";
			
			/*
			var param = new Hash();
			
			param.set('action', 'search');			
			param.set('searchTerm', '');
			param.set('isSoTrx', SearchProduct.isSoTrx);
			param.set('bpPricelistVersionId', SearchProduct.bpPricelistVersionId);
			param.set('warehouseId', SearchProduct.warehouseId);
			param.set('filterQuery', filterQuery);
			param.set('ignoreTerminalPricelist', SearchProduct.ignoreTerminalPricelist);
			
			param = param.toQueryString();
			
			var myAjax = new Ajax.Request( url, 
			{ 
				method: 'get', 
				parameters: param, 
				onSuccess: function (response) {
					//update info
					var json = response.responseText.evalJSON(true);
					SearchProduct.render(json);
				}
			});
			*/
			var data = {
				'action' : 'search',		
				'searchTerm' : '',
				'isSoTrx' : SearchProduct.isSoTrx,
				'bpPricelistVersionId' : SearchProduct.bpPricelistVersionId,
				'warehouseId' : SearchProduct.warehouseId,
				'filterQuery' : filterQuery,
				'ignoreTerminalPricelist' : SearchProduct.ignoreTerminalPricelist
			};
			
			jQuery.ajax({
				
				type: "POST",
				url: url,
				data: data,
				success: (data, status, xhr) => {
					SearchProduct.render(data);
				},
				error: (xhr, data, status) => {
					
					if(xhr.status === 0){
						status = "Server is unreachable!";
					}

					BootstrapDialog.show({
						closable: true,
						type: BootstrapDialog.TYPE_DANGER,
						title: 'Error',
						message: status,
						buttons: [{
							label: 'OK',
							action: function(dialog) {
								dialog.close();
							}
						}]
					});
				}
			});			
			
		}
};
