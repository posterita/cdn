/**
 * Call ShoppingCartAction 
 * Available actions 
 * 1. getCart - no param 
 * 2. addToCart - param1 productId, param2 qty 
 * 3. clearCart 
 * 4. removeFromCart - param1 productId 
 * 5. incrementQty - param1 productId, param2 qty
 * 6. decrementQty - param1 productId, param2 qty
 */
function reportShoppingCartReferencingError()
{
	alert("Please use ShoppingCart Class instead!");
}

function reportShoppingCartError()
{
	console.warn('Operation on cart FAILED!');
}

var ShoppingCart = Class.create( {
	initialize : function(orderType) {
		this.orderType = orderType;
		this.isSoTrx = true;
		this.bpId = null;
		this.pricelistId = null;
		this.lastUpdatedProductId = 0;
		this.orderId = 0;
		this.lines = null;
		this.container = null;
		this.subTotal = 0;
		this.grandTotal = 0;
		this.taxTotal = 0;
		this.qtyTotal = 0;
		this.discountOnTotal = 0;
		this.upsellTotal = 0;
		this.selectedIndex = -1;
		this.active = false;
		this.currencySymbol = '$';
		this.URL = 'ShoppingCartAction.do';	
		this.requestCounter = 0;
		this.initializeShortcuts();
		
		this.callbacks = jQuery.Callbacks();
	},
	setContainer : function(container) {
		this.container = container;
	},
	updateCart : function(url){

		/*TODO: check network*/	

		this.dfd = jQuery.Deferred();

		LOGGER.info('Cart requesting --> ' + url);
		this.requestCounter ++;

		jQuery.ajax({
			type: "GET",
			url: url,
			success: (data, status, xhr) => {

				this.requestCounter --;
				
				let responseText = xhr.responseText;

				if(responseText.startsWith("{")){
					var json = JSON.parse(responseText);

					if(json.error && json.error == true){

						if(json.reason == 'session.timeout'){

							BootstrapDialog.show({
								closable: false,
								type: BootstrapDialog.TYPE_DANGER,
								title: 'Session Expired',
								message: 'Your session has expired. Please login again.',
								buttons: [{
									label: 'OK',
									action: function(dialog) {
										window.location.href = "select-user.do";
									}
								}]
							});

						}
						else {
							BootstrapDialog.show({
								closable: true,
								type: BootstrapDialog.TYPE_DANGER,
								title: 'Error',
								message: json.reason,
								buttons: [{
									label: 'OK',
									action: function(dialog) {
										dialog.close();
									}
								}]
							});
						}

						return;
					}
				}
				else {
					jQuery('#' + this.container).html(data);
				}

			},
			error: function(xhr, status, error) {
				// handle error
				
				if(xhr.status === 0){
					error = "Server is unreachable!";
				}
				
				BootstrapDialog.show({
					closable: true,
					type: BootstrapDialog.TYPE_DANGER,
					title: 'Error',
					message: error,
					buttons: [{
						label: 'OK',
						action: function(dialog) {
							dialog.close();
						}
					}]
				});
			}
		});



		/*
		new Ajax.Updater(
				this.container,
				url,
				{
					evalScripts:true,
					method: "post",
					onSuccess:	this.successNotifier.bind(this),
					onComplete:	this.completeNotifier.bind(this),
					onFailure:	this.failureNotifier.bind(this),
					onException: this.exceptionNotifier.bind(this)
				}
			);
		 */

		return this.dfd.promise();
	},
	successNotifier : function(response) {
		/**/ 
		console.info('Operation on cart succeded');
		this.requestCounter --;
		this.dfd.resolve(response);
		
		this.callbacks.fire("success-notifier");
	},
	completeNotifier : function(response) {
		/**/ 
		console.info('Operation on cart completed');
		$('shopping-cart-outer-container').style.display = 'block';
		this.requestCounter --;
		this.dfd.resolve(response);
	},
	failureNotifier : function(response) {
		/**/ 
		console.warn('Operation on cart FAILED!');
		this.requestCounter --;
		this.dfd.reject(response);
	},
	exceptionNotifier : function(response) {
		/**/ 
		console.error('Operation on cart FAILED TO REQUEST!');
		this.requestCounter --;
		this.dfd.reject(response);
	},
	getCart : function() {
		/*refresh cart, call cart renderer ShoppingCartAction-->getCart */
		var url = this.URL + "?action=getCart&orderType=" + this.orderType;
		return this.updateCart(url);
	},
	clearCart : function() {
		this.lastUpdatedProductId = 0;
		this.lastUpdatedLineId = 0;
		this.selectedIndex = -1;
		var url = this.URL + "?action=clearCart&orderType=" + this.orderType + "&date=" + DateUtil.getCurrentDate();
		return this.updateCart(url);	 
	},
	
	addToCart : function(productId, qty, description, price, modifiers, warehouseId, manualEntry, attributesetInstanceId ) {
		/*this.lastUpdatedLineId = productId;*/
		var url = this.URL + "?action=addToCart&orderType=" + this.orderType + "&productId=" + productId + "&qty=" + qty 
			+ "&date=" + DateUtil.getCurrentDate();
		
		if(description) url = url + '&description=' + description;
		if(price) url = url + '&price=' + price;
		
		if(attributesetInstanceId) url = url + '&attributesetInstanceId=' + attributesetInstanceId;
		
		/* see order-screen.jsp line 1239 */
		if(modifiers){
		
			url = url + '&modifiers=';
			for(var i=0; i<modifiers.length; i++){
				if(i>0) url = url + ',';
				url = url + modifiers[i].modifierId;
			}
		}
		
		if(warehouseId) url = url + '&warehouseId=' + warehouseId;
		
		if(manualEntry){
			url = url + '&manualEntry=' + manualEntry;
		}
		
		return this.updateCart(url);
		
	},
	removeFromCart : function(lineId) {
		var url = this.URL + "?action=removeFromCart&orderType=" + this.orderType + "&lineId=" + lineId + "&date=" + DateUtil.getCurrentDate();
		return this.updateCart(url);	 
	},
	incrementQty : function(lineId) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=incrementQty&orderType=" + this.orderType + "&lineId=" + lineId;
		return this.updateCart(url);	 
	},
	decrementQty : function(lineId) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=decrementQty&orderType=" + this.orderType + "&lineId=" + lineId;
		return this.updateCart(url);	 
	},
	updateQty : function(lineId, qty, warehouseId) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=updateQty&orderType=" + this.orderType + "&lineId=" + lineId + "&qty=" + qty + "&date=" + DateUtil.getCurrentDate();
		if (warehouseId) url = url + "&warehouseId=" + warehouseId;
		return this.updateCart(url);	 
	},
	splitLines : function(lineId, qty) {
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=splitLines&orderType=" + this.orderType + "&lineId=" + lineId + "&qty=" + qty;
		return this.updateCart(url);	 
	},
	setDiscountOnLine : function(lineId, amt){
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=setDiscountOnLine&orderType=" + this.orderType + "&lineId=" + lineId + "&discountAmt=" + amt;
		return this.updateCart(url);	
	},
	setDiscountOnTotal : function(amt){
		var url = this.URL + "?action=setDiscountOnTotal&orderType=" + this.orderType + "&discountAmt=" + amt;
		return this.updateCart(url);	
	},
	setTax : function(lineId, taxId){
		this.lastUpdatedLineId = lineId;
		var url = this.URL + "?action=setTax&orderType=" + this.orderType + "&lineId=" + lineId + "&taxId=" + taxId;
		return this.updateCart(url);	
	},
	setBp : function(bpId){
		this.bpId = bpId;
		var url = this.URL + "?action=setBPartner&orderType=" + this.orderType + "&bpartnerId=" + bpId;
		return this.updateCart(url);	
	},
	setPriceList : function(pricelistId){
		this.pricelistId = pricelistId;
		var url = this.URL + "?action=setPriceList&orderType=" + this.orderType + "&pricelistId=" + pricelistId;
		return this.updateCart(url);	
	},
	resetDiscounts : function(){
		var url = this.URL + "?action=resetDiscounts&orderType=" + this.orderType;
		return this.updateCart(url);	
	},
	applyDiscountCode : function(lineId, discountCodeId){
		var url = this.URL + "?action=applyDiscountCode&orderType=" + this.orderType + "&lineId=" + lineId + "&discountCodeId=" + discountCodeId;
		return this.updateCart(url);	
	},
	
	resetLineDiscountCode: function(lineId){
		var url = this.URL + "?action=resetLineDiscountCode&orderType=" + this.orderType + "&lineId=" + lineId;
		return this.updateCart(url);	
	},
	
	setCurrentBoxNumber : function( boxNumber ){
		
		var url = this.URL + "?action=setCurrentBoxNumber&orderType=" + this.orderType + "&currentBoxNumber=" + encodeURIComponent(boxNumber);
		return this.updateCart(url);
	},
	
	addBehaviourToLines : function(){
		/*custom parser*/
		LOGGER.info('Adding behaviour to cart lines');
		
		if (this.isEmpty())
		{
			jQuery('#order-actions-button-container').hide();
			jQuery('#shopping-cart-column').hide();
			jQuery('#more-button').hide();
			jQuery('#empty-cart-button-container').show();
			
			jQuery('.disabled-when-cart-empty').attr("disabled", "disabled");
		}
		else
		{
			jQuery('#empty-cart-button-container').hide();
			jQuery('#shopping-cart-column').show();
			jQuery('#order-actions-button-container').show();
			jQuery('#more-button').show();
			
			jQuery('.disabled-when-cart-empty').removeAttr("disabled");
		}
		
		if(this.isEmpty())
		{
			if(ShoppingCartManager.quantityTextfield){
				ShoppingCartManager.quantityTextfield.value = '';
			}
			
			this.onChange();
			return;
		}
		
		if(this.selectedIndex < 0 
				|| this.selectedIndex == this.lines.length
				|| this.lines[this.selectedIndex].lineId != this.lastUpdatedLineId){
			this.selectedIndex = (this.lines.length - 1);
		}
						
		
		for(var i=0; i<this.lines.length; i++){
			var line = this.lines[i];
			var row = $('row' + (i+1));
			
			if(line.lineId == this.lastUpdatedLineId){
				this.selectedIndex = i;
			}
			
			if(row){
				line.element = row;
				row.onclick = this.setSelectedIndex.bind(this,i);
			}			
		}	
		
		this.renderLines();
	},
	setSelectedIndex : function(){
		var index = arguments[0];		
		this.selectedIndex = index;
		
		/*update qty textfield*/
		var currentLine  = this.lines[this.selectedIndex];
		var qty = currentLine.qty;		
		ShoppingCartManager.quantityTextfield.value = qty;
		
		
		this.renderLines();	
	},
	
	renderLines : function(){
		/*highlight active row*/
		for(var i=0; i<this.lines.length; i++){
			var currentLine = this.lines[i];
			
			var className = ((i%2 == 0) ? 'row even' : 'row odd');
			
			if(this.selectedIndex == i){
				var highlightColor = 'row shopping-cart-highlight';
				className = highlightColor;	
			}	
			
			var element = $('row' + (i+1));			
			if(!element) continue;
			
			element.className = className;
			
		}/*for*/ 		
		
		/*update qty textfield*/
		var currentLine  = this.lines[this.selectedIndex];
		var qty = currentLine.qty;	
		if(ShoppingCartManager.quantityTextfield){
			ShoppingCartManager.quantityTextfield.value = qty;
			
			//shoppingCartChangeNotifier();		
			this.onChange();
			
			ShoppingCartManager.quantityTextfield.select();
		}
	},
	isEmpty : function(){
		return (this.lines == null || this.lines.length == 0);
	},
	initializeShortcuts : function(){
		/*Add shortcut keys to shopping cart*/
		/*
		 * CTRL+UP move up
		 * CTRL+DOWN move down
		*/
		if (typeof shortcut != "undefined") {
			shortcut.add("Ctrl+Up", this.moveUp.bind(this));
			shortcut.add("Ctrl+Down", this.moveDown.bind(this));
		}
		
	},
	moveDown : function(){		
		if(this.selectedIndex < (this.lines.length - 1))
		{
			this.selectedIndex ++;
			this.renderLines();
		}
	},
	moveUp : function(){
		if(this.selectedIndex > 0)
		{
			this.selectedIndex --;
			this.renderLines();
		}
	},
	onChange : function(){
		alert(Translation.translate('onchange.not.initialized','onChange not initialized'));
	},
	
	onUpdate : function(fn){
		this.callbacks.add(fn);
	}
});


var ShoppingCartManager = {
		/* loop lines for products that needs age verification */
		needAgeVerification : function(){
			var cart = this.getCart();
			var lines = cart.lines;
			
			if(lines && lines.length > 0){
				for(var i=0; i<lines.length; i++){
					var line = lines[i];
					if(line.isAgeverified == true){
						return true;
					}
				}
			}			
			
			return false;
		},
		
		getCart : function(){
			return shoppingCart;
		},
		
		refreshCart : function(){
			this.getCart().getCart();
		},
		
		clearCart : function(){
			this.getCart().clearCart();
		},
		
		getOrderId : function(){
			return this.getCart().orderId;
		},
		
		getOrderType : function(){
			return this.getCart().orderType;
		},
		
		getLineId : function(){
			var lineId = null;
			
			var cart = this.getCart();
			
			if(!this.isShoppingCartEmpty()){
				lineId = cart.lines[cart.selectedIndex].lineId;
			}
			
			return lineId;
		},
		
		scrollUp : function(){
			this.getCart().moveUp();
		},
		
		scrollDown : function(){
			this.getCart().moveDown();
		},
		
		addToCart : function(productId){
			if(productId == null) return;			
			this.getCart().addToCart(productId,1);
		},
		
		incrementQty : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			var currentLine  = cart.lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
			
			/* check for gift card */
			if( currentLine.isGift && cart.isSoTrx ){
				alert(Translation.translate('operation.not.allowed.cannot.change.gift.card.quantity','Operation not allowed! Cannot change gift card quantity!'));
				return;
			}
			
			this.getCart().incrementQty(lineId);
		},
		
		decrementQty : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			this.getCart().decrementQty(lineId);
		},
		
		updateQty : function(qty){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			
			var currentLine  = cart.lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
			
			/* check for gift card */
			if( currentLine.isGift && cart.isSoTrx ){
				if(qty > 1){
					alert(Translation.translate('operation.not.allowed.cannot.change.gift.card.quantity','Operation not allowed! Cannot change gift card quantity!'));
					return;
				}
			}
			
			if(currentLine.qty != qty){ /*avoid server hits*/
				
				var select = jQuery('#warehouse-dropdown')
				if (select.length == 0){
					cart.updateQty(lineId, qty);
				} else {
					cart.updateQty(lineId, qty, select.val());
				}
				
			}
			
			/*cart.updateQty(lineId, qty);*/
		},
		
		splitLines : function(qty){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			var cart = this.getCart();
			
			var currentLine  = cart.lines[cart.selectedIndex];
			var lineId = currentLine.lineId;
						
			cart.splitLines(lineId, qty);
		},
		
		removeFromCart : function(){
			var lineId = this.getLineId();
			if(lineId == null) return;
			
			this.getCart().removeFromCart(lineId);
		},
		
		deleteLine : function(lineId){
			if(lineId == null) return;
			
			if(window.confirm('Do you want to delete line?')){
				this.getCart().removeFromCart(lineId);
			}
			
			
		},
		
		setLineTax : function(taxId){
			var lineId = this.getLineId();
			if(lineId != null && taxId != null) this.getCart().setTax(lineId, taxId);
		},
		
		setBp : function(bpId){
			return this.getCart().setBp(bpId);
		},
		
		getGrandTotal : function(){
			return this.getCart().grandTotal;
		},
		
		getSubTotal : function(){
			return this.getCart().subTotal;
		},
		
		getDiscountOnTotal : function(){
			return this.getCart().discountOnTotal;
		},
		
		getTaxTotal : function(){
			return this.getCart().taxTotal;
		},
		
		getUpsellTotal:function(){
			return this.getCart().upsellTotal;
		},
		
		getLineNetTotal : function(){
			var lineNetTotal = ShoppingCartManager.getSubTotal() + ShoppingCartManager.getTaxTotal();
			return lineNetTotal;
		},
		
		getQtyTotal : function(){
			return this.getCart().qtyTotal;
		},
		
		getCurrencySymbol : function(){
			return this.getCart().currencySymbol;
		},
		
		isShoppingCartEmpty : function(){
			return this.getCart().isEmpty();
		},
		
		isShoppingCartReady : function(){
			return (this.getCart().requestCounter <= 0); /*negative count bug*/
		},
		
		setDiscountOnTotal : function(discountAmt){			
			this.getCart().setDiscountOnTotal(discountAmt);
		},
		
		setDiscountOnLine : function(discountAmt){
			var cart = this.getCart();
			
			if(cart.lines && cart.lines.length > 0){
				var lineId = cart.lines[cart.selectedIndex].lineId;
				cart.setDiscountOnLine(lineId,discountAmt);
			}
		},
		
		applyDiscountCode : function( discountCodeId ){
			
			var cart = this.getCart();
			
			if(cart.lines && cart.lines.length > 0){
				var lineId = cart.lines[cart.selectedIndex].lineId;
				cart.applyDiscountCode(lineId, discountCodeId);
			}
			
		},
		
		resetLineDiscountCode : function(){
			
			var cart = this.getCart();
			
			if(cart.lines && cart.lines.length > 0){
				var lineId = cart.lines[cart.selectedIndex].lineId;
				cart.resetLineDiscountCode(lineId);
			}
			
		},
		
		resetDiscounts : function(){
			var cart = this.getCart();
			cart.resetDiscounts();
		},
		
		initializeComponents : function(){
			
			/*this.scrollUpButton = $('scroll-up-button');
			this.scrollDownButton = $('scroll-down-button');*/
			this.clearButton = $('clear-button');
			/*this.addButton = $('add-button');
			this.decreaseButton = $('decrease-button');*/
			this.removeButton = $('remove-button');
			this.productInfoButton = $('product-info-button');
			this.splitLineButton = $('split-line-button');
			
			this.quantityTextfield = $('quantity-texfield');
			
			/* add behaviour */	
			/*this.scrollUpButton.onclick = function(e){
				ShoppingCartManager.scrollUp();
			};
			
			this.scrollDownButton.onclick = function(e){
				ShoppingCartManager.scrollDown();
			};*/
			
			this.clearButton.onclick = function(e){
				ShoppingCartManager.clearCart();
			};
			
			/*this.addButton.onclick = function(e){
				ShoppingCartManager.incrementQty();
			};*/
			
			/*this.decreaseButton.onclick = function(e){
				ShoppingCartManager.decrementQty();
			};*/
			
			if(this.removeButton){
				this.removeButton.onclick = function(e){
					ShoppingCartManager.removeFromCart();
				};
			}
			
			if(this.productInfoButton){				
				this.productInfoButton.onclick = function(e){
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert(Translation.translate('cart.is.empty','Cart is empty!'));
						return;
					}
					
					var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
					var productId = currentLine.productId;
					
					new ProductInfoPanel().getInfo(productId);
				};
			}
			
			if(this.splitLineButton){
				this.splitLineButton.onclick = function(e){
					if(ShoppingCartManager.isShoppingCartEmpty()){
						alert(Translation.translate('cart.is.empty','Cart is empty!'));
						return;
					}
						
					var currentLine  = shoppingCart.lines[shoppingCart.selectedIndex];
					var currentLineQty = currentLine.qty;
					
					if(currentLineQty == 1) return;
						
					var qty = window.prompt('Enter qty', '1');
					qty = parseFloat(qty);
					
					if(isNaN(qty)){
						alert(Translation.translate('invalid.qty','Invalid Qty!'));
						return;					
					}
					
					if(qty >= currentLineQty){
						alert('Qty entered must be less than ' + currentLineQty + '!');						
						return;	
					}
					
					ShoppingCartManager.splitLines(qty);	
				};
			}
			
			/* add keypad to qty textfield */
			/*Event.observe(this.quantityTextfield,'click',Keypad.clickHandler.bindAsEventListener(Keypad),false);*/
			
			/* bug fix for updateQtyTextField */
			this.quantityTextfield.onkeyup = function(e){
				if(e.keyCode == Event.KEY_RETURN){
					if(!ShoppingCartManager.isShoppingCartEmpty()){
						var qty = parseFloat(this.value);
						
						if(isNaN(qty)){
							alert(Translation.translate('invalid.qty','Invalid Qty!'));
							this.selectAll();								
							return;					
						}
						ShoppingCartManager.updateQty(qty);							
					}
				}
			}
		}
		
};
