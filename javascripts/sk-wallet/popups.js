var SKWallet = {
		   
		userId : null,
		
		paymentId : null,
		
	   checkInCustomersList : [],
	   
	   checkInCustomersMap : [],
	   
	   showCheckInCustomersDialog : function(){
		   if(this.checkInCustomersDialog == null){
			   var dlg = jQuery("<div id='skwallet-checkin-dialog'><div id='skwallet-logo'><p><img src='images/sk-wallet-logo.png' align='left'><h2>SK Wallet</h2><h3>Checked-In Customers</h3></p></div></div>");
			   
			   var refreshBtn = jQuery("<button id='skwallet-refresh-btn'>Refresh</button>");
			   refreshBtn.appendTo(dlg);
			   
			   var searchContainer = jQuery("<div id='skwallet-search-container'></div>");
			   searchContainer.appendTo(dlg);
			   
			   var searchbox = jQuery("<input id='skwallet-searchbox' type='search' placeholder='Search customer'>");
			   searchbox.appendTo(searchContainer);			   
			   
			   var customersContainer = jQuery("<div id='skwallet-customers-container'>Loading ... </div>");
			   customersContainer.appendTo(dlg);				   
			   
			   /* Refresh Button */			   
			   refreshBtn.on("click", function(event){
				   SKWallet.getCheckedInCustomersList();
			   });
			   
			   /* Search Box */
			   jQuery(searchbox).autocomplete({
				   source: [],
				   appendTo: searchContainer,
				   response: function( event, ui ) {
					   
					   jQuery("#skwallet-customers-container").html("");
					   
					   var keypairs = ui.content;
					   
					   var list = [];
					   
					   for(var i=0; i < keypairs.length; i++)
					   {		   
						   var id = keypairs[i].value;							   
						   var user = SKWallet.checkInCustomersMap[id];
						   
						   list.push(user);
					   }
					   
					   SKWallet.renderCheckInCustomersList(list);
				   },
				   select: function( event, ui ) {
					   var item = ui.item;
					   var userId = item.value;
					   
					   /* on customer selection */
					   SKWallet.customerSelected(userId);
				   }
				});
			   
			   this.checkInCustomersDialog = dlg;			   
		   }
		   
		   /*request customers list*/
		   this.getCheckedInCustomersList();
		   
		   jQuery(this.checkInCustomersDialog).dialog({width:"auto", height:"auto", modal: true, autoOpen: true, position: ['center',20] });		   
		   
	   },
	   
	   getCheckedInCustomersList : function(){
		   /* get checked-in customers */
		   var container = jQuery("#skwallet-customers-container");
		   container.html("Loading ... ");
		   
		   jQuery.getJSON( "SKWalletAction.do?action=getUsers", function(data) {
			   var checkIns = data.payload.checkinUsers;
			   			   
			   /* lookup by id using map */
			   var map = [];
			   var searchboxSource = [];
			   
			   for(var i=0; i < checkIns.length; i++){
				   var user = checkIns[i];
				   map[user.userId] = user;
				   
				   searchboxSource.push({label: user.firstName + " " + user.lastName, value: user.userId});
			   }					   
			   
			   SKWallet.checkInCustomersMap = map;					   
			   SKWallet.checkInCustomersList = checkIns;						   
			   SKWallet.renderCheckInCustomersList(checkIns);
			   
			   jQuery("#skwallet-searchbox").autocomplete( "option", "source", searchboxSource);
			   
				if(checkIns.length == 0){
					container.html("No customers found!");			   
				}
		   });
	   },
	   
	   renderCheckInCustomersList : function(list){			   
		   
		   var checkInList = list || this.checkInCustomersList;
		   
		   var container = jQuery("#skwallet-customers-container");
		   container.html("");
		   
		   var ul = jQuery("<ul></ul>");
		   ul.appendTo(container);
		   
		   /* compute ul width */
		   
		   ul.width( (150 + 10) * checkInList.length);
		   
		   for(var i=0; i < checkInList.length; i++)
		   {
			   var user = checkInList[i];
			   
			   var img = jQuery("<img>");
			   
			   img.attr("src", user.profilePicture);
			   img.attr("width", "25px");
			   
			   var li = jQuery("<li></li>");
			   li.css("cursor","pointer");
			   
			   li.appendTo(ul);			   

			   img.appendTo(li);
			   li.on("click", {user : user}, function(event){
				   /* on customer selection */
				   SKWallet.customerSelected(event.data.user.userId);
			   });
			   
			   var div = jQuery("<div></div>");
			   div.html(user.firstName + " " + user.lastName);
			   
			   div.appendTo(li);			   
		   }		  		   
		   
	   },
	   
	   customerSelected : function(userId){
		   SKWallet.userId = userId;
		   jQuery.getJSON( "SKWalletAction.do?action=startPurchase&userId=" + userId, function(json, textStatus , xhr) {
			   SKWallet.paymentId = -1;
			   
			   var status = xhr.status;
			   if(status == 403){
				   alert('Failed to process payment! 403 - Access Denied');
			   }
			   
			   if(status ==500){
				   alert('Failed to process payment! 500 - Internal Server Error');
			   }
			   	
			   
			   var dlg = SKWallet.checkInCustomersDialog;
			   jQuery(dlg).dialog( "close" );
			   
			   if(status != 200){
				   return;
			   }
			   
			   SKWallet.paymentId = json.payload.purchase.id;			   
			   SKWallet.showPaymentConfirmationDialog();
			   
			   var userId = SKWallet.userId;		   
			   var user = SKWallet.checkInCustomersMap[userId];
			   
			   jQuery("#skwallet-confirmation-dialog-avatar").attr("src", user.profilePicture);
			   jQuery("#skwallet-confirmation-dialog-avatar").attr("width", "25px");
			   
			   jQuery("#skwallet-confirmation-dialog-customer-name").html(user.firstName + " " + user.lastName);
			   jQuery('#skwallet-confirmation-dialog-change-customer-link').on("click", function(event){
				   /*hide confirmation*/
				   var dlg = SKWallet.confirmationDialog;
				   jQuery(dlg).dialog( "close" );
				   /*reset cart*/
				   var cart = ShoppingCartManager.getCart();
				   cart.successNotifier = function(response){
					   /*load chekins*/
					   SKWallet.showCheckInCustomersDialog();
				   };
				   cart.failureNotifier = function(response){
					   alert("Failed to clear previous discounts");
				   }
				   
				   cart.resetDiscounts()
				   
			   });
			   
			   /*Get ShoppingCart reference*/
			   var currency = ShoppingCartManager.getCurrencySymbol() + " ";
			   
			   jQuery("#skwallet-confirmation-dialog-subtotal").html(currency + new Number(ShoppingCartManager.getSubTotal()).toFixed(2));
			   jQuery("#skwallet-confirmation-dialog-discount-description").html("0%");
			   jQuery("#skwallet-confirmation-dialog-discount").html(currency + new Number(0).toFixed(2));
			   jQuery("#skwallet-confirmation-dialog-tax").html(currency + new Number(ShoppingCartManager.getTaxTotal()).toFixed(2));
			   jQuery("#skwallet-confirmation-dialog-total").html(currency + new Number(ShoppingCartManager.getGrandTotal()).toFixed(2));
			   jQuery("#skwallet-confirmation-dialog-confirm-btn").on("click", function(event){
				   if(!confirm("Confirm payment?")){
					   return;
				   }
				   
				   /*process payment*/
				   if(window.OrderScreen){
						var OrderScreen = window.OrderScreen;					
						OrderScreen.setTenderType('SK Wallet');
						
						var payment = new Hash();
						payment.set("otk", SKWallet.paymentId);
						OrderScreen.payment = payment;
						
						OrderScreen.setSKWalletPayment();
						OrderScreen.checkout();
				  }else
				  {
					  var payment = new Hash();
					  payment.set("otk", SKWallet.paymentId);
					  payment.set("skwalletAmt", SKWallet.paymentId);
				  }
			   });
			   
		   });
	   },
	   
	   showPaymentConfirmationDialog : function(){		  
		   
		   if(this.confirmationDialog == null){
			   var dlg = jQuery('<div id="skwallet-confirmation-dialog" title="SK Wallet - Confirmation">' +
						   '<img id="skwallet-confirmation-dialog-avatar">' +
						   '<div id="skwallet-confirmation-dialog-customer-name">Fistname Lastname</div>' +
						   '<a href="javascript:void(0);" id="skwallet-confirmation-dialog-change-customer-link">Change Customer</a>' +
						   '<div id="skwallet-confirmation-dialog-payment-details">' +
						   		'<div class="subtotal">' + 
						   		'<div class="float-left">Order Subtotal</div><div class="float-right" id="skwallet-confirmation-dialog-subtotal">$ 25.00</div>' +
						   		'</div>' +
						   		'<div class="clear-both"></div>' +
						   		'<div class="float-left">Discount applied:</div>' +
						   		'<div class="clear-both"></div>' +
						   		'<div class="float-left" id="skwallet-confirmation-dialog-discount-description">20% off trans</div><div class="float-right" id="skwallet-confirmation-dialog-discount">- $5.00</div>' +
						   		'<div class="clear-both"></div>' +
						   		'<div class="float-left">Tax</div><div class="float-right" id="skwallet-confirmation-dialog-tax">$1.50</div>' +
						   		'<div class="clear-both"></div>' +
						   		'<div class="total">' + 
						   		'<div class="float-left">Order Total</div><div class="float-right" id="skwallet-confirmation-dialog-total">$21.50</div>' +
						   		'</div>' +
						   		'<div class="clear-both"></div>' +
						   		'<button id="skwallet-confirmation-dialog-confirm-btn">CONFIRM TO PAY</button>' +
						   '</div>' +
						   '<div class="clear-both"></div>' +
					   '</div>');			  
			   
			   this.confirmationDialog = dlg;
		   }
		   
		   jQuery(this.confirmationDialog).dialog({width:"340px", height:"auto", modal: true, autoOpen: true, position: ['center',20] });
		   
	   }
};