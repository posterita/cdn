var SubscriptionPopUpPanel = Class.create(PopUpPanel, {
	initialize : function($super, popupId, trigger)
	{
		$super(popupId, trigger);
		this.subscriptionGrid = null;
	},
	
	setSubscriptionGrid : function(subscriptionGrid)
	{
		this.subscriptionGrid = subscriptionGrid;
	},
	
	afterOk : function()
	{
		this.subscriptionGrid.loadBtns();
	},
	
	afterConfirmOk : function()
	{
		this.subscriptionGrid.loadBtns();
	}
	
});

var UpgradeComponentsPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('upgrade.components.panel', trigger);
		this.upgradeConfirmPanel = null;
		
		this.terminalPriceElement = $('chargify.upgrade.components.terminal.price');
		this.storePriceElement = $('chargify.upgrade.components.store.price');
		this.supportPriceElement = $('chargify.upgrade.components.support.price');
		this.supportContainer = $('chargify-upgrade-components-support-container');
		this.storeContainer = $('chargify-upgrade-components-store-container');
		this.terminalContainer = $('chargify-upgrade-components-terminal-container');
		this.panelContainer = $('chargify-upgrade-components-panel-body');
		this.terminalTotalElement = $('chargify.upgrade.components.terminal.price.total');
		this.storeTotalElement = $('chargify.upgrade.components.store.price.total');
		this.terminalInput = $('upgrade.components.panel.additional.terminals');
		this.storeInput = $('upgrade.components.panel.additional.stores');
		this.storeCountElement = $('chargify.upgrade.components.total.store');
		this.terminalCountElement = $('chargify.upgrade.components.total.terminal');
		
		this.terminalInput.onkeyup = this.updateTerminalTotal.bind(this);
		this.storeInput.onkeyup = this.updateStoreTotal.bind(this);
	},
	
	updateTerminalTotal : function()
	{
		var price = parseFloat(this.terminalPrice);
		var noOfTerminals = parseFloat(this.terminalInput.value);
		
		this.terminalTotalElement.innerHTML = '= $' + (price * noOfTerminals).toFixed(2) + (this.isYearly ? '/yr': '/mo');
		this.terminalCountElement.innerHTML = this.terminalCount + noOfTerminals;
	},
	
	updateStoreTotal : function()
	{
		var price = parseFloat(this.storePrice);
		var noOfStores = parseFloat(this.storeInput.value);
		
		this.storeTotalElement.innerHTML = '= $' + (price * noOfStores).toFixed(2) + (this.isYearly ? '/yr': '/mo');
		this.storeCountElement.innerHTML = this.storeCount + noOfStores;
		
	},
	
	initBtns : function()
	{
		this.okBtn = $('upgrade.components.panel.okBtn');
		this.cancelBtn = $('upgrade.components.panel.cancelBtn');
	},
	
	/*initOnCall : function()
	{
		this.show();
	},*/
	
	afterCall : function(json)
	{
		this.isYearly = json.isyearly;
		this.terminalPrice = json.terminalprice;
		this.storePrice = json.storeprice;
		this.supportPrice = json.supportprice;
		this.storeCount = json.storecount;
		this.terminalCount = json.terminalcount;
		
		this.terminalPriceElement.innerHTML = this.terminalPrice + (this.isYearly ? '/yr': '/mo');
		this.storePriceElement.innerHTML = this.storePrice + (this.isYearly ? '/yr': '/mo');
		this.supportPriceElement.innerHTML = this.supportPrice + (this.isYearly ? '/yr per store': '/mo per store');
		
		this.storeCountElement.innerHTML = this.storeCount;
		this.terminalCountElement.innerHTML = this.terminalCount;
		
		if (json.accounttype == 'Start Up')
		{
			this.storeContainer.style.display = 'none';
			this.terminalContainer.style.display = 'none';
			
			if (json.issupportincluded == 0)
			{
				this.supportContainer.style.display = 'block';
			}
			else
			{
				this.panelContainer.innerHTML = 'Nothing to Upgrade';
			}
		}
		
		if (json.accounttype == 'Professional')
		{
			this.storeContainer.style.display = 'block';
			this.terminalContainer.style.display = 'block';
			
			if (json.issupportincluded == 0)
			{
				this.supportContainer.style.display = 'block';
			}
			else
			{
				this.supportContainer.style.display = 'none';
			}
		}
		
		var storeTotal = parseInt(this.storeCount) + 1;
		
		$('upgrade.components.panel.support.total').innerHTML = '('+storeTotal+' X $' + this.supportPrice + ')';
		
		var accountType = $('subscription.summary.accounttype').innerHTML;
		if (accountType == 'Cash Register')
		{
			$('upgrade.components.panel.additional.stores.container').style.display = 'none';
		}
	},
	
	getParameters : function()
	{
		var params = new Hash();
		var noOfAdditionalStores = $('upgrade.components.panel.additional.stores').value;
		var noOfAdditionalTerminals = $('upgrade.components.panel.additional.terminals').value;
		var isSupportSelected = $('upgrade.components.panel.support').checked;
		
		params.set('noOfAdditionalStores', noOfAdditionalStores);
		params.set('noOfAdditionalTerminals', noOfAdditionalTerminals);
		params.set('issupportselected', isSupportSelected);
		
		return params.toJSON();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			alert(json.errorMsg);
		}
		else if (json.SubscriptionState == 'Active')
		{
			this.upgradeConfirmPanel = new UpgradeConfirmPanel(json);
			this.upgradeConfirmPanel.okBtn.onclick = this.confirmOk.bind(this);
			this.upgradeConfirmPanel.cancelBtn.onclick = this.confirmCancel.bind(this);
			this.upgradeConfirmPanel.closeBtn.onclick = this.confirmCancel.bind(this);
			this.upgradeConfirmPanel.show();
		}
		else if (json.SubscriptionState == 'Trialing')
		{
			$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
			$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
			$('subscription.Support').checked = json.support;
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			this.subscriptionGrid.loadBtns();
		}
	},
	
	confirmOk : function()
	{
		this.upgradeConfirmPanel.hide();
		this.initOnConfirmOk();
	},
	
	confirmCancel : function()
	{
		this.upgradeConfirmPanel.hide();
		this.initOnConfirmCancel();
	},
	
	afterConfirmOk : function(json)
	{
		var noOfStores = json.NoOfStores;
		var noOfTerminals = json.NoOfTerminals;
		var currentPeriodStart = json.CurrentPeriodStart;
		var currentPeriodEnd = json.CurrentPeriodEnd;
		
		$('subscription.NoOfStoresText').innerHTML = noOfStores;
		$('subscription.NoOfTerminalsText').innerHTML = noOfTerminals;
		$('subscription.Support').checked = json.support;
		//$('subscription.CurrentPeriodStartText').innerHTML = currentPeriodStart;
		$('subscription.CurrentPeriodEndText').innerHTML = currentPeriodEnd;
		$('nextbillingamt').innerHTML = json.nextbillingamt;
		
		$('success-msg-box').style.display = 'block';
		$('SuccessMessages').innerHTML = json.msg;
		this.subscriptionGrid.loadBtns();
	},
	
	afterConfirmCancel : function(json)
	{}
});

var UpgradeConfirmPanel = Class.create(PopUpBase, {
	
	initialize : function(json)
	{
		this.createPopUp($('upgrade.confirm.panel'));
		
		this.previewUpgrade = json.previewUpgrade;
		this.balance = parseFloat(this.previewUpgrade.balance_in_cents) / 100;
		this.migration = this.previewUpgrade.migration;
		this.prorated_adjustment = parseFloat(this.migration.prorated_adjustment_in_cents) / 100;
		this.charge = parseFloat(this.migration.charge_in_cents) / 100;
		this.payment_due = parseFloat(this.migration.payment_due_in_cents) / 100;
		this.credit_applied = parseFloat(this.migration.credit_applied_in_cents) / 100;
		
		this.text = 'Your current balance is $' + this.balance + '.<br>';
		this.text += 'Upgrading will have the following effects : <ul>'; 
		this.text += '<li>A prorated credit of approximately $' + this.prorated_adjustment + ' will be applied. </li>';
		this.text += '<li>A full charge of $' + this.charge + ' will be applied. </li>';		
		this.text += '<li>The payment amount will be approximately $'+ this.payment_due+ '.</li>';
		this.text += '<li>The period start date will be reset to today.</li></ul>';
		
		this.okBtn = $('upgrade.confirm.okBtn');
		this.cancelBtn = $('upgrade.confirm.cancelBtn');
		this.closeBtn = $('upgrade.confirm.closeBtn');
		
		$('upgrade.confirm.content').innerHTML = this.text;
	}
});

var DowngradeComponentsPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('downgrade.components.panel', trigger);
		this.downgradeConfirmPanel = null;
		//this.cashRegisterBtn = null;
		
		
		this.terminalPriceElement = $('chargify.downgrade.components.terminal.price');
		this.storePriceElement = $('chargify.downgrade.components.store.price');
		this.supportPriceElement = $('chargify.downgrade.components.support.price');
		this.supportContainer = $('chargify-downgrade-components-support-container');
		this.storeContainer = $('downgrade.components.panel.remove.stores.container');
		this.terminalContainer = $('chargify-downgrade-components-terminal-container');
		this.panelContainer = $('chargify-downgrade-components-panel-body');
		this.terminalTotalElement = $('chargify.downgrade.components.terminal.price.total');
		this.storeTotalElement = $('chargify.downgrade.components.store.price.total');
		this.terminalInput = $('downgrade.components.panel.remove.terminals');
		this.storeInput = $('downgrade.components.panel.remove.stores');
		this.storeCountElement = $('chargify.downgrade.components.total.store');
		this.terminalCountElement = $('chargify.downgrade.components.total.terminal');
		
		this.terminalInput.onkeyup = this.updateTerminalTotal.bind(this);
		this.storeInput.onkeyup = this.updateStoreTotal.bind(this);
	},
	
	updateTerminalTotal : function()
	{
		var price = parseFloat(this.terminalPrice);
		var noOfTerminals = parseFloat(this.terminalInput.value);
		
		this.terminalTotalElement.innerHTML = '= $-' + (price * noOfTerminals).toFixed(2) + (this.isYearly ? '/yr': '/mo');
		this.terminalCountElement.innerHTML = this.terminalCount + noOfTerminals;
	},
	
	updateStoreTotal : function()
	{
		var price = parseFloat(this.storePrice);
		var noOfStores = parseFloat(this.storeInput.value);
		
		this.storeTotalElement.innerHTML = '= $-' + (price * noOfStores).toFixed(2) + (this.isYearly ? '/yr': '/mo');
		this.storeCountElement.innerHTML = this.storeCount + noOfStores;
		
	},
	
	initBtns : function()
	{
		this.okBtn = $('downgrade.components.panel.okBtn');
		this.cancelBtn = $('downgrade.components.panel.cancelBtn');
		//this.cashRegisterBtn = $('downgrade.components.panel.cashRegisterBtn');
		

		var accountType = $('subscription.summary.accounttype').innerHTML;
		if (accountType == 'Cash Register')
		{
			//this.cashRegisterBtn.style.display = 'none';
			$('downgrade.components.panel.remove.stores.container').style.display = 'none';
		}
		else
		{
			//this.cashRegisterBtn.onclick = this.downgradeToCashRegister.bind(this);
		}
		
	},
	
	/*downgradeToCashRegister : function()
	{
		this.hide();
		
		var url = 'ChargifyAction.do';
		var pars = 'action=downgradeToCashRegister'
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.afterDowngradeCash.bind(this)
		});	
	},
	
	afterDowngradeCash : function(response)
	{
		var json = eval ('(' + response.responseText + ')');
		if (!json.error)
		{
			$('subscription.summary.accounttype').innerHTML = json.AccountType;
			$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
			$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
			$('subscription.SubscriptionStateText').innerHTML = json.SubscriptionState;
			$('msg-box').innerHTML = json.msg;
			
			this.subscriptionGrid.loadBtns();
		}
		else
		{
			alert(json.errorMsg);
		}
	},*/
	
	/*initOnCall : function()
	{
		this.show();
	},*/
	
	afterCall : function(json)
	{
		this.isYearly = json.isyearly;
		this.terminalPrice = json.terminalprice;
		this.storePrice = json.storeprice;
		this.supportPrice = json.supportprice;
		this.storeCount = json.storecount;
		this.terminalCount = json.terminalcount;
		
		this.terminalPriceElement.innerHTML = this.terminalPrice + (this.isYearly ? '/yr': '/mo');
		this.storePriceElement.innerHTML = this.storePrice + (this.isYearly ? '/yr': '/mo');
		this.supportPriceElement.innerHTML = this.supportPrice + (this.isYearly ? '/yr per store': '/mo per store');
		
		this.storeCountElement.innerHTML = this.storeCount;
		this.terminalCountElement.innerHTML = this.terminalCount;
		
		if (this.terminalCount == 0 && this.storeCount == 0 && !json.issupportincluded)
		{
			this.panelContainer.innerHTML = 'Nothing to downgrade';
		}
		
		if (json.accounttype == 'Start Up')
		{
			this.storeContainer.style.display = 'none';
			this.terminalContainer.style.display = 'none';
			
			if (json.issupportincluded > 0)
			{
				this.supportContainer.style.display = 'block';
			}
			else
			{
				this.panelContainer.innerHTML = 'Nothing to downgrade';
			}
		}
		
		if (json.accounttype == 'Professional')
		{
			if (this.terminalCount != 0)
			{
				this.terminalContainer.style.display = 'block';
			}
			else
			{
				this.terminalContainer.style.display = 'none';
			}
			
			if (this.storeCount != 0)
			{
				this.storeContainer.style.display = 'block';
			}
			else
			{
				this.storeContainer.style.display = 'none';
			}
			
			if (json.issupportincluded > 0)
			{
				this.supportContainer.style.display = 'block';
			}
			else
			{
				this.supportContainer.style.display = 'none';
			}
		}
		
		var storeTotal = parseInt(this.storeCount) + 1;
		
		if ($('downgrade.components.panel.support.total') != null)
		{
			$('downgrade.components.panel.support.total').innerHTML = '('+storeTotal+' X $' + this.supportPrice + ')';
		}
	},
	
	getParameters : function()
	{
		var params = new Hash();
		var noOfStoresToRemove = $('downgrade.components.panel.remove.stores').value;
		var noOfTerminalsToRemove = $('downgrade.components.panel.remove.terminals').value;
		var isSupportSelected = $('downgrade.components.panel.remove.support').checked;
		
		params.set('noOfStoresToRemove', noOfStoresToRemove);
		params.set('noOfTerminalsToRemove', noOfTerminalsToRemove);
		params.set('isSupportSelected', isSupportSelected);
		
		
		return params.toJSON();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			alert(json.errorMsg);
		}
		else if (json.SubscriptionState == 'Active')
		{
			this.downgradeConfirmPanel = new DowngradeConfirmPanel(json);
			this.downgradeConfirmPanel.okBtn.onclick = this.confirmOk.bind(this);
			this.downgradeConfirmPanel.cancelBtn.onclick = this.confirmCancel.bind(this);
			//this.downgradeConfirmPanel.closeBtn.onclick = this.confirmCancel.bind(this);
			this.downgradeConfirmPanel.show();
		}
		else if (json.SubscriptionState = 'Trialing')
		{
			$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
			$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
			$('subscription.Support').checked =  json.support;
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			
			this.subscriptionGrid.loadBtns();
		}
	},
	
	confirmOk : function()
	{
		this.downgradeConfirmPanel.hide();
		this.initOnConfirmOk();
	},
	
	confirmCancel : function()
	{
		this.downgradeConfirmPanel.hide();
		this.initOnConfirmCancel();
	},
	
	afterConfirmOk : function(json)
	{
		$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
		$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
		$('subscription.Support').checked =  json.support;
		//$('subscription.CurrentPeriodStartText').innerHTML = json.CurrentPeriodStart;
		$('subscription.CurrentPeriodEndText').innerHTML = json.CurrentPeriodEnd;
		$('nextbillingamt').innerHTML = json.nextbillingamt;
		$('success-msg-box').style.display = 'block';
		$('SuccessMessages').innerHTML = json.msg;
		
		this.subscriptionGrid.loadBtns();
	},
	
	afterConfirmCancel : function(json)
	{}
});

var DowngradeConfirmPanel = Class.create(PopUpBase, {
	
	initialize : function(json)
	{
		this.createPopUp($('downgrade.confirm.panel'));
		
		this.previewUpgrade = json.previewUpgrade;
		this.balance = parseFloat(this.previewUpgrade.balance_in_cents) / 100;
		this.migration = this.previewUpgrade.migration;
		this.prorated_adjustment = parseFloat(this.migration.prorated_adjustment_in_cents) / 100;
		this.charge = parseFloat(this.migration.charge_in_cents) / 100;
		this.payment_due = parseFloat(this.migration.payment_due_in_cents) / 100;
		this.credit_applied = parseFloat(this.migration.credit_applied_in_cents) / 100;
		
		this.text = 'Your current balance is $' + this.balance + '.<br>';
		this.text += 'Downgrading will have the following effects : <ul>'; 
		this.text += '<li>A prorated credit of approximately $' + this.prorated_adjustment + ' will be applied. </li>';
		this.text += '<li>A full charge of $' + this.charge + ' will be applied. </li>';		
		this.text += '<li>The payment amount will be approximately $'+ this.payment_due+ '.</li>';
		this.text += '<li>The period start date will be reset to today.</li></ul>';
		
		this.okBtn = $('downgrade.confirm.okBtn');
		this.cancelBtn = $('downgrade.confirm.cancelBtn');
		this.closeBtn = $('downgrade.confirm.closeBtn');
		
		$('downgrade.confirm.content').innerHTML = this.text;
	}
});

var DowngradeToStartUpPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('downgrade.to.start.up.panel', trigger);
		this.downgradeToStartUpConfirmPanel = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('downgrade.to.start.up.panel.okBtn');
		this.cancelBtn = $('downgrade.to.start.up.panel.cancelBtn');
		

		var accountType = $('subscription.summary.accounttype').innerHTML;
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	getParameters : function()
	{
		return new Hash().toJSON();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			alert(json.errorMsg);
		}
		else if (json.SubscriptionState == 'Active')
		{
			this.downgradeToStartUpConfirmPanel = new DowngradeToStartUpConfirmPanel(json);
			this.downgradeToStartUpConfirmPanel.okBtn.onclick = this.confirmOk.bind(this);
			this.downgradeToStartUpConfirmPanel.cancelBtn.onclick = this.confirmCancel.bind(this);
			this.downgradeToStartUpConfirmPanel.closeBtn.onclick = this.confirmCancel.bind(this);
			this.downgradeToStartUpConfirmPanel.show();
		}
		else if (json.SubscriptionState = 'Trialing')
		{
			$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
			$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			
			this.subscriptionGrid.loadBtns();
		}
	},
	
	confirmOk : function()
	{
		this.downgradeToStartUpConfirmPanel.hide();
		this.initOnConfirmOk();
	},
	
	confirmCancel : function()
	{
		this.downgradeToStartUpConfirmPanel.hide();
		this.initOnConfirmCancel();
	},
	
	afterConfirmOk : function(json)
	{
		$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
		$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
		$('subscription.Support').checked = json.support;
		//$('subscription.CurrentPeriodStartText').innerHTML = json.CurrentPeriodStart;
		$('subscription.CurrentPeriodEndText').innerHTML = json.CurrentPeriodEnd;
		$('nextbillingamt').innerHTML = json.nextbillingamt;
		$('success-msg-box').style.display = 'block';
		$('SuccessMessages').innerHTML = json.msg;
		$('subscription.summary.accounttype').innerHTML = json.AccountType;
		
		this.subscriptionGrid.loadBtns();
	},
	
	afterConfirmCancel : function(json)
	{}
});

var DowngradeToStartUpConfirmPanel = Class.create(PopUpBase, {
	
	initialize : function(json)
	{
		this.createPopUp($('downgrade.to.start.up.confirm.panel'));
		
		this.previewDowngrade = json.previewDowngrade;
		this.balance = parseFloat(this.previewDowngrade.balance_in_cents) / 100;
		this.migration = this.previewDowngrade.migration;
		this.prorated_adjustment = parseFloat(this.migration.prorated_adjustment_in_cents) / 100;
		this.charge = parseFloat(this.migration.charge_in_cents) / 100;
		this.payment_due = parseFloat(this.migration.payment_due_in_cents) / 100;
		this.credit_applied = parseFloat(this.migration.credit_applied_in_cents) / 100;
		
		this.text = 'Your current balance is $' + this.balance + '.<br>';
		this.text += 'Downgrading will have the following effects : <ul>'; 
		this.text += '<li>A prorated credit of approximately $' + this.prorated_adjustment + ' will be applied. </li>';
		this.text += '<li>A full charge of $' + this.charge + ' will be applied. </li>';		
		this.text += '<li>The payment amount will be approximately $'+ this.payment_due+ '.</li>';
		this.text += '<li>The period start date will be reset to today.</li></ul>';
		
		this.okBtn = $('downgrade.to.start.up.confirm.okBtn');
		this.cancelBtn = $('downgrade.to.start.up.confirm.cancelBtn');
		this.closeBtn = $('downgrade.to.start.up.confirm.closeBtn');
		
		$('downgrade.to.start.up.confirm.content').innerHTML = this.text;
	}
});

var DowngradeToCashRegisterPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('downgrade.to.cash.register.panel', trigger);
		this.downgradeToCashRegisterConfirmPanel = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('downgrade.to.cash.register.panel.okBtn');
		this.cancelBtn = $('downgrade.to.cash.register.panel.cancelBtn');
		

		var accountType = $('subscription.summary.accounttype').innerHTML;
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	getParameters : function()
	{
		return new Hash().toJSON();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			alert(json.errorMsg);
		}
		else if (json.SubscriptionState == 'Active')
		{
			this.downgradeToCashRegisterConfirmPanel = new DowngradeToCashRegisterConfirmPanel(json);
			this.downgradeToCashRegisterConfirmPanel.okBtn.onclick = this.confirmOk.bind(this);
			this.downgradeToCashRegisterConfirmPanel.cancelBtn.onclick = this.confirmCancel.bind(this);
			this.downgradeToCashRegisterConfirmPanel.closeBtn.onclick = this.confirmCancel.bind(this);
			this.downgradeToCashRegisterConfirmPanel.show();
		}
		else if (json.SubscriptionState = 'Trialing')
		{
			$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
			$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			
			this.subscriptionGrid.loadBtns();
		}
	},
	
	confirmOk : function()
	{
		this.downgradeToCashRegisterConfirmPanel.hide();
		this.initOnConfirmOk();
	},
	
	confirmCancel : function()
	{
		this.downgradeToCashRegisterConfirmPanel.hide();
		this.initOnConfirmCancel();
	},
	
	afterConfirmOk : function(json)
	{
		$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
		$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
		$('subscription.Support').checked = json.support;
		//$('subscription.CurrentPeriodStartText').innerHTML = json.CurrentPeriodStart;
		$('subscription.CurrentPeriodEndText').innerHTML = json.CurrentPeriodEnd;
		$('nextbillingamt').innerHTML = json.nextbillingamt;
		$('success-msg-box').style.display = 'block';
		$('SuccessMessages').innerHTML = json.msg;
		$('subscription.summary.accounttype').innerHTML = json.AccountType;
		
		this.subscriptionGrid.loadBtns();
	},
	
	afterConfirmCancel : function(json)
	{}
});

var DowngradeToCashRegisterConfirmPanel = Class.create(PopUpBase, {
	
	initialize : function(json)
	{
		this.createPopUp($('downgrade.to.cash.register.confirm.panel'));
		
		this.previewDowngrade = json.previewDowngrade;
		this.balance = parseFloat(this.previewDowngrade.balance_in_cents) / 100;
		this.migration = this.previewDowngrade.migration;
		this.prorated_adjustment = parseFloat(this.migration.prorated_adjustment_in_cents) / 100;
		this.charge = parseFloat(this.migration.charge_in_cents) / 100;
		this.payment_due = parseFloat(this.migration.payment_due_in_cents) / 100;
		this.credit_applied = parseFloat(this.migration.credit_applied_in_cents) / 100;
		
		this.text = 'Your current balance is $' + this.balance + '.<br>';
		this.text += 'Downgrading will have the following effects : <ul>'; 
		this.text += '<li>A prorated credit of approximately $' + this.prorated_adjustment + ' will be applied. </li>';
		this.text += '<li>A full charge of $' + this.charge + ' will be applied. </li>';		
		this.text += '<li>The payment amount will be approximately $'+ this.payment_due+ '.</li>';
		this.text += '<li>The period start date will be reset to today.</li></ul>';
		
		this.okBtn = $('downgrade.to.cash.register.confirm.okBtn');
		this.cancelBtn = $('downgrade.to.cash.register.confirm.cancelBtn');
		this.closeBtn = $('downgrade.to.cash.register.confirm.closeBtn');
		
		$('downgrade.to.cash.register.confirm.content').innerHTML = this.text;
	}
});

var CloseAccountPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('close.account.panel', trigger);
		this.closeBtn = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('close.account.okBtn');
		this.cancelBtn = $('close.account.cancelBtn');
		this.closeBtn = $('close.account.closeBtn');
		this.closeBtn.onclick = this.hide.bind(this);
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			alert(json.errorMsg);
		}
		else
		{
			$('subscription.SubscriptionStateText').innerHTML = json.SubscriptionState;
			//$('subscription.CurrentPeriodStartText').innerHTML = '';
			$('subscription.CurrentPeriodEndText').innerHTML = '';
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			this.subscriptionGrid.loadBtns();
		}	
	},
	
	getParameters : function()
	{
		var hash = new Hash();
		hash.set('reason', $('close.account.reason').value);
		
		return hash.toJSON();
	}
});

var ReactivateAccountPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('reactivate.account.panel', trigger);
		this.closeBtn = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('reactivate.account.okBtn');
		this.cancelBtn = $('reactivate.account.cancelBtn');
		this.closeBtn = $('reactivate.account.closeBtn');
		this.closeBtn.onclick = this.hide.bind(this);
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			alert(json.errorMsg);
		}
		else
		{
			$('subscription.summary.accounttype').innerHTML = json.AccountType;
			//$('subscription.CurrentPeriodStartText').innerHTML = json.CurrentPeriodStart;
			$('subscription.CurrentPeriodEndText').innerHTML = json.CurrentPeriodEnd;
			$('subscription.SubscriptionStateText').innerHTML = json.SubscriptionState;
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			this.subscriptionGrid.loadBtns();
		}
	}
});

var SubscriptionGrid = Class.create(PopUpGrid,{
	
	initialize : function($super, controller)
	{
		$super(controller);
		this.subscriptionController = this.controller;
	},
	
	addPanel : function($super, panel)
	{
		panel.setSubscriptionGrid(this);
		$super(panel);
	},
	
	initActions : function(upgradeAccountUrl, updatePaymentUrl)
	{
		/*$('upgradeAccount').onclick = function(e)
		{
			var firstName = $('subscription.Name').value;
			var lastName = $('subscription.Name2').value;
			
			var url = upgradeAccountUrl + "?first_name=" + escape(firstName) + 
									"&last_name=" + escape(lastName);
			top.location.href = url;
		};*/
		
		$('updatePaymentDetails').onclick = function(e)
		{
			top.location.href = updatePaymentUrl;
		}
		
		/*$('help-btn-container').style.display = 'block';
		$('downgrade-btn-container').style.display = 'none';
		$('more-btn').style.display = 'block';*/
		
		/*$('more-btn').onclick = function(e)
		{
			if ($('downgrade-btn-container').style.display == 'none')
			{
				$('help-btn-container').style.display = 'none';
				$('downgrade-btn-container').style.display = 'block';
				$('more-btn-label').innerHTML = 'Less';
			}
			else
			{
				$('downgrade-btn-container').style.display = 'none';
				$('help-btn-container').style.display = 'block';
				$('more-btn-label').innerHTML = 'More';
			}
		}*/
	},
	
	controllerSaved : function(controllerId)
	{
		this.loadBtns();
	},
	
	loadBtns : function()
	{
		var status = $('subscription.SubscriptionStateText').innerHTML;
		
		if (status == 'Canceled')
		{
			$('nextbillingamt').innerHTML = '';
			$('subscription.CurrentPeriodEnd').innerHTML = '';
			this.loadCanceledSubscriptionBtns();
		}
		else
		{
			var accountType = $('subscription.summary.accounttype').innerHTML;
			
			if (accountType == 'Cash Register')
			{
				this.loadCashRegisterBtns();
			}
			else if (accountType == 'Professional')
			{
				this.loadProfessionalBtns();
			}
			else if (accountType == 'Start Up')
			{
				this.loadStartUpBtns();
			}
			else
			{
				this.loadFreeBtns();
			}
		}
		
		$('new').style.display = 'none';
		//$('cancel').style.display = 'none';
		$('delete').style.display = 'none';
		//$('back-report').style.display = 'none';
		$('deactivate').style.display = 'none'; 
		$('activate').style.display = 'none';
		//$('uploadImage').style.display = 'block';
		
		setPlanMaxDetails(accountType);
		
		$('subscription.Support').style.display = 'none';
		
		if ($('subscription.Support').checked)
		{
			$('support.text').innerHTML = 'Yes';
		}
		else
		{
			$('support.text').innerHTML = 'No';
		}
		
		
		$('subscription.IsYearly').style.display = 'none';
		
		if ($('subscription.IsYearly').checked)
		{
			$('yearly.text').innerHTML = 'Yes';
		}
		else
		{
			$('yearly.text').innerHTML = 'No';
		}
	},
	
	loadProfessionalBtns : function()
	{
		$('UpgradeAccount').style.display = 'none';
		$('ReactivateAccount').style.display = 'none';
		
		$('updatePaymentDetails').style.display = 'block';
		$('UpgradeComponents').style.display = 'block';
		$('DowngradeComponents').style.display = 'block';
		$('CloseAccount').style.display = 'block';
		$('DowngradeAccount').style.display = 'block';
		
		var isSupportIncluded = $('subscription.Support').checked;
		
		/*if (isSupportIncluded)
		{
			jQuery('#AddRemoveSupport').on('click', function(){
				jQuery('#DowngradeComponents').click();
			});
		}
		else
		{
			jQuery('#AddRemoveSupport').on('click', function(){
				jQuery('#UpgradeComponents').click();
			});
		}*/
	},
	
	loadStartUpBtns : function()
	{
		var isSupportIncluded = $('subscription.Support').checked;
		
		/*if (isSupportIncluded)
		{
			jQuery('#AddRemoveSupport').on('click', function(){
				jQuery('#DowngradeComponents').click();
			});
		}
		else
		{
			jQuery('#AddRemoveSupport').on('click', function(){
				jQuery('#UpgradeComponents').click();
			});
		}*/
		
		$('UpgradeAccount').style.display = 'block';
		$('ReactivateAccount').style.display = 'none';
		
		$('updatePaymentDetails').style.display = 'block';
		
		$('UpgradeComponents').style.display = 'block';
		$('DowngradeComponents').style.display = 'block';
		
		$('CloseAccount').style.display = 'block';
		$('DowngradeAccount').style.display = 'none';
		
	},
	
	loadCanceledSubscriptionBtns : function()
	{
		$('ReactivateAccount').style.display = 'block';
		$('updatePaymentDetails').style.display = 'block';
		
		$('UpgradeComponents').style.display = 'none';
		$('DowngradeComponents').style.display = 'none';
		$('CloseAccount').style.display = 'none';
		$('UpgradeAccount').style.display = 'none';
		$('DowngradeAccount').style.display = 'none';
		$('AddRemoveSupport').style.display = 'none';
	},
	
	loadCashRegisterBtns : function()
	{
		$('right-content-buttons-container').style.height = '60px';
		$('UpgradeAccount').style.display = 'block';
		
		$('updatePaymentDetails').style.display = 'block';
		$('UpgradeComponents').style.display = 'block';
		$('DowngradeComponents').style.display = 'block';
		$('CloseAccount').style.display = 'block';
		$('ReactivateAccount').style.display = 'none';
		//$('DowngradeToCashRegister').style.display = 'none';
	},
	
	loadFreeBtns : function()
	{
		$('ReactivateAccount').style.display = 'none';
		$('updatePaymentDetails').style.display = 'none';
		$('UpgradeComponents').style.display = 'none';
		$('DowngradeComponents').style.display = 'none';
		$('CloseAccount').style.display = 'none';
		$('UpgradeAccount').style.display = 'none';
		$('DowngradeAccount').style.display = 'none';
		$('AddRemoveSupport').style.display = 'none';
	}
});

var UpgradeCashRegisterPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('upgrade.cash.register.panel', trigger);
		this.upgradeCashRegisterConfirmPanel = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('upgrade.cash.register.panel.okBtn');
		this.cancelBtn = $('upgrade.cash.register.panel.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	getParameters : function()
	{
		var params = new Hash();
		return params.toJSON();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			this.hide();
			alert(json.msg);
		}
		else if (json.SubscriptionState == 'Active')
		{
			this.upgradeCashRegisterConfirmPanel = new UpgradeCashRegisterConfirmPanel(json);
			this.upgradeCashRegisterConfirmPanel.okBtn.onclick = this.confirmOk.bind(this);
			this.upgradeCashRegisterConfirmPanel.cancelBtn.onclick = this.confirmCancel.bind(this);
			this.upgradeCashRegisterConfirmPanel.closeBtn.onclick = this.confirmCancel.bind(this);
			this.upgradeCashRegisterConfirmPanel.show();
		}
		else if (json.SubscriptionState == 'Trialing')
		{
			$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
			$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			this.subscriptionGrid.loadBtns();
		}
	},
	
	confirmOk : function()
	{
		this.upgradeCashRegisterConfirmPanel.hide();
		this.initOnConfirmOk();
	},
	
	confirmCancel : function()
	{
		this.upgradeCashRegisterConfirmPanel.hide();
		this.initOnConfirmCancel();
	},
	
	afterConfirmOk : function(json)
	{
		var noOfStores = json.NoOfStores;
		var noOfTerminals = json.NoOfTerminals;
		var currentPeriodStart = json.CurrentPeriodStart;
		var currentPeriodEnd = json.CurrentPeriodEnd;
		
		$('subscription.NoOfStoresText').innerHTML = noOfStores;
		$('subscription.NoOfTerminalsText').innerHTML = noOfTerminals;
		$('subscription.Support').checked = json.support;
		//$('subscription.CurrentPeriodStartText').innerHTML = currentPeriodStart;
		$('subscription.CurrentPeriodEndText').innerHTML = currentPeriodEnd;
		$('nextbillingamt').innerHTML = json.nextbillingamt;
		
		$('success-msg-box').style.display = 'block';
		$('SuccessMessages').innerHTML = json.msg;
		$('subscription.summary.accounttype').innerHTML = json.AccountType;
		
		this.subscriptionGrid.loadBtns();
	},
	
	afterConfirmCancel : function(json)
	{}
});


var UpgradeCashRegisterConfirmPanel = Class.create(PopUpBase, {
	
	initialize : function(json)
	{
		this.createPopUp($('upgrade.cash.register.confirm.panel'));
		
		this.previewUpgrade = json.previewUpgrade;
		this.balance = parseFloat(this.previewUpgrade.balance_in_cents) / 100;
		this.migration = this.previewUpgrade.migration;
		this.prorated_adjustment = parseFloat(this.migration.prorated_adjustment_in_cents) / 100;
		this.charge = parseFloat(this.migration.charge_in_cents) / 100;
		this.payment_due = parseFloat(this.migration.payment_due_in_cents) / 100;
		this.credit_applied = parseFloat(this.migration.credit_applied_in_cents) / 100;
		
		this.text = 'Your current balance is $' + this.balance + '.<br>';
		this.text += 'Upgrading will have the following effects : <ul>'; 
		this.text += '<li>A prorated credit of approximately $' + this.prorated_adjustment + ' will be applied. </li>';
		this.text += '<li>A full charge of $' + this.charge + ' will be applied. </li>';		
		this.text += '<li>The payment amount will be approximately $'+ this.payment_due+ '.</li>';
		this.text += '<li>The period start date will be reset to today.</li></ul>';
		
		this.okBtn = $('upgrade.cash.register.confirm.okBtn');
		this.cancelBtn = $('upgrade.cash.register.confirm.cancelBtn');
		this.closeBtn = $('upgrade.cash.register.confirm.closeBtn');
		
		$('upgrade.cash.register.confirm.content').innerHTML = this.text;
	}
});

var UpgradeToProfessionalPanel = Class.create(SubscriptionPopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('upgrade.to.professional.panel', trigger);
		this.upgradeToProfessionalConfirmPanel = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('upgrade.to.professional.panel.okBtn');
		this.cancelBtn = $('upgrade.to.professional.panel.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	getParameters : function()
	{
		var params = new Hash();
		return params.toJSON();
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error == null) return;
		
		if (error)
		{
			this.hide();
			alert(json.msg);
		}
		else if (json.SubscriptionState == 'Active')
		{
			this.upgradeToProfessionalConfirmPanel = new UpgradeToProfessionalConfirmPanel(json);
			this.upgradeToProfessionalConfirmPanel.okBtn.onclick = this.confirmOk.bind(this);
			this.upgradeToProfessionalConfirmPanel.cancelBtn.onclick = this.confirmCancel.bind(this);
			this.upgradeToProfessionalConfirmPanel.closeBtn.onclick = this.confirmCancel.bind(this);
			this.upgradeToProfessionalConfirmPanel.show();
		}
		else if (json.SubscriptionState == 'Trialing')
		{
			$('subscription.NoOfStoresText').innerHTML = json.NoOfStores;
			$('subscription.NoOfTerminalsText').innerHTML = json.NoOfTerminals;
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.msg;
			this.subscriptionGrid.loadBtns();
		}
	},
	
	confirmOk : function()
	{
		this.upgradeToProfessionalConfirmPanel.hide();
		this.initOnConfirmOk();
	},
	
	confirmCancel : function()
	{
		this.upgradeToProfessionalConfirmPanel.hide();
		this.initOnConfirmCancel();
	},
	
	afterConfirmOk : function(json)
	{
		var noOfStores = json.NoOfStores;
		var noOfTerminals = json.NoOfTerminals;
		var currentPeriodStart = json.CurrentPeriodStart;
		var currentPeriodEnd = json.CurrentPeriodEnd;
		
		$('subscription.NoOfStoresText').innerHTML = noOfStores;
		$('subscription.NoOfTerminalsText').innerHTML = noOfTerminals;
		$('subscription.Support').checked = json.support;
		//$('subscription.CurrentPeriodStartText').innerHTML = currentPeriodStart;
		$('subscription.CurrentPeriodEndText').innerHTML = currentPeriodEnd;
		$('nextbillingamt').innerHTML = json.nextbillingamt;
		
		$('success-msg-box').style.display = 'block';
		$('SuccessMessages').innerHTML = json.msg;
		$('subscription.summary.accounttype').innerHTML = json.AccountType;
		
		
		this.subscriptionGrid.loadBtns();
	},
	
	afterConfirmCancel : function(json)
	{}
});


var UpgradeToProfessionalConfirmPanel = Class.create(PopUpBase, {
	
	initialize : function(json)
	{
		this.createPopUp($('upgrade.to.professional.confirm.panel'));
		
		this.previewUpgrade = json.previewUpgrade;
		this.balance = parseFloat(this.previewUpgrade.balance_in_cents) / 100;
		this.migration = this.previewUpgrade.migration;
		this.prorated_adjustment = parseFloat(this.migration.prorated_adjustment_in_cents) / 100;
		this.charge = parseFloat(this.migration.charge_in_cents) / 100;
		this.payment_due = parseFloat(this.migration.payment_due_in_cents) / 100;
		this.credit_applied = parseFloat(this.migration.credit_applied_in_cents) / 100;
		
		this.text = 'Your current balance is $' + this.balance + '.<br>';
		this.text += 'Upgrading will have the following effects : <ul>'; 
		this.text += '<li>A prorated credit of approximately $' + this.prorated_adjustment + ' will be applied. </li>';
		this.text += '<li>A full charge of $' + this.charge + ' will be applied. </li>';		
		this.text += '<li>The payment amount will be approximately $'+ this.payment_due+ '.</li>';
		this.text += '<li>The period start date will be reset to today.</li></ul>';
		
		this.okBtn = $('upgrade.to.professional.confirm.okBtn');
		this.cancelBtn = $('upgrade.to.professional.confirm.cancelBtn');
		this.closeBtn = $('upgrade.to.professional.confirm.closeBtn');
		
		$('upgrade.to.professional.confirm.content').innerHTML = this.text;
	}
});

