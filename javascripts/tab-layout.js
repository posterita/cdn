var TabLayout = Class.create({
	initialize : function()
	{
		this.tabs = $$(".tab");
		this.onClickListener = null;
	},
	
	initActions : function()
	{
		for (var i=0; i<this.tabs.length; i++)
		{
			var id = this.tabs[i].id;
			$(id).onclick = this.enableTab.bind(this, id);
		}
	},
	
	setOnClickListener : function(listener)
	{
		this.onClickListener = listener;
	},

	setDefaultTab : function(id)
	{
		this.enableTab(id);
	},

	disableAllTabs : function()
	{
		for (var i=0; i<this.tabs.length; i++)
		{
			var id = this.tabs[i].id;
			this.deactivateTab(id);
		}
	},

	enableTab : function(id)
	{
		this.disableAllTabs();
		this.hideAllContents();
		this.activateTab(id);
		this.showContent('Content-' + id);
		
		if (this.onClickListener != null)
			this.onClickListener.tabClicked(id);
	},

	activateTab : function(id)
	{
		$(id).className = 'active';			
	},

	deactivateTab : function(id)
	{
		$(id).className = '';
	},

	hideAllContents : function()
	{
		for (var i=0; i<this.tabs.length; i++)
		{
			var id = this.tabs[i].id;
			this.hideContent('Content-' + id);
		}
	},

	showContent : function(contentId)
	{
		$(contentId).style.display = 'block';
	},

	hideContent : function(contentId)
	{
		$(contentId).style.display = 'none';
	}
});
