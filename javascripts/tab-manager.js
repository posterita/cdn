var TabManager = Class.create({
	
	initialize : function(controller)
	{
		this.controller = null;
		this.controllerClass = null;
		this.root = null;
		this.controllerId = 0;
		
		this.setController(controller);
		
		this.tabs = new ArrayList();
		this.tabHash = new Hash();
		
		this.activeTab = null;
		
		this.controllerFactory = new ControllerFactory();
		this.controllerFactory.setLoadListener(this);
		
		this.controllerActionListener = null;
		
		this.controllerGeneralHelp = $('help-content').innerHTML;
		
		this.popUpGrid = null;
	},
	
	setControllerActionListener : function(listener)
	{
		this.controllerActionListener = listener;
	},
	
	setPopUpGrid : function(popUpGrid)
	{
		this.popUpGrid = popUpGrid;
	},
	
	addTab : function(tab)
	{
		this.tabHash.set(tab.getTabId(), tab);
		this.tabs.add(tab);
	},
	
	setController : function(controller)
	{
		this.controller = controller;
		this.root = this.controller.getRoot();
		
		this.controllerClass = this.controller.getClassName()
		this.controller.addOnSaveListener(this);
		this.controller.addDeactivateListener(this);
		this.controller.addActivateListener(this);
		this.controller.addDeleteListener(this);
		
		this.controllerId = this.controller.getControllerId();
	},
	
	getController : function()
	{
		return this.controller;
	},
	
	init : function()
	{
		for (var i=0; i<this.tabs.size(); i++)
		{
			var tab = this.tabs.get(i);
			
			var className = this.root.getClassName();
			var tableId = this.root.getTableId();
			var id = this.root.getId();
			var productType = this.root.fields.array[14].value;
			
			tab.setControllerClass(this.controllerClass);
			tab.setModelClass(className);
			tab.setModelId(id);
			tab.setTableId(tableId);
			tab.setProductType(productType);
			tab.init();
		}
		
		if (this.controllerId != 0 && this.controller.isActive())
		{
			this.enableAllTabs();
		}
		else
		{
			this.disableAllTabs();
		}
	},
	
	disableAllTabs : function()
	{
		for (var i=0; i<this.tabs.size(); i++)
		{
			var tab = this.tabs.get(i);
			tab.disable();
		}
	},
	
	enableAllTabs : function()
	{
		for (var i=0; i<this.tabs.size(); i++)
		{
			var tab = this.tabs.get(i);
			
			tab.enable();
		};
	},
	
	tabClicked : function(id)
	{
		var tab = this.tabHash.get(id);
		if (tab != null)
		{
			tab.enable();
			tab.onCall();
		}
		else
		{
			$('help-content').innerHTML = this.controllerGeneralHelp;
		}
		
		this.activeTab = tab;
	},
	
	reloadActiveTab : function()
	{
		if (this.controller.getId() == 0)
		{
			var tabLayout = new TabLayout();
			tabLayout.setDefaultTab('Details');
			tabLayout.enableTab('Details');
		}
		else
		{
			if (this.activeTab != null)
			{
				this.activeTab.onCall();
			}
		}
	},
	
	saveAction : function()
	{
		if (this.activeTab != null)
		{
			this.activeTab.onOk();
		}
		else
		{
			this.controller.save();
		}
	},
	
	newAction : function()
	{
		this.controllerFactory.load(this.controllerClass, 0);		
	},
	
	activateAction : function()
	{
		this.controller.activate();
	},
	
	deactivateAction : function()
	{
		this.controller.deactivate();
	},
	
	deleteAction : function()
	{
		this.controller.deleteAction();
	},
	
	controllerLoaded : function(newController, summaryInfo)
	{
		this.reloadController(newController);
		summaryInfo.init();
		
		var name = null;
		
		if (newController.getClassName() == 'BankAccount')
		{
			name = $(newController.root.prefix + '.AccountNo').value;
		}
		else
		{
			name = $(newController.root.prefix + '.Name').value;
		}
		
		if (newController.getClassName() == 'ModifierGroup')
		{
			$('modifierGroup.IsMandatory').style.display="";
			$('modifierGroup.IsExclusive').style.display="";
		}
		
		if (newController.getControllerId() == 0)
			this.controllerActionListener.newActionComplete();
		else 
			this.controllerActionListener.loadActionComplete(name, newController.isActive());
		
	},
	
	controllerSaved : function(controllerId)
	{
		if (controllerId != 0)
		{
			for (var i=0; i<this.tabs.size(); i++)
			{
				var tab = this.tabs.get(i);
				tab.setModelId(controllerId);
				tab.enable();
			}
			
			this.controllerActionListener.saveActionComplete();
			/*this.controller.refreshTree();*/
		}
	},
	
	controllerDeactivated : function()
	{
		this.disableAllTabs();
		this.controllerActionListener.deactivateActionComplete();
	},
	
	controllerActivated : function()
	{
		this.enableAllTabs();
		this.controllerActionListener.activateActionComplete();
	},
	
	controllerDeleted : function()
	{
		this.disableAllTabs();
		this.controllerActionListener.deleteActionComplete();
	},
	
	reloadTab : function (id)
	{
		this.controllerFactory.load(this.controllerClass, id);
	},
	
	reloadController : function(newController)
	{
		var root = this.controller.root;
		var rootFields = root.getFields();
		for (var i=0; i<rootFields.size(); i++)
		{
			var field = rootFields.get(i);
			var displayId = field.displayId;
			
			var cfield = newController.getField(displayId);
			
			cfield.unique = field.unique;
			cfield.mandatory = field.mandatory;
			cfield.encrypted = field.encrypted;
			cfield.password = field.password;
			cfield.alphanumeric = field.alphanumeric;
			cfield.readOnly = field.readOnly;
			cfield.textOnly = field.textOnly;
			cfield.styleClass = field.styleClass;
			cfield.help = field.help
			cfield.defaultValue = field.defaultValue;
			cfield.callout = field.callout;
			cfield.updateable = field.updateable;
		}
		
		var interactives = this.controller.interactives;
		for (var i=0; i<interactives.size(); i++)
		{
			var interactive = interactives.get(i);
			var intFields = interactive.getFields();
			for (var j=0; j<intFields.size(); j++)
			{
				var jfield = rootFields.get(i);
				var displayId = jfield.displayId;
				
				var infield = newController.getField(displayId);
				infield.unique = jfield.unique;
				infield.mandatory = jfield.mandatory;
				infield.encrypted = jfield.encrypted;
				infield.password = jfield.password;
				infield.alphanumeric = jfield.alphanumeric;
				infield.readOnly = jfield.readOnly;
				infield.textOnly = jfield.textOnly;
				infield.styleClass = jfield.styleClass;
				infield.help = jfield.help
				infield.defaultValue = jfield.defaultValue;
				infield.callout = jfield.callout;
				infield.updateable = jfield.updateable;
			}
		}
		
		var refreshValuesOnly = false;
		newController.updatePage(refreshValuesOnly);
		newController.clearAllErrors();
		
		this.setController(newController);
		this.init();
		this.reloadActiveTab();
		
		if (this.popUpGrid != null)
			this.popUpGrid.reload(newController);
	}
});

var Tab = Class.create({
	
	initialize : function(tabId, className, divId)
	{
		this.tabId = tabId;
		this.className = className;
		this.divId = divId;
		
		this.controllerClass = null;
		this.tableId = 0;
		this.productType = 'I';
		this.modelClass = null;
		this.modelId = 0;
		
		this.generalHelp = '';
	},
	
	getGeneralHelp : function()
	{
		return this.generalHelp;
	},
	
	getTabId : function()
	{
		return this.tabId;
	},
	
	getProductType : function()
	{
		return this.productType;
	},
	
	setControllerClass : function(controllerClass)
	{
		this.controllerClass = controllerClass;
	},
	
	setTableId : function(tableId)
	{
		this.tableId = tableId;
	},
	
	setProductType : function(productType)
	{
		this.productType = productType;
	},
	
	setModelId : function(modelId)
	{
		this.modelId = modelId;
	},
	
	setModelClass : function(modelClass)
	{
		this.modelClass = modelClass;
	},
	
	init : function()
	{
	},
		
	disable : function()
	{
		$(this.tabId).style.display = 'none';
	},
	
	enable : function()
	{
		/*$(this.tabId).style.display = 'inline';
		
		if ($(this.divId) != null)
			$(this.divId).style.display = 'block';*/
		
		if (this.tabId == "ItemInventory")
		{
			var productType = this.getProductType();
			
			if (productType != 'S')
			{
				$(this.tabId).style.display = 'inline';
				
				if ($(this.divId) != null)
					$(this.divId).style.display = 'block';
			} else {
				$(this.tabId).style.display = 'none';
			};
		} else {
			$(this.tabId).style.display = 'inline';
			
			if ($(this.divId) != null)
				$(this.divId).style.display = 'block';
		};
	},
	
	onCall : function()
	{
		if (this.modelId != 0)
		{
			var url = 'ModelAction.do';
			var pars = 'action=popup&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId;
			
			var ajax = new Ajax.Request(url,
			{ 
				method: 'POST', 
				parameters: pars, 
				onComplete: this.callHandler.bind(this)
			});
		}
	},
	
	onOk : function()
	{
		var url = 'ModelAction.do';
		var pars = 'action=popupOk&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.okHandler.bind(this)
		});	
	},
	
	onConfirmOk : function()
	{
		var url = 'ModelAction.do';
		var pars = 'action=popupConfirmOk&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.confirmOkHandler.bind(this)
		});	
	},
	
	onConfirmCancel : function()
	{
		var url = 'ModelAction.do';
		var pars = 'action=popupConfirmCancel&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.confirmCancelHandler.bind(this)
		});	
	},
	
	onCancel : function()
	{
		this.afterCancel();
	},
	
	afterCancel : function()
	{},
	
	callHandler : function(request)
	{
		var json = eval('('+request.responseText+')');
		this.afterCall(json);
		$('help-content').innerHTML = this.generalHelp;
	},
	
	okHandler : function(request)
	{
		var response = request.responseText;
		var json = eval('('+request.responseText+')');
		this.afterOk(json);
		this.showStatus(json);
	},
	
	showStatus : function(json)
	{
		if (json.isError)
		{
			$('success-msg-box').style.display = 'none';
			$('error-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = '';
			$('ErrorMessages').innerHTML = json.message;
		}
		else
		{
			$('error-msg-box').style.display = 'none';
			$('ErrorMessages').innerHTML = ''
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.message;
		}
	},
	
	confirmOkHandler : function(request)
	{
		var response = request.responseText;
		var json = eval('('+request.responseText+')');
		this.afterConfirmOk(json);
	},
	
	confirmCancelHandler : function(request)
	{
		var response = request.responseText;
		var json = eval('('+request.responseText+')');
		this.afterConfirmCancel(json);
	},
	
	afterCall : function(json)
	{},
	
	afterOk : function(json)
	{
		if (json.isError)
		{
			$('success-msg-box').style.display = 'none';
			$('error-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = '';
			$('ErrorMessages').innerHTML = json.message;
		}
		else
		{
			$('error-msg-box').style.display = 'none';
			$('ErrorMessages').innerHTML = ''
			$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = json.message;
		}
	},
		
	
	afterConfirmOk : function(json)
	{},
	
	afterConfirmCancel : function(json)
	{},
	
	getParameters : function()
	{
		var hash = new Hash();
		return hash.toJSON();
	},
	
	beforeOk : function()
	{}
});
