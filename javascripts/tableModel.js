var Cell = Class.create({
	initialize : function(content)
	{
		this.styleClass = null;
		this.name = null;
		this.id = null;
		this.content = content;
	},
	
	setStyleClass : function(styleClass)
	{
		this.styleClass = styleClass;
	},
	
	getStyleClass : function()
	{
		return this.styleClass;
	},
	
	setName : function(name)
	{
		this.name = name;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	setId : function(id)
	{
		this.id = id;
	},
	
	getId : function()
	{
		return this.id;
	},
	
	setContent : function(content)
	{
		this.content = content;
	},
	
	getContent : function()
	{
		return this.content;
	},
	equals : function(cell)
	{
		return (this.getId() == cell.getId());
	}
});

var Column = Class.create({
	initialize : function()
	{
		this.styleClass = null;
		this.name = null;
		this.id = null;
		this.cells = new ArrayList(new Cell());
	},

	setStyleClass : function(styleClass)
	{
		this.styleClass = styleClass;
	},
	
	getStyleClass : function()
	{
		return this.styleClass;
	},
	
	setName : function(name)
	{
		this.name = name;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	setId : function(id)
	{
		this.id = id;
	},
	
	getId : function()
	{
		return this.id;
	},
	
	addCell : function(cell)
	{
		this.cells.add(cell);
	},
	
	removeCell : function(cell)
	{
		this.cells.remove(cell);
	},
	
	equals : function(column)
	{
		return (this.getId() == column.getId());
	},
	
	getCells : function()
	{
		return this.cells;
	}
});

var Table = Class.create({
	initialize : function()
	{
		this.styleClass = null;
		this.name = null;
		this.id = null;
		this.columns = new ArrayList(new Column());
	},

	setStyleClass : function(styleClass)
	{
		this.styleClass = styleClass;
	},
	
	getStyleClass : function()
	{
		return this.styleClass;
	},
	
	setName : function(name)
	{
		this.name = name;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	setId : function(id)
	{
		this.id = id;
	},
	
	getId : function()
	{
		return this.id;
	},
	
	addColumn : function(column)
	{
		this.columns.add(column);
	},
	
	removeColumn : function(column)
	{
		this.columns.remove(column);
	},
	
	equals : function(table)
	{
		return (this.getId() == table.getId());
	},
	
	getColumns : function()
	{
		return this.columns;
	}
});