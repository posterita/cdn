var TableRenderer = Class.create({
	initialize : function(table)
	{
		this.table = table;
		this.parentTable = document.createElement('table');
		this.parentTable.name = table.getName();
		this.parentTable.id = table.getId();
		this.parentTable.setAttribute('class', table.getStyleClass());
		this.parentRow = document.createElement('tr');
		this.parentTable.appendChild(this.parentRow);
	},
	
	clearRow : function()
	{
		this.parentRow.innerHTML = '';
	},
	
	getTable : function()
	{
		return this.table;
	},
	
	getComponent : function()
	{
		return this.parentTable;
	},
	
	draw : function(outputElementId)
	{
		var columns = this.table.getColumns();
		this.clearRow();
		for (var i=0;i<columns.size();i++)
		{
			var column = columns.get(i);
			var columnRenderer = new ColumnRenderer(column);
			var data = document.createElement('td');
			data.setAttribute('class', column.getStyleClass());
			data.setAttribute('valign', 'top');
			data.appendChild(columnRenderer.draw());
			this.parentRow.appendChild(data);
		}
		var output = $(outputElementId);
		if (output != null)
		{
			output.innerHTML = '';
			output.appendChild(this.parentTable);
		}
	}
});

var ColumnRenderer = Class.create({
	initialize : function(column)
	{
		this.column = column;
		this.columnTable = document.createElement('table');
		this.columnTable.setAttribute('id', column.getId());
		this.columnTable.setAttribute('class', column.getStyleClass());
	},
	
	getColumn : function()
	{
		return this.column;
	},
	
	draw : function()
	{
		var cells = this.column.getCells();
		for (var i=0;i<cells.size();i++)
		{
			var row = document.createElement('tr');
			var data = document.createElement('td');
			var cell = cells.get(i);
			if (cell != null)
			{
				row.appendChild(data);
				data.appendChild(cell.getContent());
				this.columnTable.appendChild(row);
			}
		}
		return this.columnTable;
	}
});