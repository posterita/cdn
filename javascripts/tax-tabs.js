var TaxSub = Class.create({

	initialize : function(taxSubPanel, json)
	{
		this.taxSubPanel = taxSubPanel;
		if (json != null)
		{
			this.name = json.name;
			this.rate = json.rate;
			this.id = json.id;
			this.comp = null;
			
			this.buildComp();
		}
	},
	
	getId : function()
	{
		return this.id;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getRate : function()
	{
		return this.rate;
	},
	
	getComponent : function()
	{
		return this.comp;
	},
	
	buildComp : function()
	{
		this.comp = document.createElement('div');
		this.comp.setAttribute('class', 'tax-sub-container');
		
		this.nameDiv = document.createElement('div');
		this.nameDiv.setAttribute('class', 'tax-sub-name');
		
		this.rateDiv = document.createElement('div');
		this.rateDiv.setAttribute('class', 'tax-sub-rate');
		
		this.deleteDiv = document.createElement('div');
		this.deleteDiv.setAttribute('class', 'tax-sub-delete');
		
		this.nameDiv.innerHTML = this.name;
		this.rateDiv.innerHTML = this.rate + ' %';
		
		this.deleteBtn = document.createElement('a');
		this.deleteBtn.setAttribute('class', 'delete-small');
		this.deleteBtn.setAttribute('href', 'javascript:void(0)');
		
		this.deleteDiv.appendChild(this.deleteBtn);
		
		this.comp.appendChild(this.nameDiv);
		this.comp.appendChild(this.rateDiv);
		this.comp.appendChild(this.deleteDiv);
		
		this.deleteBtn.onclick = this.remove.bind(this);
	},
	
	toJSON : function()
	{
		var hash = new Hash();
		hash.set('name', this.name);
		hash.set('id', this.id);
		hash.set('rate', this.rate);
		
		return hash.toJSON();
	},
	
	remove : function()
	{
		this.taxSubPanel.removeTaxSub(this);
	},
	
	equals : function(taxSub)
	{
		return (this.getId() == taxSub.getId());
	}
});

var TaxSubTab = Class.create(Tab, {
	
	initialize : function($super, tabId)
	{
		$super(tabId, 'TaxSubPanel');
		
		this.contentPanel = $('tax.sub.content');
		
		this.taxSubs = new ArrayList(new TaxSub());
		
		this.addBtn = $('tax.sub.addBtn');
		this.addBtn.onclick = this.add.bind(this);
		
		this.nameInput = $('tax.sub.name.input');
		this.rateInput = $('tax.sub.rate.input');
	},
	
	add : function()
	{
		var name = this.nameInput.value;
		var rate = this.rateInput.value;
		
		if (name == null || name.trim().length == 0)
		{
			return;
		}
		
		if (rate == null || rate.trim().length == 0)
		{
			rate = 0;
		}
		
		var json = {'name' : name, 'rate' : rate, 'id' : 0};
		var taxSub = new TaxSub(this, json);
		this.addTaxSub(taxSub);
	},
	
	afterCall : function(json)
	{
		this.generalHelp = json['sub.taxes.general.help'];
		this.taxSubs.clear();
		
		var subTaxes = json.subTaxes;
		this.contentPanel.innerHTML = '';
		if (subTaxes != null)
		{
			for (var i=0; i<subTaxes.length; i++)
			{
				var tax = subTaxes[i];
				var taxSub = new TaxSub(this, tax);
				
				this.addTaxSub(taxSub);
			}
		}		
	},
	
	addTaxSub : function(taxSub)
	{
		this.taxSubs.add(taxSub);
		
		var comp = taxSub.getComponent();
		
		this.contentPanel.appendChild(comp);
	},
	
	removeTaxSub : function(taxSub)
	{
		if (this.taxSubs.contains(taxSub))
		{
			var comp = taxSub.getComponent();
			this.contentPanel.removeChild(comp);
			
			this.taxSubs.remove(taxSub);
		}
	},
	
	getParameters : function()
	{
		var parameters = "{'subTaxes':" + this.taxSubs.toJSON() + "}";
		
		return parameters;
	},
	
	afterOk : function(json)
	{
		var summaryInfo = new TaxSummary(json);
		summaryInfo.init();
	}
});