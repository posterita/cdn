var OrderTax = {
		 defaultTaxId : 0,
		 isTaxIncluded : false,
		 		 
		 taxes:[],
		
		getTax:function(taxid){
	 		for(var i=0; i<this.taxes.length; i++){
	 			var tax = this.taxes[i];
	 			if(tax.id == taxid){
	 				return tax;
	 			}
	 		}
	 		
	 		return null;
 		},
 		
 		getTaxesForCategory:function(taxCategoryId){
 			var taxes = new Array();
 			
 			for(var i=0; i<this.taxes.length; i++){
	 			var tax = this.taxes[i];
	 			if(tax.categoryid == taxCategoryId){
	 				taxes.push(tax);
	 			}
	 		}
 			
 			return taxes;
 		},
 		
 		renderComponents:function(){
 			 			
 			if(this.defaultTaxId == 0){
 				/*take default tax*/
 				if(this.taxes == null || this.taxes.length == 0)
 				{
 					alert(Translation.translate("error.no.tax.found","Error! No tax found!"));
 					return;
 				}
 				
 				this.defaultTaxId = this.taxes[0].id;
 				for(var i=0; i<this.taxes.length; i++)
 				{
 					if(this.taxes[i].isdefault)
 					{
 						this.defaultTaxId = this.taxes[i].id;
 						break;
 					}
 				}
 				
 				TAX_ID = this.defaultTaxId;
 				
 			}
 			
 			
 			var defaultTax = OrderTax.getTax(this.defaultTaxId);
 			$('orderScreen.taxLabel').innerHTML = 'Tax:' + defaultTax.name + '&nbsp;&nbsp;&nbsp;Rate:' + defaultTax.rate;
 			
 			/* if price includes tax then disable tax dropdown*/
 			if(this.isTaxIncluded)
 			{
 				$('orderScreen.taxSelectList').hide();
 				return;
 			}
 			
 			var html = '';
 			for(var i=0; i<this.taxes.length; i++){
	 			var tax = this.taxes[i];
				html = html + "<option value='"+ tax.id  + ((tax.id == defaultTax.id) ? "' selected='selected'" : "'")+">" + tax.name + ' - ' + tax.rate + "%</option>";				 			
	 		} 			
 			
 			$('orderScreen.taxSelectList').innerHTML = html;
 		},
 		
 		initializeTaxes:function(){
 			var url = 'TaxAction.do';
			var pars = 'action=getAllTaxes';
			
 			var myAjax = new Ajax.Request( url, 
 			{ 
				method: 'get',
				parameters: pars,
				onSuccess: OrderTax.loadTaxes, 
				onFailure: function(request){alert(Translation.translate("failed.to.get.available.taxes","Failed to get available taxes!"));}
 			});
 		},
 		
 		loadTaxes:function(request){
 			var response = request.responseText;
 			var taxes = eval('(' + response + ')');
 			
 			OrderTax.taxes = taxes;
 			OrderTax.renderComponents();
 		},
 		
 		/* show the tax for the shopping cart line*/
 		showTax:function(taxId){
 			var options = $('orderScreen.taxSelectList').options;
 			
 			for(var i=0; i<options.length; i++)
 			{
 				var option = options[i];
 				option.selected = (option.value == taxId);
 			}
 		}
 	
 };
 
 /*Event.observe(window,'load',OrderTax.initializeTaxes.bind(OrderTax),false);*/
