 var DateUtil = {
        /* return current date yyyy-mm-dd hh:mm:ss*/
        getCurrentDate:function(){
   
            var today = new Date();
           
            var year = today.getFullYear();
            var month = today.getMonth() + 1;                   
            var date = today.getDate();
            var hours = today.getHours();
            var minutes = today.getMinutes();
            var seconds = today.getSeconds();
           
            if(month < 10) month = '0' + month;
            if(date < 10) date = '0' + date;
            if(hours < 10) hours = '0' + hours;
            if(minutes < 10) minutes = '0' + minutes;
            if(seconds < 10) seconds = '0' + seconds;
           
            var currentDate = year + "-" + month + "-" + date +
                " " + hours + ":" + minutes + ":" + seconds;
	           
	            return currentDate;
	        }
  };
  
  /* CookieManager to create, read and erase cookies */
  var CookieManager = {
          createCookie:function(name,value,days){
              if (days) {
                  var date = new Date();
                  date.setTime(date.getTime()+(days*24*60*60*1000));
                  var expires = "; expires="+date.toGMTString();
          }
          else var expires = "";
          document.cookie = name+"="+value+expires+"; path=/";
      },
     
      readCookie:function(name){
          var nameEQ = name + "=";
          var ca = document.cookie.split(';');
          for(var i=0;i < ca.length;i++) {
              var c = ca[i];
              while (c.charAt(0)==' ') c = c.substring(1,c.length);
              if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
          }
          return null;
      },
     
      eraseCookie:function(name){
          this.createCookie(name,"",-1);
          }
  };
      