MenuManager.onReady(function(){
	
	function renderTemplateMenus(json)
	{
		var parent_container = jQuery("#template-menu-container");
		parent_container.html("");
		
		var menu = '<a href="GettingStartedAction.do?action=getPOSInitializationSteps">'
				   + '<i class="glyphicons glyphicons-home"></i><span>'
				   + Translation.translate("home","Home")
				   + '</span></a>';
				   
		parent_container.append(menu);
		
		var category = "posterita";
		var menus = null;
		var menu = null;
		
		var report_menu = false;
		
		for (var i=0; i < json.length; i++)
		{
			var isPage = false;
			var parent = json[i];
			
			var parent_category = parent["parent_category"];
			var module = parent["module"];

			var categories = parent["categories"];
			
			if(category != parent_category)
			{
				if(menus != null)
				{
					//parent_container.append(menus);
				}
				
				category = parent_category;
			}
			
			if (module == 'new.report')
			{
				if (!report_menu)
				{
					menu = '<a href="backoffice-reports.do#pmenuId=-1">'
						   + '<i class="glyphicons glyphicons-stats"></i><span>'
						   + Translation.translate("reports","Reports") 
						   + '</span></a>';
						   
					parent_container.append(menu);
						   
					report_menu = true;
				}
			}
			else
			{	
				var parentName = parent["parent"];
				
				parentName = Translation.translate(parentName);
				
				if(categories.length == 1)
				{
					var category_menus = categories[0]["menus"];
					
					if(category_menus.length == 1)
					{
						menu = '<a id=' + parent["parent_id"] + ' href="' + category_menus[0]["menuLink"] + '">' 
						   + '<i class="glyphicons glyphicons-' + parent["parent_imagelink"] + '"></i><span>'
						   + parentName
						   + '</span></a>';
						   
						isPage = true;
					}
				}
				
				if(!isPage)
				{
					menu = '<a id=' + parent["parent_id"] + ' href="backoffice-menus.do#pmenuId=' + parent["parent_id"] + '">' 
					+ '<i class="glyphicons glyphicons-' + parent["parent_imagelink"] + '"></i><span>'
					+ parentName 
					+ '</span></a>';
				}
				   
				parent_container.append(menu);
			}
		};

		menu = '<a href="InitSupportAction.do?action=initRequestSupport&pmenuId=0">'
			   + '<i class="glyphicons glyphicons-circle-question-mark"></i><span>Support</span>'
			   + '</a>'
			   
		menu = menu + '<a href="javascript:MenuManager.logout();">'
			   + '<i class="glyphicons glyphicons-exit"></i><span>'				   
			   + Translation.translate("logout")
			   + '</span></a>';
		
		parent_container.append(menu);			
					
	}
	
	function renderBreadcrumbs(){
		
		var breadcrumbs = MenuManager.getBreadcrumbs();
		
		if(breadcrumbs.length == 0) return;
		
		var html = "", breadcrumb = null;
		
		for(var i=0; i<breadcrumbs.length; i++){
			
			breadcrumb = breadcrumbs[i];
			
			if( i == breadcrumbs.length -1 ){
				
				html += '<li class="breadcrumb-item active">' + breadcrumb.name + '</li>';
				continue;
			}
			
			html += '<li class="breadcrumb-item"><a href="' + breadcrumb.link + '">' + breadcrumb.name + '</a></li>';
		}
		
		jQuery("#breadcrumb-container").append(html);
		
		jQuery("#" + breadcrumbs[0].id).addClass("active");
		
	}
	
	var jsonMenu = MenuManager.json;
	
	renderTemplateMenus(jsonMenu);
	renderBreadcrumbs();
	
	var pmenuId = null;
	
	var search = window.location.search;
	
	if(search.length > 0){
		search = search.substr(1);
		
		search.split('&').forEach(function(i){ 
			if( i.startsWith('pmenuId') ){
				pmenuId = i.split("=")[1];
			}
		});
	}
	
				
	if(pmenuId != null)
	{
		if(pmenuId > 0)
		{
			var MENU_DB = TAFFY(jsonMenu);
			var menu_query = {};
			
			menu_query["parent_id"] = {'==':pmenuId};
			var menu = MENU_DB(menu_query).get();
			
			if (menu.length > 0)
			{
				jQuery("#parent-menu-name").text(Translation.translate(menu[0]["parent"]));
			}
			
			
			if ( menu[0] && menu[0]["module"] == 'new.report')
			{
				pmenuId = "-1";
			}
		}
		
		jQuery("#" + pmenuId).addClass("active");				
	}
	
	jQuery("#toggle-sidebar-visibilty-btn").on("click", function(){
		
		var sidebar = jQuery("#sidebar");
		
		if(sidebar.hasClass("collapse")){
			
			/* set menu title */
			jQuery("#template-menu-container a").each(function(index, link){
				var anchor = jQuery(link);			
				anchor.removeAttr("title");
			});
			
			jQuery("#sidebar").removeClass("collapse");				
			jQuery("#toggle-sidebar-visibilty-btn i").removeClass("glyphicons-circle-arrow-right").addClass("glyphicons-circle-arrow-left");
		}
		else
		{
			jQuery("#sidebar").addClass("collapse");	
			jQuery("#toggle-sidebar-visibilty-btn i").removeClass("glyphicons-circle-arrow-left").addClass("glyphicons-circle-arrow-right");
			
			/* set menu title */
			jQuery("#template-menu-container a").each(function(index, link){
				var anchor = jQuery(link);
				var text = anchor.text();				
				anchor.attr("title", text);
			});
		}	
		
		window.dispatchEvent(new Event('resize'));
		
	});			
	
});

