jQuery(function(){	
	
	jQuery("#terminal-details-container").on("click", function(){
		
		/* get login info
		 LoginAction.do?action=getLoginInfo */
		
		var _showInfo = function(info){
			
			var msg = `<table id="terminal-details-dialog-table"><tr><td><label>Domain</label>:</td><td>${info.domain}</td></tr><tr>
					<td>
						<label>Store</label>:
					</td>
					<td>
						${info.store}
					</td>
				</tr>
				
				<tr>
					<td>
						<label>Terminal</label>:
					</td>
					<td>
						${info.terminal}
					</td>
				</tr>
				
				<tr>
					<td>
						<label>User</label>:
					</td>
					<td>
						${info.user}
					</td>
				</tr>
				
				<tr>
					<td>
						<label>Role</label>:
					</td>
					<td>
						${info.role}
					</td>
				</tr>
				
				</table>`;
			
			BootstrapDialog.show({
	            title: '<span class="glyphicons glyphicons-pro circle_info"></span> Information',
	            message: jQuery(msg),
	            buttons: [{
	                label: 'Change Terminal',
	                action: function(dialog) {
	                    dialog.close();
	                    _changeTerminal();
	                }
	            }]
	        });
			
		};
		
		
		var info = sessionStorage.getItem("LOGIN_INFO");
		
		if(info == null){			
			
			jQuery.getJSON("LoginAction.do?action=getLoginInfo", function(data){				
				
				sessionStorage.setItem('LOGIN_INFO', JSON.stringify(data));
				
				_showInfo(data);
			});
		}
		else 
		{			
			info = JSON.parse(info);
			_showInfo(info);
		}
		
		
	});
	
	var _changeTerminal = function(){
		
		/* get all stores and terminals */
		/* DataTableAction.do?action=getStoresAndTerminals */
		
		jQuery.getJSON("DataTableAction.do?action=getStoresAndTerminals", function(data){			
			_showChangeTerminal(data.stores);
		});
		
		var _showChangeTerminal = function(stores){
			
			var tbl = `<table id="change-terminal-dialog-table">
			<tr><td><label>Store</label></td></tr>
			<tr><td><select id="change-terminal-dialog-store-dropdown" class="form-control input-lg"></select></td></tr>
			<tr><td><label>Terminal</label></td></tr>
			<tr><td><select id="change-terminal-dialog-terminal-dropdown" class="form-control input-lg"></select></td></tr>
			</table>`;
			
			tbl = jQuery(tbl);
			
			var storeDropdown = tbl.find("#change-terminal-dialog-store-dropdown");
			var terminalDropdown = tbl.find("#change-terminal-dialog-terminal-dropdown");
			
			var store, option;
			
			for(var i=0; i<stores.length; i++){
				store = stores[i];
				if(store.ad_org_id == 0) continue;
				
				option = jQuery(`<option value="${store.ad_org_id}">${store.ad_org_name}</option>`);
				storeDropdown.append(option);
				
				jQuery(option).data("terminals", store.terminals);
			}
			
			storeDropdown.on("change", function(){
				terminalDropdown.html("");
				
				var select = storeDropdown.get(0);
				var option = select.options[select.selectedIndex];
				
				var terminals = jQuery(option).data("terminals");
				var terminal;
				
				for(var i=0; i<terminals.length; i++){
					terminal = terminals[i];
					
					option = jQuery(`<option value="${terminal.u_posterminal_id}">${terminal.u_posterminal_name}</option>`);
					terminalDropdown.append(option);
				}
				
			});
			
			var info = sessionStorage.getItem("LOGIN_INFO");
			if(info){
				info = JSON.parse(info);
				jQuery(storeDropdown).val(info.ad_org_id);
			}
			
			jQuery(storeDropdown).trigger("change");			
			jQuery(terminalDropdown).val(info.u_posterminal_id);
			
			BootstrapDialog.show({
	            title: '<span class="glyphicons glyphicons-pro glyphicons-imac"></span> Change Terminal',
	            message: tbl,
	            buttons: [{
	                label: 'Cancel',
	                action: function(dialog) {
	                    dialog.close();
	                }
	            },
	            	{
	                label: 'OK',
	                cssClass: 'btn-success',
	                action: function(dialog) {
	                    dialog.close();
	                    //_setTerminal();
	                    var ad_org_id = storeDropdown.val();
	                    var u_posterminal_id = terminalDropdown.val();
	                    
	                    /* reset login info cache */
	                    sessionStorage.removeItem('LOGIN_INFO');
	                    		
	                    window.location.href = `TerminalAction.do?action=changeTerminal&ad_org_id=${ad_org_id}&u_posterminal_id=${u_posterminal_id}`;
	                }
	            }]
	        });
		};
	};
	
	
});