var TerminalManager = {
		terminal:null,
		terminalList:null,
		changeTerminalPopUp:null,
		getCurrencySymbol:function(){
			return this.terminal.currency;
		},
		getTaxRate:function(){
			return this.terminal.taxRate;
		},
		chooseTerminal:function(){
			if(!this.changeTerminalPopUp){
				this.changeTerminalPopUp = new ChangeTerminalPopUp();
			}
			
			this.changeTerminalPopUp.show();
		},
		setTerminal:function(id){
			if(this.terminal.id == id) return;			
			new Dialog().show();
			
			window.location = "TerminalAction.do?action=swapTerminal&terminalId=" + id;
		},
		resetCurrentTerminal:function(){
			CookieMananger.eraseCookie('POSTerminal');
		}
};