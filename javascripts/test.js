var getURL = function(url){
    /*TODO: check network*/
    window.location = url;
};

var LOGGER = {
        enable:false,
        info:function(msg){if(this.enable)console.info(msg);},
        warn:function(msg){if(this.enable)console.warn(msg);},
        error:function(msg){if(this.enable)console.error(msg);}
};

/******************************************************************************/
var ViewPort = {
    getHeight : function() {
        if (window.innerHeight!=window.undefined) return window.innerHeight;
        if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
        if (document.body) return document.body.clientHeight;
        return window.undefined;
    },
   
    getWidth : function() {
        if (window.innerWidth!=window.undefined) return window.innerWidth;
        if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth;
        if (document.body) return document.body.clientWidth;
        return window.undefined;
    },
   
    getScrollXY :function () {
        var scrOfX = 0, scrOfY = 0;
        if( typeof( window.pageYOffset ) == 'number' ) {
          //Netscape compliant
          scrOfY = window.pageYOffset;
          scrOfX = window.pageXOffset;
        } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
          //DOM compliant
          scrOfY = document.body.scrollTop;
          scrOfX = document.body.scrollLeft;
        } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
          //IE6 standards compliant mode
          scrOfY = document.documentElement.scrollTop;
          scrOfX = document.documentElement.scrollLeft;
        }
        return [ scrOfX, scrOfY ];
    },
   
    center:function(element){
        var xPos = (document.viewport.getWidth() - element.getDimensions().width)/2;   
        var yPos = (document.viewport.getHeight() - element.getDimensions().height)/2;
                               
        element.setStyle({
            postion: "absolute",
            left : (xPos) + "px",
            top  : (yPos - 40) + "px"}
        );
    }
};//ViewPort


/* CookieManager to create, read and erase cookies */
var CookieManager = {
        createCookie:function(name,value,days){
            if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
            }
            else var expires = "";
            document.cookie = name+"="+value+expires+"; path=/";
        },
       
        readCookie:function(name){
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        },
       
        eraseCookie:function(name){
            this.createCookie(name,"",-1);
        }
};

/* PreferenceManager loads and saves preferences cookies */
var PreferenceManager = {
        savePreference:function(name, value){
            CookieManager.eraseCookie(name);
            CookieManager.createCookie(name, value, 360);
        },
       
        getPreference:function(name){
            return CookieManager.readCookie(name);
        },
       
        CONSTANTS : {
            SEARCH_RESULT_LENGTH : 'preference.searchResultLength',
            MINIMUM_CHARACTERS : 'preference.minimumCharacters',
            LANGUAGE : 'preference.language',
            PRINTER : 'preference.printer',
            PRINTER_TYPE : 'preference.printerType',
            PRINTER_LINE_WIDTH : 'preference.printerLineWidth',
            BARCODE_PRINTER : 'preference.barcodePrinter',
            BARCODE_PRINTER_TYPE : 'preference.barcodePrinterType',
            ENABLE_PRINTING : 'preference.enablePrinting',
            ENABLE_CASH_DRAWER : 'preference.enableCashDrawer',
            PRINT_MERCHANT_COPY : 'preferences.printMerchantCopy',
            POLE_DISPLAY : 'preference.poleDisplay',
            ENABLE_POLE_DISPLAY : 'preference.enablePoleDisplay',

            ACCEPT_CASH_PAYMENT : 'preference.acceptCashPayment',
            PRINT_CASH_RECEIPT : 'preference.printCashReceipt',
            PRINT_CASH_RECEIPT_COPY : 'preference.printCashReceiptCopy',

            ACCEPT_CARD_PAYMENT : 'preference.acceptCardPayment',
            PRINT_CARD_RECEIPT : 'preference.printCardReceipt',
            PRINT_CARD_RECEIPT_COPY : 'preference.printCardReceiptCopy',

            ACCEPT_EXTERNALCARD_PAYMENT : 'preference.acceptExternalCardPayment',
            PRINT_EXTERNALCARD_RECEIPT : 'preference.printExternalCardReceipt',
            PRINT_EXTERNALCARD_RECEIPT_COPY : 'preference.printExternalCardReceiptCopy',

            ACCEPT_CHEQUE_PAYMENT : 'preference.acceptChequePayment',
            PRINT_CHEQUE_RECEIPT : 'preference.printChequeReceipt',
            PRINT_CHEQUE_RECEIPT_COPY : 'preference.printChequeReceiptCopy',

            ACCEPT_MIX_PAYMENT : 'preference.acceptMixPayment',
            PRINT_MIX_RECEIPT : 'preference.printMixReceipt',
            PRINT_MIX_RECEIPT_COPY : 'preference.printMixReceiptCopy',

            ACCEPT_ONCREDIT_PAYMENT : 'preference.acceptOnCreditPayment',
            PRINT_ONCREDIT_RECEIPT : 'preference.printOnCreditReceipt',
            PRINT_ONCREDIT_RECEIPT_COPY : 'preference.printOnCreditReceiptCopy',

            ACCEPT_VOUCHER_PAYMENT : 'preference.acceptVoucherPayment',
            PRINT_VOUCHER_RECEIPT : 'preference.printVoucherReceipt',
            PRINT_VOUCHER_RECEIPT_COPY : 'preference.printVoucherReceiptCopy'           
        }
};

/* PrinterManager communicates with printer and cash drawer */
var PrinterManager = {
        applet : null,
       
        printTestData:function(){
            var printJob = Printer_ESC_COMMANDS.FONT_NORMAL;
           
            for(var i=0; i<10; i++){
                printJob = printJob + '1234567890';
            }
           
            printJob = printJob + Printer_ESC_COMMANDS.LINE_FEED
            + Printer_ESC_COMMANDS.PAPER_CUT + Printer_ESC_COMMANDS.LINE_FEED ;
           
            printJob = printJob + Printer_ESC_COMMANDS.OPEN_DRAWER + Printer_ESC_COMMANDS.LINE_FEED;
            this.sendPrintJob(printJob);
        },
       
        openDrawer:function(){
            var printJob = "" + Printer_ESC_COMMANDS.OPEN_DRAWER + Printer_ESC_COMMANDS.LINE_FEED;           
            this.sendPrintJob(printJob);
        },
       
        sendPrintJob:function(printJob){
            /* is printing allowed */
            if(this.isPrintingEnabled()){
                /* do nothing */
            }
            else{
                LOGGER.warn('Printing is not allowed! Preferences[printing]');
                return;
            }
           
            /*printing via applet*/
            var applet = this.getPrintApplet();
           
            if(applet == null){
                LOGGER.error('Could not locate Printer Applet!');
                return false;
            }
                       
            /* see PosteritaPrinterApplet.addJob(str) */
            try{
                applet.addJob(printJob);
                return true;
            }
            catch (xception){
                LOGGER.error('Could not send job to Printer! An error has occurred: ' + e.message);
                return false;
            }           
        },
       
        getPrintApplet:function(){
           
            if(this.applet) return this.applet;
           
            var applet = null;
           
            if(document.applets.length > 0){
                applet = document.applets[0];
            }
            else{
                if(window.parent.frames){
                    if(window.parent.frames.length > 1)
                    if(window.parent.frames[1].document.applets.length > 0){
                        applet = window.parent.frames[1].document.applets[0];
                    }
                }
            }
           
            if(applet == null) return null;
           
            /* set printer name */
            var printer = this.getPrinterName();
            if (printer && printer != null && printer.length > 0 && printer != 'default' && printer != 'error'){   
                applet.setPrinterName(printer);
            }
           
            return applet;
        },
       
        isWebPRNTEnabled:function(){
            var printing = PreferenceManager.getPreference('preference.enableWebPRNT');
            return ('true' == printing);
        },
       
        getWebPRNTUrl:function(){
            var url = PreferenceManager.getPreference('preference.webPRNTURL');
            return url;
        },
       
        isPrintingEnabled:function(){
            var printing = PreferenceManager.getPreference('preference.enablePrinting');
            return ('true' == printing);
        },
       
        isCashDrawerPresent:function(){
            var printing = PreferenceManager.getPreference('preference.enableCashDrawer');
            return ('true' == printing);
        },
       
        printMerchantCopy:function(){
            var printing = PreferenceManager.getPreference('preferences.printMerchantCopy');
            return ('true' == printing);
        },
       
        getPrinterName:function(){
            var printer = PreferenceManager.getPreference('preference.printer');
            return printer || 'default';
        },
       
        getLineWidth:function(){
            var width =  PreferenceManager.getPreference('preference.printerLineWidth');
           
            if(width){
                width = parseInt(width);
            }
            else{
                width = 40;
            }
           
            return width;
        },
       
        /* returns a list of printer configured on the user machine */
        getPrintersList:function(){
            var applet = this.getPrintApplet();
            if(applet == null) return '[]';
           
            var list = applet.getPrintersAsJSON();           
            return list;
        },
       
        setPrinterName:function(printer){
            this.applet = this.getPrintApplet();
            if(this.applet == null)return;
            this.applet.setPrinterName(printer);
        }
};

/*PoleDisplayManager communicates with pole display*/
var PoleDisplay_ESC_COMMANDS = {
        CLEAR:'\x0C',
        LINE_FEED:'\x0A',
        MOVE_LEFT: '\x0D'
};

var PoleDisplayManager = {
        applet : null,
       
        printTestData:function(){
            var printJob = PoleDisplay_ESC_COMMANDS.CLEAR + "This is a test message";           
            this.sendPrintJob(printJob);
        },
       
        clearDisplay:function(){
            var printJob = PoleDisplay_ESC_COMMANDS.CLEAR;           
            this.sendPrintJob(printJob);
        },
       
        display:function(line1,line2){
            /* clear display */
            var printJob = PoleDisplay_ESC_COMMANDS.CLEAR;
            printJob = printJob + line1;
           
            if(line2){
                printJob = printJob + PoleDisplay_ESC_COMMANDS.LINE_FEED + PoleDisplay_ESC_COMMANDS.MOVE_LEFT + line2;
            }
           
            this.sendPrintJob(printJob);
        },
       
        sendPrintJob:function(printJob){
            /* is printing allowed */
            if(this.isPoleDisplayPresent()){
                /* do nothing */
            }
            else{
                LOGGER.warn('PoleDisplay is not allowed! Preferences[PoleDisplay]');
                return;
            }
           
            /*printing via applet*/
            var applet = this.getPrintApplet();
           
            if(applet == null){
                LOGGER.error('Could not locate Printer Applet!');
                return false;
            }
                       
            /* see PosteritaPrinterApplet.addJob(str) */
            try{
                applet.addJob(printJob);
                return true;
            }
            catch (xception){
                LOGGER.error('Could not send job to Pole Display! An error has occurred: ' + e.message);
                return false;
            }           
        },
       
        getPrintApplet:function(){
           
            if(this.applet) return this.applet;
           
            var applet = null;
           
            if(document.applets.length > 0){
                applet = document.applets[0];
            }
            else{
                if(window.parent.frames){
                    if(window.parent.frames[1].document.applets.length > 0){
                        applet = window.parent.frames[1].document.applets[0];
                    }
                }
            }
           
            if(applet == null) return null;
           
            /* set printer name */
            var printer = this.getPrinterName();
            if (printer && printer != null && printer.length > 0 && printer != 'default' && printer != 'error'){   
                applet.setPrinterName(printer);
            }
           
            return applet;
        },
       
        isPoleDisplayPresent:function(){
            var present = PreferenceManager.getPreference('preference.enablePoleDisplay');
            return ('true' == present);
        },
               
        getPrinterName:function(){
            var printer = PreferenceManager.getPreference('preference.poleDisplay');
            return printer || 'default';
        },       
       
        setPrinterName:function(printer){
            this.applet.setPrinterName(printer);
        }
};


/*SessionMonitor monitors uses activity*/
var SessionMonitor = {
        /*30 minutes*/
        maxInactiveInterval:30*60,
        elapsedInactiveInterval:-60,
        isActive:false,
        start:function(){
            this.isActive = true;
            this.timeout = setInterval("SessionMonitor.incrementInactiveInterval()",60000);
        },
        incrementInactiveInterval:function(){
            this.elapsedInactiveInterval = this.elapsedInactiveInterval + 60;
           
            if(this.elapsedInactiveInterval >= this.maxInactiveInterval){               
                this.stop();
                this.notifyTimeout();
                return;
            }           
        },
        stop:function(){
            clearTimeout(this.timeout);
            this.elapsedInactiveInterval = 0;
            this.isActive = false;
        },
        reset:function(){
            if(this.isActive){
                this.elapsedInactiveInterval = 0;
                return true;
            }
            return false;
        },
        isSessionActive:function(){
            return this.isActive;
        },
        notifyTimeout:function(){
            var html = "Your session has timeout! <br> <button onclick='login()'>Login</button>";
            new Dialog(html).show();
        }
};

var DateUtil = {
        /* return current date yyyy-mm-dd hh:mm:ss*/
        getCurrentDate:function(){
   
            var today = new Date();
           
            var year = today.getFullYear();
            var month = today.getMonth() + 1;                   
            var date = today.getDate();
            var hours = today.getHours();
            var minutes = today.getMinutes();
            var seconds = today.getSeconds();
           
            if(month < 10) month = '0' + month;
            if(date < 10) date = '0' + date;
            if(hours < 10) hours = '0' + hours;
            if(minutes < 10) minutes = '0' + minutes;
            if(seconds < 10) seconds = '0' + seconds;
           
            var currentDate = year + "-" + month + "-" + date +
                " " + hours + ":" + minutes + ":" + seconds;
           
            return currentDate;
        },
       
        getReportStartOfMonthDate:function(){
            var today = new Date();
           
            var year = today.getFullYear();
            var month = today.getMonth() + 1;
           
            if(month < 10) month = '0' + month;
           
            var startOfMonthDate = year + "-" + month + "-" + "01";
           
            return startOfMonthDate;
        }
};

var tabPanel = {
        btns:null,
        tabPanes:null,
        tabPaneIndex:0,
        activePane:null,
        activePaneIndex:0,
        onChangeHandler:function(){},
        render:function(){
            for(var i=0; i<this.btns.length; i++)
            {
                var btn = this.btns[i];
                var tabPane = this.tabPanes[i];

                //add behaviour to tabs
                btn.tabPane = tabPane;
                btn.tabPanel = this;

                btn.onclick = function(){
                    this.tabPanel.showTabPane(this.tabPane);
                };

                //add behaviour to panes
                tabPane.btn = btn;
                tabPane.tabPanel = this;
                tabPane.render = function()
                {
                    if(this == this.tabPanel.activePane)
                    {
                        Element.show(this);
                        this.btn.addClassName('active');
                    }
                    else
                    {
                        Element.hide(this);
                        this.btn.removeClassName('active');
                    }
                };
            }
           
            /*check active panel index*/
            if(this.activePaneIndex > this.tabPanes.length){
                this.activePaneIndex = 0;
            }

            this.showTabPane(this.tabPanes[this.activePaneIndex]);                   
   
        },/*render*/
        showTabPane:function(tabPane){

            this.activePane = tabPane;
           
            for(var i=0; i<this.tabPanes.length; i++)
            {
                var pane = this.tabPanes[i];
                if(this.activePane == pane){
                    this.activePaneIndex = i;
                }
               
                pane.render();
            }
           
            this.onChangeHandler();
        }
       
};

var Clock = {
        tick : function(){
            var currentTime = new Date ( );
           
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            var seconds = currentTime.getSeconds();
           

            var currentTimeString = currentTime.toString();
            var d = currentTimeString.split(' ');
       
            var day = d[0];
            var month = d[1];
            var date = d[2];
            var year = d[3];
            var time = d[4];
            var timezone = d[5];

            if(hours < 10) hours = '0' + hours;
            if(minutes < 10) minutes = '0' + minutes;
            if(seconds < 10) seconds = '0' + seconds;
           
            var html = month + ' ' + date + ' ' + year + ' ' + hours + ':' + minutes;
            $(this.container).innerHTML = html;
        },
       
        container : 'clock-container',
       
        start : function(){
            if($(this.container) == null) return;
            this.tick();
            setInterval('Clock.tick()', 1000 );
        }
};

Event.observe(window,'load',Clock.start.bind(Clock),false);

var MessageDisplay = {
        container : 'message-display-container',
        info : function(message1, message2){
            if(!$(this.container)) return;
            var html = message1 + "<br><span>" + message2 + "</span>";
            $(this.container).className = 'normal-message';
            $(this.container).innerHTML = html;
        },
        save : function(){
            if(!$(this.container)) return;
            $(this.container).className = 'message-display-container-saved';
            $(this.container).innerHTML = '<span>SAVED</span>';
        },
        error : function(){
            if(!$(this.container)) return;
            $(this.container).className = 'message-display-container-error';
            $(this.container).innerHTML = '<span>ERROR</span>';
        }
};