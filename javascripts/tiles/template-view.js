function initIWantTo() {
			
		}
		
		if ($('close-success-box-btn') != null)
 	    {
     	   $('close-success-box-btn').onclick = function(e){
					
					$('success-msg-box').style.display = 'none';
				};
 	   }
        
        
        
        if ($('close-error-box-btn') != null)
 	    {
				$('close-error-box-btn').onclick = function(e){
									
					$('error-msg-box').style.display = 'none';
				};
 	    }
        
		Event.observe(window, 'load', initIWantTo, false);
		
		jQuery(document).ready(function(){
     	   
     	   setContentHeight();
     		  
     	   jQuery(window).resize(function(){
	        		   setContentHeight();
       	   		});
	    });
        
        
        
        
        function setContentHeight()
        {
     	   var windowHeight = jQuery(window).height();
     	   
     	   //Summary tab is default tab, set shopping cart height
     	   var tabContentContainer = jQuery('#shopping-cart-lines-container');
     	   var tabContentContainerOffset = tabContentContainer.offset().top;
     	   
     	   // if Menu tab is active
     	   if (tabContentContainerOffset == 0)
     	   {
     		   tabContentContainer = jQuery('#menu');
     		   tabContentContainerOffset = tabContentContainer.offset().top;
     	   }
     	   
     	   var tabContentContainerHeight = windowHeight - tabContentContainerOffset;
     	   tabContentContainer.css('max-height', tabContentContainerHeight + 'px');
     	   tabContentContainer.css('height', tabContentContainerHeight + 'px');
     	   
     	   //var pageTotalHeight = jQuery(document).height();
     	   
     	   //var pateTotalHeight = getDocumentHeight();
     	   
     	  var documentHeight = jQuery(document).height();
     	   
     	  if (!jQuery.browser.mozilla)
          {
          	  var lastElement = jQuery('#last-element');
              var lastElementOffset = lastElement.offset().top;
              documentHeight = lastElementOffset + lastElement.outerHeight();
          }
          
          
     	   
     	   var excessHeight = 0;
     	   
     	   if (documentHeight >= windowHeight)
     	   {
     		   excessHeight = documentHeight - windowHeight;
     		   
     		   tabContentContainerHeight = tabContentContainerHeight - excessHeight;
     		   
     		   if (tabContentContainerHeight > 0)
     		   {
     			  tabContentContainer.css('max-height', tabContentContainerHeight + 'px');
     			  tabContentContainer.css('height', tabContentContainerHeight + 'px');
     			  
     			 documentHeight = jQuery(document).height();
     			  
     			 if (!jQuery.browser.mozilla)
     	          {
     	          	  var lastElement = jQuery('#last-element');
     	              var lastElementOffset = lastElement.offset().top;
     	              documentHeight = lastElementOffset + lastElement.outerHeight();
     	          }
     			 
     			  
     			  excessHeight = documentHeight - windowHeight;
     		   }
     	   }
     	   
     	   var leftContent = jQuery('#messages-container');
     	   
     	   if (leftContent == null ||leftContent.length == 0 )
   		   {
     		  leftContent = jQuery('#view-contents-left-container');
   		   }
     	   
     	   jQuery('#comments-container').css('display', 'block');
     	   
   		   //var leftContent = jQuery('#view-contents-left-container');
    	   var leftContentOffset = leftContent.offset().top;
    	   var leftContentHeight = windowHeight - leftContentOffset - 45;
    	   leftContent.css('max-height', leftContentHeight + 'px');
    	   leftContent.css('height', leftContentHeight + 'px');
    	   
    	   documentHeight = jQuery(document).height();
     	   
      	  if (!jQuery.browser.mozilla)
           {
           	   lastElement = jQuery('#last-element');
               lastElementOffset = lastElement.offset().top;
               documentHeight = lastElementOffset + lastElement.outerHeight();
           }
    	   
      		excessHeight = documentHeight - windowHeight;
    	   
    	   if (documentHeight >= windowHeight)
    	   {
    		   leftContentHeight = leftContentHeight - excessHeight;
    		   
    		   //Allow to scroll if height is greater than 100 px
    		   if (leftContentHeight > 0 && leftContentHeight > 100)
    		   {
    			   leftContent.css('max-height', leftContentHeight + 'px');
    			   leftContent.css('height', leftContentHeight + 'px');
    		   }
    		   else
	    	   {
	    	   		if (leftContentOffset + leftContent.outerHeight(true) < windowHeight)
	    	   		{
	    	   			leftContentHeight = windowHeight - leftContentOffset - 510;
	    	   			leftContent.css('max-height', leftContentHeight + 'px');
	    	   			leftContent.css('height', leftContentHeight + 'px');
	    	   		}
	    	   }
    	   }
    	   
    	   if (jQuery('.message').length == 0)
    	   {
    		   jQuery('#comments-container').css('display', 'none');
    	   }
        }
        
        function showSummaryTab()
        {
            jQuery('#tabs a[href="#summary"]').tab('show');
           	setContentHeight();
        }
        
        function showMenuTab()
        {
            jQuery('#tabs a[href="#menu"]').tab('show');
           	setContentHeight();
        }