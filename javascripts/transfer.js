/*var Field = Class.create({
			initialize : function(fromList, toList, optionList, elementSeparator)
			{
				this.fromList = fromList;
				this.toList = toList;
				this.optionList = optionList;
				this.elementSeparator = elementSeparator;
			},

			getFromList : function()
			{
				return this.fromList;
			},

			getToList : function()
			{
				return this.toList;
			},

			getOptionList : function()
			{
				return this.optionList;
			},
			
			getElementSeparator : function()
			{
				return this.elementSeparator;
			}
		});
	*/

var Transfer = Class.create({
		initialize:function(select1, select2, optionList1, optionList2)
		{
			this.s1=$(select1);
			this.s2=$(select2);
			this.inputFrom = $(optionList1);
			this.inputTo=$(optionList2);
			this.toRightEach = $('toRightEach');
			this.toLeftEach = $('toLeftEach');
			this.toRightAll = $('toRightAll');
			this.toLeftAll = $('toLeftAll');
			this.up = $('up');
			this.down = $('down');			
		},
		
		initBtns : function(toRightEach, toLeftEach, toRightAll, toLeftAll, up, down)
		{
			this.toRightEach = $(toRightEach);
			this.toLeftEach = $(toLeftEach);
			this.toRightAll = $(toRightAll);
			this.toLeftAll = $(toLeftAll);
			this.up = $(up);
			this.down = $(down);
		},
		
		transfer:function(s1,s2)
		{
			var options = s1.options;
			if(options.length < 1) return;
			
			var index = s1.selectedIndex;
			if(index == -1) index = 0;
							
			var option = Element.remove(options[index]);				
			Element.insert(s2, option);
			
			this.updateInputs();
		},
					
		transferAll:function(s1,s2)
		{
			var options = $A(s1.options);

			if(options.length < 1) return;
			
			var count = options.length;
			
			while(parseInt(count) > 0){
				var option = Element.remove(options[0]);				
				Element.insert(s2, option);

				options = $A(s1.options);
				count = count - 1;
			}	
			this.updateInputs();	
		},
		
		transferLeft:function(){
			//s2>s1
			this.transfer(this.s2,this.s1);
		},
		
		transferAllLeft:function(){
			//s2>>s1
			this.transferAll(this.s2,this.s1);
		},
		
		transferRight:function(){
			//s1>s2
			this.transfer(this.s1,this.s2);
		},

		transferAllRight:function(){
			//s2>>s1
			this.transferAll(this.s1,this.s2);				
		},
		
		moveUp:function()
		{
			var index = this.s2.selectedIndex;
			if(index < 1) return;

			var options = this.s2.options;
			var option1 = Element.remove(options[index]);
			var option2 = options[index-1];
			
			new Insertion.Before(option2, option1);
			this.updateInputs();
		},

		moveDown:function()
		{
			var index = this.s2.selectedIndex;
			var length = this.s2.options.length - 1;
			
			if(index == -1 || index == length) return;
			
			var options = this.s2.options;

			var option1 = Element.remove(options[index+1]);
			var option2 = options[index];
			
			new Insertion.Before(option2, option1);
			this.updateInputs();
		},
		
		updateInputs : function()
		{
			this.updateFromInput();
			this.updateToInput();
		},
		
		updateFromInput : function()
		{
			if (this.inputFrom != null)
			{
				this.inputFrom.value = "";
				
				var options = this.s1.options;
				for(var i=0; i<options.length; i++)
				{
					this.inputFrom.value = this.inputFrom.value + ((i==0)?'' : ',') + options[i].value;
				}
			}
		},
		
		updateToInput:function()
		{
			if (this.inputTo != null)
			{
				this.inputTo.value = "";
				
				var options = this.s2.options;
				for(var i=0; i<options.length; i++)
				{
					this.inputTo.value = this.inputTo.value + ((i==0)?'' : ',') + options[i].value;
				}
			}
		},
		
		init: function(){
			var transfer = this;
			var tre = function(e){
				transfer.transferRight();
			};
			var tle = function(e){
				transfer.transferLeft();
			};
			var tra = function(e){
				transfer.transferAllRight();
			};
			var tla = function(e){
				transfer.transferAllLeft();
			};
			var mu = function(e){
				transfer.moveUp();
			};			
			var md = function(e){
				transfer.moveDown();
			};
			
			this.toRightEach.onclick = tre;
			this.toLeftEach.onclick = tle;
			this.toRightAll.onclick = tra;
			this.toLeftAll.onclick = tla;
			this.up.onclick = mu;
			this.down.onclick = md;
		}
	});
		
		