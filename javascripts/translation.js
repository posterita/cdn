var Translation = {
		callbacks : jQuery.Callbacks(),
		onReady : function(fn){
			this.callbacks.add(fn);
		}
};


Translation.loadTranslation = function(language){
	
	jQuery.i18n.properties({
        name: 'Messages',
        path: 'translation/',
        mode: 'map',
        language: language,       
        callback: function() {        	
        	Translation.callbacks.fire('translations loaded');
        }
    });
};

Translation.setMessages = function(){
	jQuery("[messagekey]").each(function(index, element){
		
		element = jQuery(element);
		
		var messageKey = element.attr('messagekey');
		var translation = jQuery.i18n.prop(messageKey);
		
		var elementType = element.prop("tagName");
		
		if (elementType == 'INPUT')
		{
			element.val(translation);
		}
		else
		{
			element.text(translation);
		}
		
	});
};

Translation.translate = function(key, defaultMessage){
	
	var translatedMsg = jQuery.i18n.prop(key);
	
	if (translatedMsg.indexOf('[') == -1)
	{
		return translatedMsg;
	}
	else if (defaultMessage != null && defaultMessage != undefined)
		{
			return defaultMessage;
		}
	
	return translatedMsg;
};

Translation.onReady(function(){
	console.log('Translation -> ready');
	
	Translation.setMessages();
});

jQuery(document).ready(function(){
	var language = CookieManager.readCookie('preference.language');
	
	if ( language == undefined || language == null || language == '' )
	{
		language = 'en';
	}
	
	Translation.loadTranslation(language);
}); 
