var TreeManager = Class.create({
	
	initialize : function(divId, treeName, controllerId, className)
	{
		this.treeId = divId;
		this.treeName = treeName;
		this.controllerId = controllerId;
		this.className = className;
		this.loadTreeUrl = 'TreeAction.do?action=getTree&tree=' + this.treeName;
		this.selectedNodesIds = null;
		this.tree = new DynaTree(this.treeId, this.treeName);
		this.children = null;
		this.isLoadDefaultNode = true;
		
		if (this.className != null)
		{
			this.loadTreeUrl = this.loadTreeUrl + '&controller=' +
	 		this.className;
		}
		
		if (this.controllerId != null)
		{
			this.loadTreeUrl = this.loadTreeUrl + '&id=' + this.controllerId;
		}
		
		this.iframe = null;
		this.dialog = null;
		
		if (window.top != window.self)
		{
			this.iframe = parent.document.getElementById('window-iframe');
			this.dialog = parent.dialog;
			
			if (this.dialog == null)
			{
				this.dialog = dialog;
			}
			
			if (this.iframe == null)
			{
				this.iframe = $('window-iframe');
			}
		}
		else
		{
			this.iframe = $('window-iframe');
			this.dialog = dialog;
		}
	},
	
	init : function()
	{
		this.tree.setLoadTreeUrl(this.loadTreeUrl);
		this.tree.setTreeManager(this);
		this.tree.init();
	},
	
	refreshTree : function()
	{
		this.init();
	},
	
	loadNode : function(node)
	{
		if (node.data.controller == null || node.data.controller == 'null')
		{
			return;
		}
		
		if (this.dialog != null)
		{
			this.dialog.show();
		}
		
		//this.dialog.show();
		
		var url = 'ModelAction.do?action=load&controller=' + node.data.controller +
		'&processKey=RV_POS_' + node.data.controller + '&tree=' + node.data.tree + '&id=' + node.data.id;
		
		var params = "";
		
		if (node.data.parentId != null)
		{
			params = params + '{"prefix":' + node.data.prefix + ',"columnName":' + node.data.parentKeyName + ',"value":' + node.data.parentId +'}';
		
			url = url + '&fieldsValue=' + params;
		
		}

	    this.iframe.src = url;
	},
	
	setSelectedNodesIds : function(selectedNodesIds)
	{
		this.selectedNodesIds = selectedNodesIds;
	},
	
	getSelectedNodesIds : function()
	{
		return this.selectedNodesIds;
	},
	
	setLoadTreeUrl : function(url)
	{
		this.loadTreeUrl = url;
	},
	
	getTree : function()
	{
		return this.tree;
	},
	
	getChildren : function()
	{
		return this.children;
	},
	
	setChildren : function(children)
	{
		this.children = children;
	},
	
	selectNodes : function(keys)
	{
		for(var i=0; i<keys.length; i++)
		{
			this.tree.selectNode(keys[i]);
		}
	},
	
	deSelectNodes : function(keys)
	{
		for(var i=0; i<keys.length; i++)
		{
			this.tree.deSelectNode(keys[i]);
		}
	},
	
	fireOnSelectEvent : function(selectedNodesIds)
	{
		this.setSelectedNodesIds(selectedNodesIds);
	},
	
	setIsLoadDefaultNode : function(isLoadDefaultNode)
	{
		this.isLoadDefaultNode = isLoadDefaultNode;
	}
});


var DynaTree = Class.create({
	initialize : function(treeId, treeName)
	{
		this.treeId = treeId;
		this.treeName = treeName;
		this.treeManager = null;
		this.tree = null;
		this.loadTreeUrl = null;
		this.imagePath = "images/tree-view/";
        this.persist = false;
        this.minExpandLevel = 2;
        this.autoFocus = false;
        this.isCheckbox = false;
        this.selectMode = 2;
		this.noLink = false;
		this.children = null;
	},
	
	init : function()
	{
		var treeId = this.treeId;
		var treeManager = this.treeManager;
		var isCheckbox = this.isCheckbox;
		
		this.destroyTree();

		jQuery(this.treeId).dynatree({
			
            imagePath : this.imagePath,
            
            persist : this.persist,
            
            minExpandLevel : this.minExpandLevel,
            
            autoFocus : this.autoFocus,
            
            checkbox : this.isCheckbox,
            
            selectMode : this.selectMode,
            
            noLink : this.noLink,
            
            children : this.children,
            
            initAjax : {
           	 url: this.loadTreeUrl
        	},
		
             onPostInit : function(isReloading, isError)
             {
            	 if (treeManager.isLoadDefaultNode)
            	 {
            		 var node = jQuery(treeId).dynatree("getTree").getNodeByKey("default");
                	 
                	 if (node != null)
                	 {
     	                 node.data.focus = true;
     	                 node.data.activate = false;
     	                 treeManager.loadNode(node);
                	 }
            	 }
             },
             
             onActivate : function(node)
             {
             	if (!isCheckbox)
             	{
             		treeManager.loadNode(node);
                    node.data.activate = false;
                    node.data.focus = true;
             	}
             },
             
             onDeactivate : function(node)
             {
             	if (!isCheckbox)
             	{
             		node.data.addClass = 'default';
             	}
             },
             
             onSelect : function(flag, node)
             {
            	if (isCheckbox)
            	{
		            if (!flag)
	            	{
	            	}
		            var selectedNodes = node.tree.getSelectedNodes();
		            
		            var selectedNodesIds = jQuery.map(selectedNodes, function(node){
		                return node.data.key;
		            });
		            
		            //treeManager.setSelectedNodesIds(selectedNodesIds);
		            treeManager.fireOnSelectEvent(selectedNodesIds);
            	}
             }
         });
		
		this.tree = jQuery(treeId).dynatree("getTree");
	},
	
	setTreeManager : function(treeManager)
	{
		this.treeManager = treeManager;
	},
	
	destroyTree : function()
	{
		jQuery(this.treeId).dynatree("destroy");
	},
	
	setLoadTreeUrl : function(url)
	{
		this.loadTreeUrl = url;
	},
	
	setMinExpandLevel : function(minExpandLevel)
	{
		this.minExpandLevel = minExpandLevel;
	},
	
	setIsCheckbox : function(isCheckbox)
	{
		this.isCheckbox = isCheckbox;
	},
	
	setSelectMode : function(selectMode)
	{
		this.selectMode = selectMode;
	},
	
	setnoLink : function(noLink)
	{
		this.noLink = noLink;
	},
	
	getChildren : function()
	{
		return this.children;
	},
	
	setChildren : function(children)
	{
		this.children = children;
	},
	
	selectNode : function(key)
	{
		//this.tree.selectKey(key);
		var node = this.tree.getNodeByKey(key);
		node.select(true);
	},
	
	deSelectNode : function(key)
	{
		var selectedNodes = this.tree.getSelectedNodes();
        
        var selectedNodesIds = jQuery.map(selectedNodes, function(node){
            return node.data.key;
        });
        
        if (selectedNodesIds.indexOf(key) == -1)
        {
        	var node = this.tree.getNodeByKey(key);
        	
        	if (node != null)
    		{
        		alert(key);
    		}
        }
	}
});

