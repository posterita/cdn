var getURL = function(url){
    /*TODO: check network*/
    window.location = url;
};

var LOGGER = {
        enable:false,
        info:function(msg){if(this.enable)console.info(msg);},
        warn:function(msg){if(this.enable)console.warn(msg);},
        error:function(msg){if(this.enable)console.error(msg);}
};

/******************************************************************************/
var ViewPort = {
    getHeight : function() {
        if (window.innerHeight!=window.undefined) return window.innerHeight;
        if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
        if (document.body) return document.body.clientHeight;
        return window.undefined;
    },
   
    getWidth : function() {
        if (window.innerWidth!=window.undefined) return window.innerWidth;
        if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth;
        if (document.body) return document.body.clientWidth;
        return window.undefined;
    },
   
    getScrollXY :function () {
        var scrOfX = 0, scrOfY = 0;
        if( typeof( window.pageYOffset ) == 'number' ) {
          //Netscape compliant
          scrOfY = window.pageYOffset;
          scrOfX = window.pageXOffset;
        } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
          //DOM compliant
          scrOfY = document.body.scrollTop;
          scrOfX = document.body.scrollLeft;
        } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
          //IE6 standards compliant mode
          scrOfY = document.documentElement.scrollTop;
          scrOfX = document.documentElement.scrollLeft;
        }
        return [ scrOfX, scrOfY ];
    },
   
    center:function(element){
        var xPos = (document.viewport.getWidth() - element.getDimensions().width)/2;   
        var yPos = (document.viewport.getHeight() - element.getDimensions().height)/2;
                               
        element.setStyle({
            postion: "absolute",
            left : (xPos) + "px",
            top  : (yPos - 40) + "px"}
        );
    }
};//ViewPort


/* CookieManager to create, read and erase cookies */
var CookieManager = {
        createCookie:function(name,value,days){
            if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
            }
            else var expires = "";
            document.cookie = name+"="+value+expires+"; path=/";
        },
       
        readCookie:function(name){
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        },
       
        eraseCookie:function(name){
            this.createCookie(name,"",-1);
        }
};

/* PreferenceManager loads and saves preferences cookies */
var PreferenceManager = {
        savePreference:function(name, value){
            CookieManager.eraseCookie(name);
            CookieManager.createCookie(name, value, 360);
        },
       
        getPreference:function(name){
            return CookieManager.readCookie(name);
        },
       
        CONSTANTS : {
            SEARCH_RESULT_LENGTH : 'preference.searchResultLength',
            MINIMUM_CHARACTERS : 'preference.minimumCharacters',
            LANGUAGE : 'preference.language',
            PRINTER : 'preference.printer',
            PRINTER_TYPE : 'preference.printerType',
            PRINTER_LINE_WIDTH : 'preference.printerLineWidth',
            BARCODE_PRINTER : 'preference.barcodePrinter',
            BARCODE_PRINTER_TYPE : 'preference.barcodePrinterType',
            BARCODE_PRINTER_TEMPLATE : 'preference.printerTemplate',
            ENABLE_PRINTING : 'preference.enablePrinting',
            ENABLE_CASH_DRAWER : 'preference.enableCashDrawer',
            PRINT_MERCHANT_COPY : 'preferences.printMerchantCopy',
            POLE_DISPLAY : 'preference.poleDisplay',
            ENABLE_POLE_DISPLAY : 'preference.enablePoleDisplay',

            ACCEPT_CASH_PAYMENT : 'preference.acceptCashPayment',
            PRINT_CASH_RECEIPT : 'preference.printCashReceipt',
            PRINT_CASH_RECEIPT_COPY : 'preference.printCashReceiptCopy',

            ACCEPT_CARD_PAYMENT : 'preference.acceptCardPayment',
            PRINT_CARD_RECEIPT : 'preference.printCardReceipt',
            PRINT_CARD_RECEIPT_COPY : 'preference.printCardReceiptCopy',

            ACCEPT_EXTERNALCARD_PAYMENT : 'preference.acceptExternalCardPayment',
            PRINT_EXTERNALCARD_RECEIPT : 'preference.printExternalCardReceipt',
            PRINT_EXTERNALCARD_RECEIPT_COPY : 'preference.printExternalCardReceiptCopy',

            ACCEPT_CHEQUE_PAYMENT : 'preference.acceptChequePayment',
            PRINT_CHEQUE_RECEIPT : 'preference.printChequeReceipt',
            PRINT_CHEQUE_RECEIPT_COPY : 'preference.printChequeReceiptCopy',

            ACCEPT_MIX_PAYMENT : 'preference.acceptMixPayment',
            PRINT_MIX_RECEIPT : 'preference.printMixReceipt',
            PRINT_MIX_RECEIPT_COPY : 'preference.printMixReceiptCopy',

            ACCEPT_ONCREDIT_PAYMENT : 'preference.acceptOnCreditPayment',
            PRINT_ONCREDIT_RECEIPT : 'preference.printOnCreditReceipt',
            PRINT_ONCREDIT_RECEIPT_COPY : 'preference.printOnCreditReceiptCopy',

            ACCEPT_VOUCHER_PAYMENT : 'preference.acceptVoucherPayment',
            PRINT_VOUCHER_RECEIPT : 'preference.printVoucherReceipt',
            PRINT_VOUCHER_RECEIPT_COPY : 'preference.printVoucherReceiptCopy',
            
            ENABLE_OFFLINE : 'preference.enable.offline',
            
            ENABLE_GENIUS_PAYMENT : 'preference.enableGeniusPayment'
        }
};


/*PoleDisplayManager communicates with pole display*/
var PoleDisplay_ESC_COMMANDS = {
        CLEAR:'\x0C',
        LINE_FEED:'\x0A',
        MOVE_LEFT: '\x0D'
};

var PoleDisplayManager = {
        applet : null,
       
        printTestData:function(){
            var printJob = PoleDisplay_ESC_COMMANDS.CLEAR + "This is a test message";           
            this.sendPrintJob(printJob);
        },
       
        clearDisplay:function(){
            var printJob = PoleDisplay_ESC_COMMANDS.CLEAR;           
            this.sendPrintJob(printJob);
        },
       
        display:function(line1,line2){
            /* clear display */
            var printJob = PoleDisplay_ESC_COMMANDS.CLEAR;
            printJob = printJob + line1;
           
            if(line2){
                printJob = printJob + PoleDisplay_ESC_COMMANDS.LINE_FEED + PoleDisplay_ESC_COMMANDS.MOVE_LEFT + line2;
            }
           
            this.sendPrintJob(printJob);
        },
       
        sendPrintJob:function(printJob){
            /* is printing allowed */
            if(this.isPoleDisplayPresent()){
                /* do nothing */
            }
            else{
                LOGGER.warn('PoleDisplay is not allowed! Preferences[PoleDisplay]');
                return;
            }
           
            /*printing via applet*/
            var applet = this.getPrintApplet();
           
            if(applet == null){
                LOGGER.error('Could not locate Printer Applet!');
                return false;
            }
                       
            /* see PosteritaPrinterApplet.addJob(str) */
            try{
                applet.addJob(printJob);
                return true;
            }
            catch (xception){
                LOGGER.error('Could not send job to Pole Display! An error has occurred: ' + e.message);
                return false;
            }           
        },
       
        getPrintApplet:function(){
           
            if(this.applet) return this.applet;
           
            var applet = null;
           
            if(document.applets.length > 0){
                applet = document.applets[0];
            }
            else{
                if(window.parent.frames){
                    if(window.parent.frames[1].document.applets.length > 0){
                        applet = window.parent.frames[1].document.applets[0];
                    }
                }
            }
           
            if(applet == null) return null;
           
            /* set printer name */
            var printer = this.getPrinterName();
            if (printer && printer != null && printer.length > 0 && printer != 'default' && printer != 'error'){   
                applet.setPrinterName(printer);
            }
           
            return applet;
        },
       
        isPoleDisplayPresent:function(){
            var present = PreferenceManager.getPreference('preference.enablePoleDisplay');
            return ('true' == present);
        },
               
        getPrinterName:function(){
            var printer = PreferenceManager.getPreference('preference.poleDisplay');
            return printer || 'default';
        },       
       
        setPrinterName:function(printer){
            this.applet.setPrinterName(printer);
        }
};


/*SessionMonitor monitors uses activity*/
var SessionMonitor = {
        /*30 minutes*/
        maxInactiveInterval:30*60,
        elapsedInactiveInterval:-60,
        isActive:false,
        start:function(){
            this.isActive = true;
            this.timeout = setInterval("SessionMonitor.incrementInactiveInterval()",60000);
        },
        incrementInactiveInterval:function(){
            this.elapsedInactiveInterval = this.elapsedInactiveInterval + 60;
           
            if(this.elapsedInactiveInterval >= this.maxInactiveInterval){               
                this.stop();
                this.notifyTimeout();
                return;
            }           
        },
        stop:function(){
            clearTimeout(this.timeout);
            this.elapsedInactiveInterval = 0;
            this.isActive = false;
        },
        reset:function(){
            if(this.isActive){
                this.elapsedInactiveInterval = 0;
                return true;
            }
            return false;
        },
        isSessionActive:function(){
            return this.isActive;
        },
        notifyTimeout:function(){
            var html = "Your session has timeout! <br> <button onclick='login()'>Login</button>";
            new Dialog(html).show();
        }
};

var DateUtil = {
        /* return current date yyyy-mm-dd hh:mm:ss*/
        getCurrentDate:function(){
   
            var today = new Date();
           
            var year = today.getFullYear();
            var month = today.getMonth() + 1;                   
            var date = today.getDate();
            var hours = today.getHours();
            var minutes = today.getMinutes();
            var seconds = today.getSeconds();
           
            if(month < 10) month = '0' + month;
            if(date < 10) date = '0' + date;
            if(hours < 10) hours = '0' + hours;
            if(minutes < 10) minutes = '0' + minutes;
            if(seconds < 10) seconds = '0' + seconds;
           
            var currentDate = year + "-" + month + "-" + date +
                " " + hours + ":" + minutes + ":" + seconds;
           
            return currentDate;
        },
       
        getReportStartOfMonthDate:function(){
            var today = new Date();
           
            var year = today.getFullYear();
            var month = today.getMonth() + 1;
           
            if(month < 10) month = '0' + month;
           
            var startOfMonthDate = year + "-" + month + "-" + "01";
           
            return startOfMonthDate;
        }
};

var tabPanel = {
        btns:null,
        tabPanes:null,
        tabPaneIndex:0,
        activePane:null,
        activePaneIndex:0,
        onChangeHandler:function(){},
        render:function(){
            for(var i=0; i<this.btns.length; i++)
            {
                var btn = this.btns[i];
                var tabPane = this.tabPanes[i];

                //add behaviour to tabs
                btn.tabPane = tabPane;
                btn.tabPanel = this;

                btn.onclick = function(){
                    this.tabPanel.showTabPane(this.tabPane);
                };

                //add behaviour to panes
                tabPane.btn = btn;
                tabPane.tabPanel = this;
                tabPane.render = function()
                {
                    if(this == this.tabPanel.activePane)
                    {
                        Element.show(this);
                        this.btn.addClassName('active');
                    }
                    else
                    {
                        Element.hide(this);
                        this.btn.removeClassName('active');
                    }
                };
            }
           
            /*check active panel index*/
            if(this.activePaneIndex > this.tabPanes.length){
                this.activePaneIndex = 0;
            }

            this.showTabPane(this.tabPanes[this.activePaneIndex]);                   
   
        },/*render*/
        showTabPane:function(tabPane){

            this.activePane = tabPane;
           
            for(var i=0; i<this.tabPanes.length; i++)
            {
                var pane = this.tabPanes[i];
                if(this.activePane == pane){
                    this.activePaneIndex = i;
                }
               
                pane.render();
            }
           
            this.onChangeHandler();
        }
       
};

var ClockOrder = {
		tick : function(){
			var currentTime = new Date ( );
			
			var hours = currentTime.getHours();
			var minutes = currentTime.getMinutes();
			var seconds = currentTime.getSeconds();
			var year = currentTime.getFullYear();
			

			var currentTimeString = currentTime.toString();
			var d = currentTimeString.split(' ');
		
			var day = d[0];
			var month = d[1];
			var date = d[2];
			var year = d[3];
			var time = d[4];
			var timezone = d[5];

			if(hours < 10) hours = '0' + hours;
			if(minutes < 10) minutes = '0' + minutes;
			if(seconds < 10) seconds = '0' + seconds;
			
			var html = date + ' ' +month + ' ' + year.toString(10).substring(2,4);
			
			var htmlHoursMins = hours + ':' + minutes;
			
			for(var i =0; i<$$(this.container).length; i++)
			{
				$$(this.container)[i].innerHTML = html;
				$$(this.containerHoursMins)[i].innerHTML = htmlHoursMins;
			}
		}, 
		
		container : '.clock-container-order',
		
		start : function(){
			if($$(this.container) == null) return;
			this.tick();
			setInterval('ClockOrder.tick()', 1000 );
		},
		
		containerHoursMins : '.clock-container-hours-mins',
		
		start : function(){
			if($$(this.containerHoursMins) == null) return;
			this.tick();
			setInterval('ClockOrder.tick()', 1000 );
		}
};


var Clock = {
        tick : function(){
            var currentTime = new Date ( );
           
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            var seconds = currentTime.getSeconds();
           

            var currentTimeString = currentTime.toString();
            var d = currentTimeString.split(' ');
       
            var day = d[0];
            var month = d[1];
            var date = d[2];
            var year = d[3];
            var time = d[4];
            var timezone = d[5];

            if(hours < 10) hours = '0' + hours;
            if(minutes < 10) minutes = '0' + minutes;
            if(seconds < 10) seconds = '0' + seconds;
           
            var html = month + ' ' + date + ' ' + year + ' ' + hours + ':' + minutes;
            $(this.container).innerHTML = html;
        },
       
        container : 'clock-container',
       
        start : function(){
            if($(this.container) == null) return;
            this.tick();
            setInterval('Clock.tick()', 1000 );
        }
};



try
{
	Event.observe(window,'load',Clock.start.bind(Clock),false);
	Event.observe(window,'load',ClockOrder.start.bind(ClockOrder),false);
}
catch(e){
	
}

var MessageDisplay = {
        container : 'message-display-container',
        info : function(message1, message2){
        	$('error-msg-box').style.display = 'none';
			$('ErrorMessages').innerHTML = ''
        	$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = message1 + ' ' + message2;
        },
        save : function(){
        	$('error-msg-box').style.display = 'none';
			$('ErrorMessages').innerHTML = ''
        	$('success-msg-box').style.display = 'block';
			$('SuccessMessages').innerHTML = Translation.translate('saved','Saved');
        },
        error : function(){
            $('success-msg-box').style.display = 'none';
            $('SuccessMessages').innerHTML = '';
			$('error-msg-box').style.display = 'block';
			$('ErrorMessages').innerHTML = Translation.translate('error.message','Error');
        }
};

var SecondaryDisplayManager = {
		
		display : null, /*references display window*/
		
		test : function(){
			var msg = 'This is a test message .. sent on ' + new Date();
			this.displayMessage(msg)
		},
		
		displayMessage : function(msg){
			var display = this.getDisplay();
			if(display == null){
				console.error('Failed to get secondary display');
				return;
			}
			
			//display.document.body.innerHTML = msg;
			display.document.getElementById('receipt').innerHTML = msg;
			//display.document.body.getElementsByTagName('div').test.innerHTML = msg;
		},
		
		getDisplay : function(){
			if(this.display) return this.display;
			
			/* look for secondary_display_frame frame */
			for(var i=0; i<window.parent.frames.length; i++){
				var frame = window.parent.frames[i];
				if(frame.name == 'secondary_display_frame'){
					if(frame.getDisplay){
						this.display = frame.getDisplay();
					}
					
					break;
				}
			}
			
			if(this.display == null){
				console.debug('Creating display outside frame!!!');
				this.display = window.open('jsp/sales/secondary-screen-popup.jsp','secondary','width=' + window.screen.width + ',height=' + window.screen.height + ',left=' + (window.screen.width));
				//this.display = window.open('jsp/sales/secondary-screen-popup.jsp','secondary','width=500,height=500,left=10');
			}
			
			return this.display;
		}
		
		/*
		windowDisplay : null,
		display : function(html){
			console.debug('Secondary Display -- display');
			this.init();			
			this.windowDisplay.document.body.innerHTML = html;
			//this.windowDisplay.document.getElementById('shopping-cart-container').innerHTML = html;
		},
		init: function(){
			if(this.windowDisplay == null){
				console.debug('Secondary Display -- init');
				if(window.parent.frames){
					console.debug('Secondary Display -- init frames');
					if(window.parent.frames.length > 2){
						var windowDisplay = window.parent.frames[2].windowDisplay;
						if(windowDisplay == null){
							windowDisplay = window.open('jsp/sales/secondary-screen-popup.jsp','secondary','width=300,height=700');
							window.parent.frames[2].windowDisplay = windowDisplay;
							this.windowDisplay = windowDisplay;
						}
					}					
				}
			}
			
			if(this.windowDisplay == null){
				console.debug('Secondary Display -- init without frames');
				this.windowDisplay = window.open('jsp/sales/secondary-screen-popup.jsp','secondary','width=300,height=700');
			}
		}
		*/		
		
};

var OfflineManager = {
		enableOffline : function(){
			PreferenceManager.savePreference(PreferenceManager.CONSTANTS.ENABLE_OFFLINE, "true");
		},
		
		disableOffline : function(){
			PreferenceManager.savePreference(PreferenceManager.CONSTANTS.ENABLE_OFFLINE, "false");
		},
		
		isOfflineEnabled : function(){
			var flag = PreferenceManager.getPreference(PreferenceManager.CONSTANTS.ENABLE_OFFLINE);
			return ('true' == flag);
		},
		
		switchOffline : function(){
			if(window.confirm("Do you want to switch to OFFLINE mode?")){
				window.localStorage.setItem('continue-offline-flag', 'true'); /* see offline-monitor.js line 25*/
				
				if(window.parent){
					window.parent.location = "offline/select-user.do";
					return;
				}
				
				window.location = "offline/select-user.do";
			}
			
		}
};

/*
///////////////////////////////////////////////////////////////////////////////////
//JavaScript Magstripe (track 1, track2) data parser object
//
//Mar-22-2005	Modified by Wayne Walrath,
//				Acme Technologies http://www.acmetech.com
//			based on demo source code from www.skipjack.com
//
//USAGE:
//var p = new SwipeParserObj();
//p.dump();  -- returns parsed field values and meta info.
//	-- or --
//get individual field names (see member variables at top of object)
//
//if( p.hasTrack1 ){
//		p.surname;
//		p.firstname;
//		p.account;
//		p.exp_month + "/" + p.exp_year;
//	}
///////////////////////////////////////////////////////////////////////////////////
*/
function SwipeParserObj(strParse) {
 /*//////////////////// member variables ///////////////////////*/
 this.input_trackdata_str = strParse;
 this.account_name = null;
 this.surname = null;
 this.firstname = null;
 this.acccount = null;
 this.exp_month = null;
 this.exp_year = null;
 this.track1 = null;
 this.track2 = null;
 this.hasTrack1 = false;
 this.hasTrack2 = false;
 /*////////////////////////// end member fields ////////////////*/

 sTrackData = this.input_trackdata_str; /* Get the track data */

 /*
 //-- Example: Track 1 & 2 Data
 //-- %B1234123412341234^CardUser/John^030510100000019301000000877000000?;1234123412341234=0305101193010877?
 //-- Key off of the presence of "^" and "="

 //-- Example: Track 1 Data Only
 //-- B1234123412341234^CardUser/John^030510100000019301000000877000000?
 //-- Key off of the presence of "^" but not "="

 //-- Example: Track 2 Data Only
 //-- 1234123412341234=0305101193010877?
 //-- Key off of the presence of "=" but not "^"
 */

 if (strParse != '') {     

     
     nHasTrack1 = strParse.indexOf("^");
     nHasTrack2 = strParse.indexOf("=");

     
     this.hasTrack1 = bHasTrack1 = false;
     this.hasTrack2 = bHasTrack2 = false;
     if (nHasTrack1 > 0) {
         this.hasTrack1 = bHasTrack1 = true;
     }
     if (nHasTrack2 > 0) {
         this.hasTrack2 = bHasTrack2 = true;
     }     
     
     bTrack1_2 = false;
     bTrack1 = false;
     bTrack2 = false;
     
     if ((bHasTrack1) && (bHasTrack2)) {
         bTrack1_2 = true;
     }
     if ((bHasTrack1) && (!bHasTrack2)) {
         bTrack1 = true;
     }
     if ((!bHasTrack1) && (bHasTrack2)) {
         bTrack2 = true;
     }
        
     bShowAlert = false;
    
     if (bTrack1_2) {
         
         strCutUpSwipe = '' + strParse + ' ';
         arrayStrSwipe = new Array(4);
         arrayStrSwipe = strCutUpSwipe.split("^");

         var sAccountNumber, sName, sShipToName, sMonth, sYear;

         if (arrayStrSwipe.length > 2) {
             this.account = stripAlpha(arrayStrSwipe[0].substring(1, arrayStrSwipe[0].length));
             this.account_name = arrayStrSwipe[1];
             this.exp_month = arrayStrSwipe[2].substring(2, 4);
             this.exp_year = '20' + arrayStrSwipe[2].substring(0, 2);

             if (sTrackData.substring(0, 1) == '%') {
                 sTrackData = sTrackData.substring(1, sTrackData.length);
             }

             var track2sentinel = sTrackData.indexOf(";");
             if (track2sentinel != -1) {
                 this.track1 = sTrackData.substring(0, track2sentinel);
                 this.track2 = sTrackData.substring(track2sentinel);
             }

             var nameDelim = this.account_name.indexOf("/");
             if (nameDelim != -1) {
                 this.surname = this.account_name.substring(0, nameDelim);
                 this.firstname = this.account_name.substring(nameDelim + 1);
             }
         } else 
         {
             bShowAlert = true; 
         }
     }

     /*
     //-----------------------------------------------------------------------------
     //--- Track 1 only cards
     //--- Ex: B1234123412341234^CardUser/John^030510100000019301000000877000000?
     //-----------------------------------------------------------------------------  
     */  
     if (bTrack1) {
        
         strCutUpSwipe = '' + strParse + ' ';
         arrayStrSwipe = new Array(4);
         arrayStrSwipe = strCutUpSwipe.split("^");

         var sAccountNumber, sName, sShipToName, sMonth, sYear;

         if (arrayStrSwipe.length > 2) {
             this.account = sAccountNumber = stripAlpha(arrayStrSwipe[0].substring(1, arrayStrSwipe[0].length));
             this.account_name = sName = arrayStrSwipe[1];
             this.exp_month = sMonth = arrayStrSwipe[2].substring(2, 4);
             this.exp_year = sYear = '20' + arrayStrSwipe[2].substring(0, 2);

             /*
             //--- Different card swipe readers include or exclude the % in
             //--- the front of the track data - when it's there, there are
             //---   problems with parsing on the part of credit cards processor - so strip it off
             */
             if (sTrackData.substring(0, 1) == '%') {
                 this.track1 = sTrackData = sTrackData.substring(1, sTrackData.length);
             }

             /*
             //--- Add track 2 data to the string for processing reasons
             //        if (sTrackData.substring(sTrackData.length-1,1) != '?')  //--- Add a ? if not present
             //        { sTrackData = sTrackData + '?'; }
             */
             this.track2 = ';' + sAccountNumber + '=' + sYear.substring(2, 4) + sMonth + '111111111111?';
             sTrackData = sTrackData + this.track2;

             /*--- parse name field into first/last names */
             var nameDelim = this.account_name.indexOf("/");
             if (nameDelim != -1) {
                 this.surname = this.account_name.substring(0, nameDelim);
                 this.firstname = this.account_name.substring(nameDelim + 1);
             }

         } else 
         {
             bShowAlert = true; 
         }
     }

     /*
     //-----------------------------------------------------------------------------
     //--- Track 2 only cards
     //--- Ex: 1234123412341234=0305101193010877?
     //----------------------------------------------------------------------------- 
     */   
     if (bTrack2) {
         
         nSeperator = strParse.indexOf("=");
         sCardNumber = strParse.substring(1, nSeperator);
         sYear = strParse.substr(nSeperator + 1, 2);
         sMonth = strParse.substr(nSeperator + 3, 2);

          this.account = sAccountNumber = stripAlpha(sCardNumber);
         this.exp_month = sMonth = sMonth;
         this.exp_year = sYear = '20' + sYear;

         /*
         //--- Different card swipe readers include or exclude the % in the front of the track data - when it's there, 
         //---  there are problems with parsing on the part of credit cards processor - so strip it off
         */
         if (sTrackData.substring(0, 1) == '%') {
             sTrackData = sTrackData.substring(1, sTrackData.length);
         }

     }

     /*
     //-----------------------------------------------------------------------------
     //--- No Track Match
     //----------------------------------------------------------------------------- 
     */   
     if (((!bTrack1_2) && (!bTrack1) && (!bTrack2)) || (bShowAlert)) {
         /* alert('Difficulty Reading Card Information.\n\nPlease Swipe Card Again.'); */
     }    

 } 

 this.dump = function () {
     var s = "";
     var sep = "\r"; 
     s += "Name: " + this.account_name + sep;
     s += "Surname: " + this.surname + sep;
     s += "first name: " + this.firstname + sep;
     s += "account: " + this.account + sep;
     s += "exp_month: " + this.exp_month + sep;
     s += "exp_year: " + this.exp_year + sep;
     s += "has track1: " + this.hasTrack1 + sep;
     s += "has track2: " + this.hasTrack2 + sep;
     s += "TRACK 1: " + this.track1 + sep;
     s += "TRACK 2: " + this.track2 + sep;
     s += "Raw Input Str: " + this.input_trackdata_str + sep;

     return s;
 }

 function stripAlpha(sInput) {
     if (sInput == null) return '';
     return sInput.replace(/[^0-9]/g, '');
 }

}
