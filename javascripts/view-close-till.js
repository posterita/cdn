var ViewCloseTillManager = Class.create({

	initialize : function()
	{
		this.cashJournalId = 0;
		this.cashDifference = 0;
		this.cashier = null;
		this.printData = null;
		this.htmlReceipt = null;
		this.pseudoPDF = null;
	},

	initData : function(json)
	{
		this.cashJournalId = json.header.cashJournalId;
		this.cashDifference = json.header.cashDifference;
		this.printData = CloseTill.format(json);
		this.receiptDataJSON = json;
		this.htmlReceipt = CloseTill.formatHTML(json);
		this.pseudoPDF = CloseTill.formatPseudoPDF(json);
		
		this.cashier = json.header.cashier;
		var divEmployeeName = $$('div.profileemployee');
		if (divEmployeeName != null)
		{
			for (var i=0; i<divEmployeeName.length; i++)
			{
				var div = divEmployeeName[i];
				div.innerHTML = this.cashier;
			}
		}
		
		this.initBtns();
	},

	initBtns : function()
	{
		$('printReceipt').onclick = this.printReceipt.bind(this);
		$('exportCSV').onclick = this.exportCSV.bind(this);
		$('exportPDF').onclick = this.exportPDF.bind(this);
		
		var cashJournalId = this.cashJournalId;
		
		jQuery('#exportSales').on('click', function(e){
			getURL('report/TerminalClosureSales?format=csv&c_cash_id=' + cashJournalId);
		});
	},
	
	printReceipt : function()
	{
		var request = CloseTill.formatAsWebPRNT(this.receiptDataJSON);
		PrinterManager.print(request);
	},	
	
	exportCSV : function()
	{
		$('exportAction').value = 'exportCSV';
		$('data').value = getJSONParam();
		
		var form = $('view-close-till-form');
		form.submit();
	},
	
	exportPDF : function()
	{
		$('exportAction').value = 'exportPDF';
		$('data').value = getJSONParam();
		
		var form = $('view-close-till-form');
		form.submit();
		//getURL("TillAction.do?action=exportPDF&data=" + getJSONParam());
	},
	
	showPseudoPDF : function()
	{
		$('pseudoPDF').innerHTML = this.pseudoPDF;
		
		if (this.receiptDataJSON.header.closingDate != null){
			$(this.cashDifference == 0 ? 'congrats' : 'oops').style.display = 'block';
		}
		
	},

	showHtmlReceipt : function()
	{
		$('receipt').innerHTML = this.htmlReceipt;
	},

	setPrintData : function(printData)
	{
		this.printData = printData;
	},

	setHtmlReceipt : function(htmlReceipt)
	{
		this.htmlReceipt = htmlReceipt;
	},

	setPseudoPDF : function(pseudoPDF)
	{
		this.pseudoPDF = pseudoPDF;
	},

	getPrintData : function()
	{
		return this.printData;
	},

	getHtmlReceipt : function()
	{
		return this.htmlReceipt;
	},

	getPseudoPDF : function()
	{
		return this.pseudoPDF;
	}				
});
