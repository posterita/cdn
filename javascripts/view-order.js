/*Create Instance Product Popup Panel*/
var CreateInstanceProductsPanel = Class.create(PopUpBase, {
    initialize: function() {
        this.createPopUp($('create-instance-products-popup'));

        this.numberOfSerialNoTextField = $('create-instance-products-popup-NumberOfSerialNos-textField');
        this.saveBtn = $('create-instance-products-popup-save-button');
        this.closeBtn = $('create-instance-products-popup-close-button');
        this.serialNosListContainer = $('serialNosListContainer');
        this.label = $('label');
        this.parentListSelect = $('parentList');
        this.orderId = $('orderId').value;
        this.saveBtn.onclick = this.save.bind(this);
        this.closeBtn.onclick = this.hide.bind(this);
        this.numberOfSerialNoTextField.onkeyup = this.renderList.bind(this);
        this.parentListSelect.onchange = this.onParentChange.bind(this);
        //$('parents').value = '[{"productId":10162893,"qty":4,"name":"dvd"}, {"productId":10162894,"qty":5,"name":"refrigerator"}]';
        this.parents = eval('(' + $('parents').value.unescapeHTML() + ')');
        this.selectedParentId = null;
        this.params = null;
        this.hash = new Hash();
        this.currentParent = null;
        this.generateSerialNosButton = $('generate-serial-nos');
        this.dialog = null;

        this.init();
    },

    init: function() {
        this.resetPanel();
        this.parentListSelect.innerHTML = '';

        for (var i = 0; i < this.parents.length; i++) {
            var option = document.createElement('option');
            option.setAttribute('value', this.parents[i].productId);
            option.innerHTML = this.parents[i].name;

            this.parentListSelect.appendChild(option);
            this.parentListSelect.options[0].selected = true;

            var productId = this.parentListSelect.options[this.parentListSelect.options.selectedIndex].value;

            if (this.parents[i].productId == productId) {
                this.currentParent = this.parents[i];
                this.hash.set(productId, new ArrayList());
            }
        }
    },

    save: function() {
        this.onParentChange();
        var isValid = false;

        for (var i = 0; i < this.hash.keys().length; i++) {
            var arrayList = this.hash.get(this.hash.keys()[i]);
            var flag = true;

            if (arrayList.size() == 0) {
                flag = false;
            }

            isValid = isValid || flag;
        }

        if (!isValid) {
            alert(Translation.translate('please.enter.the.number.of.serial.no.to.generate', 'Please enter the number of serial no to generate.'));
            return;
        }

        this.params = this.hash.toJSON();

        //alert(this.params);


        var url = 'OrderAction.do';
        var params = 'action=createInstanceProducts&serialNos=' + this.params + '&orderId=' + this.orderId;
        new Ajax.Request(url, {
            ethod: 'post',
            parameters: params,
            onSuccess: this.onSuccess.bind(this),
            onFailure: this.onFailure.bind(this)
        });

        this.hide();
        this.dialog = new Dialog();
        this.dialog.show();
    },

    onSuccess: function(request) {
        this.dialog.hide();

        var response = eval('(' + request.responseText + ')');

        if (response.success) {
            $('parents').value = response.parents;

            if (!response.isGenerateSerialNo) {
                this.generateSerialNosButton.style.display = 'none';
            } else {
                this.generateSerialNosButton.style.display = 'block';
            }

            alert(Translation.translate('serial.no.generated', 'Serial No generated'));
        } else {
            alert(response.error);
        }
    },

    onFailure: function() {

    },

    renderList: function() {
        var noOfSerialNos = this.numberOfSerialNoTextField.value;
        var parentQty = this.currentParent.qty;

        if (noOfSerialNos > parentQty) {
            alert(Translation.translate('invalid.amount.entered', 'Invalid Amount Entered'));
            return;
        }

        this.label.innerHTML = 'Enter Serial No:';
        this.serialNosListContainer.innerHTML = '';

        for (var i = 1; i <= noOfSerialNos; i++) {
            var div = document.createElement('div');
            div.setAttribute('style', 'padding-bottom:10px');
            var input = document.createElement('input');
            input.setAttribute('id', i);
            input.setAttribute('name', 'serialNoTextField');

            div.appendChild(input);

            this.serialNosListContainer.appendChild(div);
        }
    },

    onParentChange: function() {
        //save current parent state and then load new parent
        //Each time a parent is loaded again, all serial no are reset again
        var serialNoTextFields = document.getElementsByName('serialNoTextField');

        if (serialNoTextFields.length == 0 && this.numberOfSerialNoTextField.value == '') {
            var productId = this.parentListSelect.options[this.parentListSelect.options.selectedIndex].value;

            for (var i = 0; i < this.parents.length; i++) {
                if (productId == this.parents[i].productId) {
                    this.currentParent = this.parents[i];
                    break;
                }
            }
            return;
        }

        for (var i = 0; i < serialNoTextFields.length; i++) {
            if (serialNoTextFields[i].value == '') {
                alert(Translation.translate('please.fill.in.all.serial.no', 'Please fill in all serial no'));
                return;
            }
        }

        for (var i = 0; i < serialNoTextFields.length; i++) {
            for (var j = 0; j < serialNoTextFields.length; j++) {
                if (i != j) {
                    if (serialNoTextFields[i].value == serialNoTextFields[j].value) {
                        alert(Translation.translate('duplicate.serial.no', 'Duplicate Serial No!'));
                        return;
                    }
                }
            }
        }

        var productId = this.currentParent.productId;
        var arrayList = this.hash.get(productId);

        if (arrayList == null) {
            arrayList = new ArrayList();
        } else {
            arrayList.clear();
        }

        for (var i = 0; i < serialNoTextFields.length; i++) {
            var hash = new Hash()
            hash.set('serialNo', serialNoTextFields[i].value);
            hash.set('locatorId', this.currentParent.locatorId);
            arrayList.add(hash);
        }

        this.hash.set(productId, arrayList);

        //load new Parent
        this.resetPanel();
        var productId = this.parentListSelect.options[this.parentListSelect.options.selectedIndex].value;

        for (var i = 0; i < this.parents.length; i++) {
            if (this.parents[i].productId == productId) {
                this.currentParent = this.parents[i];
                break;
            }
        }
    },

    resetPanel: function() {
        this.numberOfSerialNoTextField.value = '';
        this.serialNosListContainer.innerHTML = '';
        this.label.innerHTML = '';
    },

    show: function() {

        PopUpBase.show.call(this);
    }
});

/* scripts from view-order */
var requestURL = function(url) {

    BootstrapDialog.show({
        title: '',
        type: BootstrapDialog.TYPE_DEFAULT,
        message: 'Please wait ...',
        closable: false,
        closeByBackdrop: false,
        closeByKeyboard: false,
    });

    window.location = url;
};



var INVOICE = {};
var viewOrderMoreOptions = null;

// force : print receipt even it is disabled in preferences
function printReceipt(force) {

    var openDrawer = false;
    if (receiptJSON.openDrawer == true) {
        openDrawer = true;
    }
    /*
    	printReceipt	  force	  result
    	================================
    		Y				Y		Y
    		Y				N		Y
    		N				Y		Y
    		N				N		N
    */
    if (receiptJSON.printReceipt == true || force == true) {

        receiptJSON.force = force;

        // print receipt
        console.info('printing receipt ..');
        PrinterManager.printReceipt(receiptJSON, openDrawer);

        if (force == true && PrinterManager.logRePrint) {
            PrinterManager.logRePrint(receiptJSON);
        }

    } else {
        if (openDrawer) {
            PrinterManager.print([
                ['OPEN_DRAWER']
            ]);
        }
    }

}

function printGiftReceipt() {
    PrinterManager.printGiftReceipt(receiptJSON);
}

function showProcessingDialog() {
    //new Dialog("",{closeable : false}).show();
}

function init() {
    INVOICE.openAmt = receiptJSON.header.openAmt;
    INVOICE.currencySymbol = receiptJSON.header.currencySymbol;

    jQuery('#more-button').off('click').on('click', function() {
        new ViewOrderMoreOptionsPanels().show();
    });

    jQuery('#open-drawer-button').click(function() {
        /* PrinterManager.showOpenDrawerDialog(); */
        PrinterManager.print([
            ['OPEN_DRAWER']
        ]);
    });

    jQuery('#comment-button').click(function() {
        PopUpManager.clear();
        CommentsPanel.toggle();
    });

    jQuery('#gift-card-button').click(function() {
        PopUpManager.clear();
        checkGiftCardBalance();
    });

    jQuery('#export-pdf-button').click(function() {
        getURL('OrderAction.do?action=exportPDF&c_order_id=' + $c_order_id);
    });


    if ($('export-csv-button')) {
        $('export-csv-button').onclick = function() {
            getURL('DataTableAction.do?action=exportPurchaseCSV&c_order_id=' + $c_order_id);
        };
    }

    jQuery('#print-button').click(function() {
        printReceipt(true);
    });

    if ($('print-gift-button')) {
        $('print-gift-button').onclick = function() {
            PopUpManager.clear();
            printGiftReceipt();
        };
    }

    jQuery('#load-button').click(function() {
        PopUpManager.clear();
        new InvokeOrderPanel().show();
    });


    if ($('edit-invoice-button')) {

        $('edit-invoice-button').onclick = function() {
            PopUpManager.clear();
            var url = 'OrderAction.do?action=editInvoice&orderId=' + $c_order_id;
            changeReference(url);
        }

    }

    if ($('update-bp-button')) {

        $('update-bp-button').onclick = function() {
            PopUpManager.clear();
            var url = 'OrderAction.do?action=setBP&orderId=' + $c_order_id;
            updateBPName(url);
        }

    }

    //add behaviour to void btn
    if ($('void-button')) {
        $('void-button').onclick = function() {
            PopUpManager.clear();
            
            BootstrapDialog.confirm("Do you want to void this " + receiptJSON.header.title + "?", function(voidOrder) {
                if (voidOrder) {
                    showProcessingDialog();
                    requestURL('OrderAction.do?action=voidOrder&orderId=' + $c_order_id);
                }
            });            
            
        };
    };

    //add behaviour to refund btn
    if ($('refund-button')) {
        $('refund-button').onclick = function() {
            PopUpManager.clear();

            var msg = null;

            if (receiptJSON.header.soTrx == true) {
                msg = "Do you want to refund this sales receipt?";
            } else {
                msg = "Do you want to return this purchase?";
            }

            BootstrapDialog.confirm(msg, function(refundOrder) {
                if (refundOrder) {
                    showProcessingDialog();
                    requestURL('OrderAction.do?action=refund&orderId=' + $c_order_id);
                }

            });
        };
    };

    //add behaviour to exchange btn
    if ($('exchange-button')) {
        $('exchange-button').onclick = function() {
            showProcessingDialog();
            requestURL('OrderAction.do?action=exchange&orderId=' + $c_order_id);
        };
    }

    //add behaviour to copy-order-button
    if ($('copy-order-button')) {
        $('copy-order-button').onclick = function() {
            showProcessingDialog();
            requestURL('OrderAction.do?action=copy&orderId=' + $c_order_id);
        };
    };

    //add behaviour to copy-po-button
    if ($('copy-po-button')) {
        $('copy-po-button').onclick = function() {
            showProcessingDialog();
            requestURL('OrderAction.do?action=copyPO&orderId=' + $c_order_id);
        };
    };

    //add generate picking button
    if ($('create-picking-button')) {
        $('create-picking-button').onclick = function() {
            PopUpManager.clear();
            showProcessingDialog();
            requestURL('PickingListAction.do?action=createPickingListFromOrder&orderId=' + $c_order_id);
        };
    };

    //add behaviour to new-order-button
    if ($('new-order-button')) {
        $('new-order-button').onclick = function() {
            showProcessingDialog();
            requestURL('OrderAction.do?action=loadOrderScreen&orderType=' + $receiptJSON.header.orderType);
        };
    };


    if ($('payment-button')) {
        $('payment-button').onclick = function() {
            new PaymentPopUp().show();
        };
    };   
   

    $('bp-button').onclick = function() {
        var bpInfoPanel = new BPInfoPanel();
        bpInfoPanel.setId($receiptJSON.header.c_bpartner_id);
        bpInfoPanel.show();
    };


    if ($('ship-button')) {

        var shipmentMessage = $receiptJSON.header.soTrx == true ? 'Deliver goods?' : 'Receive goods?';

        $('ship-button').onclick = function() {

            PopUpManager.activePopUp.hide();

            new DeliveryPanel().show();
        };
    }

    VoucherReceipt.options = {
        LINE_WIDTH: 40
    };
    VoucherReceipt.data = $receiptJSON;

    if (VoucherReceipt.data.header.paymentRule == 'V') {

        if (VoucherReceipt.data.header.orderType == 'Customer Returned Order' || (VoucherReceipt.data.header.orderType == 'POS Order' && VoucherReceipt.data.header.payAmt < 0)) {
            if (confirm('Do you want to print voucher?')) {
                var voucherPrintData = PrinterManager.getVoucherPrintFormat($receiptJSON);
                PrinterManager.print(voucherPrintData);
            }
        }
    }

    /*init comments*/
    CommentsPanel.init();

    if (receiptJSON.comments.length > 0) {
        CommentsPanel.toggle();
    }

    jQuery(function(){
    	var serialNos = $receiptJSON.parents;
        jQuery('#parents').val(serialNos);
        jQuery('#orderId').val($c_order_id);
    });

    var divs = jQuery('#buttons-right').children('div').length;

    if (divs < 5) {
        jQuery('#buttons-right').children('div:eq(0)').removeClass('col-xs-2 col-md-2').addClass('col-md-3 col-xs-3');
        jQuery('#buttons-right').children('div:eq(1)').removeClass('col-xs-2 col-md-2 padding-left-4').addClass('col-md-3 col-xs-3 padding-left-4');
        jQuery('#buttons-right').children('div:eq(2)').removeClass('col-xs-2 col-md-2 padding-left-4').addClass('col-md-3 col-xs-3 padding-left-4');
        jQuery('#buttons-right').children('div:eq(3)').removeClass('col-md-3 col-xs-3 padding-left-4').addClass('col-md-3 col-xs-3 padding-left-4');


    }


}


var ViewOrderSplitPanel = Class.create({

    initialize: function() {
        this.splitOrderBtn = $('split_order_button');
        this.splitManager = null;
        this.salesRepSelectManager = null;
    },

    init: function(c_order_id, callback) {
        this.salesRepSelectManager = new SalesRepSelectManager();
        this.salesRepSelectManager.addEventListener(this.salesRepSelectManager.EVENT_ADD, this);
        this.salesRepSelectManager.addEventListener(this.salesRepSelectManager.EVENT_REMOVE, this);
        this.salesRepSelectManager.addSalesRepsLoadedListener(this);
        this.salesRepSelectManager.init(c_order_id);
        
        this.initCallback = callback;
    },

    salesRepsLoaded: function() {
        this.splitOrderBtn.onclick = this.showPanel.bind(this);
        var splitSalesReps = new ArrayList();
        var orderSplitSalesRep = new SplitSalesRep(this.salesRepSelectManager.orderSalesRep);
        splitSalesReps.add(orderSplitSalesRep);
        this.loadSplitPanel(splitSalesReps);
    },

    addHandler: function(salesReps) {
        var splitSalesReps = new ArrayList();
        for (var i = 0; i < salesReps.size(); i++) {
            var salesRep = salesReps.get(i);
            var splitSalesRep = new SplitSalesRep(salesRep);
            splitSalesReps.add(splitSalesRep);
        }
        var orderSplitSalesRep = new SplitSalesRep(this.salesRepSelectManager.orderSalesRep);
        splitSalesReps.add(orderSplitSalesRep);
        this.loadSplitPanel(splitSalesReps);
    },

    loadSplitPanel: function(splitSalesReps) {
        OrderAmount.isDraftedOrder = false;
        var orderType = receiptJSON.header.orderType;

        if (orderType == 'POS Order') {
            this.splitManager = new SplitOrderManager(splitSalesReps);
        } else {
            this.splitManager = new SplitReturnManager(splitSalesReps);
        }

        this.splitManager.addSplitPanelClickOkListener(this);
        this.splitManager.addSplitPanelClickCancelListener(this);

        /*notifiy loaded*/
        this.initCallback();
    },

    removeHandler: function(salesReps) {
        var spSalesReps = this.splitManager.getSplitSalesReps();
        for (var i = 0; i < salesReps.size(); i++) {
            var salesRep = salesReps.get(i);
            var splitSalesRep = new SplitSalesRep(salesRep);
            if (spSalesReps.contains(splitSalesRep)) {
                spSalesReps.remove(splitSalesRep);
            }

        }

        var orderSplitSalesRep = new SplitSalesRep(this.salesRepSelectManager.orderSalesRep);
        spSalesReps.add(orderSplitSalesRep);

        this.loadSplitPanel(spSalesReps);

    },

    showPanel: function() {
        this.splitManager.showPanel();
        $('split.order.popup.okButton').focus();
    },


    splitPanelClickOk: function() {
        this.splitManager.hidePanel();
        var json = null;
        var splitSalesReps = this.splitManager.getSplitSalesReps();
        var splitSR = splitSalesReps.clone();

        var refSalesRep = this.splitManager.getRefSalesRep();

        if (splitSalesReps != null) {
            if (refSalesRep != null)
                splitSR.add(refSalesRep);
            json = splitSR.toJSON();
        }

        if (json != null) {
            var url = "OrderAction.do?action=splitOrder";
            var pars = '&orderId=' + receiptJSON.header.c_order_id + '&splitSalesReps=' + json;
            var myAjax = new Ajax.Request(url, {
                method: 'get',
                parameters: pars,
                onSuccess: this.displaySplitSalesRepsInfo.bind(this),
                onFailure: this.splitFailure.bind(this)
            });
        }

    },

    displaySplitSalesRepsInfo: function(request) {
        var response = request.responseText;
        var json = eval('(' + response + ')');
        var splitSR = eval('(' + json.splitSalesReps + ')');

        var enableCarousel = false;
        if (splitSR.length > 6) enableCarousel = true;

        var content = '';

        if (enableCarousel) {
            //content = content + '<ul id="mycarousel" class="jcarousel-skin-tango">';
        }


        for (var i = 0; i < splitSR.length; i++) {
            var splitSalesRep = splitSR[i];
            var name = splitSalesRep.name;
            var amount = splitSalesRep.amount;

            content = content + '<span>' + name + '-' + amount + '</span>';
        }

        $('split.order').innerHTML = content;

    },

    splitFailure: function() {
        alert('failed');
    }
});

Event.observe(window, 'load', function() {
    init();
}, false);

var CommentsPanel = {
    commentPanel: null,
    slipPanel: null,
    commentTextfield: null,
    commentButton: null,
    visible: null,
    tableId: null,
    recordId: null,

    init: function() {
        this.commentPanel = $('comments-container');
        this.slipPanel = $('slip-container');
        this.commentTextfield = $('comments-textfield');
        this.commentDiv = $('comments-div');
        this.commentButton = $('post-button');
        this.commentBadge = $('comment-badge');
        this.visible = false;

        this.commentButton.onclick = this.post.bind(this);


        if (jQuery('.message').length == 0) {
            this.commentBadge.innerHTML = '';
        } else {
            this.commentBadge.innerHTML = jQuery('.message').length;
        }
    },

    toggle: function() {
        if (this.visible) {
            this.commentPanel.hide();

        } else {
            this.commentPanel.show();
        }

        this.visible = !this.visible;
    },

    post: function() {
        var url = "CommentAction.do?action=save";

        var pars = new Hash();
        pars.set('tableId', this.tableId);
        pars.set('recordId', this.recordId);
        pars.set('dateCommented', DateUtil.getCurrentDate());
        pars.set('message', this.commentTextfield.value);

        var myAjax = new Ajax.Request(url, {
            method: 'get',
            parameters: pars,
            onSuccess: this.postSuccess.bind(this),
            onFailure: this.postFailure.bind(this)
        });
    },

    postSuccess: function(request) {
        var response = request.responseText;
        var result = eval('(' + response + ')');

        var comment = result.comment;

        if (window.receiptJSON) {

            window.receiptJSON.comments.push(comment);

        }

        var styleClass1 = 'comment-col comment-even-row1';
        var styleClass2 = 'comment-col comment-even-row2';

        var lastComment = jQuery('.comment-col').last();

        if (lastComment != null && lastComment.length > 0) {
            if (lastComment.attr('class').indexOf('even') > 0) {
                styleClass1 = 'comment-col comment-odd-row1';
                styleClass2 = 'comment-col comment-odd-row2';
            }
        }

        var html = '<div class="row comment" style="display:flex;">';
        html += '<div class="col-xs-12 col-md-12">';
        html += '<div class="row" style="display:flex;">';
        html += '<div class="col-xs-2 col-md-2 ' + styleClass1 + '">' + '<div class="glyphicons glyphicons-pro user"></div><div class="header-4 sales-rep">' + comment.user + '</div></div>';
        html += '<div class="col-xs-7 col-md-7 message ' + styleClass2 + '">' + comment.message + '</div>';
        html += '<div class="col-xs-3 col-md-3 ' + styleClass2 + '" style="text-align: center;"><div class="header-4 date" style="color:#999999;">' + comment.date + '</div></div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';


        this.commentTextfield.value = '';

        this.commentDiv.innerHTML += html;

        jQuery('#messages-container').scrollTop(jQuery('#messages-container')[0].scrollHeight);

        this.commentBadge.innerHTML = jQuery('.message').length;
    },

    postFailure: function(request) {
        alert('Fail to post comments!');
    },
};



/*Delivery popup*/
/*Delivery Panel*/
var DeliveryPanel = Class.create(PopUpBase, {
    initialize: function() {
        this.createPopUp($('delivery.popup.panel'));

        /*buttons*/
        this.nowBtn = $('delivery.popup.now.button');
        this.nowBtn.onclick = this.setDeliveryNow.bind(this);

        this.laterBtn = $('delivery.popup.later.button');
        this.laterBtn.onclick = this.setDeliveryLater.bind(this);

        this.closeBtn = $('delivery.popup.close.button');
        this.closeBtn.onclick = this.hide.bind(this);

        this.scheduleBtn = $('delivery.popup.schedule.button');
        this.scheduleBtn.onclick = this.schedule.bind(this);

        /*panels*/
        this.confimationPanel = $('delivery.popup.confirmation.panel');
        this.datePanel = $('delivery.popup.date.panel');

        this.dateTextField = $('delivery.popup.date.textfield');
        this.dateButton = $('delivery.popup.date.button');

        this.calendar = Calendar.setup({
            "inputField": this.dateTextField,
            "ifFormat": "%Y-%m-%d",
            "daFormat": "%Y-%m-%d",
            "button": this.dateButton
        });
    },

    resetHandler: function() {
        this.confimationPanel.style.display = 'block';
        this.datePanel.style.display = 'none';
        this.dateTextField.value = "";
    },

    onShow: function() {},

    setDeliveryNow: function() {
        var movementDate = DateUtil.getCurrentDate();
        var url = 'OrderAction.do?action=generateShipment&orderId=' + $c_order_id + '&movementDate=' + movementDate;
        requestURL(url);
        this.hide();
    },

    setDeliveryLater: function() {
        this.confimationPanel.style.display = 'none';
        this.datePanel.style.display = 'block';
    },

    schedule: function() {
        var movementDate = this.dateTextField.value + ' 00:00:00';

        if (movementDate == ' 00:00:00') {
            alert("Schedule date cannot be blank");
            return false;
        }

        var url = 'OrderAction.do?action=setShipmentDate&orderId=' + $c_order_id + '&movementDate=' + movementDate;
        requestURL(url);
        this.hide();
    }

});

function getOrderJSON() {
    return JSON.stringify(receiptJSON);
}

function generateSerialNos() {
    var createInstanceProductsPanel = new CreateInstanceProductsPanel();
    createInstanceProductsPanel.show();
}

function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true)
    }

    return (false)
}

function emailReceipt() {
    //validate email
    var email = receiptJSON.header.bpEmail;

    if (email == null || email == "") {

        alert('Customer has no email address!');
        return;
    }

    if (!validateEmail(email)) {

        alert('Email address is not valid!');
        return;
    }

    BPManager.emailReceipt($c_order_id);
}

jQuery(document).ready(function() {
    determineLength(jQuery("#secondary-action-container"), "col-md-3 col-xs-3");
    determineLength(jQuery("#danger-action-container"), "col-md-3 col-xs-3");
    determineLength(jQuery("#danger-action-container-2"), "col-md-6 col-xs-6");
    determineLength(jQuery("#action-container"), "col-md-6 col-xs-6");
});

function determineLength(container, oldClass) {
    var size = container.children("div").length;

    if (size == 4) {
        replaceClass(container, oldClass, "col-md-3 col-xs-3");
    } else if (size == 3) {
        replaceClass(container, oldClass, "col-md-4 col-xs-4");
    } else if (size == 2) {
        replaceClass(container, oldClass, "col-md-6 col-xs-6");
    } else if (size == 1) {
        replaceClass(container, oldClass, "col-md-12 col-xs-12");
    }
};

function replaceClass(container, oldClass, newClass) {
    var array = container.children("div");

    for (var i = 0; i < array.length; i++) {
        array.eq(i).removeClass(oldClass).addClass(newClass);
    };
};

function invokeOrder(orderId) {
    var url = 'OrderAction.do?action=invoke&orderId=' + orderId;
    new Ajax.Request(url, {
        onSuccess: function success(request) {
            var responseText = request.responseText;
            var result = eval('(' + responseText + ')');

            if (result.error) {
                alert(result.error);
                return;
            }

            requestURL('OrderAction.do?action=loadOrderScreen&orderType=' + result.orderType);

            /* if (result.editable)
                requestURL('OrderAction.do?action=loadOrderScreen&orderType=' + result.orderType);
            else
                requestURL('OrderAction.do?action=view&orderId=' + result.orderId); */

        },
        onFailure: function failure(request) {
            alert('Failed to process request')
        }
    });
}


var checkGiftCardBalanceDlg = null;
var giftCardBalanceDlg = null;

function checkGiftCardBalance() {
    if (checkGiftCardBalanceDlg == null) {
        /*initialize*/

        var giftDlg = jQuery("<div/>", {
            title: Translation.translate("check.card.balance", "Check Card Balance"),
            class: "row gift-card-popup"
        });
        var cardNo = jQuery("<input>", {
            type: "text",
            id: "card-no",
            class: "numeric textField pull-right"
        });
        var cardCvv = jQuery("<input>", {
            type: "text",
            class: "numeric textField pull-right"
        });
        var checkBtn = jQuery("<input>", {
            type: "button",
            value: Translation.translate("check.balance", "Check Balance"),
            class: "pull-right"
        });

        checkBtn.on("click", function() {
            var number = cardNo.val();
            var cvv = cardCvv.val();

            if (number == null || number == "") {
                alert(Translation.translate("invalid.card.number", "Invalid Card Number!"));
            } else {

                jQuery.post("GiftCardAction.do", {
                        action: "checkGiftCardBalance",
                        cardNo: number
                    },
                    function(json, textStatus, jqXHR) {

                        if (json == null || jqXHR.status != 200) {
                            alert(Translation.translate("failed.to.check.balance", "Failed to check balance!"));
                            return;
                        }

                        if (json.error == "Invalid card number!") {
                            alert(json.error);
                            cardNo.val('');
                            cardCvv.val('');
                            return;
                        }
                        jQuery(checkGiftCardBalanceDlg).dialog("close");
                        displayGiftCard(json);
                    },
                    "json").fail(function() {
                    alert(Translation.translate("failed.to.send.check.balance", "Failed to send check balance request!"));
                });

                jQuery(document).ready(function() {
                    cardNo.val('');
                    cardCvv.val('');
                });

            }
        });

        var row = jQuery("<div/>", {
            class: "row"
        });
        var col1 = jQuery("<div/>", {
            class: "col-md-4 col-xs-4"
        });
        var col2 = jQuery("<div/>", {
            class: "col-md-8 col-xs-8"
        });
        col1.append(jQuery("<label>" + Translation.translate("card.no", "Card No") + ":</label>"));
        col2.append(cardNo)
        row.append(col1);
        row.append(col2);
        giftDlg.append(row);

        var row = jQuery("<div/>", {
            class: "row"
        });
        var col1 = jQuery("<div/>", {
            class: "col-md-4 col-xs-4"
        });
        var col2 = jQuery("<div/>", {
            class: "col-md-8 col-xs-8"
        });
        col2.append(checkBtn)
        row.append(col1);
        row.append(col2);
        giftDlg.append(row);

        checkGiftCardBalanceDlg = giftDlg;
    }

    jQuery(checkGiftCardBalanceDlg).dialog({
        position: ['center', 40],
        modal: true,
        open: function() {
            var inputs = jQuery(checkGiftCardBalanceDlg).find("input[type|='text']");
            for (var i = 0; i < inputs.length; i++) {
                jQuery(inputs[i]).val("");
                Event.observe(inputs[i], 'click', Keypad.clickHandler.bindAsEventListener(Keypad), false);
            };
        },
        close: function() {
            Keypad.hide();
        }
    });

}

function displayGiftCard(json) {
    if (giftCardBalanceDlg == null) {
        var dlg = jQuery("<div/>", {
            title: Translation.translate("gift.card.details", "Gift Card Details"),
            class: "gift-card-popup gift-card-details"
        });

        var row = jQuery("<div/>", {
            class: "row"
        });
        row.append(jQuery("<label>" + Translation.translate("balance", "Balance") + ":</label>"));
        row.append(jQuery("<span/>", {
            class: "card-balance"
        }));
        dlg.append(row);

        row = jQuery("<div/>", {
            class: "row"
        });
        row.append(jQuery("<label>" + Translation.translate("card.no", "Card No") + ":</label>"));
        row.append(jQuery("<span/>", {
            class: "card-no"
        }));
        dlg.append(row);

        row = jQuery("<div/>", {
            class: "row"
        });
        row.append(jQuery("<label>" + Translation.translate("expiry", "Expiry") + ":</label>"));
        row.append(jQuery("<span/>", {
            class: "card-expiry"
        }));
        dlg.append(row);

        row = jQuery("<div/>", {
            class: "row"
        });
        row.append(jQuery("<label>" + Translation.translate("date.issued", "Date Issued") + ":</label>"));
        row.append(jQuery("<span/>", {
            class: "card-date-issued"
        }));
        dlg.append(row);
        giftCardBalanceDlg = dlg;
    }

    var cardNoSpan = giftCardBalanceDlg.find(".card-no")[0];
    jQuery(cardNoSpan).html(json.accountnumber);

    var cardBalance = giftCardBalanceDlg.find(".card-balance")[0];
    var balance = json.balance;
    balance = new Number(balance).toFixed(2);
    balance = receiptJSON.header.currencySymbol + " " + balance;

    jQuery(cardBalance).html(balance);

    var cardExpiry = giftCardBalanceDlg.find(".card-expiry")[0];
    jQuery(cardExpiry).html(json.expiry);

    var cardDateIssued = giftCardBalanceDlg.find(".card-date-issued")[0];
    jQuery(cardDateIssued).html(json.dateissued);

    jQuery(giftCardBalanceDlg).dialog({
        modal: true,
        position: ['center', 40]
    });
}

var changeReferenceDlg = null;

function changeReference(url) {

    if (changeReferenceDlg == null) {
        /*initialize*/

        var referenceDlg = jQuery("<div/>", {
            title: Translation.translate("edit.invoice", "Edit Invoice"),
            class: "row"
        });
        var referenceNameField = jQuery("<input>", {
            type: "text",
            id: "reference-name",
            class: "textField pull-right"
        });
        var referenceNoField = jQuery("<input>", {
            type: "text",
            id: "reference-no",
            class: "textField pull-right"
        });
        var applyBtn = jQuery("<input>", {
            type: "button",
            value: Translation.translate("apply", "Apply"),
            class: "pull-right"
        });

        applyBtn.on("click", function() {

            var referenceName = referenceNameField.val();
            var referenceNo = referenceNoField.val();

            if (referenceName == undefined || referenceName == null || referenceName == '') {

                alert(Translation.translate("invalid.reference.name", "Invalid Reference Name!"));
            } else {
                window.location = url + "&referenceName=" + referenceName + "&referenceNo=" + referenceNo;

                jQuery(changeReferenceDlg).dialog("close");

                jQuery(document).ready(function() {
                    referenceNameField.val('');
                    referenceNoField.val('');
                });
            }
        });

        var row = jQuery("<div/>", {
            class: "row"
        });
        var col1 = jQuery("<div/>", {
            class: "col-md-4 col-xs-4"
        });
        var col2 = jQuery("<div/>", {
            class: "col-md-8 col-xs-8"
        });
        col1.append(jQuery("<label>" + Translation.translate("reference.name", "Reference Name") + ":</label>"));
        col2.append(referenceNameField)
        row.append(col1);
        row.append(col2);
        referenceDlg.append(row);

        var row = jQuery("<div/>", {
            class: "row"
        });
        var col1 = jQuery("<div/>", {
            class: "col-md-4 col-xs-4"
        });
        col1.append(jQuery("</br>"));
        row.append(col1);
        referenceDlg.append(row);

        var row = jQuery("<div/>", {
            class: "row"
        });
        var col1 = jQuery("<div/>", {
            class: "col-md-4 col-xs-4"
        });
        var col2 = jQuery("<div/>", {
            class: "col-md-8 col-xs-8"
        });
        col1.append(jQuery("<label>" + Translation.translate("reference.no", "Reference No") + ":</label>"));
        col2.append(referenceNoField)
        row.append(col1);
        row.append(col2);
        referenceDlg.append(row);

        var row = jQuery("<div/>", {
            class: "row"
        });
        var col1 = jQuery("<div/>", {
            class: "col-md-4 col-xs-4"
        });
        col1.append(jQuery("</br>"));
        row.append(col1);
        referenceDlg.append(row);

        var row = jQuery("<div/>", {
            class: "row"
        });
        var col1 = jQuery("<div/>", {
            class: "col-md-4 col-xs-4"
        });
        var col2 = jQuery("<div/>", {
            class: "col-md-8 col-xs-8"
        });
        col2.append(applyBtn)
        row.append(col1);
        row.append(col2);
        referenceDlg.append(row);

        changeReferenceDlg = referenceDlg;
    }

    jQuery(changeReferenceDlg).dialog({
        position: ['center', 40],
        width: 450,
        modal: true,
        open: function() {
            var inputs = jQuery(changeReferenceDlg).find("input[type|='text']");

            jQuery('#reference-name').val(receiptJSON.header.referenceName);
            jQuery('#reference-no').val(receiptJSON.header.referenceNo);
        },
        close: function() {}
    });

}

function convertQuotation(order_id) {
    PopUpManager.clear();
    BootstrapDialog.confirm("Do you want to create an order for this " + receiptJSON.header.title + "?", function(create) {
        if (create) {
            showProcessingDialog();
            requestURL('QuotationAction.do?action=convertToOrder&orderId=' + order_id);
        }
    });
}

function closeQuotation(order_id) {
    PopUpManager.clear();
    BootstrapDialog.confirm("Do you want to close this " + receiptJSON.header.title + "?", function(close) {
        if (close) {
            showProcessingDialog();
            requestURL('QuotationAction.do?action=closeQuotation&orderId=' + order_id);
        }
    });
}

/* override BootstrapDialog.confirm for testing. Add ids to cancel and ok buttons */
jQuery(function(){
	
	BootstrapDialog.confirm = function(message, callback){
		
		BootstrapDialog.show({
	    	'title': 'Confirm',
	        'message': message,
	        'buttons': [
	        	{
	        		id: 'confirm-cancel-btn',
	                label: 'Cancel',
	                cssClass: 'btn btn-default',
	                action: function(dialogRef){
	                	dialogRef.close();
	                	callback(false);
	                }
	        	},
	        	
	        	{
	        		id: 'confirm-ok-btn',
	                label: 'Ok',
	                cssClass: 'btn btn-primary',
	                action: function(dialogRef){
	                	dialogRef.close();
	                	callback(true);
	                }
	        	}
	        ]
	    });
		
	};
	
});

