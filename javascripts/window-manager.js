var WindowManager = Class.create({
	
	initialize : function(searchManager, tabManager)
	{
		this.searchManager = searchManager;
		this.tabManager = tabManager;
		
		this.popUpGrid = null;
		
		this.activateBtn = $('activate');
		this.deactivateBtn = $('deactivate');
		this.deleteBtn = $('delete');
		this.tabSaveBtn = $('tab-save');
		this.tabCancelBtn = $('tab-cancel');
		this.newBtn = $('new');
		this.helpBtn = $('general-help');
		this.title = $('item-title');
		this.uploadImage = $('uploadImage');
		
		this.viewStatementOfAccountBtn = $('view-statement-of-account');
		this.addVariantButton = $('add-variant');
		
		this.defaultImage = null;
	},
	
	setDefaultImage : function(defaultImage)
	{
		this.defaultImage = defaultImage;
	},
	
	init : function()
	{
		if (this.searchManager != null)
			this.searchManager.setSelectListener(this);
		
		this.tabManager.setControllerActionListener(this);
		
		this.tabSaveBtn.onclick = this.saveAction.bind(this);
		this.newBtn.onclick = this.newAction.bind(this);
		this.activateBtn.onclick = this.activateAction.bind(this);
		this.deactivateBtn.onclick = this.deactivateAction.bind(this);
		this.deleteBtn.onclick = this.deleteAction.bind(this);
		this.helpBtn.onclick = this.showGeneralHelp.bind(this);
		
		this.onWindowLoad();
		
		if (this.viewStatementOfAccountBtn != null)
		{
			if (this.tabManager.getController().getClassName() == 'Customer' && this.tabManager.getController().getId() != 0)
			{
				this.viewStatementOfAccountBtn.style.display = 'block';
				this.viewStatementOfAccountBtn.onclick = this.viewStatementOfAccount.bind(this);
			}
			else
			{
				this.viewStatementOfAccountBtn.style.display = 'none';
			}
		}
		
		if (this.addVariantButton != null)
		{
			var controller = this.tabManager.getController();
			
			if (controller.getClassName() == 'Product')
			{
				this.addVariantButton.onclick = this.addVariant.bind(this);
				
				if (controller.getId() != 0)
				{
					this.addVariantButton.style.display = 'block';
				}
				else
				{
					this.addVariantButton.style.display = 'none';
				}
				
				var productParentId = controller.getRoot().getColumnValue('product.M_Product_Parent_ID');
				
				if ((productParentId == null || productParentId == '' || productParentId <= 0) && controller.getId() != 0)
				{
					controller.parentProductId = controller.getId();
					//this.addVariantButton.style.display = 'block';
				}
				else
				{
					controller.parentProductId = controller.productParentId;
				}
			}
			else
			{
				this.addVariantButton.style.display = 'none';
			}
		}
	},
	
	onWindowLoad : function()
	{
		var controller = this.tabManager.getController();
		
		if (controller.getId() == 0)
		{
			this.newActionComplete();
			this.title.value = 'New ' + controller.getClassName();
		}
		else
		{
			this.loadActionComplete(controller.getItemTitle(), controller.isActive())
		}
	},
	
	showGeneralHelp : function()
	{
		var helpPanel = new HelpPanel();
		helpPanel.show();
	},
	
	saveAction : function()
	{
		this.tabManager.saveAction();
	},
	
	newAction : function()
	{
		this.tabManager.newAction();
	},
	
	activateAction : function()
	{
		this.tabManager.activateAction();
	},
	
	deactivateAction : function()
	{
		this.tabManager.deactivateAction();
	},
	
	deleteAction : function()
	{
		this.tabManager.deleteAction();
	},
	
	searchObjectSelected : function(id)
	{
		this.tabManager.reloadTab(id);
		
		if (this.tabManager.getController().getClassName() == 'Customer')
		{
			this.viewStatementOfAccountBtn.style.display = 'block';
			this.viewStatementOfAccountBtn.onclick = this.viewStatementOfAccount.bind(this);
		}
		else
		{
			this.viewStatementOfAccountBtn.style.display = 'none';
		}
		
		/*var controller = this.tabManager.getController();
		
		if (controller.getClassName() == 'Product')
		{
			var productParentId = controller.getRoot().getColumnValue('product.M_Product_Parent_ID');
			 
			if (productParentId == null || productParentId == '')
			{
				this.addVariantButton.style.display = 'block';
				controller.productParentId = controller.getId();
			}
			else
			{
				this.addVariantButton.style.display = 'none';
			}
		}*/
	},
	
	newActionComplete : function()
	{
		this.hide(this.activateBtn);
		this.hide(this.deactivateBtn);
		this.hide(this.deleteBtn);
		/*this.hide(this.uploadImage);*/
		this.hide(this.addVariantButton);
		
		this.show(this.newBtn);
		//this.show(this.tabCancelBtn);
		this.show(this.tabSaveBtn);
		
		this.title.value = '';
	},
	
	loadActionComplete : function(name, isActive)
	{
		this.title.value = name;
		
		if (isActive)
			this.activateActionComplete();
		else
			this.deactivateActionComplete();
		
		var controller = this.tabManager.getController();
		var root = controller.getRoot();
		root.reloadImage(this.defaultImage);
		
		var controller = this.tabManager.getController();
		
		if (controller.getClassName() == 'Product')
		{
			if (controller.getId() != 0)
			{
				this.addVariantButton.style.display = 'block';
			}
			else
			{
				this.addVariantButton.style.display = 'none';
			}
			
			var productParentId = controller.getRoot().getColumnValue('product.M_Product_Parent_ID');
			 
			if ((productParentId == null || productParentId == '' || productParentId <= 0) && controller.getId() != 0)
			{
				controller.parentProductId = controller.getId();
				//this.addVariantButton.style.display = 'block';
			}
			else
			{
				controller.parentProductId = controller.productParentId;
			}
			
			var activeSearchObject = this.searchManager.getActiveSearchObject();
			controller.initEditVariant(activeSearchObject);
		}
		else
		{
			this.addVariantButton.style.display = 'none';
		}
	},
	
	saveActionComplete : function()
	{
		this.activateActionComplete();
	},
	
	activateActionComplete : function()
	{
		this.show(this.newBtn);
		this.show(this.deactivateBtn);
		this.show(this.deleteBtn);
		//this.show(this.tabCancelBtn);
		this.show(this.tabSaveBtn);
		this.show(this.uploadImage);
		
		this.hide(this.activateBtn);
		
		var controller = this.tabManager.getController();
		
		if (controller.getClassName() == 'Product')
		{
			this.addVariantButton.style.display = 'block';
		}
	},
	
	deactivateActionComplete : function()
	{
		this.show(this.newBtn);
		this.show(this.activateBtn);
		
		//this.hide(this.tabCancelBtn);
		this.hide(this.tabSaveBtn);		
		this.hide(this.deactivateBtn);
		this.hide(this.deleteBtn);
		/*this.hide(this.uploadImage);*/
	},
	
	deleteActionComplete : function()
	{
		this.newAction();
	},
	
	hide : function(btn)
	{
		if (btn != null)
			btn.style.display = 'none';
	},
	
	show : function(btn)
	{
		if (btn != null)
			btn.style.display = 'block';
	},
	
	viewStatementOfAccount : function()
	{
		var datePickerPanel = new DatePickerPanel('BPartnerAction.do?action=getStatementOfAccount&customerId=');
		datePickerPanel.setRecordId(this.tabManager.getController().getId());
		datePickerPanel.show();
	},
	
	
	addVariant : function()
	{
		var controller = this.tabManager.getController();
		var productParentId = controller.getRoot().getColumnValue('product.M_Product_Parent_ID');
		
		if (productParentId == '')
		{
			productParentId = controller.getId();
		}
		//this.tabManager.reloadTab(productParentId);
		
		controller.productParentId = productParentId;
		
		//Set ParentId
		$('product.M_Product_Parent_IDText').innerHTML = controller.productParentId;
		$('product.M_Product_Parent_ID').value = controller.productParentId;
		
		//controller.getRoot().setUnique('product.Name', false);
		controller.controllerId = 0;
		controller.resetDisplay();
		controller.variantDisplayLogic();
		controller.setVariantBehaviour();
	}
});
