var PopUpPanel = Class.create(PopUpBase, {
	
	initialize : function(popUpPanel, trigger)
	{
		this.popUpPanelId = popUpPanel;
		this.triggerBtn = null;
		this.popUp = null;
		this.className = trigger;
		this.controllerClass = null;
		this.processKey = null;
		this.tableId = 0;
		this.modelClass = null;
		this.modelId = 0;
		this.okBtn = null;
		this.cancelBtn = null;
	},
	
	setProcessKey : function(processKey)
	{
		this.processKey = processKey;
	},
	
	setControllerClass : function(controllerClass)
	{
		this.controllerClass = controllerClass;
	},
	
	setTableId : function(tableId)
	{
		this.tableId = tableId;
	},
	
	setModelId : function(modelId)
	{
		this.modelId = modelId;
	},
	
	setModelClass : function(modelClass)
	{
		this.modelClass = modelClass;
	},
	
	init : function()
	{
		this.triggerBtn = $(this.className);
		this.popUp = $(this.popUpPanelId);
		
		if (this.popUp != null)
		{
			this.createPopUp(this.popUp);
			this.initBtns();
			this.initPanelActions();
		}
		
		Ajax.Responders.register({
			  onCreate: function() {
				  $('indicator').style.display = 'block';
			  },
			  onComplete: function() {
				  $('indicator').style.display = 'none';
			  }
			});
		
	},
	
	initPanelActions : function()
	{
		if (this.triggerBtn != null)
		{
			this.triggerBtn.onclick = this.initOnCall.bind(this);
		}
		
		this.okBtn.onclick = this.initOnOk.bind(this);
		this.cancelBtn.onclick = this.initOnCancel.bind(this);
	},
	
	hideBtn : function()
	{
		if (this.triggerBtn != null)
		{
			this.triggerBtn.style.display = 'none';
		}
	},
	
	showBtn : function()
	{
		if (this.triggerBtn != null)
		{
			this.triggerBtn.style.display = 'block';
		}
	},
	
	disable : function()
	{
		if (this.triggerBtn != null)
		{
			this.triggerBtn.onclick = function(e){};
		}
		
		this.hideBtn();
	},
	
	enable : function()
	{
		if (this.triggerBtn != null)
		{
			this.triggerBtn.onclick = this.initOnCall.bind(this);
		}
		
		this.showBtn();
	},
	
	initOnCall : function()
	{
		if (this.modelId != 0)
		{
			var url = 'ModelAction.do';
			var pars = 'action=popup&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId;
			
			var ajax = new Ajax.Request(url,
			{ 
				method: 'POST', 
				parameters: pars, 
				onComplete: this.callHandler.bind(this)
			});
		}
	},
	
	initOnOk : function()
	{
		this.hide();
		var url = 'ModelAction.do';
		var pars = 'action=popupOk&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.okHandler.bind(this)
		});	
	},
	
	initOnConfirmOk : function()
	{
		this.hide();
		var url = 'ModelAction.do';
		var pars = 'action=popupConfirmOk&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.confirmOkHandler.bind(this)
		});	
	},
	
	initOnConfirmCancel : function()
	{
		this.hide();
		var url = 'ModelAction.do';
		var pars = 'action=popupConfirmCancel&panel=' + this.className + '&model='+ this.modelClass + '&id=' + this.modelId + '&parameters=' + escape(this.getParameters());
		
		var ajax = new Ajax.Request(url,
		{ 
			method: 'POST', 
			parameters: pars, 
			onComplete: this.confirmCancelHandler.bind(this)
		});	
	},
	
	initOnCancel : function()
	{
		this.afterCancel();
	},
	
	afterCancel : function()
	{
		this.hide();
	},
	
	callHandler : function(request)
	{
		var json = eval('('+request.responseText+')');
		this.afterCall(json);
		this.show();
		
	},
	
	okHandler : function(request)
	{
		this.hide();
		var response = request.responseText;
		var json = eval('('+request.responseText+')');
		this.afterOk(json);
	},
	
	confirmOkHandler : function(request)
	{
		this.hide();
		var response = request.responseText;
		var json = eval('('+request.responseText+')');
		this.afterConfirmOk(json);
	},
	
	confirmCancelHandler : function(request)
	{
		this.hide();
		var response = request.responseText;
		var json = eval('('+request.responseText+')');
		this.afterConfirmCancel(json);
	},
	
	initBtns : function()
	{},
	
	afterCall : function(json)
	{},
	
	afterOk : function(json)
	{},
	
	afterConfirmOk : function(json)
	{},
	
	afterConfirmCancel : function(json)
	{},
	
	getParameters : function()
	{
		var hash = new Hash();
		return hash.toJSON();
	},
	
	show: function()
	{
		PopUpBase.show.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'block';
			}
		}
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'none';
			}
		}
	}
});

var OrgPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger, message)
	{
		$super('organisation', trigger);
		this.selectComp = null;
		this.message = message;
	},
	
	initBtns : function()
	{
		this.okBtn = $('organisation.okBtn');
		this.cancelBtn = $('organisation.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.selectComp = document.createElement('select');
		var orgs = json.orgs;
		if (orgs != null)
		{
			for (var i=0; i<orgs.length; i++)
			{
				var org = orgs[i];
				var name = org.name;
				var id = org.id;
				var selected = org.selected;
				
				var option = document.createElement('option');
				option.setAttribute('value', id);
				option.innerHTML = name;
				if (selected)
				{
					option.setAttribute('selected', true);
				}
				
				this.selectComp.appendChild(option);
			}
		}
		$('organisation.component').innerHTML = '';
		$('organisation.component').appendChild(this.selectComp);
		
		var orgModels = $$('span.organisation-model');
		for (var i=0; i<orgModels.length; i++)
		{
			var orgModel = orgModels[i];
			orgModel.innerHTML = this.message;
		}
	},
	
	getParameters : function()
	{
		var orgId = this.selectComp.value;
		var parameters = "{'orgId':"+ orgId +"}";
		return parameters;
	},
	
	afterOk : function(json)
	{}
});

var ProductOrgPanel = Class.create(OrgPanel, {
	
	initialize : function($super, trigger, message)
	{
		$super(trigger, message);
	},
	
	afterOk : function(json)
	{
		$('product.summary.orgName').innerHTML = json.orgName;
	}
});

var  PartnerOrgPanel = Class.create(OrgPanel, {
	
	initialize : function($super, trigger, message)
	{
		$super(trigger, message);
	},
	
	afterOk : function(json)
	{
		$('partner.summary.orgName').innerHTML = json.orgName;
	}
});

var  UserOrgPanel = Class.create(OrgPanel, {
	
	initialize : function($super, trigger, message)
	{
		$super(trigger, message);
	},
	
	afterOk : function(json)
	{
		$('user.summary.orgName').innerHTML = json.orgName;
	}
});

var ImagePanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('imagePanel', trigger);
	},
	
	initBtns : function()
	{
		this.okBtn = $('image.okBtn');
		this.cancelBtn = $('image.cancelBtn');
		this.takePictureBtn = $('image.takePictureBtn');
		
		this.takePictureBtn.onclick = this.takePicture.bind(this);
	},
	
	takePicture:function()
	{
		  this.hide();
		  PopUpManager.removeActivePopUp(this);
		  new PhotoBoothPanel().show();
	},
	
	initOnCall : function()
	{
		if (this.modelId != 0)
		{
			$('image.panel.frame').src = "AttachmentServlet?tableId="+ this.tableId + "&recordId=" + this.modelId;
			this.show();
		}
	},
	
	initOnOk : function()
	{
		this.uploadImage();
	},
	
	uploadImage : function()
	{
		var imageForm = document.forms.ImageForm;
		imageForm.tableId.value = this.tableId;
		imageForm.recordId.value = this.modelId;
		imageForm.controller.value = this.controllerClass;
		imageForm.processKey.value = this.processKey;
		imageForm.submit();
	}
	
});

var ImageTabPanel = new Class.create(ImagePanel, {
		
	initialize : function($super, trigger, actionUrl)
	{
		$super(trigger);
		this.actionUrl = actionUrl;
	},
	
	uploadImage : function()
	{
		var imageForm = document.forms.ImageForm;
		imageForm.action.value = 'uploadImageTab';
		imageForm.tableId.value = this.tableId;
		imageForm.recordId.value = this.modelId;
		imageForm.actionUrl.value = this.actionUrl;
		imageForm.submit();
	}
});

var ProductPrice = Class.create({
	
	initialize : function(json)
	{
		if (json!= null)
		{
			this.isTaxIncluded = json.isTaxIncluded;
			this.name = json.name;
			this.priceListId = json.priceListId;
			this.productId = json.productId;
			this.priceListVersionId = json.priceListVersionId;
			this.priceStd = json.priceStd;
			this.priceList = json.priceList;
			this.priceLimit = json.priceLimit;
			this.currentPrice = json.currentPrice;
			
			this.initComponent();
		}
	},
	
	initComponent : function()
	{
		this.std_box = this.createBox(this.priceStd);
		this.list_box = this.createBox(this.priceList);
		this.limit_box = this.createBox(this.priceLimit);
		
		this.std_box.onchange = this.updatePriceStd.bind(this);
		this.list_box.onchange = this.updatePriceList.bind(this);
		this.limit_box.onchange = this.updatePriceLimit.bind(this);
	},
	
	getCurrentPrice : function()
	{
		return this.currentPrice;
	},
	
	getName : function()
	{
		return this.name;
	},
	
	getTaxIncluded : function()
	{
		return this.isTaxIncluded;
	},
	
	getPriceStdComp : function()
	{
		return this.std_box;
	},
	
	getPriceListComp : function()
	{
		return this.list_box;
	},
	
	getPriceLimitComp : function()
	{
		return this.limit_box;
	},
	
	updatePriceStd : function()
	{
		this.priceStd = this.std_box.value;
	},
	
	updatePriceList : function()
	{
		this.priceList = this.list_box.value;
	},
	
	updatePriceLimit : function()
	{
		this.priceLimit = this.limit_box.value;
	},
	
	createBox : function(value)
	{
		var box = document.createElement('input');
		box.setAttribute('type', 'text');
		box.setAttribute('value', value);
		box.setAttribute('class', 'pricelistinput');
		return box;
	}
});

var ProductSettings = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('product.settings', trigger);
		this._editPriceOnFly = $('EditPriceOnFly');
		this._editDescOnFly = $('EditDescriptionOnFly');
	},
	
	initBtns : function()
	{
		this.okBtn = $('product.settings.okBtn');
		this.cancelBtn = $('product.settings.cancelBtn');
	},
	
	
	afterCall : function(json)
	{
		this._editDescOnFly.checked = json.editDescOnFly;
		this._editPriceOnFly.checked = json.editPriceOnFly;
		this.show();
	},
	
	afterOk : function(json)
	{
		var editDescOnFly = json.editDescOnFly;
		var editPriceOnFly = json.editPriceOnFly;
		
		$('product.summary.editPrice').innerHTML = (editPriceOnFly) ? "Yes" : "No";
		$('product.summary.editDesc').innerHTML = (editDescOnFly) ? "Yes" : "No";
	},
	
	getParameters : function()
	{
		var editDescOnFly = this._editDescOnFly.checked;
		var editPriceOnFly = this._editPriceOnFly.checked;
		
		var h = new Hash();
		h.set('editDescOnFly', editDescOnFly);
		h.set('editPriceOnFly', editPriceOnFly);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});

var ProductPairing = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger, product)
	{
		$super('product.pairing.panel', trigger);
		this.product = product;
		this.productSearch = null;
		this.productBOMGrid = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('product.pairing.okBtn');
		this.cancelBtn = $('product.pairing.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.productBOMGrid = new ProductBOMGrid(this.product, json.boms)
		this.productSearch = new ProductSearch(this.productBOMGrid);
		
		this.productBOMGrid.buildGrid();
		this.productSearch.initializeComponents();
		
		this.show();
	},
	
	afterOk : function(productPairing)
	{
		if (productPairing != null)
		{
			var boms = productPairing.boms;
			
			var t = '<table>';
			for (var i=0; i<boms.length; i++)
			{
				var bom = boms[i];
				var name = bom.name;
				var qty = bom.qty;
				
				t += '<tr><td>' + qty + 'x </td><td>' + name + '</td></tr>';
			}
			
			t+= '</table>';
			
			$('product.pairing').innerHTML = t;
		}
	},
	
	onShow : function()
	{
		jQuery('#bom-grid').jScrollPane({scrollbarWidth:46, scrollbarMargin:0});
	},
	
	getParameters : function()
	{
		var parameters = new Hash();
		
		var params = new ArrayList();
		
		var productBOMs =this.productBOMGrid.getProductBOMs();
		for (var i=0; i<productBOMs.size(); i++)
		{
			var productBOM = productBOMs.get(i);
			
			var h = new Hash();
			h.set('bomId', productBOM.getBOMId());
			h.set('productId', productBOM.getProductId());
			h.set('qty', productBOM.getQty());
			h.set('line', productBOM.getLine());
			
			params.add(h);
		}
		
		parameters.set('boms', params)
		return parameters.toJSON();
	}
});

var ProductPricesPanel = Class.create(PopUpPanel, {
	initialize : function($super, trigger)
	{
		$super('product.prices', trigger);
		this.soProductPrices = new ArrayList(new ProductPrice());
		this.poProductPrices = new ArrayList(new ProductPrice());
	},
	
	clear : function()
	{
		this.soProductPrices.clear();
		this.poProductPrices.clear();
	},
	
	afterCall : function(json)
	{
		this.clear();
		var soPrices = json.soPrices;
		var poPrices = json.poPrices;
		
		this.addProductPrices(soPrices, this.soProductPrices);
		this.addProductPrices(poPrices, this.poProductPrices);
		this.initPopup();
		this.show();
	},
	
	getParameters : function()
	{
		var parameters = '{ "soPrices":' + this.soProductPrices.toJSON();
		parameters += ', "poPrices":' + this.poProductPrices.toJSON();
		parameters += '}';
		
		return parameters;
	},
	
	initPopup : function()
	{
		this.flush();
		
		for (var i=0; i<this.soProductPrices.size(); i++)
		{
			var productPrice = this.soProductPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			var tdOriginal = document.createElement('td');
			var tdLimit = document.createElement('td');
			
			tdCurrent.setAttribute('class', 'pricelistbg');
			tdOriginal.setAttribute('class', 'pricelistbg');
			tdLimit.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			tdOriginal.appendChild(productPrice.getPriceListComp());
			tdLimit.appendChild(productPrice.getPriceLimitComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			tr.appendChild(tdOriginal);
			tr.appendChild(tdLimit);
			
			Element.insert($('product.prices.so.header'), {after:tr});
		}
		
		for (var i=0; i<this.poProductPrices.size(); i++)
		{
			var productPrice = this.poProductPrices.get(i);
			
			var tr = document.createElement('tr');
			var tdName = document.createElement('td');
			var tdInclTax = document.createElement('td');
			var tdCurrent = document.createElement('td');
			
			tdCurrent.setAttribute('class', 'pricelistbg');
			
			tdName.innerHTML = productPrice.getName();
			tdInclTax.innerHTML = productPrice.getTaxIncluded() ? 'Yes' : 'No';
			tdCurrent.appendChild(productPrice.getPriceStdComp());
			
			tr.appendChild(tdName);
			tr.appendChild(tdInclTax);
			tr.appendChild(tdCurrent);
			
			Element.insert($('product.prices.po.header'), {after:tr});
		}
	},
	
	flush : function()
	{
		var soTable = $('product.prices.so.table');
		var soHeader = $('product.prices.so.header');
		
		soTable.innerHTML = '';
		soTable.appendChild(soHeader);
		
		var poTable = $('product.prices.po.table');
		var poHeader = $('product.prices.po.header');
		
		poTable.innerHTML = '';
		poTable.appendChild(poHeader);
	},
	
	addProductPrices : function(jsonPrices, productPrices)
	{
		for (var i=0; i<jsonPrices.length; i++)
		{
			var json = jsonPrices[i];
			var productPrice = new ProductPrice(json);
			productPrices.add(productPrice);
		}
	},
	
	afterOk : function(json)
	{
		/*var soPrice = json.currentPriceSO;
		var poPrice = json.currentPricePO;
		
		$('priceSO.PriceStd').value = soPrice.priceStd;
		$('priceSO.PriceList').value = soPrice.priceList;
		$('priceSO.PriceLimit').value = soPrice.priceLimit;
		
		$('product.summary.newPrice').innerHTML = soPrice.priceStd;
		$('product.summary.oldPrice').innerHTML = soPrice.priceList;
		
		$('pricePO.PriceStd').value = poPrice.priceStd;*/
	},
	
	initBtns : function()
	{
		this.okBtn = $('product.prices.okBtn');
		this.cancelBtn = $('product.prices.cancelBtn');
	}
});

var TaxCategoryPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('tax.category', trigger);
		this.selectComp = null;
		this.hash = new Hash();
	},
	
	initBtns : function()
	{
		this.okBtn = $('tax.category.okBtn');
		this.cancelBtn = $('tax.category.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.selectComp = document.createElement('select');
		this.selectComp.setAttribute('id', 'tax.category.select');
		
		var taxCategories = json.taxCategories;
		for (var i=0; i<taxCategories.length; i++)
		{
			var taxCategory = taxCategories[i];
			var name = taxCategory.name;
			var id = taxCategory.id;
			var selected = taxCategory.selected;
			var details = taxCategory.details;
			
			this.hash.set(id, details);
			
			var option = document.createElement('option');
			option.setAttribute('value', id);
			option.innerHTML = name;
			option.onclick = this.displayDetails.bind(this, option.value);
			if (selected)
			{
				option.setAttribute('selected', true);
				this.displayDetails(option.value);
			}
			
			this.selectComp.appendChild(option);
		}
		$('taxCategoryId').innerHTML = '';
		$('taxCategoryId').appendChild(this.selectComp);
	},
	
	displayDetails : function(id)
	{
		this.flush();
		var details = this.hash.get(id);
		for (var i=0; i<details.length; i++)
		{
			var detail = details[i];
			var orgName = detail.orgName == null? "" : detail.orgName;
			var taxRate = detail.taxRate == null ? "" : detail.taxRate;
			var terminalName = detail.terminalName == null ? "" : detail.terminalName;
			
			var tr = document.createElement('tr');
			var tdOrg = document.createElement('td');
			var tdRate = document.createElement('td');
			var tdTerminal = document.createElement('td');
			
			tdOrg.innerHTML = orgName;
			tdRate.innerHTML = taxRate;
			tdTerminal.innerHTML = terminalName;
			
			tr.appendChild(tdOrg);
			tr.appendChild(tdTerminal);
			tr.appendChild(tdRate);
			
			Element.insert($('tax.category.header'), {after:tr});
		}
	},
	
	flush : function()
	{
		var table = $('tax.category.table');
		var header = $('tax.category.header');
		
		table.innerHTML = '';
		table.appendChild(header);
	},
	
	afterOk : function(json)
	{
		var taxCat = json['product.summary.taxCategoryName'];
		$('product.summary.taxCategoryName').innerHTML = taxCat;
	},
	
	getParameters : function()
	{
		var taxCategoryId = this.selectComp.value;
		var parameters = "{'taxCategoryId':" + taxCategoryId + "}";
		return parameters;
	}
});

/* Product Info Panel */
var ProductInfo =  Class.create(ProductInfoPanel, {
	
	initialize: function($super, product, summary) 
	{
		$super();
		this.product = product;
		this.summary = summary;
	}, 
	
	getInfo:function()
	{
		var summaryInfoJSON = this.summary.getSummaryInfo();
		
		var columnNames = new ArrayList();
		columnNames.add('Description');
		columnNames.add('Name');
		columnNames.add('UPC');
		
		var root = this.product.getRoot();
		var map = root.getFieldMap();
		var prefix = root.prefix;
		
		var orgName = summaryInfoJSON['product.summary.orgName'];
		var productType = $('product.ProductType');
		var uom = $('product.C_UOM_ID');
		var productId = root.getId();
		
		var productTypeVal = productType.options[productType.selectedIndex].label;
		var uomValue = uom.options[uom.selectedIndex].label;
		
		var basicInfo = '{"AD_Org_ID": {"value" : "' + orgName + '", "label" : "' + orgName + '"}, ';
		basicInfo += '"ProductType": {"value" : "' + productTypeVal + '", "label" : "' + productTypeVal + '"}, ';
		basicInfo += '"C_UOM_ID": {"value" : "' + uomValue + '", "label" : "' + uomValue + '"}, ';
		basicInfo += '"M_Product_ID": {"value" : "' + productId + '", "label" : "' + productId + '"}, ';
		basicInfo += '"CurrencySymbol":"' + summaryInfoJSON['product.summary.currencySymbol'] + '",';
		for (var i=0; i<columnNames.size(); i++)
		{
			var columnName = columnNames.get(i);
			var field = map.get(prefix + "." + columnName);
			var value = field.value;
			var label = field.label;
			var json = '{"value" : "' + value + '", "label" : "' + label + '"}';
			
			basicInfo += '"' + columnName + '":' + json;
			
			if (i < (columnNames.size() - 1))
					basicInfo += ',';
		}
		
		basicInfo += '}';
		
		var groupCols = new ArrayList();
		groupCols.add('PrimaryGroup');
		for (var i=1; i<=8; i++)
		{
			var columnName = 'Group' + i;
			groupCols.add(columnName);
		}
		
		var groups = '[';
		for (var i=0; i<groupCols.size(); i++)
		{
			var columnName = groupCols.get(i);
			var group = prefix + '.' + columnName;
			
			var field = map.get(group);
			var value = field.value;
			var label = field.label;
			
			groups += '{"' + columnName + '" : {"value": "' + value + '", "label" : "' + label + '"}}';
			if (i < (groupCols.size() - 1))
			{
				groups += ','
			}
		}
		groups += ']';
		
		var stocksList = summaryInfoJSON['product.summary.allStock'];
		
		var stocks = new Hash();
		for (var i=0; i<stocksList.length; i++)
		{
			var stock = stocksList[i];
			stocks.set(stock.name, stock.qty);
	    }
		
		var priceStd = summaryInfoJSON['product.summary.newPrice'];
		var priceList = summaryInfoJSON['product.summary.oldPrice'];
		var isTaxIncluded = summaryInfoJSON['product.summary.isTaxIncluded'];
		
		var taxName = summaryInfoJSON['product.summary.taxName'];
		
		this.productInfo = {'basicInfo': eval('(' + basicInfo + ')'),
					'groups': eval('(' +groups + ')'),
					'imageURL': $('product.image').src,
					'productImageUrl': $('product.image').src,
					'hasImageAttachment' : true,
					'isTaxInclusive' : isTaxIncluded,
					'priceList': priceList,
					'tax' : taxName,
					'priceStd': priceStd,
					'stocks': eval('(' +stocks.toJSON() + ')')};
		
		this.show();
	}	
});

var Warehouse = Class.create({
	
	initialize : function(name, id, qty)
	{
		this.name = name;
		this.id = id;
		this.qty = qty;
		
		this.initComponent();
	},
	
	initComponent : function()
	{
		this.component = document.createElement('input');
		this.component.setAttribute('type', 'text');
		this.component.setAttribute('value', this.qty);
		this.component.onchange = this.updateQty.bind(this);
	},
	
	updateQty : function()
	{
		this.qty = this.component.value;
	},
	
	getComponent : function()
	{
		return this.component;
	},
	
	getWarehouseId : function()
	{
		return this.warehouseId;
	},
	
	getQty : function()
	{
		return this.component.value;
	},
	
	equals : function (warehouse)
	{
		return (warehouse.getWarehouseId() == this.warehouseId);
	}
});

var StockPanel = Class.create(PopUpPanel, {
	initialize : function($super, trigger)
	{
		$super('update.stock', trigger);
		this.warehouseList = new ArrayList(new Warehouse());
	},
	
	initBtns : function()
	{
		this.okBtn = $('update.stock.okBtn');
		this.cancelBtn = $('update.stock.cancelBtn');
	},
	
	afterCall : function(json)
	{
		var warehouses = json.warehouses;
		var stockHeader = $('stock.header');
		var stockTable = $('stock.table');
		stockTable.innerHTML = '';
		
		stockTable.appendChild(stockHeader);
		
		for (var i=0; i<warehouses.length; i++)
		{
			var tr = document.createElement('tr');
			var wh = warehouses[i];
			var name = wh.name;
			var id = wh.id;
			var qty = wh.qty;
			
			var warehouse = new Warehouse(name, id, qty);
			this.warehouseList.add(warehouse);
			
			var style = 'text-align:left;padding-left:50px';
			
			var tdName = document.createElement('td');
			var tdComp = document.createElement('td');
			
			tdName.setAttribute('style', style);
			tdComp.setAttribute('style', style);
			
			tdName.innerHTML = name;
			tdComp.appendChild(warehouse.getComponent());
			
			tr.appendChild(tdName);
			tr.appendChild(tdComp);
			
			Element.insert('stock.header', {after : tr});
		}
	},
	
	afterOk : function(json)
	{
		var error = json.error;
		if (error != null)
		{
			alert(error);
		}
		else
		{
			var stock = json.stock;
			var allStock = json.allStock;
			
			$('product.summary.stock').innerHTML = stock;
			$('product.summary.allStock').innerHTML = allStock;
		}
	},
	
	getParameters : function()
	{
		var d = new Date();
		var movementDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() 
							+ ' ' + d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
		
		var parameters = "{'warehouses' : " + this.warehouseList.toJSON() + ", 'movementDate' : '"+ movementDate+"'}";
		
		
		return parameters;
	}
});


var PriceListPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('price.list.panel', trigger);
		this.selectComp = null;
	},
	
	initBtns : function()
	{
		this.okBtn = $('price.list.panel.okBtn');
		this.cancelBtn = $('price.list.panel.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.selectComp = document.createElement('select');
		this.selectComp.setAttribute('class','price-list-select-dimen');
		var priceLists = json.priceLists;
		for (var i=0; i<priceLists.length; i++)
		{
			var priceList = priceLists[i];
			var id = priceList.id;
			var name = priceList.name;
			
			var selected = priceList.selected;
			
			var option = document.createElement('option');
			option.setAttribute('value', id);
			option.innerHTML = name;
			if (selected)
			{
				option.setAttribute('selected', true);
			}
			
			this.selectComp.appendChild(option);
		}
		
		$('price.list.component').innerHTML = '';
		$('price.list.component').appendChild(this.selectComp);
	},
	
	afterOk : function(json)
	{
		$('partner.summary.priceListName').innerHTML = json.priceListName;
	},
	
	getParameters : function()
	{
		var priceListId = this.selectComp.value;
		var parameters = "{'priceListId':"+ priceListId +"}";
		return parameters;
	}
});

var CreditManagementPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('credit.management.panel', trigger);
		
		this.creditStatusComp = document.createElement('select');
		this.creditStatusComp.setAttribute('class','credit-status-select-dimen');
	},
	
	initBtns : function()
	{
		this.okBtn = $('credit.management.okBtn');
		this.cancelBtn = $('credit.management.cancelBtn');
	},
	
	afterCall : function(json)
	{
		this.creditStatusComp.innerHTML = '';
		var cs = json.creditStatus;
		for (var i=0; i<cs.length; i++)
		{
			var creditS = cs[i];
			var value = creditS.value;
			var display = creditS.display;
			var selected = creditS.selected;
			
			var option = document.createElement('option');
			option.setAttribute('value', value);
			option.innerHTML = display;
			if (selected)
			{
				option.setAttribute('selected', true);
			}
			
			this.creditStatusComp.appendChild(option);
		}
		
		$('credit.status').innerHTML = '';
		$('credit.status').appendChild(this.creditStatusComp);
		
		$('credit.limit').value = json.creditLimit;
		$('credit.watch').value = json.creditWatch;
	},
	
	afterOk : function(json)
	{
		$('partner.summary.creditStatus').innerHTML = json.creditStatus;
		$('partner.summary.creditLimit').innerHTML = json.creditLimit;
		$('partner.summary.creditWatch').innerHTML = json.creditWatch;
	},
	
	getParameters : function()
	{
		var creditStatus = this.creditStatusComp.value;
		var creditLimit = $('credit.limit').value
		var creditWatch = $('credit.watch').value
		
		var h = new Hash();
		h.set('creditStatus', creditStatus);
		h.set('creditLimit', creditLimit);
		h.set('creditWatch', creditWatch);
		
		var parameters = h.toJSON();
		
		return parameters;
	}
});

var CloseTillPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('close.till.panel', trigger);
		
		this.totalCashAmount = $('closeTill.TotalCashAmount');
		this.voucherAmountEntered = $('closeTill.VoucherAmtEntered');
		this.closeTillController = null;
	},
	
	setCloseTillController: function(controller)
	{
		this.closeTillController = controller;
	},
	
	initBtns : function()
	{
		this.okBtn = $('close.till.okBtn');
		this.cancelBtn = $('close.till.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	initOnOk : function($super)
	{
		this.disable();
		$super();
	},
	
	afterOk : function(json)
	{
		var success = json.success;
		
		if (success)
		{
			window.location = 'TillAction.do?action=viewCloseTill&cashJournalId=' + json.cashJournalId + '&print=true';
		}
		else
		{
			this.enable();
			var errorType = json.errorType;
			if (errorType == 'saveError')
			{
				this.closeTillController.showStatusLog(json.errorLog);
			}
			
			var cashAmount = $('#closeTill.TotalCashAmount');
			if(cashAmount==null){
				alert(Translation.translate("cash.amount.empty","Cash Amount Empty"));
			}
			
			else
			{
				alert(json.errorMsg);
			}
		}
	},
	
	getParameters : function()
	{
		var paramMap = this.closeTillController.getParametersMap();
		var d = new Date();
		var clientTime = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
		
		paramMap.set('clientTime', clientTime);
						
		return paramMap.toJSON();
	}
});

var PopUpGrid = Class.create({
	
	initialize : function(controller)
	{
		this.controller = controller;
		this.panels = new ArrayList();
		this.root = this.controller.getRoot();
		this.activateBtn = $('activate');
		this.deactivateBtn = $('deactivate');
	},
	
	reload : function(newController)
	{
		this.controller = newController;
		this.root = this.controller.getRoot();
		this.init();
	},
	
	addPanel : function(panel)
	{
		this.panels.add(panel);
	},
	
	init : function()
	{
		var controllerClass = this.controller.getClassName()
		/*this.controller.addOnSaveListener(this);
		this.controller.addDeactivateListener(this);
		this.controller.addActivateListener(this);
		this.controller.addDeleteListener(this);*/
		
		var controllerId = this.controller.getControllerId();
		
		var processKey = this.controller.getProcessKey();
		
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			
			var className = this.root.getClassName();
			var tableId = this.root.getTableId();
			var id = this.root.getId();
			
			panel.setControllerClass(controllerClass);
			panel.setModelClass(className);
			panel.setModelId(id);
			panel.setTableId(tableId);
			panel.setProcessKey(processKey);
			panel.init();
		}
		
		if (controllerId != 0)
		{
			if (!this.controller.isActive())
				this.deactivatePerformed();
			else
				this.activatePerformed();
		}
		else
		{
			this.disableAllPanels();
			
			if (this.deactivateBtn != null)
				this.deactivateBtn.style.display = 'none';
			
			if (this.activateBtn != null)
				this.activateBtn.style.display = 'none';
			
			if ($('save') != null)
				$('save').style.display = 'block';
		}
	},
	
	controllerSaved : function(controllerId)
	{
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			panel.setModelId(controllerId);
			if (controllerId != 0)
			{
				panel.enable();
			}
		}
		
		if (!this.controller.isActive())
			this.deactivatePerformed();
		else
			this.activatePerformed();
	},
	
	deactivatePerformed : function()
	{
		this.disableAllPanels();
		
		if ($('delete') != null)
			$('delete').style.display = 'none';
		
		this.toggleActivateBtn();
	},
	
	activatePerformed : function()
	{
		this.enableAllPanels();
		
		this.toggleDeactivateBtn();
	},
	
	deletePerformed : function()
	{
		this.disableAllPanels();
		this.deactivateBtn.hide();
		this.activateBtn.hide();
	},
	
	disableAllPanels : function()
	{
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			panel.disable();
		}
		
		if ($('save') != null)
			$('save').style.display = 'none';
		
		if ($('cancel') != null)
			$('cancel').style.display = 'none';
		
		if ($('delete') != null)
			$('delete').style.display = 'none';
	},
	
	enableAllPanels : function()
	{
		for (var i=0; i<this.panels.size(); i++)
		{
			var panel = this.panels.get(i);
			panel.enable();
		}
		
		if ($('save') != null)
			$('save').style.display = 'block';
		
		if ($('cancel') != null)
			$('cancel').style.display = 'block';
		
		if ($('delete') != null)
			$('delete').style.display = 'block';
	},
	
	toggleActivateBtn : function()
	{
		if (this.activateBtn != null)
			this.activateBtn.style.display = 'block';
		if (this.deactivateBtn != null)
			this.deactivateBtn.style.display = 'none';
	},
	
	toggleDeactivateBtn : function()
	{
		if (this.deactivateBtn != null)
			this.deactivateBtn.style.display = 'block';
		if (this.activateBtn != null)
			this.activateBtn.style.display = 'none';
	}
});

var CreateAccountPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('lead.create.account.confirm.panel', trigger);
		
		this.controller = null;
	},
	
	setController: function(controller)
	{
		this.controller = controller;
	},
	
	initBtns : function()
	{
		this.okBtn = $('lead.create.account.okBtn');
		this.cancelBtn = $('lead.create.account.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	afterOk : function(json)
	{
		var success = json.success;
		
		if (success)
		{}
		else
		{
			var errorType = json.errorType;
			if (errorType == 'saveError')
			{
				this.controller.showStatusLog(json.errorLog);
			}
			else
			{
				alert(json.errorMsg);
			}
		}
	},
	
	getParameters : function()
	{
		var h = new Hash();
		h.set('leadId', this.controller.getControllerId());
		
		return h.toJSON();
	}
});

var CompletePaymentPanel = Class.create(PopUpPanel, {
	
	initialize : function($super, trigger)
	{
		$super('complete.payment.panel', trigger);
		
		this.c_bpartner_id = $('payment.C_BPartner_ID');
		this.payamt = $('payment.PayAmt');
		this.tenderType = $('payment.TenderType');
		this.completePaymentController = null;
	},
	
	setCompletePaymentController: function(controller)
	{
		this.completePaymentController = controller;
	},
	
	initBtns : function()
	{
		this.okBtn = $('complete.payment.okBtn');
		this.cancelBtn = $('complete.payment.cancelBtn');
	},
	
	initOnCall : function()
	{
		this.show();
	},
	
	disable : function()
	{},
	
	afterOk : function(json)
	{
		var success = json.success;
		
		if (success)
		{
			window.location = 'PaymentAction.do?action=view&print=true&paymentId=' + json.paymentId;
		}
		else
		{
			var errorType = json.errorType;
			if (errorType == 'completeError')
			{
				this.completePaymentController.showStatusLog(json.errorLog);
			}
			else
			{
				alert(json.errorMsg);
			}
		}
	},
	
	getParameters : function()
	{
		var c_bpartner_id = this.c_bpartner_id.value;
		var payamt = this.payamt.value;
		var tenderType = this.tenderType.value;
		
		var h = new Hash();
		h.set('c_bpartner_id', c_bpartner_id);
		h.set('payamt', payamt);
		h.set('tenderType', tenderType);
		
		var d = new Date();
		var clientTime = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
		
		h.set('clientTime', clientTime);
		
		var parameters = h.toJSON();
						
		return parameters;
	}
});

var HelpPanel = Class.create(PopUpBase, {
	initialize : function()
	{
	 	this.createPopUp($('help.panel'));
	 	this.okBtn = $('help.panel.okBtn');
	 	this.cancelBtn = $('help.panel.cancelBtn');
	 	
	 	this.cancelBtn.onclick = this.hide.bind(this);
	 	this.okBtn.onclick = this.hide.bind(this);
	},
	
	show: function()
	{
		PopUpBase.show.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'block';
			}
		}
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'none';
			}
		}
	}
});


var EditParentProductPanel = Class.create(PopUpBase, {
	initialize : function(searchObject)
	{
		this.searchObject = searchObject;
	 	this.createPopUp($('edit-parent-product'));
	 	this.editParentBtn = $('edit-parent-product-panel-editParentBtn');
	 	this.editChildBtn = $('edit-parent-product-panel-editChildBtn');
	 	this.cancelBtn = $('edit-parent-product-panel-cancelBtn');
	 	
	 	this.cancelBtn.onclick = this.onCancel.bind(this);
	 	this.editParentBtn.onclick = this.editParentProduct.bind(this);
	 	this.editChildBtn.onclick = this.loadChildren.bind(this);
	},
	
	show: function()
	{
		PopUpBase.show.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'block';
			}
		}
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'none';
			}
		}
	},
	
	loadChildren : function()
	{
		this.searchObject.searchManager.params.set('filterQuery', 'p.m_product_parent_id =' + this.searchObject.id);
		this.searchObject.searchManager.search();
		
		this.hide();
	},
	
	editParentProduct : function()
	{
		this.searchObject.searchManager.onSelect(this.searchObject);
		this.hide();
	},
	
	onCancel : function()
	{
		this.hide();
	}
});

var UpdateVariantsPricesPanel = Class.create(PopUpBase, {
	initialize : function()
	{
	 	this.createPopUp($('update-variants-price'));
	 	this.okBtn = $('update-variants-price-panel-okBtn');
	 	this.cancelBtn = $('update-variants-price-panel-cancelBtn');
	 	this.closeBtn = $('update-variants-price-panel-closeBtn');
	 	
	 	this.cancelBtn.onclick = this.onCancel.bind(this);
	 	this.okBtn.onclick = this.onOk.bind(this);
	 	this.closeBtn.onclick = this.onClose.bind(this);
	 	
	 	this.tab = null;
	},
	
	show: function()
	{
		PopUpBase.show.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'block';
			}
		}
	},
	
	hide : function()
	{
		PopUpBase.hide.call(this);
		
		if (window.top != window.self)
		{
			var div = $(parent.document.getElementById('tree-mask'));
			
			if (div != null)
			{
				div.style.display = 'none';
			}
		}
	},
	
	onCancel : function()
	{
		this.hide();
		this.tab.ok();
	},
	
	onClose : function()
	{
		this.hide();
	},
	
	onOk : function()
	{
		this.tab.isUpdateVariantPrice = true;
		this.hide();
		this.tab.ok();
	},
	
	setTab : function(tab)
	{
		this.tab = tab;
	}
});

var CreateModifierPanel =  Class.create(PopUpBase, {
	  initialize: function() {
		
		this.createPopUp($('dialog-create-modifier'));
	    
	    this.saveBtn = $('create-modifier-panel-save-button');
	    this.cancelBtn = $('create-modifier-panel-cancel-button');
	    
	    this.nameTextField = $('create-modifier-name');
	    this.positionTextField = $('create-modifier-panel-position-texfield');
	    this.priceTextField = $('create-modifier-panel-price-texfield');
	    
	    
	    this.cancelBtn.onclick = this.hide.bind(this);
	    this.saveBtn.onclick = this.saveHandler.bind(this); 
	  },
	    
	  saveHandler:function(){
		  this.validate();
		  saveModifier();
		  this.hide();
	  },
	  
	  onShow:function(){
	  },
	  
	  validate:function(){
		  if (this.nameTextField.value.length == 0)
		  {
			  alert(Translation.translate('name.is.mandatory','Name is mandatory'));
		  }
	  }
	});


