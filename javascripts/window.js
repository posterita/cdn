var ModalWindow = Class.create({
	initialize: function(content, showOkCancelButton)
	{
		this.content = content;
		this.isShowOkCancelButton = showOkCancelButton;
		this.title = null;
		this.clickOk = null;
		this.div = null;
		this.init();
	},
	
	init : function()
	{
		this.mask = document.createElement('div');
		this.mask.className = "mask";
		this.mask.id = "mask";
		this.mask.style.visibility = 'hidden';		
		this.div = document.createElement('div');
		if (this.title)
		{
			var pTitle = document.createElement('p');
			pTitle.className = 'title';
			pTitle.innerHTML = this.title;
			this.div.appendChild(pTitle);
		}
		var pContent = document.createElement('p');
		pContent.className = 'msgContent';
		pContent.innerHTML = this.content;
		this.div.appendChild(pContent);
		
		this.div.className = 'windowMsg';
		
		if (this.isShowOkCancelButton)
		{
			var span = document.createElement('span');
			var divOk = document.createElement('div');
			var ok  = document.createElement('input');
			ok.setAttribute('type', 'button');
			ok.setAttribute('id', 'okBtn');
			ok.setAttribute('name', 'okBtn');
			ok.setAttribute('value', 'Ok');
			ok.setAttribute('class', 'popUpPanelBtn');
			divOk.appendChild(ok);
			divOk.style.textAlign = 'right';
			divOk.style.display = 'inline';
			
			var divCancel = document.createElement('div');
			var cancel = document.createElement('input');
			cancel.setAttribute('type', 'button');
			cancel.setAttribute('id', 'cancelBtn');
			cancel.setAttribute('name', 'cancelBtn');
			cancel.setAttribute('value', 'Cancel');
			cancel.setAttribute('class', 'popUpPanelBtn');
			divCancel.appendChild(cancel);
			divCancel.style.textAlign = 'right';
			divCancel.style.display = 'inline';
						
			span.appendChild(divCancel);
			span.appendChild(divOk);
			
			this.div.appendChild(span);
			
			//this.div.appendChild(divCancel);
			//this.div.appendChild(divOk);
			
			
			this.div.okBtn = ok;
			this.div.cancelBtn = cancel;			
			
			this.div.okBtn.onclick = this.dispose.bind(this);
			this.div.cancelBtn.onclick = this.dispose.bind(this);
		}
		
		this.div.mask = this.mask;
		this.mask.appendChild(this.div);
		document.body.appendChild(this.mask);
		
	},
	
	setTitle : function(title)
	{
		this.title = title;
	},
	
	setClickOk : function(clickOk, jsonArg)
	{
		this.clickOk = clickOk;
		this.div.okBtn.onclick = this.doClickOk.bind(this, jsonArg);
	},
	
	doClickOk : function(jsonArg)
	{
		this.clickOk(jsonArg);
		this.dispose();
	},
	
	show: function(content)
	{
		if (content != null)
		{
			this.content = content;
			this.init();
		}
		sH = parseInt(ViewPort.getHeight());
		sW = parseInt(ViewPort.getWidth());
		
		this.div.style.visibility = "hidden";
		this.mask.style.width = sW + "px";
		this.mask.style.height = sH + "px";
		this.mask.style.visibility = 'visible';	
		
		if (this.div.style.height != null && this.div.style.height != '')
		{
			kH = parseInt(this.div.style.height);
		}
		else
		{
			kH = parseInt(this.div.scrollHeight);
		}
		
		kW = parseInt(this.div.scrollWidth);
		
		cH = (sH - kH)/2;
		cW = (sW - kW)/2;
		
		this.div.style.top = cH + "px";
		this.div.style.left = cW + "px";
		this.div.style.visibility = "visible";
		
		if (this.div.okBtn != null)
		{
			this.div.style.height = kH + (this.div.okBtn.offsetHeight)*2;
			this.div.style.width = kW;
		}
				
		if($('okBtn') != null)
		{		
			$('okBtn').focus();	
			$('okBtn').onblur = function(){this.focus();};
		}
	},
	
	dispose: function()
	{
		if (this.mask)
		{
			document.body.removeChild(this.mask);
		}
	}
	
});