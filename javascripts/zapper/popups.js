/* 
==============================================================
	Zapper implementation
==============================================================
 jQuery("#testQRCode").qrcode({
		width: 100,
		height: 100,
		color: '#3a3',
		text: 'http://2.zap.pe?t=6&i=817:463:7[34|50.25|3,40n||13,63n||18,33n|Hello World|10,66|Unique Reference|10:10[39|ZAR,38|Central Perk'
	});
 */

var ZapperLogoBase64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABBCAYAAAD44bmLAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB94HFww1DtedchwAAA2USURBVHja7Z17tF3DHcc/J48miJBWSqxIEG8Rj6a6rqAeLa3We4lXUY1XUUs0QWRlBVUqRdFQVFEhVa+iXa0i8W408YyiQoUiqSQSIZKQ65z+Mb+9su3sfc78Zuace84137X2uvfuO/Ob356Z78xvZn4zAxEREREREREREREREREREc2AksX/DwM2tQirwXk577YBvg90Bz4Gfgd8EIsoIqK+RD8FmBgwrYqktyMwI/W+G/AY0JYT51rgJMf0Vgf2BQYCvYC+QA+gDLwPLAEWAy8Aj9YpfwcCg4E15e/bLOMNALYC1gDeBZ6qYx3oLmXST8piOvCGZdw2YH1pmJ8D3vuC8mgI8FXJy5lSZjYYDCwE5nSU4qOEmGX56fskcvbLSeudGuncqdD728B1wAoHHWcBZwJf8mw4zwZmF6RRhO8BlwMLquj3KXA9sH+A8t1H8jUvnXML4nwZGA1Mq5GPz0r96d6guro7MEbK/R/Aq/I8D9wj1uOxgdPcB7ij4PvPK4jzFcmXpzLhx6fC7C0N7bSC55/CoTbgaeCRgrQ2kXJ4sdpHrB2I3NnnlJy0XrZsTNpqZPwB0jL6NE7peJNTvbAtRhfIqkb0MRZx8nT8DDjd0cL4T430xmXi7Jghd1mRj/cA/etA7m2Bm6ukW/TMBH7kke4Ah/z7ujRARXqOTYUdCvwauAK4DHgbmCu/XyH/a5OGJom/bY6eF1l0LpxVB5JPyElnuqJyX1lF34cDWx9pWbY9wQOWchNsJ0MHX/0Witlogx0s8yldUe8O1HheGojgm0uv5qpTOfXzSGXa29Up/8ZWSXOKNLJ5FkVFOsrLc/4/X/5Xleg3BybNH3PS+JtSxhMFuv4rsK55lWJSjQpwpkImwLcCNkyJjBNr6NhNkWZSUd8MnLf/Brp6kPyaOuTb45Zpd3XIvzcsdalG9Kk1iD5K5kfS2Ez+NzpN9C45QpYFNLGeAA7NvLsJ+I5Szoc5784Gtg68GpA3UfkDGf8V4WKFzD7Ag6mJyRA6VoQE51QJd6kizY+BJ8VMDZm3m8mkUzdlvN4y53GixQSytmyHyRxRrQZIk39Lpd5v2ID5iUky6bxD6t2xkv7iWpGHB2oxZ+XIvsBRXl6rt6BOPXnRs1eODiOUMh6sowVSkVWGohUP2+epOupXBl5RVOT1gOV1zrOyWC+1Voxsn2kB6rZtj95LwqSHtu9LvTytlumOjP18xmXzc1rJkzxMrK7KzC97/j8v/IocHc53nOyr51Bj7YJev9JEuk6wtH4+VepS9tD/rkBE16bvQ/R1gKOAT+T9lvK+e5boXQoSGJqqIBpUJNFNZGY4wYHAb5TyElNpt4ysauET3C7jl+8C64qcEmYt/TDRBYU+JTE5T8287+tgLhbpbPPexozPLkdu4GHaavJcu0LRr0aYlyXPS5Y6vAdcLctVF8rykgYHAbvmvK9n/oXALZgl4aEps32FRsBaQhhtazYwI2eYQyuXhD2oQLchOXFuAXZRfF8P4BmlTllHkhs8e98ZwHGZpbxD5Ft8etbBOZMzPnqeK8tE6eWts4CPPCykav4RdylkPg0MKpDTBbhVYeXNKZhbqKd149ujI0PCCTKcHSHvTnNpiHvLoH+NGk/WQhjsQfKjq+izZyr8LzxbxFlK/XyJnqS1dw29kvGXi/w/BKqof7HIv+M8GqU856RhgcztNMZ4zHO0AtGPSOV/dx+iu2CARwUYWUP2MOAhoGcAPbdS6tbPg+jlAqunGs53rEC+FXWyQsd+GL8AbVmfkSNrrmXvO1NZzo9ayn04INGT9H4mpnXaIj1L8mx8FZ2nk+/dtp/I7ZsZwkxP/T2qEURfB2h3JPl4Go95Cv3W9+zRj3DQ7yaHdA52rKh5QxQb9Fc27GUZOpEZtrgOEW2W91ysNh+iP1tlHizB+rQoVpc1PBeSX9lBOl+l0NeV6GXgLQ8dlynz9HyPitrmqOM5HpYHmA1GNr3ug3Vu0Ed4Er2MceZqGnSpg8zXZXJJO/M4ScYV9cSGMqm0i5j/OwIbSQWo90xpCfi5R/xRSh33dUxnQcG40AYXOsTZPDX5OwS7rdO3O+p3o4U5WwG2D1DWO9GJoZ3YSp57A+txGMYV8QmMp1LItU8f090XmrSSJZZBDR46XaesA0dJvDMUcVZz1G2kpfy5nj361c1GzG4BZT2DWT/X9oxTCbP18gR5vubRCtcTSwLIeAf73WBJ2X6iTONFTx1nAMcrwq8nP7+JvZupq5v220qdXOvFRDoppjhOVkwLkPY4GufRle3Rf6uI90iAb9VOyqUnyWyfvp46DlamNy5F3kZ47ZXRWV/98Zt36DQ9+mRgDwcTdKbHpA8ynnsSs97cqF45C01vGWJy5o0GfNN8z/ivKMMnZdZTGd5n/GzbYLmU2dPNSHTfybhLgMMdSD4Ls7/XFT/EzNCu0UJWz4JAY/Rmx2fK7yk16Xes61g+j3c2op8O/NQhI94CtvBI90hWzp6WWojoawaQ0Qrf29Phe7o14Xd0cyyfRZ3JdB8O/EpJtgrwP8yEnSu2YqUfeMkyzXS4uZi9ze/KmLAHxm97iwY0HIMCyOjXgDrRVdkrZ6HdBNLepJaKa13oNGP03TGnxmhJvgizZu1Tif5umW4S5lrMaTb31Qh/BuGOOyrC9gFkNGJtto3iE31soB2SLWtSc//DLzLRt2XlRgsNyZcCG6Nf6slaEbZLSzOAnbHfrteIwhkQQMYQRdgVjmkM9SS61nLRHhFdwswL9apjWXXHHF/d6OFwUxB9Y8wRulqSt0tPvthT11Mt0k4m+r6hlL1ag/K7DfclxY2U4V917I12Jf/AQVscrKwjyeaUOdj7ff8Xc7JqRODWZ11WHnOrIXlJGoj5AXTdBTv3SBfPrkEN6tUP8Yg7QknY+x3HpQd69oRDlaZ14qBzh6IMDo3UDU/0Xp4kf6fB49sZDvIPbdC4b6RH3LFKws72SMt1z4HW1z295DhdUQanReqGJXpXzCaV1ZVEKGFOaJ0dSM+tFWEHK2VvSWPX4y92iHO9Q5zbHPWrYFZUtFgPs/FGk86tqb8nK9O7LNI33Bh9FuZOKW1v9wLm0sQdHHRaCPw1824tZe9/nyL8L2nsmvyZmAskZ1mGH64028H41S901C85X+8ezA04tnGeVeZjiVXvvLsR4wxlI2OkNIAvRxr7wWZvcD2f9A6gUxXx2hXfuBd+vu4TcfO1XoHdxNO5uPl3X5CZX3EtA5slx01kDsalrmQxQJmP7dIRhcJ44M98/t4Bra/7uFYi+WMdSPDsXVlgrlPWxLtBQfJyg4meTvPHBbqdKORxzbf0xpQ+nmUwlfyNLjthf/Bi3vcXbeW8E90pNRXMUqoPhmOcqBK5/b8IRD+pSUiePGfLXII23t0FY+9dZWjguhMq7Z12CWF2Ub0kJuhS/HZplTFORQQkeqLHPMza8hzC7CQr6ol7oj+SqiLfbUv4HpgbeK7KyCmz6lZWLdHHtArRF3WwyZ53XTBSAC6nyS7G3Ps1L1AFTWNCkzWKFVZdb+/TZPqVMW7M1bC/Z8M5HeMVOR5zC+nNMh8wh9pbVg/2JPrxrUL0ShM+ACfXseJ1FqJfm1OefZqM5BXslnVv6ADd8rzhtERfOxLdvxddRvjrkedKa28TdqIH0et9f9i7BeXZpwGNn+bZTVEXpzSQ5O3ku9X2V8h4vllJndeyrmjiRmiPgMtgyVLQNhg/AZuwoz3SKwHH1LFh3iaQvIukDtTDU3AcupN29sTuEgnf/FsqhF7iWZeOoYUwvsl684ty1k99e50k7qYic4ZF2LydY1rT3Wemvki35TWW6rQ9+lj8Lt8oyj/fE3BDWxqJrOeo7k/S31LOCbQgXm+CCblq56Af67GsUxETK+2EM6NK2Per9JYuRIeVVwSVPSvpSxbWiJboyT3r/QJMzCZxDwhQJzfHeFqGJPxPLNLtbzG82ZsWxt0dTPRa3m0bYI7t0VS4hbJmmsULOXGmsPIoYgITHcxptR84LiVVZHLSBq5ET3C7A7mSsPej82q0wZGs9DFw0akiy6K2OzeLbqD5QJZ+WwK1xrs9ZLmhd53GbHn6fIS5PG+5ZZwNpYc/Wn7Poh34Pcb3+6ECGdthPMiWS29vu6V2gnLcnpffJ2M82fpkxnvZ3wFew9xmozlOuA86d9ixrLo5ZUvMZZb75YxL83ANxqf/zTrWlZ0x+9JHSD2thnbMkt4DfP4CShv0luHje5gDMuZIJ7iYFkIrnbmm+aYBUkAv1jmtEERPN1gHYrzQNpaJ0rlSwd6SClp20DEE0dM4HLOtd6A0jvMwM/6LMFclvdRBZb4nxoegl/y9XPLtgSafYI5oAfiY7o2Cr+ke0QnQJWZBp0cpZkFEJHrnx/KYBRGR6H4ot4COn8ViiohE98OymAURkegRERGR6BEREZHoERERkegRdUJ0LolEj/DAkhbRc3Ysqkj0CDdU0J9b3lG4MxZX50O3mAUNQQm3SxEa3RhFkscePcIDkzAHVDYzyUuYXYARkegRSvKA2dF1dAuQfDPg41hskegROoKDuV5orybX8V7MKTWvxaKLY/SIfPTM/L0QcyjBVPQHHDSC1GD886cDf8IcD704FmNERERERERERERERERT4P8KzDyXTrMqmQAAAABJRU5ErkJggg==";
var Zapper = {};

var logo = new Image();
logo.src = ZapperLogoBase64;

function checkPaymentStatus(silent)
{
	console.info("Checking payment status ...");
	
	var dlg = jQuery("#zapper-qrcode-dialog");

	/* Get Payment Status */
	var url = "OrderAction.do";
	var data = {
            action: "getZapperPaymentStatus"
    };
	
	if(window.PaymentManager){
		url = "PaymentAction.do";
		data = {
                action: "getZapperPaymentStatus",
                orderId: PaymentManager.orderId
        };
	}
	
	jQuery.ajax({
        type: "POST",
        url: url,
        data: data
    })
    .done(function (data, textStatus, jqXHR) { 
    	
    	if(data == null || jqXHR.status != 200){
    		if(!silent) alert('Failed to query payment status!');
    	}
    	
    	var json = JSON.parse(data);
    	
    	if(json["errorId"] > 0){
    		var error = json["errorDescription"];
    		alert("Failed to process payment! " + error);
    		return;
    	}

    	if(json.statusId == 2){
    		if(!silent) alert('No payment found!');
    	}
    	
    	if(json.statusId == 1){  
    		
    		/* check if full payment was made */
    		/*
    		 {
			    "message": null,
			    "statusId": 1,
			    "status": "Success",
			    "data": [{
			        "Payments": [{
			            "PaymentItems": [{
			                "CustomFields": [],
			                "ZapperId": "HLV4M2262FMO",
			                "InvoiceAmount": "5.00",
			                "Vouchers": [],
			                "PaymentStatusId": 1,
			                "ErrorDescription": "",
			                "AmountPaid": "5.00",
			                "ZapperDiscountAmount": "0.00",
			                "TipAmount": "0.00",
			                "ErrorId": null,
			                "PaymentStatus": "Success"
			            }],
			            "PosReference": "5000045"
			        }]
			    }],
			    "authenticationToken": "00000000-0000-0000-0000-000000000000",
			    "errorDescription": null,
			    "errorId": 0
			 }
    		 */
    		
    		var openAmt = (window.PaymentManager) ? PaymentManager.zapperAmt : ShoppingCartManager.getGrandTotal();
    		var paidAmt = 0.0;
    		var tipAmt = 0.0;
    		var invoiceAmt = 0.0;
    		
    		var data = json["data"];
    		
    		for(var i=0; i<data.length; i++)
    		{
    			var reference = data[i];
    			var payments = reference["Payments"];
    			
    			for(var j=0; j<payments.length; j++)
    			{
    				var payment = payments[j];
    				
    				var paymentItems = payment["PaymentItems"];
    				
        			for(var k=0; k<paymentItems.length; k++)
        			{
        				var paymentItem = paymentItems[j];
        				if(paymentItem["PaymentStatusId"] == 1)
        				{
        					paidAmt = paidAmt + parseFloat(paymentItem["AmountPaid"]);
        					invoiceAmt = invoiceAmt + parseFloat(paymentItem["InvoiceAmount"]);
        					tipAmt = tipAmt + parseFloat(paymentItem["TipAmount"]);
        				}
        			}
    			}    			
    		}
    		    		
    		if(invoiceAmt != openAmt){
    			if(!silent) alert("Received " + new Number(invoiceAmt).toFixed(2) + " of " + new Number(openAmt).toFixed(2));
    			
    			return;
    		}
    		
    		jQuery(dlg).dialog("close");
    		
    		var payment = new Hash();
    	  	payment.set('otk', json.authenticationToken);
	  		payment.set('tipAmt', tipAmt);
    	  	
    	  	clearInterval(Zapper.periodicCheck);
    	  	
    	  	if(window.PaymentManager)
    	  	{
    	  		payment.set('zapperAmt', PaymentManager.invoiceAmt);
    	  		PaymentManager.paymentHandler(payment);
    	  	}
    	  	else
    	  	{
    	  		/* Call Orderscreen payment handler */
        	  	OrderScreen.paymentHandler(payment);
    	  	}
    	  	
    	}
    	
    })
    .fail(function(jqXHR, textStatus){
    	if(!silent) alert("Failed to get payment status!");
    });
	
}

Zapper.showQRCodeDialog = function () {
    var dlg = jQuery("#zapper-qrcode-dialog");
    if (dlg.length == 0) {
        dlg = jQuery("<div id='zapper-qrcode-dialog' title='Zapper'><div style='text-align:center;'><img width='250px' src='images/zapper-logo.png'></div><div style='text-align:center; margin:20px; height:320px;' id='zapper-qrcode'>Generating QRCode ...</div><div id='zapper-grandtotal'></div></div>");
    }

    jQuery(dlg).dialog({
        width: "400px",
        height: "auto",
        modal: true,
        autoOpen: true,
        open: function(event) {
            jQuery("#zapper-print-qrcode-button").css({"position":"relative", "left":"-210px"});
            jQuery("#zapper-ok-button").css({"position":"relative", "left":"140px"});
            jQuery("#zapper-grandtotal").css({"font-size":"20px", "text-align":"center"});
            
            var currencySymbol = null;
            var grandTotal = null;
            
            if(window.PaymentManager)
		  	  	{
			  	  	currencySymbol = PaymentManager.getCurrencySymbol();
			  	  	grandTotal = PaymentManager.zapperAmt.toFixed(2);
		  	  	}
		  	  	else
		  	  	{
			  	  	currencySymbol = ShoppingCartManager.getCurrencySymbol();
			  	  	grandTotal = ShoppingCartManager.getGrandTotal().toFixed(2);
		  	  	}
            
            jQuery("#zapper-grandtotal").html(currencySymbol + " " + grandTotal);
            
            Zapper.periodicCheck = setInterval(function(){
            	checkPaymentStatus(true);
            	
            }, 5000);
        },
        beforeclose: function(){ 
        	var close = confirm("Are you sure you want to close Zapper payment?");
        	if(close == true){
        		clearInterval(Zapper.periodicCheck);
        	}
        	
        	return close;
        },
        position:['middle',40],
        buttons: [			 
              {
            	id: "zapper-ok-button",
                text: "OK",
                click: function() {
                	checkPaymentStatus(false);
                }
              },
              {
  				id: "zapper-print-qrcode-button",
  			    text: "Print QR Code",
  			    click: function() {
  			  	  	/* Print QRCODE */
  			  	  	console.info("Printing qrcode ...");
  			  	  	
  			  	  	var zapperQRCodeCanvas = jQuery("#zapper-qrcode canvas")[0];
  			  	  	
  			  	  	var zapperLogoCanvas = document.createElement("canvas");
  			  	  	zapperLogoCanvas.width = logo.width;
  			  	  	zapperLogoCanvas.heigth = logo.height;
  			  	  		
  			  	  	var context = zapperLogoCanvas.getContext('2d');
  			  	  	context.drawImage(logo, 0, 0, logo.width, logo.height);
  			  	  	
  			  	  	var currencySymbol = null;
  			  	  	var grandTotal = null;
  			  	  	
  			  	  	if(window.PaymentManager)
  			  	  	{
	  			  	  	currencySymbol = PaymentManager.getCurrencySymbol();
	  			  	  	grandTotal = PaymentManager.zapperAmt.toFixed(2);
  			  	  	}
  			  	  	else
  			  	  	{
	  			  	  	currencySymbol = ShoppingCartManager.getCurrencySymbol();
	  			  	  	grandTotal = ShoppingCartManager.getGrandTotal().toFixed(2);
  			  	  	}
  			  	  	
  			  	  	/*
  			  	  	 	Zapper.additionalText
        				Zapper.marketingGraphic
  			  	  	 */
  		              			  	  	
  			  	  
	  			  	var pageFormat = [
						['CENTER'],
						['CANVAS', Zapper.marketingGraphicCanvas1],
						['FEED'],
						['H1', "Total"],
						['H2', currencySymbol + " " + grandTotal ],
						['FEED'],
						['CANVAS', Zapper.marketingGraphicCanvas2],
						['FEED'],
						['CANVAS', zapperQRCodeCanvas],
						['FEED'],
						['CANVAS', Zapper.marketingGraphicCanvas3],
						['N', Zapper.additionalText],
						/*
						['B','Paying your bills has never been easier.'],
						['N', 'Terms and conditions apply.'],
						['N', '(www.zapper.com/restaurants/t&c.html)'],
						['FEED'],
						['H2', 'WELCOME TO THE FUTURE!'],
						['FEED'],						
						*/
						['FEED'],
						['PAPER_CUT']
  			  	   ];
	  			  	
	  			   PrinterManager.print(pageFormat);
  			    }
  			  }
            ]
    });
    
    var qrcodeContainer = dlg.find("#zapper-qrcode");
    qrcodeContainer.html("");
    
    var url = null;
    var data = null;
    
    if(window.PaymentManager){
    	
		url = "PaymentAction.do";
    	data = {
    			action: "getZapperQRCode",
    			orderId : PaymentManager.orderId,
    			"zapperAmt": PaymentManager.zapperAmt
    	};
    }
    else
    {
    	url = "OrderAction.do";
    	data = {
    			action: "getZapperQRCode"
    	};
    }
    
    jQuery.ajax({
            type: "POST",
            url: url,
            data: data
        })
        .done(function (data, textStatus, jqXHR) {        

        	if(data == null || jqXHR.status != 200){
        		qrcodeContainer.html("Failed to generate QRCode! " + textStatus);
        		return;
        	}
        	
        	var json = JSON.parse(data);
        	var qrcodeURL = json.qrcodeURL;
        	
        	Zapper.additionalText = json.additionalText || '';
        	Zapper.marketingGraphic1 = json.marketingGraphic1 || '';
        	Zapper.marketingGraphic2 = json.marketingGraphic2 || '';
        	Zapper.marketingGraphic3 = json.marketingGraphic3 || '';
        	
        	if(Zapper.marketingGraphic1.length > 0){        		
        		
        		var img1 = new Image();
        		img1.src = Zapper.marketingGraphic1;    
        		
        		Zapper.marketingGraphicCanvas1 = document.createElement("canvas");
        		    		
        		img1.onload= function() {
        			
        			var canvas = Zapper.marketingGraphicCanvas1;
        			canvas.width = img1.width;
        			canvas.height = img1.height;
        			
        			var context = canvas.getContext('2d');
                    context.drawImage(img1, 0, 0, img1.width, img1.height);
                };
        	}
        	
        	if(Zapper.marketingGraphic2.length > 0){        		
        		
        		var img2 = new Image();
        		img2.src = Zapper.marketingGraphic2;    
        		
        		Zapper.marketingGraphicCanvas2 = document.createElement("canvas");
        		    		
        		img2.onload= function() {
        			
        			var canvas = Zapper.marketingGraphicCanvas2;
        			canvas.width = img2.width;
        			canvas.height = img2.height;
        			
        			var context = canvas.getContext('2d');
                    context.drawImage(img2, 0, 0, img2.width, img2.height);
                };
        	}

			if(Zapper.marketingGraphic3.length > 0){        		
				
				var img3 = new Image();
				img3.src = Zapper.marketingGraphic3;    
				
				Zapper.marketingGraphicCanvas3 = document.createElement("canvas");
				    		
				img3.onload= function() {
					
					var canvas = Zapper.marketingGraphicCanvas3;
					canvas.width = img3.width;
					canvas.height = img3.height;
					
					var context = canvas.getContext('2d');
			        context.drawImage(img3, 0, 0, img3.width, img3.height);
			    };
			}
        	
            jQuery(qrcodeContainer).qrcode({
                size: 300,
                fill: '#000',
                text: qrcodeURL
            });
        })
        .fail(function(jqXHR, textStatus){
        	qrcodeContainer.html("Failed to generate QRCode! " + textStatus);
        });

};
