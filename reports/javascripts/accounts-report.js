Report.REPORT_CLASS_NAME = "report/AccountsReport";


Report.renderReportTable = function() {
	
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
	     "language": {
				         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				     }
    });
	
};

Report.getParams = function () {
	
	var params = {};
	
	/*if (Report.CustomerId != null)
	{
		params.c_bpartner_id = Report.CustomerId;
	};*/
	
	var fromDate = jQuery("#date-from").val();
	var toDate = jQuery("#date-to").val();
	
	params.fromDate = (fromDate.trim() ? moment(fromDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	params.toDate = (toDate.trim() ? moment(toDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	
	return params;
};

jQuery(document).ready(function() {
	jQuery( "#date-from" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
	
	jQuery( "#date-to" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
	
});