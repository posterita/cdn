Report.REPORT_CLASS_NAME = "report/AttendanceEmployeeReport";

var u_user_clockinout_id;
var salesrep_id;
var storeId;
var terminalId;
var clockintime;
var clockouttime;

Report.editClockInClockOut = function(data){
	//to fill in popup when the popup opens
	if (data!=null && data!=0){
		Report.renderAttendancePopup(data);
	}
	
	jQuery("#edit-clockin-clockout").dialog({
		modal: true,
		height: '450',
		width: '350',
		title: Translation.translate("edit.clock.in.out"),
		close:function(){
			jQuery("#edit-clockin-clockout input").val("");
			jQuery("#edit-clockin-clockout select").val("");
		},
	
		buttons: {
			Save : {
				'id' : 'save_attendance',
				'text' : 'Save',
				'class' : 'btn btn-xs btn-success',
				'click' : function(){
										
					var validatedataentered = Report.checkAttendanceDataEntered();
					
					if (validatedataentered)
					{
						//build json array
						var jsonarray = {}
						
						//check time entered
						var validatetime = Report.checkAttendanceTime(clockintime, clockouttime);
						
						if (validatetime)
						{
							if(data==0)//new record
							{
								jsonarray.u_user_clockinout_id = 0;
								jsonarray.salesrep_id = salesrep_id;
							}
							else//edit record
							{
								jsonarray.u_user_clockinout_id = u_user_clockinout_id;
							}
							jsonarray.store_id = storeId;
							jsonarray.terminal_id = terminalId;						
							jsonarray.clockintime = moment(clockintime).format('YYYY-MM-DD HH:mm:ss');
							jsonarray.clockouttime = moment(clockouttime).format('YYYY-MM-DD HH:mm:ss');
							
							//save data
							jQuery.ajax({
								type: "POST",
								url: "DataTableAction.do?action=saveAttendance",
								data: {"json": JSON.stringify(jsonarray)}
							}).done(function(data, textStatus, jqXHR){
								
								if(data == null || jqXHR.status != 200){
						    		alert('Failed to activate!');
						    	}
						    	
						    	var json = JSON.parse(data);
						    	
						    	if(json.success)
						    	{
						    		jQuery("#edit-clockin-clockout").dialog("close");	
						    		Report.update();

						    		Report.displayMessage(json.message);
						    	}
						    	else
						    	{
						    		jQuery("#edit-clockin-clockout").dialog("close");
						    		Report.displayError(json.message);
						    	}
							});
							
							return true;
						}
					}
					
					return false;
				}
			}
		}
	});
	
	jQuery("#save_attendance").html(Translation.translate("save"));
	jQuery("div.ui-dialog-titlebar").children("span").addClass("glyphicons popup").addClass("glyphicons-edit");
};

//render data in popup
Report.renderAttendancePopup = function (data) {
	var split = data.split('_');

	u_user_clockinout_id = split[0];
	salesrep_id = split[1];
	storeId = split[2];
	terminalId = split[3];
	clockintime = split[4];
	clockouttime = split[5];
	
	jQuery("#user_id").val(salesrep_id);
	jQuery("#store_id").val(storeId).change();
	jQuery("#terminal_id").val(terminalId);
	jQuery("#clock-in").val(clockintime);
	
	if (clockouttime != Translation.translate("still.working")){
		jQuery("#clock-out").val(clockouttime);
	}
}

//check data entered in popup
Report.checkAttendanceDataEntered = function () {
	//to retrieve data from the fields
	salesrep_id = jQuery("#user_id").val();
	storeId = jQuery("#store_id").val();
	terminalId = jQuery("#terminal_id").val();
	clockintime = jQuery("#clock-in").val();
	clockouttime = jQuery("#clock-out").val();
	
	if (salesrep_id.trim().length <= 0 || storeId.trim().length <= 0 || terminalId.trim().length <= 0 || clockintime.trim().length <= 0 || clockouttime.trim().length <= 0)
	{
		alert(Translation.translate("error.enter.all.data"));
		
		return false;
	}
	else
	{
		return true;
	}
}

//validate clockintime and clockouttime
Report.checkAttendanceTime = function (clockin, clockout) {
	
	var currentdate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	var clockin_time = moment(clockin).format('YYYY-MM-DD HH:mm:ss');
	var clockout_time = moment(clockout).format('YYYY-MM-DD HH:mm:ss');
		
	if (clockin_time > clockout_time)
	{
		alert(Translation.translate("error.clock.in.greater"));
		return false;
	}
	else
	{
		if (clockin_time==clockout_time)
		{
			alert(Translation.translate("error.clock.time.same"));
			return false;
		}
		else
		{
			return true;
		}
	}
}

Report.onFormReady(function() {
	jQuery('#clock-in').datetimepicker({
		maxDate:'+0d'
	});
	jQuery('#clock-out').datetimepicker({
		maxDate:'+0d'
	});
	
	//build dropdowns
	//copy sales reps
	jQuery('#user_id').html( jQuery('#ad_user_id').html() );
	jQuery('#user_id').val('');
	
	//build store & terminal dropdowns
	var stores = Report.storesDB({ad_org_id:{'>':0}}).get();

    DataTable.utils.populateSelect('#store_id', stores, "ad_org_name", "ad_org_id");
    
    jQuery('#store_id').val('');
    
    jQuery('#store_id').on("change", function() { 
    	
    	var ad_org_id = jQuery(this).val();
    	
    	var storesDB = Report.storesDB;

        var store = storesDB({
            "ad_org_id": {
                '==': ad_org_id
            }
        }).first();
        
        var terminals = store.terminals;

        DataTable.utils.populateSelect('#terminal_id', terminals, "u_posterminal_name", "u_posterminal_id");

    });
	
});

