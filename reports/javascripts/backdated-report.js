Report.REPORT_CLASS_NAME = "report/BackdatedReport";

Report.hash_id = null;
	
Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       {  "width": "auto", "targets": [0] },
                       {
    			        "render": function (data, type, row) {
    			        	
    			        	var doctype = row[3];
    			        	var id = row[6];
    			        	
    			        	var link;
    			        	
    			        	if( doctype == "Stock Transfer" ){
    			        		
    			        		link = "<a href='javascript:void(0);' onclick='viewTransfer(" + id + ")'>" + data + "</a>";
    			        		
    			        	}
    			        	else if( doctype == "Stock Adjustment" ){
    			        		
    			        		link = "<a href='javascript:void(0);' onclick='viewInventory(" + id + ")'>" + data + "</a>";
    			        		
    			        	}
    			        	else
    			        	{
    			        		link = "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
    			        	}
    			        	
    			        	return link;
    			        },
    			        "targets": 5
                       },
                       { "visible": false, "targets": [6] }
                      ],
	     "order": [ [0, "desc"] ],
	     "drawCallback": function (settings) {
	    	 
	     }
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	date.setTime(date.getTime() - (30*24*60*60*1000));
	
	var currentYear = date.getFullYear();
	var firstDay = "01";
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};


/* set report defaults */
Report.defaults = {
		"date-from" : moment(Report.getStartingDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY")
		
};

/* update report on load */
Report.updateOnReady = true;
