Report.REPORT_CLASS_NAME = "report/BestSellers";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
        "columnDefs": [ 
			             { className: "numberedCell" },
			             /*{ 	"visible": false, "targets": [5] }*/
		      		  ]
    });
};

function setIsSold()
{
	var isSold = jQuery("#issoldcheckbox").prop("checked");
	
	if (isSold)
	{
		jQuery("#issold").val("Y");
	}
	else
	{
		jQuery("#issold").val("N");
	}
}