Report.REPORT_CLASS_NAME = "report/CancelledSalesReceipt";

Report.renderReportTable = function() {
	
	Report.table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": false,
        "scrollCollapse": true,
        "columnDefs": [
	        		     {
          			        "render": function (data, type, row) {
          			          var id = row[9];
          			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
          			        },
          			        "targets": 1
 	         			 },
 	             		 { "visible": false, "targets": 9 }
                      ],
	     "language": {
				         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				     }
    });
	
};