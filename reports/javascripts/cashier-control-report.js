Report.REPORT_CLASS_NAME = "report/CashierControlReport";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                       { className: "numberedCell", "targets": [1, 2, 3, 4, 5, 6, 7, 8] },
                       { 
                            "width": "auto", "targets": [0] 
                       }
                       ],
         "order": [[0, "desc"]],
         "autoWidth": false,
         "language": {
	         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       }
    });
};
