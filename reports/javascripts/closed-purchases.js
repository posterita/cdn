Report.REPORT_CLASS_NAME = "report/ClosedPurchases";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                       { 
                            "width": "auto", "targets": [0] 
                       },
	        		   {
	     			        "render": function (data, type, row) {
	     			          var id = row[6];
	     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 1
	         		   },
	             	   { 	"visible": false, "targets": [6] }
                       ],
         "order": [[1, "desc"]],
         "autoWidth": false,
         "language": {
	         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       }
    });
};
