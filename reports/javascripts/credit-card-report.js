Report.REPORT_CLASS_NAME = "report/CreditCardReport";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                       { className: "numberedCell", "targets": [3, 4, 5] },
                       { 
                            "width": "auto", "targets": [0] 
                       },
	        		    {
	     			        "render": function (data, type, row) {
	     			          var id = row[10];
	     			          return "<a href='javascript:void(0);' onclick='viewPayment(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 4
	         			},
	         			{
	     			        "render": function (data, type, row) {
	     			          var id = row[12];
	     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 6
	         			},
	             		{ 	"visible": false, "targets": [10,11,12,13] }
                       ],
         "order": [[1, "desc"]],
         "autoWidth": false,
         "language": {
	         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       }
    });
};
