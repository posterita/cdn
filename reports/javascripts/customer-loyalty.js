Report.REPORT_CLASS_NAME = "report/CustomerLoyalty";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                   		{ className: "numberedCell", "targets": [1,2,3] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    }
	   });
};
