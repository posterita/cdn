Report.REPORT_CLASS_NAME = "report/DailyItemSalesReport";

Report.renderRemoteDate = function(){
	
	var dfd = jQuery.Deferred();

    var field = "[data-report-field='remote-date']";
    
    var currentDate = moment().format('MM/DD/YYYY');
    //set value
    jQuery(field).val( currentDate );

    dfd.resolve();
    
    jQuery("span.today").html(moment().format('LL'));

    return dfd.promise();
	
};

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
        "columnDefs": []
    });
};

Report.defaults = {
		"date-from" : moment().subtract(2, 'days').format('MM/DD/YYYY'),
		"date-to" : moment().format('MM/DD/YYYY'),
		"ad_org_id" : TerminalManager.terminal.orgId			
};

Report.updateOnReady = true;



