Report.REPORT_CLASS_NAME = "report/DailySalesReceipt";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
        "columnDefs": [ 
			             { className: "numberedCell" },
			             /*{ 	"visible": false, "targets": [5] }*/
			             {
		     			        "render": function (data, type, row) {
		     			          var id = row[7];
		     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
		     			        },
		     			        "targets": 1 
		         			},
		             		{ 	"visible": false, "targets": [7] }
		      		  ]
    });
};

Report.updateOnReady = true;