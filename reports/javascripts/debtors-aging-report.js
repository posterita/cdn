Report.REPORT_CLASS_NAME = "report/DebtorsAgingReport";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "order": [[0, "asc"]],        
        "columnDefs": [
        {
         			        "render": function (data, type, row) {
         			          var id = row[10];
         			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
         			        },
         			        "targets": 1
	         			 },
        { "visible": false, "targets": 10 },
                   		{ className: "numberedCell", "targets": [4,5,6,7,8,9] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "drawCallback": function ( settings ) {	    	
            
  	    }
	});
};
