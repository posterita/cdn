Report.REPORT_CLASS_NAME = "report/DebtorsAgingSummaryReport";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "order": [[0, "asc"]],
        "columnDefs": [
                   		{ className: "numberedCell", "targets": [1,2,3,4,5,6] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "drawCallback": function ( settings ) {	    	
            
  	    }
	});
};
