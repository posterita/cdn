Report.REPORT_CLASS_NAME = "report/DepositRedeemed";
	
Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                   		{ className: "numberedCell", "targets": [3, 4] },
	        		    {
	     			        "render": function (data, type, row) {
	     			          var id = row[9];
	     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 5
	         			},
	             		{ 	"visible": false, "targets": [9] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "order": [[1, "desc"]],
  	    "drawCallback": function ( settings ) {}
	});
};
