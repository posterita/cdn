Report.REPORT_CLASS_NAME = "report/DepositRefunded";
	
Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                   		{ className: "numberedCell", "targets": [4, 5] },
	        		    {
	     			        "render": function (data, type, row) {
	     			          var id = row[7];
	     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 6
	         			},
	             		{ 	"visible": false, "targets": [7] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "order": [[1, "desc"]]  	    
	});
};
