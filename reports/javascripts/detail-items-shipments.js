Report.REPORT_CLASS_NAME = "report/DetailItemsShipments";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
		"scrollCollapse": true,
		
		"columnDefs": [
			{ className: "numberedCell", "targets": [9, 10] },
			{
				"render": function(data, type, row) {
					var id = row[15];
					return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
				},
				"targets": 1
			},
			{
				"render": function(data, type, row) {
					var id = row[16];
					return "<a href='javascript:void(0);' onclick='viewShipment(" + id + ")'>" + data + "</a>";
				},
				"targets": 11
			},
			{ "visible": false, "targets": [15, 16] }
		],
		
		"language": {
			"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
		},
		"order": [[0, "desc"]]
	});
};
