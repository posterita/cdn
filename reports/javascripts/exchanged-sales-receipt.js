Report.REPORT_CLASS_NAME = "report/ExchangedSalesReceipt";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
        "columnDefs": [ 
			             { 	className: "numberedCell", "targets": [3, 4, 6, 11, 12, 13] },
			             { 	"width": "150px", "targets": 0 },
	        		     {
         			        "render": function (data, type, row) {
         			          var id = row[16];
         			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
         			        },
         			        "targets": 1
	         			 },
	             		 { "visible": false, "targets": 16 }
		      		  ],
	    "drawCallback": function ( settings ) {}
    });
};