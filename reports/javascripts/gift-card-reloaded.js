Report.REPORT_CLASS_NAME = "report/GiftCardReloaded";
	
Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                   		{ className: "numberedCell", "targets": [2] },
	        		    {
	     			        "render": function (data, type, row) {
	     			          var id = row[5];
	     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 4
	         			},
	             		{ 	"visible": false, "targets": [5] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "order": [[1, "desc"]],
  	    "drawCallback": function ( settings ) {}
	});
};
