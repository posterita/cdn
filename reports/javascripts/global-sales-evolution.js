Report.REPORT_CLASS_NAME = "report/GlobalSalesEvolution";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		 order: [],
		"searching": false,
        "autoWidth": false,
        "paging": false,
        "language": {
	         			"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       			},
	       			
        "columnDefs": [ 
                       { 	"className" : "numberedCell", 
                       		"targets": [1,2,3,4,5] 
                       },
                       {
                       		"render" : function (data, type, row) {
                       			return Report.formatNumber(data);
	         			    },
	         			    "targets": [1,2,4]
                       }
           		      ],
        "drawCallback": function (settings) {}
    });    
    
};



