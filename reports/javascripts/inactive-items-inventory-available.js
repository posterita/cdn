Report.REPORT_CLASS_NAME = "report/InActiveItemsInventoryAvailable";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": [1,2,3] }
                      ]
    });
};

Report.renderProductAutocomplete = function() {
	
	var dfd = jQuery.Deferred();
	
	dfd.notify('rendering product autocomplete');
	
	var selector = "[data-report-field='product']";
	var source = "DataTableAction.do?action=search&field=product&isactive=N";
	var labelKey = "name";
	var valueKey = "m_product_id";
	    
	Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);	
	
	var array = [
			"description", 
			"upc", 
			"primarygroup",
			"group1",
			"group2",
			"group3",
			"group4",
			"group5",
			"group6",
			"group7",
			"group8"
		];
	
	jQuery(array).each(function(index, value){
		
		var selector = "[data-report-field='product-" + value + "']";
		var source = "DataTableAction.do?action=search&field=product&isactive=N&column=" + value;
		var labelKey = value;
		var valueKey = value;
		
		dfd.notify('rendering product ' + value + ' autocomplete');
		
		Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);		
		
	});
	
	dfd.resolve("Successfully rendered product autocomplete");
	
	return dfd.promise();
};