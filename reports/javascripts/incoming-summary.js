Report.REPORT_CLASS_NAME = "report/IncomingSummaryReport";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
        	{ 	
        		"visible": false, "targets": [16,17] 
        	},
        	
        	{
		        "render": function (data, type, row) {
		          var id = row[16];
		          return "<a href='javascript:void(0);' onclick='viewPurchase(" + id + ")'>" + data + "</a>";
		        },
		        "targets": 1
 			},
 			
 			{
		        "render": function (data, type, row) {
		          var id = row[17];
		          return "<a href='javascript:void(0);' onclick='viewShipment(" + id + ")'>" + data + "</a>";
		        },
		        "targets": 2
 			}
                       ],
         "order": [[0, "desc"]],
         "autoWidth": false,
         "language": {
	         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       }
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	date.setTime(date.getTime() - (7*24*60*60*1000));
	
	var currentYear = date.getFullYear();
	var firstDay = ( date.getDate() < 10 ? "0" : "") + date.getDate();
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};

/* set report defaults */
Report.defaults = {
		"date-from" : moment(Report.getStartingDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY")
		
};

/* update report on load */
Report.updateOnReady = true;
