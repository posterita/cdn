Report.REPORT_CLASS_NAME = "report/InventoryAdjustment";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "order": [
                  [1, "desc"]
                 ],
        "columnDefs": [
                   		{ "visible": false, "targets": [5] },
                   		{
         			        "render": function (data, type, row) {
         			          var id = row[5];
         			          return "<a href='javascript:void(0);' onclick='viewInventory(" + id + ")'>" + data + "</a>";
         			        },
         			        "targets": 1
	         			 }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
  	    "drawCallback": function ( settings ) {}
	});
};
