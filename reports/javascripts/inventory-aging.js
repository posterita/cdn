Report.REPORT_CLASS_NAME = "report/InventoryAging";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "ordering": false,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": [2,3,4,5,6,7,8,9] }
                      ],
 	    "order": [
	               [0, "asc"]
	              ]
    });
	
	new jQuery.fn.dataTable.FixedColumns( table );
};
