Report.REPORT_CLASS_NAME = "report/InventoryAvailablePerBatch";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [{className: "numberedCell", "targets": [2,3,4,6,7,8,9]}],
        	
                      /*[{
                    	  className: "numberedCell", "targets": [2,3,4,6,7,8],
                    	  className: "x", "targets": [9],
                    	  
                    	  render: function (data, type, row) {
                    		  
                    		  if(jQuery("#showpurchaseprice").val() == "N"){
                    			  
                    			  return data;
                    		  }
                    		  
                              return data;
                          }     	  
                      },
                      {
                    	  className: "numberedCell", "targets": [2,3,4,6,7,8,9],
                    	  
                    	  render: function (data, type, row) {
                             
                    		  if(jQuery("#showpurchaseprice").val() == "Y"){
                    			  
                    			  return data;
                    		  }
                    		  
                              return data;
                          }     	  
                      }],*/
                      rowCallback: function(row, data, index){
                    	  
                    	  var threeMonthsFromToday = moment(new Date()).add(3, 'M').format('YYYY-MM-DD HH:mm:ss'); 
                    	  
                    	  if(data[5] < threeMonthsFromToday && data[5] != '')
                    	  {
                    		  jQuery(row).find('td:eq(0)').css('color', 'red');
                    	  }
                    	},
    });
};

/*function setIsSold()
{
	var isSold = jQuery("#issoldcheckbox").prop("checked");
	
	if (isSold)
	{
		jQuery("#issold").val("Y");
	}
	else
	{
		jQuery("#issold").val("N");
	}
}*/

function setPurchasePrice()
{
	var purchasePrice = jQuery("#showpurchasepricecheckbox").prop("checked");
	
	if (purchasePrice)
	{
		jQuery("#showpurchaseprice").val("Y");
	}
	else
	{
		jQuery("#showpurchaseprice").val("N");
	}
}

Report.onFormReady(function() {

    jQuery('#report-btn-reset').on("click", function(){
    	
    	setPurchasePrice();
    });
});