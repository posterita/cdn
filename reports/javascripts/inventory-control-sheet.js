Report.REPORT_CLASS_NAME = "report/InventoryControlSheet";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
		"order": [ [0, "desc"] ],
        "columnDefs": [
                       { className: "numberedCell", "targets": [2, 3, 4] },
                       { 
                            "width": "100px", "targets": [2, 3, 4] 
                       }
                       ]
    });
	
	/*
	jQuery('#addRow').on( 'click', function () {
        table.row.add( [
            "<input type='text' name='date' id='date' />", // date
            "<input type='text' name='transaction_type' id='transactionType' />", // transaction type
            "<input type='text' name='pos_qty' id='posQty' />", // pos qty
            "<input type='text' name='control_qty' id='controlQty' />", // control qty
            "<input type='text' name='difference' id='difference' />", // difference
            "<input type='text' name='comments' id='comments' />" // comments
        ] ).draw( false );
    });
	*/
	
	jQuery('#sendbutton').click( function() {
		var noOfInputs = table.rows().nodes().page.len(-1).draw(false).length;
		
		var records = [];
		
		for (var i=0; i<noOfInputs; i++)
		{
			var controlQtyValue = jQuery('input[id=controlQty' + i + ']').val();
			var commentsValue = jQuery('input[id=comments' + i + ']').val();
			var keyValue = jQuery('input[id=key' + i + ']').val();
			var isRowAlreadyExistValue = jQuery('input[id=isRowAlreadyExist' + i + ']').val();
			
			if (!(controlQtyValue.length === 0) || !(commentsValue.length === 0))
			{
				var record = {};
				
				record["key"] = keyValue;
				record["control_qty"] = ( (controlQtyValue.length === 0) ? "0" : controlQtyValue);
				record["comments"] = commentsValue;
				record["is_row_already_exist"] = isRowAlreadyExistValue;
				
				records.push(record);
			}
		}
		
		if (records == 0) {
			Report.displayError("No filled fields to save");
		}
		else {
			//save data
			jQuery.ajax({
				type: "POST",
				url: "DataTableAction.do?action=saveInventoryControlSheet",
				data: {"json": Object.toJSON(records)}
			}).done(function(data, textStatus, jqXHR){
				
				if(data == null || jqXHR.status != 200){
		    		alert('Failed to activate!');
		    	}
		    	
		    	var json = JSON.parse(data);
		    	
		    	if(json.success)
		    	{
		    		Report.displayMessage(json.message);
		    	}
		    	else
		    	{
		    		Report.displayError(json.message);
		    	}
			});
		}
		
		return false;
	} );
};

Report.renderDifference = function (element) {
	   var id = element.id;
	   var index = (id.split('controlQty'))[1];

	   var difference = 0;
	   var pos_qty = jQuery('#posQty'+index).val();
	   var control_qty = jQuery('#controlQty'+index).val();
	   
	   if (control_qty != null)
	   {
		   if (isNaN(control_qty))
		   {
			   jQuery('#controlQty'+index).val("");
		   }
		   else
		   {
			   difference = pos_qty - control_qty;
		   }
	   }
	   
	   jQuery('#difference'+index).val(difference);
}


Report.setDefault = function() {
	jQuery("#ad_org_id").val(jQuery("#org_id").val());
};

Report.onFormReady(function() {
	
	jQuery("#ad_org_id option[value='0']").hide();
	
	if (window.location.hash.length == 0) {
		Report.setDefault();
	}
});
