Report.REPORT_CLASS_NAME = "report/InventoryCountsByItems";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "order": [
                  [1, "desc"]
                 ],
        "columnDefs": [
                   		{ className: "numberedCell", "targets": [4, 5, 6, 7] },
                   		{ "visible": false, "targets": [12,13,14,15,16,17,18,19,20] },
                   		{
         			        "render": function (data, type, row) {
         			          var id = row[20];
         			          return "<a href='javascript:void(0);' onclick='viewInventory(" + id + ")'>" + data + "</a>";
         			        },
         			        "targets": 1 
	         			 }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
  	    "drawCallback": function ( settings ) {}
	});
};
