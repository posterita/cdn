Report.REPORT_CLASS_NAME = "report/InventoryHistoryByStore";

Report.renderReportTable = function() {

	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=2; i<size; i++)
	{
		array[i-1] = i;
	}
	
	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": array }
                      ]
    });
};

Report.getParams = function() {
	var inventory_date = jQuery("#inventory_date").val();
	
	if (inventory_date.trim())
	{
		jQuery("#inventory_date").val(moment(inventory_date).format("YYYY-MM-DD 00:00:00"));
	}
	
    var params = jQuery("#report-form").serialize();
    
    if (inventory_date.trim())
	{
		jQuery("#inventory_date").val(moment(inventory_date).format("YYYY-MM-DD"));
	}
    return params;
};
