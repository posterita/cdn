Report.REPORT_CLASS_NAME = "report/IssuedRedeemedCoupon";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
		"order": [ [0, "desc"] ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "columnDefs": [ 
			             { 	className: "numberedCell", "targets": [2,3,4,5] },
			             { 	className: "capitalizeStatus", "targets": [7] },
			             {
         			        "render": function (data, type, row) {
         			          var id = row[18];
         			          if(id > 0){
         			        	 return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
         			          }
         			          else
         			          {
         			        	  return data;
         			          }
         			          
         			        },
         			        "targets": 7
	         			 },
	         			{ "visible": false, "targets": 18 }
		      		  ]
	   });
};

Report.onFormReady(function(){
	
	
	
});