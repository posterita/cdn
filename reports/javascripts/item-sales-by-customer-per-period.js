Report.REPORT_CLASS_NAME = "report/ItemSalesByCustomerPerPeriod";

Report.renderReportTable = function() {
	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=3; i<size; i++)
	{
		array[i-1] = i;
	}
	
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
        "columnDefs": [ 
			             { className: "numberedCell", "targets": array },
					     { "visible": false, "targets": 0 }
		      		  ],
        "drawCallback": function (settings) {
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    jQuery(rows).eq( i ).before(
                        '<tr class="group"><td colspan="14"><strong>'+ Translation.translate("customer.name") +':&nbsp;&nbsp;&nbsp;</strong>'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            });
        }
    });
};