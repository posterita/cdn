Report.REPORT_CLASS_NAME = "report/ItemsSalesByPaymentType";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
		"columnDefs": [ 
			             { className: "numberedCell", "targets": [3,4,5,7] },
	                     { "width": "150px", "targets": 0 },
			             {
					        "render": function (data, type, row) {
						          return parseFloat(data).format(2);
						     },
						     "targets": [3,4,5,7]
					      },
					      {
					        "render": function (data, type, row) {
					        	  var date = moment(data).format("MMM DD YYYY, hh:mm a")
						          return date;
						     },
						     "targets": 0
					      },
					      {
         			        "render": function (data, type, row) {
           			          var id = row[12];
           			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
           			        },
           			        "targets": 1
           			      },
					      { "visible": false, "targets": [6,12] }
		      		  ],
  		"order": [[6, "asc"]],
        "drawCallback": function (settings) {
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            
            api.column(6, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    jQuery(rows).eq( i ).before(
                        '<tr class="group"><td colspan="11"><strong>'+ Translation.translate("payment.type") +':&nbsp;&nbsp;&nbsp;</strong>'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            });
            
            var trLength = jQuery(rows[0]).children('td').length;
            var totals = [];
            var columnToCalculate = [3,4];
            

            for (var j = 0; j < columnToCalculate.length; j++) {
				totals[j] = 0;
		
				for (var i = 0; i < rows.length; i++) {
					
					var trValue = jQuery(rows[i]).children('td').eq(columnToCalculate[j]).text() + "";
		
					trValue = trValue.replace(/,/g, '');
					totals[j] = totals[j] + parseFloat(trValue);
				}
			}

            var tr = '<tr><td>' + Translation.translate("total") + ':</td>';


			for (var k = 1; k < trLength; k++) {

				if (jQuery.inArray(k, columnToCalculate) >= 0) {

					for (var l = 0; l < columnToCalculate.length; l++) {
						var index = columnToCalculate[l];

						if (index == k) {
							
							tr = tr + '<td class="numberedCell">' + parseFloat(totals[l]).format(2) + '</td>';
							break;
						}
					}

				} else {
					tr = tr + '<td class="numberedCell"></td>';
				}
			}

			tr = tr + '</tr>';

			jQuery(rows).eq(rows.length - 1).after(tr);
        }
    });
};