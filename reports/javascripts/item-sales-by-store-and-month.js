Report.REPORT_CLASS_NAME = "report/ItemSalesByStoreAndMonth";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"ordering": false,
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    }
	   });
	
		
	/* new jQuery.fn.dataTable.FixedColumns( table ); */
};

Report.getParams = function() {
	
	var startYear = jQuery("#year").val();
	var startMonth = jQuery("#start_month").val();
	var period = parseInt(jQuery("#period").val());
	
	var toDate = new Date(startYear + "/" + startMonth);
	
	var fromDate = moment(toDate).subtract(period, 'month');
	
	toDate = moment(toDate).endOf('month');
	
	jQuery("#date-to").val(moment(toDate).format("MM/DD/YYYY"));
	jQuery("#date-from").val(moment(fromDate).format("MM/DD/YYYY"));
	
    var params = jQuery("#report-form").serialize();
    return params;
};

Report.selectCurrentMonth = function() {
	
	var select = jQuery("#start_month option");
	var currentMonth = new Date().getMonth();
	
	select.eq(currentMonth).prop('selected', true);
};
 
Report.onFormReady(function() {
	Report.selectCurrentMonth();
});