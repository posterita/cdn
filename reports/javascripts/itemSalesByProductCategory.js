Report.REPORT_CLASS_NAME = "report/ItemSalesByProductCategory";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": [3,4,5,6,7,8] },
                       {  "width": "auto", "targets": [0] },
	             	   { "visible": false, "targets": 0 }
                      ],
	     "order": [
	               [0, "asc"], 
	               [1, "asc"]
	              ],
	     "drawCallback": function (settings) {
		        	var api = this.api();
		            var rows = api.rows( {page:'current'} ).nodes();
		            var last=null;
		            
		            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
		                if ( last !== group ) {
		                    jQuery(rows).eq( i ).before(
		                        '<tr class="group"><td colspan="15"><strong>'+ Translation.translate("primary.group") +':&nbsp;&nbsp;&nbsp;</strong>'+group+'</td></tr>'
		                    );
		 
		                    last = group;
		                }
		            });
		            
		            var trLength = jQuery(rows[0]).children('td').length;
		            
		            var totals = [];
		            
		            var columnToCalculate = [2, 3, 4, 5, 6, 7];
		            
		        	for(var j=0; j<columnToCalculate.length; j++){
		        		 totals[j] = 0;
		  
						 for (var i=0; i<rows.length; i++){
		        			 var trValue = jQuery(rows[i]).children('td').eq(columnToCalculate[j]).text() + "";
		        			 
		        			 trValue = trValue.replace(/,/g,'');
		        			 
		        			 totals[j] = totals[j] + parseFloat(trValue);
		        		 }
		        			
		        	}
		        	
		        	var tr = '<tr><td>' + Translation.translate("total") + ':</td>';
		        	
		        	for(var k=1; k<trLength; k++){
		        		
		        		if (jQuery.inArray(k, columnToCalculate) >= 0){
		        			
		        			for (var l=0; l<columnToCalculate.length; l++)
		        			{
		        				var index = columnToCalculate[l];
		        				
		        				if (index==k) {
		        					
		        					if (l == 0)
		    						{
		        						tr = tr + '<td class="numberedCell">' + parseInt(totals[l]).format(0) + '</td>';
		    						}
		        					else
		    						{
		        						tr = tr + '<td class="numberedCell">' + parseFloat(totals[l]).format(2) + '</td>';
		    						}
		        					
		        					break;
		        				}
		        			}
		        			
		        		} else {
		        			tr = tr + '<td class="numberedCell"></td>';
		        		}
		        	}
		        		
		        	tr = tr + '</tr>';
		        	
		        	jQuery(rows).eq(rows.length - 1).after(
		        			tr
		            );
 		}
    });
};

function setIsSold()
{
	var isSold = jQuery("#issoldcheckbox").prop("checked");
	
	if (isSold)
	{
		jQuery("#issold").val("Y");
	}
	else
	{
		jQuery("#issold").val("N");
	}
}