Report.REPORT_CLASS_NAME = "report/ItemsInventoryByWarehouse";

Report.renderReportTable = function() {
	
	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=2; i<size; i++)
	{
		array[i-2] = i;
	}

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": array }
                      ],
	     "order": [
	               [0, "asc"]
	              ]
    });
};

/*function setIsSold()
{
	var isSold = jQuery("#issoldcheckbox").prop("checked");
	
	if (isSold)
	{
		jQuery("#issold").val("Y");
	}
	else
	{
		jQuery("#issold").val("N");
	}
}*/

/*function setPurchasePrice()
{
	var purchasePrice = jQuery("#hidepurchasepricecheckbox").prop("checked");
	
	if (purchasePrice)
	{
		jQuery("#hidepurchaseprice").val("Y");
	}
	else
	{
		jQuery("#hidepurchaseprice").val("N");
	}

}*/