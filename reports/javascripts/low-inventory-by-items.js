Report.REPORT_CLASS_NAME = "report/LowInventoryByItemsNew";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell" }
                      ]
    });
};
