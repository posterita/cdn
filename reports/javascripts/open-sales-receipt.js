Report.REPORT_CLASS_NAME = "report/OpenSalesReceipt";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
		"columnDefs": [ 
			             { className: "numberedCell", "targets": 2 },
			             /*
			             {
					        "render": function (data, type, row) {
					        		
					        	  if (data == "null") return 0;
					        	  
						          return parseFloat(data).format(0);
						     },
						     "targets": 2
					      },
					      */
					      {
					        "render": function (data, type, row) {
					        	  var date = moment(data).format("MMM DD YYYY, hh:mm a")
						          return date;
						     },
						     "targets": 0
					      },
					      {
         			        "render": function (data, type, row) {
         			          var id = row[7];
         			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
         			        },
         			        "targets": 1
         			      },
         			     { "visible": false, "targets": 7 }
		      		  ],
  		"drawCallback": function ( settings ) {
  		   var api = this.api();
           var rows = api.rows( {page:'current'} ).nodes();
           var last=null;
  		           
  		   var trLength = jQuery(rows[0]).children('td').length;
  		   var totals = [];
  		   var columnToCalculate = [2];
  		           
  		   for(var j=0; j<columnToCalculate.length; j++){
  		        totals[j] = 0;
  		 
				for (var i=0; i<rows.length; i++){
			        var trValue = jQuery(rows[i]).children('td').eq(columnToCalculate[j]).text() + "";
			       
			        trValue = trValue.replace(/,/g,'');
			       
			        totals[j] = totals[j] + parseFloat(trValue);
			    }
  		       
  		    }
  		       
  		   	var tr = '<tr><td>' + Translation.translate("total") + ':</td>';
  		    
  		    for(var k=1; k<trLength; k++) {
  		       
  		        if (jQuery.inArray(k, columnToCalculate) >= 0){
  		       
	  		        for (var l=0; l<columnToCalculate.length; l++)
	  		        {
	  		        	
	  		        	var index = columnToCalculate[l];
	  		       
	  		        	if (index==k) {
	  		       
	  		        		if (l == 0)
	  		        		{
	  		        			tr = tr + '<td class="numberedCell">' + parseInt(totals[l]).format(0) + '</td>';
	  		        		}
	  		        		else
	  		        		{
	  		        			tr = tr + '<td class="numberedCell">' + parseFloat(totals[l]).format(2) + '</td>';
	  		        		}
	  		       
	  		        		break;
	  		        	}
	  		        }
  		       
  		        } else {
  		        	tr = tr + '<td class="numberedCell"></td>';
  		        }
  		        
  		    }
  		       
  		    tr = tr + '</tr>';
  		       
  		    jQuery(rows).eq(rows.length - 1).after(
  		        tr
  		    );
  		}
    });
};