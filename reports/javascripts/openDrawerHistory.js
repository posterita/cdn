Report.REPORT_CLASS_NAME = "report/OpenDrawerReport";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "order": [[0, "desc"]],
        "drawCallback": function (settings) {}
    });
};
