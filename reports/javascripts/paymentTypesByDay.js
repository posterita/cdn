Report.REPORT_CLASS_NAME = "report/PaymentTypesByDay";

Report.renderReportTable = function() {
	
	var size = jQuery("#example thead th").size();
	
	var array = [];
	
	for(var i=1; i<size; i++)
	{
		array[i-1]=i;
	}
	
    var table = jQuery('#example').DataTable({
    	"paging":   false,
    	"searching":   false,
        "ordering": false,
        "info":     false,
        "scrollX": true,
        "scrollCollapse": true,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
        "columnDefs": [ 
                       { className: "numberedCell", "targets": array }
                      ]
    });
    
    new jQuery.fn.dataTable.FixedColumns( table );
};