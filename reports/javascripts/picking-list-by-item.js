Report.REPORT_CLASS_NAME = "report/PickingListByItems";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
            
			            {  "width": "auto", "targets": [0] },
			            {
					        "render": function (data, type, row) {
					          var id = row[9];
					          return "<a href='javascript:void(0);' onclick='viewPickingList(" + id + ")'>" + data + "</a>";
					        },
					        "targets": 0
			            },
			            { "visible": false, "targets": 9 }
        ],
	     "order": [ [1, "asc"] ]
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	
	var currentYear = date.getFullYear();
	var firstDay = "01";
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};

Report.setDefault = function() {
	jQuery("#date-from").val(moment(Report.getStartingDate()).format("MM/DD/YYYY"));
	jQuery("#date-to").val(moment(Report.getCurrentDate()).format("MM/DD/YYYY"));
	jQuery("#warehouse_from").val(jQuery("#warehouse_id").val());
	//jQuery("#document_status").val('CO');
};

Report.onFormReady(function() {
	
	if (window.location.hash.length == 0) {
		Report.setDefault();
		
		Report.update();
	}
});

var viewPickingList = function(id){
	Report.preloader.show();
	
	window.location = "PickingListAction.do?action=view&pickingListId=" + id;
};
