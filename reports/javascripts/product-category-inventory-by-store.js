Report.REPORT_CLASS_NAME = "report/ProductCategoryInventoryByStore";

Report.renderReportTable = function() {
	
	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=1; i<size; i++)
	{
		array[i-1] = i;
	}

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": array }
                      ],
	     "order": [
	               [0, "asc"]
	              ]
    });
};

/*Report.getParams = function () {
	
	var params = {};
	
	var category = jQuery("#product-category option:selected");
	
	var groupname = category.text();

	params.group_option = category.val();
	params.group_name = groupname.substring(1, groupname.length-1);
	
	return params;
};

jQuery(document).ready(function() {
	
	
});*/