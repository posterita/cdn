Report.REPORT_CLASS_NAME = "report/ProductCategoryInventoryMatrix";

Report.renderReportTable = function() {

	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=0; i<size; i++)
	{
		array[i-1] = i;
	}
	
	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                       { className: "numberedCell", "targets": array },
                       { 
                            "width": "auto", "targets": [0] 
                       }
                       ],
         "order": [[0, "asc"]],
         "autoWidth": false,
         "language": {
				         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				      },
	     "drawCallback": function ( settings ) {
	    	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            
            var trLength = jQuery(rows[0]).children('td').length;
            
            var totals = [];
            
        	for(var j=1; j<trLength; j++){
        		 totals[j-1] = 0;
        		 
        		 for (var i=0; i<rows.length; i++){
        			 var trValue = jQuery(rows[i]).children('td').eq(j).text() + "";
        			 
        			 if (trValue == "-") continue;
        			 
        			 trValueOrder = trValue.substring(0, trValue.lastIndexOf("("));
        			 trValueOrder = trValue.replace(/,/g,'');
        			 /*console.log(trValueCount);*/
        			 
        			 totals[j-1] = totals[j-1] + parseFloat(trValueOrder);
        		 }
        	}
        	
        	var tr = '<tr><td>' + Translation.translate("total") + ':</td>';
        	
        	for(var k=0; k<totals.length; k++){
        		tr = tr + '<td class="numberedCell">' + parseFloat(totals[k]).format(0) + '</td>';
        	}
        		
        	tr = tr + '</tr>';
        	
        	jQuery(rows).eq(rows.length - 1).after(
        			tr
            );
            
	     }
    });
};
