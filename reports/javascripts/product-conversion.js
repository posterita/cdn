Report.REPORT_CLASS_NAME = "report/ProductConversion";
	
Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "columnDefs": [
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "order": [[4, "asc"]],
	});
};

Report.setDefault = function() {
	jQuery("#date-from").val(moment(Report.getStartingDate()).format("MM/DD/YYYY"));
	jQuery("#date-to").val(moment(Report.getCurrentDate()).format("MM/DD/YYYY"));
	jQuery("#warehouse_from").val(jQuery("#warehouse_id").val());
	//jQuery("#document_status").val('CO');
};

Report.onFormReady(function() {
	
	if (window.location.hash.length == 0) {
		Report.setDefault();
		
		Report.update();
	}
});