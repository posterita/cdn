Report.REPORT_CLASS_NAME = "report/RePrintReport";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "order": [[0, "desc"]],
        "drawCallback": function (settings) {},
        "columnDefs": [
                       
          		     {
          		        "render": function (data, type, row) {
          		          var id = row[5];
          		          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
          		        },
          		        "targets": 4
             			  },
          		      { "visible" : false, "targets" : 5 }]
    });
};
