Report.REPORT_CLASS_NAME = "report/RemovedItemLinesReport";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                       { className: "numberedCell", "targets": [2] },
                       { 
                            "width": "auto", "targets": [0] 
                       }
                       ],
         "order": [[0, "desc"]],
         "autoWidth": false,
         "language": {
	         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       }
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	date.setTime(date.getTime() - (7*24*60*60*1000));
	
	var currentYear = date.getFullYear();
	var firstDay = ( date.getDate() < 10 ? "0" : "") + date.getDate();
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};

/* set report defaults */
Report.defaults = {
		"date-from" : moment(Report.getStartingDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY")
		
};

/* update report on load */
Report.updateOnReady = true;