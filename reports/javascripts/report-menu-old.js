var ReportMenuOld = {};

ReportMenuOld.buildParentMenu = function() {
	var menus = ReportMenuOld.menuJSON;
	
	for(var i=0; i<menus.length; i++){
		var parentMenu = menus[i];
		
		var childContainer = "<div class='report-submenus'>";
		
		var categories = parentMenu.categories;
		for(var j=0; j< categories.length; j++){
			
			var category = categories[j];
			
			var className = category.category.toLowerCase();
			className = className.replace(/ /g, "-");
			
			childContainer = childContainer + "<div>"
							+ "<p class='glyphicons report-icons'>" + category.category +"</p>" 
							+ "<div class='inner-report-menu-container'>";
			
			var categoryMenus = category.menus;
			for (var k=0; k<categoryMenus.length; k++){
				childContainer = childContainer + "<div><a href=\""
								 + categoryMenus[k].link + "\">"
								 + categoryMenus[k].name +  "</a></div>";
			}
			
			childContainer = childContainer + "</div></div>";
			
		}
		
		childContainer = childContainer + "</div>";
		
		jQuery("#report-homepage-container").append(childContainer);
		
	}
}

jQuery(document).ready(function() {
	
	ReportMenuOld.buildParentMenu();
	
	jQuery(".report-btn-new").on("click", function() {
		window.location = "backoffice-reports.do";
	});
	
});