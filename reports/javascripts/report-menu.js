var ReportMenu = {};

ReportMenu.getMenuJson = function() {
	jQuery.post("DataTableAction.do", { action: "getReportMenus"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}
				
				ReportMenu.menuJSON = json;
				
				ReportMenu.buildParentMenu();
				
				jQuery(".report-categories li").on("click", function() {

					jQuery(".report-categories li")
						.removeClass("active")
						.children("i")
						.removeClass("fa-chevron-right")
						.addClass("fa-chevron-down");
					
					jQuery(this)
						.addClass("active")
						.children("i")
						.removeClass("fa-chevron-down")
						.addClass("fa-chevron-right");
					
					ReportMenu.getReportByCategory(jQuery(this).attr("id"), jQuery(this).text());
				});
				
			},"json").fail(function(){
				alert("Failed to request data!");
			});
}

ReportMenu.buildParentMenu = function() {
	var menus = ReportMenu.menuJSON;
	
	var parentContainer = jQuery("ul.report-categories");
	
	//array of name for "new" tag
	//var newParentReportsArray = ["Debtors and Creditors Report", "Employees Report"];
	var newReportsArray = ["Inventory Transfers by items"];
	
	for(var i=0; i<menus.length; i++){
		var parentMenu = menus[i];
		
		if (parentMenu.categories.length > 0)
		{
			var parent = "<li id='"  + parentMenu.id + "'>" + Translation.translate(parentMenu.name);
			
			//add new tag for new reports
			/*if (jQuery.inArray(parentMenu.name, newParentReportsArray) >= 0){
				parent = parent + "<span class='report-new'>New</span>"
			}*/
			
			parent = parent	+ "<i class='fa fa-chevron-down arrow-icons'></i>" 
					+ "</li>";
			
			parentContainer.append(parent);
			
			var childContainer = "<div id='report-" + parentMenu.id + "' class='report-submenus' style='display: none'>";
			
			var categories = parentMenu.categories;
			for(var j=0; j< categories.length; j++){
				
				var category = categories[j];

				var className = category.category.toLowerCase();
				className = className.replace(/ /g, "-");
				
				childContainer = childContainer + "<div>"
								+ "<p class='glyphicons  glyphicons-" + category.categorykey + " report-icons'>" + Translation.translate(category.category) +"</p>" 
								+ "<div class='inner-report-menu-container' data-test-id='" + category.categorykey + "'>";
				
				var categoryMenus = category.menus;
				var menu;
				
				for (var k=0; k<categoryMenus.length; k++)
				{
					menu = categoryMenus[k];
					
					childContainer = childContainer + "<div" + (menu.hasAccess == false ? " class='report-menu-lock'" : "") + "><a href=\"" + menu.link + "\">" + Translation.translate(menu.name) + "</a>"
									 
					if (menu.link.toLowerCase().indexOf('coming soon') > 0) 
					{
						childContainer = childContainer + '<span class="report-coming-soon">' + Translation.translate("coming.soon") + '</span>'
					}
					
					//add new tag for new reports
					if (jQuery.inArray(menu.name, newReportsArray) >= 0){
						childContainer = childContainer + "<span class='report-new'>" + Translation.translate("new") + "</span>"
					}
					
					childContainer = childContainer +"</div>";
				}
				
				childContainer = childContainer + "</div></div>";
				
			}
			
			childContainer = childContainer + "</div>";
			
			jQuery("#report-homepage-container").append(childContainer);
		}
	}
}

ReportMenu.getReportByCategory = function(id, categoryName) {
	
	if (categoryName.indexOf("New") >= 0)
	{
		categoryName = categoryName.substring(0, categoryName.length - 3);
	}
	
	jQuery("#report-breadcrumb-inactive").css({
		"display": "none"
	});
	
	jQuery("#report-breadcrumb-active").css({
		"display": "block"
	});
	
	jQuery("#dynamic-report-category").html(categoryName);
	
	jQuery("#report-homepage-container > div[id != 'report-categories-menu-container']").css({
		"display" : "none"
	});
	
	jQuery("#report-" + id).css({
		"display" : "block"
	});
}

ReportMenu.cancel = function(){
	
	var reportHomepageVisible = jQuery("#report-breadcrumb-inactive :visible").length;
	
	if (reportHomepageVisible > 0) {
		history.go(-1);
		
		return;
	}
	
	jQuery("#report-breadcrumb-active").css({
		"display": "none"
	});
	
	jQuery("#report-breadcrumb-inactive").css({
		"display": "block"
	});
	
	jQuery("#report-homepage-container > div[id = 'report-homepage-empty-container']").nextAll().css({
		"display": "none"
	});
	
	/*jQuery("#report-homepage-empty-container").css({
		"display": "block"
	});*/
	
	jQuery(".report-categories li")
	.removeClass("active")
	.children("i")
	.removeClass("fa-chevron-right")
	.addClass("fa-chevron-down");
	
	ReportMenu.defaultSelection();
	
}

ReportMenu.defaultSelection = function(){

	var defaultSelectedList = jQuery(".report-categories li").eq(0);
	
	ReportMenu.getReportByCategory(jQuery(defaultSelectedList).attr("id"), jQuery(defaultSelectedList).text());
	
	jQuery(defaultSelectedList)
	.addClass("active")
	.children("i")
	.removeClass("fa-chevron-down")
	.addClass("fa-chevron-right");
}

ReportMenu.sortMenusAscendingOrder = function(){
	
	/*sort report menus in ascending order*/
	var menus = ReportMenu.menuJSON;
	
	for(var i=0; i<menus.length; i++){
		var parentMenu = menus[i];
		
		if (parentMenu.categories.length > 0)
		{
			var categories = parentMenu.categories;
			for(var j=0; j< categories.length; j++)
			{
				var category = categories[j]; /*{"categorykey": "category1", "menus": "[{\"hasAccess\": true, \"name\": \"smenu.sales.totals.by.period.report.new\", \"link\": \"report-sales-total-by-period.do?pmenuId=10002371&menuId=10002372\", \"id\": 10002372}, .......]","category": "sales.reports.new"}*/
				
				var categoryDiv = document.querySelector("[data-test-id = '" + category.categorykey + "']");

				var menuLinkDiv = document.querySelectorAll("[data-test-id = '" + category.categorykey + "'] div");/*get all menus for category. e.g for category1*/
				
				var sortedMenusArr = [].slice.call(menuLinkDiv).sort(function (a, b) {
					
				       return a.textContent > b.textContent ? 1 : -1;				       
				   });
				
				sortedMenusArr.forEach(function (m) {
					
					categoryDiv.appendChild(m);					
				});				
			}			
		}		
	}	
}

ReportMenu.searchMenusAutocomplete = function(){
	
	/*autocomplete*/
	jQuery( "#filter_menu" ).autocomplete({
		
		source: function(request, response) {
			
			var filteredMenusArr = [];
			var obj = {};/*cache search term with matching menus*/
			
			//var parentMenuId = jQuery(".report-categories > li.active").attr("id");
			
			var allMenus = ReportMenu.menuJSON;
			
			for(var i=0; i<allMenus.length; i++){
				
				//if(allMenus[i].id == parentMenuId){
					
					var categories = allMenus[i].categories;
					
					for(var j=0; j< categories.length; j++){
						
						var menuList = categories[j].menus;						
						
						for(var k=0; k< menuList.length; k++){
							
							var menuObj = {}
							
							var name = Translation.translate(menuList[k].name);
							var link = menuList[k].link;
							
							menuObj['name'] = name;
							menuObj['link'] = link
							
							filteredMenusArr.push(menuObj);							
						}						
					}					
				//}
			}			
			
			/*match search term*/
			var searchTerm = request.term.toLowerCase();
			
			if ( !(searchTerm in obj) ) {
		    
		    var matcher = new RegExp("\\b" + jQuery.ui.autocomplete.escapeRegex(searchTerm), "i");
		    
		    obj[searchTerm] = filteredMenusArr.filter(function(menu) {
		    	
		      return matcher.test(menu.name);
		    });
		    
		  }
		  response( obj[searchTerm] );
		},        
		
		//source: "DataTableAction.do?action=filtermenu&parentMenuId=" + parentMenuId,	
		
		create: function() {
			
			var label = "name";
			
			jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
				
				if(item == null) return jQuery("<li>");
				
			      return jQuery( "<li>" )
			        .data( "item.autocomplete", item )
			        .append( "<a>" + item[label] + "</a>" )
			        .appendTo( ul );
			};
	    },
	    
	    select: function(event, ui) {
	    	
            var field = jQuery(this);
            
            var label = "name";
            var value = "name";
            
            field.val(ui.item[label]);
            
            var href = ui.item['link'];
            window.location.href = href;

            return false;
        },
        
        change: function(event, ui) {
            var field = jQuery(this);
           
            console.log(field.val());            
            return false;
        }		
	});	
}

jQuery(document).ready(function() {
	
	jQuery(".report-btn-cancel").on("click", function(){
		ReportMenu.cancel();
	});
	
	//ReportMenu.getMenuJson();
	
	/*jQuery(window).on('load', function(){
		jQuery(".report-categories li").on("click", function() {

			jQuery(".report-categories li")
				.removeClass("active")
				.children("i")
				.removeClass("fa-chevron-right")
				.addClass("fa-chevron-down");
			
			jQuery(this)
				.addClass("active")
				.children("i")
				.removeClass("fa-chevron-down")
				.addClass("fa-chevron-right");
			
			ReportMenu.getReportByCategory(jQuery(this).attr("id"));
		});
	});*/
	
	ReportMenu.buildParentMenu();
	
	jQuery(".report-categories li").on("click", function() {

		jQuery(".report-categories li")
			.removeClass("active")
			.children("i")
			.removeClass("fa-chevron-right")
			.addClass("fa-chevron-down");
		
		jQuery(this)
			.addClass("active")
			.children("i")
			.removeClass("fa-chevron-down")
			.addClass("fa-chevron-right");
		
		ReportMenu.getReportByCategory(jQuery(this).attr("id"), jQuery(this).text());
	});
	
	ReportMenu.defaultSelection();
	
	var hash = window.location.hash;
	if(hash.length > 1){
		hash = hash.substring(1);
		
		jQuery(".report-categories li").eq(hash).click();
	}
	
	ReportMenu.sortMenusAscendingOrder();
	
	ReportMenu.searchMenusAutocomplete();
	
});