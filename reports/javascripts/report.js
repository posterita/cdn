var Report = {};
Report.REPORT_CLASS_NAME = null;
Report.DEFAULT_VALUES = null;

Report.toggleMoreFilter = function() {
	jQuery("#more-filters-area").toggle();
	
	var json_select = jQuery("#more-filters-area select");
	var json_input = jQuery("#more-filters-area input");
	
	var json_inputs = jQuery.merge(json_select, json_input);
	
	for (var i=0; i<json_inputs.length; i++)
	{
		var id = "#" + json_inputs[i].id;
		
		var default_value = null;
		
		if (Report.DEFAULT_VALUES != null)
		{
			default_value = Report.DEFAULT_VALUES[id];
		}
		
		if (default_value != null)
		{
			jQuery(json_inputs[i]).val(default_value);
		}
		else
		{
			jQuery(json_inputs[i]).val("");
		}
	}
};

Report.getStoresAndTerminals = function() {
	
	var dfd = jQuery.Deferred();
	
	dfd.notify('requesting stores and terminals');
	
	jQuery.post("DataTableAction.do", { action: "getStoresAndTerminals"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					dfd.reject("Failed to request data!");
					return;
				}
				
				dfd.notify('rendering stores and terminals');
				
				var stores = json["stores"];
				Report.storesDB = TAFFY(stores);

				DataTable.utils.populateSelect(jQuery("#ad_org_id"), stores, "ad_org_name", "ad_org_id");
				
				dfd.resolve('Successfully rendered Store and Terminal dropdowns');
				
			},"json").fail(function(){
				dfd.reject("Failed to request data!");
			});
	
	return dfd.promise();
};

Report.getProductAutocomplete = function() {
	
	var dfd = jQuery.Deferred();
	
	dfd.notify('requesting product autocomplete data');
	
	jQuery.post("DataTableAction.do", { action: "getProductsAutocomplete"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					dfd.reject("Failed to request data!");
					return;
				}
				
				dfd.notify('rendering product autocomplete');
				
				var names = json["names"];
				var upcs = json["upcs"];
				var descriptions = json["descriptions"];
				
				
				
				jQuery("#product-name").autocomplete({
				    source: names,
				    select: function( event, ui ) {
				    	jQuery(this).val(ui.item.value)
				    	
				    	Report.ProductName = ui.item.value;
						Report.ProductId = ui.item.id;
						
						return false;
					}
				});
				
				jQuery("#barcode").autocomplete({
				    source: upcs
				});
				
				jQuery("#description").autocomplete({
				    source: descriptions
				});
				
				dfd.resolve('Successfully rendered Product autocompletes');
				
			},"json").fail(function(){
				dfd.reject("Failed to request data!");
			});
	
	return dfd.promise();
};

Report.afterRender = function() {
	var limit = 1000;
	
	var table = jQuery('#example').dataTable();
	var size = table.fnGetData().length;
	
	if (size == limit)
	{
		jQuery(".message-report").show();
		resetStarPosOfMainBody(10);
	} 
	else 
	{
		jQuery(".message-report").hide();
		resetStarPosOfMainBody(10);
	}
};

Report.getReportTable = function() {
	var discontinue = Report.checkParams();
	
	var params = Report.getParams();
	
	if (discontinue){
		return;
	}

	window.location.hash = jQuery.param(params);
	params.format = "html";
	
	Report.preloader.show();

	jQuery.ajax({
		type: "POST",
		url: Report.REPORT_CLASS_NAME,
		data: params
		}).done(function(data, textStatus, jqXHR){
			
		Report.preloader.hide();
		
		if(data == null || jqXHR.status != 200){
			alert('Failed to request!');			
		}
		
		if(jqXHR.status == 504)
		{
			alert('Request Failed! Please try again.');
		}
		
		var html = jqXHR.responseText;
		
		var table = document.getElementById("report-table-container");
		table.innerHTML = html;
		
		Report.renderReportTable();
		
		Report.afterRender();
		
	}).fail(function(resp) {
		Report.preloader.hide();
	});
	
};

Report.getReportCSV = function() {
	var params = Report.getParams();
	
	params.format = "csv";

	var serializedJSON = "?";
	
	for(var key in params)
	{
		var param = params[key];
		if(param == null) continue;
		
		serializedJSON = serializedJSON + key + "=" + params[key] + "&";
	}
	
	var size = serializedJSON.lastIndexOf('&');
	serializedJSON = Report.REPORT_CLASS_NAME + serializedJSON.substring(0, size);
	
	return serializedJSON;
};

Report.getParams = function () {
	alert("Function getParams() is missing!");
};

Report.checkParams = function () {	
	var fromDate = jQuery("#date-from");
	var toDate = jQuery("#date-to");
	var inventory_date = jQuery("#inventory_date");
	
	var row_selector = jQuery("#row");
	var column_selector = jQuery("#column");
	
	if (row_selector.size() > 0 && column_selector.size() > 0)
	{
		if ((row_selector.val().trim().length <= 0) || (column_selector.val().trim().length <= 0))
		{
			jQuery("#report-table-container").html("<div class='show-notification'><div class='report-notification-header'><p>&nbsp;</p></div>" +
					"<div class='report-notification-area'><p>" + 
					Translation.translate("error.no.criteria.html") +
					"</p></div></div>");

			return true;
		}
		
		if (row_selector.val() == column_selector.val())
		{
			jQuery("#report-table-container").html("<div class='show-notification'><div class='report-notification-header'><p>&nbsp;</p></div>" +
			"<div class='report-notification-area'><p>" +
			Translation.translate("error.no.criteria.html") +
			"</p></div></div>");

			return true;
		}
	}
	
	if (inventory_date.size() > 0)
	{
		if (inventory_date.val().trim().length <= 0)
		{
			jQuery("#report-table-container").html("<div class='show-notification'><div class='report-notification-header'><p>&nbsp;</p></div>" +
					"<div class='report-notification-area'><p>" +
					Translation.translate("error.no.criteria.html") +
					"</p></div></div>");

			return true;
		}
	}
	
	if (fromDate.size() > 0 && toDate.size() > 0)
	{
		if ((fromDate.val().trim().length <= 0) || (toDate.val().trim().length <= 0))
		{
			jQuery("#report-table-container").html("<div class='show-notification'><div class='report-notification-header'><p>&nbsp;</p></div>" +
					"<div class='report-notification-area'><p>" +
					Translation.translate("error.no.criteria.html") +
					"</p></div></div>");
			
			return true;
		}
	}
	
	return false;
};

Report.renderReportTable = function() {};

Report.update = function() {
	Report.getReportTable();
};

Report.getCSV = function() {
	var discontinue = Report.checkParams();
	
	if (discontinue)
	{
		alert(Translation.translate("error.no.criteria.csv"));
		return;
	}
	
	window.location.href = Report.getReportCSV();
};

Report.clearSelection = function() {
	
	jQuery(".view-report input").val("");
	jQuery(".view-report select").val("");
	
};

Report.getCurrentDate = function() {
	
	var date = new Date();
	
	var currentYear = date.getFullYear();
	var currentDay = ( date.getDate() < 10 ? "0" : "") + date.getDate();
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var currentDate = currentYear + "-" + currentMonth + "-" + currentDay + " 00:00:00";
	
	return currentDate;
};

jQuery(document).ready(function() {	

	Report.preloader = new Dialog(Translation.translate("loading")+" ...");
	
	Report.clearSelection();
	
	jQuery("#report-btn-more-filters").on("click", function(){
		Report.toggleMoreFilter();
	});
	
	jQuery("#report-btn-refresh").on("click", function(){
		Report.update();
	});
	
	jQuery("#report-btn-export-csv").on("click", function(){
		Report.getCSV();
	});
	
	Report.getStoresAndTerminals().done(function(){
				
		
		jQuery('#ad_org_id option').each(function(index){
			
			this.selected = false;
			
			if(index > 1) return;
			jQuery(this).remove();
		});		
		
		jQuery('.view-report').css({overflow:'visible'});
		jQuery("#more-filters-area label").css({display:'block'});
			
		
		
		jQuery('#ad_org_id').prop('multiple','multiple').SumoSelect({selectAll: true});
		if(jQuery('#ad_org_id')[0]) jQuery('#ad_org_id')[0].sumo.unSelectAll();
		

		
	});
	
	jQuery("#ad_org_id").on("change", function(){
		if(jQuery("#ad_org_id").val() != null && jQuery("#ad_org_id").val().length == 1){
			renderTerminalsDropdown(jQuery("#ad_org_id").val());
		}
		else
		{
			jQuery("#u_posterminal_id").html("");
		}
	});
	
});

function renderTerminalsDropdown(ad_org_id){
	var storesDB = Report.storesDB;
	
	var store = storesDB({"ad_org_id" :{'==':ad_org_id}}).first();	
	var terminals = store.terminals;
	
	DataTable.utils.populateSelect(jQuery("#u_posterminal_id"), terminals, "u_posterminal_name", "u_posterminal_id");
}

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function viewOrder(id) {
	Report.preloader.show();
	
	window.location = "OrderAction.do?action=view&orderId=" + id;
};

function viewInventory(id) {
	Report.preloader.show();
	
	window.location = "InventoryAction.do?action=view&inventoryId=" + id;
};

function viewTransfer(id) {
	Report.preloader.show();
	
	window.location = "StockTransferAction.do?action=view&movementId=" + id;
};

function viewPayment(id) {
	Report.preloader.show();
	
	window.location = "PaymentAction.do?action=view&print=true&paymentId=" + id;
};

function viewPurchase(id) {
	Report.preloader.show();
	
	window.location = "PurchaseAction.do?action=view&c_order_id=" + id;
};

function viewShipment(id) {
	Report.preloader.show();
	
	window.location = "PurchaseAction.do?action=viewShipment&c_order_id=" + id;
};

Report.displayError = function (error){
	var container = jQuery(".attendance-message-report");	
	container.html('<div class="backoffice-alert backoffice-alert-danger"><i class="fa fa-times"></i>&nbsp;'+ error +'<button type="button" class="close" onclick="">&times;</button></div>');
	jQuery(".attendance-message-report").show();
	
	resetStarPosOfMainBody(10);
	
	Report.closeAlert();
};

Report.displayMessage = function(success){
	var container = jQuery(".attendance-message-report");
	container.html('<div class="backoffice-alert backoffice-alert-success"><i class="fa fa-check"></i>&nbsp;'+ success +'<button type="button" class="close" onclick="">&times;</button></div>');
	jQuery(".attendance-message-report").show();
	
	resetStarPosOfMainBody(10);
	
	Report.closeAlert();
};

Report.clearMessage = function(){
	jQuery(".attendance-message-report").empty();
	
	resetStarPosOfMainBody(10);
};

Report.closeAlert = function(){
	jQuery(".attendance-message-report").on("click", function(e){
		Report.clearMessage();
	});
};

Report.afterAjaxCompletion = function(numberOfAjaxRequests){
	var numOfAjaxRequests = numberOfAjaxRequests;

	jQuery(document).ajaxComplete(function() {
	    numOfAjaxRequests--;
	    if(!numOfAjaxRequests) {
	    	Report.update();
	    }
	});
};
		