Report.REPORT_CLASS_NAME = "report/SalesActivityByHour";

Report.renderReportTable = function() {

	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=0; i<size; i++)
	{
		array[i-1] = i;
	}
	
	var table = jQuery('#example').DataTable({
    	"searching":   false,
        "ordering": false,
        "scrollX": true,
        "scrollCollapse": true,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
	    "columnDefs": [ 
	                   { className: "numberedCell", "targets": array },
                       { "width": "150px", "targets": 0 }
                      ],
	    "drawCallback": function ( settings ) {
	    	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            
            var trLength = jQuery(rows[0]).children('td').length;
            
            var totals = [];
            var counts = [];
            
        	for(var j=1; j<trLength; j++){
        		 totals[j-1] = 0;
        		 counts[j-1] = 0;
        		 
        		 for (var i=0; i<rows.length; i++){
        			 var trValue = jQuery(rows[i]).children('td').eq(j).text() + "";
        			 
        			 if (trValue == "-") continue;
        			 
        			 trValueOrder = trValue.substring(0, trValue.lastIndexOf("("));
        			 trValueOrder = trValue.replace(/,/g,'');
        			 
        			 trValueCount = trValue.substring(trValue.lastIndexOf("(")+1, trValue.lastIndexOf(")"));
        			 
        			 /*console.log(trValueCount);*/
        			 
        			 totals[j-1] = totals[j-1] + parseFloat(trValueOrder);
        			 counts[j-1] = counts[j-1] + parseFloat(trValueCount);
        		 }
        	}
        	
        	var tr = '<tr><td>' + Translation.translate("total") + ':</td>';
        	
        	for(var k=0; k<totals.length; k++){
        		tr = tr + '<td class="numberedCell">' + parseFloat(totals[k]).format(2) + '(' + counts[k] + ')</td>';
        	}
        		
        	tr = tr + '</tr>';
        	
        	jQuery(rows).eq(rows.length - 1).after(
        			tr
            );
            
	        }
    });
    
    new jQuery.fn.dataTable.FixedColumns( table );
};