Report.REPORT_CLASS_NAME = "report/SalesReturn";


Report.renderReportTable = function() {
    var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
            "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
        },
        "columnDefs": [{
                className: "numberedCell",
                "targets": [2, 3]
            },
            {
                "width": "150px",
                "targets": 0
            },
            {
                "render": function(data, type, row) {
                    return parseInt(data).format(0);
                },
                "targets": 2
            },
            {
                "render": function(data, type, row) {
                    return parseFloat(data).format(2);
                },
                "targets": 3
            },
            {
                "render": function(data, type, row) {
                    var id = row[14];
                    return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
                },
                "targets": 1
            },

            {
                "render": function(data, type, row) {
                    var id = row[15];

                    if (id == 0) {
                        return "<a href='javascript:void(0);'" + data + "</a>";
                    }

                    return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
                },
                "targets": 12
            },
			
            {
                "visible": false,
                "targets": [14, 15]
            }
			

        ]
    });
};

//refactored