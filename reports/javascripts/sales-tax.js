Report.REPORT_CLASS_NAME = "report/SalesTax";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    }
	   });
};