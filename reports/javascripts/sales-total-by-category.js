Report.REPORT_CLASS_NAME = "report/SalesByCategory";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	         			"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       			},
        "columnDefs": [ 
                       { className: "numberedCell", "targets": [1,2,3,4,5,6,7] }
           		      ],
        "drawCallback": function (settings) {}
    });    
    
};

jQuery(function(){
	Report.renderProductAutocomplete();
});

