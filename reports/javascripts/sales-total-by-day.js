Report.REPORT_CLASS_NAME = "report/SalesTotalByDay";

Report.renderReportTable = function() {
	
	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=0; i<size; i++)
	{
		array[i-1] = i;
	}
	
    var table = jQuery('#example').DataTable({
    	"paging":   false,
    	"searching":   false,
        "ordering": false,
        "info":     false,
        "scrollX": true,
        "scrollCollapse": true,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
	    "columnDefs": [ 
                       { className: "numberedCell", "targets": array },
                       { "width": "150px", "targets": 0 }
                      ]
    });
    
    new jQuery.fn.dataTable.FixedColumns( table );
};