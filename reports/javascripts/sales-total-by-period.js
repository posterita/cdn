Report.REPORT_CLASS_NAME = "report/SalesTotalByPeriod";

Report.renderReportTable = function() {
	
	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=0; i<size; i++)
	{
		array[i-1] = i;
	}
	
    var table = jQuery('#example').DataTable({
    	"paging":   false,
    	"searching":   false,
        "ordering": false,
        "info":     false,
        "scrollX": true,
        "scrollCollapse": true,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
	    "columnDefs": [ 
                       { className: "numberedCell", "targets": array },
                       { "width": "150px", "targets": 0 }
                      ]
    });
    
    new jQuery.fn.dataTable.FixedColumns( table );
};

/*Report.getParams = function () {
	
	var params = {};
	
	var fromDate = jQuery("#date-from").val();
	var toDate = jQuery("#date-to").val();
	var period = jQuery("#periods").val();

	var ad_org_id = jQuery("#ad_org_id").val();
	var u_posterminal_id = jQuery("#u_posterminal_id").val();
	
	params.fromDate = (fromDate.trim() ? moment(fromDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	params.toDate = (toDate.trim() ? moment(toDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	params.period = period;
			
	params.ad_org_id = ad_org_id;
	params.u_posterminal_id = u_posterminal_id;
	
	return params;
};

jQuery(document).ready(function() {
	jQuery( "#date-from" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
	
	jQuery( "#date-to" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
});*/