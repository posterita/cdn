Report.REPORT_CLASS_NAME = "report/Shipments";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
        	{ 	
        		"visible": false, "targets": [17,18] 
        	},
        	
        	{
		        "render": function (data, type, row) {
		          var id = row[17];
		          return "<a href='javascript:void(0);' onclick='viewPurchase(" + id + ")'>" + data + "</a>";
		        },
		        "targets": 1
 			},
 			
 			{
		        "render": function (data, type, row) {
		          var id = row[18];
		          return "<a href='javascript:void(0);' onclick='viewShipment(" + id + ")'>" + data + "</a>";
		        },
		        "targets": 2
 			}
 			
 			/*,
 			
 			{
 		    	  "render": function (data, type, row) {
 			        	 var value = row[16];
 			          
 						 if (value == "Y")
 						 {
 							  value = "Received";
 						 }
 						 else
 						 {
 							 value = "Not Received";
 						 }
 						 
 						 return value;
 			        },
 			     "targets": 16
 		      }*/
 			
 			
                       ],
         "order": [[0, "desc"]],
         "autoWidth": false,
         "language": {
	         "emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	       }
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	date.setTime(date.getTime() - (7*24*60*60*1000));
	
	var currentYear = date.getFullYear();
	var firstDay = ( date.getDate() < 10 ? "0" : "") + date.getDate();
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};

/* set report defaults */
Report.defaults = {
		"date-from" : moment(Report.getStartingDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY")
		
};

/* update report on load */
Report.updateOnReady = true;

Report.onFormReady(function() {
	
	jQuery("#btn-export-pdf").on("click",function(){
		
		 var params = jQuery("#report-form").serialize();
				
		/*jQuery.ajax({
			type: "POST",
			url: "DataTableAction.do?action=exportShipmentPDF",
			data: params
			}).done(function(data, textStatus, jqXHR){
										
			if(data == null || jqXHR.status != 200){
				alert('Failed to request!');			
			}
			
			if(jqXHR.status == 504)
			{
				alert('Request Failed! Please try again.');
			}
						
			
		}).fail(function(resp) {
			
			
		});*/
		
		window.location = "ShipmentPDFAction.do?action=exportShipmentPDF&" + params ;
		
	});
	
	
});

