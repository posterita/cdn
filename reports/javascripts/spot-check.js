Report.REPORT_CLASS_NAME = "report/SpotCheck";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "order": [
                  [1, "desc"]
                 ],
        "columnDefs": [
                   		{ "visible": false, "targets": [7,8] },
                   		{
         			        "render": function (data, type, row) {
         			          var id = row[7];
         			          return "<a href='javascript:void(0);' onclick='viewSpotCheck(" + id + ")'>" + data + "</a>";
         			        },
         			        "targets": 1
	         			 },
	         			 
	         			{
	         			        "render": function (data, type, row) {
	         			        	
	         			          if(data.length == 0) return data;
	         			          
	         			          var id = row[8];
	         			          return "<a href='javascript:void(0);' onclick='viewInventory(" + id + ")'>" + data + "</a>";
	         			        },
	         			        "targets": 6
		         			 }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
  	    "drawCallback": function ( settings ) {}
	});
};
