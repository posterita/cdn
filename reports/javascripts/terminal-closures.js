Report.REPORT_CLASS_NAME = "report/TerminalClosures";

Report.renderReportTable = function() {
	
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
        "columnDefs": [ 
			             { 
							className: "numberedCell", 
							"targets": [5,6,7,8,9,10,11,12,13],
							"render": function (data, type, row) {
								return Report.formatNumber(data);
			                }
							
						 },
			             {
			     	        "render": function (data, type, row) {
			     	          var id = row[2];
			     	          return "<a href='javascript:void(0);' onclick='Report.view(" + id + ")'>" + data + "</a>";
			     	        },
			     	        "targets": 2
			     	      }
		      		  ],
        "order": [
                  [3, "desc"]
                 ]
    });
};

Report.view = function(id){
	window.location = "TillAction.do?action=viewCloseTill&print=false&cashJournalId=" + id;
};

/*

Report.getParams = function () {
	
	var params = {};
	
	var fromDate = jQuery("#date-from").val();
	var toDate = jQuery("#date-to").val();
	
	var ad_org_id = jQuery("#ad_org_id").val();
	var u_posterminal_id = jQuery("#u_posterminal_id").val();
	
	params.fromDate = (fromDate.trim() ? moment(fromDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	params.toDate = (toDate.trim() ? moment(toDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	
	params.ad_org_id = ad_org_id;
	params.u_posterminal_id = u_posterminal_id;
		
	return params;
};

jQuery(document).ready(function() {
	jQuery( "#date-from" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
	
	jQuery( "#date-to" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
});*/