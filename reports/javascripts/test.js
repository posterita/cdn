var Report = {
	updateOnReady : false,
    defaults: {},
    callbacks: jQuery.Callbacks(),
    onFormReady: function(fn) {
        this.callbacks.add(fn);
    }
};

Report.getParams = function() {
    var params = jQuery("#report-form").serialize();
    return params;
};


Report.renderStoreList = function() {
	
	var selector = "[data-report-field='store']";
	var url = "DataTableAction.do?action=getStoresAndTerminalsAndWarehouses";
	
	/* set select type to multiselect */
	var select = jQuery(selector);
	
	if( select.length == 1 ) {
		
		select.prop('multiple','multiple');
		
	}
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var stores = json["stores"];
        Report.storesDB = TAFFY(stores);

        DataTable.utils.populateSelect(dropdown, stores, "ad_org_name", "ad_org_id");

        dropdown.on("change", function() {        	
        	
        	var value = jQuery(this).val();
        	
        	/* multiselects return an array of values */
        	
        	if( jQuery.isArray(value) ) {
        		
        		var ad_org_id = -1;
                
                /* only render terminal & warehouse if only one store is selected */
                
                if ( value != null && value.length == 1 ) {
                	
                	ad_org_id = value[0];
                }
        	}
        	else
        	{
        		ad_org_id = value;
        		
        	}            
            
            Report.renderTerminalList(ad_org_id);
            Report.renderWarehouseList(ad_org_id);
            
            /* multiple selects */
            Report.renderTerminalMultipleList(value);
            Report.renderWarehouseMultipleList(value);
        });
        
	});
	
};

Report.renderTerminalList = function(ad_org_id) {
	
	var dropdown = jQuery("[data-report-field='terminal']");
	if(dropdown.length == 0) return;
	
    var storesDB = Report.storesDB;

    var store = storesDB({
        "ad_org_id": {
            '==': ad_org_id
        }
    }).first();
    var terminals = store.terminals;
    
    DataTable.utils.populateSelect(dropdown, terminals, "u_posterminal_name", "u_posterminal_id");

    //set value
    dropdown.each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });
};

Report.renderWarehouseList = function(ad_org_id) {
	
	var dropdown = jQuery("[data-report-field='warehouse']");
	if(dropdown.length == 0) return;
	
    var storesDB = Report.storesDB;

    var store = storesDB({
        "ad_org_id": {
            '==': ad_org_id
        }
    }).first();
    var warehouses = store.warehouses;

    DataTable.utils.populateSelect(dropdown, warehouses, "m_warehouse_name", "m_warehouse_id");

    //set value
    dropdown.each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });
};

Report.renderWarehouseMultipleList = function(ad_org_ids) {
	
	var dropdown = jQuery("[data-report-field='warehouse-multiple']");
	if(dropdown.length == 0) return;
	
    var storesDB = Report.storesDB;
    
    var select = dropdown;
	select.html("");	
	
	if(ad_org_ids != null)
	{
		for(var j=0; j < ad_org_ids.length; j++)
		{
			var ad_org_id = ad_org_ids[j];
			
			var store = storesDB({
		        "ad_org_id": {
		            '==': ad_org_id
		        }
		    }).first();
		    
		    var warehouses = store.warehouses; 
		    
		    var optgroup = jQuery("<optgroup/>", { label: store.ad_org_name });
			
			if (warehouses != null)
			{		
				for(var i=0; i<warehouses.length; i++)
				{
					var json = warehouses[i];
					optgroup.append(jQuery("<option/>",{value:json["m_warehouse_id"], html:json["m_warehouse_name"]}));
				}
			}
			
			select.append(optgroup);
		}
	}	

    //set value
    dropdown.each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
        
        if(this.sumo){
        	this.sumo.reload();
        }
    });
    
    
};

Report.renderTerminalMultipleList = function(ad_org_ids) {
	
	var dropdown = jQuery("[data-report-field='terminal-multiple']");
	if(dropdown.length == 0) return;
	
    var storesDB = Report.storesDB;
    
    var select = dropdown;
	select.html("");	
	
	if(ad_org_ids != null)
	{
		for(var j=0; j < ad_org_ids.length; j++)
		{
			var ad_org_id = ad_org_ids[j];
			
			var store = storesDB({
		        "ad_org_id": {
		            '==': ad_org_id
		        }
		    }).first();
		    
			var terminals = store.terminals; 
		    
		    var optgroup = jQuery("<optgroup/>", { label: store.ad_org_name });
			
			if (terminals != null)
			{		
				for(var i=0; i<terminals.length; i++)
				{
					var json = terminals[i];
					optgroup.append(jQuery("<option/>",{value:json["u_posterminal_id"], html:json["u_posterminal_name"]}));
				}
			}
			
			select.append(optgroup);
		}
	}	

    //set value
    dropdown.each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
        
        if(this.sumo){
        	this.sumo.reload();
        }
    });
    
    
};


Report.renderStoreToList = function() {
	
	var selector = "[data-report-field='store-to']";
	var url = "DataTableAction.do?action=getStoresAndTerminalsAndWarehouses";
	
	/* set select type to multiselect */
	var select = jQuery(selector);
	
	if( select.length == 1 ) {
		
		select.prop('multiple','multiple');
		
	}
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var stores = json["stores"];
        Report.storesDB = TAFFY(stores);

        DataTable.utils.populateSelect(dropdown, stores, "ad_org_name", "ad_org_id");

        dropdown.on("change", function() {        	
        	
        	var value = jQuery(this).val();
        	
        	/* multiselects return an array of values */
        	
        	if( jQuery.isArray(value) ) {
        		
        		var ad_org_id = -1;
                
                /* only render terminal & warehouse if only one store is selected */
                
                if ( value != null && value.length == 1 ) {
                	
                	ad_org_id = value[0];
                }
        	}
        	else
        	{
        		ad_org_id = value;
        		
        	}
        	
            
            
            //Report.renderTerminalList(ad_org_id);
            Report.renderWarehouseToList(ad_org_id);
        });
        
	});
	
};

Report.renderWarehouseToList = function(ad_org_id) {
    var storesDB = Report.storesDB;

    var store = storesDB({
        "ad_org_id": {
            '==': ad_org_id
        }
    }).first();
    var warehouses = store.warehouses;

    var dropdown = jQuery("[data-report-field='warehouse-to']");
    DataTable.utils.populateSelect(dropdown, warehouses, "m_warehouse_name", "m_warehouse_id");

    //set value
    dropdown.each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });
};

Report.renderStoreFromList = function() {
	
	var selector = "[data-report-field='store-from']";
	var url = "DataTableAction.do?action=getStoresAndTerminalsAndWarehouses";
	
	/* set select type to multiselect */
	var select = jQuery(selector);
	
	if( select.length == 1 ) {
		
		select.prop('multiple','multiple');
		
	}
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var stores = json["stores"];
        Report.storesDB = TAFFY(stores);

        DataTable.utils.populateSelect(dropdown, stores, "ad_org_name", "ad_org_id");

        dropdown.on("change", function() {        	
        	
        	var value = jQuery(this).val();
        	
        	/* multiselects return an array of values */
        	
        	if( jQuery.isArray(value) ) {
        		
        		var ad_org_id = -1;
                
                /* only render terminal & warehouse if only one store is selected */
                
                if ( value != null && value.length == 1 ) {
                	
                	ad_org_id = value[0];
                }
        	}
        	else
        	{
        		ad_org_id = value;
        		
        	}
            
            //Report.renderTerminalList(ad_org_id);
            Report.renderWarehouseFromList(ad_org_id);
        });
        
	});
	
};

Report.renderWarehouseFromList = function(ad_org_id) {
    var storesDB = Report.storesDB;

    var store = storesDB({
        "ad_org_id": {
            '==': ad_org_id
        }
    }).first();
    var warehouses = store.warehouses;

    var dropdown = jQuery("[data-report-field='warehouse-from']");
    DataTable.utils.populateSelect(dropdown, warehouses, "m_warehouse_name", "m_warehouse_id");

    //set value
    dropdown.each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });
};


Report.renderSalesRepList = function() {
	
	var selector = "[data-report-field='sales-rep']";
	var url = "DataTableAction.do?action=getUsersAutocomplete";
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var users = json["users"];

		dropdown.append(jQuery("<option>"));
		
        for (var i = 0; i < users.length; i++) {
            var option = jQuery("<option>");
            option.html(users[i].name);
            option.val(users[i].id);

            dropdown.append(option);
        }	
	});
	
};

Report.renderDiscountCodeList = function() {
	
	var selector = "[data-report-field='discount-code']";
	var url = "DataTableAction.do?action=getDiscountCodeAutocomplete";
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var discountCode = json["discountCode"];

		dropdown.append(jQuery("<option>"));
		
        for (var i = 0; i < discountCode.length; i++) {
            var option = jQuery("<option>");
            option.html(discountCode[i].name);
            option.val(discountCode[i].id);

            dropdown.append(option);
        }	
	});
	
};

Report.renderCategoryNamingList = function() {
	
	var selector = "[data-report-field='category-naming']";
	var url = "DataTableAction.do?action=getCategoryNamingList";
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var groups = json["groups"];

		dropdown.append(jQuery("<option>"));
		
        for (var i = 0; i < groups.length; i++) {
            var option = jQuery("<option>");
            option.html(groups[i].name);
            option.val(groups[i].id);

            dropdown.append(option);
        }	
	});
	
};

Report.renderDateSelector = function() {

    var dfd = jQuery.Deferred();
    
    var field = "[data-report-field='date']";

    jQuery(field).datepicker({
        format: "mm/dd/yyyy",
        orientation: "top left",
        endDate: '+0d',
        autoclose: true
    }).on('hide', function(e) {
        e.preventDefault();
    });

    //set value
    jQuery(field).each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
        
        //disable autocomplete
        jQuery(this).attr("autocomplete","off");
    });

    dfd.resolve();

    return dfd.promise();
};

Report.renderYear = function() {

    var dfd = jQuery.Deferred();
    
    var field = "[data-report-field='year']";

    var options= "";
	
	var startYear = new Date().getFullYear();
	
	for(var i=0; i<5; i++){
		var year = startYear - i;
		var option = "<option value='" + year + "'>" + year + "</option>";
		
		options = options + option;
	};
	
	jQuery(field).html(options);

    //set value
    jQuery(field).each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });

    dfd.resolve();

    return dfd.promise();
};

Report.renderText = function() {

    var dfd = jQuery.Deferred();

    var field = "[data-report-field='text']";
    
    //set value
    jQuery(field).each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });

    dfd.resolve();

    return dfd.promise();
};

/* Populate hidden parameter with client date and time */
Report.renderRemoteDate = function(){
	
	var dfd = jQuery.Deferred();

    var field = "[data-report-field='remote-date']";
    
    var currentDate = moment().format('DD-MM-YYYY HH:mm:ss');
    //set value
    jQuery(field).val( currentDate );

    dfd.resolve();

    return dfd.promise();
	
};

Report.renderSelect = function() {

    var dfd = jQuery.Deferred();

    var field = "[data-report-field='select']";
    
    //set value
    jQuery(field).each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });

    dfd.resolve();

    return dfd.promise();
};

Report.renderRadio = function() {

    var dfd = jQuery.Deferred();

    var field = "[data-report-field='radio']";
    
    //set value
    jQuery(field).each(function() {
    	
        var name = jQuery(this).attr("name");
        
        if (Report.defaults[name]) {
        	
        	// radios are quite different from other elements
        	var value = Report.defaults[name];
        	
        	// get all radios with same name
        	var radios = jQuery("input[name=" + name + "]:radio");
        	
        	radios.each(function(index, element){
        		
        		var radio = jQuery(element);
        		
        		if(radio.attr("value") == value){
        			
        			radio.prop( "checked", true );     			
        		} 
        		else
        		{
        			radio.prop( "checked", false );     
        		}
        		
        	});
        	
            //clear default
            Report.defaults[name] = null;
            
            console.log("Setting --> " + name);
        }
    });

    dfd.resolve();

    return dfd.promise();
};

Report.renderCheckbox = function() {

    var dfd = jQuery.Deferred();
    
    var field = "[data-report-field='checkbox']";

    //set value
    jQuery(field).each(function() {
    	
    	var element = jQuery(this);
    	
        var name = element.attr("name");       
        
        if (Report.defaults[name]) {
        	
        	var value = Report.defaults[name];
        	
        	if( element.attr("value") == value ) {
        		
        		element.prop( "checked", true );       		
        	}
        	else
        	{
        		element.prop( "checked", false );  
        	}
        	
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });

    dfd.resolve();

    return dfd.promise();
};

Report.renderTextarea = function() {

    var dfd = jQuery.Deferred();
    
    var field = "[data-report-field='textarea']";

    //set value
    jQuery(field).each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }
    });

    dfd.resolve();

    return dfd.promise();
};


Report.renderSalesPriceList = function() {
	
	var selector = "[data-report-field='sales-pricelist']";
	var url = "DataTableAction.do?action=getSalesPriceList";
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var pricelists = json["salesPrice"];
        var selected = json["selected"];

        dropdown.append(jQuery("<option>"));
        
        for (var i = 0; i < pricelists.length; i++) {
            var option = jQuery("<option>");
            option.html(pricelists[i].name);
            option.val(pricelists[i].value);

            dropdown.append(option);
        }
        
        dropdown.val(selected);		
	});
	
};

Report.renderPurchasePriceList = function() {
	
	var selector = "[data-report-field='purchase-pricelist']";
	var url = "DataTableAction.do?action=getPurchasePriceList";
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var pricelists = json["purchasePrice"];
        var selected = json["selected"];

        dropdown.append(jQuery("<option>"));
        
        for (var i = 0; i < pricelists.length; i++) {
            var option = jQuery("<option>");
            option.html(pricelists[i].name);
            option.val(pricelists[i].value);

            dropdown.append(option);
        }
        
        dropdown.val(selected);		
	});
	
};

Report.renderAllPriceList = function() {
	
	var selector = "[data-report-field='all-pricelist']";
	var url = "DataTableAction.do?action=getAllPriceList";
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var pricelists = json["allPrice"];
        var selected = json["selected"];

        dropdown.append(jQuery("<option>"));
        
        for (var i = 0; i < pricelists.length; i++) {
            var option = jQuery("<option>");
            option.html(pricelists[i].name);
            option.val(pricelists[i].value);

            dropdown.append(option);
        }
        
        dropdown.val(selected);		
	});
	
};

Report.renderPaymentRuleList = function() {
	
	var selector = "[data-report-field='payment-rule']";
	var url = "DataTableAction.do?action=getPaymentRules";
	
	return this.DROPDOWN( selector, url);
};

Report.renderDocumentStatus = function() {
	
	var selector = "[data-report-field='document-status']";
	var url = "DataTableAction.do?action=getDocumentStatus";
	
	return this.DROPDOWN( selector, url);
};

Report.getCreditCardTypes = function() {
	
	var selector = "[data-report-field='credit-card']";
	var url = "DataTableAction.do?action=getCreditCardTypes";
	
	return this.DROPDOWN( selector, url);
};

Report.getTenderTypes = function() {

	var selector = "[data-report-field='tender-type']";
	var url = "DataTableAction.do?action=getTenderTypes";
	
	return this.DROPDOWN( selector, url);
};

Report.getWarehouses = function() {

	var selector = "[data-report-field='warehouse-to-from']";
	var url = "DataTableAction.do?action=getWarehousesList";
	
	return this.DROPDOWN( selector, url);
};

Report.getCurrencies = function() {

	var selector = "[data-report-field='currency']";
	var url = "DataTableAction.do?action=getCurrencies";
	
	return this.DROPDOWN( selector, url, function( dropdown, json ){
		
		var currencies = json;
		var selected = currencies[0].id;
		
		jQuery("#currency_rate").val(currencies[0].rate);

        for (var i = 0; i < currencies.length; i++) {
            var option = jQuery("<option>");
            option.html(currencies[i].description);
            option.val(currencies[i].id);
            option.attr("rate", currencies[i].rate);

            dropdown.append(option);
        }
        
        dropdown.val(selected);	
        
        dropdown.on('change', function(){ 
        	var select = jQuery(this)[0];        	
        	var option = select.options[select.selectedIndex];
        	
        	jQuery("#currency_rate").val(jQuery(option).attr("rate"));
        });
	});
};

Report.toggleMoreFilter = function() {
    jQuery("#more-filters-area").toggle();
};

Report.validate = function() {
    var mandatoryFields = jQuery('[data-report-field-mandatory]');
    
    var valid = true;

    for (var i = 0; i < mandatoryFields.length; i++) {
        var mandatoryField = mandatoryFields[i];
        var value = jQuery(mandatoryField).val();

        if (value == null || value == ''){
        	valid = false;
        	
        	//highlight field
        	jQuery(mandatoryField).addClass("report-field-error-cue");
        }
        else
        {
        	jQuery(mandatoryField).removeClass("report-field-error-cue");
        }
    }

    return valid;

};

Report.isValid = function() {
    var mandatoryFields = jQuery('[data-report-field-mandatory]');
    
    var valid = true;

    for (var i = 0; i < mandatoryFields.length; i++) {
        var mandatoryField = mandatoryFields[i];
        var value = jQuery(mandatoryField).val();

        if (value == null || value == ''){
        	return false;
        }
    }

    return valid;

};


Report.initializeForm = function() {
	
    var fields = jQuery('[data-report-field]');
    
    var set = {}; // hash set to store unique types

    var array = [];

    for (var i = 0; i < fields.length; i++) {
    	
        var field = fields[i];
        
        var type = jQuery(field).attr('data-report-field');        
        
        if ( ! set.hasOwnProperty(type) ) {
        	
        	set[type] = true;
        	
        }
    }
    
    var types = Object.keys(set);
    
    for( var i = 0; i < types.length; i++ ) {
    	
    	var type = types[i];
    	
    	switch (type) {
    	
	        case 'store':
	            var d = Report.renderStoreList();
	            array.push(d);
	            break;
	            
	        case 'customer':
	            var d = Report.renderCustomerAutocomplete();
	            array.push(d);
	            break;
	            
	        case 'date':
	            var d = Report.renderDateSelector();
	            array.push(d);
	            break;
	            
	        case 'sales-rep':
	            var d = Report.renderSalesRepList();
	            array.push(d);
	            break;
	            
	        case 'payment-rule':
	            var d = Report.renderPaymentRuleList();
	            array.push(d);
	            break;

	        case 'document-status':
                var d = Report.renderDocumentStatus();
                array.push(d);
               	break;
	            
	        case 'product':
	            var d = Report.renderProductAutocomplete();
	            array.push(d);
	            break;
	            
	        case 'sales-pricelist':
	            var d = Report.renderSalesPriceList();
	            array.push(d);
	            break;
	        case 'purchase-pricelist':
	            var d = Report.renderPurchasePriceList();
	            array.push(d);
	            break;
	           
	        case 'all-pricelist':
	            var d = Report.renderAllPriceList();
	            array.push(d);
	            break; 
	            
	        case 'text':
	            var d = Report.renderText();
	            array.push(d);
	            break;
	            
	        case 'select':
	            var d = Report.renderSelect();
	            array.push(d);
	            break;
	            
	        case 'radio':
	            var d = Report.renderRadio();
	            array.push(d);
	            break;
	            
	        case 'checkbox':
	            var d = Report.renderCheckbox();
	            array.push(d);
	            break;
	            
	        case 'textarea':
	            var d = Report.renderTextarea();
	            array.push(d);
	            break;
	            
	        case 'vendor':
	            var d = Report.renderVendorAutocomplete();
	            array.push(d);
	            break;
	            
	        case 'credit-card':
                var d = Report.getCreditCardTypes();
                array.push(d);
                break;
                
	        case 'tender-type':
                var d = Report.getTenderTypes();
                array.push(d);
                break;
                
	        case 'warehouse-to-from':
                var d = Report.getWarehouses();
                array.push(d);
                break;
                
	        case 'store-to':
                var d = Report.renderStoreToList();
                array.push(d);
                break;
                
	        case 'store-from':
                var d = Report.renderStoreFromList();
                array.push(d);
                break;
            
	        case 'year':
                var d = Report.renderYear();
                array.push(d);
                break;
                
	        case 'coupon':
                var d = Report.renderCouponAutocomplete();
                array.push(d);
                break;
                
	        case 'remote-date':
	            var d = Report.renderRemoteDate();
	            array.push(d);
	            break;
                
	        case 'discount-code':
	            var d = Report.renderDiscountCodeList();
	            array.push(d);
	            break;
	            
	        case 'currency':
	        	var d = Report.getCurrencies();
	        	array.push(d);
	            break;
	            
	        case 'doctor':
	            var d = Report.renderDoctorAutocomplete();
	            array.push(d);
	            break;
	            
	        case 'category-naming':
	        	var d = Report.renderCategoryNamingList();
	        	array.push(d);
	            break;
    	}
    	
    } 
    
    jQuery.when.apply(jQuery,array).done(function() {        
        for (var i = 0, j = arguments.length; i < j; i++) {
        	if(arguments[i]) console.log(arguments[i]);
        }
        
        Report.callbacks.fire();
    });
};

Report.getReportTable = function() {
	
    var params = Report.getParams();
    
    window.location.hash = params;
    
    params = params + "&format=html";
	
	Report.preloader.show();

	jQuery.ajax({
		type: "POST",
		url: Report.REPORT_CLASS_NAME,
		data: params
		}).done(function(data, textStatus, jqXHR){
			
		Report.preloader.hide();
		
		if(data == null || jqXHR.status != 200){
			alert('Failed to request!');			
		}
		
		if(jqXHR.status == 504)
		{
			alert('Request Failed! Please try again.');
		}
		
		var html = jqXHR.responseText;
		
		var table = document.getElementById("report-table-container");
		table.innerHTML = html;
		
		Report.renderReportTable();
		
		Report.afterRender();
		
	}).fail(function(resp) {
		Report.preloader.hide();
		
	}).always(function(){
		Report.preloader.hide();
	});
};

Report.setDefaults = function() {
    //parse hash for history
    if (window.location.hash.length > 0) {
        var hash = window.location.hash;
        hash = hash.substr(1);
        var splits = hash.split("&");
        var name, val;

        var data = {};

        for (var i = 0; i < splits.length; i++) {
            var split = splits[i].split("=");

            name = decodeURIComponent(split[0].replace(/\+/g, "%20"));
            val = decodeURIComponent(split[1].replace(/\+/g, "%20"));

            if (!data[name]) {
                data[name] = val;
            } else {
                if (!jQuery.isArray(data[name])) {
                    data[name] = [data[name]];
                }

                data[name].push(val);
            }
        }

        Report.defaults = jQuery.extend( Report.defaults, data );
        
        Report.updateOnReady = true; /* load from history */
    }

    //determining whether to show filters
    var showMoreFilters = false;

    var keys = Object.keys(Report.defaults);
    var area = jQuery("#more-filters-area");

    if( area.length > 0 )
    for ( var i = 0; i < keys.length; i++ ) {
        var key = keys[i];
        var element = jQuery("#" + key);
        var value = Report.defaults[key];

        if (jQuery.contains(area[0], element[0])) {
            if (value != "") {
                showMoreFilters = true;
                break;
            }
        }

    }

    if (showMoreFilters) {
        jQuery("#more-filters-area").show();
        console.log("Show more ..");
    }    
    
};

Report.update = function() {
	Report.getReportTable();
};

Report.getReportCSV = function() {
	
	var params = Report.getParams();    
	params = params + "&format=csv";
    
	window.location.href = Report.REPORT_CLASS_NAME + "?" + params;
};

Report.getReportExcel = function() {
	
	var params = Report.getParams();    
	params = params + "&format=xls";
    
	window.location.href = Report.REPORT_CLASS_NAME + "?" + params;
};

Report.afterRender = function() {
	var limit = 500;
	
	var table = jQuery('#example').dataTable();
	var size = table.fnGetData().length;
	
	if (size == limit)
	{
		jQuery(".message-report").show();
		resetStarPosOfMainBody(10);
	} 
	else 
	{
		jQuery(".message-report").hide();
		resetStarPosOfMainBody(10);
	}
};

jQuery(document).ready(function() {
	
	Report.preloader = new Dialog(Translation.translate("loading")+" ...");
	
    Report.setDefaults();
    Report.initializeForm();
    
    jQuery("#report-btn-more-filters").on("click", function(){
		Report.toggleMoreFilter();
	});
		
	jQuery("#report-btn-export-csv").on("click", function(){
		
		var isFormValid = Report.validate();

        if (isFormValid) {
            Report.getReportCSV();
        } 
        else
        {
        	alert(Translation.translate("error.no.criteria.csv"));
        }
	});
	
	jQuery("#report-btn-export-excel").on("click", function(){
		
		var isFormValid = Report.validate();

        if (isFormValid) {
            Report.getReportExcel();
        } 
        else
        {
        	alert(Translation.translate("error.no.criteria.csv"));
        }
	});
	
	jQuery("#report-btn-refresh").on("click", function() {
        var isFormValid = Report.validate();

        if (isFormValid) {
            Report.getReportTable();
        } 
        else
        {
        	Report.clearMessage();
        	
        	jQuery("#report-table-container").html("<div class='show-notification'><div class='report-notification-header'><p>&nbsp;</p></div>" +
        			"<div class='report-notification-area'><p>" +
        			Translation.translate("error.no.criteria.html") +
        			"</p></div></div>");
        }
    });
});


/* Autocompletes*/
Report.renderProductAutocomplete = function() {
	
	var dfd = jQuery.Deferred();
	
	dfd.notify('rendering product autocomplete');
	
	var selector = "[data-report-field='product']";
	var source = "DataTableAction.do?action=search&field=product";
	var labelKey = "name";
	var valueKey = "m_product_id";
	    
	Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);	
	
	var array = [
			"description", 
			"upc", 
			"primarygroup",
			"group1",
			"group2",
			"group3",
			"group4",
			"group5",
			"group6",
			"group7",
			"group8"
		];
	
	jQuery(array).each(function(index, value){
		
		var selector = "[data-report-field='product-" + value + "']";
		var source = "DataTableAction.do?action=search&field=product&column=" + value;
		var labelKey = value;
		var valueKey = value;
		
		dfd.notify('rendering product ' + value + ' autocomplete');
		
		Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);		
		
	});
	
	dfd.resolve("Successfully rendered product autocomplete");
	
	return dfd.promise();
}; 

Report.renderCustomerAutocomplete = function() {
	
	var dfd = jQuery.Deferred();

	dfd.notify('rendering customer autocomplete');
	
	var selector = "[data-report-field='customer']";
	var source = "DataTableAction.do?action=search&field=customer";
	var labelKey = "name";
	var valueKey = "c_bpartner_id";
	    
	Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);	
	
	dfd.resolve("Successfully rendered customer autocomplete");
	
	return dfd.promise();
};

Report.renderVendorAutocomplete = function() {
	
	var dfd = jQuery.Deferred();

	dfd.notify('rendering vendor autocomplete');
	
	var selector = "[data-report-field='vendor']";
	var source = "DataTableAction.do?action=search&field=vendor";
	var labelKey = "name";
	var valueKey = "c_bpartner_id";
	    
	Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);	
	
	dfd.resolve("Successfully rendered vendor autocomplete");
	
	return dfd.promise();
};

Report.renderCouponAutocomplete = function() {
	
	var dfd = jQuery.Deferred();

	dfd.notify('rendering coupon autocomplete');
	
	var selector = "[data-report-field='coupon']";
	var source = "DataTableAction.do?action=search&field=coupon&column=couponno";
	var labelKey = "couponno";
	var valueKey = "u_coupon_id";
	    
	Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);	
	
	dfd.resolve("Successfully rendered coupon autocomplete");
	
	return dfd.promise();
};

Report.renderDoctorAutocomplete = function() {
	
	var dfd = jQuery.Deferred();

	dfd.notify('rendering doctor autocomplete');
	
	var selector = "[data-report-field='doctor']";
	var source = "DataTableAction.do?action=search&field=customer&bpgroup=Doctor";
	var labelKey = "name";
	var valueKey = "c_bpartner_id";
	    
	Report.AUTO_COMPLETE(selector, source, labelKey, valueKey);	
	
	dfd.resolve("Successfully rendered doctor autocomplete");
	
	return dfd.promise();
};


/**
 * selector - [data-report-field='product']
 * src - DataTableAction.do?action=search&field=product
 * labelKey - name
 * valueKey - m_product_id
 */
Report.AUTO_COMPLETE = function( selector, src, labelKey, valueKey ){
	
	var autocomplete = jQuery( selector ).autocomplete({
		
		source: src,
		
		create: function() {
			
			var label = labelKey;
			
			jQuery(this).data('autocomplete')._renderItem = function( ul, item ) {
				
				if(item == null) return jQuery("<li>");
				
			      return jQuery( "<li>" )
			        .data( "item.autocomplete", item )
			        .append( "<a>" + item[label] + "</a>" )
			        .appendTo( ul );
			};
	    },
	    
	    select: function(event, ui) {
	    	
            var field = jQuery(this);
            
            var label = labelKey;
            var value = valueKey;
            
            field.val(ui.item[label]);

            var hiddenTextfieldId = field.attr('data-report-field-autocomplete');
            
            if( hiddenTextfieldId != null && hiddenTextfieldId.length > 0 ) {
            	
            	jQuery('#' + hiddenTextfieldId).val(ui.item[value]);
            }            

            return false;
        },
        
        change: function(event, ui) {
            var field = jQuery(this);
            var hiddenTextfieldId = field.attr('data-report-field-autocomplete');

            if (ui.item == null) {
                jQuery('#' + hiddenTextfieldId).val('');
                jQuery(this).val('');
            }

            return false;
        }
		
	});
	
	
	/* set default value */	
	
	jQuery(selector).each(function() {
        var name = jQuery(this).attr("name");
        if (Report.defaults[name]) {
            jQuery(this).val(Report.defaults[name]);
            //clear default
            Report.defaults[name] = null;
            console.log("Setting --> " + name);
        }

        var hiddenTextfieldId = jQuery(this).attr('data-report-field-autocomplete');
        jQuery('#' + hiddenTextfieldId).each(function() {
            var name = jQuery(this).attr("name");
            if (Report.defaults[name]) {
                jQuery(this).val(Report.defaults[name]);
                //clear default
                Report.defaults[name] = null;
                console.log("Setting --> " + name);
            }
        });
    });
	
	return autocomplete;
	
};

/**
 * selector - select element
 * url - url for data
 * renderDropdown - function to render select
 */
Report.DROPDOWN = function( selector, url, renderDropdown ){
	
	var dfd = jQuery.Deferred();
    
    dfd.notify('requesting ' + selector + ' data ..');
    
    jQuery.post( url, 
    		{},
			function(json, textStatus, jqXHR){	
				
    			if (json == null || jqXHR.status != 200) {
                    dfd.reject("Failed to request data!");
                    return;
                }

                var dropdown = jQuery(selector);
                
                if(renderDropdown == null){
                	
                	renderDropdown = function( element, json ){
                		
                		DataTable.utils.populateSelect(dropdown, json, "name", "value");
                		
                	};
                }
                
                renderDropdown(dropdown, json);

                //set value
                dropdown.each(function() {
                    var name = jQuery(this).attr("name");
                    if (Report.defaults[name]) {
                        jQuery(this).val(Report.defaults[name]);
                        
                        //trigger events
                        jQuery(this).trigger( "change" );
                        
                        //clear default
                        Report.defaults[name] = null;
                        console.log("Setting --> " + name);
                    }
                });
                
                dfd.resolve("Successfully rendered " + selector + " dropdown");
				
			},"json").fail(function(){
				alert("Failed to request " + selector + " data!");
			});
    
    return dfd.promise();
};

//--------------------------------------------------------------------------
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function viewOrder(id) {
	Report.preloader.show();
	
	window.location = "OrderAction.do?action=view&orderId=" + id;
};

function viewInventory(id) {
	Report.preloader.show();
	
	window.location = "InventoryAction.do?action=view&inventoryId=" + id;
};

function viewTransfer(id) {
	Report.preloader.show();
	
	window.location = "StockTransferAction.do?action=view&movementId=" + id;
};

function viewPayment(id) {
	Report.preloader.show();
	
	window.location = "PaymentAction.do?action=view&print=true&paymentId=" + id;
};

function viewSpotCheck(id) {
	Report.preloader.show();
	
	window.location = "InventorySpotCheckAction.do?action=view&inventorySpotCheckId=" + id;
};

function viewPurchase(id) {
	Report.preloader.show();
	
	window.location = "PurchaseAction.do?action=view&c_order_id=" + id;
};

function viewShipment(id) {
	Report.preloader.show();
	
	window.location = "PurchaseAction.do?action=viewShipment&m_inout_id=" + id;
};

Report.displayError = function (error){
	var container = jQuery(".attendance-message-report");	
	container.html('<div class="backoffice-alert backoffice-alert-danger"><i class="fa fa-times"></i>&nbsp;'+ error +'<button type="button" class="close" onclick="">&times;</button></div>');
	jQuery(".attendance-message-report").show();
	
	resetStarPosOfMainBody(10);
	
	Report.closeAlert();
};

Report.displayMessage = function(success){
	var container = jQuery(".attendance-message-report");
	container.html('<div class="backoffice-alert backoffice-alert-success"><i class="fa fa-check"></i>&nbsp;'+ success +'<button type="button" class="close" onclick="">&times;</button></div>');
	jQuery(".attendance-message-report").show();
	
	resetStarPosOfMainBody(10);
	
	Report.closeAlert();
};

Report.clearMessage = function(){
	jQuery(".backoffice-template-message-container").empty();
	jQuery(".message-report").hide();	
	resetStarPosOfMainBody(10);
};

Report.closeAlert = function(){
	jQuery(".attendance-message-report").on("click", function(e){
		Report.clearMessage();
	});
};

Report.getCurrentDate = function() {
	
	var date = new Date();
	
	var currentYear = date.getFullYear();
	var currentDay = ( date.getDate() < 10 ? "0" : "") + date.getDate();
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var currentDate = currentYear + "-" + currentMonth + "-" + currentDay + " 00:00:00";
	
	return currentDate;
};


/* load report */
Report.onFormReady(function(){
	
	var dropdowns = ['store','store-to','store-from','warehouse-multiple', 'terminal-multiple'];
	    
    /* make store multiselect */
		
    jQuery('.view-report').css({overflow:'visible'});
    
    
    for( var i=0; i<dropdowns.length; i++ ){
    	
    	var dropdown = jQuery("[data-report-field='" + dropdowns[i] + "']");
   
	    if( dropdown.length == 1 ) {
	    	
	    	dropdown.find("option").each(function(index) {
	        	
	        	var option = jQuery(this);
	        	var value = option.attr("value");
	        	
	        	if(value == "" || value == "0") {
	        		option.remove();
	        	}			
	    	});	
	    	
	    	dropdown.prop('multiple','multiple');    	
	    	
	    	if(!detectMob()){
	    		
	    		dropdown.SumoSelect({selectAll: true});	    	
		    	dropdown.each(function(){
		    		
		    		if(this.sumo){
		    			
		    			this.sumo.reload();
		    		}
		    		
		    	});
	    		
	    	}
	    	
	    }   
    
    }
	
	/* check if valid */
    if ( Report.isValid() && Report.updateOnReady ) {
    	Report.update();
    }
});

/*Report.onFormReady(function(){

make store multiselect 
	
jQuery('.view-report').css({overflow:'visible'});


var dropdown = jQuery("[data-report-field='store']");

if( dropdown.length == 1 ) {
	
	dropdown.find("option").each(function(index) {
   	
   	var option = jQuery(this);
   	var value = option.attr("value");
   	
   	if(value == "" || value == "0") {
   		option.remove();
   	}			
	});	
   
	dropdown.prop('multiple','multiple').SumoSelect({selectAll: true});
	
	dropdown.each(function(){
		
		if(this.sumo){
			
			this.sumo.reload();
		}
		
	});
	
}    


check if valid 
if ( Report.isValid() && Report.updateOnReady ) {
	Report.update();
}
});*/

Report.onFormReady(function() {

    console.log('form is ready');
    
    var btn = jQuery("<input type='button' id='report-btn-reset' class='btn btn-default' value='Reset'/>");
    
    btn.on("click", function(){
    	
    	jQuery("#report-form").get(0).reset();
    	
    	/* bug fix clear autocompletes */
    	jQuery('#m_product_id').val('');
    	jQuery('#c_bpartner_id').val('');
    	jQuery('#bpartner2_id').val('');
    	
    	var sumoLists = ["store", "store-to", "store-from", "warehouse", "terminal"];
    	
    	for(var i = 0; i < sumoLists.length; i++){
    		
    		var selector = "[data-report-field='" + sumoLists[i] + "']";
    		
    		if(jQuery(selector).length == 1) {
        		
        		if(jQuery(selector)[0].sumo){
        			jQuery(selector)[0].sumo.unSelectAll();
                	jQuery(selector).trigger("change");
        		}    		
            	
        	}
    	}    	
    	
    });
    
    btn.insertBefore(jQuery("#report-btn-refresh"));
});



/* bug fix bootstrap dropdown */
(function() {
    var isBootstrapEvent = false;
    if (window.jQuery) {
        var all = jQuery('*');
        jQuery.each(['hide.bs.dropdown', 
            'hide.bs.collapse', 
            'hide.bs.modal', 
            'hide.bs.tooltip',
            'hide.bs.popover'], function(index, eventName) {
            all.on(eventName, function( event ) {
                isBootstrapEvent = true;
            });
        });
    }
    var originalHide = Element.hide;
    Element.addMethods({
        hide: function(element) {
            if(isBootstrapEvent) {
                isBootstrapEvent = false;
                return element;
            }
            return originalHide(element);
        }
    });
})();

function detectMob() {
    const toMatch = [
        /Android/i,
        /webOS/i,
        /iPhone/i,
        /iPad/i,
        /iPod/i,
        /BlackBerry/i,
        /Windows Phone/i
    ];
    
    return toMatch.some((toMatchItem) => {
        return navigator.userAgent.match(toMatchItem);
    });
}

/* util functions */
Report.formatNumber = function(data){

	if(data == '') return data;
                       			
	var num = new Number(data);
	
	if(isNaN(num)) return data;
	
	return num.toLocaleString('en',{
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });

};

