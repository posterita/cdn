Report.REPORT_CLASS_NAME = "report/TotalMonthlySales";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       /*{ className: "numberedCell", "targets": [1,2,3,4,5,6,7] }*/
                      ]
    });
};

function setIsSold()
{
	var isSold = jQuery("#issoldcheckbox").prop("checked");
	
	if (isSold)
	{
		jQuery("#issold").val("Y");
	}
	else
	{
		jQuery("#issold").val("N");
	}
}

function setShowAll()
{
	var showAll = jQuery("#showallcheckbox").prop("checked");
	
	if (showAll)
	{
		jQuery("#showall").val("Y");
	}
	else
	{
		jQuery("#showall").val("N");
	}
}

jQuery(document).ready(function(){
	
jQuery("#report-btn-create-po").on('click', function(){
	
	jQuery('#modal-select-pricelist').css('display', 'block');
	
	//jQuery('#supplier_id').val(''); 
	//jQuery('#supplier_purchase_pricelist_id').val('');
	//jQuery('#m_warehouse_id').val('');
	//jQuery('#payment_rule').val('');
	//jQuery('#delivery-date').val('');
	//jQuery('#arrival-date').val('');
	
	//renderSupplierList();
	
	renderWarehouseList();
	renderSupplierListForPO();	
});

jQuery('.close, #close-popup').on('click', function(){
	
	jQuery('#modal-select-pricelist').css('display', 'none');
	
	//jQuery('#supplier_id').val(''); 
	//jQuery('#supplier_purchase_pricelist_id').val('');
	//jQuery('#m_warehouse_id').val('');
	//jQuery('#payment_rule').val('');
	//jQuery('#delivery-date').val('');
	//jQuery('#arrival-date').val('');
});

jQuery('#create').on('click', function(){
	
	var isValidateForm = validateForm();
	
	if(!isValidateForm)
		return;
	
	var isFormValid = Report.validate();

    if (isFormValid) {
        Report.getReportTable();
        
        var dateordered = moment().format('YYYY-MM-DD HH:mm:ss');
        
        var params = Report.getParams();    
    	params = params + "&format=json&createpo=true&invoicerule=D&deliveryrule=M&dateordered="+dateordered;
    	
    	Report.preloader.show();
    	
    	jQuery.ajax({
    		type: "POST",
    		url: Report.REPORT_CLASS_NAME,
    		data: params
    		}).done(function(data, textStatus, jqXHR){
    	
    	Report.preloader.hide();
    	
    	if(data == null || jqXHR.status != 200){
			BootstrapDialog.alert('Failed to request!');			
		}
    	
    	if(jqXHR.status == 504)
		{
			BootstrapDialog.alert('Request Failed! Please try again.');
		}
    	
    	if(jqXHR.status == 200){
			
			var response = jqXHR.responseText;	
			
			var document = JSON.parse(response);
			
			if(document.error){	    
				
				BootstrapDialog.alert( "Error: " + document.error );
				jQuery('#modal-select-pricelist').css('display', 'none');
			}
			else
			{
				var documentno = document['documentno'];
				var c_order_id = document['c_order_id'];	    				

				//window.location='PurchaseAction.do?action=edit&c_order_id=' + c_order_id;
				
				//BootstrapDialog.alert('Created PO: ' + document['purchaseorders']);
				
				 BootstrapDialog.confirm('Created PO: ' + document['purchaseorders'], function(e) {
		                if (e) {		                   
		                	window.location='report-shipments.do';
		                }
		            }); 
				
				jQuery('#modal-select-pricelist').css('display', 'none');
			}			
		}
    	
    	}).fail(function(resp) {
	    	Report.preloader.hide();
	    });
    	
    } 
    else
    {
    	alert(Translation.translate("error.no.criteria.csv"));
    }
    
   //jQuery('#supplier_id').val(''); 
   //jQuery('#supplier_purchase_pricelist_id').val('');
   //jQuery('#m_warehouse_id').val('');
   //jQuery('#payment_rule').val('');
   //jQuery('#delivery-date').val('');
   //jQuery('#arrival-date').val('');
	
});

validateForm = function(){
	
	/*	
	if( jQuery('#supplier_purchase_pricelist_id').val() == '' ){
		alert("Select Price List");
		jQuery("#supplier_purchase_pricelist_id").focus();					
		return false;
	}*/
	
	if( jQuery('#m_warehouse_id').val() == '' ){
		alert("Select Deliver To");
		jQuery("#m_warehouse_id").focus();					
		return false;
	}
	
	if( jQuery('#po_supplier_id').val() == null ){
		alert("Select supplier");
		jQuery("#po_supplier_id").focus();					
		return false;
	}
	
	/*if( jQuery('#payment_rule').val() == '' ){
		alert("Select Payment");
		jQuery("#payment_rule").focus();					
		return false;
	}	
	
	if( jQuery('#delivery-date').val() == '' ){
		alert("Select Delivery Date");
		jQuery("#delivery-date").focus();					
		return false;
	}
	
	if( jQuery('#arrival-date').val() == '' ){
		alert("Select Arrival Date");
		jQuery("#arrival-date").focus();					
		return false;
	}*/
	
	return true;
	
}

renderSupplierList = function() {
	
	jQuery.post("DataTableAction.do", { action: "getVendorList"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}				
				
				var supplier_list = json["vendor"];
				
				//render purchases price list
				DataTable.utils.populateSelect(jQuery("#supplier_id"), supplier_list, "name", "value");							
								
			},"json").fail(function(){
				alert("Failed to request data!");
			});	
};

renderSupplierPurchasePriceList = function (){	
	
	var supplier_id = jQuery('#supplier_id').val();
	
	if(supplier_id == ''){
		alert('Supplier cannot be empty');
		return;
	}
	
	var supplierPurchasePriceList = [];
			
	var params = {
				action: "getSupplierPurchasePriceList",
				c_bpartner_id: supplier_id
			};

	 jQuery.ajax({
         url: 'DataTableAction.do',
         dataType: 'text',
         type: 'post',
         contentType: 'application/x-www-form-urlencoded',
         data: params,
         success: function( data, textStatus, jQxhr ){
        	 
        	 var json = JSON.parse(data);
        		 
        		 var supplier_purchase_price_list = json["supplierPurchasePriceList"];
        		 
        		 if( supplier_purchase_price_list.hasOwnProperty('name') ){
        			 
        			 supplierPurchasePriceList.push(supplier_purchase_price_list);
     				
                	 //render purchases price list
        			DataTable.utils.populateSelect(jQuery("#supplier_purchase_pricelist_id"), supplierPurchasePriceList, "name", "value");
        		 }
        		 else
        		 {
        			 jQuery.post("DataTableAction.do", { action: "getPurchasePriceList"},
    						function(json, textStatus, jqXHR){	
    							
    							if(json == null || jqXHR.status != 200){
    								alert("Failed to request data!"); 
    								return;
    							}				
    							
    							supplierPurchasePriceList = json["purchasePrice"];
    							
    							//render purchases price list
    							DataTable.utils.populateSelect(jQuery("#supplier_purchase_pricelist_id"), supplierPurchasePriceList, "name", "value");							
    											
    						},"json").fail(function(){
    							alert("Failed to request data!");
    						});
        		 }
        	 
         },
         error: function( jqXhr, textStatus, errorThrown ){
             console.log( errorThrown );
         }
     });
}

renderWarehouseList = function(){
	
	jQuery.post("DataTableAction.do", { action: "getWarehousesList"},
			function(json, textStatus, jqXHR){	
				
				if(json == null || jqXHR.status != 200){
					alert("Failed to request data!"); 
					return;
				}				
				
				var warehouse_list = json;
				
				//render purchases price list
				DataTable.utils.populateSelect(jQuery("#m_warehouse_id"), warehouse_list, "name", "value");							
								
			},"json").fail(function(){
				alert("Failed to request data!");
			});
}



var dropdowns = ['supplier-multiple'];
    
/* make supplier multiselect */
	
jQuery('.view-report').css({overflow:'visible'});


for( var i=0; i<dropdowns.length; i++ ){
	
	var dropdown = jQuery("[data-report-field='" + dropdowns[i] + "']");

    if( dropdown.length == 1 ) {
    	
    	dropdown.find("option").each(function(index) {
        	
        	var option = jQuery(this);
        	var value = option.attr("value");
        	
        	if(value == "" || value == "0") {
        		option.remove();
        	}			
    	});	
    	
    	dropdown.prop('multiple','multiple');  
    	
    	if(!detectMob()){
    		
    		dropdown.SumoSelect({selectAll: true});	    	
	    	dropdown.each(function(){
	    		
	    		if(this.sumo){
	    			
	    			this.sumo.reload();
	    		}
	    		
	    	});
    		
    	}
    	
    }   

}


renderSupplierListForPO = function(){
        
        var params = Report.getParams();    
    	params = params + "&getsuppliersforpo=true";
    	
    	Report.preloader.show();
    	
    	jQuery.ajax({
    		type: "POST",
    		url: Report.REPORT_CLASS_NAME,
    		data: params
    		}).done(function(data, textStatus, jqXHR){
    	
    	Report.preloader.hide();
    	
    	if(data == null || jqXHR.status != 200){
			BootstrapDialog.alert('Failed to request!');			
		}
    	
    	if(jqXHR.status == 504)
		{
			BootstrapDialog.alert('Request Failed! Please try again.');
		}
    	
    	if(jqXHR.status == 200){
			
		}
    	    	
    	jQuery.post("DataTableAction.do", { action: "getSupplierListForPO"},
    			function(json, textStatus, jqXHR){	
    				
    				if(json == null || jqXHR.status != 200){
    					alert("Failed to request data!"); 
    					return;
    				}				
    				
    				var supplier_list_for_po = JSON.parse(json.supplier_list);
    				
    				//
    				var dropdown = jQuery("[data-report-field='supplier-multiple']");
    				if(dropdown.length == 0) return;
    				
    			    var select = dropdown;
    				select.html("");	
    				
    						
    				var optgroup = jQuery("<optgroup/>");
    					    
    						
    						if (supplier_list_for_po != null)
    						{	
    							var list = supplier_list_for_po.data;
    							
    							for(var i=0; i<list.length; i++)
    							{
    								var json = list[i];
    								optgroup.append(jQuery("<option/>",{value:json["id"], html:json["name"]}));
    							}
    						}
    						
    						select.append(optgroup);
    						

    			    //set value
    			    dropdown.each(function() {
    			        var name = jQuery(this).attr("name");
    			        if (Report.defaults[name]) {
    			            jQuery(this).val(Report.defaults[name]);
    			            //clear default
    			            Report.defaults[name] = null;
    			            console.log("Setting --> " + name);
    			        }
    			        
    			        if(this.sumo){
    			        	this.sumo.reload();
    			        	this.sumo.selectAll();
    			        }
    			    });
    				
    				//DataTable.utils.populateSelect(jQuery("#m_warehouse_id"), warehouse_list, "name", "value");							
    								
    			},"json").fail(function(){
    				alert("Failed to request data!");
    			});
    	
    	}).fail(function(resp) {
	    	Report.preloader.hide();
	    });
	
}





});
