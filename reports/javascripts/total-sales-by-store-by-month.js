Report.REPORT_CLASS_NAME = "report/TotalSalesByStoreByMonth";

Report.renderReportTable = function() {
	
	var size = jQuery("#example thead th").size();
	var array = [];
	
	for (var i=0; i<size; i++)
	{
		array[i-1] = i;
	}
	
    var table = jQuery('#example').DataTable({
    	"paging":   false,
    	"searching":   false,
        "ordering": false,
        "info":     false,
        "scrollX": true,
        "scrollCollapse": true,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
	    "columnDefs": [ 
                       { 
                       	"className" : "numberedCell", 
                       	"targets" : array , 
                       	"render" : function (data, type, row) {
         			    	return Report.formatNumber(data);
         			    }
         			   },
                       { "width": "150px", "targets": 0 }
                      ]
    });
    
    new jQuery.fn.dataTable.FixedColumns( table );
};

Report.getParams = function() {
	
	var startYear = jQuery("#year").val();
	var startMonth = jQuery("#start_month").val();
	var period = parseInt(jQuery("#period").val());
	
	
	var fromDate = new Date(startYear + "/" + startMonth);
	var toDate = moment(fromDate).add(period, 'month');	
	
	toDate = moment(toDate).endOf('month');
	
	jQuery("#date-to").val(moment(toDate).format("MM/DD/YYYY"));
	jQuery("#date-from").val(moment(fromDate).format("MM/DD/YYYY"));
	
    var params = jQuery("#report-form").serialize();
    return params;
};

Report.selectCurrentMonth = function() {
	
	var select = jQuery("#start_month option");
	var currentMonth = new Date().getMonth();
	
	select.eq(currentMonth).prop('selected', true);
};
 
Report.onFormReady(function() {
	Report.selectCurrentMonth();
});