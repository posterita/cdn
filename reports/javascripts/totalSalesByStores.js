Report.REPORT_CLASS_NAME = "report/TotalSalesByStore";

Report.renderReportTable = function() {

	var size = jQuery("#example thead th").size();
	
	var array = [];
	
	for(var i=1; i<size; i++)
	{
		array[i-1]=i;
	}
	
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                        { className: "numberedCell", "targets": array },
                       	{ "width": "150px", "targets": [0] },
                       	{
              		        "render": function (data, type, row) {
              		        	
              		        	if (data == "-") return data;
              		        	
              		        	return parseFloat(data).format(2);
              			     },
              			     "targets": array
              		     }
                      ],
	     "language": {
				     	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				     },
	    "drawCallback": function ( settings ) {
	    	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            
            var trLength = jQuery(rows[0]).children('td').length;
            
            var totals = [];
            
        	for(var j=1; j<trLength; j++){
        		 totals[j-1] = 0;
        		 
        		 for (var i=0; i<rows.length; i++){
        			 var trValue = jQuery(rows[i]).children('td').eq(j).text() + "";

        			 if (trValue == "-") continue;
        			 
        			 trValue = trValue.replace(/,/g,'');
        			 
        			 totals[j-1] = totals[j-1] + parseFloat(trValue);
        		 }
        	}
        	
        	var tr = '<tr><td>' + Translation.translate("total") + ':</td>';
        	
        	for(var k=0; k<totals.length; k++){
        		tr = tr + '<td class="numberedCell">' + parseFloat(totals[k]).format(2) + '</td>';
        	}
        		
        	tr = tr + '</tr>';
        	
        	jQuery(rows).eq(rows.length - 1).after(
        			tr
            );
            
	        }
    });
};
/*
Report.getParams = function () {
	
	var params = {};
	
	var fromDate = jQuery("#date-from").val();
	var toDate = jQuery("#date-to").val();
	var ad_org_id = jQuery("#ad_org_id").val();
	var u_posterminal_id = jQuery("#u_posterminal_id").val();
	var period = jQuery("#periods").val();
	
	params.fromDate = (fromDate.trim() ? moment(fromDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	params.toDate = (toDate.trim() ? moment(toDate).format("YYYY-MM-DD 00:00:00") : Report.getCurrentDate() );
	
	params.ad_org_id = ad_org_id;
	params.u_posterminal_id = u_posterminal_id;
	
	params.period = period;
		
	return params;
};

jQuery(document).ready(function() {
	jQuery( "#date-from" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
	
	jQuery( "#date-to" ).datepicker({
		format: "mm/dd/yyyy",
		orientation: "top left",
		endDate: '+0d',
		autoclose: true
	}).on('hide', function (e) { e.preventDefault(); });
});*/