Report.REPORT_CLASS_NAME = "report/TransactionDetails";


Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
		"order": [ [0, "desc"] ],
        "columnDefs": [ 
			             { 	className: "numberedCell", "targets": [5] },
			             
	        		     {
         			        "render": function (data, type, row) {
         			          var id = row[9];
         			          var trxType = row[10];
         			          
         			          if(trxType == 'M+' || trxType == 'M-'){
         			        	 return "<a href='javascript:void(0);' onclick='viewTransfer(" + id + ")'>" + data + "</a>";
         			          }
         			          if(trxType == 'I+' || trxType == 'I-'){
         			        	 return "<a href='javascript:void(0);' onclick='viewInventory(" + id + ")'>" + data + "</a>";
         			          }
         			          else
         			          {
         			        	 return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
         			          }
         			         
         			        },
         			        "targets": 2
	         			 },
	             		 { "visible": false, "targets": [9,10] }
	             		
		      		  ]
	});
}

Report.getCurrentDate = function() {
	
	var date = new Date();
	
	var currentYear = date.getFullYear();
	var currentDay = ( date.getDate() < 10 ? "0" : "") + date.getDate();
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var currentDate = currentYear + "-" + currentMonth + "-" + currentDay + " 00:00:00";
	
	return currentDate;
};

Report.getStartingDate = function() {
	var date = new Date();
	
	if ( window.location.hash.indexOf("m_product_id") < 0 ) 
	{
		date.setTime(date.getTime() - (7*24*60*60*1000));
	}
	else
	{
		date.setTime(date.getTime() - (30*24*60*60*1000));
		
		/* update report on load */
		Report.updateOnReady = true;
	}	
	
	var currentYear = date.getFullYear();
	var firstDay = "01";
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};


/* set report defaults */
Report.defaults = {
		"date-from" : moment(Report.getStartingDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY")
		
};
