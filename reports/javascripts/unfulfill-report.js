Report.REPORT_CLASS_NAME = "report/Unfulfill";
	
Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                   		{ className: "numberedCell", "targets": [3,4,5,6,7,8,9,10] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "order": [[1, "asc"]]
	});
};

jQuery(document).ready(function(){
	
	//override validate function
	var original = Report.validate;
	
	Report.validate = function(){
		
		var result = original();
		
		if(result == false){
			return result;
		}
		else
		{
			//validate shop and ho 
			if(jQuery('#m_warehouse_id_shop').val() == jQuery('#m_warehouse_id_HO').val()){
				
				BootstrapDialog.alert("Error: Shop and Head Office cannot be same");
				
				return false;
			}
			
			return true;
		}
		
	};
	
	jQuery("#report-btn-generate-picking-list").on("click", function(){
		
		var isFormValid = Report.validate();

	    if (isFormValid) {
	        
	    	var params = Report.getParams();    
	    	params = params + "&format=json&getpickinglist=true";
	    	
	    	Report.preloader.show();
	    	
	    	//generate a picking document and return the document no
	    	jQuery.ajax({
	    		type: "POST",
	    		url: Report.REPORT_CLASS_NAME,
	    		data: params
	    		}).done(function(data, textStatus, jqXHR){
	    			
	    		Report.preloader.hide();
	    		
	    		if(data == null || jqXHR.status != 200){
	    			BootstrapDialog.alert('Failed to request!');			
	    		}
	    		
	    		if(jqXHR.status == 504)
	    		{
	    			BootstrapDialog.alert('Request Failed! Please try again.');
	    		}
	    		
	    		if(jqXHR.status == 200){
	    			
	    			var response = jqXHR.responseText;	
	    			
	    			var document = JSON.parse(response);
	    			
	    			if(document.error){	    				
	    				BootstrapDialog.alert( "Error: " + document.error );	    				
	    			}
	    			else
	    			{
	    				var documentno = document['documentno'];
	    				var u_pos_pickinglist_id = document['u_pos_pickinglist_id'];	    				

	    				BootstrapDialog.alert("Successfully generated picking list. <a href='PickingListAction.do?action=view&pickingListId=" + u_pos_pickinglist_id + "'>#" + documentno + "</a>");
	    			}
	    			
	    		}	    		
	    		
	    		
	    	}).fail(function(resp) {
	    		Report.preloader.hide();
	    	});
	    	
	    } 
	    else
	    {
	    	alert(Translation.translate("error.no.criteria.csv"));
	    }
	});
	
});

function printPickingList( document ){
	
	var configuration = PrinterManager.getPrinterConfiguration();

    var LINE_WIDTH = configuration.LINE_WIDTH;
    var LINE_SEPARATOR = JSReceiptUtils.replicate('-', LINE_WIDTH);
    
    var date = moment().format('DD MMM YYYY, HH:mm');
	
	var printFormat = [
		['CENTER'],
		['N', LINE_SEPARATOR],
		["H1", "Picking List"],
		['N', LINE_SEPARATOR],
		["B","From: " + document.header.from ],
		["B","To: " + document.header.to ],
		["B","User: " + document.header.user ],
		["B","Date: " + date ]
	];
	
	/*
	// add table header
	printFormat.push(['N', LINE_SEPARATOR]);
	printFormat.push(['B', JSReceiptUtils.format('Location', 10) + JSReceiptUtils.format('Product', LINE_WIDTH - 22 ) + JSReceiptUtils.format('QtyToPick', 10)]);
	printFormat.push(['N', LINE_SEPARATOR]);
	*/
	
	var line;
	
	var currentLocation = null;	
	
	for(var i=0; i< document.lines.length; i++){
		
		line = document.lines[i];
		
		if(currentLocation != line.location){
			
			if( currentLocation != null ){
				printFormat.push(['FEED']);
			}
			
			currentLocation = line.location;
			
			printFormat.push(['N', LINE_SEPARATOR]);
			//printFormat.push(['LEFT']);
			printFormat.push(['H2', JSReceiptUtils.format( 'LOC: ' + currentLocation, 30 )]);
			//printFormat.push(['CENTER']);
			printFormat.push(['N', LINE_SEPARATOR]);
			
		}
		
		//printFormat.push(['LEFT']);
		printFormat.push(['H4', JSReceiptUtils.format( line.qty + " X " + line.name, 30 ) ]);
		
	}
	
	printFormat.push(['FEED']);
	printFormat.push(['PAPER_CUT']);
	
	PrinterManager.print(printFormat);	
	
	
}


