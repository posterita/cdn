Report.REPORT_CLASS_NAME = "report/ViewInvoicesList";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": [4, 5] },
                       {  "width": "auto", "targets": [0] },
                       {
    			        "render": function (data, type, row) {
    			          var id = row[11];
    			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
    			        },
    			        "targets": 3
                       },
                       { "visible": false, "targets": 11 }
                      ],
	     "order": [ [0, "desc"] ]
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	
	var currentYear = date.getFullYear();
	var firstDay = "01";
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};

/* set report defaults */
Report.defaults = {
		"date-from" : moment(Report.getStartingDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY")
		
};