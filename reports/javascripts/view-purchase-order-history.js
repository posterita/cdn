Report.REPORT_CLASS_NAME = "report/ViewPurchaseOrderHistory";

Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": [3,4,6] },
                       {  "width": "auto", "targets": [0] },
                       {
    			        "render": function (data, type, row) {
    			          var id = row[12];
    			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
    			        },
    			        "targets": 1
                       },
                       { "visible": false, "targets": 12 }
                      ],
	     "order": [ [0, "desc"] ]
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	
	var currentYear = date.getFullYear();
	var firstDay = "01";
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};


/* set report defaults */
Report.defaults = {
		"date-from" : moment(Report.getCurrentDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY"),
		"document_status" : "CO"		
};

/* update report on load */
Report.updateOnReady = true;