Report.REPORT_CLASS_NAME = "report/ViewQuotationHistory";

Report.hash_id = null;
	
Report.renderReportTable = function() {
	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       { className: "numberedCell", "targets": [2,3,4] },
                       {  "width": "auto", "targets": [0] },
                       {
    			        "render": function (data, type, row) {
    			          var id = row[10];
    			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
    			        },
    			        "targets": 1
                       },
                       {
       			        "render": function (data, type, row) {
       			          if(data == null || data == 'null') return "";
       			          var id = row[11];    			          
       			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
       			        },
       			        "targets": 9
                       },
                       { "visible": false, "targets": [10,11] }
                      ],
	     "order": [ [0, "desc"] ],
	     "drawCallback": function (settings) {}
    });
};

Report.getStartingDate = function() {
	var date = new Date();
	date.setTime(date.getTime() - (30*24*60*60*1000));
	
	var currentYear = date.getFullYear();
	var firstDay = "01";
	var currentMonth = ( (date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1);
	
	var startDate = currentYear + "-" + currentMonth + "-" + firstDay + " 00:00:00";
	
	return startDate;
};


/* set report defaults */

Report.defaults = {
		"date-from" : moment(Report.getStartingDate()).format("MM/DD/YYYY"),
		"date-to" : moment(Report.getCurrentDate()).format("MM/DD/YYYY"),
		"document_status" : "IP"		
};

/* update report on load */
Report.updateOnReady = true;
