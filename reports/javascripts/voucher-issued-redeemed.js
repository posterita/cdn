Report.REPORT_CLASS_NAME = "report/VoucherIssuedRedeemed";
	
Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
		"searching": false,
		"scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
                   		{
	     			        "render": function (data, type, row) {
	     			          var id = row[8];
	     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 0
	         			},
	         			{
	     			        "render": function (data, type, row) {
	     			          var id = row[7];
	     			          
	     			          if(id == 0) return row[6];
	     			          
	     			          return "<a href='javascript:void(0);' onclick='viewOrder(" + id + ")'>" + data + "</a>";
	     			        },
	     			        "targets": 6
	         			},
	         			
	             		{ 	"visible": false, "targets": [7,8] }
                      ],
	    "language": {
				    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
				    },
	    "order": [[1, "desc"]]  	    
	});
};
