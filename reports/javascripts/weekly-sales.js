Report.REPORT_CLASS_NAME = "report/WeeklySales";

Report.renderReportTable = function() {

	var table = jQuery('#example').DataTable({
        "searching": false,
        "scrollX": true,
        "scrollCollapse": true,
        "autoWidth": false,
        "language": {
	    	"emptyTable": Translation.translate("no.data.available.for.criteria.selected")
	    },
        "columnDefs": [
                       /*{ className: "numberedCell", "targets": [1,2,3,4,5,6,7] }*/
                      ]
    });
};

function setIsSold()
{
	var isSold = jQuery("#issoldcheckbox").prop("checked");
	
	if (isSold)
	{
		jQuery("#issold").val("Y");
	}
	else
	{
		jQuery("#issold").val("N");
	}
}

function setShowAll()
{
	var showAll = jQuery("#showallcheckbox").prop("checked");
	
	if (showAll)
	{
		jQuery("#showall").val("Y");
	}
	else
	{
		jQuery("#showall").val("N");
	}
}